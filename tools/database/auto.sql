/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50638
Source Host           : localhost:3306
Source Database       : auto

Target Server Type    : MYSQL
Target Server Version : 50638
File Encoding         : 65001

Date: 2017-11-27 22:24:14
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `t_com_doc`
-- ----------------------------
DROP TABLE IF EXISTS `t_com_doc`;
CREATE TABLE `t_com_doc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `TYPE_ID` int(11) NOT NULL COMMENT '知识分类',
  `TITLE` varchar(256) NOT NULL COMMENT '标题',
  `KEYWORD` varchar(512) DEFAULT NULL COMMENT '关键字',
  `CTIME` datetime DEFAULT NULL COMMENT '上传日期',
  `USER_ID` int(11) DEFAULT NULL COMMENT '上传人员',
  `EVALUATE` varchar(32) DEFAULT NULL COMMENT '知识评星',
  `REMARK` varchar(1024) DEFAULT NULL COMMENT '知识简介',
  `STATE` varchar(32) DEFAULT NULL COMMENT '状态',
  `SORT` int(11) NOT NULL COMMENT '排序号',
  `PARAM1` varchar(512) DEFAULT NULL COMMENT '参数一',
  `PARAM2` varchar(512) DEFAULT NULL COMMENT '参数二',
  `PARAM3` varchar(512) DEFAULT NULL COMMENT '参数三',
  `LUSER` int(11) DEFAULT NULL COMMENT '最后编辑人',
  `LTIME` datetime DEFAULT NULL COMMENT '最后编辑时间',
  PRIMARY KEY (`ID`),
  KEY `FK_REFERENCE_7` (`TYPE_ID`),
  CONSTRAINT `FK_REFERENCE_7` FOREIGN KEY (`TYPE_ID`) REFERENCES `t_com_doc_type` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='知识库';

-- ----------------------------
-- Records of t_com_doc
-- ----------------------------
INSERT INTO t_com_doc VALUES ('1', '2', '注册账号', null, '2017-11-10 00:00:00', '1', null, '<div>如何注册京东账号？</div><p>若您还没有京东账号，请点击<span><a target=\"_blank\" rel=\"nofollow\" href=\"https://reg.jd.com/reg/person?ReturnUrl=http%3A%2F%2Fwww.jd.com\">注册</a>，详细操作步骤如下：&nbsp;</span></p><p>1. 打开京东首页，在右上方，点击“免费注册”按钮；</p><p><img width=\"882\" alt=\"\" src=\"https://img30.360buyimg.com/pophelp/jfs/t3070/231/61588979/456289/7ac22ef8/57a1b0d1Nd006ba3b.png\" height=\"404\" title=\"Image: https://img30.360buyimg.com/pophelp/jfs/t3070/231/61588979/456289/7ac22ef8/57a1b0d1Nd006ba3b.png\"></p><p>&nbsp;</p><p>2. 进入到注册页面，请填写您的邮箱、手机等信息完成注册；</p><p><img width=\"935\" alt=\"\" src=\"https://img30.360buyimg.com/pophelp/jfs/t3100/138/55572335/120548/eee1bd21/57a1be2eN2594202b.png\" height=\"490\"></p><p>&nbsp;</p><p>3. 注册成功后，请完成账户安全验证，来提高您的账户安全等级。</p>', '01', '10', null, null, null, '1', '2017-11-10 10:23:47');
INSERT INTO t_com_doc VALUES ('2', '2', '购物流程', '购物  流程  商城', '2017-11-10 00:00:00', '1', null, '<div>京东购物流程</div><p><img alt=\"\" src=\"https://img30.360buyimg.com/pophelp/g10/M00/13/11/rBEQWVFlHpYIAAAAAACI4qKbEu4AAD0VAKcRBMAAIj6957.jpg\" title=\"Image: https://img30.360buyimg.com/pophelp/g10/M00/13/11/rBEQWVFlHpYIAAAAAACI4qKbEu4AAD0VAKcRBMAAIj6957.jpg\"></p>', '01', '10', null, null, null, '1', '2017-11-10 10:32:53');

-- ----------------------------
-- Table structure for `t_com_doc_type`
-- ----------------------------
DROP TABLE IF EXISTS `t_com_doc_type`;
CREATE TABLE `t_com_doc_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '类型主键',
  `NAME` varchar(64) NOT NULL COMMENT '类型名称',
  `PARENT_ID` int(11) NOT NULL COMMENT '上级类型',
  `STATE` varchar(32) NOT NULL COMMENT '类型状态',
  `PATH` varchar(256) DEFAULT NULL COMMENT '类型路径',
  `SORT` int(11) NOT NULL COMMENT '排序号',
  `REMARK` varchar(1024) DEFAULT NULL COMMENT '类型描述',
  `PARAM1` varchar(512) DEFAULT NULL COMMENT '参数1',
  `PARAM2` varchar(512) DEFAULT NULL COMMENT '参数2',
  `PARAM3` varchar(512) DEFAULT NULL COMMENT '参数3',
  `LUSER` int(11) DEFAULT NULL COMMENT '最后修改人',
  `LTIME` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='知识库类型';

-- ----------------------------
-- Records of t_com_doc_type
-- ----------------------------
INSERT INTO t_com_doc_type VALUES ('1', '帮助指南', '0', '01', ',1,', '0', null, null, null, null, '1', '2017-11-09 22:35:01');
INSERT INTO t_com_doc_type VALUES ('2', '购物指南', '1', '01', ',1,2,', '1', null, null, null, null, '1', '2017-11-09 22:41:50');
INSERT INTO t_com_doc_type VALUES ('4', '支付方式', '1', '01', ',1,4,', '2', null, null, null, null, '1', '2017-11-09 23:01:43');
INSERT INTO t_com_doc_type VALUES ('5', '物流配送', '1', '02', ',1,5,', '3', null, null, null, null, '1', '2017-11-09 23:38:37');
INSERT INTO t_com_doc_type VALUES ('6', '售后服务', '1', '01', ',1,6,', '4', null, null, null, null, '1', '2017-11-09 22:47:02');
INSERT INTO t_com_doc_type VALUES ('7', '开发手册', '0', '01', ',7,', '1', null, null, null, null, '1', '2017-11-09 23:03:24');
INSERT INTO t_com_doc_type VALUES ('8', '系统配置', '7', '01', ',7,8,', '0', null, null, null, null, '1', '2017-11-10 15:24:23');

-- ----------------------------
-- Table structure for `t_com_feedback`
-- ----------------------------
DROP TABLE IF EXISTS `t_com_feedback`;
CREATE TABLE `t_com_feedback` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `TITLE` varchar(256) NOT NULL COMMENT '标题',
  `TYPE` varchar(32) NOT NULL COMMENT '类型',
  `SRC` varchar(64) DEFAULT NULL COMMENT '来源',
  `USER_ID` int(11) DEFAULT NULL COMMENT '反馈人ID',
  `USER_NAME` varchar(64) NOT NULL COMMENT '反馈人名称',
  `CTIME` datetime NOT NULL COMMENT '反馈日期',
  `CONTENT` text COMMENT '反馈内容',
  `STATE` varchar(32) NOT NULL COMMENT '状态',
  `ATTA` varchar(512) DEFAULT NULL COMMENT '附件信息',
  `PARAM1` varchar(512) DEFAULT NULL COMMENT '参数一',
  `PARAM2` varchar(512) DEFAULT NULL COMMENT '参数二',
  `PARAM3` varchar(512) DEFAULT NULL COMMENT '参数三',
  `LUSER` int(11) DEFAULT NULL COMMENT '最后编辑人',
  `LTIME` datetime DEFAULT NULL COMMENT '最后编辑时间',
  `REPLY_ID` int(11) DEFAULT NULL COMMENT '回复人ID',
  `REPLY_TIME` datetime DEFAULT NULL COMMENT '回复日期',
  `REPLY_CONTENT` text COMMENT '回复内容',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='系统反馈';

-- ----------------------------
-- Records of t_com_feedback
-- ----------------------------
INSERT INTO t_com_feedback VALUES ('1', '油价迎年内第9次上涨', '01', null, '0', '小王', '2017-11-10 12:53:52', 'index/feedback', '02', null, null, null, null, '1', '2017-11-10 13:17:28', '1', '2017-11-10 13:17:28', '回复内容回复内容回复内容回复内容回复内容回复内容回复内容回复内容回复内容回复内容回复内容回复内容');
INSERT INTO t_com_feedback VALUES ('2', '登录错误', '01', null, '0', '小王', '2017-11-10 12:57:46', '显示用户名不存在', '02', null, null, null, null, '1', '2017-11-10 13:21:00', '1', '2017-11-10 13:21:00', '重新登录呀');
INSERT INTO t_com_feedback VALUES ('3', '7位新任省委书记的开局', '01', null, '1', '小张', '2017-11-11 09:45:37', '都是官司', '01', null, null, null, null, '1', '2017-11-11 09:45:37', null, null, null);
INSERT INTO t_com_feedback VALUES ('4', '10月30日，朱镕基对苹果宝马老总们说了什么？', '01', null, '0', '小陈', '2017-11-11 09:46:03', '老领导', '01', null, null, null, null, '0', '2017-11-11 09:46:03', null, null, null);

-- ----------------------------
-- Table structure for `t_com_msg`
-- ----------------------------
DROP TABLE IF EXISTS `t_com_msg`;
CREATE TABLE `t_com_msg` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '消息主键',
  `SUBJECT` varchar(256) NOT NULL COMMENT '消息主题',
  `CTIME` datetime NOT NULL COMMENT '发送时间',
  `FROM_ID` int(11) DEFAULT NULL COMMENT '发件人ID',
  `TO_ID` varchar(512) DEFAULT NULL COMMENT '接收人主键',
  `TO_NAME` varchar(1024) DEFAULT NULL COMMENT '接收人名称',
  `CC_ID` varchar(512) DEFAULT NULL COMMENT '抄送主键',
  `CC_NAME` varchar(1024) DEFAULT NULL COMMENT '抄送名称',
  `ATTA` varchar(32) DEFAULT NULL COMMENT '是否有附件',
  `FLAG` varchar(32) DEFAULT NULL COMMENT '邮件重要性',
  `RECEIPT` varchar(32) DEFAULT NULL COMMENT '是否需要回执',
  `CONTENT` text COMMENT '消息内容',
  `PARAM1` varchar(512) DEFAULT NULL COMMENT '参数1',
  `PARAM2` varchar(512) DEFAULT NULL COMMENT '参数2',
  `PARAM3` varchar(512) DEFAULT NULL COMMENT '参数3',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='系统消息';

-- ----------------------------
-- Records of t_com_msg
-- ----------------------------
INSERT INTO t_com_msg VALUES ('11', '会议通知', '2017-11-12 22:37:18', '2', 'U1', 'Super', null, null, null, null, null, 'test', null, null, null);
INSERT INTO t_com_msg VALUES ('12', '附件测试', '2017-11-12 22:39:08', '2', 'U1;U2', 'Super;管理员', null, null, null, '1', null, '<p>昨天零点一过，天猫与京东就各自晒出了自己的双十一战绩，不出意外天猫以1682亿成为第一名，但是离预想的2000亿还是有点差距，但是相对于去年的1207亿，或是好了不少。<br></p><p>而京东以1271亿也不甘示弱，不过京东是累计11月1日至11日这11天的战绩，如果单论11号这一天，估计千亿都没有，你觉得呢？</p><p>那么有网友就在猜测了，这1682亿与1271亿背后，刘强东与马云谁赚钱更多一点？有人表示肯定马云的少，因为天猫都不是直营。而京东太多直营了。另外刘强东的股份多，所以赚的多。</p><p>事实是不是这样呢？我们来分析分析。</p><p><img alt=\"1116821271\" src=\"http://p1.pstatp.com/large/46d20000e9514d12ba31\"></p><p>京东确实是直营较多，但是电商的利润率并不高，所以看似1271亿元，要论中间的利润率，估计少的可怜，但参照今年京东的收入，肯定不是亏本的。</p><p>而阿里不是直营，所以商家利润与自己无关，但是天猫每年要收服务费的， 还有每笔交易有2-5%的交易费，不管商家赚不赚钱，只要有交易只要入驻，天猫肯定是赚钱的。还有各种广告，营销，推广，直通车等服务都是要钱的。</p><p><img alt=\"1116821271\" src=\"http://p1.pstatp.com/large/46d0000120d1f0a1d6de\"></p><p>另外我们知道天猫还是淘宝的交易都是通过支付宝，支付宝这一天就沉淀了1681亿的资金，而这些资金需要几天才会真正支付到商家手中，这中间产生的拆借利息也是马云赚的。</p><p>另外天猫还有针对商家的各种贷款，针对买家的各种信用服务，这些都是要赚钱的，所以马云赚钱并不是从这1681亿中直接赚，还是间接赚。</p><p><img alt=\"1116821271\" src=\"http://p3.pstatp.com/large/46d30000d731a1ba9050\"></p><p>虽然马云只有7%的股份，但是由于赚钱的门路太多，所以双11，肯定马云赚的比刘强东多多了，而刘强东真的可能只有微利，自营的东西看似笔笔赚钱，但天猫不管商家赚不赚，自己都赚啊，没有对比，就没有伤害。</p>', null, null, null);
INSERT INTO t_com_msg VALUES ('13', '回复：附件测试', '2017-11-12 22:44:23', '1', 'U2', '管理员', null, null, null, '1', null, '<p>昨天零点一过，天猫与京东就各自晒出了自己的双十一战绩，不出意外天猫以1682亿成为第一名，但是离预想的2000亿还是有点差距，但是相对于去年的1207亿，或是好了不少。<br></p><p>而京东以1271亿也不甘示弱，不过京东是累计11月1日至11日这11天的战绩，如果单论11号这一天，估计千亿都没有，你觉得呢？</p><p>那么有网友就在猜测了，这1682亿与1271亿背后，刘强东与马云谁赚钱更多一点？有人表示肯定马云的少，因为天猫都不是直营。而京东太多直营了。另外刘强东的股份多，所以赚的多。</p><p>事实是不是这样呢？我们来分析分析。</p><p><img alt=\"1116821271\" src=\"http://p1.pstatp.com/large/46d20000e9514d12ba31\"></p><p>京东确实是直营较多，但是电商的利润率并不高，所以看似1271亿元，要论中间的利润率，估计少的可怜，但参照今年京东的收入，肯定不是亏本的。</p><p>而阿里不是直营，所以商家利润与自己无关，但是天猫每年要收服务费的， 还有每笔交易有2-5%的交易费，不管商家赚不赚钱，只要有交易只要入驻，天猫肯定是赚钱的。还有各种广告，营销，推广，直通车等服务都是要钱的。</p><p><img alt=\"1116821271\" src=\"http://p1.pstatp.com/large/46d0000120d1f0a1d6de\"></p><p>另外我们知道天猫还是淘宝的交易都是通过支付宝，支付宝这一天就沉淀了1681亿的资金，而这些资金需要几天才会真正支付到商家手中，这中间产生的拆借利息也是马云赚的。</p><p>另外天猫还有针对商家的各种贷款，针对买家的各种信用服务，这些都是要赚钱的，所以马云赚钱并不是从这1681亿中直接赚，还是间接赚。</p><p><img alt=\"1116821271\" src=\"http://p3.pstatp.com/large/46d30000d731a1ba9050\"></p><p>虽然马云只有7%的股份，但是由于赚钱的门路太多，所以双11，肯定马云赚的比刘强东多多了，而刘强东真的可能只有微利，自营的东西看似笔笔赚钱，但天猫不管商家赚不赚，自己都赚啊，没有对比，就没有伤害。</p>', null, null, null);
INSERT INTO t_com_msg VALUES ('14', '转发：会议通知', '2017-11-12 22:44:59', '1', 'U1', 'Super', null, null, null, '1', null, 'test', null, null, null);
INSERT INTO t_com_msg VALUES ('15', '测试', '2017-11-12 23:11:18', '1', 'U1;U2', 'Super;管理员', null, null, null, '1', null, 'test', null, null, null);
INSERT INTO t_com_msg VALUES ('16', '是否有附件', '2017-11-13 12:36:00', '1', 'U2', '管理员', null, null, '1', '1', null, '<p>谈论起华为，人们首先可能会想到那个“军人总裁”任正非，但是请注意，任正非只是总裁。说出来你可能不信，世界500强华为的董事长竟然是一个女的！她叫孙亚芳。在华为，孙亚芳可谓一言九鼎、雷厉风行，尽管她并非华为的创始人，却被圈内人士赋予“至尊红颜”、“华为的‘国务卿’”、“华为女皇”等称谓，孙亚芳的崇高地位由此可见一斑。</p><p>《走出华为》一书中曾经提到，华为的董事长是在1998年华为出现一些风波的情况下设立的，由于孙亚芳在对外协调上的出色能力，任正非提议孙亚芳做董事长，负责外部的协调，自己专心做内部管理。</p>', null, null, null);

-- ----------------------------
-- Table structure for `t_com_msg_box`
-- ----------------------------
DROP TABLE IF EXISTS `t_com_msg_box`;
CREATE TABLE `t_com_msg_box` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '文件夹主键',
  `TYPE` varchar(32) NOT NULL COMMENT '队列类别：01=收件箱；02=发件箱；03=草稿箱',
  `MSG_ID` int(11) NOT NULL COMMENT '消息主键',
  `USER_ID` int(11) NOT NULL COMMENT '文件夹拥有者',
  `READ_TIME` datetime DEFAULT NULL COMMENT '首次阅读时间',
  `RECEIPT_TIME` datetime DEFAULT NULL COMMENT '回执时间',
  `PARAM1` varchar(512) DEFAULT NULL COMMENT '参数1',
  `PARAM2` varchar(512) DEFAULT NULL COMMENT '参数2',
  `PARAM3` varchar(512) DEFAULT NULL COMMENT '参数3',
  PRIMARY KEY (`ID`),
  KEY `FK_REF_SYS_MAIL_FOLDER` (`MSG_ID`) USING BTREE,
  CONSTRAINT `FK_REF_SYS_MAIL_FOLDER` FOREIGN KEY (`MSG_ID`) REFERENCES `t_com_msg` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COMMENT='系统消息文件夹';

-- ----------------------------
-- Records of t_com_msg_box
-- ----------------------------
INSERT INTO t_com_msg_box VALUES ('32', '02', '11', '2', null, null, null, null, null);
INSERT INTO t_com_msg_box VALUES ('33', '01', '11', '1', '2017-11-13 12:37:05', null, null, null, null);
INSERT INTO t_com_msg_box VALUES ('36', '02', '12', '2', null, null, null, null, null);
INSERT INTO t_com_msg_box VALUES ('37', '01', '12', '1', '2017-11-27 20:43:15', null, null, null, null);
INSERT INTO t_com_msg_box VALUES ('38', '01', '12', '2', null, null, null, null, null);
INSERT INTO t_com_msg_box VALUES ('39', '02', '13', '1', null, null, null, null, null);
INSERT INTO t_com_msg_box VALUES ('40', '01', '13', '2', null, null, null, null, null);
INSERT INTO t_com_msg_box VALUES ('41', '02', '14', '1', null, null, null, null, null);
INSERT INTO t_com_msg_box VALUES ('43', '02', '15', '1', '2017-11-13 12:36:58', null, null, null, null);
INSERT INTO t_com_msg_box VALUES ('44', '01', '15', '1', '2017-11-13 12:36:58', null, null, null, null);
INSERT INTO t_com_msg_box VALUES ('45', '01', '15', '2', null, null, null, null, null);
INSERT INTO t_com_msg_box VALUES ('46', '02', '16', '1', '2017-11-13 12:36:00', null, null, null, null);
INSERT INTO t_com_msg_box VALUES ('47', '01', '16', '2', null, null, null, null, null);

-- ----------------------------
-- Table structure for `t_com_notice`
-- ----------------------------
DROP TABLE IF EXISTS `t_com_notice`;
CREATE TABLE `t_com_notice` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `TITLE` varchar(256) NOT NULL COMMENT '标题',
  `TYPE` varchar(32) NOT NULL COMMENT '类型',
  `SRC` varchar(256) DEFAULT NULL COMMENT '来源',
  `ISSURE` varchar(64) DEFAULT NULL COMMENT '签发人',
  `STIME` datetime DEFAULT NULL COMMENT '签发日期',
  `ETIME` datetime DEFAULT NULL COMMENT '到期日期',
  `CONTENT` text COMMENT '公告内容',
  `STATE` varchar(32) NOT NULL COMMENT '状态',
  `PARAM1` varchar(512) DEFAULT NULL COMMENT '参数一',
  `PARAM2` varchar(512) DEFAULT NULL COMMENT '参数二',
  `PARAM3` varchar(512) DEFAULT NULL COMMENT '参数三',
  `LUSER` int(11) DEFAULT NULL COMMENT '最后编辑人',
  `LTIME` datetime DEFAULT NULL COMMENT '最后编辑时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='通知公告';

-- ----------------------------
-- Records of t_com_notice
-- ----------------------------
INSERT INTO t_com_notice VALUES ('1', '无人驾驶中国大跃进：估值1亿美元起，量产时间口炮乱相', '01', '上级通知', '魏总', '2017-11-01 00:00:00', '2017-11-30 00:00:00', '<blockquote><p>资本裹挟下的无人驾驶业正在进行一场大跃进，参与者估值都在1亿美元之上，量产时间被吹着向前。</p><p><img alt=\"1\" src=\"https://p1.pstatp.com/large/4357000165286a61df27\" title=\"Image: https://p1.pstatp.com/large/4357000165286a61df27\"></p><p>文 | AI财经社 四月<br></p><p>编辑 | 赵艳秋</p><p>“我们认为还可以再提前一年”，沉寂半年后，王劲首次公开露面，对着台下数百位交通业同行侃侃而谈。他口中的“提前一年”，指的是无人驾驶车的落地时间点，“到2020年，无人驾驶汽车就会进入大众生活，规模是‘数以万计’”。</p><p>这位百度自动驾驶事业部的首位掌门人，在2015年百度自动驾驶事业部刚落地时，便喊出“将在2021年量产自动驾驶汽车”的口号。跳脱百度这艘大船后，王劲自立门户，创立无人驾驶创业公司景驰科技，判断与言论更为激进。</p><h1>激进！激进！再激进！</h1><p>在过去22个月里，自动驾驶业的“王劲们”越来越激进，尤以中美两地为甚，产业链也被急剧重塑。19家巨头，包括特斯拉、通用等车企，博世、德尔福等汽车业一级供应商，谷歌、百度等科技公司，纷纷提出自动驾驶的落地时间表。“2021年”曾被这些第一梯队企业认为是最早的落地时间。</p><p>然而，这个时间点正被“后进者”推翻。</p><p>据AI财经社统计，在国内公开可查的30家主要的自动驾驶创业公司里，声称在“2021年前”就能实现高等级自动驾驶（L3、L4级）落地的公司超过7家。这批激进的公司大多创立不到一年，而且多为百度系前员工。</p></blockquote>', '01', null, null, null, '1', '2017-11-08 19:18:26');
INSERT INTO t_com_notice VALUES ('2', '油价迎年内第9次上涨', '01', '上级单位', '魏总', '2017-11-16 00:00:00', '2017-11-28 00:00:00', '<blockquote><p><b>央视财经（记者 平凡&nbsp;张宁）11月2日24时，国内成品油调价窗口将再次开启，本计价周期以来，国际原油价格整体呈现出震荡上涨走势，国际油价突破60美元。受此影响，国内汽油、柴油的零售价格也随之上调。央视财经记者最新从国家发改委获悉，此次油价调整具体为：汽、柴每吨均上调150元。</b></p></blockquote><p><img alt=\"96\" src=\"https://p3.pstatp.com/large/435a0002e62c095e91f4\"></p><p>国内油价迎年内第九次上涨！明起加一箱油多花6元</p><p>央视财经记者从国家发改委获悉，本次油价调整具体情况如下：90号汽油每升上调0.11元，92号汽油每升上调0.12元，95号汽油每升上调0.12元，0号柴油每升上调0.13元。按一般家用汽车油箱50L容量估测，加满一箱92号汽油将多花费约6元。</p><p><img alt=\"96\" src=\"https://p3.pstatp.com/large/435a0002e62d7a7b8a29\"></p><p>本轮成品油调价周期内国际油价大幅上涨</p><p>央视财经记者从国家发改委监测中心了解，本轮成品油调价周期内（10月20日—11月2日）国际油价大幅上涨，其中10月27日，布伦特原油期货价格自2015年7月以来首次突破每桶60美元大关，随后价格在60美元上方波动。平均来看，布伦特、WTI油价原油价格比上轮调价周期上涨3.99%。</p><p><img alt=\"96\" src=\"https://p9.pstatp.com/large/435900035f699cbf41bf\"></p><p><img alt=\"96\" src=\"https://p1.pstatp.com/large/435c00006ec57a4c9b49\"></p><p>预计后市国际油价维持高位震荡。一方面，如果欧佩克减产协议得到延长，将加速全球原油市场恢复平衡，为油价提供支撑；同时近期中东地缘政治局势升温也会形成一定的风险溢价。但另一方面，美国页岩油可能利用油价上涨再度扩大生产；同时随着冬季炼厂检修，原油需求将出现季节性低迷，库存可能再度上升；此外，基于油价涨幅较大，部分投资者可能获利了结。</p><p>今年油价调价一览：九涨六跌六搁浅</p><p>经过本次调价，2017年以来，国内成品油调价已呈现“九涨六跌六搁浅”的格局。整体来看，截止至本次调价，2017年汽油价格每吨累计上涨100元，柴油价格每吨累计上涨100元。</p><p><img alt=\"96\" src=\"https://p3.pstatp.com/large/435b000087f6cdd8f116\"></p>', '01', null, null, null, '1', '2017-11-09 13:00:36');
INSERT INTO t_com_notice VALUES ('3', '7位新任省委书记的开局', '01', '上级单位', '魏总', '2017-11-09 00:00:00', '2017-11-23 00:00:00', '<u><i>近日，7省份“一把手”连续调整：李希兼任广东省委书记、李强兼任上海市委书记、于伟国任福建省委书记、王东峰任河北省委书记、陈求发任辽宁省委书记、娄勤俭任江苏省委书记、胡和平任陕西省委书记。</i></u>', '01', null, null, null, '1', '2017-11-11 13:43:15');
INSERT INTO t_com_notice VALUES ('4', '10月30日，朱镕基对苹果宝马老总们说了什么？', '01', null, null, '2017-11-02 00:00:00', '2017-11-23 00:00:00', '<p>2017年10月30日下午，清华大学经济管理学院首任院长、学院顾问委员会名誉主席朱镕基及夫人劳安，国务院副总理刘延东，国务院副总理马凯，全国政协副主席陈元，全国政协副主席、中国人民银行行长周小川在钓鱼台国宾馆会见了参加清华大学经济管理学院顾问委员会2017年会议的顾问委员。</p><p><img alt=\"1030\" src=\"https://p1.pstatp.com/large/4359000392cc39bca7f7\"></p><p>朱镕基与清华经管学院顾问委员会委员、苹果公司首席执行官蒂姆•库克（Tim Cook）亲切握手交谈</p><p>参加会见的有24位顾问委员会海外委员，比如大家熟悉的——</p><p>布雷耶资本创始人兼首席执行官吉姆·布雷耶</p><p>保尔森基金会主席、美国财政部原部长、高盛集团原董事长兼首席执行官亨利·保尔森</p><p>巴理克黄金公司董事长、美国华盛顿布鲁金斯学会董事会主席约翰·桑顿</p><p>美国史带金融财团董事长莫里斯•格林伯格；</p><p>苹果公司首席执行官蒂姆•库克</p><p>黑石集团董事长、首席执行官兼共同创办人苏世民</p><p>新任委员宝马集团董事长科鲁格</p><p>参加会见的中方委员有郭树清、刘士余、楼继伟、邱勇、王大中、顾秉林、赵纯均、常振明、李彦宏、马化腾、马云等。</p>', '01', null, null, null, '1', '2017-11-02 18:27:04');
INSERT INTO t_com_notice VALUES ('6', '蚂蚁花呗不还钱会有什么后果', '01', null, null, '2017-10-29 00:00:00', '2017-12-01 00:00:00', '<p>自从用了蚂蚁花呗之后，每个月到了9号都有一大批小伙伴哭喊：“剁手”太厉害了，还不起钱了!</p><p>那么，蚂蚁花呗能不还吗?蚂蚁花呗还钱会有什么后果呢?今天，qaz就给大家盘点一下。排名分先后，越往后越严重!</p><p><img alt=\"\" src=\"https://p3.pstatp.com/large/435b0001ebc0b95d515d\" title=\"Image: https://p3.pstatp.com/large/435b0001ebc0b95d515d\"></p><p>一、遭受电话“骚扰”</p><p>蚂蚁花呗欠款不还，像在网贷平台贷款不还一样，三天两头会有催收人员打电话“提醒”你赶快还钱。如果是蚂蚁花呗客服打电话还稍微好点，至少态度不会太差，但如果碰到外包的催收人员，破口大骂、威胁恐吓都再正常不过了。</p><p>这还不算什么，如果被催收之后仍然不还钱的话，支付宝、通讯录好友都可能被“骚扰”。身边的人都会知道你欠钱不还，然后被花呗“逼着”催你还钱。</p><p>下面是一网友的真实经历：</p><p>我一关系挺好的同学，也是支付宝好友，因为花呗欠的钱太多一时还不上，就干脆不还了。结果支付宝那边天天打我电话，让催那哥们还钱，天天打，拉黑都不行，拉黑了又换一个打，搞得我都快崩溃了。陆续也有几个同学也是这样，最后我们几个都受不了啦，联合批斗那哥们，让他把钱还了。</p>', '01', null, null, null, '1', '2017-11-03 23:39:33');
INSERT INTO t_com_notice VALUES ('7', '全文检索测试', '01', '上级单位', '魏总', '2017-11-15 00:00:00', '2017-11-30 00:00:00', '<div>发展历史</div><div>全文检索是20世纪末产生的一种新的信息检索技术。经过几十年的发展，特别是以计算机技术为代表的新一代信息技术应用，使全文检索从最初的字符串匹配和简单的布尔逻辑检索技术演进到能对超大文本、语音、图像、活动影像等非结构化数据进行综合管理的复合技术。由于内涵和外延的深刻变化，全文检索系统已成为新一代管理系统的代名词，衡量全文检索系统的基本指标和全文检索的内涵也发生巨大变化。</div><div>全文检索系统及功能</div><div>全文数据库是全文检索系统的主要构成部分。所谓全文数据库是将一个完整的信息源的全部内容转化为计算机可以识别、处理的信息单元而形成的数据集合。全文数据库不仅存储了信息，而且还有对全文数据进行词、字、段落等更深层次的编辑、加工的功能，而且所有全文数据库无一不是海量信息数据库。对于全文数据库这种比较非结构化的数据，用RDBMS（关系数据库管理系统）技术来管理是目前最好的一种方式。但是由于RDBMS底层结构的缘故使得它管理大量非结构化数据显得有些先天不足，特别是查询这些海量非结构化数据的速度较慢，而通过全文检索技术就能高效地管理这些非结构化数据。</div><div>关于全文数据库的特点，空军政治学院计算机中心王兰成副教授认为全文数据库与书目数据库、事实数据库相比较主要有如下特点：（1）全文数据库包含信息的原始性 库中信息基本上是未经信息加工的原始文本，具有客观性。（2）信息检索的彻底性 可对文中任何字、词、句进行检索，还可表示检索之间的复杂位置关系（3）所用检索语言的自然性 不做人工标引，借助截词、邻接等匹配方法，以自然语言检索所需文献。这是与传统主题词检索方法的根本区别。（4）数据相对的稳定性 全文数据库基本上是封闭的，一般不需更新。（5）检索结果的准确性（6）数据结构的非结构性</div><div>全文检索的实现技术编辑</div><div>全文检索系统的实现技术分为三个方面：关系型全文检索系统、层次型全文检索系统、面向对象的全文检索系统及自动标引技术。</div><div>针对全文数据系统的构建，提出全文检索系统的实现技术，主要分为5个步骤。</div><div>（1）数据准备：它是指针对计划加载到全文数据库中的数据进行收集、整理、归类等预先处理的过程。加载到全文数据中的数据可以从多种途径获得，常见的数据来源有：电脑打字产生的文件，电子印刷产生的文稿，计算机网上传送的文件，电子出版物，图文处理产生的文件，专门组织人力录入建库。</div><div>（2）文本预处理：包括规范格式，当格式多种多样时，应加以整理，使文献的格式规范化；批式标引，文本预处理阶段完成的批式标引，不受全文数据库结构的限制，效率较高。</div><div>（3）数据加载：数据准备好以后，便可以加载（拷入、输入）到数据库文件中去了。加载数据可有单篇方式或批量方式。单篇方式一次加载一篇，适于平时文献随时加载的情况。批量方式一次加载多篇，适于集中大量加载的情况。</div><div>（4）数据检索：数据库建立起来之后，便可根据全文检索系统提供的检索功能对数据库进行检索。</div><div>（5）数据维护：全文数据建立以后，需要经常对数据库的内容进行索引、更新、追加和清理。[1]&nbsp;</div>', '01', null, null, null, '1', '2017-11-09 12:35:23');

-- ----------------------------
-- Table structure for `t_log_access`
-- ----------------------------
DROP TABLE IF EXISTS `t_log_access`;
CREATE TABLE `t_log_access` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `USER_ID` int(11) DEFAULT NULL COMMENT '用户主键',
  `USER_CODE` varchar(64) DEFAULT NULL COMMENT '用户代码',
  `USER_NAME` varchar(64) DEFAULT NULL COMMENT '用户名称',
  `CLIENT_IP` varchar(32) DEFAULT NULL COMMENT '客户端IP',
  `SERVER_IP` varchar(32) DEFAULT NULL COMMENT '服务器IP',
  `TARGET` varchar(256) DEFAULT NULL COMMENT '访问功能',
  `URI` varchar(256) NOT NULL COMMENT '目标URI',
  `ACCESS_TIME` datetime DEFAULT NULL COMMENT '访问时间',
  `QUERY_STRING` text COMMENT '查询参数',
  `PARAMS_MAP` text COMMENT '提交参数JSON',
  `SESSION_ID` varchar(64) DEFAULT NULL COMMENT '会话ID',
  `COOKIE_ID` varchar(64) DEFAULT NULL COMMENT 'COOKIE_ID',
  `UA` text COMMENT 'UA',
  `REFERER` varchar(256) DEFAULT NULL COMMENT '页面跳转来源',
  `CHANNEL` varchar(32) DEFAULT NULL COMMENT '访问渠道',
  `PLATFORM` varchar(32) DEFAULT NULL COMMENT '访问平台',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='访问日志';

-- ----------------------------
-- Records of t_log_access
-- ----------------------------

-- ----------------------------
-- Table structure for `t_log_login`
-- ----------------------------
DROP TABLE IF EXISTS `t_log_login`;
CREATE TABLE `t_log_login` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `USER_ID` int(11) NOT NULL COMMENT '用户主键',
  `USER_CODE` varchar(64) NOT NULL COMMENT '用户代码',
  `USER_NAME` varchar(64) NOT NULL COMMENT '用户名称',
  `CLIENT_IP` varchar(32) DEFAULT NULL COMMENT '客户端IP',
  `SERVER_IP` varchar(32) DEFAULT NULL COMMENT '服务器主机名或者IP',
  `LOGIN_TIME` datetime NOT NULL COMMENT '登录时间',
  `LOGOUT_TIME` datetime DEFAULT NULL COMMENT '退出时间',
  `SESSION_ID` varchar(64) DEFAULT NULL COMMENT '会话ID',
  `COOKIE_ID` varchar(64) DEFAULT NULL COMMENT 'COOKIE_ID',
  `CHANNEL` varchar(32) DEFAULT NULL COMMENT '访问渠道',
  `PLATFORM` varchar(32) DEFAULT NULL COMMENT '访问平台',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=619 DEFAULT CHARSET=utf8 COMMENT='登录日志';

-- ----------------------------
-- Records of t_log_login
-- ----------------------------
INSERT INTO t_log_login VALUES ('601', '1', 'sa', 'Super', '127.0.0.1', '192.168.1.14', '2017-11-13 12:24:36', '2017-11-13 12:24:37', '992E104F2F505E9CCC5C0EE9F8D4E2BF', 'JSESSIONID:992E104F2F505E9CCC5C0EE9F8D4E2BF', null, null);
INSERT INTO t_log_login VALUES ('602', '1', 'sa', 'Super', '127.0.0.1', '192.168.1.14', '2017-11-13 12:35:15', '2017-11-13 13:13:12', '5D964ECACC9F6E74032B70A1806539A7', 'JSESSIONID:992E104F2F505E9CCC5C0EE9F8D4E2BF', null, null);
INSERT INTO t_log_login VALUES ('603', '1', 'sa', 'Super', '127.0.0.1', '192.168.1.14', '2017-11-13 12:41:13', '2017-11-13 13:13:12', '5D964ECACC9F6E74032B70A1806539A7', 'JSESSIONID:5D964ECACC9F6E74032B70A1806539A7', null, null);
INSERT INTO t_log_login VALUES ('604', '1', 'sa', 'Super', '127.0.0.1', '192.168.1.102', '2017-11-23 20:53:59', '2017-11-27 22:10:31', '2DC069D2230D47B478DF45D09C806F1B', 'JSESSIONID:2DC069D2230D47B478DF45D09C806F1B', null, null);
INSERT INTO t_log_login VALUES ('607', '1', 'sa', 'Super', '127.0.0.1', '192.168.1.100', '2017-11-27 21:53:00', '2017-11-27 22:07:14', '11C9FC43CADD0824CCD012554696E3EE', 'JSESSIONID:11C9FC43CADD0824CCD012554696E3EE', null, null);
INSERT INTO t_log_login VALUES ('608', '1', 'sa', 'Super', '127.0.0.1', '192.168.1.100', '2017-11-27 22:07:28', '2017-11-27 22:10:31', '4656499C0CE540C22346E30C9288519B', 'JSESSIONID:4656499C0CE540C22346E30C9288519B', null, null);
INSERT INTO t_log_login VALUES ('609', '1', 'sa', 'Super', '127.0.0.1', '192.168.1.100', '2017-11-27 22:10:58', '2017-11-27 22:13:48', 'FA2460FF90EB2E16D833984F62C63FFC', 'JSESSIONID:4656499C0CE540C22346E30C9288519B', null, null);
INSERT INTO t_log_login VALUES ('610', '1', 'sa', 'Super', '127.0.0.1', '192.168.1.100', '2017-11-27 22:14:55', '2017-11-27 22:18:51', '637747FAECB3D3E8AD9EC2F64EB871BD', 'JSESSIONID:637747FAECB3D3E8AD9EC2F64EB871BD', null, null);
INSERT INTO t_log_login VALUES ('611', '2', 'admin', '管理员', '127.0.0.1', '192.168.1.100', '2017-11-27 22:15:34', '2017-11-27 22:15:44', 'D369E1E1BA6661FC0A2F20F9F48E91E9', 'JSESSIONID:D369E1E1BA6661FC0A2F20F9F48E91E9', null, null);
INSERT INTO t_log_login VALUES ('612', '2', 'admin', '管理员', '127.0.0.1', '192.168.1.100', '2017-11-27 22:16:05', '2017-11-27 22:16:15', 'ED37E800195CF6976585DF1CF4A87392', 'JSESSIONID:ED37E800195CF6976585DF1CF4A87392', null, null);
INSERT INTO t_log_login VALUES ('613', '2', 'admin', '管理员', '127.0.0.1', '192.168.1.100', '2017-11-27 22:17:33', '2017-11-27 22:20:26', '139C900C7941100E1AC2E644DD3051C4', 'JSESSIONID:ED37E800195CF6976585DF1CF4A87392', null, null);
INSERT INTO t_log_login VALUES ('614', '2', 'admin', '管理员', '127.0.0.1', '192.168.1.100', '2017-11-27 22:17:35', '2017-11-27 22:20:26', '139C900C7941100E1AC2E644DD3051C4', 'JSESSIONID:139C900C7941100E1AC2E644DD3051C4', null, null);
INSERT INTO t_log_login VALUES ('615', '2', 'admin', '管理员', '127.0.0.1', '192.168.1.100', '2017-11-27 22:18:04', '2017-11-27 22:20:26', '139C900C7941100E1AC2E644DD3051C4', 'JSESSIONID:139C900C7941100E1AC2E644DD3051C4', null, null);
INSERT INTO t_log_login VALUES ('616', '1', 'sa', 'Super', '127.0.0.1', '192.168.1.100', '2017-11-27 22:18:51', null, '637747FAECB3D3E8AD9EC2F64EB871BD', 'JSESSIONID:637747FAECB3D3E8AD9EC2F64EB871BD', null, null);
INSERT INTO t_log_login VALUES ('617', '2', 'admin', '管理员', '127.0.0.1', '192.168.1.100', '2017-11-27 22:20:31', '2017-11-27 22:20:43', '324686377573D5A3293ED7351E7FA5D2', 'JSESSIONID:139C900C7941100E1AC2E644DD3051C4', null, null);
INSERT INTO t_log_login VALUES ('618', '2', 'admin', '管理员', '127.0.0.1', '192.168.1.100', '2017-11-27 22:22:57', '2017-11-27 22:23:03', '48E511441DBEE7D1C4B8C08CF8B1B12F', 'JSESSIONID:48E511441DBEE7D1C4B8C08CF8B1B12F', null, null);

-- ----------------------------
-- Table structure for `t_log_reading`
-- ----------------------------
DROP TABLE IF EXISTS `t_log_reading`;
CREATE TABLE `t_log_reading` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `USER_ID` int(11) NOT NULL COMMENT '用户主键',
  `TYPE` varchar(64) NOT NULL COMMENT '业务类型',
  `DATA_ID` int(11) NOT NULL COMMENT '业务主键',
  `READ_TIME` datetime NOT NULL COMMENT '阅读时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='阅读记录';

-- ----------------------------
-- Records of t_log_reading
-- ----------------------------
INSERT INTO t_log_reading VALUES ('1', '1', 't_com_notice', '2', '2017-11-13 11:16:42');
INSERT INTO t_log_reading VALUES ('2', '1', 't_com_notice', '7', '2017-11-13 12:41:40');
INSERT INTO t_log_reading VALUES ('3', '1', 't_com_notice', '7', '2017-11-13 12:42:14');
INSERT INTO t_log_reading VALUES ('4', '1', 't_com_notice', '2', '2017-11-23 20:54:04');

-- ----------------------------
-- Table structure for `t_log_schedule`
-- ----------------------------
DROP TABLE IF EXISTS `t_log_schedule`;
CREATE TABLE `t_log_schedule` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `SCHEDULE_ID` int(11) NOT NULL COMMENT '任务主键',
  `STATE` varchar(32) NOT NULL COMMENT '执行结果',
  `SERVER_IP` varchar(32) NOT NULL COMMENT '执行服务器主机名或者IP',
  `STIME` datetime NOT NULL COMMENT '开始时间',
  `ETIME` datetime NOT NULL COMMENT '结束时间',
  `SPENT` bigint(11) NOT NULL COMMENT '耗时（毫秒）',
  `REMARK` varchar(1024) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=485 DEFAULT CHARSET=utf8 COMMENT='任务执行日志';

-- ----------------------------
-- Records of t_log_schedule
-- ----------------------------
INSERT INTO t_log_schedule VALUES ('339', '4', '01', '192.168.1.14', '2017-11-08 14:39:38', '2017-11-08 14:39:39', '1633', null);
INSERT INTO t_log_schedule VALUES ('340', '4', '01', '192.168.1.14', '2017-11-08 14:43:42', '2017-11-08 14:43:43', '567', null);
INSERT INTO t_log_schedule VALUES ('341', '3', '01', '192.168.1.14', '2017-11-08 15:00:00', '2017-11-08 15:00:00', '39', null);
INSERT INTO t_log_schedule VALUES ('342', '1', '01', '192.168.1.14', '2017-11-08 15:00:00', '2017-11-08 15:00:00', '22', null);
INSERT INTO t_log_schedule VALUES ('343', '2', '01', '192.168.1.14', '2017-11-08 15:00:00', '2017-11-08 15:00:00', '0', null);
INSERT INTO t_log_schedule VALUES ('344', '4', '01', '192.168.1.14', '2017-11-08 15:00:00', '2017-11-08 15:00:00', '398', null);
INSERT INTO t_log_schedule VALUES ('345', '4', '01', '192.168.1.14', '2017-11-08 15:36:11', '2017-11-08 15:36:52', '40769', null);
INSERT INTO t_log_schedule VALUES ('346', '4', '01', '192.168.1.14', '2017-11-08 15:38:14', '2017-11-08 15:38:15', '639', null);
INSERT INTO t_log_schedule VALUES ('347', '4', '01', '192.168.1.14', '2017-11-08 15:39:36', '2017-11-08 15:43:50', '254215', null);
INSERT INTO t_log_schedule VALUES ('348', '4', '01', '192.168.1.14', '2017-11-08 15:56:11', '2017-11-08 15:56:24', '13290', null);
INSERT INTO t_log_schedule VALUES ('349', '4', '01', '192.168.1.14', '2017-11-08 15:59:38', '2017-11-08 15:59:41', '3060', null);
INSERT INTO t_log_schedule VALUES ('350', '1', '01', '192.168.1.14', '2017-11-08 16:00:00', '2017-11-08 16:00:00', '11', null);
INSERT INTO t_log_schedule VALUES ('351', '3', '01', '192.168.1.14', '2017-11-08 16:00:00', '2017-11-08 16:00:00', '46', null);
INSERT INTO t_log_schedule VALUES ('352', '2', '01', '192.168.1.14', '2017-11-08 16:00:00', '2017-11-08 16:00:00', '15', null);
INSERT INTO t_log_schedule VALUES ('353', '4', '01', '192.168.1.14', '2017-11-08 16:00:00', '2017-11-08 16:04:08', '247585', null);
INSERT INTO t_log_schedule VALUES ('354', '4', '01', '192.168.1.14', '2017-11-08 18:16:23', '2017-11-08 18:16:23', '791', null);
INSERT INTO t_log_schedule VALUES ('355', '4', '01', '192.168.1.14', '2017-11-08 18:17:12', '2017-11-08 18:17:12', '75', null);
INSERT INTO t_log_schedule VALUES ('356', '4', '01', '192.168.1.14', '2017-11-08 18:17:46', '2017-11-08 18:18:42', '56655', null);
INSERT INTO t_log_schedule VALUES ('357', '4', '01', '192.168.1.14', '2017-11-08 18:19:41', '2017-11-08 18:19:54', '12590', null);
INSERT INTO t_log_schedule VALUES ('358', '4', '01', '192.168.1.14', '2017-11-08 18:25:25', '2017-11-08 18:25:25', '100', null);
INSERT INTO t_log_schedule VALUES ('359', '4', '01', '192.168.1.14', '2017-11-08 18:26:07', '2017-11-08 18:26:07', '67', null);
INSERT INTO t_log_schedule VALUES ('360', '4', '02', '192.168.1.14', '2017-11-08 18:27:27', '2017-11-08 18:27:27', '100', 'add index error');
INSERT INTO t_log_schedule VALUES ('361', '4', '01', '192.168.1.14', '2017-11-08 18:28:28', '2017-11-08 18:28:28', '566', null);
INSERT INTO t_log_schedule VALUES ('362', '4', '02', '192.168.1.14', '2017-11-08 18:28:42', '2017-11-08 18:28:42', '93', 'add index error');
INSERT INTO t_log_schedule VALUES ('363', '4', '01', '192.168.1.14', '2017-11-08 18:32:47', '2017-11-08 18:32:48', '563', null);
INSERT INTO t_log_schedule VALUES ('364', '4', '01', '192.168.1.14', '2017-11-08 18:33:09', '2017-11-08 18:33:09', '184', null);
INSERT INTO t_log_schedule VALUES ('365', '4', '01', '192.168.1.14', '2017-11-08 18:33:56', '2017-11-08 18:34:01', '5105', null);
INSERT INTO t_log_schedule VALUES ('366', '4', '01', '192.168.1.14', '2017-11-08 18:48:20', '2017-11-08 18:48:26', '6767', null);
INSERT INTO t_log_schedule VALUES ('367', '4', '01', '192.168.1.14', '2017-11-08 18:54:05', '2017-11-08 18:54:12', '6714', null);
INSERT INTO t_log_schedule VALUES ('368', '4', '01', '192.168.1.14', '2017-11-08 19:03:18', '2017-11-08 19:03:18', '7', null);
INSERT INTO t_log_schedule VALUES ('369', '4', '01', '192.168.1.14', '2017-11-08 19:08:31', '2017-11-08 19:08:37', '6342', null);
INSERT INTO t_log_schedule VALUES ('370', '4', '01', '192.168.1.14', '2017-11-08 19:10:51', '2017-11-08 19:10:58', '6716', null);
INSERT INTO t_log_schedule VALUES ('371', '4', '01', '192.168.1.14', '2017-11-08 19:18:45', '2017-11-08 19:19:24', '38237', null);
INSERT INTO t_log_schedule VALUES ('372', '4', '01', '192.168.1.14', '2017-11-08 19:23:04', '2017-11-08 19:23:46', '41751', null);
INSERT INTO t_log_schedule VALUES ('373', '4', '01', '192.168.1.14', '2017-11-08 19:24:04', '2017-11-08 19:24:59', '55494', null);
INSERT INTO t_log_schedule VALUES ('374', '4', '01', '192.168.1.14', '2017-11-08 19:30:23', '2017-11-08 19:30:30', '6066', null);
INSERT INTO t_log_schedule VALUES ('375', '4', '01', '192.168.1.14', '2017-11-08 19:31:12', '2017-11-08 19:31:23', '10647', null);
INSERT INTO t_log_schedule VALUES ('376', '4', '01', '192.168.1.14', '2017-11-08 19:34:18', '2017-11-08 19:34:24', '5971', null);
INSERT INTO t_log_schedule VALUES ('377', '4', '01', '192.168.1.14', '2017-11-08 19:36:38', '2017-11-08 19:36:39', '883', null);
INSERT INTO t_log_schedule VALUES ('378', '4', '01', '192.168.1.14', '2017-11-08 19:36:53', '2017-11-08 19:36:53', '189', null);
INSERT INTO t_log_schedule VALUES ('379', '4', '01', '192.168.1.14', '2017-11-08 19:37:24', '2017-11-08 19:37:30', '5482', null);
INSERT INTO t_log_schedule VALUES ('380', '4', '01', '192.168.1.14', '2017-11-08 19:38:58', '2017-11-08 19:39:05', '6714', null);
INSERT INTO t_log_schedule VALUES ('381', '4', '01', '192.168.1.102', '2017-11-08 21:55:29', '2017-11-08 21:55:29', '165', null);
INSERT INTO t_log_schedule VALUES ('382', '4', '01', '192.168.1.102', '2017-11-08 21:56:16', '2017-11-08 21:56:16', '10', null);
INSERT INTO t_log_schedule VALUES ('383', '4', '01', '192.168.1.102', '2017-11-08 21:57:01', '2017-11-08 21:57:19', '18580', null);
INSERT INTO t_log_schedule VALUES ('384', '4', '01', '192.168.1.102', '2017-11-08 21:58:32', '2017-11-08 21:58:50', '17943', null);
INSERT INTO t_log_schedule VALUES ('385', '4', '01', '192.168.1.102', '2017-11-08 22:00:00', '2017-11-08 22:00:17', '17268', null);
INSERT INTO t_log_schedule VALUES ('386', '4', '01', '192.168.1.102', '2017-11-08 22:05:52', '2017-11-08 22:05:52', '62', null);
INSERT INTO t_log_schedule VALUES ('387', '4', '01', '192.168.1.102', '2017-11-08 22:08:29', '2017-11-08 22:08:47', '18587', null);
INSERT INTO t_log_schedule VALUES ('388', '4', '01', '192.168.1.102', '2017-11-08 22:11:53', '2017-11-08 22:11:58', '4708', null);
INSERT INTO t_log_schedule VALUES ('389', '4', '01', '192.168.1.102', '2017-11-08 22:13:37', '2017-11-08 22:13:44', '7570', null);
INSERT INTO t_log_schedule VALUES ('390', '4', '02', '192.168.1.102', '2017-11-08 22:13:56', '2017-11-08 22:14:00', '3623', 'add index error');
INSERT INTO t_log_schedule VALUES ('391', '4', '02', '192.168.1.102', '2017-11-08 22:14:11', '2017-11-08 22:14:11', '12', 'add index error');
INSERT INTO t_log_schedule VALUES ('392', '4', '01', '192.168.1.102', '2017-11-08 22:30:23', '2017-11-08 22:30:23', '250', null);
INSERT INTO t_log_schedule VALUES ('393', '4', '01', '192.168.1.102', '2017-11-08 22:30:53', '2017-11-08 22:30:53', '24', null);
INSERT INTO t_log_schedule VALUES ('394', '4', '01', '192.168.1.102', '2017-11-08 22:31:26', '2017-11-08 22:31:26', '16', null);
INSERT INTO t_log_schedule VALUES ('395', '4', '01', '192.168.1.102', '2017-11-08 22:31:45', '2017-11-08 22:32:12', '26956', null);
INSERT INTO t_log_schedule VALUES ('396', '4', '01', '192.168.1.102', '2017-11-08 22:33:04', '2017-11-08 22:33:26', '21963', null);
INSERT INTO t_log_schedule VALUES ('397', '4', '01', '192.168.1.102', '2017-11-08 22:33:54', '2017-11-08 22:35:09', '74315', null);
INSERT INTO t_log_schedule VALUES ('398', '4', '01', '192.168.1.102', '2017-11-08 22:42:17', '2017-11-08 22:42:26', '8834', null);
INSERT INTO t_log_schedule VALUES ('399', '4', '01', '192.168.1.102', '2017-11-08 22:47:19', '2017-11-08 22:49:14', '114797', null);
INSERT INTO t_log_schedule VALUES ('400', '4', '01', '192.168.1.102', '2017-11-08 23:00:00', '2017-11-08 23:00:04', '4098', null);
INSERT INTO t_log_schedule VALUES ('401', '4', '02', '192.168.1.102', '2017-11-08 23:07:59', '2017-11-08 23:07:59', '8', 'For input string: \"\"');
INSERT INTO t_log_schedule VALUES ('402', '4', '01', '192.168.1.102', '2017-11-08 23:09:31', '2017-11-08 23:09:31', '207', null);
INSERT INTO t_log_schedule VALUES ('403', '4', '01', '192.168.1.102', '2017-11-08 23:10:47', '2017-11-08 23:10:54', '6777', null);
INSERT INTO t_log_schedule VALUES ('404', '4', '01', '192.168.1.102', '2017-11-08 23:24:56', '2017-11-08 23:25:04', '7879', null);
INSERT INTO t_log_schedule VALUES ('405', '4', '01', '192.168.1.102', '2017-11-08 23:30:33', '2017-11-08 23:30:36', '3293', null);
INSERT INTO t_log_schedule VALUES ('406', '4', '01', '192.168.1.102', '2017-11-08 23:31:22', '2017-11-08 23:31:37', '14530', null);
INSERT INTO t_log_schedule VALUES ('407', '4', '01', '192.168.1.102', '2017-11-08 23:32:02', '2017-11-08 23:35:10', '188327', null);
INSERT INTO t_log_schedule VALUES ('408', '4', '02', '192.168.1.102', '2017-11-08 23:35:03', '2017-11-08 23:35:14', '10897', 'add index error');
INSERT INTO t_log_schedule VALUES ('409', '4', '01', '192.168.1.14', '2017-11-09 11:58:56', '2017-11-09 11:59:02', '6182', null);
INSERT INTO t_log_schedule VALUES ('410', '2', '01', '192.168.1.14', '2017-11-09 12:00:00', '2017-11-09 12:00:00', '9', null);
INSERT INTO t_log_schedule VALUES ('411', '1', '01', '192.168.1.14', '2017-11-09 12:00:00', '2017-11-09 12:00:00', '23', null);
INSERT INTO t_log_schedule VALUES ('412', '3', '01', '192.168.1.14', '2017-11-09 12:00:00', '2017-11-09 12:00:00', '24', null);
INSERT INTO t_log_schedule VALUES ('413', '4', '01', '192.168.1.14', '2017-11-09 12:00:00', '2017-11-09 12:00:03', '3066', null);
INSERT INTO t_log_schedule VALUES ('414', '4', '01', '192.168.1.14', '2017-11-09 12:04:09', '2017-11-09 12:04:12', '2964', null);
INSERT INTO t_log_schedule VALUES ('415', '4', '01', '192.168.1.14', '2017-11-09 12:07:24', '2017-11-09 12:07:26', '2620', null);
INSERT INTO t_log_schedule VALUES ('416', '4', '01', '192.168.1.14', '2017-11-09 12:24:17', '2017-11-09 12:24:24', '6674', null);
INSERT INTO t_log_schedule VALUES ('417', '4', '01', '192.168.1.14', '2017-11-09 12:28:35', '2017-11-09 12:28:38', '3093', null);
INSERT INTO t_log_schedule VALUES ('418', '4', '01', '192.168.1.14', '2017-11-09 12:29:24', '2017-11-09 12:29:27', '2706', null);
INSERT INTO t_log_schedule VALUES ('419', '4', '01', '192.168.1.14', '2017-11-09 12:33:34', '2017-11-09 12:33:40', '6232', null);
INSERT INTO t_log_schedule VALUES ('420', '4', '01', '192.168.1.14', '2017-11-09 12:34:04', '2017-11-09 12:34:04', '134', null);
INSERT INTO t_log_schedule VALUES ('421', '4', '01', '192.168.1.14', '2017-11-09 12:36:05', '2017-11-09 12:37:32', '86727', null);
INSERT INTO t_log_schedule VALUES ('422', '2', '01', '192.168.1.14', '2017-11-09 13:00:00', '2017-11-09 13:00:00', '12', null);
INSERT INTO t_log_schedule VALUES ('423', '3', '01', '192.168.1.14', '2017-11-09 13:00:00', '2017-11-09 13:00:00', '26', null);
INSERT INTO t_log_schedule VALUES ('424', '1', '01', '192.168.1.14', '2017-11-09 13:00:00', '2017-11-09 13:00:00', '13', null);
INSERT INTO t_log_schedule VALUES ('425', '4', '01', '192.168.1.14', '2017-11-09 13:00:00', '2017-11-09 13:00:00', '86', null);
INSERT INTO t_log_schedule VALUES ('426', '4', '01', '192.168.1.14', '2017-11-09 13:00:41', '2017-11-09 13:00:56', '15532', null);
INSERT INTO t_log_schedule VALUES ('427', '2', '01', '192.168.1.14', '2017-11-09 16:00:00', '2017-11-09 16:00:00', '18', null);
INSERT INTO t_log_schedule VALUES ('428', '1', '01', '192.168.1.14', '2017-11-09 16:00:00', '2017-11-09 16:00:00', '22', null);
INSERT INTO t_log_schedule VALUES ('429', '3', '01', '192.168.1.14', '2017-11-09 16:00:00', '2017-11-09 16:00:00', '31', null);
INSERT INTO t_log_schedule VALUES ('430', '4', '01', '192.168.1.14', '2017-11-09 16:00:00', '2017-11-09 16:00:00', '200', null);
INSERT INTO t_log_schedule VALUES ('431', '1', '01', '192.168.1.14', '2017-11-09 17:00:00', '2017-11-09 17:00:00', '9', null);
INSERT INTO t_log_schedule VALUES ('432', '3', '01', '192.168.1.14', '2017-11-09 17:00:00', '2017-11-09 17:00:00', '22', null);
INSERT INTO t_log_schedule VALUES ('433', '4', '01', '192.168.1.14', '2017-11-09 17:00:00', '2017-11-09 17:00:00', '30', null);
INSERT INTO t_log_schedule VALUES ('434', '2', '01', '192.168.1.14', '2017-11-09 17:00:00', '2017-11-09 17:00:00', '30', null);
INSERT INTO t_log_schedule VALUES ('435', '2', '01', '192.168.1.14', '2017-11-09 18:00:00', '2017-11-09 18:00:00', '26', null);
INSERT INTO t_log_schedule VALUES ('436', '1', '01', '192.168.1.14', '2017-11-09 18:00:00', '2017-11-09 18:00:00', '11', null);
INSERT INTO t_log_schedule VALUES ('437', '3', '01', '192.168.1.14', '2017-11-09 18:00:00', '2017-11-09 18:00:00', '46', null);
INSERT INTO t_log_schedule VALUES ('438', '4', '01', '192.168.1.14', '2017-11-09 18:00:00', '2017-11-09 18:00:00', '205', null);
INSERT INTO t_log_schedule VALUES ('439', '1', '01', '192.168.1.14', '2017-11-09 19:00:00', '2017-11-09 19:00:00', '28', null);
INSERT INTO t_log_schedule VALUES ('440', '2', '02', '192.168.1.14', '2017-11-09 19:00:00', '2017-11-09 19:00:00', '24', 'DirectoryNotEmptyException: D:\\temp\\auto\\temp\\output\\1510224856385\\src');
INSERT INTO t_log_schedule VALUES ('441', '3', '01', '192.168.1.14', '2017-11-09 19:00:00', '2017-11-09 19:00:00', '64', null);
INSERT INTO t_log_schedule VALUES ('442', '4', '01', '192.168.1.14', '2017-11-09 19:00:00', '2017-11-09 19:00:00', '248', null);
INSERT INTO t_log_schedule VALUES ('443', '1', '01', '192.168.1.14', '2017-11-10 10:00:00', '2017-11-10 10:00:00', '11', null);
INSERT INTO t_log_schedule VALUES ('444', '3', '01', '192.168.1.14', '2017-11-10 10:00:00', '2017-11-10 10:00:00', '26', null);
INSERT INTO t_log_schedule VALUES ('445', '2', '01', '192.168.1.14', '2017-11-10 10:00:00', '2017-11-10 10:00:00', '10', null);
INSERT INTO t_log_schedule VALUES ('446', '4', '01', '192.168.1.14', '2017-11-10 10:00:00', '2017-11-10 10:00:00', '232', null);
INSERT INTO t_log_schedule VALUES ('447', '2', '02', '192.168.1.14', '2017-11-10 12:00:00', '2017-11-10 12:00:00', '26', 'DirectoryNotEmptyException: D:\\temp\\auto\\temp\\output');
INSERT INTO t_log_schedule VALUES ('448', '3', '01', '192.168.1.14', '2017-11-10 12:00:00', '2017-11-10 12:00:00', '26', null);
INSERT INTO t_log_schedule VALUES ('449', '1', '01', '192.168.1.14', '2017-11-10 12:00:00', '2017-11-10 12:00:00', '155', null);
INSERT INTO t_log_schedule VALUES ('450', '4', '01', '192.168.1.14', '2017-11-10 12:00:00', '2017-11-10 12:00:00', '174', null);
INSERT INTO t_log_schedule VALUES ('451', '1', '01', '192.168.1.14', '2017-11-10 13:00:00', '2017-11-10 13:00:00', '15', null);
INSERT INTO t_log_schedule VALUES ('452', '2', '02', '192.168.1.14', '2017-11-10 13:00:00', '2017-11-10 13:00:00', '18', 'DirectoryNotEmptyException: D:\\temp\\auto\\temp');
INSERT INTO t_log_schedule VALUES ('453', '3', '01', '192.168.1.14', '2017-11-10 13:00:00', '2017-11-10 13:00:00', '33', null);
INSERT INTO t_log_schedule VALUES ('454', '4', '01', '192.168.1.14', '2017-11-10 13:00:00', '2017-11-10 13:00:00', '197', null);
INSERT INTO t_log_schedule VALUES ('458', '2', '01', '192.168.1.14', '2017-11-10 14:00:00', '2017-11-10 14:00:00', '2', null);
INSERT INTO t_log_schedule VALUES ('462', '4', '01', '192.168.1.14', '2017-11-10 15:00:00', '2017-11-10 15:00:00', '28', null);
INSERT INTO t_log_schedule VALUES ('463', '3', '01', '192.168.1.14', '2017-11-10 16:00:00', '2017-11-10 16:00:00', '17', null);
INSERT INTO t_log_schedule VALUES ('465', '2', '02', '192.168.1.14', '2017-11-10 16:00:00', '2017-11-10 16:00:00', '33', 'DirectoryNotEmptyException: D:\\temp\\auto\\temp\\output');
INSERT INTO t_log_schedule VALUES ('466', '4', '01', '192.168.1.14', '2017-11-10 16:00:00', '2017-11-10 16:00:00', '28', null);
INSERT INTO t_log_schedule VALUES ('467', '2', '01', '192.168.1.105', '2017-11-10 21:00:00', '2017-11-10 21:00:00', '1', null);
INSERT INTO t_log_schedule VALUES ('468', '1', '01', '192.168.1.105', '2017-11-10 21:00:00', '2017-11-10 21:00:00', '11', null);
INSERT INTO t_log_schedule VALUES ('469', '3', '01', '192.168.1.105', '2017-11-10 21:00:00', '2017-11-10 21:00:00', '28', null);
INSERT INTO t_log_schedule VALUES ('470', '4', '01', '192.168.1.105', '2017-11-10 21:00:00', '2017-11-10 21:00:00', '25', null);
INSERT INTO t_log_schedule VALUES ('471', '4', '01', '192.168.1.105', '2017-11-12 22:18:30', '2017-11-12 22:18:30', '299', null);
INSERT INTO t_log_schedule VALUES ('472', '4', '01', '192.168.1.105', '2017-11-12 23:00:00', '2017-11-12 23:00:00', '27', null);
INSERT INTO t_log_schedule VALUES ('473', '1', '01', '192.168.1.14', '2017-11-13 10:00:00', '2017-11-13 10:00:00', '31', null);
INSERT INTO t_log_schedule VALUES ('474', '3', '01', '192.168.1.14', '2017-11-13 10:00:00', '2017-11-13 10:00:00', '21', null);
INSERT INTO t_log_schedule VALUES ('475', '2', '01', '192.168.1.14', '2017-11-13 10:00:00', '2017-11-13 10:00:00', '30', null);
INSERT INTO t_log_schedule VALUES ('476', '2', '01', '192.168.1.14', '2017-11-13 11:00:00', '2017-11-13 11:00:00', '17', null);
INSERT INTO t_log_schedule VALUES ('477', '3', '01', '192.168.1.14', '2017-11-13 11:00:00', '2017-11-13 11:00:00', '41', null);
INSERT INTO t_log_schedule VALUES ('478', '1', '01', '192.168.1.14', '2017-11-13 11:00:00', '2017-11-13 11:00:00', '19', null);
INSERT INTO t_log_schedule VALUES ('479', '1', '01', '192.168.1.14', '2017-11-13 13:00:00', '2017-11-13 13:00:00', '22', null);
INSERT INTO t_log_schedule VALUES ('480', '2', '01', '192.168.1.14', '2017-11-13 13:00:00', '2017-11-13 13:00:00', '12', null);
INSERT INTO t_log_schedule VALUES ('481', '3', '01', '192.168.1.14', '2017-11-13 13:00:00', '2017-11-13 13:00:00', '33', null);
INSERT INTO t_log_schedule VALUES ('482', '1', '01', '192.168.1.14', '2017-11-13 15:00:00', '2017-11-13 15:00:00', '11', null);
INSERT INTO t_log_schedule VALUES ('483', '2', '01', '192.168.1.14', '2017-11-13 15:00:00', '2017-11-13 15:00:00', '1', null);
INSERT INTO t_log_schedule VALUES ('484', '3', '01', '192.168.1.14', '2017-11-13 15:00:00', '2017-11-13 15:00:00', '23', null);

-- ----------------------------
-- Table structure for `t_sys_area`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_area`;
CREATE TABLE `t_sys_area` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CODE` varchar(32) NOT NULL COMMENT '区域编码',
  `PHONE` varchar(32) DEFAULT NULL COMMENT '电话区号',
  `ZIP` varchar(32) DEFAULT NULL COMMENT '邮政编码',
  `NAME` varchar(64) NOT NULL COMMENT '区域名称',
  `ENAME` varchar(64) DEFAULT NULL COMMENT '区域英文名称',
  `SPELL` varchar(64) DEFAULT NULL COMMENT '区域拼音',
  `PATH` varchar(64) DEFAULT NULL COMMENT '区域路径',
  `FULLNAME` varchar(256) DEFAULT NULL COMMENT '区域全名称',
  `PARENT_ID` int(11) NOT NULL COMMENT '上级区域',
  `HOT` varchar(32) DEFAULT NULL COMMENT '是否热点区域',
  `LEVEL` varchar(32) DEFAULT NULL COMMENT '区域级别',
  `STATE` varchar(32) NOT NULL COMMENT '区域状态',
  `SORT` int(11) NOT NULL COMMENT '排序号',
  `REMARK` varchar(1024) DEFAULT NULL COMMENT '描述',
  `LUSER` int(11) DEFAULT NULL COMMENT '最后修改人',
  `LTIME` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5001 DEFAULT CHARSET=utf8 COMMENT='区域管理';

-- ----------------------------
-- Records of t_sys_area
-- ----------------------------
INSERT INTO t_sys_area VALUES ('1', '000000', '000000', '000000', '中国', 'Zhong Guo', 'Zhong Guo', ',1,', '中国', '0', '02', '01', '01', '1', '', null, null);
INSERT INTO t_sys_area VALUES ('2', '110000', '110000', '110000', '北京', 'Beijing Shi', 'Beijing Shi', ',1,2,', '中国-北京', '1', '02', '02', '01', '2', null, '1', '2017-11-01 15:40:22');
INSERT INTO t_sys_area VALUES ('3', '120000', '120000', '120000', '天津', 'Tianjin Shi', 'Tianjin Shi', ',1,3,', '中国-天津', '1', '02', '02', '01', '3', null, '1', '2017-10-29 17:28:17');
INSERT INTO t_sys_area VALUES ('4', '130000', '130000', '130000', '河北省', 'Hebei Sheng', 'Hebei Sheng', ',4,', '河北省', '1', '02', '02', '01', '4', '', null, null);
INSERT INTO t_sys_area VALUES ('5', '140000', '140000', '140000', '山西省', 'Shanxi Sheng ', 'Shanxi Sheng ', ',1,5,', '中国-山西省', '1', '02', '02', '01', '5', null, '1', '2017-11-01 16:33:13');
INSERT INTO t_sys_area VALUES ('6', '150000', '150000', '150000', '内蒙古自治区', 'Nei Mongol Zizhiqu', 'Nei Mongol Zizhiqu', ',1,6,', '中国-内蒙古自治区', '1', '02', '02', '01', '6', null, '1', '2017-10-29 23:21:18');
INSERT INTO t_sys_area VALUES ('7', '210000', '210000', '210000', '辽宁省', 'Liaoning Sheng', 'Liaoning Sheng', ',7,', '辽宁省', '1', '02', '02', '01', '7', '', null, null);
INSERT INTO t_sys_area VALUES ('8', '220000', '220000', '220000', '吉林省', 'Jilin Sheng', 'Jilin Sheng', ',8,', '吉林省', '1', '02', '02', '01', '8', '', null, null);
INSERT INTO t_sys_area VALUES ('9', '230000', '230000', '230000', '黑龙江省', 'Heilongjiang Sheng', 'Heilongjiang Sheng', ',9,', '黑龙江省', '1', '02', '02', '01', '9', '', null, null);
INSERT INTO t_sys_area VALUES ('10', '310000', '310000', '310000', '上海', 'Shanghai Shi', 'Shanghai Shi', ',10,', '上海', '1', '02', '02', '01', '10', '', null, null);
INSERT INTO t_sys_area VALUES ('11', '320000', '320000', '320000', '江苏省', 'Jiangsu Sheng', 'Jiangsu Sheng', ',11,', '江苏省', '1', '02', '02', '01', '11', '', null, null);
INSERT INTO t_sys_area VALUES ('12', '330000', '330000', '330000', '浙江省', 'Zhejiang Sheng', 'Zhejiang Sheng', ',12,', '浙江省', '1', '02', '02', '01', '12', '', null, null);
INSERT INTO t_sys_area VALUES ('13', '340000', '340000', '340000', '安徽省', 'Anhui Sheng', 'Anhui Sheng', ',13,', '安徽省', '1', '02', '02', '01', '13', '', null, null);
INSERT INTO t_sys_area VALUES ('14', '350000', '350000', '350000', '福建省', 'Fujian Sheng ', 'Fujian Sheng ', ',14,', '福建省', '1', '02', '02', '01', '14', '', null, null);
INSERT INTO t_sys_area VALUES ('15', '360000', '360000', '360000', '江西省', 'Jiangxi Sheng', 'Jiangxi Sheng', ',15,', '江西省', '1', '02', '02', '01', '15', '', null, null);
INSERT INTO t_sys_area VALUES ('16', '370000', '370000', '370000', '山东省', 'Shandong Sheng ', 'Shandong Sheng ', ',16,', '山东省', '1', '02', '02', '01', '16', '', null, null);
INSERT INTO t_sys_area VALUES ('17', '410000', '410000', '410000', '河南省', 'Henan Sheng', 'Henan Sheng', ',17,', '河南省', '1', '02', '02', '01', '17', '', null, null);
INSERT INTO t_sys_area VALUES ('18', '420000', '420000', '420000', '湖北省', 'Hubei Sheng', 'Hubei Sheng', ',18,', '湖北省', '1', '02', '02', '01', '18', '', null, null);
INSERT INTO t_sys_area VALUES ('19', '430000', '430000', '430000', '湖南省', 'Hunan Sheng', 'Hunan Sheng', ',19,', '湖南省', '1', '02', '02', '01', '19', '', null, null);
INSERT INTO t_sys_area VALUES ('20', '440000', '440000', '440000', '广东省', 'Guangdong Sheng', 'Guangdong Sheng', ',20,', '广东省', '1', '02', '02', '01', '20', '', null, null);
INSERT INTO t_sys_area VALUES ('21', '450000', '450000', '450000', '广西壮族自治区', 'Guangxi Zhuangzu Zizhiqu', 'Guangxi Zhuangzu Zizhiqu', ',21,', '广西壮族自治区', '1', '02', '02', '01', '21', '', null, null);
INSERT INTO t_sys_area VALUES ('22', '460000', '460000', '460000', '海南省', 'Hainan Sheng', 'Hainan Sheng', ',22,', '海南省', '1', '02', '02', '01', '22', '', null, null);
INSERT INTO t_sys_area VALUES ('23', '500000', '500000', '500000', '重庆', 'Chongqing Shi', 'Chongqing Shi', ',23,', '重庆', '1', '02', '02', '01', '23', '', null, null);
INSERT INTO t_sys_area VALUES ('24', '510000', '510000', '510000', '四川省', 'Sichuan Sheng', 'Sichuan Sheng', ',24,', '四川省', '1', '02', '02', '01', '24', '', null, null);
INSERT INTO t_sys_area VALUES ('25', '520000', '520000', '520000', '贵州省', 'Guizhou Sheng', 'Guizhou Sheng', ',25,', '贵州省', '1', '02', '02', '01', '25', '', null, null);
INSERT INTO t_sys_area VALUES ('26', '530000', '530000', '530000', '云南省', 'Yunnan Sheng', 'Yunnan Sheng', ',26,', '云南省', '1', '02', '02', '01', '26', '', null, null);
INSERT INTO t_sys_area VALUES ('27', '540000', '540000', '540000', '西藏自治区', 'Xizang Zizhiqu', 'Xizang Zizhiqu', ',27,', '西藏自治区', '1', '02', '02', '01', '27', '', null, null);
INSERT INTO t_sys_area VALUES ('28', '610000', '610000', '610000', '陕西省', 'Shanxi Sheng ', 'Shanxi Sheng ', ',28,', '陕西省', '1', '02', '02', '01', '28', '', null, null);
INSERT INTO t_sys_area VALUES ('29', '620000', '620000', '620000', '甘肃省', 'Gansu Sheng', 'Gansu Sheng', ',29,', '甘肃省', '1', '02', '02', '01', '29', '', null, null);
INSERT INTO t_sys_area VALUES ('30', '630000', '630000', '630000', '青海省', 'Qinghai Sheng', 'Qinghai Sheng', ',30,', '青海省', '1', '02', '02', '01', '30', '', null, null);
INSERT INTO t_sys_area VALUES ('31', '640000', '640000', '640000', '宁夏回族自治区', 'Ningxia Huizu Zizhiqu', 'Ningxia Huizu Zizhiqu', ',31,', '宁夏回族自治区', '1', '02', '02', '01', '31', '', null, null);
INSERT INTO t_sys_area VALUES ('32', '650000', '650000', '650000', '新疆维吾尔自治区', 'Xinjiang Uygur Zizhiqu', 'Xinjiang Uygur Zizhiqu', ',32,', '新疆维吾尔自治区', '1', '02', '02', '01', '32', '', null, null);
INSERT INTO t_sys_area VALUES ('33', '110100', '110100', '110100', '北京市', 'Shixiaqu', 'Shixiaqu', ',2,33,', '北京-北京市', '2', '02', '03', '01', '33', '', null, null);
INSERT INTO t_sys_area VALUES ('35', '120100', '120100', '120100', '天津市', 'Shixiaqu', 'Shixiaqu', ',3,35,', '天津-天津市', '3', '02', '03', '01', '35', '', null, null);
INSERT INTO t_sys_area VALUES ('37', '130100', '130100', '130100', '石家庄市', 'Shijiazhuang Shi', 'Shijiazhuang Shi', ',4,37,', '河北省-石家庄市', '4', '02', '03', '01', '37', '', null, null);
INSERT INTO t_sys_area VALUES ('38', '130200', '130200', '130200', '唐山市', 'Tangshan Shi', 'Tangshan Shi', ',4,38,', '河北省-唐山市', '4', '02', '03', '01', '38', '', null, null);
INSERT INTO t_sys_area VALUES ('39', '130300', '130300', '130300', '秦皇岛市', 'Qinhuangdao Shi', 'Qinhuangdao Shi', ',4,39,', '河北省-秦皇岛市', '4', '02', '03', '01', '39', '', null, null);
INSERT INTO t_sys_area VALUES ('40', '130400', '130400', '130400', '邯郸市', 'Handan Shi', 'Handan Shi', ',4,40,', '河北省-邯郸市', '4', '02', '03', '01', '40', '', null, null);
INSERT INTO t_sys_area VALUES ('41', '130500', '130500', '130500', '邢台市', 'Xingtai Shi', 'Xingtai Shi', ',4,41,', '河北省-邢台市', '4', '02', '03', '01', '41', '', null, null);
INSERT INTO t_sys_area VALUES ('42', '130600', '130600', '130600', '保定市', 'Baoding Shi', 'Baoding Shi', ',4,42,', '河北省-保定市', '4', '02', '03', '01', '42', '', null, null);
INSERT INTO t_sys_area VALUES ('43', '130700', '130700', '130700', '张家口市', 'Zhangjiakou Shi ', 'Zhangjiakou Shi ', ',4,43,', '河北省-张家口市', '4', '02', '03', '01', '43', '', null, null);
INSERT INTO t_sys_area VALUES ('44', '130800', '130800', '130800', '承德市', 'Chengde Shi', 'Chengde Shi', ',4,44,', '河北省-承德市', '4', '02', '03', '01', '44', '', null, null);
INSERT INTO t_sys_area VALUES ('45', '130900', '130900', '130900', '沧州市', 'Cangzhou Shi', 'Cangzhou Shi', ',4,45,', '河北省-沧州市', '4', '02', '03', '01', '45', '', null, null);
INSERT INTO t_sys_area VALUES ('46', '131000', '131000', '131000', '廊坊市', 'Langfang Shi', 'Langfang Shi', ',4,46,', '河北省-廊坊市', '4', '02', '03', '01', '46', '', null, null);
INSERT INTO t_sys_area VALUES ('47', '131100', '131100', '131100', '衡水市', 'Hengshui Shi ', 'Hengshui Shi ', ',4,47,', '河北省-衡水市', '4', '02', '03', '01', '47', '', null, null);
INSERT INTO t_sys_area VALUES ('48', '140100', '140100', '140100', '太原市', 'Taiyuan Shi', 'Taiyuan Shi', ',5,48,', '山西省-太原市', '5', '02', '03', '01', '48', '', null, null);
INSERT INTO t_sys_area VALUES ('49', '140200', '140200', '140200', '大同市', 'Datong Shi ', 'Datong Shi ', ',5,49,', '山西省-大同市', '5', '02', '03', '01', '49', '', null, null);
INSERT INTO t_sys_area VALUES ('50', '140300', '140300', '140300', '阳泉市', 'Yangquan Shi', 'Yangquan Shi', ',5,50,', '山西省-阳泉市', '5', '02', '03', '01', '50', '', null, null);
INSERT INTO t_sys_area VALUES ('51', '140400', '140400', '140400', '长治市', 'Changzhi Shi', 'Changzhi Shi', ',5,51,', '山西省-长治市', '5', '02', '03', '01', '51', '', null, null);
INSERT INTO t_sys_area VALUES ('52', '140500', '140500', '140500', '晋城市', 'Jincheng Shi ', 'Jincheng Shi ', ',5,52,', '山西省-晋城市', '5', '02', '03', '01', '52', '', null, null);
INSERT INTO t_sys_area VALUES ('53', '140600', '140600', '140600', '朔州市', 'Shuozhou Shi ', 'Shuozhou Shi ', ',5,53,', '山西省-朔州市', '5', '02', '03', '01', '53', '', null, null);
INSERT INTO t_sys_area VALUES ('54', '140700', '140700', '140700', '晋中市', 'Jinzhong Shi', 'Jinzhong Shi', ',5,54,', '山西省-晋中市', '5', '02', '03', '01', '54', '', null, null);
INSERT INTO t_sys_area VALUES ('55', '140800', '140800', '140800', '运城市', 'Yuncheng Shi', 'Yuncheng Shi', ',5,55,', '山西省-运城市', '5', '02', '03', '01', '55', '', null, null);
INSERT INTO t_sys_area VALUES ('56', '140900', '140900', '140900', '忻州市', 'Xinzhou Shi', 'Xinzhou Shi', ',5,56,', '山西省-忻州市', '5', '02', '03', '01', '56', '', null, null);
INSERT INTO t_sys_area VALUES ('57', '141000', '141000', '141000', '临汾市', 'Linfen Shi', 'Linfen Shi', ',5,57,', '山西省-临汾市', '5', '02', '03', '01', '57', '', null, null);
INSERT INTO t_sys_area VALUES ('58', '141100', '141100', '141100', '吕梁市', 'Lvliang Shi', 'Lvliang Shi', ',5,58,', '山西省-吕梁市', '5', '02', '03', '01', '58', '', null, null);
INSERT INTO t_sys_area VALUES ('59', '150100', '150100', '150100', '呼和浩特市', 'Hohhot Shi', 'Hohhot Shi', ',6,59,', '内蒙古自治区-呼和浩特市', '6', '02', '03', '01', '59', '', null, null);
INSERT INTO t_sys_area VALUES ('60', '150200', '150200', '150200', '包头市', 'Baotou Shi ', 'Baotou Shi ', ',6,60,', '内蒙古自治区-包头市', '6', '02', '03', '01', '60', '', null, null);
INSERT INTO t_sys_area VALUES ('61', '150300', '150300', '150300', '乌海市', 'Wuhai Shi', 'Wuhai Shi', ',6,61,', '内蒙古自治区-乌海市', '6', '02', '03', '01', '61', '', null, null);
INSERT INTO t_sys_area VALUES ('62', '150400', '150400', '150400', '赤峰市', 'Chifeng (Ulanhad)Shi', 'Chifeng (Ulanhad)Shi', ',6,62,', '内蒙古自治区-赤峰市', '6', '02', '03', '01', '62', '', null, null);
INSERT INTO t_sys_area VALUES ('63', '150500', '150500', '150500', '通辽市', 'Tongliao Shi', 'Tongliao Shi', ',6,63,', '内蒙古自治区-通辽市', '6', '02', '03', '01', '63', '', null, null);
INSERT INTO t_sys_area VALUES ('64', '150600', '150600', '150600', '鄂尔多斯市', 'Eerduosi Shi', 'Eerduosi Shi', ',6,64,', '内蒙古自治区-鄂尔多斯市', '6', '02', '03', '01', '64', '', null, null);
INSERT INTO t_sys_area VALUES ('65', '150700', '150700', '150700', '呼伦贝尔市', 'Hulunbeier Shi ', 'Hulunbeier Shi ', ',6,65,', '内蒙古自治区-呼伦贝尔市', '6', '02', '03', '01', '65', '', null, null);
INSERT INTO t_sys_area VALUES ('66', '150800', '150800', '150800', '巴彦淖尔市', 'Bayannaoer Shi', 'Bayannaoer Shi', ',6,66,', '内蒙古自治区-巴彦淖尔市', '6', '02', '03', '01', '66', '', null, null);
INSERT INTO t_sys_area VALUES ('67', '150900', '150900', '150900', '乌兰察布市', 'Wulanchabu Shi', 'Wulanchabu Shi', ',6,67,', '内蒙古自治区-乌兰察布市', '6', '02', '03', '01', '67', '', null, null);
INSERT INTO t_sys_area VALUES ('68', '152200', '152200', '152200', '兴安盟', 'Hinggan Meng', 'Hinggan Meng', ',6,68,', '内蒙古自治区-兴安盟', '6', '02', '03', '01', '68', '', null, null);
INSERT INTO t_sys_area VALUES ('69', '152500', '152500', '152500', '锡林郭勒盟', 'Xilin Gol Meng', 'Xilin Gol Meng', ',6,69,', '内蒙古自治区-锡林郭勒盟', '6', '02', '03', '01', '69', '', null, null);
INSERT INTO t_sys_area VALUES ('70', '152900', '152900', '152900', '阿拉善盟', 'Alxa Meng', 'Alxa Meng', ',6,70,', '内蒙古自治区-阿拉善盟', '6', '02', '03', '01', '70', '', null, null);
INSERT INTO t_sys_area VALUES ('71', '210100', '210100', '210100', '沈阳市', 'Shenyang Shi', 'Shenyang Shi', ',7,71,', '辽宁省-沈阳市', '7', '02', '03', '01', '71', '', null, null);
INSERT INTO t_sys_area VALUES ('72', '210200', '210200', '210200', '大连市', 'Dalian Shi', 'Dalian Shi', ',7,72,', '辽宁省-大连市', '7', '02', '03', '01', '72', '', null, null);
INSERT INTO t_sys_area VALUES ('73', '210300', '210300', '210300', '鞍山市', 'AnShan Shi', 'AnShan Shi', ',7,73,', '辽宁省-鞍山市', '7', '02', '03', '01', '73', '', null, null);
INSERT INTO t_sys_area VALUES ('74', '210400', '210400', '210400', '抚顺市', 'Fushun Shi', 'Fushun Shi', ',7,74,', '辽宁省-抚顺市', '7', '02', '03', '01', '74', '', null, null);
INSERT INTO t_sys_area VALUES ('75', '210500', '210500', '210500', '本溪市', 'Benxi Shi', 'Benxi Shi', ',7,75,', '辽宁省-本溪市', '7', '02', '03', '01', '75', '', null, null);
INSERT INTO t_sys_area VALUES ('76', '210600', '210600', '210600', '丹东市', 'Dandong Shi', 'Dandong Shi', ',7,76,', '辽宁省-丹东市', '7', '02', '03', '01', '76', '', null, null);
INSERT INTO t_sys_area VALUES ('77', '210700', '210700', '210700', '锦州市', 'Jinzhou Shi', 'Jinzhou Shi', ',7,77,', '辽宁省-锦州市', '7', '02', '03', '01', '77', '', null, null);
INSERT INTO t_sys_area VALUES ('78', '210800', '210800', '210800', '营口市', 'Yingkou Shi', 'Yingkou Shi', ',7,78,', '辽宁省-营口市', '7', '02', '03', '01', '78', '', null, null);
INSERT INTO t_sys_area VALUES ('79', '210900', '210900', '210900', '阜新市', 'Fuxin Shi', 'Fuxin Shi', ',7,79,', '辽宁省-阜新市', '7', '02', '03', '01', '79', '', null, null);
INSERT INTO t_sys_area VALUES ('80', '211000', '211000', '211000', '辽阳市', 'Liaoyang Shi', 'Liaoyang Shi', ',7,80,', '辽宁省-辽阳市', '7', '02', '03', '01', '80', '', null, null);
INSERT INTO t_sys_area VALUES ('81', '211100', '211100', '211100', '盘锦市', 'Panjin Shi', 'Panjin Shi', ',7,81,', '辽宁省-盘锦市', '7', '02', '03', '01', '81', '', null, null);
INSERT INTO t_sys_area VALUES ('82', '211200', '211200', '211200', '铁岭市', 'Tieling Shi', 'Tieling Shi', ',7,82,', '辽宁省-铁岭市', '7', '02', '03', '01', '82', '', null, null);
INSERT INTO t_sys_area VALUES ('83', '211300', '211300', '211300', '朝阳市', 'Chaoyang Shi', 'Chaoyang Shi', ',7,83,', '辽宁省-朝阳市', '7', '02', '03', '01', '83', '', null, null);
INSERT INTO t_sys_area VALUES ('84', '211400', '211400', '211400', '葫芦岛市', 'Huludao Shi', 'Huludao Shi', ',7,84,', '辽宁省-葫芦岛市', '7', '02', '03', '01', '84', '', null, null);
INSERT INTO t_sys_area VALUES ('85', '220100', '220100', '220100', '长春市', 'Changchun Shi ', 'Changchun Shi ', ',8,85,', '吉林省-长春市', '8', '02', '03', '01', '85', '', null, null);
INSERT INTO t_sys_area VALUES ('86', '220200', '220200', '220200', '吉林市', 'Jilin Shi ', 'Jilin Shi ', ',8,86,', '吉林省-吉林市', '8', '02', '03', '01', '86', '', null, null);
INSERT INTO t_sys_area VALUES ('87', '220300', '220300', '220300', '四平市', 'Siping Shi', 'Siping Shi', ',8,87,', '吉林省-四平市', '8', '02', '03', '01', '87', '', null, null);
INSERT INTO t_sys_area VALUES ('88', '220400', '220400', '220400', '辽源市', 'Liaoyuan Shi', 'Liaoyuan Shi', ',8,88,', '吉林省-辽源市', '8', '02', '03', '01', '88', '', null, null);
INSERT INTO t_sys_area VALUES ('89', '220500', '220500', '220500', '通化市', 'Tonghua Shi', 'Tonghua Shi', ',8,89,', '吉林省-通化市', '8', '02', '03', '01', '89', '', null, null);
INSERT INTO t_sys_area VALUES ('90', '220600', '220600', '220600', '白山市', 'Baishan Shi', 'Baishan Shi', ',8,90,', '吉林省-白山市', '8', '02', '03', '01', '90', '', null, null);
INSERT INTO t_sys_area VALUES ('91', '220700', '220700', '220700', '松原市', 'Songyuan Shi', 'Songyuan Shi', ',8,91,', '吉林省-松原市', '8', '02', '03', '01', '91', '', null, null);
INSERT INTO t_sys_area VALUES ('92', '220800', '220800', '220800', '白城市', 'Baicheng Shi', 'Baicheng Shi', ',8,92,', '吉林省-白城市', '8', '02', '03', '01', '92', '', null, null);
INSERT INTO t_sys_area VALUES ('93', '222400', '222400', '222400', '延边朝鲜族自治州', 'Yanbian Chosenzu Zizhizhou', 'Yanbian Chosenzu Zizhizhou', ',8,93,', '吉林省-延边朝鲜族自治州', '8', '02', '03', '01', '93', '', null, null);
INSERT INTO t_sys_area VALUES ('94', '230100', '230100', '230100', '哈尔滨市', 'Harbin Shi', 'Harbin Shi', ',9,94,', '黑龙江省-哈尔滨市', '9', '02', '03', '01', '94', '', null, null);
INSERT INTO t_sys_area VALUES ('95', '230200', '230200', '230200', '齐齐哈尔市', 'Qiqihar Shi', 'Qiqihar Shi', ',9,95,', '黑龙江省-齐齐哈尔市', '9', '02', '03', '01', '95', '', null, null);
INSERT INTO t_sys_area VALUES ('96', '230300', '230300', '230300', '鸡西市', 'Jixi Shi', 'Jixi Shi', ',9,96,', '黑龙江省-鸡西市', '9', '02', '03', '01', '96', '', null, null);
INSERT INTO t_sys_area VALUES ('97', '230400', '230400', '230400', '鹤岗市', 'Hegang Shi', 'Hegang Shi', ',9,97,', '黑龙江省-鹤岗市', '9', '02', '03', '01', '97', '', null, null);
INSERT INTO t_sys_area VALUES ('98', '230500', '230500', '230500', '双鸭山市', 'Shuangyashan Shi', 'Shuangyashan Shi', ',9,98,', '黑龙江省-双鸭山市', '9', '02', '03', '01', '98', '', null, null);
INSERT INTO t_sys_area VALUES ('99', '230600', '230600', '230600', '大庆市', 'Daqing Shi', 'Daqing Shi', ',9,99,', '黑龙江省-大庆市', '9', '02', '03', '01', '99', '', null, null);
INSERT INTO t_sys_area VALUES ('100', '230700', '230700', '230700', '伊春市', 'Yichun Shi', 'Yichun Shi', ',9,100,', '黑龙江省-伊春市', '9', '02', '03', '01', '100', '', null, null);
INSERT INTO t_sys_area VALUES ('101', '230800', '230800', '230800', '佳木斯市', 'Jiamusi Shi', 'Jiamusi Shi', ',9,101,', '黑龙江省-佳木斯市', '9', '02', '03', '01', '101', '', null, null);
INSERT INTO t_sys_area VALUES ('102', '230900', '230900', '230900', '七台河市', 'Qitaihe Shi', 'Qitaihe Shi', ',9,102,', '黑龙江省-七台河市', '9', '02', '03', '01', '102', '', null, null);
INSERT INTO t_sys_area VALUES ('103', '231000', '231000', '231000', '牡丹江市', 'Mudanjiang Shi', 'Mudanjiang Shi', ',9,103,', '黑龙江省-牡丹江市', '9', '02', '03', '01', '103', '', null, null);
INSERT INTO t_sys_area VALUES ('104', '231100', '231100', '231100', '黑河市', 'Heihe Shi', 'Heihe Shi', ',9,104,', '黑龙江省-黑河市', '9', '02', '03', '01', '104', '', null, null);
INSERT INTO t_sys_area VALUES ('105', '231200', '231200', '231200', '绥化市', 'Suihua Shi', 'Suihua Shi', ',9,105,', '黑龙江省-绥化市', '9', '02', '03', '01', '105', '', null, null);
INSERT INTO t_sys_area VALUES ('106', '232700', '232700', '232700', '大兴安岭地区', 'Da Hinggan Ling Diqu', 'Da Hinggan Ling Diqu', ',9,106,', '黑龙江省-大兴安岭地区', '9', '02', '03', '01', '106', '', null, null);
INSERT INTO t_sys_area VALUES ('107', '310100', '310100', '310100', '上海市', 'Shixiaqu', 'Shixiaqu', ',10,107,', '上海-上海市', '10', '02', '03', '01', '107', '', null, null);
INSERT INTO t_sys_area VALUES ('109', '320100', '320100', '320100', '南京市', 'Nanjing Shi', 'Nanjing Shi', ',11,109,', '江苏省-南京市', '11', '02', '03', '01', '109', '', null, null);
INSERT INTO t_sys_area VALUES ('110', '320200', '320200', '320200', '无锡市', 'Wuxi Shi', 'Wuxi Shi', ',11,110,', '江苏省-无锡市', '11', '02', '03', '01', '110', '', null, null);
INSERT INTO t_sys_area VALUES ('111', '320300', '320300', '320300', '徐州市', 'Xuzhou Shi', 'Xuzhou Shi', ',11,111,', '江苏省-徐州市', '11', '02', '03', '01', '111', '', null, null);
INSERT INTO t_sys_area VALUES ('112', '320400', '320400', '320400', '常州市', 'Changzhou Shi', 'Changzhou Shi', ',11,112,', '江苏省-常州市', '11', '02', '03', '01', '112', '', null, null);
INSERT INTO t_sys_area VALUES ('113', '320500', '320500', '320500', '苏州市', 'Suzhou Shi', 'Suzhou Shi', ',11,113,', '江苏省-苏州市', '11', '02', '03', '01', '113', '', null, null);
INSERT INTO t_sys_area VALUES ('114', '320600', '320600', '320600', '南通市', 'Nantong Shi', 'Nantong Shi', ',11,114,', '江苏省-南通市', '11', '02', '03', '01', '114', '', null, null);
INSERT INTO t_sys_area VALUES ('115', '320700', '320700', '320700', '连云港市', 'Lianyungang Shi', 'Lianyungang Shi', ',11,115,', '江苏省-连云港市', '11', '02', '03', '01', '115', '', null, null);
INSERT INTO t_sys_area VALUES ('116', '320800', '320800', '320800', '淮安市', 'Huai,an Xian', 'Huai,an Xian', ',11,116,', '江苏省-淮安市', '11', '02', '03', '01', '116', '', null, null);
INSERT INTO t_sys_area VALUES ('117', '320900', '320900', '320900', '盐城市', 'Yancheng Shi', 'Yancheng Shi', ',11,117,', '江苏省-盐城市', '11', '02', '03', '01', '117', '', null, null);
INSERT INTO t_sys_area VALUES ('118', '321000', '321000', '321000', '扬州市', 'Yangzhou Shi', 'Yangzhou Shi', ',11,118,', '江苏省-扬州市', '11', '02', '03', '01', '118', '', null, null);
INSERT INTO t_sys_area VALUES ('119', '321100', '321100', '321100', '镇江市', 'Zhenjiang Shi', 'Zhenjiang Shi', ',11,119,', '江苏省-镇江市', '11', '02', '03', '01', '119', '', null, null);
INSERT INTO t_sys_area VALUES ('120', '321200', '321200', '321200', '泰州市', 'Taizhou Shi', 'Taizhou Shi', ',11,120,', '江苏省-泰州市', '11', '02', '03', '01', '120', '', null, null);
INSERT INTO t_sys_area VALUES ('121', '321300', '321300', '321300', '宿迁市', 'Suqian Shi', 'Suqian Shi', ',11,121,', '江苏省-宿迁市', '11', '02', '03', '01', '121', '', null, null);
INSERT INTO t_sys_area VALUES ('122', '330100', '330100', '330100', '杭州市', 'Hangzhou Shi', 'Hangzhou Shi', ',12,122,', '浙江省-杭州市', '12', '02', '03', '01', '122', '', null, null);
INSERT INTO t_sys_area VALUES ('123', '330200', '330200', '330200', '宁波市', 'Ningbo Shi', 'Ningbo Shi', ',12,123,', '浙江省-宁波市', '12', '02', '03', '01', '123', '', null, null);
INSERT INTO t_sys_area VALUES ('124', '330300', '330300', '330300', '温州市', 'Wenzhou Shi', 'Wenzhou Shi', ',12,124,', '浙江省-温州市', '12', '02', '03', '01', '124', '', null, null);
INSERT INTO t_sys_area VALUES ('125', '330400', '330400', '330400', '嘉兴市', 'Jiaxing Shi', 'Jiaxing Shi', ',12,125,', '浙江省-嘉兴市', '12', '02', '03', '01', '125', '', null, null);
INSERT INTO t_sys_area VALUES ('126', '330500', '330500', '330500', '湖州市', 'Huzhou Shi ', 'Huzhou Shi ', ',12,126,', '浙江省-湖州市', '12', '02', '03', '01', '126', '', null, null);
INSERT INTO t_sys_area VALUES ('127', '330600', '330600', '330600', '绍兴市', 'Shaoxing Shi', 'Shaoxing Shi', ',12,127,', '浙江省-绍兴市', '12', '02', '03', '01', '127', '', null, null);
INSERT INTO t_sys_area VALUES ('128', '330700', '330700', '330700', '金华市', 'Jinhua Shi', 'Jinhua Shi', ',12,128,', '浙江省-金华市', '12', '02', '03', '01', '128', '', null, null);
INSERT INTO t_sys_area VALUES ('129', '330800', '330800', '330800', '衢州市', 'Quzhou Shi', 'Quzhou Shi', ',12,129,', '浙江省-衢州市', '12', '02', '03', '01', '129', '', null, null);
INSERT INTO t_sys_area VALUES ('130', '330900', '330900', '330900', '舟山市', 'Zhoushan Shi', 'Zhoushan Shi', ',12,130,', '浙江省-舟山市', '12', '02', '03', '01', '130', '', null, null);
INSERT INTO t_sys_area VALUES ('131', '331000', '331000', '331000', '台州市', 'Taizhou Shi', 'Taizhou Shi', ',12,131,', '浙江省-台州市', '12', '02', '03', '01', '131', '', null, null);
INSERT INTO t_sys_area VALUES ('132', '331100', '331100', '331100', '丽水市', 'Lishui Shi', 'Lishui Shi', ',12,132,', '浙江省-丽水市', '12', '02', '03', '01', '132', '', null, null);
INSERT INTO t_sys_area VALUES ('133', '340100', '340100', '340100', '合肥市', 'Hefei Shi', 'Hefei Shi', ',13,133,', '安徽省-合肥市', '13', '02', '03', '01', '133', '', null, null);
INSERT INTO t_sys_area VALUES ('134', '340200', '340200', '340200', '芜湖市', 'Wuhu Shi', 'Wuhu Shi', ',13,134,', '安徽省-芜湖市', '13', '02', '03', '01', '134', '', null, null);
INSERT INTO t_sys_area VALUES ('135', '340300', '340300', '340300', '蚌埠市', 'Bengbu Shi', 'Bengbu Shi', ',13,135,', '安徽省-蚌埠市', '13', '02', '03', '01', '135', '', null, null);
INSERT INTO t_sys_area VALUES ('136', '340400', '340400', '340400', '淮南市', 'Huainan Shi', 'Huainan Shi', ',13,136,', '安徽省-淮南市', '13', '02', '03', '01', '136', '', null, null);
INSERT INTO t_sys_area VALUES ('137', '340500', '340500', '340500', '马鞍山市', 'Ma,anshan Shi', 'Ma,anshan Shi', ',13,137,', '安徽省-马鞍山市', '13', '02', '03', '01', '137', '', null, null);
INSERT INTO t_sys_area VALUES ('138', '340600', '340600', '340600', '淮北市', 'Huaibei Shi', 'Huaibei Shi', ',13,138,', '安徽省-淮北市', '13', '02', '03', '01', '138', '', null, null);
INSERT INTO t_sys_area VALUES ('139', '340700', '340700', '340700', '铜陵市', 'Tongling Shi', 'Tongling Shi', ',13,139,', '安徽省-铜陵市', '13', '02', '03', '01', '139', '', null, null);
INSERT INTO t_sys_area VALUES ('140', '340800', '340800', '340800', '安庆市', 'Anqing Shi', 'Anqing Shi', ',13,140,', '安徽省-安庆市', '13', '02', '03', '01', '140', '', null, null);
INSERT INTO t_sys_area VALUES ('141', '341000', '341000', '341000', '黄山市', 'Huangshan Shi', 'Huangshan Shi', ',13,141,', '安徽省-黄山市', '13', '02', '03', '01', '141', '', null, null);
INSERT INTO t_sys_area VALUES ('142', '341100', '341100', '341100', '滁州市', 'Chuzhou Shi', 'Chuzhou Shi', ',13,142,', '安徽省-滁州市', '13', '02', '03', '01', '142', '', null, null);
INSERT INTO t_sys_area VALUES ('143', '341200', '341200', '341200', '阜阳市', 'Fuyang Shi', 'Fuyang Shi', ',13,143,', '安徽省-阜阳市', '13', '02', '03', '01', '143', '', null, null);
INSERT INTO t_sys_area VALUES ('144', '341300', '341300', '341300', '宿州市', 'Suzhou Shi', 'Suzhou Shi', ',13,144,', '安徽省-宿州市', '13', '02', '03', '01', '144', '', null, null);
INSERT INTO t_sys_area VALUES ('145', '341400', '341400', '341400', '巢湖市', 'Chaohu Shi', 'Chaohu Shi', ',13,145,', '安徽省-巢湖市', '13', '02', '03', '01', '145', '', null, null);
INSERT INTO t_sys_area VALUES ('146', '341500', '341500', '341500', '六安市', 'Liu,an Shi', 'Liu,an Shi', ',13,146,', '安徽省-六安市', '13', '02', '03', '01', '146', '', null, null);
INSERT INTO t_sys_area VALUES ('147', '341600', '341600', '341600', '亳州市', 'Bozhou Shi', 'Bozhou Shi', ',13,147,', '安徽省-亳州市', '13', '02', '03', '01', '147', '', null, null);
INSERT INTO t_sys_area VALUES ('148', '341700', '341700', '341700', '池州市', 'Chizhou Shi', 'Chizhou Shi', ',13,148,', '安徽省-池州市', '13', '02', '03', '01', '148', '', null, null);
INSERT INTO t_sys_area VALUES ('149', '341800', '341800', '341800', '宣城市', 'Xuancheng Shi', 'Xuancheng Shi', ',13,149,', '安徽省-宣城市', '13', '02', '03', '01', '149', '', null, null);
INSERT INTO t_sys_area VALUES ('150', '350100', '350100', '350100', '福州市', 'Fuzhou Shi', 'Fuzhou Shi', ',14,150,', '福建省-福州市', '14', '02', '03', '01', '150', '', null, null);
INSERT INTO t_sys_area VALUES ('151', '350200', '350200', '350200', '厦门市', 'Xiamen Shi', 'Xiamen Shi', ',14,151,', '福建省-厦门市', '14', '02', '03', '01', '151', '', null, null);
INSERT INTO t_sys_area VALUES ('152', '350300', '350300', '350300', '莆田市', 'Putian Shi', 'Putian Shi', ',14,152,', '福建省-莆田市', '14', '02', '03', '01', '152', '', null, null);
INSERT INTO t_sys_area VALUES ('153', '350400', '350400', '350400', '三明市', 'Sanming Shi', 'Sanming Shi', ',14,153,', '福建省-三明市', '14', '02', '03', '01', '153', '', null, null);
INSERT INTO t_sys_area VALUES ('154', '350500', '350500', '350500', '泉州市', 'Quanzhou Shi', 'Quanzhou Shi', ',14,154,', '福建省-泉州市', '14', '02', '03', '01', '154', '', null, null);
INSERT INTO t_sys_area VALUES ('155', '350600', '350600', '350600', '漳州市', 'Zhangzhou Shi', 'Zhangzhou Shi', ',14,155,', '福建省-漳州市', '14', '02', '03', '01', '155', '', null, null);
INSERT INTO t_sys_area VALUES ('156', '350700', '350700', '350700', '南平市', 'Nanping Shi', 'Nanping Shi', ',14,156,', '福建省-南平市', '14', '02', '03', '01', '156', '', null, null);
INSERT INTO t_sys_area VALUES ('157', '350800', '350800', '350800', '龙岩市', 'Longyan Shi', 'Longyan Shi', ',14,157,', '福建省-龙岩市', '14', '02', '03', '01', '157', '', null, null);
INSERT INTO t_sys_area VALUES ('158', '350900', '350900', '350900', '宁德市', 'Ningde Shi', 'Ningde Shi', ',14,158,', '福建省-宁德市', '14', '02', '03', '01', '158', '', null, null);
INSERT INTO t_sys_area VALUES ('159', '360100', '360100', '360100', '南昌市', 'Nanchang Shi', 'Nanchang Shi', ',15,159,', '江西省-南昌市', '15', '02', '03', '01', '159', '', null, null);
INSERT INTO t_sys_area VALUES ('160', '360200', '360200', '360200', '景德镇市', 'Jingdezhen Shi', 'Jingdezhen Shi', ',15,160,', '江西省-景德镇市', '15', '02', '03', '01', '160', '', null, null);
INSERT INTO t_sys_area VALUES ('161', '360300', '360300', '360300', '萍乡市', 'Pingxiang Shi', 'Pingxiang Shi', ',15,161,', '江西省-萍乡市', '15', '02', '03', '01', '161', '', null, null);
INSERT INTO t_sys_area VALUES ('162', '360400', '360400', '360400', '九江市', 'Jiujiang Shi', 'Jiujiang Shi', ',15,162,', '江西省-九江市', '15', '02', '03', '01', '162', '', null, null);
INSERT INTO t_sys_area VALUES ('163', '360500', '360500', '360500', '新余市', 'Xinyu Shi', 'Xinyu Shi', ',15,163,', '江西省-新余市', '15', '02', '03', '01', '163', '', null, null);
INSERT INTO t_sys_area VALUES ('164', '360600', '360600', '360600', '鹰潭市', 'Yingtan Shi', 'Yingtan Shi', ',15,164,', '江西省-鹰潭市', '15', '02', '03', '01', '164', '', null, null);
INSERT INTO t_sys_area VALUES ('165', '360700', '360700', '360700', '赣州市', 'Ganzhou Shi', 'Ganzhou Shi', ',15,165,', '江西省-赣州市', '15', '02', '03', '01', '165', '', null, null);
INSERT INTO t_sys_area VALUES ('166', '360800', '360800', '360800', '吉安市', 'Ji,an Shi', 'Ji,an Shi', ',15,166,', '江西省-吉安市', '15', '02', '03', '01', '166', '', null, null);
INSERT INTO t_sys_area VALUES ('167', '360900', '360900', '360900', '宜春市', 'Yichun Shi', 'Yichun Shi', ',15,167,', '江西省-宜春市', '15', '02', '03', '01', '167', '', null, null);
INSERT INTO t_sys_area VALUES ('168', '361000', '361000', '361000', '抚州市', 'Wuzhou Shi', 'Wuzhou Shi', ',15,168,', '江西省-抚州市', '15', '02', '03', '01', '168', '', null, null);
INSERT INTO t_sys_area VALUES ('169', '361100', '361100', '361100', '上饶市', 'Shangrao Shi', 'Shangrao Shi', ',15,169,', '江西省-上饶市', '15', '02', '03', '01', '169', '', null, null);
INSERT INTO t_sys_area VALUES ('170', '370100', '370100', '370100', '济南市', 'Jinan Shi', 'Jinan Shi', ',16,170,', '山东省-济南市', '16', '02', '03', '01', '170', '', null, null);
INSERT INTO t_sys_area VALUES ('171', '370200', '370200', '370200', '青岛市', 'Qingdao Shi', 'Qingdao Shi', ',16,171,', '山东省-青岛市', '16', '02', '03', '01', '171', '', null, null);
INSERT INTO t_sys_area VALUES ('172', '370300', '370300', '370300', '淄博市', 'Zibo Shi', 'Zibo Shi', ',16,172,', '山东省-淄博市', '16', '02', '03', '01', '172', '', null, null);
INSERT INTO t_sys_area VALUES ('173', '370400', '370400', '370400', '枣庄市', 'Zaozhuang Shi', 'Zaozhuang Shi', ',16,173,', '山东省-枣庄市', '16', '02', '03', '01', '173', '', null, null);
INSERT INTO t_sys_area VALUES ('174', '370500', '370500', '370500', '东营市', 'Dongying Shi', 'Dongying Shi', ',16,174,', '山东省-东营市', '16', '02', '03', '01', '174', '', null, null);
INSERT INTO t_sys_area VALUES ('175', '370600', '370600', '370600', '烟台市', 'Yantai Shi', 'Yantai Shi', ',16,175,', '山东省-烟台市', '16', '02', '03', '01', '175', '', null, null);
INSERT INTO t_sys_area VALUES ('176', '370700', '370700', '370700', '潍坊市', 'Weifang Shi', 'Weifang Shi', ',16,176,', '山东省-潍坊市', '16', '02', '03', '01', '176', '', null, null);
INSERT INTO t_sys_area VALUES ('177', '370800', '370800', '370800', '济宁市', 'Jining Shi', 'Jining Shi', ',16,177,', '山东省-济宁市', '16', '02', '03', '01', '177', '', null, null);
INSERT INTO t_sys_area VALUES ('178', '370900', '370900', '370900', '泰安市', 'Tai,an Shi', 'Tai,an Shi', ',16,178,', '山东省-泰安市', '16', '02', '03', '01', '178', '', null, null);
INSERT INTO t_sys_area VALUES ('179', '371000', '371000', '371000', '威海市', 'Weihai Shi', 'Weihai Shi', ',16,179,', '山东省-威海市', '16', '02', '03', '01', '179', '', null, null);
INSERT INTO t_sys_area VALUES ('180', '371100', '371100', '371100', '日照市', 'Rizhao Shi', 'Rizhao Shi', ',16,180,', '山东省-日照市', '16', '02', '03', '01', '180', '', null, null);
INSERT INTO t_sys_area VALUES ('181', '371200', '371200', '371200', '莱芜市', 'Laiwu Shi', 'Laiwu Shi', ',16,181,', '山东省-莱芜市', '16', '02', '03', '01', '181', '', null, null);
INSERT INTO t_sys_area VALUES ('182', '371300', '371300', '371300', '临沂市', 'Linyi Shi', 'Linyi Shi', ',16,182,', '山东省-临沂市', '16', '02', '03', '01', '182', '', null, null);
INSERT INTO t_sys_area VALUES ('183', '371400', '371400', '371400', '德州市', 'Dezhou Shi', 'Dezhou Shi', ',16,183,', '山东省-德州市', '16', '02', '03', '01', '183', '', null, null);
INSERT INTO t_sys_area VALUES ('184', '371500', '371500', '371500', '聊城市', 'Liaocheng Shi', 'Liaocheng Shi', ',16,184,', '山东省-聊城市', '16', '02', '03', '01', '184', '', null, null);
INSERT INTO t_sys_area VALUES ('185', '371600', '371600', '371600', '滨州市', 'Binzhou Shi', 'Binzhou Shi', ',16,185,', '山东省-滨州市', '16', '02', '03', '01', '185', '', null, null);
INSERT INTO t_sys_area VALUES ('186', '371700', '371700', '371700', '菏泽市', 'Heze Shi', 'Heze Shi', ',16,186,', '山东省-菏泽市', '16', '02', '03', '01', '186', '', null, null);
INSERT INTO t_sys_area VALUES ('187', '410100', '410100', '410100', '郑州市', 'Zhengzhou Shi', 'Zhengzhou Shi', ',17,187,', '河南省-郑州市', '17', '02', '03', '01', '187', '', null, null);
INSERT INTO t_sys_area VALUES ('188', '410200', '410200', '410200', '开封市', 'Kaifeng Shi', 'Kaifeng Shi', ',17,188,', '河南省-开封市', '17', '02', '03', '01', '188', '', null, null);
INSERT INTO t_sys_area VALUES ('189', '410300', '410300', '410300', '洛阳市', 'Luoyang Shi', 'Luoyang Shi', ',17,189,', '河南省-洛阳市', '17', '02', '03', '01', '189', '', null, null);
INSERT INTO t_sys_area VALUES ('190', '410400', '410400', '410400', '平顶山市', 'Pingdingshan Shi', 'Pingdingshan Shi', ',17,190,', '河南省-平顶山市', '17', '02', '03', '01', '190', '', null, null);
INSERT INTO t_sys_area VALUES ('191', '410500', '410500', '410500', '安阳市', 'Anyang Shi', 'Anyang Shi', ',17,191,', '河南省-安阳市', '17', '02', '03', '01', '191', '', null, null);
INSERT INTO t_sys_area VALUES ('192', '410600', '410600', '410600', '鹤壁市', 'Hebi Shi', 'Hebi Shi', ',17,192,', '河南省-鹤壁市', '17', '02', '03', '01', '192', '', null, null);
INSERT INTO t_sys_area VALUES ('193', '410700', '410700', '410700', '新乡市', 'Xinxiang Shi', 'Xinxiang Shi', ',17,193,', '河南省-新乡市', '17', '02', '03', '01', '193', '', null, null);
INSERT INTO t_sys_area VALUES ('194', '410800', '410800', '410800', '焦作市', 'Jiaozuo Shi', 'Jiaozuo Shi', ',17,194,', '河南省-焦作市', '17', '02', '03', '01', '194', '', null, null);
INSERT INTO t_sys_area VALUES ('195', '410900', '410900', '410900', '濮阳市', 'Puyang Shi', 'Puyang Shi', ',17,195,', '河南省-濮阳市', '17', '02', '03', '01', '195', '', null, null);
INSERT INTO t_sys_area VALUES ('196', '411000', '411000', '411000', '许昌市', 'Xuchang Shi', 'Xuchang Shi', ',17,196,', '河南省-许昌市', '17', '02', '03', '01', '196', '', null, null);
INSERT INTO t_sys_area VALUES ('197', '411100', '411100', '411100', '漯河市', 'Luohe Shi', 'Luohe Shi', ',17,197,', '河南省-漯河市', '17', '02', '03', '01', '197', '', null, null);
INSERT INTO t_sys_area VALUES ('198', '411200', '411200', '411200', '三门峡市', 'Sanmenxia Shi', 'Sanmenxia Shi', ',17,198,', '河南省-三门峡市', '17', '02', '03', '01', '198', '', null, null);
INSERT INTO t_sys_area VALUES ('199', '411300', '411300', '411300', '南阳市', 'Nanyang Shi', 'Nanyang Shi', ',17,199,', '河南省-南阳市', '17', '02', '03', '01', '199', '', null, null);
INSERT INTO t_sys_area VALUES ('200', '411400', '411400', '411400', '商丘市', 'Shangqiu Shi', 'Shangqiu Shi', ',17,200,', '河南省-商丘市', '17', '02', '03', '01', '200', '', null, null);
INSERT INTO t_sys_area VALUES ('201', '411500', '411500', '411500', '信阳市', 'Xinyang Shi', 'Xinyang Shi', ',17,201,', '河南省-信阳市', '17', '02', '03', '01', '201', '', null, null);
INSERT INTO t_sys_area VALUES ('202', '411600', '411600', '411600', '周口市', 'Zhoukou Shi', 'Zhoukou Shi', ',17,202,', '河南省-周口市', '17', '02', '03', '01', '202', '', null, null);
INSERT INTO t_sys_area VALUES ('203', '411700', '411700', '411700', '驻马店市', 'Zhumadian Shi', 'Zhumadian Shi', ',17,203,', '河南省-驻马店市', '17', '02', '03', '01', '203', '', null, null);
INSERT INTO t_sys_area VALUES ('204', '420100', '420100', '420100', '武汉市', 'Wuhan Shi', 'Wuhan Shi', ',18,204,', '湖北省-武汉市', '18', '02', '03', '01', '204', '', null, null);
INSERT INTO t_sys_area VALUES ('205', '420200', '420200', '420200', '黄石市', 'Huangshi Shi', 'Huangshi Shi', ',18,205,', '湖北省-黄石市', '18', '02', '03', '01', '205', '', null, null);
INSERT INTO t_sys_area VALUES ('206', '420300', '420300', '420300', '十堰市', 'Shiyan Shi', 'Shiyan Shi', ',18,206,', '湖北省-十堰市', '18', '02', '03', '01', '206', '', null, null);
INSERT INTO t_sys_area VALUES ('207', '420500', '420500', '420500', '宜昌市', 'Yichang Shi', 'Yichang Shi', ',18,207,', '湖北省-宜昌市', '18', '02', '03', '01', '207', '', null, null);
INSERT INTO t_sys_area VALUES ('208', '420600', '420600', '420600', '襄樊市', 'Xiangfan Shi', 'Xiangfan Shi', ',18,208,', '湖北省-襄樊市', '18', '02', '03', '01', '208', '', null, null);
INSERT INTO t_sys_area VALUES ('209', '420700', '420700', '420700', '鄂州市', 'Ezhou Shi', 'Ezhou Shi', ',18,209,', '湖北省-鄂州市', '18', '02', '03', '01', '209', '', null, null);
INSERT INTO t_sys_area VALUES ('210', '420800', '420800', '420800', '荆门市', 'Jingmen Shi', 'Jingmen Shi', ',18,210,', '湖北省-荆门市', '18', '02', '03', '01', '210', '', null, null);
INSERT INTO t_sys_area VALUES ('211', '420900', '420900', '420900', '孝感市', 'Xiaogan Shi', 'Xiaogan Shi', ',18,211,', '湖北省-孝感市', '18', '02', '03', '01', '211', '', null, null);
INSERT INTO t_sys_area VALUES ('212', '421000', '421000', '421000', '荆州市', 'Jingzhou Shi', 'Jingzhou Shi', ',18,212,', '湖北省-荆州市', '18', '02', '03', '01', '212', '', null, null);
INSERT INTO t_sys_area VALUES ('213', '421100', '421100', '421100', '黄冈市', 'Huanggang Shi', 'Huanggang Shi', ',18,213,', '湖北省-黄冈市', '18', '02', '03', '01', '213', '', null, null);
INSERT INTO t_sys_area VALUES ('214', '421200', '421200', '421200', '咸宁市', 'Xianning Xian', 'Xianning Xian', ',18,214,', '湖北省-咸宁市', '18', '02', '03', '01', '214', '', null, null);
INSERT INTO t_sys_area VALUES ('215', '421300', '421300', '421300', '随州市', 'Suizhou Shi', 'Suizhou Shi', ',18,215,', '湖北省-随州市', '18', '02', '03', '01', '215', '', null, null);
INSERT INTO t_sys_area VALUES ('216', '422800', '422800', '422800', '恩施土家族苗族自治州', 'Enshi Tujiazu Miaozu Zizhizhou', 'Enshi Tujiazu Miaozu Zizhizhou', ',18,216,', '湖北省-恩施土家族苗族自治州', '18', '02', '03', '01', '216', '', null, null);
INSERT INTO t_sys_area VALUES ('217', '429000', '429000', '429000', '省直辖县级行政区划', 'shengzhixiaxianjixingzhengquhua', 'shengzhixiaxianjixingzhengquhua', ',18,217,', '湖北省-省直辖县级行政区划', '18', '02', '03', '01', '217', '', null, null);
INSERT INTO t_sys_area VALUES ('218', '430100', '430100', '430100', '长沙市', 'Changsha Shi', 'Changsha Shi', ',19,218,', '湖南省-长沙市', '19', '02', '03', '01', '218', '', null, null);
INSERT INTO t_sys_area VALUES ('219', '430200', '430200', '430200', '株洲市', 'Zhuzhou Shi', 'Zhuzhou Shi', ',19,219,', '湖南省-株洲市', '19', '02', '03', '01', '219', '', null, null);
INSERT INTO t_sys_area VALUES ('220', '430300', '430300', '430300', '湘潭市', 'Xiangtan Shi', 'Xiangtan Shi', ',19,220,', '湖南省-湘潭市', '19', '02', '03', '01', '220', '', null, null);
INSERT INTO t_sys_area VALUES ('221', '430400', '430400', '430400', '衡阳市', 'Hengyang Shi', 'Hengyang Shi', ',19,221,', '湖南省-衡阳市', '19', '02', '03', '01', '221', '', null, null);
INSERT INTO t_sys_area VALUES ('222', '430500', '430500', '430500', '邵阳市', 'Shaoyang Shi', 'Shaoyang Shi', ',19,222,', '湖南省-邵阳市', '19', '02', '03', '01', '222', '', null, null);
INSERT INTO t_sys_area VALUES ('223', '430600', '430600', '430600', '岳阳市', 'Yueyang Shi', 'Yueyang Shi', ',19,223,', '湖南省-岳阳市', '19', '02', '03', '01', '223', '', null, null);
INSERT INTO t_sys_area VALUES ('224', '430700', '430700', '430700', '常德市', 'Changde Shi', 'Changde Shi', ',19,224,', '湖南省-常德市', '19', '02', '03', '01', '224', '', null, null);
INSERT INTO t_sys_area VALUES ('225', '430800', '430800', '430800', '张家界市', 'Zhangjiajie Shi', 'Zhangjiajie Shi', ',19,225,', '湖南省-张家界市', '19', '02', '03', '01', '225', '', null, null);
INSERT INTO t_sys_area VALUES ('226', '430900', '430900', '430900', '益阳市', 'Yiyang Shi', 'Yiyang Shi', ',19,226,', '湖南省-益阳市', '19', '02', '03', '01', '226', '', null, null);
INSERT INTO t_sys_area VALUES ('227', '431000', '431000', '431000', '郴州市', 'Chenzhou Shi', 'Chenzhou Shi', ',19,227,', '湖南省-郴州市', '19', '02', '03', '01', '227', '', null, null);
INSERT INTO t_sys_area VALUES ('228', '431100', '431100', '431100', '永州市', 'Yongzhou Shi', 'Yongzhou Shi', ',19,228,', '湖南省-永州市', '19', '02', '03', '01', '228', '', null, null);
INSERT INTO t_sys_area VALUES ('229', '431200', '431200', '431200', '怀化市', 'Huaihua Shi', 'Huaihua Shi', ',19,229,', '湖南省-怀化市', '19', '02', '03', '01', '229', '', null, null);
INSERT INTO t_sys_area VALUES ('230', '431300', '431300', '431300', '娄底市', 'Loudi Shi', 'Loudi Shi', ',19,230,', '湖南省-娄底市', '19', '02', '03', '01', '230', '', null, null);
INSERT INTO t_sys_area VALUES ('231', '433100', '433100', '433100', '湘西土家族苗族自治州', 'Xiangxi Tujiazu Miaozu Zizhizhou ', 'Xiangxi Tujiazu Miaozu Zizhizhou ', ',19,231,', '湖南省-湘西土家族苗族自治州', '19', '02', '03', '01', '231', '', null, null);
INSERT INTO t_sys_area VALUES ('232', '440100', '440100', '440100', '广州市', 'Guangzhou Shi', 'Guangzhou Shi', ',20,232,', '广东省-广州市', '20', '02', '03', '01', '232', '', null, null);
INSERT INTO t_sys_area VALUES ('233', '440200', '440200', '440200', '韶关市', 'Shaoguan Shi', 'Shaoguan Shi', ',20,233,', '广东省-韶关市', '20', '02', '03', '01', '233', '', null, null);
INSERT INTO t_sys_area VALUES ('234', '440300', '440300', '440300', '深圳市', 'Shenzhen Shi', 'Shenzhen Shi', ',20,234,', '广东省-深圳市', '20', '02', '03', '01', '234', '', null, null);
INSERT INTO t_sys_area VALUES ('235', '440400', '440400', '440400', '珠海市', 'Zhuhai Shi', 'Zhuhai Shi', ',20,235,', '广东省-珠海市', '20', '02', '03', '01', '235', '', null, null);
INSERT INTO t_sys_area VALUES ('236', '440500', '440500', '440500', '汕头市', 'Shantou Shi', 'Shantou Shi', ',20,236,', '广东省-汕头市', '20', '02', '03', '01', '236', '', null, null);
INSERT INTO t_sys_area VALUES ('237', '440600', '440600', '440600', '佛山市', 'Foshan Shi', 'Foshan Shi', ',20,237,', '广东省-佛山市', '20', '02', '03', '01', '237', '', null, null);
INSERT INTO t_sys_area VALUES ('238', '440700', '440700', '440700', '江门市', 'Jiangmen Shi', 'Jiangmen Shi', ',20,238,', '广东省-江门市', '20', '02', '03', '01', '238', '', null, null);
INSERT INTO t_sys_area VALUES ('239', '440800', '440800', '440800', '湛江市', 'Zhanjiang Shi', 'Zhanjiang Shi', ',20,239,', '广东省-湛江市', '20', '02', '03', '01', '239', '', null, null);
INSERT INTO t_sys_area VALUES ('240', '440900', '440900', '440900', '茂名市', 'Maoming Shi', 'Maoming Shi', ',20,240,', '广东省-茂名市', '20', '02', '03', '01', '240', '', null, null);
INSERT INTO t_sys_area VALUES ('241', '441200', '441200', '441200', '肇庆市', 'Zhaoqing Shi', 'Zhaoqing Shi', ',20,241,', '广东省-肇庆市', '20', '02', '03', '01', '241', '', null, null);
INSERT INTO t_sys_area VALUES ('242', '441300', '441300', '441300', '惠州市', 'Huizhou Shi', 'Huizhou Shi', ',20,242,', '广东省-惠州市', '20', '02', '03', '01', '242', '', null, null);
INSERT INTO t_sys_area VALUES ('243', '441400', '441400', '441400', '梅州市', 'Meizhou Shi', 'Meizhou Shi', ',20,243,', '广东省-梅州市', '20', '02', '03', '01', '243', '', null, null);
INSERT INTO t_sys_area VALUES ('244', '441500', '441500', '441500', '汕尾市', 'Shanwei Shi', 'Shanwei Shi', ',20,244,', '广东省-汕尾市', '20', '02', '03', '01', '244', '', null, null);
INSERT INTO t_sys_area VALUES ('245', '441600', '441600', '441600', '河源市', 'Heyuan Shi', 'Heyuan Shi', ',20,245,', '广东省-河源市', '20', '02', '03', '01', '245', '', null, null);
INSERT INTO t_sys_area VALUES ('246', '441700', '441700', '441700', '阳江市', 'Yangjiang Shi', 'Yangjiang Shi', ',20,246,', '广东省-阳江市', '20', '02', '03', '01', '246', '', null, null);
INSERT INTO t_sys_area VALUES ('247', '441800', '441800', '441800', '清远市', 'Qingyuan Shi', 'Qingyuan Shi', ',20,247,', '广东省-清远市', '20', '02', '03', '01', '247', '', null, null);
INSERT INTO t_sys_area VALUES ('248', '441900', '441900', '441900', '东莞市', 'Dongguan Shi', 'Dongguan Shi', ',20,248,', '广东省-东莞市', '20', '02', '03', '01', '248', '', null, null);
INSERT INTO t_sys_area VALUES ('249', '442000', '442000', '442000', '中山市', 'Zhongshan Shi', 'Zhongshan Shi', ',20,249,', '广东省-中山市', '20', '02', '03', '01', '249', '', null, null);
INSERT INTO t_sys_area VALUES ('250', '445100', '445100', '445100', '潮州市', 'Chaozhou Shi', 'Chaozhou Shi', ',20,250,', '广东省-潮州市', '20', '02', '03', '01', '250', '', null, null);
INSERT INTO t_sys_area VALUES ('251', '445200', '445200', '445200', '揭阳市', 'Jieyang Shi', 'Jieyang Shi', ',20,251,', '广东省-揭阳市', '20', '02', '03', '01', '251', '', null, null);
INSERT INTO t_sys_area VALUES ('252', '445300', '445300', '445300', '云浮市', 'Yunfu Shi', 'Yunfu Shi', ',20,252,', '广东省-云浮市', '20', '02', '03', '01', '252', '', null, null);
INSERT INTO t_sys_area VALUES ('253', '450100', '450100', '450100', '南宁市', 'Nanning Shi', 'Nanning Shi', ',21,253,', '广西壮族自治区-南宁市', '21', '02', '03', '01', '253', '', null, null);
INSERT INTO t_sys_area VALUES ('254', '450200', '450200', '450200', '柳州市', 'Liuzhou Shi', 'Liuzhou Shi', ',21,254,', '广西壮族自治区-柳州市', '21', '02', '03', '01', '254', '', null, null);
INSERT INTO t_sys_area VALUES ('255', '450300', '450300', '450300', '桂林市', 'Guilin Shi', 'Guilin Shi', ',21,255,', '广西壮族自治区-桂林市', '21', '02', '03', '01', '255', '', null, null);
INSERT INTO t_sys_area VALUES ('256', '450400', '450400', '450400', '梧州市', 'Wuzhou Shi', 'Wuzhou Shi', ',21,256,', '广西壮族自治区-梧州市', '21', '02', '03', '01', '256', '', null, null);
INSERT INTO t_sys_area VALUES ('257', '450500', '450500', '450500', '北海市', 'Beihai Shi', 'Beihai Shi', ',21,257,', '广西壮族自治区-北海市', '21', '02', '03', '01', '257', '', null, null);
INSERT INTO t_sys_area VALUES ('258', '450600', '450600', '450600', '防城港市', 'Fangchenggang Shi', 'Fangchenggang Shi', ',21,258,', '广西壮族自治区-防城港市', '21', '02', '03', '01', '258', '', null, null);
INSERT INTO t_sys_area VALUES ('259', '450700', '450700', '450700', '钦州市', 'Qinzhou Shi', 'Qinzhou Shi', ',21,259,', '广西壮族自治区-钦州市', '21', '02', '03', '01', '259', '', null, null);
INSERT INTO t_sys_area VALUES ('260', '450800', '450800', '450800', '贵港市', 'Guigang Shi', 'Guigang Shi', ',21,260,', '广西壮族自治区-贵港市', '21', '02', '03', '01', '260', '', null, null);
INSERT INTO t_sys_area VALUES ('261', '450900', '450900', '450900', '玉林市', 'Yulin Shi', 'Yulin Shi', ',21,261,', '广西壮族自治区-玉林市', '21', '02', '03', '01', '261', '', null, null);
INSERT INTO t_sys_area VALUES ('262', '451000', '451000', '451000', '百色市', 'Baise Shi', 'Baise Shi', ',21,262,', '广西壮族自治区-百色市', '21', '02', '03', '01', '262', '', null, null);
INSERT INTO t_sys_area VALUES ('263', '451100', '451100', '451100', '贺州市', 'Hezhou Shi', 'Hezhou Shi', ',21,263,', '广西壮族自治区-贺州市', '21', '02', '03', '01', '263', '', null, null);
INSERT INTO t_sys_area VALUES ('264', '451200', '451200', '451200', '河池市', 'Hechi Shi', 'Hechi Shi', ',21,264,', '广西壮族自治区-河池市', '21', '02', '03', '01', '264', '', null, null);
INSERT INTO t_sys_area VALUES ('265', '451300', '451300', '451300', '来宾市', 'Laibin Shi', 'Laibin Shi', ',21,265,', '广西壮族自治区-来宾市', '21', '02', '03', '01', '265', '', null, null);
INSERT INTO t_sys_area VALUES ('266', '451400', '451400', '451400', '崇左市', 'Chongzuo Shi', 'Chongzuo Shi', ',21,266,', '广西壮族自治区-崇左市', '21', '02', '03', '01', '266', '', null, null);
INSERT INTO t_sys_area VALUES ('267', '460100', '460100', '460100', '海口市', 'Haikou Shi', 'Haikou Shi', ',22,267,', '海南省-海口市', '22', '02', '03', '01', '267', '', null, null);
INSERT INTO t_sys_area VALUES ('268', '460200', '460200', '460200', '三亚市', 'Sanya Shi', 'Sanya Shi', ',22,268,', '海南省-三亚市', '22', '02', '03', '01', '268', '', null, null);
INSERT INTO t_sys_area VALUES ('269', '469000', '469000', '469000', '省直辖县级行政区划', 'shengzhixiaxianjixingzhengquhua', 'shengzhixiaxianjixingzhengquhua', ',22,269,', '海南省-省直辖县级行政区划', '22', '02', '03', '01', '269', '', null, null);
INSERT INTO t_sys_area VALUES ('270', '500100', '500100', '500100', '重庆市', 'Shixiaqu', 'Shixiaqu', ',23,270,', '重庆-重庆市', '23', '02', '03', '01', '270', '', null, null);
INSERT INTO t_sys_area VALUES ('273', '510100', '510100', '510100', '成都市', 'Chengdu Shi', 'Chengdu Shi', ',24,273,', '四川省-成都市', '24', '02', '03', '01', '273', '', null, null);
INSERT INTO t_sys_area VALUES ('274', '510300', '510300', '510300', '自贡市', 'Zigong Shi', 'Zigong Shi', ',24,274,', '四川省-自贡市', '24', '02', '03', '01', '274', '', null, null);
INSERT INTO t_sys_area VALUES ('275', '510400', '510400', '510400', '攀枝花市', 'Panzhihua Shi', 'Panzhihua Shi', ',24,275,', '四川省-攀枝花市', '24', '02', '03', '01', '275', '', null, null);
INSERT INTO t_sys_area VALUES ('276', '510500', '510500', '510500', '泸州市', 'Luzhou Shi', 'Luzhou Shi', ',24,276,', '四川省-泸州市', '24', '02', '03', '01', '276', '', null, null);
INSERT INTO t_sys_area VALUES ('277', '510600', '510600', '510600', '德阳市', 'Deyang Shi', 'Deyang Shi', ',24,277,', '四川省-德阳市', '24', '02', '03', '01', '277', '', null, null);
INSERT INTO t_sys_area VALUES ('278', '510700', '510700', '510700', '绵阳市', 'Mianyang Shi', 'Mianyang Shi', ',24,278,', '四川省-绵阳市', '24', '02', '03', '01', '278', '', null, null);
INSERT INTO t_sys_area VALUES ('279', '510800', '510800', '510800', '广元市', 'Guangyuan Shi', 'Guangyuan Shi', ',24,279,', '四川省-广元市', '24', '02', '03', '01', '279', '', null, null);
INSERT INTO t_sys_area VALUES ('280', '510900', '510900', '510900', '遂宁市', 'Suining Shi', 'Suining Shi', ',24,280,', '四川省-遂宁市', '24', '02', '03', '01', '280', '', null, null);
INSERT INTO t_sys_area VALUES ('281', '511000', '511000', '511000', '内江市', 'Neijiang Shi', 'Neijiang Shi', ',24,281,', '四川省-内江市', '24', '02', '03', '01', '281', '', null, null);
INSERT INTO t_sys_area VALUES ('282', '511100', '511100', '511100', '乐山市', 'Leshan Shi', 'Leshan Shi', ',24,282,', '四川省-乐山市', '24', '02', '03', '01', '282', '', null, null);
INSERT INTO t_sys_area VALUES ('283', '511300', '511300', '511300', '南充市', 'Nanchong Shi', 'Nanchong Shi', ',24,283,', '四川省-南充市', '24', '02', '03', '01', '283', '', null, null);
INSERT INTO t_sys_area VALUES ('284', '511400', '511400', '511400', '眉山市', 'Meishan Shi', 'Meishan Shi', ',24,284,', '四川省-眉山市', '24', '02', '03', '01', '284', '', null, null);
INSERT INTO t_sys_area VALUES ('285', '511500', '511500', '511500', '宜宾市', 'Yibin Shi', 'Yibin Shi', ',24,285,', '四川省-宜宾市', '24', '02', '03', '01', '285', '', null, null);
INSERT INTO t_sys_area VALUES ('286', '511600', '511600', '511600', '广安市', 'Guang,an Shi', 'Guang,an Shi', ',24,286,', '四川省-广安市', '24', '02', '03', '01', '286', '', null, null);
INSERT INTO t_sys_area VALUES ('287', '511700', '511700', '511700', '达州市', 'Dazhou Shi', 'Dazhou Shi', ',24,287,', '四川省-达州市', '24', '02', '03', '01', '287', '', null, null);
INSERT INTO t_sys_area VALUES ('288', '511800', '511800', '511800', '雅安市', 'Ya,an Shi', 'Ya,an Shi', ',24,288,', '四川省-雅安市', '24', '02', '03', '01', '288', '', null, null);
INSERT INTO t_sys_area VALUES ('289', '511900', '511900', '511900', '巴中市', 'Bazhong Shi', 'Bazhong Shi', ',24,289,', '四川省-巴中市', '24', '02', '03', '01', '289', '', null, null);
INSERT INTO t_sys_area VALUES ('290', '512000', '512000', '512000', '资阳市', 'Ziyang Shi', 'Ziyang Shi', ',24,290,', '四川省-资阳市', '24', '02', '03', '01', '290', '', null, null);
INSERT INTO t_sys_area VALUES ('291', '513200', '513200', '513200', '阿坝藏族羌族自治州', 'Aba(Ngawa) Zangzu Qiangzu Zizhizhou', 'Aba(Ngawa) Zangzu Qiangzu Zizhizhou', ',24,291,', '四川省-阿坝藏族羌族自治州', '24', '02', '03', '01', '291', '', null, null);
INSERT INTO t_sys_area VALUES ('292', '513300', '513300', '513300', '甘孜藏族自治州', 'Garze Zangzu Zizhizhou', 'Garze Zangzu Zizhizhou', ',24,292,', '四川省-甘孜藏族自治州', '24', '02', '03', '01', '292', '', null, null);
INSERT INTO t_sys_area VALUES ('293', '513400', '513400', '513400', '凉山彝族自治州', 'Liangshan Yizu Zizhizhou', 'Liangshan Yizu Zizhizhou', ',24,293,', '四川省-凉山彝族自治州', '24', '02', '03', '01', '293', '', null, null);
INSERT INTO t_sys_area VALUES ('294', '520100', '520100', '520100', '贵阳市', 'Guiyang Shi', 'Guiyang Shi', ',25,294,', '贵州省-贵阳市', '25', '02', '03', '01', '294', '', null, null);
INSERT INTO t_sys_area VALUES ('295', '520200', '520200', '520200', '六盘水市', 'Liupanshui Shi', 'Liupanshui Shi', ',25,295,', '贵州省-六盘水市', '25', '02', '03', '01', '295', '', null, null);
INSERT INTO t_sys_area VALUES ('296', '520300', '520300', '520300', '遵义市', 'Zunyi Shi', 'Zunyi Shi', ',25,296,', '贵州省-遵义市', '25', '02', '03', '01', '296', '', null, null);
INSERT INTO t_sys_area VALUES ('297', '520400', '520400', '520400', '安顺市', 'Anshun Xian', 'Anshun Xian', ',25,297,', '贵州省-安顺市', '25', '02', '03', '01', '297', '', null, null);
INSERT INTO t_sys_area VALUES ('298', '522200', '522200', '522200', '铜仁地区', 'Tongren Diqu', 'Tongren Diqu', ',25,298,', '贵州省-铜仁地区', '25', '02', '03', '01', '298', '', null, null);
INSERT INTO t_sys_area VALUES ('299', '522300', '522300', '522300', '黔西南布依族苗族自治州', 'Qianxinan Buyeizu Zizhizhou', 'Qianxinan Buyeizu Zizhizhou', ',25,299,', '贵州省-黔西南布依族苗族自治州', '25', '02', '03', '01', '299', '', null, null);
INSERT INTO t_sys_area VALUES ('300', '522400', '522400', '522400', '毕节地区', 'Bijie Diqu', 'Bijie Diqu', ',25,300,', '贵州省-毕节地区', '25', '02', '03', '01', '300', '', null, null);
INSERT INTO t_sys_area VALUES ('301', '522600', '522600', '522600', '黔东南苗族侗族自治州', 'Qiandongnan Miaozu Dongzu Zizhizhou', 'Qiandongnan Miaozu Dongzu Zizhizhou', ',25,301,', '贵州省-黔东南苗族侗族自治州', '25', '02', '03', '01', '301', '', null, null);
INSERT INTO t_sys_area VALUES ('302', '522700', '522700', '522700', '黔南布依族苗族自治州', 'Qiannan Buyeizu Miaozu Zizhizhou', 'Qiannan Buyeizu Miaozu Zizhizhou', ',25,302,', '贵州省-黔南布依族苗族自治州', '25', '02', '03', '01', '302', '', null, null);
INSERT INTO t_sys_area VALUES ('303', '530100', '530100', '530100', '昆明市', 'Kunming Shi', 'Kunming Shi', ',26,303,', '云南省-昆明市', '26', '02', '03', '01', '303', '', null, null);
INSERT INTO t_sys_area VALUES ('304', '530300', '530300', '530300', '曲靖市', 'Qujing Shi', 'Qujing Shi', ',26,304,', '云南省-曲靖市', '26', '02', '03', '01', '304', '', null, null);
INSERT INTO t_sys_area VALUES ('305', '530400', '530400', '530400', '玉溪市', 'Yuxi Shi', 'Yuxi Shi', ',26,305,', '云南省-玉溪市', '26', '02', '03', '01', '305', '', null, null);
INSERT INTO t_sys_area VALUES ('306', '530500', '530500', '530500', '保山市', 'Baoshan Shi', 'Baoshan Shi', ',26,306,', '云南省-保山市', '26', '02', '03', '01', '306', '', null, null);
INSERT INTO t_sys_area VALUES ('307', '530600', '530600', '530600', '昭通市', 'Zhaotong Shi', 'Zhaotong Shi', ',26,307,', '云南省-昭通市', '26', '02', '03', '01', '307', '', null, null);
INSERT INTO t_sys_area VALUES ('308', '530700', '530700', '530700', '丽江市', 'Lijiang Shi', 'Lijiang Shi', ',26,308,', '云南省-丽江市', '26', '02', '03', '01', '308', '', null, null);
INSERT INTO t_sys_area VALUES ('309', '530800', '530800', '530800', '普洱市', 'Simao Shi', 'Simao Shi', ',26,309,', '云南省-普洱市', '26', '02', '03', '01', '309', '', null, null);
INSERT INTO t_sys_area VALUES ('310', '530900', '530900', '530900', '临沧市', 'Lincang Shi', 'Lincang Shi', ',26,310,', '云南省-临沧市', '26', '02', '03', '01', '310', '', null, null);
INSERT INTO t_sys_area VALUES ('311', '532300', '532300', '532300', '楚雄彝族自治州', 'Chuxiong Yizu Zizhizhou', 'Chuxiong Yizu Zizhizhou', ',26,311,', '云南省-楚雄彝族自治州', '26', '02', '03', '01', '311', '', null, null);
INSERT INTO t_sys_area VALUES ('312', '532500', '532500', '532500', '红河哈尼族彝族自治州', 'Honghe Hanizu Yizu Zizhizhou', 'Honghe Hanizu Yizu Zizhizhou', ',26,312,', '云南省-红河哈尼族彝族自治州', '26', '02', '03', '01', '312', '', null, null);
INSERT INTO t_sys_area VALUES ('313', '532600', '532600', '532600', '文山壮族苗族自治州', 'Wenshan Zhuangzu Miaozu Zizhizhou', 'Wenshan Zhuangzu Miaozu Zizhizhou', ',26,313,', '云南省-文山壮族苗族自治州', '26', '02', '03', '01', '313', '', null, null);
INSERT INTO t_sys_area VALUES ('314', '532800', '532800', '532800', '西双版纳傣族自治州', 'Xishuangbanna Daizu Zizhizhou', 'Xishuangbanna Daizu Zizhizhou', ',26,314,', '云南省-西双版纳傣族自治州', '26', '02', '03', '01', '314', '', null, null);
INSERT INTO t_sys_area VALUES ('315', '532900', '532900', '532900', '大理白族自治州', 'Dali Baizu Zizhizhou', 'Dali Baizu Zizhizhou', ',26,315,', '云南省-大理白族自治州', '26', '02', '03', '01', '315', '', null, null);
INSERT INTO t_sys_area VALUES ('316', '533100', '533100', '533100', '德宏傣族景颇族自治州', 'Dehong Daizu Jingpozu Zizhizhou', 'Dehong Daizu Jingpozu Zizhizhou', ',26,316,', '云南省-德宏傣族景颇族自治州', '26', '02', '03', '01', '316', '', null, null);
INSERT INTO t_sys_area VALUES ('317', '533300', '533300', '533300', '怒江傈僳族自治州', 'Nujiang Lisuzu Zizhizhou', 'Nujiang Lisuzu Zizhizhou', ',26,317,', '云南省-怒江傈僳族自治州', '26', '02', '03', '01', '317', '', null, null);
INSERT INTO t_sys_area VALUES ('318', '533400', '533400', '533400', '迪庆藏族自治州', 'Deqen Zangzu Zizhizhou', 'Deqen Zangzu Zizhizhou', ',26,318,', '云南省-迪庆藏族自治州', '26', '02', '03', '01', '318', '', null, null);
INSERT INTO t_sys_area VALUES ('319', '540100', '540100', '540100', '拉萨市', 'Lhasa Shi', 'Lhasa Shi', ',27,319,', '西藏自治区-拉萨市', '27', '02', '03', '01', '319', '', null, null);
INSERT INTO t_sys_area VALUES ('320', '542100', '542100', '542100', '昌都地区', 'Qamdo Diqu', 'Qamdo Diqu', ',27,320,', '西藏自治区-昌都地区', '27', '02', '03', '01', '320', '', null, null);
INSERT INTO t_sys_area VALUES ('321', '542200', '542200', '542200', '山南地区', 'Shannan Diqu', 'Shannan Diqu', ',27,321,', '西藏自治区-山南地区', '27', '02', '03', '01', '321', '', null, null);
INSERT INTO t_sys_area VALUES ('322', '542300', '542300', '542300', '日喀则地区', 'Xigaze Diqu', 'Xigaze Diqu', ',27,322,', '西藏自治区-日喀则地区', '27', '02', '03', '01', '322', '', null, null);
INSERT INTO t_sys_area VALUES ('323', '542400', '542400', '542400', '那曲地区', 'Nagqu Diqu', 'Nagqu Diqu', ',27,323,', '西藏自治区-那曲地区', '27', '02', '03', '01', '323', '', null, null);
INSERT INTO t_sys_area VALUES ('324', '542500', '542500', '542500', '阿里地区', 'Ngari Diqu', 'Ngari Diqu', ',27,324,', '西藏自治区-阿里地区', '27', '02', '03', '01', '324', '', null, null);
INSERT INTO t_sys_area VALUES ('325', '542600', '542600', '542600', '林芝地区', 'Nyingchi Diqu', 'Nyingchi Diqu', ',27,325,', '西藏自治区-林芝地区', '27', '02', '03', '01', '325', '', null, null);
INSERT INTO t_sys_area VALUES ('326', '610100', '610100', '610100', '西安市', 'Xi,an Shi', 'Xi,an Shi', ',28,326,', '陕西省-西安市', '28', '02', '03', '01', '326', '', null, null);
INSERT INTO t_sys_area VALUES ('327', '610200', '610200', '610200', '铜川市', 'Tongchuan Shi', 'Tongchuan Shi', ',28,327,', '陕西省-铜川市', '28', '02', '03', '01', '327', '', null, null);
INSERT INTO t_sys_area VALUES ('328', '610300', '610300', '610300', '宝鸡市', 'Baoji Shi', 'Baoji Shi', ',28,328,', '陕西省-宝鸡市', '28', '02', '03', '01', '328', '', null, null);
INSERT INTO t_sys_area VALUES ('329', '610400', '610400', '610400', '咸阳市', 'Xianyang Shi', 'Xianyang Shi', ',28,329,', '陕西省-咸阳市', '28', '02', '03', '01', '329', '', null, null);
INSERT INTO t_sys_area VALUES ('330', '610500', '610500', '610500', '渭南市', 'Weinan Shi', 'Weinan Shi', ',28,330,', '陕西省-渭南市', '28', '02', '03', '01', '330', '', null, null);
INSERT INTO t_sys_area VALUES ('331', '610600', '610600', '610600', '延安市', 'Yan,an Shi', 'Yan,an Shi', ',28,331,', '陕西省-延安市', '28', '02', '03', '01', '331', '', null, null);
INSERT INTO t_sys_area VALUES ('332', '610700', '610700', '610700', '汉中市', 'Hanzhong Shi', 'Hanzhong Shi', ',28,332,', '陕西省-汉中市', '28', '02', '03', '01', '332', '', null, null);
INSERT INTO t_sys_area VALUES ('333', '610800', '610800', '610800', '榆林市', 'Yulin Shi', 'Yulin Shi', ',28,333,', '陕西省-榆林市', '28', '02', '03', '01', '333', '', null, null);
INSERT INTO t_sys_area VALUES ('334', '610900', '610900', '610900', '安康市', 'Ankang Shi', 'Ankang Shi', ',28,334,', '陕西省-安康市', '28', '02', '03', '01', '334', '', null, null);
INSERT INTO t_sys_area VALUES ('335', '611000', '611000', '611000', '商洛市', 'Shangluo Shi', 'Shangluo Shi', ',28,335,', '陕西省-商洛市', '28', '02', '03', '01', '335', '', null, null);
INSERT INTO t_sys_area VALUES ('336', '620100', '620100', '620100', '兰州市', 'Lanzhou Shi', 'Lanzhou Shi', ',29,336,', '甘肃省-兰州市', '29', '02', '03', '01', '336', '', null, null);
INSERT INTO t_sys_area VALUES ('337', '620200', '620200', '620200', '嘉峪关市', 'Jiayuguan Shi', 'Jiayuguan Shi', ',29,337,', '甘肃省-嘉峪关市', '29', '02', '03', '01', '337', '', null, null);
INSERT INTO t_sys_area VALUES ('338', '620300', '620300', '620300', '金昌市', 'Jinchang Shi', 'Jinchang Shi', ',29,338,', '甘肃省-金昌市', '29', '02', '03', '01', '338', '', null, null);
INSERT INTO t_sys_area VALUES ('339', '620400', '620400', '620400', '白银市', 'Baiyin Shi', 'Baiyin Shi', ',29,339,', '甘肃省-白银市', '29', '02', '03', '01', '339', '', null, null);
INSERT INTO t_sys_area VALUES ('340', '620500', '620500', '620500', '天水市', 'Tianshui Shi', 'Tianshui Shi', ',29,340,', '甘肃省-天水市', '29', '02', '03', '01', '340', '', null, null);
INSERT INTO t_sys_area VALUES ('341', '620600', '620600', '620600', '武威市', 'Wuwei Shi', 'Wuwei Shi', ',29,341,', '甘肃省-武威市', '29', '02', '03', '01', '341', '', null, null);
INSERT INTO t_sys_area VALUES ('342', '620700', '620700', '620700', '张掖市', 'Zhangye Shi', 'Zhangye Shi', ',29,342,', '甘肃省-张掖市', '29', '02', '03', '01', '342', '', null, null);
INSERT INTO t_sys_area VALUES ('343', '620800', '620800', '620800', '平凉市', 'Pingliang Shi', 'Pingliang Shi', ',29,343,', '甘肃省-平凉市', '29', '02', '03', '01', '343', '', null, null);
INSERT INTO t_sys_area VALUES ('344', '620900', '620900', '620900', '酒泉市', 'Jiuquan Shi', 'Jiuquan Shi', ',29,344,', '甘肃省-酒泉市', '29', '02', '03', '01', '344', '', null, null);
INSERT INTO t_sys_area VALUES ('345', '621000', '621000', '621000', '庆阳市', 'Qingyang Shi', 'Qingyang Shi', ',29,345,', '甘肃省-庆阳市', '29', '02', '03', '01', '345', '', null, null);
INSERT INTO t_sys_area VALUES ('346', '621100', '621100', '621100', '定西市', 'Dingxi Shi', 'Dingxi Shi', ',29,346,', '甘肃省-定西市', '29', '02', '03', '01', '346', '', null, null);
INSERT INTO t_sys_area VALUES ('347', '621200', '621200', '621200', '陇南市', 'Longnan Shi', 'Longnan Shi', ',29,347,', '甘肃省-陇南市', '29', '02', '03', '01', '347', '', null, null);
INSERT INTO t_sys_area VALUES ('348', '622900', '622900', '622900', '临夏回族自治州', 'Linxia Huizu Zizhizhou ', 'Linxia Huizu Zizhizhou ', ',29,348,', '甘肃省-临夏回族自治州', '29', '02', '03', '01', '348', '', null, null);
INSERT INTO t_sys_area VALUES ('349', '623000', '623000', '623000', '甘南藏族自治州', 'Gannan Zangzu Zizhizhou', 'Gannan Zangzu Zizhizhou', ',29,349,', '甘肃省-甘南藏族自治州', '29', '02', '03', '01', '349', '', null, null);
INSERT INTO t_sys_area VALUES ('350', '630100', '630100', '630100', '西宁市', 'Xining Shi', 'Xining Shi', ',30,350,', '青海省-西宁市', '30', '02', '03', '01', '350', '', null, null);
INSERT INTO t_sys_area VALUES ('351', '632100', '632100', '632100', '海东地区', 'Haidong Diqu', 'Haidong Diqu', ',30,351,', '青海省-海东地区', '30', '02', '03', '01', '351', '', null, null);
INSERT INTO t_sys_area VALUES ('352', '632200', '632200', '632200', '海北藏族自治州', 'Haibei Zangzu Zizhizhou', 'Haibei Zangzu Zizhizhou', ',30,352,', '青海省-海北藏族自治州', '30', '02', '03', '01', '352', '', null, null);
INSERT INTO t_sys_area VALUES ('353', '632300', '632300', '632300', '黄南藏族自治州', 'Huangnan Zangzu Zizhizhou', 'Huangnan Zangzu Zizhizhou', ',30,353,', '青海省-黄南藏族自治州', '30', '02', '03', '01', '353', '', null, null);
INSERT INTO t_sys_area VALUES ('354', '632500', '632500', '632500', '海南藏族自治州', 'Hainan Zangzu Zizhizhou', 'Hainan Zangzu Zizhizhou', ',30,354,', '青海省-海南藏族自治州', '30', '02', '03', '01', '354', '', null, null);
INSERT INTO t_sys_area VALUES ('355', '632600', '632600', '632600', '果洛藏族自治州', 'Golog Zangzu Zizhizhou', 'Golog Zangzu Zizhizhou', ',30,355,', '青海省-果洛藏族自治州', '30', '02', '03', '01', '355', '', null, null);
INSERT INTO t_sys_area VALUES ('356', '632700', '632700', '632700', '玉树藏族自治州', 'Yushu Zangzu Zizhizhou', 'Yushu Zangzu Zizhizhou', ',30,356,', '青海省-玉树藏族自治州', '30', '02', '03', '01', '356', '', null, null);
INSERT INTO t_sys_area VALUES ('357', '632800', '632800', '632800', '海西蒙古族藏族自治州', 'Haixi Mongolzu Zangzu Zizhizhou', 'Haixi Mongolzu Zangzu Zizhizhou', ',30,357,', '青海省-海西蒙古族藏族自治州', '30', '02', '03', '01', '357', '', null, null);
INSERT INTO t_sys_area VALUES ('358', '640100', '640100', '640100', '银川市', 'Yinchuan Shi', 'Yinchuan Shi', ',31,358,', '宁夏回族自治区-银川市', '31', '02', '03', '01', '358', '', null, null);
INSERT INTO t_sys_area VALUES ('359', '640200', '640200', '640200', '石嘴山市', 'Shizuishan Shi', 'Shizuishan Shi', ',31,359,', '宁夏回族自治区-石嘴山市', '31', '02', '03', '01', '359', '', null, null);
INSERT INTO t_sys_area VALUES ('360', '640300', '640300', '640300', '吴忠市', 'Wuzhong Shi', 'Wuzhong Shi', ',31,360,', '宁夏回族自治区-吴忠市', '31', '02', '03', '01', '360', '', null, null);
INSERT INTO t_sys_area VALUES ('361', '640400', '640400', '640400', '固原市', 'Guyuan Shi', 'Guyuan Shi', ',31,361,', '宁夏回族自治区-固原市', '31', '02', '03', '01', '361', '', null, null);
INSERT INTO t_sys_area VALUES ('362', '640500', '640500', '640500', '中卫市', 'Zhongwei Shi', 'Zhongwei Shi', ',31,362,', '宁夏回族自治区-中卫市', '31', '02', '03', '01', '362', '', null, null);
INSERT INTO t_sys_area VALUES ('363', '650100', '650100', '650100', '乌鲁木齐市', 'Urumqi Shi', 'Urumqi Shi', ',32,363,', '新疆维吾尔自治区-乌鲁木齐市', '32', '02', '03', '01', '363', '', null, null);
INSERT INTO t_sys_area VALUES ('364', '650200', '650200', '650200', '克拉玛依市', 'Karamay Shi', 'Karamay Shi', ',32,364,', '新疆维吾尔自治区-克拉玛依市', '32', '02', '03', '01', '364', '', null, null);
INSERT INTO t_sys_area VALUES ('365', '652100', '652100', '652100', '吐鲁番地区', 'Turpan Diqu', 'Turpan Diqu', ',32,365,', '新疆维吾尔自治区-吐鲁番地区', '32', '02', '03', '01', '365', '', null, null);
INSERT INTO t_sys_area VALUES ('366', '652200', '652200', '652200', '哈密地区', 'Hami(kumul) Diqu', 'Hami(kumul) Diqu', ',32,366,', '新疆维吾尔自治区-哈密地区', '32', '02', '03', '01', '366', '', null, null);
INSERT INTO t_sys_area VALUES ('367', '652300', '652300', '652300', '昌吉回族自治州', 'Changji Huizu Zizhizhou', 'Changji Huizu Zizhizhou', ',32,367,', '新疆维吾尔自治区-昌吉回族自治州', '32', '02', '03', '01', '367', '', null, null);
INSERT INTO t_sys_area VALUES ('368', '652700', '652700', '652700', '博尔塔拉蒙古自治州', 'Bortala Monglo Zizhizhou', 'Bortala Monglo Zizhizhou', ',32,368,', '新疆维吾尔自治区-博尔塔拉蒙古自治州', '32', '02', '03', '01', '368', '', null, null);
INSERT INTO t_sys_area VALUES ('369', '652800', '652800', '652800', '巴音郭楞蒙古自治州', 'bayinguolengmengguzizhizhou', 'bayinguolengmengguzizhizhou', ',32,369,', '新疆维吾尔自治区-巴音郭楞蒙古自治州', '32', '02', '03', '01', '369', '', null, null);
INSERT INTO t_sys_area VALUES ('370', '652900', '652900', '652900', '阿克苏地区', 'Aksu Diqu', 'Aksu Diqu', ',32,370,', '新疆维吾尔自治区-阿克苏地区', '32', '02', '03', '01', '370', '', null, null);
INSERT INTO t_sys_area VALUES ('371', '653000', '653000', '653000', '克孜勒苏柯尔克孜自治州', 'Kizilsu Kirgiz Zizhizhou', 'Kizilsu Kirgiz Zizhizhou', ',32,371,', '新疆维吾尔自治区-克孜勒苏柯尔克孜自治州', '32', '02', '03', '01', '371', '', null, null);
INSERT INTO t_sys_area VALUES ('372', '653100', '653100', '653100', '喀什地区', 'Kashi(Kaxgar) Diqu', 'Kashi(Kaxgar) Diqu', ',32,372,', '新疆维吾尔自治区-喀什地区', '32', '02', '03', '01', '372', '', null, null);
INSERT INTO t_sys_area VALUES ('373', '653200', '653200', '653200', '和田地区', 'Hotan Diqu', 'Hotan Diqu', ',32,373,', '新疆维吾尔自治区-和田地区', '32', '02', '03', '01', '373', '', null, null);
INSERT INTO t_sys_area VALUES ('374', '654000', '654000', '654000', '伊犁哈萨克自治州', 'Ili Kazak Zizhizhou', 'Ili Kazak Zizhizhou', ',32,374,', '新疆维吾尔自治区-伊犁哈萨克自治州', '32', '02', '03', '01', '374', '', null, null);
INSERT INTO t_sys_area VALUES ('375', '654200', '654200', '654200', '塔城地区', 'Tacheng(Qoqek) Diqu', 'Tacheng(Qoqek) Diqu', ',32,375,', '新疆维吾尔自治区-塔城地区', '32', '02', '03', '01', '375', '', null, null);
INSERT INTO t_sys_area VALUES ('376', '654300', '654300', '654300', '阿勒泰地区', 'Altay Diqu', 'Altay Diqu', ',32,376,', '新疆维吾尔自治区-阿勒泰地区', '32', '02', '03', '01', '376', '', null, null);
INSERT INTO t_sys_area VALUES ('377', '659000', '659000', '659000', '自治区直辖县级行政区划', 'zizhiquzhixiaxianjixingzhengquhua', 'zizhiquzhixiaxianjixingzhengquhua', ',32,377,', '新疆维吾尔自治区-自治区直辖县级行政区划', '32', '02', '03', '01', '377', '', null, null);
INSERT INTO t_sys_area VALUES ('378', '110101', '110101', '110101', '东城区', 'Dongcheng Qu', 'Dongcheng Qu', ',2,33,378,', '北京-北京市-东城区', '33', '02', '04', '01', '378', '', null, null);
INSERT INTO t_sys_area VALUES ('379', '110102', '110102', '110102', '西城区', 'Xicheng Qu', 'Xicheng Qu', ',2,33,379,', '北京-北京市-西城区', '33', '02', '04', '01', '379', '', null, null);
INSERT INTO t_sys_area VALUES ('382', '110105', '110105', '110105', '朝阳区', 'Chaoyang Qu', 'Chaoyang Qu', ',2,33,382,', '北京-北京市-朝阳区', '33', '02', '04', '01', '382', '', null, null);
INSERT INTO t_sys_area VALUES ('383', '110106', '110106', '110106', '丰台区', 'Fengtai Qu', 'Fengtai Qu', ',2,33,383,', '北京-北京市-丰台区', '33', '02', '04', '01', '383', '', null, null);
INSERT INTO t_sys_area VALUES ('384', '110107', '110107', '110107', '石景山区', 'Shijingshan Qu', 'Shijingshan Qu', ',2,33,384,', '北京-北京市-石景山区', '33', '02', '04', '01', '384', '', null, null);
INSERT INTO t_sys_area VALUES ('385', '110108', '110108', '110108', '海淀区', 'Haidian Qu', 'Haidian Qu', ',2,33,385,', '北京-北京市-海淀区', '33', '02', '04', '01', '385', '', null, null);
INSERT INTO t_sys_area VALUES ('386', '110109', '110109', '110109', '门头沟区', 'Mentougou Qu', 'Mentougou Qu', ',2,33,386,', '北京-北京市-门头沟区', '33', '02', '04', '01', '386', '', null, null);
INSERT INTO t_sys_area VALUES ('387', '110111', '110111', '110111', '房山区', 'Fangshan Qu', 'Fangshan Qu', ',2,33,387,', '北京-北京市-房山区', '33', '02', '04', '01', '387', '', null, null);
INSERT INTO t_sys_area VALUES ('388', '110112', '110112', '110112', '通州区', 'Tongzhou Qu', 'Tongzhou Qu', ',2,33,388,', '北京-北京市-通州区', '33', '02', '04', '01', '388', '', null, null);
INSERT INTO t_sys_area VALUES ('389', '110113', '110113', '110113', '顺义区', 'Shunyi Qu', 'Shunyi Qu', ',2,33,389,', '北京-北京市-顺义区', '33', '02', '04', '01', '389', '', null, null);
INSERT INTO t_sys_area VALUES ('390', '110114', '110114', '110114', '昌平区', 'Changping Qu', 'Changping Qu', ',2,33,390,', '北京-北京市-昌平区', '33', '02', '04', '01', '390', '', null, null);
INSERT INTO t_sys_area VALUES ('391', '110115', '110115', '110115', '大兴区', 'Daxing Qu', 'Daxing Qu', ',2,33,391,', '北京-北京市-大兴区', '33', '02', '04', '01', '391', '', null, null);
INSERT INTO t_sys_area VALUES ('392', '110116', '110116', '110116', '怀柔区', 'Huairou Qu', 'Huairou Qu', ',2,33,392,', '北京-北京市-怀柔区', '33', '02', '04', '01', '392', '', null, null);
INSERT INTO t_sys_area VALUES ('393', '110117', '110117', '110117', '平谷区', 'Pinggu Qu', 'Pinggu Qu', ',2,33,393,', '北京-北京市-平谷区', '33', '02', '04', '01', '393', '', null, null);
INSERT INTO t_sys_area VALUES ('394', '110228', '110228', '110228', '密云县', 'Miyun Xian ', 'Miyun Xian ', ',2,33,394,', '北京-北京市-密云县', '33', '02', '04', '01', '394', '', null, null);
INSERT INTO t_sys_area VALUES ('395', '110229', '110229', '110229', '延庆县', 'Yanqing Xian', 'Yanqing Xian', ',395,', '延庆县', '34', '02', '04', '01', '395', '', null, null);
INSERT INTO t_sys_area VALUES ('396', '120101', '120101', '120101', '和平区', 'Heping Qu', 'Heping Qu', ',3,35,396,', '天津-天津市-和平区', '35', '02', '04', '01', '396', '', null, null);
INSERT INTO t_sys_area VALUES ('397', '120102', '120102', '120102', '河东区', 'Hedong Qu', 'Hedong Qu', ',3,35,397,', '天津-天津市-河东区', '35', '02', '04', '01', '397', '', null, null);
INSERT INTO t_sys_area VALUES ('398', '120103', '120103', '120103', '河西区', 'Hexi Qu', 'Hexi Qu', ',3,35,398,', '天津-天津市-河西区', '35', '02', '04', '01', '398', '', null, null);
INSERT INTO t_sys_area VALUES ('399', '120104', '120104', '120104', '南开区', 'Nankai Qu', 'Nankai Qu', ',3,35,399,', '天津-天津市-南开区', '35', '02', '04', '01', '399', '', null, null);
INSERT INTO t_sys_area VALUES ('400', '120105', '120105', '120105', '河北区', 'Hebei Qu', 'Hebei Qu', ',3,35,400,', '天津-天津市-河北区', '35', '02', '04', '01', '400', '', null, null);
INSERT INTO t_sys_area VALUES ('401', '120106', '120106', '120106', '红桥区', 'Hongqiao Qu', 'Hongqiao Qu', ',3,35,401,', '天津-天津市-红桥区', '35', '02', '04', '01', '401', '', null, null);
INSERT INTO t_sys_area VALUES ('404', '120116', '120116', '120116', '滨海新区', 'Dagang Qu', 'Dagang Qu', ',3,35,404,', '天津-天津市-滨海新区', '35', '02', '04', '01', '404', '', null, null);
INSERT INTO t_sys_area VALUES ('405', '120110', '120110', '120110', '东丽区', 'Dongli Qu', 'Dongli Qu', ',3,35,405,', '天津-天津市-东丽区', '35', '02', '04', '01', '405', '', null, null);
INSERT INTO t_sys_area VALUES ('406', '120111', '120111', '120111', '西青区', 'Xiqing Qu', 'Xiqing Qu', ',3,35,406,', '天津-天津市-西青区', '35', '02', '04', '01', '406', '', null, null);
INSERT INTO t_sys_area VALUES ('407', '120112', '120112', '120112', '津南区', 'Jinnan Qu', 'Jinnan Qu', ',3,35,407,', '天津-天津市-津南区', '35', '02', '04', '01', '407', '', null, null);
INSERT INTO t_sys_area VALUES ('408', '120113', '120113', '120113', '北辰区', 'Beichen Qu', 'Beichen Qu', ',3,35,408,', '天津-天津市-北辰区', '35', '02', '04', '01', '408', '', null, null);
INSERT INTO t_sys_area VALUES ('409', '120114', '120114', '120114', '武清区', 'Wuqing Qu', 'Wuqing Qu', ',3,35,409,', '天津-天津市-武清区', '35', '02', '04', '01', '409', '', null, null);
INSERT INTO t_sys_area VALUES ('410', '120115', '120115', '120115', '宝坻区', 'Baodi Qu', 'Baodi Qu', ',3,35,410,', '天津-天津市-宝坻区', '35', '02', '04', '01', '410', '', null, null);
INSERT INTO t_sys_area VALUES ('411', '120221', '120221', '120221', '宁河县', 'Ninghe Xian', 'Ninghe Xian', ',3,35,411,', '天津-天津市-宁河县', '35', '02', '04', '01', '411', '', null, null);
INSERT INTO t_sys_area VALUES ('412', '120223', '120223', '120223', '静海县', 'Jinghai Xian', 'Jinghai Xian', ',3,35,412,', '天津-天津市-静海县', '35', '02', '04', '01', '412', '', null, null);
INSERT INTO t_sys_area VALUES ('413', '120225', '120225', '120225', '蓟县', 'Ji Xian', 'Ji Xian', ',3,35,413,', '天津-天津市-蓟县', '35', '02', '04', '01', '413', '', null, null);
INSERT INTO t_sys_area VALUES ('414', '130101', '130101', '130101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',4,37,414,', '河北省-石家庄市-市辖区', '37', '02', '04', '01', '414', '', null, null);
INSERT INTO t_sys_area VALUES ('415', '130102', '130102', '130102', '长安区', 'Chang,an Qu', 'Chang,an Qu', ',4,37,415,', '河北省-石家庄市-长安区', '37', '02', '04', '01', '415', '', null, null);
INSERT INTO t_sys_area VALUES ('416', '130103', '130103', '130103', '桥东区', 'Qiaodong Qu', 'Qiaodong Qu', ',4,37,416,', '河北省-石家庄市-桥东区', '37', '02', '04', '01', '416', '', null, null);
INSERT INTO t_sys_area VALUES ('417', '130104', '130104', '130104', '桥西区', 'Qiaoxi Qu', 'Qiaoxi Qu', ',4,37,417,', '河北省-石家庄市-桥西区', '37', '02', '04', '01', '417', '', null, null);
INSERT INTO t_sys_area VALUES ('418', '130105', '130105', '130105', '新华区', 'Xinhua Qu', 'Xinhua Qu', ',4,37,418,', '河北省-石家庄市-新华区', '37', '02', '04', '01', '418', '', null, null);
INSERT INTO t_sys_area VALUES ('419', '130107', '130107', '130107', '井陉矿区', 'Jingxing Kuangqu', 'Jingxing Kuangqu', ',4,37,419,', '河北省-石家庄市-井陉矿区', '37', '02', '04', '01', '419', '', null, null);
INSERT INTO t_sys_area VALUES ('420', '130108', '130108', '130108', '裕华区', 'Yuhua Qu', 'Yuhua Qu', ',4,37,420,', '河北省-石家庄市-裕华区', '37', '02', '04', '01', '420', '', null, null);
INSERT INTO t_sys_area VALUES ('421', '130121', '130121', '130121', '井陉县', 'Jingxing Xian', 'Jingxing Xian', ',4,37,421,', '河北省-石家庄市-井陉县', '37', '02', '04', '01', '421', '', null, null);
INSERT INTO t_sys_area VALUES ('422', '130123', '130123', '130123', '正定县', 'Zhengding Xian', 'Zhengding Xian', ',4,37,422,', '河北省-石家庄市-正定县', '37', '02', '04', '01', '422', '', null, null);
INSERT INTO t_sys_area VALUES ('423', '130124', '130124', '130124', '栾城县', 'Luancheng Xian', 'Luancheng Xian', ',4,37,423,', '河北省-石家庄市-栾城县', '37', '02', '04', '01', '423', '', null, null);
INSERT INTO t_sys_area VALUES ('424', '130125', '130125', '130125', '行唐县', 'Xingtang Xian', 'Xingtang Xian', ',4,37,424,', '河北省-石家庄市-行唐县', '37', '02', '04', '01', '424', '', null, null);
INSERT INTO t_sys_area VALUES ('425', '130126', '130126', '130126', '灵寿县', 'Lingshou Xian ', 'Lingshou Xian ', ',4,37,425,', '河北省-石家庄市-灵寿县', '37', '02', '04', '01', '425', '', null, null);
INSERT INTO t_sys_area VALUES ('426', '130127', '130127', '130127', '高邑县', 'Gaoyi Xian', 'Gaoyi Xian', ',4,37,426,', '河北省-石家庄市-高邑县', '37', '02', '04', '01', '426', '', null, null);
INSERT INTO t_sys_area VALUES ('427', '130128', '130128', '130128', '深泽县', 'Shenze Xian', 'Shenze Xian', ',4,37,427,', '河北省-石家庄市-深泽县', '37', '02', '04', '01', '427', '', null, null);
INSERT INTO t_sys_area VALUES ('428', '130129', '130129', '130129', '赞皇县', 'Zanhuang Xian', 'Zanhuang Xian', ',4,37,428,', '河北省-石家庄市-赞皇县', '37', '02', '04', '01', '428', '', null, null);
INSERT INTO t_sys_area VALUES ('429', '130130', '130130', '130130', '无极县', 'Wuji Xian', 'Wuji Xian', ',4,37,429,', '河北省-石家庄市-无极县', '37', '02', '04', '01', '429', '', null, null);
INSERT INTO t_sys_area VALUES ('430', '130131', '130131', '130131', '平山县', 'Pingshan Xian', 'Pingshan Xian', ',4,37,430,', '河北省-石家庄市-平山县', '37', '02', '04', '01', '430', '', null, null);
INSERT INTO t_sys_area VALUES ('431', '130132', '130132', '130132', '元氏县', 'Yuanshi Xian', 'Yuanshi Xian', ',4,37,431,', '河北省-石家庄市-元氏县', '37', '02', '04', '01', '431', '', null, null);
INSERT INTO t_sys_area VALUES ('432', '130133', '130133', '130133', '赵县', 'Zhao Xian', 'Zhao Xian', ',4,37,432,', '河北省-石家庄市-赵县', '37', '02', '04', '01', '432', '', null, null);
INSERT INTO t_sys_area VALUES ('433', '130181', '130181', '130181', '辛集市', 'Xinji Shi', 'Xinji Shi', ',4,37,433,', '河北省-石家庄市-辛集市', '37', '02', '04', '01', '433', '', null, null);
INSERT INTO t_sys_area VALUES ('434', '130182', '130182', '130182', '藁城市', 'Gaocheng Shi', 'Gaocheng Shi', ',4,37,434,', '河北省-石家庄市-藁城市', '37', '02', '04', '01', '434', '', null, null);
INSERT INTO t_sys_area VALUES ('435', '130183', '130183', '130183', '晋州市', 'Jinzhou Shi', 'Jinzhou Shi', ',4,37,435,', '河北省-石家庄市-晋州市', '37', '02', '04', '01', '435', '', null, null);
INSERT INTO t_sys_area VALUES ('436', '130184', '130184', '130184', '新乐市', 'Xinle Shi', 'Xinle Shi', ',4,37,436,', '河北省-石家庄市-新乐市', '37', '02', '04', '01', '436', '', null, null);
INSERT INTO t_sys_area VALUES ('437', '130185', '130185', '130185', '鹿泉市', 'Luquan Shi', 'Luquan Shi', ',4,37,437,', '河北省-石家庄市-鹿泉市', '37', '02', '04', '01', '437', '', null, null);
INSERT INTO t_sys_area VALUES ('438', '130201', '130201', '130201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',4,38,438,', '河北省-唐山市-市辖区', '38', '02', '04', '01', '438', '', null, null);
INSERT INTO t_sys_area VALUES ('439', '130202', '130202', '130202', '路南区', 'Lunan Qu', 'Lunan Qu', ',4,38,439,', '河北省-唐山市-路南区', '38', '02', '04', '01', '439', '', null, null);
INSERT INTO t_sys_area VALUES ('440', '130203', '130203', '130203', '路北区', 'Lubei Qu', 'Lubei Qu', ',4,38,440,', '河北省-唐山市-路北区', '38', '02', '04', '01', '440', '', null, null);
INSERT INTO t_sys_area VALUES ('441', '130204', '130204', '130204', '古冶区', 'Guye Qu', 'Guye Qu', ',4,38,441,', '河北省-唐山市-古冶区', '38', '02', '04', '01', '441', '', null, null);
INSERT INTO t_sys_area VALUES ('442', '130205', '130205', '130205', '开平区', 'Kaiping Qu', 'Kaiping Qu', ',4,38,442,', '河北省-唐山市-开平区', '38', '02', '04', '01', '442', '', null, null);
INSERT INTO t_sys_area VALUES ('443', '130207', '130207', '130207', '丰南区', 'Fengnan Qu', 'Fengnan Qu', ',4,38,443,', '河北省-唐山市-丰南区', '38', '02', '04', '01', '443', '', null, null);
INSERT INTO t_sys_area VALUES ('444', '130208', '130208', '130208', '丰润区', 'Fengrun Qu', 'Fengrun Qu', ',4,38,444,', '河北省-唐山市-丰润区', '38', '02', '04', '01', '444', '', null, null);
INSERT INTO t_sys_area VALUES ('445', '130223', '130223', '130223', '滦县', 'Luan Xian', 'Luan Xian', ',4,38,445,', '河北省-唐山市-滦县', '38', '02', '04', '01', '445', '', null, null);
INSERT INTO t_sys_area VALUES ('446', '130224', '130224', '130224', '滦南县', 'Luannan Xian', 'Luannan Xian', ',4,38,446,', '河北省-唐山市-滦南县', '38', '02', '04', '01', '446', '', null, null);
INSERT INTO t_sys_area VALUES ('447', '130225', '130225', '130225', '乐亭县', 'Leting Xian', 'Leting Xian', ',4,38,447,', '河北省-唐山市-乐亭县', '38', '02', '04', '01', '447', '', null, null);
INSERT INTO t_sys_area VALUES ('448', '130227', '130227', '130227', '迁西县', 'Qianxi Xian', 'Qianxi Xian', ',4,38,448,', '河北省-唐山市-迁西县', '38', '02', '04', '01', '448', '', null, null);
INSERT INTO t_sys_area VALUES ('449', '130229', '130229', '130229', '玉田县', 'Yutian Xian', 'Yutian Xian', ',4,38,449,', '河北省-唐山市-玉田县', '38', '02', '04', '01', '449', '', null, null);
INSERT INTO t_sys_area VALUES ('450', '130230', '130230', '130230', '唐海县', 'Tanghai Xian ', 'Tanghai Xian ', ',4,38,450,', '河北省-唐山市-唐海县', '38', '02', '04', '01', '450', '', null, null);
INSERT INTO t_sys_area VALUES ('451', '130281', '130281', '130281', '遵化市', 'Zunhua Shi', 'Zunhua Shi', ',4,38,451,', '河北省-唐山市-遵化市', '38', '02', '04', '01', '451', '', null, null);
INSERT INTO t_sys_area VALUES ('452', '130283', '130283', '130283', '迁安市', 'Qian,an Shi', 'Qian,an Shi', ',4,38,452,', '河北省-唐山市-迁安市', '38', '02', '04', '01', '452', '', null, null);
INSERT INTO t_sys_area VALUES ('453', '130301', '130301', '130301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',4,39,453,', '河北省-秦皇岛市-市辖区', '39', '02', '04', '01', '453', '', null, null);
INSERT INTO t_sys_area VALUES ('454', '130302', '130302', '130302', '海港区', 'Haigang Qu', 'Haigang Qu', ',4,39,454,', '河北省-秦皇岛市-海港区', '39', '02', '04', '01', '454', '', null, null);
INSERT INTO t_sys_area VALUES ('455', '130303', '130303', '130303', '山海关区', 'Shanhaiguan Qu', 'Shanhaiguan Qu', ',4,39,455,', '河北省-秦皇岛市-山海关区', '39', '02', '04', '01', '455', '', null, null);
INSERT INTO t_sys_area VALUES ('456', '130304', '130304', '130304', '北戴河区', 'Beidaihe Qu', 'Beidaihe Qu', ',4,39,456,', '河北省-秦皇岛市-北戴河区', '39', '02', '04', '01', '456', '', null, null);
INSERT INTO t_sys_area VALUES ('457', '130321', '130321', '130321', '青龙满族自治县', 'Qinglong Manzu Zizhixian', 'Qinglong Manzu Zizhixian', ',4,39,457,', '河北省-秦皇岛市-青龙满族自治县', '39', '02', '04', '01', '457', '', null, null);
INSERT INTO t_sys_area VALUES ('458', '130322', '130322', '130322', '昌黎县', 'Changli Xian', 'Changli Xian', ',4,39,458,', '河北省-秦皇岛市-昌黎县', '39', '02', '04', '01', '458', '', null, null);
INSERT INTO t_sys_area VALUES ('459', '130323', '130323', '130323', '抚宁县', 'Funing Xian ', 'Funing Xian ', ',4,39,459,', '河北省-秦皇岛市-抚宁县', '39', '02', '04', '01', '459', '', null, null);
INSERT INTO t_sys_area VALUES ('460', '130324', '130324', '130324', '卢龙县', 'Lulong Xian', 'Lulong Xian', ',4,39,460,', '河北省-秦皇岛市-卢龙县', '39', '02', '04', '01', '460', '', null, null);
INSERT INTO t_sys_area VALUES ('461', '130401', '130401', '130401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',4,40,461,', '河北省-邯郸市-市辖区', '40', '02', '04', '01', '461', '', null, null);
INSERT INTO t_sys_area VALUES ('462', '130402', '130402', '130402', '邯山区', 'Hanshan Qu', 'Hanshan Qu', ',4,40,462,', '河北省-邯郸市-邯山区', '40', '02', '04', '01', '462', '', null, null);
INSERT INTO t_sys_area VALUES ('463', '130403', '130403', '130403', '丛台区', 'Congtai Qu', 'Congtai Qu', ',4,40,463,', '河北省-邯郸市-丛台区', '40', '02', '04', '01', '463', '', null, null);
INSERT INTO t_sys_area VALUES ('464', '130404', '130404', '130404', '复兴区', 'Fuxing Qu', 'Fuxing Qu', ',4,40,464,', '河北省-邯郸市-复兴区', '40', '02', '04', '01', '464', '', null, null);
INSERT INTO t_sys_area VALUES ('465', '130406', '130406', '130406', '峰峰矿区', 'Fengfeng Kuangqu', 'Fengfeng Kuangqu', ',4,40,465,', '河北省-邯郸市-峰峰矿区', '40', '02', '04', '01', '465', '', null, null);
INSERT INTO t_sys_area VALUES ('466', '130421', '130421', '130421', '邯郸县', 'Handan Xian ', 'Handan Xian ', ',4,40,466,', '河北省-邯郸市-邯郸县', '40', '02', '04', '01', '466', '', null, null);
INSERT INTO t_sys_area VALUES ('467', '130423', '130423', '130423', '临漳县', 'Linzhang Xian ', 'Linzhang Xian ', ',4,40,467,', '河北省-邯郸市-临漳县', '40', '02', '04', '01', '467', '', null, null);
INSERT INTO t_sys_area VALUES ('468', '130424', '130424', '130424', '成安县', 'Cheng,an Xian', 'Cheng,an Xian', ',4,40,468,', '河北省-邯郸市-成安县', '40', '02', '04', '01', '468', '', null, null);
INSERT INTO t_sys_area VALUES ('469', '130425', '130425', '130425', '大名县', 'Daming Xian', 'Daming Xian', ',4,40,469,', '河北省-邯郸市-大名县', '40', '02', '04', '01', '469', '', null, null);
INSERT INTO t_sys_area VALUES ('470', '130426', '130426', '130426', '涉县', 'She Xian', 'She Xian', ',4,40,470,', '河北省-邯郸市-涉县', '40', '02', '04', '01', '470', '', null, null);
INSERT INTO t_sys_area VALUES ('471', '130427', '130427', '130427', '磁县', 'Ci Xian', 'Ci Xian', ',4,40,471,', '河北省-邯郸市-磁县', '40', '02', '04', '01', '471', '', null, null);
INSERT INTO t_sys_area VALUES ('472', '130428', '130428', '130428', '肥乡县', 'Feixiang Xian', 'Feixiang Xian', ',4,40,472,', '河北省-邯郸市-肥乡县', '40', '02', '04', '01', '472', '', null, null);
INSERT INTO t_sys_area VALUES ('473', '130429', '130429', '130429', '永年县', 'Yongnian Xian', 'Yongnian Xian', ',4,40,473,', '河北省-邯郸市-永年县', '40', '02', '04', '01', '473', '', null, null);
INSERT INTO t_sys_area VALUES ('474', '130430', '130430', '130430', '邱县', 'Qiu Xian', 'Qiu Xian', ',4,40,474,', '河北省-邯郸市-邱县', '40', '02', '04', '01', '474', '', null, null);
INSERT INTO t_sys_area VALUES ('475', '130431', '130431', '130431', '鸡泽县', 'Jize Xian', 'Jize Xian', ',4,40,475,', '河北省-邯郸市-鸡泽县', '40', '02', '04', '01', '475', '', null, null);
INSERT INTO t_sys_area VALUES ('476', '130432', '130432', '130432', '广平县', 'Guangping Xian ', 'Guangping Xian ', ',4,40,476,', '河北省-邯郸市-广平县', '40', '02', '04', '01', '476', '', null, null);
INSERT INTO t_sys_area VALUES ('477', '130433', '130433', '130433', '馆陶县', 'Guantao Xian', 'Guantao Xian', ',4,40,477,', '河北省-邯郸市-馆陶县', '40', '02', '04', '01', '477', '', null, null);
INSERT INTO t_sys_area VALUES ('478', '130434', '130434', '130434', '魏县', 'Wei Xian ', 'Wei Xian ', ',4,40,478,', '河北省-邯郸市-魏县', '40', '02', '04', '01', '478', '', null, null);
INSERT INTO t_sys_area VALUES ('479', '130435', '130435', '130435', '曲周县', 'Quzhou Xian ', 'Quzhou Xian ', ',4,40,479,', '河北省-邯郸市-曲周县', '40', '02', '04', '01', '479', '', null, null);
INSERT INTO t_sys_area VALUES ('480', '130481', '130481', '130481', '武安市', 'Wu,an Shi', 'Wu,an Shi', ',4,40,480,', '河北省-邯郸市-武安市', '40', '02', '04', '01', '480', '', null, null);
INSERT INTO t_sys_area VALUES ('481', '130501', '130501', '130501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',4,41,481,', '河北省-邢台市-市辖区', '41', '02', '04', '01', '481', '', null, null);
INSERT INTO t_sys_area VALUES ('482', '130502', '130502', '130502', '桥东区', 'Qiaodong Qu', 'Qiaodong Qu', ',4,41,482,', '河北省-邢台市-桥东区', '41', '02', '04', '01', '482', '', null, null);
INSERT INTO t_sys_area VALUES ('483', '130503', '130503', '130503', '桥西区', 'Qiaoxi Qu', 'Qiaoxi Qu', ',4,41,483,', '河北省-邢台市-桥西区', '41', '02', '04', '01', '483', '', null, null);
INSERT INTO t_sys_area VALUES ('484', '130521', '130521', '130521', '邢台县', 'Xingtai Xian', 'Xingtai Xian', ',4,41,484,', '河北省-邢台市-邢台县', '41', '02', '04', '01', '484', '', null, null);
INSERT INTO t_sys_area VALUES ('485', '130522', '130522', '130522', '临城县', 'Lincheng Xian ', 'Lincheng Xian ', ',4,41,485,', '河北省-邢台市-临城县', '41', '02', '04', '01', '485', '', null, null);
INSERT INTO t_sys_area VALUES ('486', '130523', '130523', '130523', '内丘县', 'Neiqiu Xian ', 'Neiqiu Xian ', ',4,41,486,', '河北省-邢台市-内丘县', '41', '02', '04', '01', '486', '', null, null);
INSERT INTO t_sys_area VALUES ('487', '130524', '130524', '130524', '柏乡县', 'Baixiang Xian', 'Baixiang Xian', ',4,41,487,', '河北省-邢台市-柏乡县', '41', '02', '04', '01', '487', '', null, null);
INSERT INTO t_sys_area VALUES ('488', '130525', '130525', '130525', '隆尧县', 'Longyao Xian', 'Longyao Xian', ',4,41,488,', '河北省-邢台市-隆尧县', '41', '02', '04', '01', '488', '', null, null);
INSERT INTO t_sys_area VALUES ('489', '130526', '130526', '130526', '任县', 'Ren Xian', 'Ren Xian', ',4,41,489,', '河北省-邢台市-任县', '41', '02', '04', '01', '489', '', null, null);
INSERT INTO t_sys_area VALUES ('490', '130527', '130527', '130527', '南和县', 'Nanhe Xian', 'Nanhe Xian', ',4,41,490,', '河北省-邢台市-南和县', '41', '02', '04', '01', '490', '', null, null);
INSERT INTO t_sys_area VALUES ('491', '130528', '130528', '130528', '宁晋县', 'Ningjin Xian', 'Ningjin Xian', ',4,41,491,', '河北省-邢台市-宁晋县', '41', '02', '04', '01', '491', '', null, null);
INSERT INTO t_sys_area VALUES ('492', '130529', '130529', '130529', '巨鹿县', 'Julu Xian', 'Julu Xian', ',4,41,492,', '河北省-邢台市-巨鹿县', '41', '02', '04', '01', '492', '', null, null);
INSERT INTO t_sys_area VALUES ('493', '130530', '130530', '130530', '新河县', 'Xinhe Xian ', 'Xinhe Xian ', ',4,41,493,', '河北省-邢台市-新河县', '41', '02', '04', '01', '493', '', null, null);
INSERT INTO t_sys_area VALUES ('494', '130531', '130531', '130531', '广宗县', 'Guangzong Xian ', 'Guangzong Xian ', ',4,41,494,', '河北省-邢台市-广宗县', '41', '02', '04', '01', '494', '', null, null);
INSERT INTO t_sys_area VALUES ('495', '130532', '130532', '130532', '平乡县', 'Pingxiang Xian', 'Pingxiang Xian', ',4,41,495,', '河北省-邢台市-平乡县', '41', '02', '04', '01', '495', '', null, null);
INSERT INTO t_sys_area VALUES ('496', '130533', '130533', '130533', '威县', 'Wei Xian ', 'Wei Xian ', ',4,41,496,', '河北省-邢台市-威县', '41', '02', '04', '01', '496', '', null, null);
INSERT INTO t_sys_area VALUES ('497', '130534', '130534', '130534', '清河县', 'Qinghe Xian', 'Qinghe Xian', ',4,41,497,', '河北省-邢台市-清河县', '41', '02', '04', '01', '497', '', null, null);
INSERT INTO t_sys_area VALUES ('498', '130535', '130535', '130535', '临西县', 'Linxi Xian', 'Linxi Xian', ',4,41,498,', '河北省-邢台市-临西县', '41', '02', '04', '01', '498', '', null, null);
INSERT INTO t_sys_area VALUES ('499', '130581', '130581', '130581', '南宫市', 'Nangong Shi', 'Nangong Shi', ',4,41,499,', '河北省-邢台市-南宫市', '41', '02', '04', '01', '499', '', null, null);
INSERT INTO t_sys_area VALUES ('500', '130582', '130582', '130582', '沙河市', 'Shahe Shi', 'Shahe Shi', ',4,41,500,', '河北省-邢台市-沙河市', '41', '02', '04', '01', '500', '', null, null);
INSERT INTO t_sys_area VALUES ('501', '130601', '130601', '130601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',4,42,501,', '河北省-保定市-市辖区', '42', '02', '04', '01', '501', '', null, null);
INSERT INTO t_sys_area VALUES ('502', '130600', '130600', '130600', '新市区', 'Xinshi Qu', 'Xinshi Qu', ',4,42,502,', '河北省-保定市-新市区', '42', '02', '04', '01', '502', '', null, null);
INSERT INTO t_sys_area VALUES ('503', '130603', '130603', '130603', '北市区', 'Beishi Qu', 'Beishi Qu', ',4,42,503,', '河北省-保定市-北市区', '42', '02', '04', '01', '503', '', null, null);
INSERT INTO t_sys_area VALUES ('504', '130604', '130604', '130604', '南市区', 'Nanshi Qu', 'Nanshi Qu', ',4,42,504,', '河北省-保定市-南市区', '42', '02', '04', '01', '504', '', null, null);
INSERT INTO t_sys_area VALUES ('505', '130621', '130621', '130621', '满城县', 'Mancheng Xian ', 'Mancheng Xian ', ',4,42,505,', '河北省-保定市-满城县', '42', '02', '04', '01', '505', '', null, null);
INSERT INTO t_sys_area VALUES ('506', '130622', '130622', '130622', '清苑县', 'Qingyuan Xian', 'Qingyuan Xian', ',4,42,506,', '河北省-保定市-清苑县', '42', '02', '04', '01', '506', '', null, null);
INSERT INTO t_sys_area VALUES ('507', '130623', '130623', '130623', '涞水县', 'Laishui Xian', 'Laishui Xian', ',4,42,507,', '河北省-保定市-涞水县', '42', '02', '04', '01', '507', '', null, null);
INSERT INTO t_sys_area VALUES ('508', '130624', '130624', '130624', '阜平县', 'Fuping Xian ', 'Fuping Xian ', ',4,42,508,', '河北省-保定市-阜平县', '42', '02', '04', '01', '508', '', null, null);
INSERT INTO t_sys_area VALUES ('509', '130625', '130625', '130625', '徐水县', 'Xushui Xian ', 'Xushui Xian ', ',4,42,509,', '河北省-保定市-徐水县', '42', '02', '04', '01', '509', '', null, null);
INSERT INTO t_sys_area VALUES ('510', '130626', '130626', '130626', '定兴县', 'Dingxing Xian ', 'Dingxing Xian ', ',4,42,510,', '河北省-保定市-定兴县', '42', '02', '04', '01', '510', '', null, null);
INSERT INTO t_sys_area VALUES ('511', '130627', '130627', '130627', '唐县', 'Tang Xian ', 'Tang Xian ', ',4,42,511,', '河北省-保定市-唐县', '42', '02', '04', '01', '511', '', null, null);
INSERT INTO t_sys_area VALUES ('512', '130628', '130628', '130628', '高阳县', 'Gaoyang Xian ', 'Gaoyang Xian ', ',4,42,512,', '河北省-保定市-高阳县', '42', '02', '04', '01', '512', '', null, null);
INSERT INTO t_sys_area VALUES ('513', '130629', '130629', '130629', '容城县', 'Rongcheng Xian ', 'Rongcheng Xian ', ',4,42,513,', '河北省-保定市-容城县', '42', '02', '04', '01', '513', '', null, null);
INSERT INTO t_sys_area VALUES ('514', '130630', '130630', '130630', '涞源县', 'Laiyuan Xian ', 'Laiyuan Xian ', ',4,42,514,', '河北省-保定市-涞源县', '42', '02', '04', '01', '514', '', null, null);
INSERT INTO t_sys_area VALUES ('515', '130631', '130631', '130631', '望都县', 'Wangdu Xian ', 'Wangdu Xian ', ',4,42,515,', '河北省-保定市-望都县', '42', '02', '04', '01', '515', '', null, null);
INSERT INTO t_sys_area VALUES ('516', '130632', '130632', '130632', '安新县', 'Anxin Xian ', 'Anxin Xian ', ',4,42,516,', '河北省-保定市-安新县', '42', '02', '04', '01', '516', '', null, null);
INSERT INTO t_sys_area VALUES ('517', '130633', '130633', '130633', '易县', 'Yi Xian', 'Yi Xian', ',4,42,517,', '河北省-保定市-易县', '42', '02', '04', '01', '517', '', null, null);
INSERT INTO t_sys_area VALUES ('518', '130634', '130634', '130634', '曲阳县', 'Quyang Xian ', 'Quyang Xian ', ',4,42,518,', '河北省-保定市-曲阳县', '42', '02', '04', '01', '518', '', null, null);
INSERT INTO t_sys_area VALUES ('519', '130635', '130635', '130635', '蠡县', 'Li Xian', 'Li Xian', ',4,42,519,', '河北省-保定市-蠡县', '42', '02', '04', '01', '519', '', null, null);
INSERT INTO t_sys_area VALUES ('520', '130636', '130636', '130636', '顺平县', 'Shunping Xian ', 'Shunping Xian ', ',4,42,520,', '河北省-保定市-顺平县', '42', '02', '04', '01', '520', '', null, null);
INSERT INTO t_sys_area VALUES ('521', '130637', '130637', '130637', '博野县', 'Boye Xian ', 'Boye Xian ', ',4,42,521,', '河北省-保定市-博野县', '42', '02', '04', '01', '521', '', null, null);
INSERT INTO t_sys_area VALUES ('522', '130638', '130638', '130638', '雄县', 'Xiong Xian', 'Xiong Xian', ',4,42,522,', '河北省-保定市-雄县', '42', '02', '04', '01', '522', '', null, null);
INSERT INTO t_sys_area VALUES ('523', '130681', '130681', '130681', '涿州市', 'Zhuozhou Shi', 'Zhuozhou Shi', ',4,42,523,', '河北省-保定市-涿州市', '42', '02', '04', '01', '523', '', null, null);
INSERT INTO t_sys_area VALUES ('524', '130682', '130682', '130682', '定州市', 'Dingzhou Shi ', 'Dingzhou Shi ', ',4,42,524,', '河北省-保定市-定州市', '42', '02', '04', '01', '524', '', null, null);
INSERT INTO t_sys_area VALUES ('525', '130683', '130683', '130683', '安国市', 'Anguo Shi ', 'Anguo Shi ', ',4,42,525,', '河北省-保定市-安国市', '42', '02', '04', '01', '525', '', null, null);
INSERT INTO t_sys_area VALUES ('526', '130684', '130684', '130684', '高碑店市', 'Gaobeidian Shi', 'Gaobeidian Shi', ',4,42,526,', '河北省-保定市-高碑店市', '42', '02', '04', '01', '526', '', null, null);
INSERT INTO t_sys_area VALUES ('527', '130701', '130701', '130701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',4,43,527,', '河北省-张家口市-市辖区', '43', '02', '04', '01', '527', '', null, null);
INSERT INTO t_sys_area VALUES ('528', '130702', '130702', '130702', '桥东区', 'Qiaodong Qu', 'Qiaodong Qu', ',4,43,528,', '河北省-张家口市-桥东区', '43', '02', '04', '01', '528', '', null, null);
INSERT INTO t_sys_area VALUES ('529', '130703', '130703', '130703', '桥西区', 'Qiaoxi Qu', 'Qiaoxi Qu', ',4,43,529,', '河北省-张家口市-桥西区', '43', '02', '04', '01', '529', '', null, null);
INSERT INTO t_sys_area VALUES ('530', '130705', '130705', '130705', '宣化区', 'Xuanhua Qu', 'Xuanhua Qu', ',4,43,530,', '河北省-张家口市-宣化区', '43', '02', '04', '01', '530', '', null, null);
INSERT INTO t_sys_area VALUES ('531', '130706', '130706', '130706', '下花园区', 'Xiahuayuan Qu ', 'Xiahuayuan Qu ', ',4,43,531,', '河北省-张家口市-下花园区', '43', '02', '04', '01', '531', '', null, null);
INSERT INTO t_sys_area VALUES ('532', '130721', '130721', '130721', '宣化县', 'Xuanhua Xian ', 'Xuanhua Xian ', ',4,43,532,', '河北省-张家口市-宣化县', '43', '02', '04', '01', '532', '', null, null);
INSERT INTO t_sys_area VALUES ('533', '130722', '130722', '130722', '张北县', 'Zhangbei Xian ', 'Zhangbei Xian ', ',4,43,533,', '河北省-张家口市-张北县', '43', '02', '04', '01', '533', '', null, null);
INSERT INTO t_sys_area VALUES ('534', '130723', '130723', '130723', '康保县', 'Kangbao Xian', 'Kangbao Xian', ',4,43,534,', '河北省-张家口市-康保县', '43', '02', '04', '01', '534', '', null, null);
INSERT INTO t_sys_area VALUES ('535', '130724', '130724', '130724', '沽源县', 'Guyuan Xian', 'Guyuan Xian', ',4,43,535,', '河北省-张家口市-沽源县', '43', '02', '04', '01', '535', '', null, null);
INSERT INTO t_sys_area VALUES ('536', '130725', '130725', '130725', '尚义县', 'Shangyi Xian', 'Shangyi Xian', ',4,43,536,', '河北省-张家口市-尚义县', '43', '02', '04', '01', '536', '', null, null);
INSERT INTO t_sys_area VALUES ('537', '130726', '130726', '130726', '蔚县', 'Yu Xian', 'Yu Xian', ',4,43,537,', '河北省-张家口市-蔚县', '43', '02', '04', '01', '537', '', null, null);
INSERT INTO t_sys_area VALUES ('538', '130727', '130727', '130727', '阳原县', 'Yangyuan Xian', 'Yangyuan Xian', ',4,43,538,', '河北省-张家口市-阳原县', '43', '02', '04', '01', '538', '', null, null);
INSERT INTO t_sys_area VALUES ('539', '130728', '130728', '130728', '怀安县', 'Huai,an Xian', 'Huai,an Xian', ',4,43,539,', '河北省-张家口市-怀安县', '43', '02', '04', '01', '539', '', null, null);
INSERT INTO t_sys_area VALUES ('540', '130729', '130729', '130729', '万全县', 'Wanquan Xian ', 'Wanquan Xian ', ',4,43,540,', '河北省-张家口市-万全县', '43', '02', '04', '01', '540', '', null, null);
INSERT INTO t_sys_area VALUES ('541', '130730', '130730', '130730', '怀来县', 'Huailai Xian', 'Huailai Xian', ',4,43,541,', '河北省-张家口市-怀来县', '43', '02', '04', '01', '541', '', null, null);
INSERT INTO t_sys_area VALUES ('542', '130731', '130731', '130731', '涿鹿县', 'Zhuolu Xian ', 'Zhuolu Xian ', ',4,43,542,', '河北省-张家口市-涿鹿县', '43', '02', '04', '01', '542', '', null, null);
INSERT INTO t_sys_area VALUES ('543', '130732', '130732', '130732', '赤城县', 'Chicheng Xian', 'Chicheng Xian', ',4,43,543,', '河北省-张家口市-赤城县', '43', '02', '04', '01', '543', '', null, null);
INSERT INTO t_sys_area VALUES ('544', '130733', '130733', '130733', '崇礼县', 'Chongli Xian', 'Chongli Xian', ',4,43,544,', '河北省-张家口市-崇礼县', '43', '02', '04', '01', '544', '', null, null);
INSERT INTO t_sys_area VALUES ('545', '130801', '130801', '130801', '市辖区', 'Shixiaqu', 'Shixiaqu', ',4,44,545,', '河北省-承德市-市辖区', '44', '02', '04', '01', '545', '', null, null);
INSERT INTO t_sys_area VALUES ('546', '130802', '130802', '130802', '双桥区', 'Shuangqiao Qu ', 'Shuangqiao Qu ', ',4,44,546,', '河北省-承德市-双桥区', '44', '02', '04', '01', '546', '', null, null);
INSERT INTO t_sys_area VALUES ('547', '130803', '130803', '130803', '双滦区', 'Shuangluan Qu', 'Shuangluan Qu', ',4,44,547,', '河北省-承德市-双滦区', '44', '02', '04', '01', '547', '', null, null);
INSERT INTO t_sys_area VALUES ('548', '130804', '130804', '130804', '鹰手营子矿区', 'Yingshouyingzi Kuangqu', 'Yingshouyingzi Kuangqu', ',4,44,548,', '河北省-承德市-鹰手营子矿区', '44', '02', '04', '01', '548', '', null, null);
INSERT INTO t_sys_area VALUES ('549', '130821', '130821', '130821', '承德县', 'Chengde Xian', 'Chengde Xian', ',4,44,549,', '河北省-承德市-承德县', '44', '02', '04', '01', '549', '', null, null);
INSERT INTO t_sys_area VALUES ('550', '130822', '130822', '130822', '兴隆县', 'Xinglong Xian', 'Xinglong Xian', ',4,44,550,', '河北省-承德市-兴隆县', '44', '02', '04', '01', '550', '', null, null);
INSERT INTO t_sys_area VALUES ('551', '130823', '130823', '130823', '平泉县', 'Pingquan Xian', 'Pingquan Xian', ',4,44,551,', '河北省-承德市-平泉县', '44', '02', '04', '01', '551', '', null, null);
INSERT INTO t_sys_area VALUES ('552', '130824', '130824', '130824', '滦平县', 'Luanping Xian ', 'Luanping Xian ', ',4,44,552,', '河北省-承德市-滦平县', '44', '02', '04', '01', '552', '', null, null);
INSERT INTO t_sys_area VALUES ('553', '130825', '130825', '130825', '隆化县', 'Longhua Xian', 'Longhua Xian', ',4,44,553,', '河北省-承德市-隆化县', '44', '02', '04', '01', '553', '', null, null);
INSERT INTO t_sys_area VALUES ('554', '130826', '130826', '130826', '丰宁满族自治县', 'Fengning Manzu Zizhixian', 'Fengning Manzu Zizhixian', ',4,44,554,', '河北省-承德市-丰宁满族自治县', '44', '02', '04', '01', '554', '', null, null);
INSERT INTO t_sys_area VALUES ('555', '130827', '130827', '130827', '宽城满族自治县', 'Kuancheng Manzu Zizhixian', 'Kuancheng Manzu Zizhixian', ',4,44,555,', '河北省-承德市-宽城满族自治县', '44', '02', '04', '01', '555', '', null, null);
INSERT INTO t_sys_area VALUES ('556', '130828', '130828', '130828', '围场满族蒙古族自治县', 'Weichang Manzu Menggolzu Zizhixian', 'Weichang Manzu Menggolzu Zizhixian', ',4,44,556,', '河北省-承德市-围场满族蒙古族自治县', '44', '02', '04', '01', '556', '', null, null);
INSERT INTO t_sys_area VALUES ('557', '130901', '130901', '130901', '市辖区', 'Shixiaqu', 'Shixiaqu', ',4,45,557,', '河北省-沧州市-市辖区', '45', '02', '04', '01', '557', '', null, null);
INSERT INTO t_sys_area VALUES ('558', '130902', '130902', '130902', '新华区', 'Xinhua Qu', 'Xinhua Qu', ',4,45,558,', '河北省-沧州市-新华区', '45', '02', '04', '01', '558', '', null, null);
INSERT INTO t_sys_area VALUES ('559', '130903', '130903', '130903', '运河区', 'Yunhe Qu', 'Yunhe Qu', ',4,45,559,', '河北省-沧州市-运河区', '45', '02', '04', '01', '559', '', null, null);
INSERT INTO t_sys_area VALUES ('560', '130921', '130921', '130921', '沧县', 'Cang Xian', 'Cang Xian', ',4,45,560,', '河北省-沧州市-沧县', '45', '02', '04', '01', '560', '', null, null);
INSERT INTO t_sys_area VALUES ('561', '130922', '130922', '130922', '青县', 'Qing Xian', 'Qing Xian', ',4,45,561,', '河北省-沧州市-青县', '45', '02', '04', '01', '561', '', null, null);
INSERT INTO t_sys_area VALUES ('562', '130923', '130923', '130923', '东光县', 'Dongguang Xian ', 'Dongguang Xian ', ',4,45,562,', '河北省-沧州市-东光县', '45', '02', '04', '01', '562', '', null, null);
INSERT INTO t_sys_area VALUES ('563', '130924', '130924', '130924', '海兴县', 'Haixing Xian', 'Haixing Xian', ',4,45,563,', '河北省-沧州市-海兴县', '45', '02', '04', '01', '563', '', null, null);
INSERT INTO t_sys_area VALUES ('564', '130925', '130925', '130925', '盐山县', 'Yanshan Xian', 'Yanshan Xian', ',4,45,564,', '河北省-沧州市-盐山县', '45', '02', '04', '01', '564', '', null, null);
INSERT INTO t_sys_area VALUES ('565', '130926', '130926', '130926', '肃宁县', 'Suning Xian ', 'Suning Xian ', ',4,45,565,', '河北省-沧州市-肃宁县', '45', '02', '04', '01', '565', '', null, null);
INSERT INTO t_sys_area VALUES ('566', '130927', '130927', '130927', '南皮县', 'Nanpi Xian', 'Nanpi Xian', ',4,45,566,', '河北省-沧州市-南皮县', '45', '02', '04', '01', '566', '', null, null);
INSERT INTO t_sys_area VALUES ('567', '130928', '130928', '130928', '吴桥县', 'Wuqiao Xian ', 'Wuqiao Xian ', ',4,45,567,', '河北省-沧州市-吴桥县', '45', '02', '04', '01', '567', '', null, null);
INSERT INTO t_sys_area VALUES ('568', '130929', '130929', '130929', '献县', 'Xian Xian ', 'Xian Xian ', ',4,45,568,', '河北省-沧州市-献县', '45', '02', '04', '01', '568', '', null, null);
INSERT INTO t_sys_area VALUES ('569', '130930', '130930', '130930', '孟村回族自治县', 'Mengcun Huizu Zizhixian', 'Mengcun Huizu Zizhixian', ',4,45,569,', '河北省-沧州市-孟村回族自治县', '45', '02', '04', '01', '569', '', null, null);
INSERT INTO t_sys_area VALUES ('570', '130981', '130981', '130981', '泊头市', 'Botou Shi ', 'Botou Shi ', ',4,45,570,', '河北省-沧州市-泊头市', '45', '02', '04', '01', '570', '', null, null);
INSERT INTO t_sys_area VALUES ('571', '130982', '130982', '130982', '任丘市', 'Renqiu Shi', 'Renqiu Shi', ',4,45,571,', '河北省-沧州市-任丘市', '45', '02', '04', '01', '571', '', null, null);
INSERT INTO t_sys_area VALUES ('572', '130983', '130983', '130983', '黄骅市', 'Huanghua Shi', 'Huanghua Shi', ',4,45,572,', '河北省-沧州市-黄骅市', '45', '02', '04', '01', '572', '', null, null);
INSERT INTO t_sys_area VALUES ('573', '130984', '130984', '130984', '河间市', 'Hejian Shi', 'Hejian Shi', ',4,45,573,', '河北省-沧州市-河间市', '45', '02', '04', '01', '573', '', null, null);
INSERT INTO t_sys_area VALUES ('574', '131001', '131001', '131001', '市辖区', 'Shixiaqu', 'Shixiaqu', ',4,46,574,', '河北省-廊坊市-市辖区', '46', '02', '04', '01', '574', '', null, null);
INSERT INTO t_sys_area VALUES ('575', '131002', '131002', '131002', '安次区', 'Anci Qu', 'Anci Qu', ',4,46,575,', '河北省-廊坊市-安次区', '46', '02', '04', '01', '575', '', null, null);
INSERT INTO t_sys_area VALUES ('576', '131003', '131003', '131003', '广阳区', 'Guangyang Qu', 'Guangyang Qu', ',4,46,576,', '河北省-廊坊市-广阳区', '46', '02', '04', '01', '576', '', null, null);
INSERT INTO t_sys_area VALUES ('577', '131022', '131022', '131022', '固安县', 'Gu,an Xian', 'Gu,an Xian', ',4,46,577,', '河北省-廊坊市-固安县', '46', '02', '04', '01', '577', '', null, null);
INSERT INTO t_sys_area VALUES ('578', '131023', '131023', '131023', '永清县', 'Yongqing Xian ', 'Yongqing Xian ', ',4,46,578,', '河北省-廊坊市-永清县', '46', '02', '04', '01', '578', '', null, null);
INSERT INTO t_sys_area VALUES ('579', '131024', '131024', '131024', '香河县', 'Xianghe Xian', 'Xianghe Xian', ',4,46,579,', '河北省-廊坊市-香河县', '46', '02', '04', '01', '579', '', null, null);
INSERT INTO t_sys_area VALUES ('580', '131025', '131025', '131025', '大城县', 'Dacheng Xian', 'Dacheng Xian', ',4,46,580,', '河北省-廊坊市-大城县', '46', '02', '04', '01', '580', '', null, null);
INSERT INTO t_sys_area VALUES ('581', '131026', '131026', '131026', '文安县', 'Wen,an Xian', 'Wen,an Xian', ',4,46,581,', '河北省-廊坊市-文安县', '46', '02', '04', '01', '581', '', null, null);
INSERT INTO t_sys_area VALUES ('582', '131028', '131028', '131028', '大厂回族自治县', 'Dachang Huizu Zizhixian', 'Dachang Huizu Zizhixian', ',4,46,582,', '河北省-廊坊市-大厂回族自治县', '46', '02', '04', '01', '582', '', null, null);
INSERT INTO t_sys_area VALUES ('583', '131081', '131081', '131081', '霸州市', 'Bazhou Shi', 'Bazhou Shi', ',4,46,583,', '河北省-廊坊市-霸州市', '46', '02', '04', '01', '583', '', null, null);
INSERT INTO t_sys_area VALUES ('584', '131082', '131082', '131082', '三河市', 'Sanhe Shi', 'Sanhe Shi', ',4,46,584,', '河北省-廊坊市-三河市', '46', '02', '04', '01', '584', '', null, null);
INSERT INTO t_sys_area VALUES ('585', '131101', '131101', '131101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',4,47,585,', '河北省-衡水市-市辖区', '47', '02', '04', '01', '585', '', null, null);
INSERT INTO t_sys_area VALUES ('586', '131102', '131102', '131102', '桃城区', 'Taocheng Qu', 'Taocheng Qu', ',4,47,586,', '河北省-衡水市-桃城区', '47', '02', '04', '01', '586', '', null, null);
INSERT INTO t_sys_area VALUES ('587', '131121', '131121', '131121', '枣强县', 'Zaoqiang Xian ', 'Zaoqiang Xian ', ',4,47,587,', '河北省-衡水市-枣强县', '47', '02', '04', '01', '587', '', null, null);
INSERT INTO t_sys_area VALUES ('588', '131122', '131122', '131122', '武邑县', 'Wuyi Xian', 'Wuyi Xian', ',4,47,588,', '河北省-衡水市-武邑县', '47', '02', '04', '01', '588', '', null, null);
INSERT INTO t_sys_area VALUES ('589', '131123', '131123', '131123', '武强县', 'Wuqiang Xian ', 'Wuqiang Xian ', ',4,47,589,', '河北省-衡水市-武强县', '47', '02', '04', '01', '589', '', null, null);
INSERT INTO t_sys_area VALUES ('590', '131124', '131124', '131124', '饶阳县', 'Raoyang Xian', 'Raoyang Xian', ',4,47,590,', '河北省-衡水市-饶阳县', '47', '02', '04', '01', '590', '', null, null);
INSERT INTO t_sys_area VALUES ('591', '131125', '131125', '131125', '安平县', 'Anping Xian', 'Anping Xian', ',4,47,591,', '河北省-衡水市-安平县', '47', '02', '04', '01', '591', '', null, null);
INSERT INTO t_sys_area VALUES ('592', '131126', '131126', '131126', '故城县', 'Gucheng Xian', 'Gucheng Xian', ',4,47,592,', '河北省-衡水市-故城县', '47', '02', '04', '01', '592', '', null, null);
INSERT INTO t_sys_area VALUES ('593', '131127', '131127', '131127', '景县', 'Jing Xian ', 'Jing Xian ', ',4,47,593,', '河北省-衡水市-景县', '47', '02', '04', '01', '593', '', null, null);
INSERT INTO t_sys_area VALUES ('594', '131128', '131128', '131128', '阜城县', 'Fucheng Xian ', 'Fucheng Xian ', ',4,47,594,', '河北省-衡水市-阜城县', '47', '02', '04', '01', '594', '', null, null);
INSERT INTO t_sys_area VALUES ('595', '131181', '131181', '131181', '冀州市', 'Jizhou Shi ', 'Jizhou Shi ', ',4,47,595,', '河北省-衡水市-冀州市', '47', '02', '04', '01', '595', '', null, null);
INSERT INTO t_sys_area VALUES ('596', '131182', '131182', '131182', '深州市', 'Shenzhou Shi', 'Shenzhou Shi', ',4,47,596,', '河北省-衡水市-深州市', '47', '02', '04', '01', '596', '', null, null);
INSERT INTO t_sys_area VALUES ('597', '140101', '140101', '140101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',5,48,597,', '山西省-太原市-市辖区', '48', '02', '04', '01', '597', '', null, null);
INSERT INTO t_sys_area VALUES ('598', '140105', '140105', '140105', '小店区', 'Xiaodian Qu', 'Xiaodian Qu', ',5,48,598,', '山西省-太原市-小店区', '48', '02', '04', '01', '598', '', null, null);
INSERT INTO t_sys_area VALUES ('599', '140106', '140106', '140106', '迎泽区', 'Yingze Qu', 'Yingze Qu', ',5,48,599,', '山西省-太原市-迎泽区', '48', '02', '04', '01', '599', '', null, null);
INSERT INTO t_sys_area VALUES ('600', '140107', '140107', '140107', '杏花岭区', 'Xinghualing Qu', 'Xinghualing Qu', ',5,48,600,', '山西省-太原市-杏花岭区', '48', '02', '04', '01', '600', '', null, null);
INSERT INTO t_sys_area VALUES ('601', '140108', '140108', '140108', '尖草坪区', 'Jiancaoping Qu', 'Jiancaoping Qu', ',5,48,601,', '山西省-太原市-尖草坪区', '48', '02', '04', '01', '601', '', null, null);
INSERT INTO t_sys_area VALUES ('602', '140109', '140109', '140109', '万柏林区', 'Wanbailin Qu', 'Wanbailin Qu', ',5,48,602,', '山西省-太原市-万柏林区', '48', '02', '04', '01', '602', '', null, null);
INSERT INTO t_sys_area VALUES ('603', '140110', '140110', '140110', '晋源区', 'Jinyuan Qu', 'Jinyuan Qu', ',5,48,603,', '山西省-太原市-晋源区', '48', '02', '04', '01', '603', '', null, null);
INSERT INTO t_sys_area VALUES ('604', '140121', '140121', '140121', '清徐县', 'Qingxu Xian ', 'Qingxu Xian ', ',5,48,604,', '山西省-太原市-清徐县', '48', '02', '04', '01', '604', '', null, null);
INSERT INTO t_sys_area VALUES ('605', '140122', '140122', '140122', '阳曲县', 'Yangqu Xian ', 'Yangqu Xian ', ',5,48,605,', '山西省-太原市-阳曲县', '48', '02', '04', '01', '605', '', null, null);
INSERT INTO t_sys_area VALUES ('606', '140123', '140123', '140123', '娄烦县', 'Loufan Xian', 'Loufan Xian', ',5,48,606,', '山西省-太原市-娄烦县', '48', '02', '04', '01', '606', '', null, null);
INSERT INTO t_sys_area VALUES ('607', '140181', '140181', '140181', '古交市', 'Gujiao Shi', 'Gujiao Shi', ',5,48,607,', '山西省-太原市-古交市', '48', '02', '04', '01', '607', '', null, null);
INSERT INTO t_sys_area VALUES ('608', '140201', '140201', '140201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',5,49,608,', '山西省-大同市-市辖区', '49', '02', '04', '01', '608', '', null, null);
INSERT INTO t_sys_area VALUES ('609', '140202', '140202', '140202', '城区', 'Chengqu', 'Chengqu', ',5,49,609,', '山西省-大同市-城区', '49', '02', '04', '01', '609', '', null, null);
INSERT INTO t_sys_area VALUES ('610', '140203', '140203', '140203', '矿区', 'Kuangqu', 'Kuangqu', ',5,49,610,', '山西省-大同市-矿区', '49', '02', '04', '01', '610', '', null, null);
INSERT INTO t_sys_area VALUES ('611', '140211', '140211', '140211', '南郊区', 'Nanjiao Qu', 'Nanjiao Qu', ',5,49,611,', '山西省-大同市-南郊区', '49', '02', '04', '01', '611', '', null, null);
INSERT INTO t_sys_area VALUES ('612', '140212', '140212', '140212', '新荣区', 'Xinrong Qu', 'Xinrong Qu', ',5,49,612,', '山西省-大同市-新荣区', '49', '02', '04', '01', '612', '', null, null);
INSERT INTO t_sys_area VALUES ('613', '140221', '140221', '140221', '阳高县', 'Yanggao Xian ', 'Yanggao Xian ', ',5,49,613,', '山西省-大同市-阳高县', '49', '02', '04', '01', '613', '', null, null);
INSERT INTO t_sys_area VALUES ('614', '140222', '140222', '140222', '天镇县', 'Tianzhen Xian ', 'Tianzhen Xian ', ',5,49,614,', '山西省-大同市-天镇县', '49', '02', '04', '01', '614', '', null, null);
INSERT INTO t_sys_area VALUES ('615', '140223', '140223', '140223', '广灵县', 'Guangling Xian ', 'Guangling Xian ', ',5,49,615,', '山西省-大同市-广灵县', '49', '02', '04', '01', '615', '', null, null);
INSERT INTO t_sys_area VALUES ('616', '140224', '140224', '140224', '灵丘县', 'Lingqiu Xian ', 'Lingqiu Xian ', ',5,49,616,', '山西省-大同市-灵丘县', '49', '02', '04', '01', '616', '', null, null);
INSERT INTO t_sys_area VALUES ('617', '140225', '140225', '140225', '浑源县', 'Hunyuan Xian', 'Hunyuan Xian', ',5,49,617,', '山西省-大同市-浑源县', '49', '02', '04', '01', '617', '', null, null);
INSERT INTO t_sys_area VALUES ('618', '140226', '140226', '140226', '左云县', 'Zuoyun Xian', 'Zuoyun Xian', ',5,49,618,', '山西省-大同市-左云县', '49', '02', '04', '01', '618', '', null, null);
INSERT INTO t_sys_area VALUES ('619', '140227', '140227', '140227', '大同县', 'Datong Xian ', 'Datong Xian ', ',5,49,619,', '山西省-大同市-大同县', '49', '02', '04', '01', '619', '', null, null);
INSERT INTO t_sys_area VALUES ('620', '140301', '140301', '140301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',5,50,620,', '山西省-阳泉市-市辖区', '50', '02', '04', '01', '620', '', null, null);
INSERT INTO t_sys_area VALUES ('621', '140302', '140302', '140302', '城区', 'Chengqu', 'Chengqu', ',5,50,621,', '山西省-阳泉市-城区', '50', '02', '04', '01', '621', '', null, null);
INSERT INTO t_sys_area VALUES ('622', '140303', '140303', '140303', '矿区', 'Kuangqu', 'Kuangqu', ',5,50,622,', '山西省-阳泉市-矿区', '50', '02', '04', '01', '622', '', null, null);
INSERT INTO t_sys_area VALUES ('623', '140311', '140311', '140311', '郊区', 'Jiaoqu', 'Jiaoqu', ',5,50,623,', '山西省-阳泉市-郊区', '50', '02', '04', '01', '623', '', null, null);
INSERT INTO t_sys_area VALUES ('624', '140321', '140321', '140321', '平定县', 'Pingding Xian', 'Pingding Xian', ',5,50,624,', '山西省-阳泉市-平定县', '50', '02', '04', '01', '624', '', null, null);
INSERT INTO t_sys_area VALUES ('625', '140322', '140322', '140322', '盂县', 'Yu Xian', 'Yu Xian', ',5,50,625,', '山西省-阳泉市-盂县', '50', '02', '04', '01', '625', '', null, null);
INSERT INTO t_sys_area VALUES ('626', '140401', '140401', '140401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',5,51,626,', '山西省-长治市-市辖区', '51', '02', '04', '01', '626', '', null, null);
INSERT INTO t_sys_area VALUES ('627', '140402', '140402', '140402', '城区', 'Chengqu ', 'Chengqu ', ',5,51,627,', '山西省-长治市-城区', '51', '02', '04', '01', '627', '', null, null);
INSERT INTO t_sys_area VALUES ('628', '140411', '140411', '140411', '郊区', 'Jiaoqu', 'Jiaoqu', ',5,51,628,', '山西省-长治市-郊区', '51', '02', '04', '01', '628', '', null, null);
INSERT INTO t_sys_area VALUES ('629', '140421', '140421', '140421', '长治县', 'Changzhi Xian', 'Changzhi Xian', ',5,51,629,', '山西省-长治市-长治县', '51', '02', '04', '01', '629', '', null, null);
INSERT INTO t_sys_area VALUES ('630', '140423', '140423', '140423', '襄垣县', 'Xiangyuan Xian', 'Xiangyuan Xian', ',5,51,630,', '山西省-长治市-襄垣县', '51', '02', '04', '01', '630', '', null, null);
INSERT INTO t_sys_area VALUES ('631', '140424', '140424', '140424', '屯留县', 'Tunliu Xian', 'Tunliu Xian', ',5,51,631,', '山西省-长治市-屯留县', '51', '02', '04', '01', '631', '', null, null);
INSERT INTO t_sys_area VALUES ('632', '140425', '140425', '140425', '平顺县', 'Pingshun Xian', 'Pingshun Xian', ',5,51,632,', '山西省-长治市-平顺县', '51', '02', '04', '01', '632', '', null, null);
INSERT INTO t_sys_area VALUES ('633', '140426', '140426', '140426', '黎城县', 'Licheng Xian', 'Licheng Xian', ',5,51,633,', '山西省-长治市-黎城县', '51', '02', '04', '01', '633', '', null, null);
INSERT INTO t_sys_area VALUES ('634', '140427', '140427', '140427', '壶关县', 'Huguan Xian', 'Huguan Xian', ',5,51,634,', '山西省-长治市-壶关县', '51', '02', '04', '01', '634', '', null, null);
INSERT INTO t_sys_area VALUES ('635', '140428', '140428', '140428', '长子县', 'Zhangzi Xian ', 'Zhangzi Xian ', ',5,51,635,', '山西省-长治市-长子县', '51', '02', '04', '01', '635', '', null, null);
INSERT INTO t_sys_area VALUES ('636', '140429', '140429', '140429', '武乡县', 'Wuxiang Xian', 'Wuxiang Xian', ',5,51,636,', '山西省-长治市-武乡县', '51', '02', '04', '01', '636', '', null, null);
INSERT INTO t_sys_area VALUES ('637', '140430', '140430', '140430', '沁县', 'Qin Xian', 'Qin Xian', ',5,51,637,', '山西省-长治市-沁县', '51', '02', '04', '01', '637', '', null, null);
INSERT INTO t_sys_area VALUES ('638', '140431', '140431', '140431', '沁源县', 'Qinyuan Xian ', 'Qinyuan Xian ', ',5,51,638,', '山西省-长治市-沁源县', '51', '02', '04', '01', '638', '', null, null);
INSERT INTO t_sys_area VALUES ('639', '140481', '140481', '140481', '潞城市', 'Lucheng Shi', 'Lucheng Shi', ',5,51,639,', '山西省-长治市-潞城市', '51', '02', '04', '01', '639', '', null, null);
INSERT INTO t_sys_area VALUES ('640', '140501', '140501', '140501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',5,52,640,', '山西省-晋城市-市辖区', '52', '02', '04', '01', '640', '', null, null);
INSERT INTO t_sys_area VALUES ('641', '140502', '140502', '140502', '城区', 'Chengqu ', 'Chengqu ', ',5,52,641,', '山西省-晋城市-城区', '52', '02', '04', '01', '641', '', null, null);
INSERT INTO t_sys_area VALUES ('642', '140521', '140521', '140521', '沁水县', 'Qinshui Xian', 'Qinshui Xian', ',5,52,642,', '山西省-晋城市-沁水县', '52', '02', '04', '01', '642', '', null, null);
INSERT INTO t_sys_area VALUES ('643', '140522', '140522', '140522', '阳城县', 'Yangcheng Xian ', 'Yangcheng Xian ', ',5,52,643,', '山西省-晋城市-阳城县', '52', '02', '04', '01', '643', '', null, null);
INSERT INTO t_sys_area VALUES ('644', '140524', '140524', '140524', '陵川县', 'Lingchuan Xian', 'Lingchuan Xian', ',5,52,644,', '山西省-晋城市-陵川县', '52', '02', '04', '01', '644', '', null, null);
INSERT INTO t_sys_area VALUES ('645', '140525', '140525', '140525', '泽州县', 'Zezhou Xian', 'Zezhou Xian', ',5,52,645,', '山西省-晋城市-泽州县', '52', '02', '04', '01', '645', '', null, null);
INSERT INTO t_sys_area VALUES ('646', '140581', '140581', '140581', '高平市', 'Gaoping Shi ', 'Gaoping Shi ', ',5,52,646,', '山西省-晋城市-高平市', '52', '02', '04', '01', '646', '', null, null);
INSERT INTO t_sys_area VALUES ('647', '140601', '140601', '140601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',5,53,647,', '山西省-朔州市-市辖区', '53', '02', '04', '01', '647', '', null, null);
INSERT INTO t_sys_area VALUES ('648', '140602', '140602', '140602', '朔城区', 'Shuocheng Qu', 'Shuocheng Qu', ',5,53,648,', '山西省-朔州市-朔城区', '53', '02', '04', '01', '648', '', null, null);
INSERT INTO t_sys_area VALUES ('649', '140603', '140603', '140603', '平鲁区', 'Pinglu Qu', 'Pinglu Qu', ',5,53,649,', '山西省-朔州市-平鲁区', '53', '02', '04', '01', '649', '', null, null);
INSERT INTO t_sys_area VALUES ('650', '140621', '140621', '140621', '山阴县', 'Shanyin Xian', 'Shanyin Xian', ',5,53,650,', '山西省-朔州市-山阴县', '53', '02', '04', '01', '650', '', null, null);
INSERT INTO t_sys_area VALUES ('651', '140622', '140622', '140622', '应县', 'Ying Xian', 'Ying Xian', ',5,53,651,', '山西省-朔州市-应县', '53', '02', '04', '01', '651', '', null, null);
INSERT INTO t_sys_area VALUES ('652', '140623', '140623', '140623', '右玉县', 'Youyu Xian ', 'Youyu Xian ', ',5,53,652,', '山西省-朔州市-右玉县', '53', '02', '04', '01', '652', '', null, null);
INSERT INTO t_sys_area VALUES ('653', '140624', '140624', '140624', '怀仁县', 'Huairen Xian', 'Huairen Xian', ',5,53,653,', '山西省-朔州市-怀仁县', '53', '02', '04', '01', '653', '', null, null);
INSERT INTO t_sys_area VALUES ('654', '140701', '140701', '140701', '市辖区', '1', '1', ',5,54,654,', '山西省-晋中市-市辖区', '54', '02', '04', '01', '654', '', null, null);
INSERT INTO t_sys_area VALUES ('655', '140702', '140702', '140702', '榆次区', 'Yuci Qu', 'Yuci Qu', ',5,54,655,', '山西省-晋中市-榆次区', '54', '02', '04', '01', '655', '', null, null);
INSERT INTO t_sys_area VALUES ('656', '140721', '140721', '140721', '榆社县', 'Yushe Xian', 'Yushe Xian', ',5,54,656,', '山西省-晋中市-榆社县', '54', '02', '04', '01', '656', '', null, null);
INSERT INTO t_sys_area VALUES ('657', '140722', '140722', '140722', '左权县', 'Zuoquan Xian', 'Zuoquan Xian', ',5,54,657,', '山西省-晋中市-左权县', '54', '02', '04', '01', '657', '', null, null);
INSERT INTO t_sys_area VALUES ('658', '140723', '140723', '140723', '和顺县', 'Heshun Xian', 'Heshun Xian', ',5,54,658,', '山西省-晋中市-和顺县', '54', '02', '04', '01', '658', '', null, null);
INSERT INTO t_sys_area VALUES ('659', '140724', '140724', '140724', '昔阳县', 'Xiyang Xian', 'Xiyang Xian', ',5,54,659,', '山西省-晋中市-昔阳县', '54', '02', '04', '01', '659', '', null, null);
INSERT INTO t_sys_area VALUES ('660', '140725', '140725', '140725', '寿阳县', 'Shouyang Xian', 'Shouyang Xian', ',5,54,660,', '山西省-晋中市-寿阳县', '54', '02', '04', '01', '660', '', null, null);
INSERT INTO t_sys_area VALUES ('661', '140726', '140726', '140726', '太谷县', 'Taigu Xian', 'Taigu Xian', ',5,54,661,', '山西省-晋中市-太谷县', '54', '02', '04', '01', '661', '', null, null);
INSERT INTO t_sys_area VALUES ('662', '140727', '140727', '140727', '祁县', 'Qi Xian', 'Qi Xian', ',5,54,662,', '山西省-晋中市-祁县', '54', '02', '04', '01', '662', '', null, null);
INSERT INTO t_sys_area VALUES ('663', '140728', '140728', '140728', '平遥县', 'Pingyao Xian', 'Pingyao Xian', ',5,54,663,', '山西省-晋中市-平遥县', '54', '02', '04', '01', '663', '', null, null);
INSERT INTO t_sys_area VALUES ('664', '140729', '140729', '140729', '灵石县', 'Lingshi Xian', 'Lingshi Xian', ',5,54,664,', '山西省-晋中市-灵石县', '54', '02', '04', '01', '664', '', null, null);
INSERT INTO t_sys_area VALUES ('665', '140781', '140781', '140781', '介休市', 'Jiexiu Shi', 'Jiexiu Shi', ',5,54,665,', '山西省-晋中市-介休市', '54', '02', '04', '01', '665', '', null, null);
INSERT INTO t_sys_area VALUES ('666', '140801', '140801', '140801', '市辖区', '1', '1', ',5,55,666,', '山西省-运城市-市辖区', '55', '02', '04', '01', '666', '', null, null);
INSERT INTO t_sys_area VALUES ('667', '140802', '140802', '140802', '盐湖区', 'Yanhu Qu', 'Yanhu Qu', ',5,55,667,', '山西省-运城市-盐湖区', '55', '02', '04', '01', '667', '', null, null);
INSERT INTO t_sys_area VALUES ('668', '140821', '140821', '140821', '临猗县', 'Linyi Xian', 'Linyi Xian', ',5,55,668,', '山西省-运城市-临猗县', '55', '02', '04', '01', '668', '', null, null);
INSERT INTO t_sys_area VALUES ('669', '140822', '140822', '140822', '万荣县', 'Wanrong Xian', 'Wanrong Xian', ',5,55,669,', '山西省-运城市-万荣县', '55', '02', '04', '01', '669', '', null, null);
INSERT INTO t_sys_area VALUES ('670', '140823', '140823', '140823', '闻喜县', 'Wenxi Xian', 'Wenxi Xian', ',5,55,670,', '山西省-运城市-闻喜县', '55', '02', '04', '01', '670', '', null, null);
INSERT INTO t_sys_area VALUES ('671', '140824', '140824', '140824', '稷山县', 'Jishan Xian', 'Jishan Xian', ',5,55,671,', '山西省-运城市-稷山县', '55', '02', '04', '01', '671', '', null, null);
INSERT INTO t_sys_area VALUES ('672', '140825', '140825', '140825', '新绛县', 'Xinjiang Xian', 'Xinjiang Xian', ',5,55,672,', '山西省-运城市-新绛县', '55', '02', '04', '01', '672', '', null, null);
INSERT INTO t_sys_area VALUES ('673', '140826', '140826', '140826', '绛县', 'Jiang Xian', 'Jiang Xian', ',5,55,673,', '山西省-运城市-绛县', '55', '02', '04', '01', '673', '', null, null);
INSERT INTO t_sys_area VALUES ('674', '140827', '140827', '140827', '垣曲县', 'Yuanqu Xian', 'Yuanqu Xian', ',5,55,674,', '山西省-运城市-垣曲县', '55', '02', '04', '01', '674', '', null, null);
INSERT INTO t_sys_area VALUES ('675', '140828', '140828', '140828', '夏县', 'Xia Xian ', 'Xia Xian ', ',5,55,675,', '山西省-运城市-夏县', '55', '02', '04', '01', '675', '', null, null);
INSERT INTO t_sys_area VALUES ('676', '140829', '140829', '140829', '平陆县', 'Pinglu Xian', 'Pinglu Xian', ',5,55,676,', '山西省-运城市-平陆县', '55', '02', '04', '01', '676', '', null, null);
INSERT INTO t_sys_area VALUES ('677', '140830', '140830', '140830', '芮城县', 'Ruicheng Xian', 'Ruicheng Xian', ',5,55,677,', '山西省-运城市-芮城县', '55', '02', '04', '01', '677', '', null, null);
INSERT INTO t_sys_area VALUES ('678', '140881', '140881', '140881', '永济市', 'Yongji Shi ', 'Yongji Shi ', ',5,55,678,', '山西省-运城市-永济市', '55', '02', '04', '01', '678', '', null, null);
INSERT INTO t_sys_area VALUES ('679', '140882', '140882', '140882', '河津市', 'Hejin Shi', 'Hejin Shi', ',5,55,679,', '山西省-运城市-河津市', '55', '02', '04', '01', '679', '', null, null);
INSERT INTO t_sys_area VALUES ('680', '140901', '140901', '140901', '市辖区', '1', '1', ',5,56,680,', '山西省-忻州市-市辖区', '56', '02', '04', '01', '680', '', null, null);
INSERT INTO t_sys_area VALUES ('681', '140902', '140902', '140902', '忻府区', 'Xinfu Qu', 'Xinfu Qu', ',5,56,681,', '山西省-忻州市-忻府区', '56', '02', '04', '01', '681', '', null, null);
INSERT INTO t_sys_area VALUES ('682', '140921', '140921', '140921', '定襄县', 'Dingxiang Xian', 'Dingxiang Xian', ',5,56,682,', '山西省-忻州市-定襄县', '56', '02', '04', '01', '682', '', null, null);
INSERT INTO t_sys_area VALUES ('683', '140922', '140922', '140922', '五台县', 'Wutai Xian', 'Wutai Xian', ',5,56,683,', '山西省-忻州市-五台县', '56', '02', '04', '01', '683', '', null, null);
INSERT INTO t_sys_area VALUES ('684', '140923', '140923', '140923', '代县', 'Dai Xian', 'Dai Xian', ',5,56,684,', '山西省-忻州市-代县', '56', '02', '04', '01', '684', '', null, null);
INSERT INTO t_sys_area VALUES ('685', '140924', '140924', '140924', '繁峙县', 'Fanshi Xian', 'Fanshi Xian', ',5,56,685,', '山西省-忻州市-繁峙县', '56', '02', '04', '01', '685', '', null, null);
INSERT INTO t_sys_area VALUES ('686', '140925', '140925', '140925', '宁武县', 'Ningwu Xian', 'Ningwu Xian', ',5,56,686,', '山西省-忻州市-宁武县', '56', '02', '04', '01', '686', '', null, null);
INSERT INTO t_sys_area VALUES ('687', '140926', '140926', '140926', '静乐县', 'Jingle Xian', 'Jingle Xian', ',5,56,687,', '山西省-忻州市-静乐县', '56', '02', '04', '01', '687', '', null, null);
INSERT INTO t_sys_area VALUES ('688', '140927', '140927', '140927', '神池县', 'Shenchi Xian', 'Shenchi Xian', ',5,56,688,', '山西省-忻州市-神池县', '56', '02', '04', '01', '688', '', null, null);
INSERT INTO t_sys_area VALUES ('689', '140928', '140928', '140928', '五寨县', 'Wuzhai Xian', 'Wuzhai Xian', ',5,56,689,', '山西省-忻州市-五寨县', '56', '02', '04', '01', '689', '', null, null);
INSERT INTO t_sys_area VALUES ('690', '140929', '140929', '140929', '岢岚县', 'Kelan Xian', 'Kelan Xian', ',5,56,690,', '山西省-忻州市-岢岚县', '56', '02', '04', '01', '690', '', null, null);
INSERT INTO t_sys_area VALUES ('691', '140930', '140930', '140930', '河曲县', 'Hequ Xian ', 'Hequ Xian ', ',5,56,691,', '山西省-忻州市-河曲县', '56', '02', '04', '01', '691', '', null, null);
INSERT INTO t_sys_area VALUES ('692', '140931', '140931', '140931', '保德县', 'Baode Xian', 'Baode Xian', ',5,56,692,', '山西省-忻州市-保德县', '56', '02', '04', '01', '692', '', null, null);
INSERT INTO t_sys_area VALUES ('693', '140932', '140932', '140932', '偏关县', 'Pianguan Xian', 'Pianguan Xian', ',5,56,693,', '山西省-忻州市-偏关县', '56', '02', '04', '01', '693', '', null, null);
INSERT INTO t_sys_area VALUES ('694', '140981', '140981', '140981', '原平市', 'Yuanping Shi', 'Yuanping Shi', ',5,56,694,', '山西省-忻州市-原平市', '56', '02', '04', '01', '694', '', null, null);
INSERT INTO t_sys_area VALUES ('695', '141001', '141001', '141001', '市辖区', '1', '1', ',5,57,695,', '山西省-临汾市-市辖区', '57', '02', '04', '01', '695', '', null, null);
INSERT INTO t_sys_area VALUES ('696', '141002', '141002', '141002', '尧都区', 'Yaodu Qu', 'Yaodu Qu', ',5,57,696,', '山西省-临汾市-尧都区', '57', '02', '04', '01', '696', '', null, null);
INSERT INTO t_sys_area VALUES ('697', '141021', '141021', '141021', '曲沃县', 'Quwo Xian ', 'Quwo Xian ', ',5,57,697,', '山西省-临汾市-曲沃县', '57', '02', '04', '01', '697', '', null, null);
INSERT INTO t_sys_area VALUES ('698', '141022', '141022', '141022', '翼城县', 'Yicheng Xian', 'Yicheng Xian', ',5,57,698,', '山西省-临汾市-翼城县', '57', '02', '04', '01', '698', '', null, null);
INSERT INTO t_sys_area VALUES ('699', '141023', '141023', '141023', '襄汾县', 'Xiangfen Xian', 'Xiangfen Xian', ',5,57,699,', '山西省-临汾市-襄汾县', '57', '02', '04', '01', '699', '', null, null);
INSERT INTO t_sys_area VALUES ('700', '141024', '141024', '141024', '洪洞县', 'Hongtong Xian', 'Hongtong Xian', ',5,57,700,', '山西省-临汾市-洪洞县', '57', '02', '04', '01', '700', '', null, null);
INSERT INTO t_sys_area VALUES ('701', '141025', '141025', '141025', '古县', 'Gu Xian', 'Gu Xian', ',5,57,701,', '山西省-临汾市-古县', '57', '02', '04', '01', '701', '', null, null);
INSERT INTO t_sys_area VALUES ('702', '141026', '141026', '141026', '安泽县', 'Anze Xian', 'Anze Xian', ',5,57,702,', '山西省-临汾市-安泽县', '57', '02', '04', '01', '702', '', null, null);
INSERT INTO t_sys_area VALUES ('703', '141027', '141027', '141027', '浮山县', 'Fushan Xian ', 'Fushan Xian ', ',5,57,703,', '山西省-临汾市-浮山县', '57', '02', '04', '01', '703', '', null, null);
INSERT INTO t_sys_area VALUES ('704', '141028', '141028', '141028', '吉县', 'Ji Xian', 'Ji Xian', ',5,57,704,', '山西省-临汾市-吉县', '57', '02', '04', '01', '704', '', null, null);
INSERT INTO t_sys_area VALUES ('705', '141029', '141029', '141029', '乡宁县', 'Xiangning Xian', 'Xiangning Xian', ',5,57,705,', '山西省-临汾市-乡宁县', '57', '02', '04', '01', '705', '', null, null);
INSERT INTO t_sys_area VALUES ('706', '141030', '141030', '141030', '大宁县', 'Daning Xian', 'Daning Xian', ',5,57,706,', '山西省-临汾市-大宁县', '57', '02', '04', '01', '706', '', null, null);
INSERT INTO t_sys_area VALUES ('707', '141031', '141031', '141031', '隰县', 'Xi Xian', 'Xi Xian', ',5,57,707,', '山西省-临汾市-隰县', '57', '02', '04', '01', '707', '', null, null);
INSERT INTO t_sys_area VALUES ('708', '141032', '141032', '141032', '永和县', 'Yonghe Xian', 'Yonghe Xian', ',5,57,708,', '山西省-临汾市-永和县', '57', '02', '04', '01', '708', '', null, null);
INSERT INTO t_sys_area VALUES ('709', '141033', '141033', '141033', '蒲县', 'Pu Xian', 'Pu Xian', ',5,57,709,', '山西省-临汾市-蒲县', '57', '02', '04', '01', '709', '', null, null);
INSERT INTO t_sys_area VALUES ('710', '141034', '141034', '141034', '汾西县', 'Fenxi Xian', 'Fenxi Xian', ',5,57,710,', '山西省-临汾市-汾西县', '57', '02', '04', '01', '710', '', null, null);
INSERT INTO t_sys_area VALUES ('711', '141081', '141081', '141081', '侯马市', 'Houma Shi ', 'Houma Shi ', ',5,57,711,', '山西省-临汾市-侯马市', '57', '02', '04', '01', '711', '', null, null);
INSERT INTO t_sys_area VALUES ('712', '141082', '141082', '141082', '霍州市', 'Huozhou Shi ', 'Huozhou Shi ', ',5,57,712,', '山西省-临汾市-霍州市', '57', '02', '04', '01', '712', '', null, null);
INSERT INTO t_sys_area VALUES ('713', '141101', '141101', '141101', '市辖区', '1', '1', ',5,58,713,', '山西省-吕梁市-市辖区', '58', '02', '04', '01', '713', '', null, null);
INSERT INTO t_sys_area VALUES ('714', '141102', '141102', '141102', '离石区', 'Lishi Qu', 'Lishi Qu', ',5,58,714,', '山西省-吕梁市-离石区', '58', '02', '04', '01', '714', '', null, null);
INSERT INTO t_sys_area VALUES ('715', '141121', '141121', '141121', '文水县', 'Wenshui Xian', 'Wenshui Xian', ',5,58,715,', '山西省-吕梁市-文水县', '58', '02', '04', '01', '715', '', null, null);
INSERT INTO t_sys_area VALUES ('716', '141122', '141122', '141122', '交城县', 'Jiaocheng Xian', 'Jiaocheng Xian', ',5,58,716,', '山西省-吕梁市-交城县', '58', '02', '04', '01', '716', '', null, null);
INSERT INTO t_sys_area VALUES ('717', '141123', '141123', '141123', '兴县', 'Xing Xian', 'Xing Xian', ',5,58,717,', '山西省-吕梁市-兴县', '58', '02', '04', '01', '717', '', null, null);
INSERT INTO t_sys_area VALUES ('718', '141124', '141124', '141124', '临县', 'Lin Xian ', 'Lin Xian ', ',5,58,718,', '山西省-吕梁市-临县', '58', '02', '04', '01', '718', '', null, null);
INSERT INTO t_sys_area VALUES ('719', '141125', '141125', '141125', '柳林县', 'Liulin Xian', 'Liulin Xian', ',5,58,719,', '山西省-吕梁市-柳林县', '58', '02', '04', '01', '719', '', null, null);
INSERT INTO t_sys_area VALUES ('720', '141126', '141126', '141126', '石楼县', 'Shilou Xian', 'Shilou Xian', ',5,58,720,', '山西省-吕梁市-石楼县', '58', '02', '04', '01', '720', '', null, null);
INSERT INTO t_sys_area VALUES ('721', '141127', '141127', '141127', '岚县', 'Lan Xian', 'Lan Xian', ',5,58,721,', '山西省-吕梁市-岚县', '58', '02', '04', '01', '721', '', null, null);
INSERT INTO t_sys_area VALUES ('722', '141128', '141128', '141128', '方山县', 'Fangshan Xian', 'Fangshan Xian', ',5,58,722,', '山西省-吕梁市-方山县', '58', '02', '04', '01', '722', '', null, null);
INSERT INTO t_sys_area VALUES ('723', '141129', '141129', '141129', '中阳县', 'Zhongyang Xian', 'Zhongyang Xian', ',5,58,723,', '山西省-吕梁市-中阳县', '58', '02', '04', '01', '723', '', null, null);
INSERT INTO t_sys_area VALUES ('724', '141130', '141130', '141130', '交口县', 'Jiaokou Xian', 'Jiaokou Xian', ',5,58,724,', '山西省-吕梁市-交口县', '58', '02', '04', '01', '724', '', null, null);
INSERT INTO t_sys_area VALUES ('725', '141181', '141181', '141181', '孝义市', 'Xiaoyi Shi', 'Xiaoyi Shi', ',5,58,725,', '山西省-吕梁市-孝义市', '58', '02', '04', '01', '725', '', null, null);
INSERT INTO t_sys_area VALUES ('726', '141182', '141182', '141182', '汾阳市', 'Fenyang Shi', 'Fenyang Shi', ',5,58,726,', '山西省-吕梁市-汾阳市', '58', '02', '04', '01', '726', '', null, null);
INSERT INTO t_sys_area VALUES ('727', '150101', '150101', '150101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',6,59,727,', '内蒙古自治区-呼和浩特市-市辖区', '59', '02', '04', '01', '727', '', null, null);
INSERT INTO t_sys_area VALUES ('728', '150102', '150102', '150102', '新城区', 'Xincheng Qu', 'Xincheng Qu', ',6,59,728,', '内蒙古自治区-呼和浩特市-新城区', '59', '02', '04', '01', '728', '', null, null);
INSERT INTO t_sys_area VALUES ('729', '150103', '150103', '150103', '回民区', 'Huimin Qu', 'Huimin Qu', ',6,59,729,', '内蒙古自治区-呼和浩特市-回民区', '59', '02', '04', '01', '729', '', null, null);
INSERT INTO t_sys_area VALUES ('730', '150104', '150104', '150104', '玉泉区', 'Yuquan Qu', 'Yuquan Qu', ',6,59,730,', '内蒙古自治区-呼和浩特市-玉泉区', '59', '02', '04', '01', '730', '', null, null);
INSERT INTO t_sys_area VALUES ('731', '150105', '150105', '150105', '赛罕区', 'Saihan Qu', 'Saihan Qu', ',6,59,731,', '内蒙古自治区-呼和浩特市-赛罕区', '59', '02', '04', '01', '731', '', null, null);
INSERT INTO t_sys_area VALUES ('732', '150121', '150121', '150121', '土默特左旗', 'Tumd Zuoqi', 'Tumd Zuoqi', ',6,59,732,', '内蒙古自治区-呼和浩特市-土默特左旗', '59', '02', '04', '01', '732', '', null, null);
INSERT INTO t_sys_area VALUES ('733', '150122', '150122', '150122', '托克托县', 'Togtoh Xian', 'Togtoh Xian', ',6,59,733,', '内蒙古自治区-呼和浩特市-托克托县', '59', '02', '04', '01', '733', '', null, null);
INSERT INTO t_sys_area VALUES ('734', '150123', '150123', '150123', '和林格尔县', 'Horinger Xian', 'Horinger Xian', ',6,59,734,', '内蒙古自治区-呼和浩特市-和林格尔县', '59', '02', '04', '01', '734', '', null, null);
INSERT INTO t_sys_area VALUES ('735', '150124', '150124', '150124', '清水河县', 'Qingshuihe Xian', 'Qingshuihe Xian', ',6,59,735,', '内蒙古自治区-呼和浩特市-清水河县', '59', '02', '04', '01', '735', '', null, null);
INSERT INTO t_sys_area VALUES ('736', '150125', '150125', '150125', '武川县', 'Wuchuan Xian', 'Wuchuan Xian', ',6,59,736,', '内蒙古自治区-呼和浩特市-武川县', '59', '02', '04', '01', '736', '', null, null);
INSERT INTO t_sys_area VALUES ('737', '150201', '150201', '150201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',6,60,737,', '内蒙古自治区-包头市-市辖区', '60', '02', '04', '01', '737', '', null, null);
INSERT INTO t_sys_area VALUES ('738', '150202', '150202', '150202', '东河区', 'Donghe Qu', 'Donghe Qu', ',6,60,738,', '内蒙古自治区-包头市-东河区', '60', '02', '04', '01', '738', '', null, null);
INSERT INTO t_sys_area VALUES ('739', '150203', '150203', '150203', '昆都仑区', 'Kundulun Qu', 'Kundulun Qu', ',6,60,739,', '内蒙古自治区-包头市-昆都仑区', '60', '02', '04', '01', '739', '', null, null);
INSERT INTO t_sys_area VALUES ('740', '150204', '150204', '150204', '青山区', 'Qingshan Qu', 'Qingshan Qu', ',6,60,740,', '内蒙古自治区-包头市-青山区', '60', '02', '04', '01', '740', '', null, null);
INSERT INTO t_sys_area VALUES ('741', '150205', '150205', '150205', '石拐区', 'Shiguai Qu', 'Shiguai Qu', ',6,60,741,', '内蒙古自治区-包头市-石拐区', '60', '02', '04', '01', '741', '', null, null);
INSERT INTO t_sys_area VALUES ('742', '150206', '150206', '150206', '白云鄂博矿区', 'Baiyun Kuangqu', 'Baiyun Kuangqu', ',6,60,742,', '内蒙古自治区-包头市-白云鄂博矿区', '60', '02', '04', '01', '742', '', null, null);
INSERT INTO t_sys_area VALUES ('743', '150207', '150207', '150207', '九原区', 'Jiuyuan Qu', 'Jiuyuan Qu', ',6,60,743,', '内蒙古自治区-包头市-九原区', '60', '02', '04', '01', '743', '', null, null);
INSERT INTO t_sys_area VALUES ('744', '150221', '150221', '150221', '土默特右旗', 'Tumd Youqi', 'Tumd Youqi', ',6,60,744,', '内蒙古自治区-包头市-土默特右旗', '60', '02', '04', '01', '744', '', null, null);
INSERT INTO t_sys_area VALUES ('745', '150222', '150222', '150222', '固阳县', 'Guyang Xian', 'Guyang Xian', ',6,60,745,', '内蒙古自治区-包头市-固阳县', '60', '02', '04', '01', '745', '', null, null);
INSERT INTO t_sys_area VALUES ('746', '150223', '150223', '150223', '达尔罕茂明安联合旗', 'Darhan Muminggan Lianheqi', 'Darhan Muminggan Lianheqi', ',6,60,746,', '内蒙古自治区-包头市-达尔罕茂明安联合旗', '60', '02', '04', '01', '746', '', null, null);
INSERT INTO t_sys_area VALUES ('747', '150301', '150301', '150301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',6,61,747,', '内蒙古自治区-乌海市-市辖区', '61', '02', '04', '01', '747', '', null, null);
INSERT INTO t_sys_area VALUES ('748', '150302', '150302', '150302', '海勃湾区', 'Haibowan Qu', 'Haibowan Qu', ',6,61,748,', '内蒙古自治区-乌海市-海勃湾区', '61', '02', '04', '01', '748', '', null, null);
INSERT INTO t_sys_area VALUES ('749', '150303', '150303', '150303', '海南区', 'Hainan Qu', 'Hainan Qu', ',6,61,749,', '内蒙古自治区-乌海市-海南区', '61', '02', '04', '01', '749', '', null, null);
INSERT INTO t_sys_area VALUES ('750', '150304', '150304', '150304', '乌达区', 'Ud Qu', 'Ud Qu', ',6,61,750,', '内蒙古自治区-乌海市-乌达区', '61', '02', '04', '01', '750', '', null, null);
INSERT INTO t_sys_area VALUES ('751', '150401', '150401', '150401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',6,62,751,', '内蒙古自治区-赤峰市-市辖区', '62', '02', '04', '01', '751', '', null, null);
INSERT INTO t_sys_area VALUES ('752', '150402', '150402', '150402', '红山区', 'Hongshan Qu', 'Hongshan Qu', ',6,62,752,', '内蒙古自治区-赤峰市-红山区', '62', '02', '04', '01', '752', '', null, null);
INSERT INTO t_sys_area VALUES ('753', '150403', '150403', '150403', '元宝山区', 'Yuanbaoshan Qu', 'Yuanbaoshan Qu', ',6,62,753,', '内蒙古自治区-赤峰市-元宝山区', '62', '02', '04', '01', '753', '', null, null);
INSERT INTO t_sys_area VALUES ('754', '150404', '150404', '150404', '松山区', 'Songshan Qu', 'Songshan Qu', ',6,62,754,', '内蒙古自治区-赤峰市-松山区', '62', '02', '04', '01', '754', '', null, null);
INSERT INTO t_sys_area VALUES ('755', '150421', '150421', '150421', '阿鲁科尔沁旗', 'Ar Horqin Qi', 'Ar Horqin Qi', ',6,62,755,', '内蒙古自治区-赤峰市-阿鲁科尔沁旗', '62', '02', '04', '01', '755', '', null, null);
INSERT INTO t_sys_area VALUES ('756', '150422', '150422', '150422', '巴林左旗', 'Bairin Zuoqi', 'Bairin Zuoqi', ',6,62,756,', '内蒙古自治区-赤峰市-巴林左旗', '62', '02', '04', '01', '756', '', null, null);
INSERT INTO t_sys_area VALUES ('757', '150423', '150423', '150423', '巴林右旗', 'Bairin Youqi', 'Bairin Youqi', ',6,62,757,', '内蒙古自治区-赤峰市-巴林右旗', '62', '02', '04', '01', '757', '', null, null);
INSERT INTO t_sys_area VALUES ('758', '150424', '150424', '150424', '林西县', 'Linxi Xian', 'Linxi Xian', ',6,62,758,', '内蒙古自治区-赤峰市-林西县', '62', '02', '04', '01', '758', '', null, null);
INSERT INTO t_sys_area VALUES ('759', '150425', '150425', '150425', '克什克腾旗', 'Hexigten Qi', 'Hexigten Qi', ',6,62,759,', '内蒙古自治区-赤峰市-克什克腾旗', '62', '02', '04', '01', '759', '', null, null);
INSERT INTO t_sys_area VALUES ('760', '150426', '150426', '150426', '翁牛特旗', 'Ongniud Qi', 'Ongniud Qi', ',6,62,760,', '内蒙古自治区-赤峰市-翁牛特旗', '62', '02', '04', '01', '760', '', null, null);
INSERT INTO t_sys_area VALUES ('761', '150428', '150428', '150428', '喀喇沁旗', 'Harqin Qi', 'Harqin Qi', ',6,62,761,', '内蒙古自治区-赤峰市-喀喇沁旗', '62', '02', '04', '01', '761', '', null, null);
INSERT INTO t_sys_area VALUES ('762', '150429', '150429', '150429', '宁城县', 'Ningcheng Xian', 'Ningcheng Xian', ',6,62,762,', '内蒙古自治区-赤峰市-宁城县', '62', '02', '04', '01', '762', '', null, null);
INSERT INTO t_sys_area VALUES ('763', '150430', '150430', '150430', '敖汉旗', 'Aohan Qi', 'Aohan Qi', ',6,62,763,', '内蒙古自治区-赤峰市-敖汉旗', '62', '02', '04', '01', '763', '', null, null);
INSERT INTO t_sys_area VALUES ('764', '150501', '150501', '150501', '市辖区', '1', '1', ',6,63,764,', '内蒙古自治区-通辽市-市辖区', '63', '02', '04', '01', '764', '', null, null);
INSERT INTO t_sys_area VALUES ('765', '150502', '150502', '150502', '科尔沁区', 'Keermi Qu', 'Keermi Qu', ',6,63,765,', '内蒙古自治区-通辽市-科尔沁区', '63', '02', '04', '01', '765', '', null, null);
INSERT INTO t_sys_area VALUES ('766', '150521', '150521', '150521', '科尔沁左翼中旗', 'Horqin Zuoyi Zhongqi', 'Horqin Zuoyi Zhongqi', ',6,63,766,', '内蒙古自治区-通辽市-科尔沁左翼中旗', '63', '02', '04', '01', '766', '', null, null);
INSERT INTO t_sys_area VALUES ('767', '150522', '150522', '150522', '科尔沁左翼后旗', 'Horqin Zuoyi Houqi', 'Horqin Zuoyi Houqi', ',6,63,767,', '内蒙古自治区-通辽市-科尔沁左翼后旗', '63', '02', '04', '01', '767', '', null, null);
INSERT INTO t_sys_area VALUES ('768', '150523', '150523', '150523', '开鲁县', 'Kailu Xian', 'Kailu Xian', ',6,63,768,', '内蒙古自治区-通辽市-开鲁县', '63', '02', '04', '01', '768', '', null, null);
INSERT INTO t_sys_area VALUES ('769', '150524', '150524', '150524', '库伦旗', 'Hure Qi', 'Hure Qi', ',6,63,769,', '内蒙古自治区-通辽市-库伦旗', '63', '02', '04', '01', '769', '', null, null);
INSERT INTO t_sys_area VALUES ('770', '150525', '150525', '150525', '奈曼旗', 'Naiman Qi', 'Naiman Qi', ',6,63,770,', '内蒙古自治区-通辽市-奈曼旗', '63', '02', '04', '01', '770', '', null, null);
INSERT INTO t_sys_area VALUES ('771', '150526', '150526', '150526', '扎鲁特旗', 'Jarud Qi', 'Jarud Qi', ',6,63,771,', '内蒙古自治区-通辽市-扎鲁特旗', '63', '02', '04', '01', '771', '', null, null);
INSERT INTO t_sys_area VALUES ('772', '150581', '150581', '150581', '霍林郭勒市', 'Holingol Shi', 'Holingol Shi', ',6,63,772,', '内蒙古自治区-通辽市-霍林郭勒市', '63', '02', '04', '01', '772', '', null, null);
INSERT INTO t_sys_area VALUES ('773', '150602', '150602', '150602', '东胜区', 'Dongsheng Qu', 'Dongsheng Qu', ',6,64,773,', '内蒙古自治区-鄂尔多斯市-东胜区', '64', '02', '04', '01', '773', '', null, null);
INSERT INTO t_sys_area VALUES ('774', '150621', '150621', '150621', '达拉特旗', 'Dalad Qi', 'Dalad Qi', ',6,64,774,', '内蒙古自治区-鄂尔多斯市-达拉特旗', '64', '02', '04', '01', '774', '', null, null);
INSERT INTO t_sys_area VALUES ('775', '150622', '150622', '150622', '准格尔旗', 'Jungar Qi', 'Jungar Qi', ',6,64,775,', '内蒙古自治区-鄂尔多斯市-准格尔旗', '64', '02', '04', '01', '775', '', null, null);
INSERT INTO t_sys_area VALUES ('776', '150623', '150623', '150623', '鄂托克前旗', 'Otog Qianqi', 'Otog Qianqi', ',6,64,776,', '内蒙古自治区-鄂尔多斯市-鄂托克前旗', '64', '02', '04', '01', '776', '', null, null);
INSERT INTO t_sys_area VALUES ('777', '150624', '150624', '150624', '鄂托克旗', 'Otog Qi', 'Otog Qi', ',6,64,777,', '内蒙古自治区-鄂尔多斯市-鄂托克旗', '64', '02', '04', '01', '777', '', null, null);
INSERT INTO t_sys_area VALUES ('778', '150625', '150625', '150625', '杭锦旗', 'Hanggin Qi', 'Hanggin Qi', ',6,64,778,', '内蒙古自治区-鄂尔多斯市-杭锦旗', '64', '02', '04', '01', '778', '', null, null);
INSERT INTO t_sys_area VALUES ('779', '150626', '150626', '150626', '乌审旗', 'Uxin Qi', 'Uxin Qi', ',6,64,779,', '内蒙古自治区-鄂尔多斯市-乌审旗', '64', '02', '04', '01', '779', '', null, null);
INSERT INTO t_sys_area VALUES ('780', '150627', '150627', '150627', '伊金霍洛旗', 'Ejin Horo Qi', 'Ejin Horo Qi', ',6,64,780,', '内蒙古自治区-鄂尔多斯市-伊金霍洛旗', '64', '02', '04', '01', '780', '', null, null);
INSERT INTO t_sys_area VALUES ('781', '150701', '150701', '150701', '市辖区', '1', '1', ',6,65,781,', '内蒙古自治区-呼伦贝尔市-市辖区', '65', '02', '04', '01', '781', '', null, null);
INSERT INTO t_sys_area VALUES ('782', '150702', '150702', '150702', '海拉尔区', 'Hailaer Qu', 'Hailaer Qu', ',6,65,782,', '内蒙古自治区-呼伦贝尔市-海拉尔区', '65', '02', '04', '01', '782', '', null, null);
INSERT INTO t_sys_area VALUES ('783', '150721', '150721', '150721', '阿荣旗', 'Arun Qi', 'Arun Qi', ',6,65,783,', '内蒙古自治区-呼伦贝尔市-阿荣旗', '65', '02', '04', '01', '783', '', null, null);
INSERT INTO t_sys_area VALUES ('784', '150722', '150722', '150722', '莫力达瓦达斡尔族自治旗', 'Morin Dawa Daurzu Zizhiqi', 'Morin Dawa Daurzu Zizhiqi', ',6,65,784,', '内蒙古自治区-呼伦贝尔市-莫力达瓦达斡尔族自治旗', '65', '02', '04', '01', '784', '', null, null);
INSERT INTO t_sys_area VALUES ('785', '150723', '150723', '150723', '鄂伦春自治旗', 'Oroqen Zizhiqi', 'Oroqen Zizhiqi', ',6,65,785,', '内蒙古自治区-呼伦贝尔市-鄂伦春自治旗', '65', '02', '04', '01', '785', '', null, null);
INSERT INTO t_sys_area VALUES ('786', '150724', '150724', '150724', '鄂温克族自治旗', 'Ewenkizu Zizhiqi', 'Ewenkizu Zizhiqi', ',6,65,786,', '内蒙古自治区-呼伦贝尔市-鄂温克族自治旗', '65', '02', '04', '01', '786', '', null, null);
INSERT INTO t_sys_area VALUES ('787', '150725', '150725', '150725', '陈巴尔虎旗', 'Chen Barag Qi', 'Chen Barag Qi', ',6,65,787,', '内蒙古自治区-呼伦贝尔市-陈巴尔虎旗', '65', '02', '04', '01', '787', '', null, null);
INSERT INTO t_sys_area VALUES ('788', '150726', '150726', '150726', '新巴尔虎左旗', 'Xin Barag Zuoqi', 'Xin Barag Zuoqi', ',6,65,788,', '内蒙古自治区-呼伦贝尔市-新巴尔虎左旗', '65', '02', '04', '01', '788', '', null, null);
INSERT INTO t_sys_area VALUES ('789', '150727', '150727', '150727', '新巴尔虎右旗', 'Xin Barag Youqi', 'Xin Barag Youqi', ',6,65,789,', '内蒙古自治区-呼伦贝尔市-新巴尔虎右旗', '65', '02', '04', '01', '789', '', null, null);
INSERT INTO t_sys_area VALUES ('790', '150781', '150781', '150781', '满洲里市', 'Manzhouli Shi', 'Manzhouli Shi', ',6,65,790,', '内蒙古自治区-呼伦贝尔市-满洲里市', '65', '02', '04', '01', '790', '', null, null);
INSERT INTO t_sys_area VALUES ('791', '150782', '150782', '150782', '牙克石市', 'Yakeshi Shi', 'Yakeshi Shi', ',6,65,791,', '内蒙古自治区-呼伦贝尔市-牙克石市', '65', '02', '04', '01', '791', '', null, null);
INSERT INTO t_sys_area VALUES ('792', '150783', '150783', '150783', '扎兰屯市', 'Zalantun Shi', 'Zalantun Shi', ',6,65,792,', '内蒙古自治区-呼伦贝尔市-扎兰屯市', '65', '02', '04', '01', '792', '', null, null);
INSERT INTO t_sys_area VALUES ('793', '150784', '150784', '150784', '额尔古纳市', 'Ergun Shi', 'Ergun Shi', ',6,65,793,', '内蒙古自治区-呼伦贝尔市-额尔古纳市', '65', '02', '04', '01', '793', '', null, null);
INSERT INTO t_sys_area VALUES ('794', '150785', '150785', '150785', '根河市', 'Genhe Shi', 'Genhe Shi', ',6,65,794,', '内蒙古自治区-呼伦贝尔市-根河市', '65', '02', '04', '01', '794', '', null, null);
INSERT INTO t_sys_area VALUES ('795', '150801', '150801', '150801', '市辖区', '1', '1', ',6,66,795,', '内蒙古自治区-巴彦淖尔市-市辖区', '66', '02', '04', '01', '795', '', null, null);
INSERT INTO t_sys_area VALUES ('796', '150802', '150802', '150802', '临河区', 'Linhe Qu', 'Linhe Qu', ',6,66,796,', '内蒙古自治区-巴彦淖尔市-临河区', '66', '02', '04', '01', '796', '', null, null);
INSERT INTO t_sys_area VALUES ('797', '150821', '150821', '150821', '五原县', 'Wuyuan Xian', 'Wuyuan Xian', ',6,66,797,', '内蒙古自治区-巴彦淖尔市-五原县', '66', '02', '04', '01', '797', '', null, null);
INSERT INTO t_sys_area VALUES ('798', '150822', '150822', '150822', '磴口县', 'Dengkou Xian', 'Dengkou Xian', ',6,66,798,', '内蒙古自治区-巴彦淖尔市-磴口县', '66', '02', '04', '01', '798', '', null, null);
INSERT INTO t_sys_area VALUES ('799', '150823', '150823', '150823', '乌拉特前旗', 'Urad Qianqi', 'Urad Qianqi', ',6,66,799,', '内蒙古自治区-巴彦淖尔市-乌拉特前旗', '66', '02', '04', '01', '799', '', null, null);
INSERT INTO t_sys_area VALUES ('800', '150824', '150824', '150824', '乌拉特中旗', 'Urad Zhongqi', 'Urad Zhongqi', ',6,66,800,', '内蒙古自治区-巴彦淖尔市-乌拉特中旗', '66', '02', '04', '01', '800', '', null, null);
INSERT INTO t_sys_area VALUES ('801', '150825', '150825', '150825', '乌拉特后旗', 'Urad Houqi', 'Urad Houqi', ',6,66,801,', '内蒙古自治区-巴彦淖尔市-乌拉特后旗', '66', '02', '04', '01', '801', '', null, null);
INSERT INTO t_sys_area VALUES ('802', '150826', '150826', '150826', '杭锦后旗', 'Hanggin Houqi', 'Hanggin Houqi', ',6,66,802,', '内蒙古自治区-巴彦淖尔市-杭锦后旗', '66', '02', '04', '01', '802', '', null, null);
INSERT INTO t_sys_area VALUES ('803', '150901', '150901', '150901', '市辖区', '1', '1', ',6,67,803,', '内蒙古自治区-乌兰察布市-市辖区', '67', '02', '04', '01', '803', '', null, null);
INSERT INTO t_sys_area VALUES ('804', '150902', '150902', '150902', '集宁区', 'Jining Qu', 'Jining Qu', ',6,67,804,', '内蒙古自治区-乌兰察布市-集宁区', '67', '02', '04', '01', '804', '', null, null);
INSERT INTO t_sys_area VALUES ('805', '150921', '150921', '150921', '卓资县', 'Zhuozi Xian', 'Zhuozi Xian', ',6,67,805,', '内蒙古自治区-乌兰察布市-卓资县', '67', '02', '04', '01', '805', '', null, null);
INSERT INTO t_sys_area VALUES ('806', '150922', '150922', '150922', '化德县', 'Huade Xian', 'Huade Xian', ',6,67,806,', '内蒙古自治区-乌兰察布市-化德县', '67', '02', '04', '01', '806', '', null, null);
INSERT INTO t_sys_area VALUES ('807', '150923', '150923', '150923', '商都县', 'Shangdu Xian', 'Shangdu Xian', ',6,67,807,', '内蒙古自治区-乌兰察布市-商都县', '67', '02', '04', '01', '807', '', null, null);
INSERT INTO t_sys_area VALUES ('808', '150924', '150924', '150924', '兴和县', 'Xinghe Xian', 'Xinghe Xian', ',6,67,808,', '内蒙古自治区-乌兰察布市-兴和县', '67', '02', '04', '01', '808', '', null, null);
INSERT INTO t_sys_area VALUES ('809', '150925', '150925', '150925', '凉城县', 'Liangcheng Xian', 'Liangcheng Xian', ',6,67,809,', '内蒙古自治区-乌兰察布市-凉城县', '67', '02', '04', '01', '809', '', null, null);
INSERT INTO t_sys_area VALUES ('810', '150926', '150926', '150926', '察哈尔右翼前旗', 'Qahar Youyi Qianqi', 'Qahar Youyi Qianqi', ',6,67,810,', '内蒙古自治区-乌兰察布市-察哈尔右翼前旗', '67', '02', '04', '01', '810', '', null, null);
INSERT INTO t_sys_area VALUES ('811', '150927', '150927', '150927', '察哈尔右翼中旗', 'Qahar Youyi Zhongqi', 'Qahar Youyi Zhongqi', ',6,67,811,', '内蒙古自治区-乌兰察布市-察哈尔右翼中旗', '67', '02', '04', '01', '811', '', null, null);
INSERT INTO t_sys_area VALUES ('812', '150928', '150928', '150928', '察哈尔右翼后旗', 'Qahar Youyi Houqi', 'Qahar Youyi Houqi', ',6,67,812,', '内蒙古自治区-乌兰察布市-察哈尔右翼后旗', '67', '02', '04', '01', '812', '', null, null);
INSERT INTO t_sys_area VALUES ('813', '150929', '150929', '150929', '四子王旗', 'Dorbod Qi', 'Dorbod Qi', ',6,67,813,', '内蒙古自治区-乌兰察布市-四子王旗', '67', '02', '04', '01', '813', '', null, null);
INSERT INTO t_sys_area VALUES ('814', '150981', '150981', '150981', '丰镇市', 'Fengzhen Shi', 'Fengzhen Shi', ',6,67,814,', '内蒙古自治区-乌兰察布市-丰镇市', '67', '02', '04', '01', '814', '', null, null);
INSERT INTO t_sys_area VALUES ('815', '152201', '152201', '152201', '乌兰浩特市', 'Ulan Hot Shi', 'Ulan Hot Shi', ',6,68,815,', '内蒙古自治区-兴安盟-乌兰浩特市', '68', '02', '04', '01', '815', '', null, null);
INSERT INTO t_sys_area VALUES ('816', '152202', '152202', '152202', '阿尔山市', 'Arxan Shi', 'Arxan Shi', ',6,68,816,', '内蒙古自治区-兴安盟-阿尔山市', '68', '02', '04', '01', '816', '', null, null);
INSERT INTO t_sys_area VALUES ('817', '152221', '152221', '152221', '科尔沁右翼前旗', 'Horqin Youyi Qianqi', 'Horqin Youyi Qianqi', ',6,68,817,', '内蒙古自治区-兴安盟-科尔沁右翼前旗', '68', '02', '04', '01', '817', '', null, null);
INSERT INTO t_sys_area VALUES ('818', '152222', '152222', '152222', '科尔沁右翼中旗', 'Horqin Youyi Zhongqi', 'Horqin Youyi Zhongqi', ',6,68,818,', '内蒙古自治区-兴安盟-科尔沁右翼中旗', '68', '02', '04', '01', '818', '', null, null);
INSERT INTO t_sys_area VALUES ('819', '152223', '152223', '152223', '扎赉特旗', 'Jalaid Qi', 'Jalaid Qi', ',6,68,819,', '内蒙古自治区-兴安盟-扎赉特旗', '68', '02', '04', '01', '819', '', null, null);
INSERT INTO t_sys_area VALUES ('820', '152224', '152224', '152224', '突泉县', 'Tuquan Xian', 'Tuquan Xian', ',6,68,820,', '内蒙古自治区-兴安盟-突泉县', '68', '02', '04', '01', '820', '', null, null);
INSERT INTO t_sys_area VALUES ('821', '152501', '152501', '152501', '二连浩特市', 'Erenhot Shi', 'Erenhot Shi', ',6,69,821,', '内蒙古自治区-锡林郭勒盟-二连浩特市', '69', '02', '04', '01', '821', '', null, null);
INSERT INTO t_sys_area VALUES ('822', '152502', '152502', '152502', '锡林浩特市', 'Xilinhot Shi', 'Xilinhot Shi', ',6,69,822,', '内蒙古自治区-锡林郭勒盟-锡林浩特市', '69', '02', '04', '01', '822', '', null, null);
INSERT INTO t_sys_area VALUES ('823', '152522', '152522', '152522', '阿巴嘎旗', 'Abag Qi', 'Abag Qi', ',6,69,823,', '内蒙古自治区-锡林郭勒盟-阿巴嘎旗', '69', '02', '04', '01', '823', '', null, null);
INSERT INTO t_sys_area VALUES ('824', '152523', '152523', '152523', '苏尼特左旗', 'Sonid Zuoqi', 'Sonid Zuoqi', ',6,69,824,', '内蒙古自治区-锡林郭勒盟-苏尼特左旗', '69', '02', '04', '01', '824', '', null, null);
INSERT INTO t_sys_area VALUES ('825', '152524', '152524', '152524', '苏尼特右旗', 'Sonid Youqi', 'Sonid Youqi', ',6,69,825,', '内蒙古自治区-锡林郭勒盟-苏尼特右旗', '69', '02', '04', '01', '825', '', null, null);
INSERT INTO t_sys_area VALUES ('826', '152525', '152525', '152525', '东乌珠穆沁旗', 'Dong Ujimqin Qi', 'Dong Ujimqin Qi', ',6,69,826,', '内蒙古自治区-锡林郭勒盟-东乌珠穆沁旗', '69', '02', '04', '01', '826', '', null, null);
INSERT INTO t_sys_area VALUES ('827', '152526', '152526', '152526', '西乌珠穆沁旗', 'Xi Ujimqin Qi', 'Xi Ujimqin Qi', ',6,69,827,', '内蒙古自治区-锡林郭勒盟-西乌珠穆沁旗', '69', '02', '04', '01', '827', '', null, null);
INSERT INTO t_sys_area VALUES ('828', '152527', '152527', '152527', '太仆寺旗', 'Taibus Qi', 'Taibus Qi', ',6,69,828,', '内蒙古自治区-锡林郭勒盟-太仆寺旗', '69', '02', '04', '01', '828', '', null, null);
INSERT INTO t_sys_area VALUES ('829', '152528', '152528', '152528', '镶黄旗', 'Xianghuang(Hobot Xar) Qi', 'Xianghuang(Hobot Xar) Qi', ',6,69,829,', '内蒙古自治区-锡林郭勒盟-镶黄旗', '69', '02', '04', '01', '829', '', null, null);
INSERT INTO t_sys_area VALUES ('830', '152529', '152529', '152529', '正镶白旗', 'Zhengxiangbai(Xulun Hobot Qagan)Qi', 'Zhengxiangbai(Xulun Hobot Qagan)Qi', ',6,69,830,', '内蒙古自治区-锡林郭勒盟-正镶白旗', '69', '02', '04', '01', '830', '', null, null);
INSERT INTO t_sys_area VALUES ('831', '152530', '152530', '152530', '正蓝旗', 'Zhenglan(Xulun Hoh)Qi', 'Zhenglan(Xulun Hoh)Qi', ',6,69,831,', '内蒙古自治区-锡林郭勒盟-正蓝旗', '69', '02', '04', '01', '831', '', null, null);
INSERT INTO t_sys_area VALUES ('832', '152531', '152531', '152531', '多伦县', 'Duolun (Dolonnur)Xian', 'Duolun (Dolonnur)Xian', ',6,69,832,', '内蒙古自治区-锡林郭勒盟-多伦县', '69', '02', '04', '01', '832', '', null, null);
INSERT INTO t_sys_area VALUES ('833', '152921', '152921', '152921', '阿拉善左旗', 'Alxa Zuoqi', 'Alxa Zuoqi', ',6,70,833,', '内蒙古自治区-阿拉善盟-阿拉善左旗', '70', '02', '04', '01', '833', '', null, null);
INSERT INTO t_sys_area VALUES ('834', '152922', '152922', '152922', '阿拉善右旗', 'Alxa Youqi', 'Alxa Youqi', ',6,70,834,', '内蒙古自治区-阿拉善盟-阿拉善右旗', '70', '02', '04', '01', '834', '', null, null);
INSERT INTO t_sys_area VALUES ('835', '152923', '152923', '152923', '额济纳旗', 'Ejin Qi', 'Ejin Qi', ',6,70,835,', '内蒙古自治区-阿拉善盟-额济纳旗', '70', '02', '04', '01', '835', '', null, null);
INSERT INTO t_sys_area VALUES ('836', '210101', '210101', '210101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',7,71,836,', '辽宁省-沈阳市-市辖区', '71', '02', '04', '01', '836', '', null, null);
INSERT INTO t_sys_area VALUES ('837', '210102', '210102', '210102', '和平区', 'Heping Qu', 'Heping Qu', ',7,71,837,', '辽宁省-沈阳市-和平区', '71', '02', '04', '01', '837', '', null, null);
INSERT INTO t_sys_area VALUES ('838', '210103', '210103', '210103', '沈河区', 'Shenhe Qu ', 'Shenhe Qu ', ',7,71,838,', '辽宁省-沈阳市-沈河区', '71', '02', '04', '01', '838', '', null, null);
INSERT INTO t_sys_area VALUES ('839', '210104', '210104', '210104', '大东区', 'Dadong Qu ', 'Dadong Qu ', ',7,71,839,', '辽宁省-沈阳市-大东区', '71', '02', '04', '01', '839', '', null, null);
INSERT INTO t_sys_area VALUES ('840', '210105', '210105', '210105', '皇姑区', 'Huanggu Qu', 'Huanggu Qu', ',7,71,840,', '辽宁省-沈阳市-皇姑区', '71', '02', '04', '01', '840', '', null, null);
INSERT INTO t_sys_area VALUES ('841', '210106', '210106', '210106', '铁西区', 'Tiexi Qu', 'Tiexi Qu', ',7,71,841,', '辽宁省-沈阳市-铁西区', '71', '02', '04', '01', '841', '', null, null);
INSERT INTO t_sys_area VALUES ('842', '210111', '210111', '210111', '苏家屯区', 'Sujiatun Qu', 'Sujiatun Qu', ',7,71,842,', '辽宁省-沈阳市-苏家屯区', '71', '02', '04', '01', '842', '', null, null);
INSERT INTO t_sys_area VALUES ('843', '210112', '210112', '210112', '东陵区', 'Dongling Qu ', 'Dongling Qu ', ',7,71,843,', '辽宁省-沈阳市-东陵区', '71', '02', '04', '01', '843', '', null, null);
INSERT INTO t_sys_area VALUES ('844', '210113', '210113', '210113', '沈北新区', 'Xinchengzi Qu', 'Xinchengzi Qu', ',7,71,844,', '辽宁省-沈阳市-沈北新区', '71', '02', '04', '01', '844', '', null, null);
INSERT INTO t_sys_area VALUES ('845', '210114', '210114', '210114', '于洪区', 'Yuhong Qu ', 'Yuhong Qu ', ',7,71,845,', '辽宁省-沈阳市-于洪区', '71', '02', '04', '01', '845', '', null, null);
INSERT INTO t_sys_area VALUES ('846', '210122', '210122', '210122', '辽中县', 'Liaozhong Xian', 'Liaozhong Xian', ',7,71,846,', '辽宁省-沈阳市-辽中县', '71', '02', '04', '01', '846', '', null, null);
INSERT INTO t_sys_area VALUES ('847', '210123', '210123', '210123', '康平县', 'Kangping Xian', 'Kangping Xian', ',7,71,847,', '辽宁省-沈阳市-康平县', '71', '02', '04', '01', '847', '', null, null);
INSERT INTO t_sys_area VALUES ('848', '210124', '210124', '210124', '法库县', 'Faku Xian', 'Faku Xian', ',7,71,848,', '辽宁省-沈阳市-法库县', '71', '02', '04', '01', '848', '', null, null);
INSERT INTO t_sys_area VALUES ('849', '210181', '210181', '210181', '新民市', 'Xinmin Shi', 'Xinmin Shi', ',7,71,849,', '辽宁省-沈阳市-新民市', '71', '02', '04', '01', '849', '', null, null);
INSERT INTO t_sys_area VALUES ('850', '210201', '210201', '210201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',7,72,850,', '辽宁省-大连市-市辖区', '72', '02', '04', '01', '850', '', null, null);
INSERT INTO t_sys_area VALUES ('851', '210202', '210202', '210202', '中山区', 'Zhongshan Qu', 'Zhongshan Qu', ',7,72,851,', '辽宁省-大连市-中山区', '72', '02', '04', '01', '851', '', null, null);
INSERT INTO t_sys_area VALUES ('852', '210203', '210203', '210203', '西岗区', 'Xigang Qu', 'Xigang Qu', ',7,72,852,', '辽宁省-大连市-西岗区', '72', '02', '04', '01', '852', '', null, null);
INSERT INTO t_sys_area VALUES ('853', '210204', '210204', '210204', '沙河口区', 'Shahekou Qu', 'Shahekou Qu', ',7,72,853,', '辽宁省-大连市-沙河口区', '72', '02', '04', '01', '853', '', null, null);
INSERT INTO t_sys_area VALUES ('854', '210211', '210211', '210211', '甘井子区', 'Ganjingzi Qu', 'Ganjingzi Qu', ',7,72,854,', '辽宁省-大连市-甘井子区', '72', '02', '04', '01', '854', '', null, null);
INSERT INTO t_sys_area VALUES ('855', '210212', '210212', '210212', '旅顺口区', 'Lvshunkou Qu ', 'Lvshunkou Qu ', ',7,72,855,', '辽宁省-大连市-旅顺口区', '72', '02', '04', '01', '855', '', null, null);
INSERT INTO t_sys_area VALUES ('856', '210213', '210213', '210213', '金州区', 'Jinzhou Qu', 'Jinzhou Qu', ',7,72,856,', '辽宁省-大连市-金州区', '72', '02', '04', '01', '856', '', null, null);
INSERT INTO t_sys_area VALUES ('857', '210224', '210224', '210224', '长海县', 'Changhai Xian', 'Changhai Xian', ',7,72,857,', '辽宁省-大连市-长海县', '72', '02', '04', '01', '857', '', null, null);
INSERT INTO t_sys_area VALUES ('858', '210281', '210281', '210281', '瓦房店市', 'Wafangdian Shi', 'Wafangdian Shi', ',7,72,858,', '辽宁省-大连市-瓦房店市', '72', '02', '04', '01', '858', '', null, null);
INSERT INTO t_sys_area VALUES ('859', '210282', '210282', '210282', '普兰店市', 'Pulandian Shi', 'Pulandian Shi', ',7,72,859,', '辽宁省-大连市-普兰店市', '72', '02', '04', '01', '859', '', null, null);
INSERT INTO t_sys_area VALUES ('860', '210283', '210283', '210283', '庄河市', 'Zhuanghe Shi', 'Zhuanghe Shi', ',7,72,860,', '辽宁省-大连市-庄河市', '72', '02', '04', '01', '860', '', null, null);
INSERT INTO t_sys_area VALUES ('861', '210301', '210301', '210301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',7,73,861,', '辽宁省-鞍山市-市辖区', '73', '02', '04', '01', '861', '', null, null);
INSERT INTO t_sys_area VALUES ('862', '210302', '210302', '210302', '铁东区', 'Tiedong Qu ', 'Tiedong Qu ', ',7,73,862,', '辽宁省-鞍山市-铁东区', '73', '02', '04', '01', '862', '', null, null);
INSERT INTO t_sys_area VALUES ('863', '210303', '210303', '210303', '铁西区', 'Tiexi Qu', 'Tiexi Qu', ',7,73,863,', '辽宁省-鞍山市-铁西区', '73', '02', '04', '01', '863', '', null, null);
INSERT INTO t_sys_area VALUES ('864', '210304', '210304', '210304', '立山区', 'Lishan Qu', 'Lishan Qu', ',7,73,864,', '辽宁省-鞍山市-立山区', '73', '02', '04', '01', '864', '', null, null);
INSERT INTO t_sys_area VALUES ('865', '210311', '210311', '210311', '千山区', 'Qianshan Qu ', 'Qianshan Qu ', ',7,73,865,', '辽宁省-鞍山市-千山区', '73', '02', '04', '01', '865', '', null, null);
INSERT INTO t_sys_area VALUES ('866', '210321', '210321', '210321', '台安县', 'Tai,an Xian', 'Tai,an Xian', ',7,73,866,', '辽宁省-鞍山市-台安县', '73', '02', '04', '01', '866', '', null, null);
INSERT INTO t_sys_area VALUES ('867', '210323', '210323', '210323', '岫岩满族自治县', 'Xiuyan Manzu Zizhixian', 'Xiuyan Manzu Zizhixian', ',7,73,867,', '辽宁省-鞍山市-岫岩满族自治县', '73', '02', '04', '01', '867', '', null, null);
INSERT INTO t_sys_area VALUES ('868', '210381', '210381', '210381', '海城市', 'Haicheng Shi', 'Haicheng Shi', ',7,73,868,', '辽宁省-鞍山市-海城市', '73', '02', '04', '01', '868', '', null, null);
INSERT INTO t_sys_area VALUES ('869', '210401', '210401', '210401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',7,74,869,', '辽宁省-抚顺市-市辖区', '74', '02', '04', '01', '869', '', null, null);
INSERT INTO t_sys_area VALUES ('870', '210402', '210402', '210402', '新抚区', 'Xinfu Qu', 'Xinfu Qu', ',7,74,870,', '辽宁省-抚顺市-新抚区', '74', '02', '04', '01', '870', '', null, null);
INSERT INTO t_sys_area VALUES ('871', '210403', '210403', '210403', '东洲区', 'Dongzhou Qu', 'Dongzhou Qu', ',7,74,871,', '辽宁省-抚顺市-东洲区', '74', '02', '04', '01', '871', '', null, null);
INSERT INTO t_sys_area VALUES ('872', '210404', '210404', '210404', '望花区', 'Wanghua Qu', 'Wanghua Qu', ',7,74,872,', '辽宁省-抚顺市-望花区', '74', '02', '04', '01', '872', '', null, null);
INSERT INTO t_sys_area VALUES ('873', '210411', '210411', '210411', '顺城区', 'Shuncheng Qu', 'Shuncheng Qu', ',7,74,873,', '辽宁省-抚顺市-顺城区', '74', '02', '04', '01', '873', '', null, null);
INSERT INTO t_sys_area VALUES ('874', '210421', '210421', '210421', '抚顺县', 'Fushun Xian', 'Fushun Xian', ',7,74,874,', '辽宁省-抚顺市-抚顺县', '74', '02', '04', '01', '874', '', null, null);
INSERT INTO t_sys_area VALUES ('875', '210422', '210422', '210422', '新宾满族自治县', 'Xinbinmanzuzizhi Xian', 'Xinbinmanzuzizhi Xian', ',7,74,875,', '辽宁省-抚顺市-新宾满族自治县', '74', '02', '04', '01', '875', '', null, null);
INSERT INTO t_sys_area VALUES ('876', '210423', '210423', '210423', '清原满族自治县', 'Qingyuanmanzuzizhi Xian', 'Qingyuanmanzuzizhi Xian', ',7,74,876,', '辽宁省-抚顺市-清原满族自治县', '74', '02', '04', '01', '876', '', null, null);
INSERT INTO t_sys_area VALUES ('877', '210501', '210501', '210501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',7,75,877,', '辽宁省-本溪市-市辖区', '75', '02', '04', '01', '877', '', null, null);
INSERT INTO t_sys_area VALUES ('878', '210502', '210502', '210502', '平山区', 'Pingshan Qu', 'Pingshan Qu', ',7,75,878,', '辽宁省-本溪市-平山区', '75', '02', '04', '01', '878', '', null, null);
INSERT INTO t_sys_area VALUES ('879', '210503', '210503', '210503', '溪湖区', 'Xihu Qu ', 'Xihu Qu ', ',7,75,879,', '辽宁省-本溪市-溪湖区', '75', '02', '04', '01', '879', '', null, null);
INSERT INTO t_sys_area VALUES ('880', '210504', '210504', '210504', '明山区', 'Mingshan Qu', 'Mingshan Qu', ',7,75,880,', '辽宁省-本溪市-明山区', '75', '02', '04', '01', '880', '', null, null);
INSERT INTO t_sys_area VALUES ('881', '210505', '210505', '210505', '南芬区', 'Nanfen Qu', 'Nanfen Qu', ',7,75,881,', '辽宁省-本溪市-南芬区', '75', '02', '04', '01', '881', '', null, null);
INSERT INTO t_sys_area VALUES ('882', '210521', '210521', '210521', '本溪满族自治县', 'Benxi Manzu Zizhixian', 'Benxi Manzu Zizhixian', ',7,75,882,', '辽宁省-本溪市-本溪满族自治县', '75', '02', '04', '01', '882', '', null, null);
INSERT INTO t_sys_area VALUES ('883', '210522', '210522', '210522', '桓仁满族自治县', 'Huanren Manzu Zizhixian', 'Huanren Manzu Zizhixian', ',7,75,883,', '辽宁省-本溪市-桓仁满族自治县', '75', '02', '04', '01', '883', '', null, null);
INSERT INTO t_sys_area VALUES ('884', '210601', '210601', '210601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',7,76,884,', '辽宁省-丹东市-市辖区', '76', '02', '04', '01', '884', '', null, null);
INSERT INTO t_sys_area VALUES ('885', '210602', '210602', '210602', '元宝区', 'Yuanbao Qu', 'Yuanbao Qu', ',7,76,885,', '辽宁省-丹东市-元宝区', '76', '02', '04', '01', '885', '', null, null);
INSERT INTO t_sys_area VALUES ('886', '210603', '210603', '210603', '振兴区', 'Zhenxing Qu ', 'Zhenxing Qu ', ',7,76,886,', '辽宁省-丹东市-振兴区', '76', '02', '04', '01', '886', '', null, null);
INSERT INTO t_sys_area VALUES ('887', '210604', '210604', '210604', '振安区', 'Zhen,an Qu', 'Zhen,an Qu', ',7,76,887,', '辽宁省-丹东市-振安区', '76', '02', '04', '01', '887', '', null, null);
INSERT INTO t_sys_area VALUES ('888', '210624', '210624', '210624', '宽甸满族自治县', 'Kuandian Manzu Zizhixian', 'Kuandian Manzu Zizhixian', ',7,76,888,', '辽宁省-丹东市-宽甸满族自治县', '76', '02', '04', '01', '888', '', null, null);
INSERT INTO t_sys_area VALUES ('889', '210681', '210681', '210681', '东港市', 'Donggang Shi', 'Donggang Shi', ',7,76,889,', '辽宁省-丹东市-东港市', '76', '02', '04', '01', '889', '', null, null);
INSERT INTO t_sys_area VALUES ('890', '210682', '210682', '210682', '凤城市', 'Fengcheng Shi', 'Fengcheng Shi', ',7,76,890,', '辽宁省-丹东市-凤城市', '76', '02', '04', '01', '890', '', null, null);
INSERT INTO t_sys_area VALUES ('891', '210701', '210701', '210701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',7,77,891,', '辽宁省-锦州市-市辖区', '77', '02', '04', '01', '891', '', null, null);
INSERT INTO t_sys_area VALUES ('892', '210702', '210702', '210702', '古塔区', 'Guta Qu', 'Guta Qu', ',7,77,892,', '辽宁省-锦州市-古塔区', '77', '02', '04', '01', '892', '', null, null);
INSERT INTO t_sys_area VALUES ('893', '210703', '210703', '210703', '凌河区', 'Linghe Qu', 'Linghe Qu', ',7,77,893,', '辽宁省-锦州市-凌河区', '77', '02', '04', '01', '893', '', null, null);
INSERT INTO t_sys_area VALUES ('894', '210711', '210711', '210711', '太和区', 'Taihe Qu', 'Taihe Qu', ',7,77,894,', '辽宁省-锦州市-太和区', '77', '02', '04', '01', '894', '', null, null);
INSERT INTO t_sys_area VALUES ('895', '210726', '210726', '210726', '黑山县', 'Heishan Xian', 'Heishan Xian', ',7,77,895,', '辽宁省-锦州市-黑山县', '77', '02', '04', '01', '895', '', null, null);
INSERT INTO t_sys_area VALUES ('896', '210727', '210727', '210727', '义县', 'Yi Xian', 'Yi Xian', ',7,77,896,', '辽宁省-锦州市-义县', '77', '02', '04', '01', '896', '', null, null);
INSERT INTO t_sys_area VALUES ('897', '210781', '210781', '210781', '凌海市', 'Linghai Shi ', 'Linghai Shi ', ',7,77,897,', '辽宁省-锦州市-凌海市', '77', '02', '04', '01', '897', '', null, null);
INSERT INTO t_sys_area VALUES ('898', '210782', '210782', '210782', '北镇市', 'Beining Shi', 'Beining Shi', ',7,77,898,', '辽宁省-锦州市-北镇市', '77', '02', '04', '01', '898', '', null, null);
INSERT INTO t_sys_area VALUES ('899', '210801', '210801', '210801', '市辖区', 'Shixiaqu', 'Shixiaqu', ',7,78,899,', '辽宁省-营口市-市辖区', '78', '02', '04', '01', '899', '', null, null);
INSERT INTO t_sys_area VALUES ('900', '210802', '210802', '210802', '站前区', 'Zhanqian Qu', 'Zhanqian Qu', ',7,78,900,', '辽宁省-营口市-站前区', '78', '02', '04', '01', '900', '', null, null);
INSERT INTO t_sys_area VALUES ('901', '210803', '210803', '210803', '西市区', 'Xishi Qu', 'Xishi Qu', ',7,78,901,', '辽宁省-营口市-西市区', '78', '02', '04', '01', '901', '', null, null);
INSERT INTO t_sys_area VALUES ('902', '210804', '210804', '210804', '鲅鱼圈区', 'Bayuquan Qu', 'Bayuquan Qu', ',7,78,902,', '辽宁省-营口市-鲅鱼圈区', '78', '02', '04', '01', '902', '', null, null);
INSERT INTO t_sys_area VALUES ('903', '210811', '210811', '210811', '老边区', 'Laobian Qu', 'Laobian Qu', ',7,78,903,', '辽宁省-营口市-老边区', '78', '02', '04', '01', '903', '', null, null);
INSERT INTO t_sys_area VALUES ('904', '210881', '210881', '210881', '盖州市', 'Gaizhou Shi', 'Gaizhou Shi', ',7,78,904,', '辽宁省-营口市-盖州市', '78', '02', '04', '01', '904', '', null, null);
INSERT INTO t_sys_area VALUES ('905', '210882', '210882', '210882', '大石桥市', 'Dashiqiao Shi', 'Dashiqiao Shi', ',7,78,905,', '辽宁省-营口市-大石桥市', '78', '02', '04', '01', '905', '', null, null);
INSERT INTO t_sys_area VALUES ('906', '210901', '210901', '210901', '市辖区', 'Shixiaqu', 'Shixiaqu', ',7,79,906,', '辽宁省-阜新市-市辖区', '79', '02', '04', '01', '906', '', null, null);
INSERT INTO t_sys_area VALUES ('907', '210902', '210902', '210902', '海州区', 'Haizhou Qu', 'Haizhou Qu', ',7,79,907,', '辽宁省-阜新市-海州区', '79', '02', '04', '01', '907', '', null, null);
INSERT INTO t_sys_area VALUES ('908', '210903', '210903', '210903', '新邱区', 'Xinqiu Qu', 'Xinqiu Qu', ',7,79,908,', '辽宁省-阜新市-新邱区', '79', '02', '04', '01', '908', '', null, null);
INSERT INTO t_sys_area VALUES ('909', '210904', '210904', '210904', '太平区', 'Taiping Qu', 'Taiping Qu', ',7,79,909,', '辽宁省-阜新市-太平区', '79', '02', '04', '01', '909', '', null, null);
INSERT INTO t_sys_area VALUES ('910', '210905', '210905', '210905', '清河门区', 'Qinghemen Qu', 'Qinghemen Qu', ',7,79,910,', '辽宁省-阜新市-清河门区', '79', '02', '04', '01', '910', '', null, null);
INSERT INTO t_sys_area VALUES ('911', '210911', '210911', '210911', '细河区', 'Xihe Qu', 'Xihe Qu', ',7,79,911,', '辽宁省-阜新市-细河区', '79', '02', '04', '01', '911', '', null, null);
INSERT INTO t_sys_area VALUES ('912', '210921', '210921', '210921', '阜新蒙古族自治县', 'Fuxin Mongolzu Zizhixian', 'Fuxin Mongolzu Zizhixian', ',7,79,912,', '辽宁省-阜新市-阜新蒙古族自治县', '79', '02', '04', '01', '912', '', null, null);
INSERT INTO t_sys_area VALUES ('913', '210922', '210922', '210922', '彰武县', 'Zhangwu Xian', 'Zhangwu Xian', ',7,79,913,', '辽宁省-阜新市-彰武县', '79', '02', '04', '01', '913', '', null, null);
INSERT INTO t_sys_area VALUES ('914', '211001', '211001', '211001', '市辖区', 'Shixiaqu', 'Shixiaqu', ',7,80,914,', '辽宁省-辽阳市-市辖区', '80', '02', '04', '01', '914', '', null, null);
INSERT INTO t_sys_area VALUES ('915', '211002', '211002', '211002', '白塔区', 'Baita Qu', 'Baita Qu', ',7,80,915,', '辽宁省-辽阳市-白塔区', '80', '02', '04', '01', '915', '', null, null);
INSERT INTO t_sys_area VALUES ('916', '211003', '211003', '211003', '文圣区', 'Wensheng Qu', 'Wensheng Qu', ',7,80,916,', '辽宁省-辽阳市-文圣区', '80', '02', '04', '01', '916', '', null, null);
INSERT INTO t_sys_area VALUES ('917', '211004', '211004', '211004', '宏伟区', 'Hongwei Qu', 'Hongwei Qu', ',7,80,917,', '辽宁省-辽阳市-宏伟区', '80', '02', '04', '01', '917', '', null, null);
INSERT INTO t_sys_area VALUES ('918', '211005', '211005', '211005', '弓长岭区', 'Gongchangling Qu', 'Gongchangling Qu', ',7,80,918,', '辽宁省-辽阳市-弓长岭区', '80', '02', '04', '01', '918', '', null, null);
INSERT INTO t_sys_area VALUES ('919', '211011', '211011', '211011', '太子河区', 'Taizihe Qu', 'Taizihe Qu', ',7,80,919,', '辽宁省-辽阳市-太子河区', '80', '02', '04', '01', '919', '', null, null);
INSERT INTO t_sys_area VALUES ('920', '211021', '211021', '211021', '辽阳县', 'Liaoyang Xian', 'Liaoyang Xian', ',7,80,920,', '辽宁省-辽阳市-辽阳县', '80', '02', '04', '01', '920', '', null, null);
INSERT INTO t_sys_area VALUES ('921', '211081', '211081', '211081', '灯塔市', 'Dengta Shi', 'Dengta Shi', ',7,80,921,', '辽宁省-辽阳市-灯塔市', '80', '02', '04', '01', '921', '', null, null);
INSERT INTO t_sys_area VALUES ('922', '211101', '211101', '211101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',7,81,922,', '辽宁省-盘锦市-市辖区', '81', '02', '04', '01', '922', '', null, null);
INSERT INTO t_sys_area VALUES ('923', '211102', '211102', '211102', '双台子区', 'Shuangtaizi Qu', 'Shuangtaizi Qu', ',7,81,923,', '辽宁省-盘锦市-双台子区', '81', '02', '04', '01', '923', '', null, null);
INSERT INTO t_sys_area VALUES ('924', '211103', '211103', '211103', '兴隆台区', 'Xinglongtai Qu', 'Xinglongtai Qu', ',7,81,924,', '辽宁省-盘锦市-兴隆台区', '81', '02', '04', '01', '924', '', null, null);
INSERT INTO t_sys_area VALUES ('925', '211121', '211121', '211121', '大洼县', 'Dawa Xian', 'Dawa Xian', ',7,81,925,', '辽宁省-盘锦市-大洼县', '81', '02', '04', '01', '925', '', null, null);
INSERT INTO t_sys_area VALUES ('926', '211122', '211122', '211122', '盘山县', 'Panshan Xian', 'Panshan Xian', ',7,81,926,', '辽宁省-盘锦市-盘山县', '81', '02', '04', '01', '926', '', null, null);
INSERT INTO t_sys_area VALUES ('927', '211201', '211201', '211201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',7,82,927,', '辽宁省-铁岭市-市辖区', '82', '02', '04', '01', '927', '', null, null);
INSERT INTO t_sys_area VALUES ('928', '211202', '211202', '211202', '银州区', 'Yinzhou Qu', 'Yinzhou Qu', ',7,82,928,', '辽宁省-铁岭市-银州区', '82', '02', '04', '01', '928', '', null, null);
INSERT INTO t_sys_area VALUES ('929', '211204', '211204', '211204', '清河区', 'Qinghe Qu', 'Qinghe Qu', ',7,82,929,', '辽宁省-铁岭市-清河区', '82', '02', '04', '01', '929', '', null, null);
INSERT INTO t_sys_area VALUES ('930', '211221', '211221', '211221', '铁岭县', 'Tieling Xian', 'Tieling Xian', ',7,82,930,', '辽宁省-铁岭市-铁岭县', '82', '02', '04', '01', '930', '', null, null);
INSERT INTO t_sys_area VALUES ('931', '211223', '211223', '211223', '西丰县', 'Xifeng Xian', 'Xifeng Xian', ',7,82,931,', '辽宁省-铁岭市-西丰县', '82', '02', '04', '01', '931', '', null, null);
INSERT INTO t_sys_area VALUES ('932', '211224', '211224', '211224', '昌图县', 'Changtu Xian', 'Changtu Xian', ',7,82,932,', '辽宁省-铁岭市-昌图县', '82', '02', '04', '01', '932', '', null, null);
INSERT INTO t_sys_area VALUES ('933', '211281', '211281', '211281', '调兵山市', 'Diaobingshan Shi', 'Diaobingshan Shi', ',7,82,933,', '辽宁省-铁岭市-调兵山市', '82', '02', '04', '01', '933', '', null, null);
INSERT INTO t_sys_area VALUES ('934', '211282', '211282', '211282', '开原市', 'Kaiyuan Shi', 'Kaiyuan Shi', ',7,82,934,', '辽宁省-铁岭市-开原市', '82', '02', '04', '01', '934', '', null, null);
INSERT INTO t_sys_area VALUES ('935', '211301', '211301', '211301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',7,83,935,', '辽宁省-朝阳市-市辖区', '83', '02', '04', '01', '935', '', null, null);
INSERT INTO t_sys_area VALUES ('936', '211302', '211302', '211302', '双塔区', 'Shuangta Qu', 'Shuangta Qu', ',7,83,936,', '辽宁省-朝阳市-双塔区', '83', '02', '04', '01', '936', '', null, null);
INSERT INTO t_sys_area VALUES ('937', '211303', '211303', '211303', '龙城区', 'Longcheng Qu', 'Longcheng Qu', ',7,83,937,', '辽宁省-朝阳市-龙城区', '83', '02', '04', '01', '937', '', null, null);
INSERT INTO t_sys_area VALUES ('938', '211321', '211321', '211321', '朝阳县', 'Chaoyang Xian', 'Chaoyang Xian', ',7,83,938,', '辽宁省-朝阳市-朝阳县', '83', '02', '04', '01', '938', '', null, null);
INSERT INTO t_sys_area VALUES ('939', '211322', '211322', '211322', '建平县', 'Jianping Xian', 'Jianping Xian', ',7,83,939,', '辽宁省-朝阳市-建平县', '83', '02', '04', '01', '939', '', null, null);
INSERT INTO t_sys_area VALUES ('940', '211324', '211324', '211324', '喀喇沁左翼蒙古族自治县', 'Harqin Zuoyi Mongolzu Zizhixian', 'Harqin Zuoyi Mongolzu Zizhixian', ',7,83,940,', '辽宁省-朝阳市-喀喇沁左翼蒙古族自治县', '83', '02', '04', '01', '940', '', null, null);
INSERT INTO t_sys_area VALUES ('941', '211381', '211381', '211381', '北票市', 'Beipiao Shi', 'Beipiao Shi', ',7,83,941,', '辽宁省-朝阳市-北票市', '83', '02', '04', '01', '941', '', null, null);
INSERT INTO t_sys_area VALUES ('942', '211382', '211382', '211382', '凌源市', 'Lingyuan Shi', 'Lingyuan Shi', ',7,83,942,', '辽宁省-朝阳市-凌源市', '83', '02', '04', '01', '942', '', null, null);
INSERT INTO t_sys_area VALUES ('943', '211401', '211401', '211401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',7,84,943,', '辽宁省-葫芦岛市-市辖区', '84', '02', '04', '01', '943', '', null, null);
INSERT INTO t_sys_area VALUES ('944', '211402', '211402', '211402', '连山区', 'Lianshan Qu', 'Lianshan Qu', ',7,84,944,', '辽宁省-葫芦岛市-连山区', '84', '02', '04', '01', '944', '', null, null);
INSERT INTO t_sys_area VALUES ('945', '211403', '211403', '211403', '龙港区', 'Longgang Qu', 'Longgang Qu', ',7,84,945,', '辽宁省-葫芦岛市-龙港区', '84', '02', '04', '01', '945', '', null, null);
INSERT INTO t_sys_area VALUES ('946', '211404', '211404', '211404', '南票区', 'Nanpiao Qu', 'Nanpiao Qu', ',7,84,946,', '辽宁省-葫芦岛市-南票区', '84', '02', '04', '01', '946', '', null, null);
INSERT INTO t_sys_area VALUES ('947', '211421', '211421', '211421', '绥中县', 'Suizhong Xian', 'Suizhong Xian', ',7,84,947,', '辽宁省-葫芦岛市-绥中县', '84', '02', '04', '01', '947', '', null, null);
INSERT INTO t_sys_area VALUES ('948', '211422', '211422', '211422', '建昌县', 'Jianchang Xian', 'Jianchang Xian', ',7,84,948,', '辽宁省-葫芦岛市-建昌县', '84', '02', '04', '01', '948', '', null, null);
INSERT INTO t_sys_area VALUES ('949', '211481', '211481', '211481', '兴城市', 'Xingcheng Shi', 'Xingcheng Shi', ',7,84,949,', '辽宁省-葫芦岛市-兴城市', '84', '02', '04', '01', '949', '', null, null);
INSERT INTO t_sys_area VALUES ('950', '220101', '220101', '220101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',8,85,950,', '吉林省-长春市-市辖区', '85', '02', '04', '01', '950', '', null, null);
INSERT INTO t_sys_area VALUES ('951', '220102', '220102', '220102', '南关区', 'Nanguan Qu', 'Nanguan Qu', ',8,85,951,', '吉林省-长春市-南关区', '85', '02', '04', '01', '951', '', null, null);
INSERT INTO t_sys_area VALUES ('952', '220103', '220103', '220103', '宽城区', 'Kuancheng Qu', 'Kuancheng Qu', ',8,85,952,', '吉林省-长春市-宽城区', '85', '02', '04', '01', '952', '', null, null);
INSERT INTO t_sys_area VALUES ('953', '220104', '220104', '220104', '朝阳区', 'Chaoyang Qu ', 'Chaoyang Qu ', ',8,85,953,', '吉林省-长春市-朝阳区', '85', '02', '04', '01', '953', '', null, null);
INSERT INTO t_sys_area VALUES ('954', '220105', '220105', '220105', '二道区', 'Erdao Qu', 'Erdao Qu', ',8,85,954,', '吉林省-长春市-二道区', '85', '02', '04', '01', '954', '', null, null);
INSERT INTO t_sys_area VALUES ('955', '220106', '220106', '220106', '绿园区', 'Lvyuan Qu', 'Lvyuan Qu', ',8,85,955,', '吉林省-长春市-绿园区', '85', '02', '04', '01', '955', '', null, null);
INSERT INTO t_sys_area VALUES ('956', '220112', '220112', '220112', '双阳区', 'Shuangyang Qu', 'Shuangyang Qu', ',8,85,956,', '吉林省-长春市-双阳区', '85', '02', '04', '01', '956', '', null, null);
INSERT INTO t_sys_area VALUES ('957', '220122', '220122', '220122', '农安县', 'Nong,an Xian ', 'Nong,an Xian ', ',8,85,957,', '吉林省-长春市-农安县', '85', '02', '04', '01', '957', '', null, null);
INSERT INTO t_sys_area VALUES ('958', '220181', '220181', '220181', '九台市', 'Jiutai Shi', 'Jiutai Shi', ',8,85,958,', '吉林省-长春市-九台市', '85', '02', '04', '01', '958', '', null, null);
INSERT INTO t_sys_area VALUES ('959', '220182', '220182', '220182', '榆树市', 'Yushu Shi', 'Yushu Shi', ',8,85,959,', '吉林省-长春市-榆树市', '85', '02', '04', '01', '959', '', null, null);
INSERT INTO t_sys_area VALUES ('960', '220183', '220183', '220183', '德惠市', 'Dehui Shi', 'Dehui Shi', ',8,85,960,', '吉林省-长春市-德惠市', '85', '02', '04', '01', '960', '', null, null);
INSERT INTO t_sys_area VALUES ('961', '220201', '220201', '220201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',8,86,961,', '吉林省-吉林市-市辖区', '86', '02', '04', '01', '961', '', null, null);
INSERT INTO t_sys_area VALUES ('962', '220202', '220202', '220202', '昌邑区', 'Changyi Qu', 'Changyi Qu', ',8,86,962,', '吉林省-吉林市-昌邑区', '86', '02', '04', '01', '962', '', null, null);
INSERT INTO t_sys_area VALUES ('963', '220203', '220203', '220203', '龙潭区', 'Longtan Qu', 'Longtan Qu', ',8,86,963,', '吉林省-吉林市-龙潭区', '86', '02', '04', '01', '963', '', null, null);
INSERT INTO t_sys_area VALUES ('964', '220204', '220204', '220204', '船营区', 'Chuanying Qu', 'Chuanying Qu', ',8,86,964,', '吉林省-吉林市-船营区', '86', '02', '04', '01', '964', '', null, null);
INSERT INTO t_sys_area VALUES ('965', '220211', '220211', '220211', '丰满区', 'Fengman Qu', 'Fengman Qu', ',8,86,965,', '吉林省-吉林市-丰满区', '86', '02', '04', '01', '965', '', null, null);
INSERT INTO t_sys_area VALUES ('966', '220221', '220221', '220221', '永吉县', 'Yongji Xian', 'Yongji Xian', ',8,86,966,', '吉林省-吉林市-永吉县', '86', '02', '04', '01', '966', '', null, null);
INSERT INTO t_sys_area VALUES ('967', '220281', '220281', '220281', '蛟河市', 'Jiaohe Shi', 'Jiaohe Shi', ',8,86,967,', '吉林省-吉林市-蛟河市', '86', '02', '04', '01', '967', '', null, null);
INSERT INTO t_sys_area VALUES ('968', '220282', '220282', '220282', '桦甸市', 'Huadian Shi', 'Huadian Shi', ',8,86,968,', '吉林省-吉林市-桦甸市', '86', '02', '04', '01', '968', '', null, null);
INSERT INTO t_sys_area VALUES ('969', '220283', '220283', '220283', '舒兰市', 'Shulan Shi', 'Shulan Shi', ',8,86,969,', '吉林省-吉林市-舒兰市', '86', '02', '04', '01', '969', '', null, null);
INSERT INTO t_sys_area VALUES ('970', '220284', '220284', '220284', '磐石市', 'Panshi Shi', 'Panshi Shi', ',8,86,970,', '吉林省-吉林市-磐石市', '86', '02', '04', '01', '970', '', null, null);
INSERT INTO t_sys_area VALUES ('971', '220301', '220301', '220301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',8,87,971,', '吉林省-四平市-市辖区', '87', '02', '04', '01', '971', '', null, null);
INSERT INTO t_sys_area VALUES ('972', '220302', '220302', '220302', '铁西区', 'Tiexi Qu', 'Tiexi Qu', ',8,87,972,', '吉林省-四平市-铁西区', '87', '02', '04', '01', '972', '', null, null);
INSERT INTO t_sys_area VALUES ('973', '220303', '220303', '220303', '铁东区', 'Tiedong Qu ', 'Tiedong Qu ', ',8,87,973,', '吉林省-四平市-铁东区', '87', '02', '04', '01', '973', '', null, null);
INSERT INTO t_sys_area VALUES ('974', '220322', '220322', '220322', '梨树县', 'Lishu Xian', 'Lishu Xian', ',8,87,974,', '吉林省-四平市-梨树县', '87', '02', '04', '01', '974', '', null, null);
INSERT INTO t_sys_area VALUES ('975', '220323', '220323', '220323', '伊通满族自治县', 'Yitong Manzu Zizhixian', 'Yitong Manzu Zizhixian', ',8,87,975,', '吉林省-四平市-伊通满族自治县', '87', '02', '04', '01', '975', '', null, null);
INSERT INTO t_sys_area VALUES ('976', '220381', '220381', '220381', '公主岭市', 'Gongzhuling Shi', 'Gongzhuling Shi', ',8,87,976,', '吉林省-四平市-公主岭市', '87', '02', '04', '01', '976', '', null, null);
INSERT INTO t_sys_area VALUES ('977', '220382', '220382', '220382', '双辽市', 'Shuangliao Shi', 'Shuangliao Shi', ',8,87,977,', '吉林省-四平市-双辽市', '87', '02', '04', '01', '977', '', null, null);
INSERT INTO t_sys_area VALUES ('978', '220401', '220401', '220401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',8,88,978,', '吉林省-辽源市-市辖区', '88', '02', '04', '01', '978', '', null, null);
INSERT INTO t_sys_area VALUES ('979', '220402', '220402', '220402', '龙山区', 'Longshan Qu', 'Longshan Qu', ',8,88,979,', '吉林省-辽源市-龙山区', '88', '02', '04', '01', '979', '', null, null);
INSERT INTO t_sys_area VALUES ('980', '220403', '220403', '220403', '西安区', 'Xi,an Qu', 'Xi,an Qu', ',8,88,980,', '吉林省-辽源市-西安区', '88', '02', '04', '01', '980', '', null, null);
INSERT INTO t_sys_area VALUES ('981', '220421', '220421', '220421', '东丰县', 'Dongfeng Xian', 'Dongfeng Xian', ',8,88,981,', '吉林省-辽源市-东丰县', '88', '02', '04', '01', '981', '', null, null);
INSERT INTO t_sys_area VALUES ('982', '220422', '220422', '220422', '东辽县', 'Dongliao Xian ', 'Dongliao Xian ', ',8,88,982,', '吉林省-辽源市-东辽县', '88', '02', '04', '01', '982', '', null, null);
INSERT INTO t_sys_area VALUES ('983', '220501', '220501', '220501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',8,89,983,', '吉林省-通化市-市辖区', '89', '02', '04', '01', '983', '', null, null);
INSERT INTO t_sys_area VALUES ('984', '220502', '220502', '220502', '东昌区', 'Dongchang Qu', 'Dongchang Qu', ',8,89,984,', '吉林省-通化市-东昌区', '89', '02', '04', '01', '984', '', null, null);
INSERT INTO t_sys_area VALUES ('985', '220503', '220503', '220503', '二道江区', 'Erdaojiang Qu', 'Erdaojiang Qu', ',8,89,985,', '吉林省-通化市-二道江区', '89', '02', '04', '01', '985', '', null, null);
INSERT INTO t_sys_area VALUES ('986', '220521', '220521', '220521', '通化县', 'Tonghua Xian ', 'Tonghua Xian ', ',8,89,986,', '吉林省-通化市-通化县', '89', '02', '04', '01', '986', '', null, null);
INSERT INTO t_sys_area VALUES ('987', '220523', '220523', '220523', '辉南县', 'Huinan Xian ', 'Huinan Xian ', ',8,89,987,', '吉林省-通化市-辉南县', '89', '02', '04', '01', '987', '', null, null);
INSERT INTO t_sys_area VALUES ('988', '220524', '220524', '220524', '柳河县', 'Liuhe Xian ', 'Liuhe Xian ', ',8,89,988,', '吉林省-通化市-柳河县', '89', '02', '04', '01', '988', '', null, null);
INSERT INTO t_sys_area VALUES ('989', '220581', '220581', '220581', '梅河口市', 'Meihekou Shi', 'Meihekou Shi', ',8,89,989,', '吉林省-通化市-梅河口市', '89', '02', '04', '01', '989', '', null, null);
INSERT INTO t_sys_area VALUES ('990', '220582', '220582', '220582', '集安市', 'Ji,an Shi', 'Ji,an Shi', ',8,89,990,', '吉林省-通化市-集安市', '89', '02', '04', '01', '990', '', null, null);
INSERT INTO t_sys_area VALUES ('991', '220601', '220601', '220601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',8,90,991,', '吉林省-白山市-市辖区', '90', '02', '04', '01', '991', '', null, null);
INSERT INTO t_sys_area VALUES ('992', '220602', '220602', '220602', '八道江区', 'Badaojiang Qu', 'Badaojiang Qu', ',8,90,992,', '吉林省-白山市-八道江区', '90', '02', '04', '01', '992', '', null, null);
INSERT INTO t_sys_area VALUES ('993', '220621', '220621', '220621', '抚松县', 'Fusong Xian', 'Fusong Xian', ',8,90,993,', '吉林省-白山市-抚松县', '90', '02', '04', '01', '993', '', null, null);
INSERT INTO t_sys_area VALUES ('994', '220622', '220622', '220622', '靖宇县', 'Jingyu Xian', 'Jingyu Xian', ',8,90,994,', '吉林省-白山市-靖宇县', '90', '02', '04', '01', '994', '', null, null);
INSERT INTO t_sys_area VALUES ('995', '220623', '220623', '220623', '长白朝鲜族自治县', 'Changbaichaoxianzuzizhi Xian', 'Changbaichaoxianzuzizhi Xian', ',8,90,995,', '吉林省-白山市-长白朝鲜族自治县', '90', '02', '04', '01', '995', '', null, null);
INSERT INTO t_sys_area VALUES ('996', '220605', '220605', '220605', '江源区', 'Jiangyuan Xian', 'Jiangyuan Xian', ',8,90,996,', '吉林省-白山市-江源区', '90', '02', '04', '01', '996', '', null, null);
INSERT INTO t_sys_area VALUES ('997', '220681', '220681', '220681', '临江市', 'Linjiang Shi', 'Linjiang Shi', ',8,90,997,', '吉林省-白山市-临江市', '90', '02', '04', '01', '997', '', null, null);
INSERT INTO t_sys_area VALUES ('998', '220701', '220701', '220701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',8,91,998,', '吉林省-松原市-市辖区', '91', '02', '04', '01', '998', '', null, null);
INSERT INTO t_sys_area VALUES ('999', '220702', '220702', '220702', '宁江区', 'Ningjiang Qu', 'Ningjiang Qu', ',8,91,999,', '吉林省-松原市-宁江区', '91', '02', '04', '01', '999', '', null, null);
INSERT INTO t_sys_area VALUES ('1000', '220721', '220721', '220721', '前郭尔罗斯蒙古族自治县', 'Qian Gorlos Mongolzu Zizhixian', 'Qian Gorlos Mongolzu Zizhixian', ',8,91,1000,', '吉林省-松原市-前郭尔罗斯蒙古族自治县', '91', '02', '04', '01', '1000', '', null, null);
INSERT INTO t_sys_area VALUES ('1001', '220722', '220722', '220722', '长岭县', 'Changling Xian', 'Changling Xian', ',8,91,1001,', '吉林省-松原市-长岭县', '91', '02', '04', '01', '1001', '', null, null);
INSERT INTO t_sys_area VALUES ('1002', '220723', '220723', '220723', '乾安县', 'Qian,an Xian', 'Qian,an Xian', ',8,91,1002,', '吉林省-松原市-乾安县', '91', '02', '04', '01', '1002', '', null, null);
INSERT INTO t_sys_area VALUES ('1003', '220724', '220724', '220724', '扶余县', 'Fuyu Xian', 'Fuyu Xian', ',8,91,1003,', '吉林省-松原市-扶余县', '91', '02', '04', '01', '1003', '', null, null);
INSERT INTO t_sys_area VALUES ('1004', '220801', '220801', '220801', '市辖区', 'Shixiaqu', 'Shixiaqu', ',8,92,1004,', '吉林省-白城市-市辖区', '92', '02', '04', '01', '1004', '', null, null);
INSERT INTO t_sys_area VALUES ('1005', '220802', '220802', '220802', '洮北区', 'Taobei Qu', 'Taobei Qu', ',8,92,1005,', '吉林省-白城市-洮北区', '92', '02', '04', '01', '1005', '', null, null);
INSERT INTO t_sys_area VALUES ('1006', '220821', '220821', '220821', '镇赉县', 'Zhenlai Xian', 'Zhenlai Xian', ',8,92,1006,', '吉林省-白城市-镇赉县', '92', '02', '04', '01', '1006', '', null, null);
INSERT INTO t_sys_area VALUES ('1007', '220822', '220822', '220822', '通榆县', 'Tongyu Xian', 'Tongyu Xian', ',8,92,1007,', '吉林省-白城市-通榆县', '92', '02', '04', '01', '1007', '', null, null);
INSERT INTO t_sys_area VALUES ('1008', '220881', '220881', '220881', '洮南市', 'Taonan Shi', 'Taonan Shi', ',8,92,1008,', '吉林省-白城市-洮南市', '92', '02', '04', '01', '1008', '', null, null);
INSERT INTO t_sys_area VALUES ('1009', '220882', '220882', '220882', '大安市', 'Da,an Shi', 'Da,an Shi', ',8,92,1009,', '吉林省-白城市-大安市', '92', '02', '04', '01', '1009', '', null, null);
INSERT INTO t_sys_area VALUES ('1010', '222401', '222401', '222401', '延吉市', 'Yanji Shi', 'Yanji Shi', ',8,93,1010,', '吉林省-延边朝鲜族自治州-延吉市', '93', '02', '04', '01', '1010', '', null, null);
INSERT INTO t_sys_area VALUES ('1011', '222402', '222402', '222402', '图们市', 'Tumen Shi', 'Tumen Shi', ',8,93,1011,', '吉林省-延边朝鲜族自治州-图们市', '93', '02', '04', '01', '1011', '', null, null);
INSERT INTO t_sys_area VALUES ('1012', '222403', '222403', '222403', '敦化市', 'Dunhua Shi', 'Dunhua Shi', ',8,93,1012,', '吉林省-延边朝鲜族自治州-敦化市', '93', '02', '04', '01', '1012', '', null, null);
INSERT INTO t_sys_area VALUES ('1013', '222404', '222404', '222404', '珲春市', 'Hunchun Shi', 'Hunchun Shi', ',8,93,1013,', '吉林省-延边朝鲜族自治州-珲春市', '93', '02', '04', '01', '1013', '', null, null);
INSERT INTO t_sys_area VALUES ('1014', '222405', '222405', '222405', '龙井市', 'Longjing Shi', 'Longjing Shi', ',8,93,1014,', '吉林省-延边朝鲜族自治州-龙井市', '93', '02', '04', '01', '1014', '', null, null);
INSERT INTO t_sys_area VALUES ('1015', '222406', '222406', '222406', '和龙市', 'Helong Shi', 'Helong Shi', ',8,93,1015,', '吉林省-延边朝鲜族自治州-和龙市', '93', '02', '04', '01', '1015', '', null, null);
INSERT INTO t_sys_area VALUES ('1016', '222424', '222424', '222424', '汪清县', 'Wangqing Xian', 'Wangqing Xian', ',8,93,1016,', '吉林省-延边朝鲜族自治州-汪清县', '93', '02', '04', '01', '1016', '', null, null);
INSERT INTO t_sys_area VALUES ('1017', '222426', '222426', '222426', '安图县', 'Antu Xian', 'Antu Xian', ',8,93,1017,', '吉林省-延边朝鲜族自治州-安图县', '93', '02', '04', '01', '1017', '', null, null);
INSERT INTO t_sys_area VALUES ('1018', '230101', '230101', '230101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',9,94,1018,', '黑龙江省-哈尔滨市-市辖区', '94', '02', '04', '01', '1018', '', null, null);
INSERT INTO t_sys_area VALUES ('1019', '230102', '230102', '230102', '道里区', 'Daoli Qu', 'Daoli Qu', ',9,94,1019,', '黑龙江省-哈尔滨市-道里区', '94', '02', '04', '01', '1019', '', null, null);
INSERT INTO t_sys_area VALUES ('1020', '230103', '230103', '230103', '南岗区', 'Nangang Qu', 'Nangang Qu', ',9,94,1020,', '黑龙江省-哈尔滨市-南岗区', '94', '02', '04', '01', '1020', '', null, null);
INSERT INTO t_sys_area VALUES ('1021', '230104', '230104', '230104', '道外区', 'Daowai Qu', 'Daowai Qu', ',9,94,1021,', '黑龙江省-哈尔滨市-道外区', '94', '02', '04', '01', '1021', '', null, null);
INSERT INTO t_sys_area VALUES ('1022', '230110', '230110', '230110', '香坊区', 'Xiangfang Qu', 'Xiangfang Qu', ',9,94,1022,', '黑龙江省-哈尔滨市-香坊区', '94', '02', '04', '01', '1022', '', null, null);
INSERT INTO t_sys_area VALUES ('1024', '230108', '230108', '230108', '平房区', 'Pingfang Qu', 'Pingfang Qu', ',9,94,1024,', '黑龙江省-哈尔滨市-平房区', '94', '02', '04', '01', '1024', '', null, null);
INSERT INTO t_sys_area VALUES ('1025', '230109', '230109', '230109', '松北区', 'Songbei Qu', 'Songbei Qu', ',9,94,1025,', '黑龙江省-哈尔滨市-松北区', '94', '02', '04', '01', '1025', '', null, null);
INSERT INTO t_sys_area VALUES ('1026', '230111', '230111', '230111', '呼兰区', 'Hulan Qu', 'Hulan Qu', ',9,94,1026,', '黑龙江省-哈尔滨市-呼兰区', '94', '02', '04', '01', '1026', '', null, null);
INSERT INTO t_sys_area VALUES ('1027', '230123', '230123', '230123', '依兰县', 'Yilan Xian', 'Yilan Xian', ',9,94,1027,', '黑龙江省-哈尔滨市-依兰县', '94', '02', '04', '01', '1027', '', null, null);
INSERT INTO t_sys_area VALUES ('1028', '230124', '230124', '230124', '方正县', 'Fangzheng Xian', 'Fangzheng Xian', ',9,94,1028,', '黑龙江省-哈尔滨市-方正县', '94', '02', '04', '01', '1028', '', null, null);
INSERT INTO t_sys_area VALUES ('1029', '230125', '230125', '230125', '宾县', 'Bin Xian', 'Bin Xian', ',9,94,1029,', '黑龙江省-哈尔滨市-宾县', '94', '02', '04', '01', '1029', '', null, null);
INSERT INTO t_sys_area VALUES ('1030', '230126', '230126', '230126', '巴彦县', 'Bayan Xian', 'Bayan Xian', ',9,94,1030,', '黑龙江省-哈尔滨市-巴彦县', '94', '02', '04', '01', '1030', '', null, null);
INSERT INTO t_sys_area VALUES ('1031', '230127', '230127', '230127', '木兰县', 'Mulan Xian ', 'Mulan Xian ', ',9,94,1031,', '黑龙江省-哈尔滨市-木兰县', '94', '02', '04', '01', '1031', '', null, null);
INSERT INTO t_sys_area VALUES ('1032', '230128', '230128', '230128', '通河县', 'Tonghe Xian', 'Tonghe Xian', ',9,94,1032,', '黑龙江省-哈尔滨市-通河县', '94', '02', '04', '01', '1032', '', null, null);
INSERT INTO t_sys_area VALUES ('1033', '230129', '230129', '230129', '延寿县', 'Yanshou Xian', 'Yanshou Xian', ',9,94,1033,', '黑龙江省-哈尔滨市-延寿县', '94', '02', '04', '01', '1033', '', null, null);
INSERT INTO t_sys_area VALUES ('1034', '230112', '230112', '230112', '阿城区', 'Acheng Shi', 'Acheng Shi', ',9,94,1034,', '黑龙江省-哈尔滨市-阿城区', '94', '02', '04', '01', '1034', '', null, null);
INSERT INTO t_sys_area VALUES ('1035', '230182', '230182', '230182', '双城市', 'Shuangcheng Shi', 'Shuangcheng Shi', ',9,94,1035,', '黑龙江省-哈尔滨市-双城市', '94', '02', '04', '01', '1035', '', null, null);
INSERT INTO t_sys_area VALUES ('1036', '230183', '230183', '230183', '尚志市', 'Shangzhi Shi', 'Shangzhi Shi', ',9,94,1036,', '黑龙江省-哈尔滨市-尚志市', '94', '02', '04', '01', '1036', '', null, null);
INSERT INTO t_sys_area VALUES ('1037', '230184', '230184', '230184', '五常市', 'Wuchang Shi', 'Wuchang Shi', ',9,94,1037,', '黑龙江省-哈尔滨市-五常市', '94', '02', '04', '01', '1037', '', null, null);
INSERT INTO t_sys_area VALUES ('1038', '230201', '230201', '230201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',9,95,1038,', '黑龙江省-齐齐哈尔市-市辖区', '95', '02', '04', '01', '1038', '', null, null);
INSERT INTO t_sys_area VALUES ('1039', '230202', '230202', '230202', '龙沙区', 'Longsha Qu', 'Longsha Qu', ',9,95,1039,', '黑龙江省-齐齐哈尔市-龙沙区', '95', '02', '04', '01', '1039', '', null, null);
INSERT INTO t_sys_area VALUES ('1040', '230203', '230203', '230203', '建华区', 'Jianhua Qu', 'Jianhua Qu', ',9,95,1040,', '黑龙江省-齐齐哈尔市-建华区', '95', '02', '04', '01', '1040', '', null, null);
INSERT INTO t_sys_area VALUES ('1041', '230204', '230204', '230204', '铁锋区', 'Tiefeng Qu', 'Tiefeng Qu', ',9,95,1041,', '黑龙江省-齐齐哈尔市-铁锋区', '95', '02', '04', '01', '1041', '', null, null);
INSERT INTO t_sys_area VALUES ('1042', '230205', '230205', '230205', '昂昂溪区', 'Ang,angxi Qu', 'Ang,angxi Qu', ',9,95,1042,', '黑龙江省-齐齐哈尔市-昂昂溪区', '95', '02', '04', '01', '1042', '', null, null);
INSERT INTO t_sys_area VALUES ('1043', '230206', '230206', '230206', '富拉尔基区', 'Hulan Ergi Qu', 'Hulan Ergi Qu', ',9,95,1043,', '黑龙江省-齐齐哈尔市-富拉尔基区', '95', '02', '04', '01', '1043', '', null, null);
INSERT INTO t_sys_area VALUES ('1044', '230207', '230207', '230207', '碾子山区', 'Nianzishan Qu', 'Nianzishan Qu', ',9,95,1044,', '黑龙江省-齐齐哈尔市-碾子山区', '95', '02', '04', '01', '1044', '', null, null);
INSERT INTO t_sys_area VALUES ('1045', '230208', '230208', '230208', '梅里斯达斡尔族区', 'Meilisidawoerzu Qu', 'Meilisidawoerzu Qu', ',9,95,1045,', '黑龙江省-齐齐哈尔市-梅里斯达斡尔族区', '95', '02', '04', '01', '1045', '', null, null);
INSERT INTO t_sys_area VALUES ('1046', '230221', '230221', '230221', '龙江县', 'Longjiang Xian', 'Longjiang Xian', ',9,95,1046,', '黑龙江省-齐齐哈尔市-龙江县', '95', '02', '04', '01', '1046', '', null, null);
INSERT INTO t_sys_area VALUES ('1047', '230223', '230223', '230223', '依安县', 'Yi,an Xian', 'Yi,an Xian', ',9,95,1047,', '黑龙江省-齐齐哈尔市-依安县', '95', '02', '04', '01', '1047', '', null, null);
INSERT INTO t_sys_area VALUES ('1048', '230224', '230224', '230224', '泰来县', 'Tailai Xian', 'Tailai Xian', ',9,95,1048,', '黑龙江省-齐齐哈尔市-泰来县', '95', '02', '04', '01', '1048', '', null, null);
INSERT INTO t_sys_area VALUES ('1049', '230225', '230225', '230225', '甘南县', 'Gannan Xian', 'Gannan Xian', ',9,95,1049,', '黑龙江省-齐齐哈尔市-甘南县', '95', '02', '04', '01', '1049', '', null, null);
INSERT INTO t_sys_area VALUES ('1050', '230227', '230227', '230227', '富裕县', 'Fuyu Xian', 'Fuyu Xian', ',9,95,1050,', '黑龙江省-齐齐哈尔市-富裕县', '95', '02', '04', '01', '1050', '', null, null);
INSERT INTO t_sys_area VALUES ('1051', '230229', '230229', '230229', '克山县', 'Keshan Xian', 'Keshan Xian', ',9,95,1051,', '黑龙江省-齐齐哈尔市-克山县', '95', '02', '04', '01', '1051', '', null, null);
INSERT INTO t_sys_area VALUES ('1052', '230230', '230230', '230230', '克东县', 'Kedong Xian', 'Kedong Xian', ',9,95,1052,', '黑龙江省-齐齐哈尔市-克东县', '95', '02', '04', '01', '1052', '', null, null);
INSERT INTO t_sys_area VALUES ('1053', '230231', '230231', '230231', '拜泉县', 'Baiquan Xian', 'Baiquan Xian', ',9,95,1053,', '黑龙江省-齐齐哈尔市-拜泉县', '95', '02', '04', '01', '1053', '', null, null);
INSERT INTO t_sys_area VALUES ('1054', '230281', '230281', '230281', '讷河市', 'Nehe Shi', 'Nehe Shi', ',9,95,1054,', '黑龙江省-齐齐哈尔市-讷河市', '95', '02', '04', '01', '1054', '', null, null);
INSERT INTO t_sys_area VALUES ('1055', '230301', '230301', '230301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',9,96,1055,', '黑龙江省-鸡西市-市辖区', '96', '02', '04', '01', '1055', '', null, null);
INSERT INTO t_sys_area VALUES ('1056', '230302', '230302', '230302', '鸡冠区', 'Jiguan Qu', 'Jiguan Qu', ',9,96,1056,', '黑龙江省-鸡西市-鸡冠区', '96', '02', '04', '01', '1056', '', null, null);
INSERT INTO t_sys_area VALUES ('1057', '230303', '230303', '230303', '恒山区', 'Hengshan Qu', 'Hengshan Qu', ',9,96,1057,', '黑龙江省-鸡西市-恒山区', '96', '02', '04', '01', '1057', '', null, null);
INSERT INTO t_sys_area VALUES ('1058', '230304', '230304', '230304', '滴道区', 'Didao Qu', 'Didao Qu', ',9,96,1058,', '黑龙江省-鸡西市-滴道区', '96', '02', '04', '01', '1058', '', null, null);
INSERT INTO t_sys_area VALUES ('1059', '230305', '230305', '230305', '梨树区', 'Lishu Qu', 'Lishu Qu', ',9,96,1059,', '黑龙江省-鸡西市-梨树区', '96', '02', '04', '01', '1059', '', null, null);
INSERT INTO t_sys_area VALUES ('1060', '230306', '230306', '230306', '城子河区', 'Chengzihe Qu', 'Chengzihe Qu', ',9,96,1060,', '黑龙江省-鸡西市-城子河区', '96', '02', '04', '01', '1060', '', null, null);
INSERT INTO t_sys_area VALUES ('1061', '230307', '230307', '230307', '麻山区', 'Mashan Qu', 'Mashan Qu', ',9,96,1061,', '黑龙江省-鸡西市-麻山区', '96', '02', '04', '01', '1061', '', null, null);
INSERT INTO t_sys_area VALUES ('1062', '230321', '230321', '230321', '鸡东县', 'Jidong Xian', 'Jidong Xian', ',9,96,1062,', '黑龙江省-鸡西市-鸡东县', '96', '02', '04', '01', '1062', '', null, null);
INSERT INTO t_sys_area VALUES ('1063', '230381', '230381', '230381', '虎林市', 'Hulin Shi', 'Hulin Shi', ',9,96,1063,', '黑龙江省-鸡西市-虎林市', '96', '02', '04', '01', '1063', '', null, null);
INSERT INTO t_sys_area VALUES ('1064', '230382', '230382', '230382', '密山市', 'Mishan Shi', 'Mishan Shi', ',9,96,1064,', '黑龙江省-鸡西市-密山市', '96', '02', '04', '01', '1064', '', null, null);
INSERT INTO t_sys_area VALUES ('1065', '230401', '230401', '230401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',9,97,1065,', '黑龙江省-鹤岗市-市辖区', '97', '02', '04', '01', '1065', '', null, null);
INSERT INTO t_sys_area VALUES ('1066', '230402', '230402', '230402', '向阳区', 'Xiangyang  Qu ', 'Xiangyang  Qu ', ',9,97,1066,', '黑龙江省-鹤岗市-向阳区', '97', '02', '04', '01', '1066', '', null, null);
INSERT INTO t_sys_area VALUES ('1067', '230403', '230403', '230403', '工农区', 'Gongnong Qu', 'Gongnong Qu', ',9,97,1067,', '黑龙江省-鹤岗市-工农区', '97', '02', '04', '01', '1067', '', null, null);
INSERT INTO t_sys_area VALUES ('1068', '230404', '230404', '230404', '南山区', 'Nanshan Qu', 'Nanshan Qu', ',9,97,1068,', '黑龙江省-鹤岗市-南山区', '97', '02', '04', '01', '1068', '', null, null);
INSERT INTO t_sys_area VALUES ('1069', '230405', '230405', '230405', '兴安区', 'Xing,an Qu', 'Xing,an Qu', ',9,97,1069,', '黑龙江省-鹤岗市-兴安区', '97', '02', '04', '01', '1069', '', null, null);
INSERT INTO t_sys_area VALUES ('1070', '230406', '230406', '230406', '东山区', 'Dongshan Qu', 'Dongshan Qu', ',9,97,1070,', '黑龙江省-鹤岗市-东山区', '97', '02', '04', '01', '1070', '', null, null);
INSERT INTO t_sys_area VALUES ('1071', '230407', '230407', '230407', '兴山区', 'Xingshan Qu', 'Xingshan Qu', ',9,97,1071,', '黑龙江省-鹤岗市-兴山区', '97', '02', '04', '01', '1071', '', null, null);
INSERT INTO t_sys_area VALUES ('1072', '230421', '230421', '230421', '萝北县', 'Luobei Xian', 'Luobei Xian', ',9,97,1072,', '黑龙江省-鹤岗市-萝北县', '97', '02', '04', '01', '1072', '', null, null);
INSERT INTO t_sys_area VALUES ('1073', '230422', '230422', '230422', '绥滨县', 'Suibin Xian', 'Suibin Xian', ',9,97,1073,', '黑龙江省-鹤岗市-绥滨县', '97', '02', '04', '01', '1073', '', null, null);
INSERT INTO t_sys_area VALUES ('1074', '230501', '230501', '230501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',9,98,1074,', '黑龙江省-双鸭山市-市辖区', '98', '02', '04', '01', '1074', '', null, null);
INSERT INTO t_sys_area VALUES ('1075', '230502', '230502', '230502', '尖山区', 'Jianshan Qu', 'Jianshan Qu', ',9,98,1075,', '黑龙江省-双鸭山市-尖山区', '98', '02', '04', '01', '1075', '', null, null);
INSERT INTO t_sys_area VALUES ('1076', '230503', '230503', '230503', '岭东区', 'Lingdong Qu', 'Lingdong Qu', ',9,98,1076,', '黑龙江省-双鸭山市-岭东区', '98', '02', '04', '01', '1076', '', null, null);
INSERT INTO t_sys_area VALUES ('1077', '230505', '230505', '230505', '四方台区', 'Sifangtai Qu', 'Sifangtai Qu', ',9,98,1077,', '黑龙江省-双鸭山市-四方台区', '98', '02', '04', '01', '1077', '', null, null);
INSERT INTO t_sys_area VALUES ('1078', '230506', '230506', '230506', '宝山区', 'Baoshan Qu', 'Baoshan Qu', ',9,98,1078,', '黑龙江省-双鸭山市-宝山区', '98', '02', '04', '01', '1078', '', null, null);
INSERT INTO t_sys_area VALUES ('1079', '230521', '230521', '230521', '集贤县', 'Jixian Xian', 'Jixian Xian', ',9,98,1079,', '黑龙江省-双鸭山市-集贤县', '98', '02', '04', '01', '1079', '', null, null);
INSERT INTO t_sys_area VALUES ('1080', '230522', '230522', '230522', '友谊县', 'Youyi Xian', 'Youyi Xian', ',9,98,1080,', '黑龙江省-双鸭山市-友谊县', '98', '02', '04', '01', '1080', '', null, null);
INSERT INTO t_sys_area VALUES ('1081', '230523', '230523', '230523', '宝清县', 'Baoqing Xian', 'Baoqing Xian', ',9,98,1081,', '黑龙江省-双鸭山市-宝清县', '98', '02', '04', '01', '1081', '', null, null);
INSERT INTO t_sys_area VALUES ('1082', '230524', '230524', '230524', '饶河县', 'Raohe Xian ', 'Raohe Xian ', ',9,98,1082,', '黑龙江省-双鸭山市-饶河县', '98', '02', '04', '01', '1082', '', null, null);
INSERT INTO t_sys_area VALUES ('1083', '230601', '230601', '230601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',9,99,1083,', '黑龙江省-大庆市-市辖区', '99', '02', '04', '01', '1083', '', null, null);
INSERT INTO t_sys_area VALUES ('1084', '230602', '230602', '230602', '萨尔图区', 'Sairt Qu', 'Sairt Qu', ',9,99,1084,', '黑龙江省-大庆市-萨尔图区', '99', '02', '04', '01', '1084', '', null, null);
INSERT INTO t_sys_area VALUES ('1085', '230603', '230603', '230603', '龙凤区', 'Longfeng Qu', 'Longfeng Qu', ',9,99,1085,', '黑龙江省-大庆市-龙凤区', '99', '02', '04', '01', '1085', '', null, null);
INSERT INTO t_sys_area VALUES ('1086', '230604', '230604', '230604', '让胡路区', 'Ranghulu Qu', 'Ranghulu Qu', ',9,99,1086,', '黑龙江省-大庆市-让胡路区', '99', '02', '04', '01', '1086', '', null, null);
INSERT INTO t_sys_area VALUES ('1087', '230605', '230605', '230605', '红岗区', 'Honggang Qu', 'Honggang Qu', ',9,99,1087,', '黑龙江省-大庆市-红岗区', '99', '02', '04', '01', '1087', '', null, null);
INSERT INTO t_sys_area VALUES ('1088', '230606', '230606', '230606', '大同区', 'Datong Qu', 'Datong Qu', ',9,99,1088,', '黑龙江省-大庆市-大同区', '99', '02', '04', '01', '1088', '', null, null);
INSERT INTO t_sys_area VALUES ('1089', '230621', '230621', '230621', '肇州县', 'Zhaozhou Xian', 'Zhaozhou Xian', ',9,99,1089,', '黑龙江省-大庆市-肇州县', '99', '02', '04', '01', '1089', '', null, null);
INSERT INTO t_sys_area VALUES ('1090', '230622', '230622', '230622', '肇源县', 'Zhaoyuan Xian', 'Zhaoyuan Xian', ',9,99,1090,', '黑龙江省-大庆市-肇源县', '99', '02', '04', '01', '1090', '', null, null);
INSERT INTO t_sys_area VALUES ('1091', '230623', '230623', '230623', '林甸县', 'Lindian Xian ', 'Lindian Xian ', ',9,99,1091,', '黑龙江省-大庆市-林甸县', '99', '02', '04', '01', '1091', '', null, null);
INSERT INTO t_sys_area VALUES ('1092', '230624', '230624', '230624', '杜尔伯特蒙古族自治县', 'Dorbod Mongolzu Zizhixian', 'Dorbod Mongolzu Zizhixian', ',9,99,1092,', '黑龙江省-大庆市-杜尔伯特蒙古族自治县', '99', '02', '04', '01', '1092', '', null, null);
INSERT INTO t_sys_area VALUES ('1093', '230701', '230701', '230701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',9,100,1093,', '黑龙江省-伊春市-市辖区', '100', '02', '04', '01', '1093', '', null, null);
INSERT INTO t_sys_area VALUES ('1094', '230702', '230702', '230702', '伊春区', 'Yichun Qu', 'Yichun Qu', ',9,100,1094,', '黑龙江省-伊春市-伊春区', '100', '02', '04', '01', '1094', '', null, null);
INSERT INTO t_sys_area VALUES ('1095', '230703', '230703', '230703', '南岔区', 'Nancha Qu', 'Nancha Qu', ',9,100,1095,', '黑龙江省-伊春市-南岔区', '100', '02', '04', '01', '1095', '', null, null);
INSERT INTO t_sys_area VALUES ('1096', '230704', '230704', '230704', '友好区', 'Youhao Qu', 'Youhao Qu', ',9,100,1096,', '黑龙江省-伊春市-友好区', '100', '02', '04', '01', '1096', '', null, null);
INSERT INTO t_sys_area VALUES ('1097', '230705', '230705', '230705', '西林区', 'Xilin Qu', 'Xilin Qu', ',9,100,1097,', '黑龙江省-伊春市-西林区', '100', '02', '04', '01', '1097', '', null, null);
INSERT INTO t_sys_area VALUES ('1098', '230706', '230706', '230706', '翠峦区', 'Cuiluan Qu', 'Cuiluan Qu', ',9,100,1098,', '黑龙江省-伊春市-翠峦区', '100', '02', '04', '01', '1098', '', null, null);
INSERT INTO t_sys_area VALUES ('1099', '230707', '230707', '230707', '新青区', 'Xinqing Qu', 'Xinqing Qu', ',9,100,1099,', '黑龙江省-伊春市-新青区', '100', '02', '04', '01', '1099', '', null, null);
INSERT INTO t_sys_area VALUES ('1100', '230708', '230708', '230708', '美溪区', 'Meixi Qu', 'Meixi Qu', ',9,100,1100,', '黑龙江省-伊春市-美溪区', '100', '02', '04', '01', '1100', '', null, null);
INSERT INTO t_sys_area VALUES ('1101', '230709', '230709', '230709', '金山屯区', 'Jinshantun Qu', 'Jinshantun Qu', ',9,100,1101,', '黑龙江省-伊春市-金山屯区', '100', '02', '04', '01', '1101', '', null, null);
INSERT INTO t_sys_area VALUES ('1102', '230710', '230710', '230710', '五营区', 'Wuying Qu', 'Wuying Qu', ',9,100,1102,', '黑龙江省-伊春市-五营区', '100', '02', '04', '01', '1102', '', null, null);
INSERT INTO t_sys_area VALUES ('1103', '230711', '230711', '230711', '乌马河区', 'Wumahe Qu', 'Wumahe Qu', ',9,100,1103,', '黑龙江省-伊春市-乌马河区', '100', '02', '04', '01', '1103', '', null, null);
INSERT INTO t_sys_area VALUES ('1104', '230712', '230712', '230712', '汤旺河区', 'Tangwanghe Qu', 'Tangwanghe Qu', ',9,100,1104,', '黑龙江省-伊春市-汤旺河区', '100', '02', '04', '01', '1104', '', null, null);
INSERT INTO t_sys_area VALUES ('1105', '230713', '230713', '230713', '带岭区', 'Dailing Qu', 'Dailing Qu', ',9,100,1105,', '黑龙江省-伊春市-带岭区', '100', '02', '04', '01', '1105', '', null, null);
INSERT INTO t_sys_area VALUES ('1106', '230714', '230714', '230714', '乌伊岭区', 'Wuyiling Qu', 'Wuyiling Qu', ',9,100,1106,', '黑龙江省-伊春市-乌伊岭区', '100', '02', '04', '01', '1106', '', null, null);
INSERT INTO t_sys_area VALUES ('1107', '230715', '230715', '230715', '红星区', 'Hongxing Qu', 'Hongxing Qu', ',9,100,1107,', '黑龙江省-伊春市-红星区', '100', '02', '04', '01', '1107', '', null, null);
INSERT INTO t_sys_area VALUES ('1108', '230716', '230716', '230716', '上甘岭区', 'Shangganling Qu', 'Shangganling Qu', ',9,100,1108,', '黑龙江省-伊春市-上甘岭区', '100', '02', '04', '01', '1108', '', null, null);
INSERT INTO t_sys_area VALUES ('1109', '230722', '230722', '230722', '嘉荫县', 'Jiayin Xian', 'Jiayin Xian', ',9,100,1109,', '黑龙江省-伊春市-嘉荫县', '100', '02', '04', '01', '1109', '', null, null);
INSERT INTO t_sys_area VALUES ('1110', '230781', '230781', '230781', '铁力市', 'Tieli Shi', 'Tieli Shi', ',9,100,1110,', '黑龙江省-伊春市-铁力市', '100', '02', '04', '01', '1110', '', null, null);
INSERT INTO t_sys_area VALUES ('1111', '230801', '230801', '230801', '市辖区', 'Shixiaqu', 'Shixiaqu', ',9,101,1111,', '黑龙江省-佳木斯市-市辖区', '101', '02', '04', '01', '1111', '', null, null);
INSERT INTO t_sys_area VALUES ('1113', '230803', '230803', '230803', '向阳区', 'Xiangyang  Qu ', 'Xiangyang  Qu ', ',9,101,1113,', '黑龙江省-佳木斯市-向阳区', '101', '02', '04', '01', '1113', '', null, null);
INSERT INTO t_sys_area VALUES ('1114', '230804', '230804', '230804', '前进区', 'Qianjin Qu', 'Qianjin Qu', ',9,101,1114,', '黑龙江省-佳木斯市-前进区', '101', '02', '04', '01', '1114', '', null, null);
INSERT INTO t_sys_area VALUES ('1115', '230805', '230805', '230805', '东风区', 'Dongfeng Qu', 'Dongfeng Qu', ',9,101,1115,', '黑龙江省-佳木斯市-东风区', '101', '02', '04', '01', '1115', '', null, null);
INSERT INTO t_sys_area VALUES ('1116', '230811', '230811', '230811', '郊区', 'Jiaoqu', 'Jiaoqu', ',9,101,1116,', '黑龙江省-佳木斯市-郊区', '101', '02', '04', '01', '1116', '', null, null);
INSERT INTO t_sys_area VALUES ('1117', '230822', '230822', '230822', '桦南县', 'Huanan Xian', 'Huanan Xian', ',9,101,1117,', '黑龙江省-佳木斯市-桦南县', '101', '02', '04', '01', '1117', '', null, null);
INSERT INTO t_sys_area VALUES ('1118', '230826', '230826', '230826', '桦川县', 'Huachuan Xian', 'Huachuan Xian', ',9,101,1118,', '黑龙江省-佳木斯市-桦川县', '101', '02', '04', '01', '1118', '', null, null);
INSERT INTO t_sys_area VALUES ('1119', '230828', '230828', '230828', '汤原县', 'Tangyuan Xian', 'Tangyuan Xian', ',9,101,1119,', '黑龙江省-佳木斯市-汤原县', '101', '02', '04', '01', '1119', '', null, null);
INSERT INTO t_sys_area VALUES ('1120', '230833', '230833', '230833', '抚远县', 'Fuyuan Xian', 'Fuyuan Xian', ',9,101,1120,', '黑龙江省-佳木斯市-抚远县', '101', '02', '04', '01', '1120', '', null, null);
INSERT INTO t_sys_area VALUES ('1121', '230881', '230881', '230881', '同江市', 'Tongjiang Shi', 'Tongjiang Shi', ',9,101,1121,', '黑龙江省-佳木斯市-同江市', '101', '02', '04', '01', '1121', '', null, null);
INSERT INTO t_sys_area VALUES ('1122', '230882', '230882', '230882', '富锦市', 'Fujin Shi', 'Fujin Shi', ',9,101,1122,', '黑龙江省-佳木斯市-富锦市', '101', '02', '04', '01', '1122', '', null, null);
INSERT INTO t_sys_area VALUES ('1123', '230901', '230901', '230901', '市辖区', 'Shixiaqu', 'Shixiaqu', ',9,102,1123,', '黑龙江省-七台河市-市辖区', '102', '02', '04', '01', '1123', '', null, null);
INSERT INTO t_sys_area VALUES ('1124', '230902', '230902', '230902', '新兴区', 'Xinxing Qu', 'Xinxing Qu', ',9,102,1124,', '黑龙江省-七台河市-新兴区', '102', '02', '04', '01', '1124', '', null, null);
INSERT INTO t_sys_area VALUES ('1125', '230903', '230903', '230903', '桃山区', 'Taoshan Qu', 'Taoshan Qu', ',9,102,1125,', '黑龙江省-七台河市-桃山区', '102', '02', '04', '01', '1125', '', null, null);
INSERT INTO t_sys_area VALUES ('1126', '230904', '230904', '230904', '茄子河区', 'Qiezihe Qu', 'Qiezihe Qu', ',9,102,1126,', '黑龙江省-七台河市-茄子河区', '102', '02', '04', '01', '1126', '', null, null);
INSERT INTO t_sys_area VALUES ('1127', '230921', '230921', '230921', '勃利县', 'Boli Xian', 'Boli Xian', ',9,102,1127,', '黑龙江省-七台河市-勃利县', '102', '02', '04', '01', '1127', '', null, null);
INSERT INTO t_sys_area VALUES ('1128', '231001', '231001', '231001', '市辖区', 'Shixiaqu', 'Shixiaqu', ',9,103,1128,', '黑龙江省-牡丹江市-市辖区', '103', '02', '04', '01', '1128', '', null, null);
INSERT INTO t_sys_area VALUES ('1129', '231002', '231002', '231002', '东安区', 'Dong,an Qu', 'Dong,an Qu', ',9,103,1129,', '黑龙江省-牡丹江市-东安区', '103', '02', '04', '01', '1129', '', null, null);
INSERT INTO t_sys_area VALUES ('1130', '231003', '231003', '231003', '阳明区', 'Yangming Qu', 'Yangming Qu', ',9,103,1130,', '黑龙江省-牡丹江市-阳明区', '103', '02', '04', '01', '1130', '', null, null);
INSERT INTO t_sys_area VALUES ('1131', '231004', '231004', '231004', '爱民区', 'Aimin Qu', 'Aimin Qu', ',9,103,1131,', '黑龙江省-牡丹江市-爱民区', '103', '02', '04', '01', '1131', '', null, null);
INSERT INTO t_sys_area VALUES ('1132', '231005', '231005', '231005', '西安区', 'Xi,an Qu', 'Xi,an Qu', ',9,103,1132,', '黑龙江省-牡丹江市-西安区', '103', '02', '04', '01', '1132', '', null, null);
INSERT INTO t_sys_area VALUES ('1133', '231024', '231024', '231024', '东宁县', 'Dongning Xian', 'Dongning Xian', ',9,103,1133,', '黑龙江省-牡丹江市-东宁县', '103', '02', '04', '01', '1133', '', null, null);
INSERT INTO t_sys_area VALUES ('1134', '231025', '231025', '231025', '林口县', 'Linkou Xian', 'Linkou Xian', ',9,103,1134,', '黑龙江省-牡丹江市-林口县', '103', '02', '04', '01', '1134', '', null, null);
INSERT INTO t_sys_area VALUES ('1135', '231081', '231081', '231081', '绥芬河市', 'Suifenhe Shi', 'Suifenhe Shi', ',9,103,1135,', '黑龙江省-牡丹江市-绥芬河市', '103', '02', '04', '01', '1135', '', null, null);
INSERT INTO t_sys_area VALUES ('1136', '231083', '231083', '231083', '海林市', 'Hailin Shi', 'Hailin Shi', ',9,103,1136,', '黑龙江省-牡丹江市-海林市', '103', '02', '04', '01', '1136', '', null, null);
INSERT INTO t_sys_area VALUES ('1137', '231084', '231084', '231084', '宁安市', 'Ning,an Shi', 'Ning,an Shi', ',9,103,1137,', '黑龙江省-牡丹江市-宁安市', '103', '02', '04', '01', '1137', '', null, null);
INSERT INTO t_sys_area VALUES ('1138', '231085', '231085', '231085', '穆棱市', 'Muling Shi', 'Muling Shi', ',9,103,1138,', '黑龙江省-牡丹江市-穆棱市', '103', '02', '04', '01', '1138', '', null, null);
INSERT INTO t_sys_area VALUES ('1139', '231101', '231101', '231101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',9,104,1139,', '黑龙江省-黑河市-市辖区', '104', '02', '04', '01', '1139', '', null, null);
INSERT INTO t_sys_area VALUES ('1140', '231102', '231102', '231102', '爱辉区', 'Aihui Qu', 'Aihui Qu', ',9,104,1140,', '黑龙江省-黑河市-爱辉区', '104', '02', '04', '01', '1140', '', null, null);
INSERT INTO t_sys_area VALUES ('1141', '231121', '231121', '231121', '嫩江县', 'Nenjiang Xian', 'Nenjiang Xian', ',9,104,1141,', '黑龙江省-黑河市-嫩江县', '104', '02', '04', '01', '1141', '', null, null);
INSERT INTO t_sys_area VALUES ('1142', '231123', '231123', '231123', '逊克县', 'Xunke Xian', 'Xunke Xian', ',9,104,1142,', '黑龙江省-黑河市-逊克县', '104', '02', '04', '01', '1142', '', null, null);
INSERT INTO t_sys_area VALUES ('1143', '231124', '231124', '231124', '孙吴县', 'Sunwu Xian', 'Sunwu Xian', ',9,104,1143,', '黑龙江省-黑河市-孙吴县', '104', '02', '04', '01', '1143', '', null, null);
INSERT INTO t_sys_area VALUES ('1144', '231181', '231181', '231181', '北安市', 'Bei,an Shi', 'Bei,an Shi', ',9,104,1144,', '黑龙江省-黑河市-北安市', '104', '02', '04', '01', '1144', '', null, null);
INSERT INTO t_sys_area VALUES ('1145', '231182', '231182', '231182', '五大连池市', 'Wudalianchi Shi', 'Wudalianchi Shi', ',9,104,1145,', '黑龙江省-黑河市-五大连池市', '104', '02', '04', '01', '1145', '', null, null);
INSERT INTO t_sys_area VALUES ('1146', '231201', '231201', '231201', '市辖区', '1', '1', ',9,105,1146,', '黑龙江省-绥化市-市辖区', '105', '02', '04', '01', '1146', '', null, null);
INSERT INTO t_sys_area VALUES ('1147', '231202', '231202', '231202', '北林区', 'Beilin Qu', 'Beilin Qu', ',9,105,1147,', '黑龙江省-绥化市-北林区', '105', '02', '04', '01', '1147', '', null, null);
INSERT INTO t_sys_area VALUES ('1148', '231221', '231221', '231221', '望奎县', 'Wangkui Xian', 'Wangkui Xian', ',9,105,1148,', '黑龙江省-绥化市-望奎县', '105', '02', '04', '01', '1148', '', null, null);
INSERT INTO t_sys_area VALUES ('1149', '231222', '231222', '231222', '兰西县', 'Lanxi Xian', 'Lanxi Xian', ',9,105,1149,', '黑龙江省-绥化市-兰西县', '105', '02', '04', '01', '1149', '', null, null);
INSERT INTO t_sys_area VALUES ('1150', '231223', '231223', '231223', '青冈县', 'Qinggang Xian', 'Qinggang Xian', ',9,105,1150,', '黑龙江省-绥化市-青冈县', '105', '02', '04', '01', '1150', '', null, null);
INSERT INTO t_sys_area VALUES ('1151', '231224', '231224', '231224', '庆安县', 'Qing,an Xian', 'Qing,an Xian', ',9,105,1151,', '黑龙江省-绥化市-庆安县', '105', '02', '04', '01', '1151', '', null, null);
INSERT INTO t_sys_area VALUES ('1152', '231225', '231225', '231225', '明水县', 'Mingshui Xian', 'Mingshui Xian', ',9,105,1152,', '黑龙江省-绥化市-明水县', '105', '02', '04', '01', '1152', '', null, null);
INSERT INTO t_sys_area VALUES ('1153', '231226', '231226', '231226', '绥棱县', 'Suileng Xian', 'Suileng Xian', ',9,105,1153,', '黑龙江省-绥化市-绥棱县', '105', '02', '04', '01', '1153', '', null, null);
INSERT INTO t_sys_area VALUES ('1154', '231281', '231281', '231281', '安达市', 'Anda Shi', 'Anda Shi', ',9,105,1154,', '黑龙江省-绥化市-安达市', '105', '02', '04', '01', '1154', '', null, null);
INSERT INTO t_sys_area VALUES ('1155', '231282', '231282', '231282', '肇东市', 'Zhaodong Shi', 'Zhaodong Shi', ',9,105,1155,', '黑龙江省-绥化市-肇东市', '105', '02', '04', '01', '1155', '', null, null);
INSERT INTO t_sys_area VALUES ('1156', '231283', '231283', '231283', '海伦市', 'Hailun Shi', 'Hailun Shi', ',9,105,1156,', '黑龙江省-绥化市-海伦市', '105', '02', '04', '01', '1156', '', null, null);
INSERT INTO t_sys_area VALUES ('1157', '232721', '232721', '232721', '呼玛县', 'Huma Xian', 'Huma Xian', ',9,106,1157,', '黑龙江省-大兴安岭地区-呼玛县', '106', '02', '04', '01', '1157', '', null, null);
INSERT INTO t_sys_area VALUES ('1158', '232722', '232722', '232722', '塔河县', 'Tahe Xian', 'Tahe Xian', ',9,106,1158,', '黑龙江省-大兴安岭地区-塔河县', '106', '02', '04', '01', '1158', '', null, null);
INSERT INTO t_sys_area VALUES ('1159', '232723', '232723', '232723', '漠河县', 'Mohe Xian', 'Mohe Xian', ',9,106,1159,', '黑龙江省-大兴安岭地区-漠河县', '106', '02', '04', '01', '1159', '', null, null);
INSERT INTO t_sys_area VALUES ('1160', '310101', '310101', '310101', '黄浦区', 'Huangpu Qu', 'Huangpu Qu', ',10,107,1160,', '上海-上海市-黄浦区', '107', '02', '04', '01', '1160', '', null, null);
INSERT INTO t_sys_area VALUES ('1161', '310103', '310103', '310103', '卢湾区', 'Luwan Qu', 'Luwan Qu', ',10,107,1161,', '上海-上海市-卢湾区', '107', '02', '04', '01', '1161', '', null, null);
INSERT INTO t_sys_area VALUES ('1162', '310104', '310104', '310104', '徐汇区', 'Xuhui Qu', 'Xuhui Qu', ',10,107,1162,', '上海-上海市-徐汇区', '107', '02', '04', '01', '1162', '', null, null);
INSERT INTO t_sys_area VALUES ('1163', '310105', '310105', '310105', '长宁区', 'Changning Qu', 'Changning Qu', ',10,107,1163,', '上海-上海市-长宁区', '107', '02', '04', '01', '1163', '', null, null);
INSERT INTO t_sys_area VALUES ('1164', '310106', '310106', '310106', '静安区', 'Jing,an Qu', 'Jing,an Qu', ',10,107,1164,', '上海-上海市-静安区', '107', '02', '04', '01', '1164', '', null, null);
INSERT INTO t_sys_area VALUES ('1165', '310107', '310107', '310107', '普陀区', 'Putuo Qu', 'Putuo Qu', ',10,107,1165,', '上海-上海市-普陀区', '107', '02', '04', '01', '1165', '', null, null);
INSERT INTO t_sys_area VALUES ('1166', '310108', '310108', '310108', '闸北区', 'Zhabei Qu', 'Zhabei Qu', ',10,107,1166,', '上海-上海市-闸北区', '107', '02', '04', '01', '1166', '', null, null);
INSERT INTO t_sys_area VALUES ('1167', '310109', '310109', '310109', '虹口区', 'Hongkou Qu', 'Hongkou Qu', ',10,107,1167,', '上海-上海市-虹口区', '107', '02', '04', '01', '1167', '', null, null);
INSERT INTO t_sys_area VALUES ('1168', '310110', '310110', '310110', '杨浦区', 'Yangpu Qu', 'Yangpu Qu', ',10,107,1168,', '上海-上海市-杨浦区', '107', '02', '04', '01', '1168', '', null, null);
INSERT INTO t_sys_area VALUES ('1169', '310112', '310112', '310112', '闵行区', 'Minhang Qu', 'Minhang Qu', ',10,107,1169,', '上海-上海市-闵行区', '107', '02', '04', '01', '1169', '', null, null);
INSERT INTO t_sys_area VALUES ('1170', '310113', '310113', '310113', '宝山区', 'Baoshan Qu', 'Baoshan Qu', ',10,107,1170,', '上海-上海市-宝山区', '107', '02', '04', '01', '1170', '', null, null);
INSERT INTO t_sys_area VALUES ('1171', '310114', '310114', '310114', '嘉定区', 'Jiading Qu', 'Jiading Qu', ',10,107,1171,', '上海-上海市-嘉定区', '107', '02', '04', '01', '1171', '', null, null);
INSERT INTO t_sys_area VALUES ('1172', '310115', '310115', '310115', '浦东新区', 'Pudong Xinqu', 'Pudong Xinqu', ',10,107,1172,', '上海-上海市-浦东新区', '107', '02', '04', '01', '1172', '', null, null);
INSERT INTO t_sys_area VALUES ('1173', '310116', '310116', '310116', '金山区', 'Jinshan Qu', 'Jinshan Qu', ',10,107,1173,', '上海-上海市-金山区', '107', '02', '04', '01', '1173', '', null, null);
INSERT INTO t_sys_area VALUES ('1174', '310117', '310117', '310117', '松江区', 'Songjiang Qu', 'Songjiang Qu', ',10,107,1174,', '上海-上海市-松江区', '107', '02', '04', '01', '1174', '', null, null);
INSERT INTO t_sys_area VALUES ('1175', '310118', '310118', '310118', '青浦区', 'Qingpu  Qu', 'Qingpu  Qu', ',10,107,1175,', '上海-上海市-青浦区', '107', '02', '04', '01', '1175', '', null, null);
INSERT INTO t_sys_area VALUES ('1177', '310120', '310120', '310120', '奉贤区', 'Fengxian Qu', 'Fengxian Qu', ',10,107,1177,', '上海-上海市-奉贤区', '107', '02', '04', '01', '1177', '', null, null);
INSERT INTO t_sys_area VALUES ('1178', '310230', '310230', '310230', '崇明县', 'Chongming Xian', 'Chongming Xian', ',10,107,1178,', '上海-上海市-崇明县', '107', '02', '04', '01', '1178', '', null, null);
INSERT INTO t_sys_area VALUES ('1179', '320101', '320101', '320101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',11,109,1179,', '江苏省-南京市-市辖区', '109', '02', '04', '01', '1179', '', null, null);
INSERT INTO t_sys_area VALUES ('1180', '320102', '320102', '320102', '玄武区', 'Xuanwu Qu', 'Xuanwu Qu', ',11,109,1180,', '江苏省-南京市-玄武区', '109', '02', '04', '01', '1180', '', null, null);
INSERT INTO t_sys_area VALUES ('1181', '320103', '320103', '320103', '白下区', 'Baixia Qu', 'Baixia Qu', ',11,109,1181,', '江苏省-南京市-白下区', '109', '02', '04', '01', '1181', '', null, null);
INSERT INTO t_sys_area VALUES ('1182', '320104', '320104', '320104', '秦淮区', 'Qinhuai Qu', 'Qinhuai Qu', ',11,109,1182,', '江苏省-南京市-秦淮区', '109', '02', '04', '01', '1182', '', null, null);
INSERT INTO t_sys_area VALUES ('1183', '320105', '320105', '320105', '建邺区', 'Jianye Qu', 'Jianye Qu', ',11,109,1183,', '江苏省-南京市-建邺区', '109', '02', '04', '01', '1183', '', null, null);
INSERT INTO t_sys_area VALUES ('1184', '320106', '320106', '320106', '鼓楼区', 'Gulou Qu', 'Gulou Qu', ',11,109,1184,', '江苏省-南京市-鼓楼区', '109', '02', '04', '01', '1184', '', null, null);
INSERT INTO t_sys_area VALUES ('1185', '320107', '320107', '320107', '下关区', 'Xiaguan Qu', 'Xiaguan Qu', ',11,109,1185,', '江苏省-南京市-下关区', '109', '02', '04', '01', '1185', '', null, null);
INSERT INTO t_sys_area VALUES ('1186', '320111', '320111', '320111', '浦口区', 'Pukou Qu', 'Pukou Qu', ',11,109,1186,', '江苏省-南京市-浦口区', '109', '02', '04', '01', '1186', '', null, null);
INSERT INTO t_sys_area VALUES ('1187', '320113', '320113', '320113', '栖霞区', 'Qixia Qu', 'Qixia Qu', ',11,109,1187,', '江苏省-南京市-栖霞区', '109', '02', '04', '01', '1187', '', null, null);
INSERT INTO t_sys_area VALUES ('1188', '320114', '320114', '320114', '雨花台区', 'Yuhuatai Qu', 'Yuhuatai Qu', ',11,109,1188,', '江苏省-南京市-雨花台区', '109', '02', '04', '01', '1188', '', null, null);
INSERT INTO t_sys_area VALUES ('1189', '320115', '320115', '320115', '江宁区', 'Jiangning Qu', 'Jiangning Qu', ',11,109,1189,', '江苏省-南京市-江宁区', '109', '02', '04', '01', '1189', '', null, null);
INSERT INTO t_sys_area VALUES ('1190', '320116', '320116', '320116', '六合区', 'Liuhe Qu', 'Liuhe Qu', ',11,109,1190,', '江苏省-南京市-六合区', '109', '02', '04', '01', '1190', '', null, null);
INSERT INTO t_sys_area VALUES ('1191', '320124', '320124', '320124', '溧水县', 'Lishui Xian', 'Lishui Xian', ',11,109,1191,', '江苏省-南京市-溧水县', '109', '02', '04', '01', '1191', '', null, null);
INSERT INTO t_sys_area VALUES ('1192', '320125', '320125', '320125', '高淳县', 'Gaochun Xian', 'Gaochun Xian', ',11,109,1192,', '江苏省-南京市-高淳县', '109', '02', '04', '01', '1192', '', null, null);
INSERT INTO t_sys_area VALUES ('1193', '320201', '320201', '320201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',11,110,1193,', '江苏省-无锡市-市辖区', '110', '02', '04', '01', '1193', '', null, null);
INSERT INTO t_sys_area VALUES ('1194', '320202', '320202', '320202', '崇安区', 'Chong,an Qu', 'Chong,an Qu', ',11,110,1194,', '江苏省-无锡市-崇安区', '110', '02', '04', '01', '1194', '', null, null);
INSERT INTO t_sys_area VALUES ('1195', '320203', '320203', '320203', '南长区', 'Nanchang Qu', 'Nanchang Qu', ',11,110,1195,', '江苏省-无锡市-南长区', '110', '02', '04', '01', '1195', '', null, null);
INSERT INTO t_sys_area VALUES ('1196', '320204', '320204', '320204', '北塘区', 'Beitang Qu', 'Beitang Qu', ',11,110,1196,', '江苏省-无锡市-北塘区', '110', '02', '04', '01', '1196', '', null, null);
INSERT INTO t_sys_area VALUES ('1197', '320205', '320205', '320205', '锡山区', 'Xishan Qu', 'Xishan Qu', ',11,110,1197,', '江苏省-无锡市-锡山区', '110', '02', '04', '01', '1197', '', null, null);
INSERT INTO t_sys_area VALUES ('1198', '320206', '320206', '320206', '惠山区', 'Huishan Qu', 'Huishan Qu', ',11,110,1198,', '江苏省-无锡市-惠山区', '110', '02', '04', '01', '1198', '', null, null);
INSERT INTO t_sys_area VALUES ('1199', '320211', '320211', '320211', '滨湖区', 'Binhu Qu', 'Binhu Qu', ',11,110,1199,', '江苏省-无锡市-滨湖区', '110', '02', '04', '01', '1199', '', null, null);
INSERT INTO t_sys_area VALUES ('1200', '320281', '320281', '320281', '江阴市', 'Jiangyin Shi', 'Jiangyin Shi', ',11,110,1200,', '江苏省-无锡市-江阴市', '110', '02', '04', '01', '1200', '', null, null);
INSERT INTO t_sys_area VALUES ('1201', '320282', '320282', '320282', '宜兴市', 'Yixing Shi', 'Yixing Shi', ',11,110,1201,', '江苏省-无锡市-宜兴市', '110', '02', '04', '01', '1201', '', null, null);
INSERT INTO t_sys_area VALUES ('1202', '320301', '320301', '320301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',11,111,1202,', '江苏省-徐州市-市辖区', '111', '02', '04', '01', '1202', '', null, null);
INSERT INTO t_sys_area VALUES ('1203', '320302', '320302', '320302', '鼓楼区', 'Gulou Qu', 'Gulou Qu', ',11,111,1203,', '江苏省-徐州市-鼓楼区', '111', '02', '04', '01', '1203', '', null, null);
INSERT INTO t_sys_area VALUES ('1204', '320303', '320303', '320303', '云龙区', 'Yunlong Qu', 'Yunlong Qu', ',11,111,1204,', '江苏省-徐州市-云龙区', '111', '02', '04', '01', '1204', '', null, null);
INSERT INTO t_sys_area VALUES ('1206', '320305', '320305', '320305', '贾汪区', 'Jiawang Qu', 'Jiawang Qu', ',11,111,1206,', '江苏省-徐州市-贾汪区', '111', '02', '04', '01', '1206', '', null, null);
INSERT INTO t_sys_area VALUES ('1207', '320311', '320311', '320311', '泉山区', 'Quanshan Qu', 'Quanshan Qu', ',11,111,1207,', '江苏省-徐州市-泉山区', '111', '02', '04', '01', '1207', '', null, null);
INSERT INTO t_sys_area VALUES ('1208', '320321', '320321', '320321', '丰县', 'Feng Xian', 'Feng Xian', ',11,111,1208,', '江苏省-徐州市-丰县', '111', '02', '04', '01', '1208', '', null, null);
INSERT INTO t_sys_area VALUES ('1209', '320322', '320322', '320322', '沛县', 'Pei Xian', 'Pei Xian', ',11,111,1209,', '江苏省-徐州市-沛县', '111', '02', '04', '01', '1209', '', null, null);
INSERT INTO t_sys_area VALUES ('1210', '320312', '320312', '320312', '铜山区', 'Tongshan Xian', 'Tongshan Xian', ',11,111,1210,', '江苏省-徐州市-铜山区', '111', '02', '04', '01', '1210', '', null, null);
INSERT INTO t_sys_area VALUES ('1211', '320324', '320324', '320324', '睢宁县', 'Suining Xian', 'Suining Xian', ',11,111,1211,', '江苏省-徐州市-睢宁县', '111', '02', '04', '01', '1211', '', null, null);
INSERT INTO t_sys_area VALUES ('1212', '320381', '320381', '320381', '新沂市', 'Xinyi Shi', 'Xinyi Shi', ',11,111,1212,', '江苏省-徐州市-新沂市', '111', '02', '04', '01', '1212', '', null, null);
INSERT INTO t_sys_area VALUES ('1213', '320382', '320382', '320382', '邳州市', 'Pizhou Shi', 'Pizhou Shi', ',11,111,1213,', '江苏省-徐州市-邳州市', '111', '02', '04', '01', '1213', '', null, null);
INSERT INTO t_sys_area VALUES ('1214', '320401', '320401', '320401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',11,112,1214,', '江苏省-常州市-市辖区', '112', '02', '04', '01', '1214', '', null, null);
INSERT INTO t_sys_area VALUES ('1215', '320402', '320402', '320402', '天宁区', 'Tianning Qu', 'Tianning Qu', ',11,112,1215,', '江苏省-常州市-天宁区', '112', '02', '04', '01', '1215', '', null, null);
INSERT INTO t_sys_area VALUES ('1216', '320404', '320404', '320404', '钟楼区', 'Zhonglou Qu', 'Zhonglou Qu', ',11,112,1216,', '江苏省-常州市-钟楼区', '112', '02', '04', '01', '1216', '', null, null);
INSERT INTO t_sys_area VALUES ('1217', '320405', '320405', '320405', '戚墅堰区', 'Qishuyan Qu', 'Qishuyan Qu', ',11,112,1217,', '江苏省-常州市-戚墅堰区', '112', '02', '04', '01', '1217', '', null, null);
INSERT INTO t_sys_area VALUES ('1218', '320411', '320411', '320411', '新北区', 'Xinbei Qu', 'Xinbei Qu', ',11,112,1218,', '江苏省-常州市-新北区', '112', '02', '04', '01', '1218', '', null, null);
INSERT INTO t_sys_area VALUES ('1219', '320412', '320412', '320412', '武进区', 'Wujin Qu', 'Wujin Qu', ',11,112,1219,', '江苏省-常州市-武进区', '112', '02', '04', '01', '1219', '', null, null);
INSERT INTO t_sys_area VALUES ('1220', '320481', '320481', '320481', '溧阳市', 'Liyang Shi', 'Liyang Shi', ',11,112,1220,', '江苏省-常州市-溧阳市', '112', '02', '04', '01', '1220', '', null, null);
INSERT INTO t_sys_area VALUES ('1221', '320482', '320482', '320482', '金坛市', 'Jintan Shi', 'Jintan Shi', ',11,112,1221,', '江苏省-常州市-金坛市', '112', '02', '04', '01', '1221', '', null, null);
INSERT INTO t_sys_area VALUES ('1222', '320501', '320501', '320501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',11,113,1222,', '江苏省-苏州市-市辖区', '113', '02', '04', '01', '1222', '', null, null);
INSERT INTO t_sys_area VALUES ('1223', '320502', '320502', '320502', '沧浪区', 'Canglang Qu', 'Canglang Qu', ',11,113,1223,', '江苏省-苏州市-沧浪区', '113', '02', '04', '01', '1223', '', null, null);
INSERT INTO t_sys_area VALUES ('1224', '320503', '320503', '320503', '平江区', 'Pingjiang Qu', 'Pingjiang Qu', ',11,113,1224,', '江苏省-苏州市-平江区', '113', '02', '04', '01', '1224', '', null, null);
INSERT INTO t_sys_area VALUES ('1225', '320504', '320504', '320504', '金阊区', 'Jinchang Qu', 'Jinchang Qu', ',11,113,1225,', '江苏省-苏州市-金阊区', '113', '02', '04', '01', '1225', '', null, null);
INSERT INTO t_sys_area VALUES ('1226', '320505', '320505', '320505', '虎丘区', 'Huqiu Qu', 'Huqiu Qu', ',11,113,1226,', '江苏省-苏州市-虎丘区', '113', '02', '04', '01', '1226', '', null, null);
INSERT INTO t_sys_area VALUES ('1227', '320506', '320506', '320506', '吴中区', 'Wuzhong Qu', 'Wuzhong Qu', ',11,113,1227,', '江苏省-苏州市-吴中区', '113', '02', '04', '01', '1227', '', null, null);
INSERT INTO t_sys_area VALUES ('1228', '320507', '320507', '320507', '相城区', 'Xiangcheng Qu', 'Xiangcheng Qu', ',11,113,1228,', '江苏省-苏州市-相城区', '113', '02', '04', '01', '1228', '', null, null);
INSERT INTO t_sys_area VALUES ('1229', '320581', '320581', '320581', '常熟市', 'Changshu Shi', 'Changshu Shi', ',11,113,1229,', '江苏省-苏州市-常熟市', '113', '02', '04', '01', '1229', '', null, null);
INSERT INTO t_sys_area VALUES ('1230', '320582', '320582', '320582', '张家港市', 'Zhangjiagang Shi ', 'Zhangjiagang Shi ', ',11,113,1230,', '江苏省-苏州市-张家港市', '113', '02', '04', '01', '1230', '', null, null);
INSERT INTO t_sys_area VALUES ('1231', '320583', '320583', '320583', '昆山市', 'Kunshan Shi', 'Kunshan Shi', ',11,113,1231,', '江苏省-苏州市-昆山市', '113', '02', '04', '01', '1231', '', null, null);
INSERT INTO t_sys_area VALUES ('1232', '320584', '320584', '320584', '吴江市', 'Wujiang Shi', 'Wujiang Shi', ',11,113,1232,', '江苏省-苏州市-吴江市', '113', '02', '04', '01', '1232', '', null, null);
INSERT INTO t_sys_area VALUES ('1233', '320585', '320585', '320585', '太仓市', 'Taicang Shi', 'Taicang Shi', ',11,113,1233,', '江苏省-苏州市-太仓市', '113', '02', '04', '01', '1233', '', null, null);
INSERT INTO t_sys_area VALUES ('1234', '320601', '320601', '320601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',11,114,1234,', '江苏省-南通市-市辖区', '114', '02', '04', '01', '1234', '', null, null);
INSERT INTO t_sys_area VALUES ('1235', '320602', '320602', '320602', '崇川区', 'Chongchuan Qu', 'Chongchuan Qu', ',11,114,1235,', '江苏省-南通市-崇川区', '114', '02', '04', '01', '1235', '', null, null);
INSERT INTO t_sys_area VALUES ('1236', '320611', '320611', '320611', '港闸区', 'Gangzha Qu', 'Gangzha Qu', ',11,114,1236,', '江苏省-南通市-港闸区', '114', '02', '04', '01', '1236', '', null, null);
INSERT INTO t_sys_area VALUES ('1237', '320621', '320621', '320621', '海安县', 'Hai,an Xian', 'Hai,an Xian', ',11,114,1237,', '江苏省-南通市-海安县', '114', '02', '04', '01', '1237', '', null, null);
INSERT INTO t_sys_area VALUES ('1238', '320623', '320623', '320623', '如东县', 'Rudong Xian', 'Rudong Xian', ',11,114,1238,', '江苏省-南通市-如东县', '114', '02', '04', '01', '1238', '', null, null);
INSERT INTO t_sys_area VALUES ('1239', '320681', '320681', '320681', '启东市', 'Qidong Shi', 'Qidong Shi', ',11,114,1239,', '江苏省-南通市-启东市', '114', '02', '04', '01', '1239', '', null, null);
INSERT INTO t_sys_area VALUES ('1240', '320682', '320682', '320682', '如皋市', 'Rugao Shi', 'Rugao Shi', ',11,114,1240,', '江苏省-南通市-如皋市', '114', '02', '04', '01', '1240', '', null, null);
INSERT INTO t_sys_area VALUES ('1241', '320612', '320612', '320612', '通州区', 'Tongzhou Shi', 'Tongzhou Shi', ',11,114,1241,', '江苏省-南通市-通州区', '114', '02', '04', '01', '1241', '', null, null);
INSERT INTO t_sys_area VALUES ('1242', '320684', '320684', '320684', '海门市', 'Haimen Shi', 'Haimen Shi', ',11,114,1242,', '江苏省-南通市-海门市', '114', '02', '04', '01', '1242', '', null, null);
INSERT INTO t_sys_area VALUES ('1243', '320701', '320701', '320701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',11,115,1243,', '江苏省-连云港市-市辖区', '115', '02', '04', '01', '1243', '', null, null);
INSERT INTO t_sys_area VALUES ('1244', '320703', '320703', '320703', '连云区', 'Lianyun Qu', 'Lianyun Qu', ',11,115,1244,', '江苏省-连云港市-连云区', '115', '02', '04', '01', '1244', '', null, null);
INSERT INTO t_sys_area VALUES ('1245', '320705', '320705', '320705', '新浦区', 'Xinpu Qu', 'Xinpu Qu', ',11,115,1245,', '江苏省-连云港市-新浦区', '115', '02', '04', '01', '1245', '', null, null);
INSERT INTO t_sys_area VALUES ('1246', '320706', '320706', '320706', '海州区', 'Haizhou Qu', 'Haizhou Qu', ',11,115,1246,', '江苏省-连云港市-海州区', '115', '02', '04', '01', '1246', '', null, null);
INSERT INTO t_sys_area VALUES ('1247', '320721', '320721', '320721', '赣榆县', 'Ganyu Xian', 'Ganyu Xian', ',11,115,1247,', '江苏省-连云港市-赣榆县', '115', '02', '04', '01', '1247', '', null, null);
INSERT INTO t_sys_area VALUES ('1248', '320722', '320722', '320722', '东海县', 'Donghai Xian', 'Donghai Xian', ',11,115,1248,', '江苏省-连云港市-东海县', '115', '02', '04', '01', '1248', '', null, null);
INSERT INTO t_sys_area VALUES ('1249', '320723', '320723', '320723', '灌云县', 'Guanyun Xian', 'Guanyun Xian', ',11,115,1249,', '江苏省-连云港市-灌云县', '115', '02', '04', '01', '1249', '', null, null);
INSERT INTO t_sys_area VALUES ('1250', '320724', '320724', '320724', '灌南县', 'Guannan Xian', 'Guannan Xian', ',11,115,1250,', '江苏省-连云港市-灌南县', '115', '02', '04', '01', '1250', '', null, null);
INSERT INTO t_sys_area VALUES ('1251', '320801', '320801', '320801', '市辖区', 'Shixiaqu', 'Shixiaqu', ',11,116,1251,', '江苏省-淮安市-市辖区', '116', '02', '04', '01', '1251', '', null, null);
INSERT INTO t_sys_area VALUES ('1252', '320802', '320802', '320802', '清河区', 'Qinghe Qu', 'Qinghe Qu', ',11,116,1252,', '江苏省-淮安市-清河区', '116', '02', '04', '01', '1252', '', null, null);
INSERT INTO t_sys_area VALUES ('1253', '320803', '320803', '320803', '楚州区', 'Chuzhou Qu', 'Chuzhou Qu', ',11,116,1253,', '江苏省-淮安市-楚州区', '116', '02', '04', '01', '1253', '', null, null);
INSERT INTO t_sys_area VALUES ('1254', '320804', '320804', '320804', '淮阴区', 'Huaiyin Qu', 'Huaiyin Qu', ',11,116,1254,', '江苏省-淮安市-淮阴区', '116', '02', '04', '01', '1254', '', null, null);
INSERT INTO t_sys_area VALUES ('1255', '320811', '320811', '320811', '清浦区', 'Qingpu Qu', 'Qingpu Qu', ',11,116,1255,', '江苏省-淮安市-清浦区', '116', '02', '04', '01', '1255', '', null, null);
INSERT INTO t_sys_area VALUES ('1256', '320826', '320826', '320826', '涟水县', 'Lianshui Xian', 'Lianshui Xian', ',11,116,1256,', '江苏省-淮安市-涟水县', '116', '02', '04', '01', '1256', '', null, null);
INSERT INTO t_sys_area VALUES ('1257', '320829', '320829', '320829', '洪泽县', 'Hongze Xian', 'Hongze Xian', ',11,116,1257,', '江苏省-淮安市-洪泽县', '116', '02', '04', '01', '1257', '', null, null);
INSERT INTO t_sys_area VALUES ('1258', '320830', '320830', '320830', '盱眙县', 'Xuyi Xian', 'Xuyi Xian', ',11,116,1258,', '江苏省-淮安市-盱眙县', '116', '02', '04', '01', '1258', '', null, null);
INSERT INTO t_sys_area VALUES ('1259', '320831', '320831', '320831', '金湖县', 'Jinhu Xian', 'Jinhu Xian', ',11,116,1259,', '江苏省-淮安市-金湖县', '116', '02', '04', '01', '1259', '', null, null);
INSERT INTO t_sys_area VALUES ('1260', '320901', '320901', '320901', '市辖区', 'Shixiaqu', 'Shixiaqu', ',11,117,1260,', '江苏省-盐城市-市辖区', '117', '02', '04', '01', '1260', '', null, null);
INSERT INTO t_sys_area VALUES ('1261', '320902', '320902', '320902', '亭湖区', 'Tinghu Qu', 'Tinghu Qu', ',11,117,1261,', '江苏省-盐城市-亭湖区', '117', '02', '04', '01', '1261', '', null, null);
INSERT INTO t_sys_area VALUES ('1262', '320903', '320903', '320903', '盐都区', 'Yandu Qu', 'Yandu Qu', ',11,117,1262,', '江苏省-盐城市-盐都区', '117', '02', '04', '01', '1262', '', null, null);
INSERT INTO t_sys_area VALUES ('1263', '320921', '320921', '320921', '响水县', 'Xiangshui Xian', 'Xiangshui Xian', ',11,117,1263,', '江苏省-盐城市-响水县', '117', '02', '04', '01', '1263', '', null, null);
INSERT INTO t_sys_area VALUES ('1264', '320922', '320922', '320922', '滨海县', 'Binhai Xian', 'Binhai Xian', ',11,117,1264,', '江苏省-盐城市-滨海县', '117', '02', '04', '01', '1264', '', null, null);
INSERT INTO t_sys_area VALUES ('1265', '320923', '320923', '320923', '阜宁县', 'Funing Xian', 'Funing Xian', ',11,117,1265,', '江苏省-盐城市-阜宁县', '117', '02', '04', '01', '1265', '', null, null);
INSERT INTO t_sys_area VALUES ('1266', '320924', '320924', '320924', '射阳县', 'Sheyang Xian', 'Sheyang Xian', ',11,117,1266,', '江苏省-盐城市-射阳县', '117', '02', '04', '01', '1266', '', null, null);
INSERT INTO t_sys_area VALUES ('1267', '320925', '320925', '320925', '建湖县', 'Jianhu Xian', 'Jianhu Xian', ',11,117,1267,', '江苏省-盐城市-建湖县', '117', '02', '04', '01', '1267', '', null, null);
INSERT INTO t_sys_area VALUES ('1268', '320981', '320981', '320981', '东台市', 'Dongtai Shi', 'Dongtai Shi', ',11,117,1268,', '江苏省-盐城市-东台市', '117', '02', '04', '01', '1268', '', null, null);
INSERT INTO t_sys_area VALUES ('1269', '320982', '320982', '320982', '大丰市', 'Dafeng Shi', 'Dafeng Shi', ',11,117,1269,', '江苏省-盐城市-大丰市', '117', '02', '04', '01', '1269', '', null, null);
INSERT INTO t_sys_area VALUES ('1270', '321001', '321001', '321001', '市辖区', 'Shixiaqu', 'Shixiaqu', ',11,118,1270,', '江苏省-扬州市-市辖区', '118', '02', '04', '01', '1270', '', null, null);
INSERT INTO t_sys_area VALUES ('1271', '321002', '321002', '321002', '广陵区', 'Guangling Qu', 'Guangling Qu', ',11,118,1271,', '江苏省-扬州市-广陵区', '118', '02', '04', '01', '1271', '', null, null);
INSERT INTO t_sys_area VALUES ('1272', '321003', '321003', '321003', '邗江区', 'Hanjiang Qu', 'Hanjiang Qu', ',11,118,1272,', '江苏省-扬州市-邗江区', '118', '02', '04', '01', '1272', '', null, null);
INSERT INTO t_sys_area VALUES ('1273', '321011', '321011', '321011', '维扬区', 'Weiyang Qu', 'Weiyang Qu', ',11,118,1273,', '江苏省-扬州市-维扬区', '118', '02', '04', '01', '1273', '', null, null);
INSERT INTO t_sys_area VALUES ('1274', '321023', '321023', '321023', '宝应县', 'Baoying Xian ', 'Baoying Xian ', ',11,118,1274,', '江苏省-扬州市-宝应县', '118', '02', '04', '01', '1274', '', null, null);
INSERT INTO t_sys_area VALUES ('1275', '321081', '321081', '321081', '仪征市', 'Yizheng Shi', 'Yizheng Shi', ',11,118,1275,', '江苏省-扬州市-仪征市', '118', '02', '04', '01', '1275', '', null, null);
INSERT INTO t_sys_area VALUES ('1276', '321084', '321084', '321084', '高邮市', 'Gaoyou Shi', 'Gaoyou Shi', ',11,118,1276,', '江苏省-扬州市-高邮市', '118', '02', '04', '01', '1276', '', null, null);
INSERT INTO t_sys_area VALUES ('1277', '321088', '321088', '321088', '江都市', 'Jiangdu Shi', 'Jiangdu Shi', ',11,118,1277,', '江苏省-扬州市-江都市', '118', '02', '04', '01', '1277', '', null, null);
INSERT INTO t_sys_area VALUES ('1278', '321101', '321101', '321101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',11,119,1278,', '江苏省-镇江市-市辖区', '119', '02', '04', '01', '1278', '', null, null);
INSERT INTO t_sys_area VALUES ('1279', '321102', '321102', '321102', '京口区', 'Jingkou Qu', 'Jingkou Qu', ',11,119,1279,', '江苏省-镇江市-京口区', '119', '02', '04', '01', '1279', '', null, null);
INSERT INTO t_sys_area VALUES ('1280', '321111', '321111', '321111', '润州区', 'Runzhou Qu', 'Runzhou Qu', ',11,119,1280,', '江苏省-镇江市-润州区', '119', '02', '04', '01', '1280', '', null, null);
INSERT INTO t_sys_area VALUES ('1281', '321112', '321112', '321112', '丹徒区', 'Dantu Qu', 'Dantu Qu', ',11,119,1281,', '江苏省-镇江市-丹徒区', '119', '02', '04', '01', '1281', '', null, null);
INSERT INTO t_sys_area VALUES ('1282', '321181', '321181', '321181', '丹阳市', 'Danyang Xian', 'Danyang Xian', ',11,119,1282,', '江苏省-镇江市-丹阳市', '119', '02', '04', '01', '1282', '', null, null);
INSERT INTO t_sys_area VALUES ('1283', '321182', '321182', '321182', '扬中市', 'Yangzhong Shi', 'Yangzhong Shi', ',11,119,1283,', '江苏省-镇江市-扬中市', '119', '02', '04', '01', '1283', '', null, null);
INSERT INTO t_sys_area VALUES ('1284', '321183', '321183', '321183', '句容市', 'Jurong Shi', 'Jurong Shi', ',11,119,1284,', '江苏省-镇江市-句容市', '119', '02', '04', '01', '1284', '', null, null);
INSERT INTO t_sys_area VALUES ('1285', '321201', '321201', '321201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',11,120,1285,', '江苏省-泰州市-市辖区', '120', '02', '04', '01', '1285', '', null, null);
INSERT INTO t_sys_area VALUES ('1286', '321202', '321202', '321202', '海陵区', 'Hailing Qu', 'Hailing Qu', ',11,120,1286,', '江苏省-泰州市-海陵区', '120', '02', '04', '01', '1286', '', null, null);
INSERT INTO t_sys_area VALUES ('1287', '321203', '321203', '321203', '高港区', 'Gaogang Qu', 'Gaogang Qu', ',11,120,1287,', '江苏省-泰州市-高港区', '120', '02', '04', '01', '1287', '', null, null);
INSERT INTO t_sys_area VALUES ('1288', '321281', '321281', '321281', '兴化市', 'Xinghua Shi', 'Xinghua Shi', ',11,120,1288,', '江苏省-泰州市-兴化市', '120', '02', '04', '01', '1288', '', null, null);
INSERT INTO t_sys_area VALUES ('1289', '321282', '321282', '321282', '靖江市', 'Jingjiang Shi', 'Jingjiang Shi', ',11,120,1289,', '江苏省-泰州市-靖江市', '120', '02', '04', '01', '1289', '', null, null);
INSERT INTO t_sys_area VALUES ('1290', '321283', '321283', '321283', '泰兴市', 'Taixing Shi', 'Taixing Shi', ',11,120,1290,', '江苏省-泰州市-泰兴市', '120', '02', '04', '01', '1290', '', null, null);
INSERT INTO t_sys_area VALUES ('1291', '321284', '321284', '321284', '姜堰市', 'Jiangyan Shi', 'Jiangyan Shi', ',11,120,1291,', '江苏省-泰州市-姜堰市', '120', '02', '04', '01', '1291', '', null, null);
INSERT INTO t_sys_area VALUES ('1292', '321301', '321301', '321301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',11,121,1292,', '江苏省-宿迁市-市辖区', '121', '02', '04', '01', '1292', '', null, null);
INSERT INTO t_sys_area VALUES ('1293', '321302', '321302', '321302', '宿城区', 'Sucheng Qu', 'Sucheng Qu', ',11,121,1293,', '江苏省-宿迁市-宿城区', '121', '02', '04', '01', '1293', '', null, null);
INSERT INTO t_sys_area VALUES ('1294', '321311', '321311', '321311', '宿豫区', 'Suyu Qu', 'Suyu Qu', ',11,121,1294,', '江苏省-宿迁市-宿豫区', '121', '02', '04', '01', '1294', '', null, null);
INSERT INTO t_sys_area VALUES ('1295', '321322', '321322', '321322', '沭阳县', 'Shuyang Xian', 'Shuyang Xian', ',11,121,1295,', '江苏省-宿迁市-沭阳县', '121', '02', '04', '01', '1295', '', null, null);
INSERT INTO t_sys_area VALUES ('1296', '321323', '321323', '321323', '泗阳县', 'Siyang Xian ', 'Siyang Xian ', ',11,121,1296,', '江苏省-宿迁市-泗阳县', '121', '02', '04', '01', '1296', '', null, null);
INSERT INTO t_sys_area VALUES ('1297', '321324', '321324', '321324', '泗洪县', 'Sihong Xian', 'Sihong Xian', ',11,121,1297,', '江苏省-宿迁市-泗洪县', '121', '02', '04', '01', '1297', '', null, null);
INSERT INTO t_sys_area VALUES ('1298', '330101', '330101', '330101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',12,122,1298,', '浙江省-杭州市-市辖区', '122', '02', '04', '01', '1298', '', null, null);
INSERT INTO t_sys_area VALUES ('1299', '330102', '330102', '330102', '上城区', 'Shangcheng Qu', 'Shangcheng Qu', ',12,122,1299,', '浙江省-杭州市-上城区', '122', '02', '04', '01', '1299', '', null, null);
INSERT INTO t_sys_area VALUES ('1300', '330103', '330103', '330103', '下城区', 'Xiacheng Qu', 'Xiacheng Qu', ',12,122,1300,', '浙江省-杭州市-下城区', '122', '02', '04', '01', '1300', '', null, null);
INSERT INTO t_sys_area VALUES ('1301', '330104', '330104', '330104', '江干区', 'Jianggan Qu', 'Jianggan Qu', ',12,122,1301,', '浙江省-杭州市-江干区', '122', '02', '04', '01', '1301', '', null, null);
INSERT INTO t_sys_area VALUES ('1302', '330105', '330105', '330105', '拱墅区', 'Gongshu Qu', 'Gongshu Qu', ',12,122,1302,', '浙江省-杭州市-拱墅区', '122', '02', '04', '01', '1302', '', null, null);
INSERT INTO t_sys_area VALUES ('1303', '330106', '330106', '330106', '西湖区', 'Xihu Qu ', 'Xihu Qu ', ',12,122,1303,', '浙江省-杭州市-西湖区', '122', '02', '04', '01', '1303', '', null, null);
INSERT INTO t_sys_area VALUES ('1304', '330108', '330108', '330108', '滨江区', 'Binjiang Qu', 'Binjiang Qu', ',12,122,1304,', '浙江省-杭州市-滨江区', '122', '02', '04', '01', '1304', '', null, null);
INSERT INTO t_sys_area VALUES ('1305', '330109', '330109', '330109', '萧山区', 'Xiaoshan Qu', 'Xiaoshan Qu', ',12,122,1305,', '浙江省-杭州市-萧山区', '122', '02', '04', '01', '1305', '', null, null);
INSERT INTO t_sys_area VALUES ('1306', '330110', '330110', '330110', '余杭区', 'Yuhang Qu', 'Yuhang Qu', ',12,122,1306,', '浙江省-杭州市-余杭区', '122', '02', '04', '01', '1306', '', null, null);
INSERT INTO t_sys_area VALUES ('1307', '330122', '330122', '330122', '桐庐县', 'Tonglu Xian', 'Tonglu Xian', ',12,122,1307,', '浙江省-杭州市-桐庐县', '122', '02', '04', '01', '1307', '', null, null);
INSERT INTO t_sys_area VALUES ('1308', '330127', '330127', '330127', '淳安县', 'Chun,an Xian', 'Chun,an Xian', ',12,122,1308,', '浙江省-杭州市-淳安县', '122', '02', '04', '01', '1308', '', null, null);
INSERT INTO t_sys_area VALUES ('1309', '330182', '330182', '330182', '建德市', 'Jiande Shi', 'Jiande Shi', ',12,122,1309,', '浙江省-杭州市-建德市', '122', '02', '04', '01', '1309', '', null, null);
INSERT INTO t_sys_area VALUES ('1310', '330183', '330183', '330183', '富阳市', 'Fuyang Shi', 'Fuyang Shi', ',12,122,1310,', '浙江省-杭州市-富阳市', '122', '02', '04', '01', '1310', '', null, null);
INSERT INTO t_sys_area VALUES ('1311', '330185', '330185', '330185', '临安市', 'Lin,an Shi', 'Lin,an Shi', ',12,122,1311,', '浙江省-杭州市-临安市', '122', '02', '04', '01', '1311', '', null, null);
INSERT INTO t_sys_area VALUES ('1312', '330201', '330201', '330201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',12,123,1312,', '浙江省-宁波市-市辖区', '123', '02', '04', '01', '1312', '', null, null);
INSERT INTO t_sys_area VALUES ('1313', '330203', '330203', '330203', '海曙区', 'Haishu Qu', 'Haishu Qu', ',12,123,1313,', '浙江省-宁波市-海曙区', '123', '02', '04', '01', '1313', '', null, null);
INSERT INTO t_sys_area VALUES ('1314', '330204', '330204', '330204', '江东区', 'Jiangdong Qu', 'Jiangdong Qu', ',12,123,1314,', '浙江省-宁波市-江东区', '123', '02', '04', '01', '1314', '', null, null);
INSERT INTO t_sys_area VALUES ('1315', '330205', '330205', '330205', '江北区', 'Jiangbei Qu', 'Jiangbei Qu', ',12,123,1315,', '浙江省-宁波市-江北区', '123', '02', '04', '01', '1315', '', null, null);
INSERT INTO t_sys_area VALUES ('1316', '330206', '330206', '330206', '北仑区', 'Beilun Qu', 'Beilun Qu', ',12,123,1316,', '浙江省-宁波市-北仑区', '123', '02', '04', '01', '1316', '', null, null);
INSERT INTO t_sys_area VALUES ('1317', '330211', '330211', '330211', '镇海区', 'Zhenhai Qu', 'Zhenhai Qu', ',12,123,1317,', '浙江省-宁波市-镇海区', '123', '02', '04', '01', '1317', '', null, null);
INSERT INTO t_sys_area VALUES ('1318', '330212', '330212', '330212', '鄞州区', 'Yinzhou Qu', 'Yinzhou Qu', ',12,123,1318,', '浙江省-宁波市-鄞州区', '123', '02', '04', '01', '1318', '', null, null);
INSERT INTO t_sys_area VALUES ('1319', '330225', '330225', '330225', '象山县', 'Xiangshan Xian', 'Xiangshan Xian', ',12,123,1319,', '浙江省-宁波市-象山县', '123', '02', '04', '01', '1319', '', null, null);
INSERT INTO t_sys_area VALUES ('1320', '330226', '330226', '330226', '宁海县', 'Ninghai Xian', 'Ninghai Xian', ',12,123,1320,', '浙江省-宁波市-宁海县', '123', '02', '04', '01', '1320', '', null, null);
INSERT INTO t_sys_area VALUES ('1321', '330281', '330281', '330281', '余姚市', 'Yuyao Shi', 'Yuyao Shi', ',12,123,1321,', '浙江省-宁波市-余姚市', '123', '02', '04', '01', '1321', '', null, null);
INSERT INTO t_sys_area VALUES ('1322', '330282', '330282', '330282', '慈溪市', 'Cixi Shi', 'Cixi Shi', ',12,123,1322,', '浙江省-宁波市-慈溪市', '123', '02', '04', '01', '1322', '', null, null);
INSERT INTO t_sys_area VALUES ('1323', '330283', '330283', '330283', '奉化市', 'Fenghua Shi', 'Fenghua Shi', ',12,123,1323,', '浙江省-宁波市-奉化市', '123', '02', '04', '01', '1323', '', null, null);
INSERT INTO t_sys_area VALUES ('1324', '330301', '330301', '330301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',12,124,1324,', '浙江省-温州市-市辖区', '124', '02', '04', '01', '1324', '', null, null);
INSERT INTO t_sys_area VALUES ('1325', '330302', '330302', '330302', '鹿城区', 'Lucheng Qu', 'Lucheng Qu', ',12,124,1325,', '浙江省-温州市-鹿城区', '124', '02', '04', '01', '1325', '', null, null);
INSERT INTO t_sys_area VALUES ('1326', '330303', '330303', '330303', '龙湾区', 'Longwan Qu', 'Longwan Qu', ',12,124,1326,', '浙江省-温州市-龙湾区', '124', '02', '04', '01', '1326', '', null, null);
INSERT INTO t_sys_area VALUES ('1327', '330304', '330304', '330304', '瓯海区', 'Ouhai Qu', 'Ouhai Qu', ',12,124,1327,', '浙江省-温州市-瓯海区', '124', '02', '04', '01', '1327', '', null, null);
INSERT INTO t_sys_area VALUES ('1328', '330322', '330322', '330322', '洞头县', 'Dongtou Xian', 'Dongtou Xian', ',12,124,1328,', '浙江省-温州市-洞头县', '124', '02', '04', '01', '1328', '', null, null);
INSERT INTO t_sys_area VALUES ('1329', '330324', '330324', '330324', '永嘉县', 'Yongjia Xian', 'Yongjia Xian', ',12,124,1329,', '浙江省-温州市-永嘉县', '124', '02', '04', '01', '1329', '', null, null);
INSERT INTO t_sys_area VALUES ('1330', '330326', '330326', '330326', '平阳县', 'Pingyang Xian', 'Pingyang Xian', ',12,124,1330,', '浙江省-温州市-平阳县', '124', '02', '04', '01', '1330', '', null, null);
INSERT INTO t_sys_area VALUES ('1331', '330327', '330327', '330327', '苍南县', 'Cangnan Xian', 'Cangnan Xian', ',12,124,1331,', '浙江省-温州市-苍南县', '124', '02', '04', '01', '1331', '', null, null);
INSERT INTO t_sys_area VALUES ('1332', '330328', '330328', '330328', '文成县', 'Wencheng Xian', 'Wencheng Xian', ',12,124,1332,', '浙江省-温州市-文成县', '124', '02', '04', '01', '1332', '', null, null);
INSERT INTO t_sys_area VALUES ('1333', '330329', '330329', '330329', '泰顺县', 'Taishun Xian', 'Taishun Xian', ',12,124,1333,', '浙江省-温州市-泰顺县', '124', '02', '04', '01', '1333', '', null, null);
INSERT INTO t_sys_area VALUES ('1334', '330381', '330381', '330381', '瑞安市', 'Rui,an Xian', 'Rui,an Xian', ',12,124,1334,', '浙江省-温州市-瑞安市', '124', '02', '04', '01', '1334', '', null, null);
INSERT INTO t_sys_area VALUES ('1335', '330382', '330382', '330382', '乐清市', 'Yueqing Shi', 'Yueqing Shi', ',12,124,1335,', '浙江省-温州市-乐清市', '124', '02', '04', '01', '1335', '', null, null);
INSERT INTO t_sys_area VALUES ('1336', '330401', '330401', '330401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',12,125,1336,', '浙江省-嘉兴市-市辖区', '125', '02', '04', '01', '1336', '', null, null);
INSERT INTO t_sys_area VALUES ('1338', '330411', '330411', '330411', '秀洲区', 'Xiuzhou Qu', 'Xiuzhou Qu', ',12,125,1338,', '浙江省-嘉兴市-秀洲区', '125', '02', '04', '01', '1338', '', null, null);
INSERT INTO t_sys_area VALUES ('1339', '330421', '330421', '330421', '嘉善县', 'Jiashan Xian', 'Jiashan Xian', ',12,125,1339,', '浙江省-嘉兴市-嘉善县', '125', '02', '04', '01', '1339', '', null, null);
INSERT INTO t_sys_area VALUES ('1340', '330424', '330424', '330424', '海盐县', 'Haiyan Xian', 'Haiyan Xian', ',12,125,1340,', '浙江省-嘉兴市-海盐县', '125', '02', '04', '01', '1340', '', null, null);
INSERT INTO t_sys_area VALUES ('1341', '330481', '330481', '330481', '海宁市', 'Haining Shi', 'Haining Shi', ',12,125,1341,', '浙江省-嘉兴市-海宁市', '125', '02', '04', '01', '1341', '', null, null);
INSERT INTO t_sys_area VALUES ('1342', '330482', '330482', '330482', '平湖市', 'Pinghu Shi', 'Pinghu Shi', ',12,125,1342,', '浙江省-嘉兴市-平湖市', '125', '02', '04', '01', '1342', '', null, null);
INSERT INTO t_sys_area VALUES ('1343', '330483', '330483', '330483', '桐乡市', 'Tongxiang Shi', 'Tongxiang Shi', ',12,125,1343,', '浙江省-嘉兴市-桐乡市', '125', '02', '04', '01', '1343', '', null, null);
INSERT INTO t_sys_area VALUES ('1344', '330501', '330501', '330501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',12,126,1344,', '浙江省-湖州市-市辖区', '126', '02', '04', '01', '1344', '', null, null);
INSERT INTO t_sys_area VALUES ('1345', '330502', '330502', '330502', '吴兴区', 'Wuxing Qu', 'Wuxing Qu', ',12,126,1345,', '浙江省-湖州市-吴兴区', '126', '02', '04', '01', '1345', '', null, null);
INSERT INTO t_sys_area VALUES ('1346', '330503', '330503', '330503', '南浔区', 'Nanxun Qu', 'Nanxun Qu', ',12,126,1346,', '浙江省-湖州市-南浔区', '126', '02', '04', '01', '1346', '', null, null);
INSERT INTO t_sys_area VALUES ('1347', '330521', '330521', '330521', '德清县', 'Deqing Xian', 'Deqing Xian', ',12,126,1347,', '浙江省-湖州市-德清县', '126', '02', '04', '01', '1347', '', null, null);
INSERT INTO t_sys_area VALUES ('1348', '330522', '330522', '330522', '长兴县', 'Changxing Xian', 'Changxing Xian', ',12,126,1348,', '浙江省-湖州市-长兴县', '126', '02', '04', '01', '1348', '', null, null);
INSERT INTO t_sys_area VALUES ('1349', '330523', '330523', '330523', '安吉县', 'Anji Xian', 'Anji Xian', ',12,126,1349,', '浙江省-湖州市-安吉县', '126', '02', '04', '01', '1349', '', null, null);
INSERT INTO t_sys_area VALUES ('1350', '330601', '330601', '330601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',12,127,1350,', '浙江省-绍兴市-市辖区', '127', '02', '04', '01', '1350', '', null, null);
INSERT INTO t_sys_area VALUES ('1351', '330602', '330602', '330602', '越城区', 'Yuecheng Qu', 'Yuecheng Qu', ',12,127,1351,', '浙江省-绍兴市-越城区', '127', '02', '04', '01', '1351', '', null, null);
INSERT INTO t_sys_area VALUES ('1352', '330621', '330621', '330621', '绍兴县', 'Shaoxing Xian', 'Shaoxing Xian', ',12,127,1352,', '浙江省-绍兴市-绍兴县', '127', '02', '04', '01', '1352', '', null, null);
INSERT INTO t_sys_area VALUES ('1353', '330624', '330624', '330624', '新昌县', 'Xinchang Xian', 'Xinchang Xian', ',12,127,1353,', '浙江省-绍兴市-新昌县', '127', '02', '04', '01', '1353', '', null, null);
INSERT INTO t_sys_area VALUES ('1354', '330681', '330681', '330681', '诸暨市', 'Zhuji Shi', 'Zhuji Shi', ',12,127,1354,', '浙江省-绍兴市-诸暨市', '127', '02', '04', '01', '1354', '', null, null);
INSERT INTO t_sys_area VALUES ('1355', '330682', '330682', '330682', '上虞市', 'Shangyu Shi', 'Shangyu Shi', ',12,127,1355,', '浙江省-绍兴市-上虞市', '127', '02', '04', '01', '1355', '', null, null);
INSERT INTO t_sys_area VALUES ('1356', '330683', '330683', '330683', '嵊州市', 'Shengzhou Shi', 'Shengzhou Shi', ',12,127,1356,', '浙江省-绍兴市-嵊州市', '127', '02', '04', '01', '1356', '', null, null);
INSERT INTO t_sys_area VALUES ('1357', '330701', '330701', '330701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',12,128,1357,', '浙江省-金华市-市辖区', '128', '02', '04', '01', '1357', '', null, null);
INSERT INTO t_sys_area VALUES ('1358', '330702', '330702', '330702', '婺城区', 'Wucheng Qu', 'Wucheng Qu', ',12,128,1358,', '浙江省-金华市-婺城区', '128', '02', '04', '01', '1358', '', null, null);
INSERT INTO t_sys_area VALUES ('1359', '330703', '330703', '330703', '金东区', 'Jindong Qu', 'Jindong Qu', ',12,128,1359,', '浙江省-金华市-金东区', '128', '02', '04', '01', '1359', '', null, null);
INSERT INTO t_sys_area VALUES ('1360', '330723', '330723', '330723', '武义县', 'Wuyi Xian', 'Wuyi Xian', ',12,128,1360,', '浙江省-金华市-武义县', '128', '02', '04', '01', '1360', '', null, null);
INSERT INTO t_sys_area VALUES ('1361', '330726', '330726', '330726', '浦江县', 'Pujiang Xian ', 'Pujiang Xian ', ',12,128,1361,', '浙江省-金华市-浦江县', '128', '02', '04', '01', '1361', '', null, null);
INSERT INTO t_sys_area VALUES ('1362', '330727', '330727', '330727', '磐安县', 'Pan,an Xian', 'Pan,an Xian', ',12,128,1362,', '浙江省-金华市-磐安县', '128', '02', '04', '01', '1362', '', null, null);
INSERT INTO t_sys_area VALUES ('1363', '330781', '330781', '330781', '兰溪市', 'Lanxi Shi', 'Lanxi Shi', ',12,128,1363,', '浙江省-金华市-兰溪市', '128', '02', '04', '01', '1363', '', null, null);
INSERT INTO t_sys_area VALUES ('1364', '330782', '330782', '330782', '义乌市', 'Yiwu Shi', 'Yiwu Shi', ',12,128,1364,', '浙江省-金华市-义乌市', '128', '02', '04', '01', '1364', '', null, null);
INSERT INTO t_sys_area VALUES ('1365', '330783', '330783', '330783', '东阳市', 'Dongyang Shi', 'Dongyang Shi', ',12,128,1365,', '浙江省-金华市-东阳市', '128', '02', '04', '01', '1365', '', null, null);
INSERT INTO t_sys_area VALUES ('1366', '330784', '330784', '330784', '永康市', 'Yongkang Shi', 'Yongkang Shi', ',12,128,1366,', '浙江省-金华市-永康市', '128', '02', '04', '01', '1366', '', null, null);
INSERT INTO t_sys_area VALUES ('1367', '330801', '330801', '330801', '市辖区', 'Shixiaqu', 'Shixiaqu', ',12,129,1367,', '浙江省-衢州市-市辖区', '129', '02', '04', '01', '1367', '', null, null);
INSERT INTO t_sys_area VALUES ('1368', '330802', '330802', '330802', '柯城区', 'Kecheng Qu', 'Kecheng Qu', ',12,129,1368,', '浙江省-衢州市-柯城区', '129', '02', '04', '01', '1368', '', null, null);
INSERT INTO t_sys_area VALUES ('1369', '330803', '330803', '330803', '衢江区', 'Qujiang Qu', 'Qujiang Qu', ',12,129,1369,', '浙江省-衢州市-衢江区', '129', '02', '04', '01', '1369', '', null, null);
INSERT INTO t_sys_area VALUES ('1370', '330822', '330822', '330822', '常山县', 'Changshan Xian', 'Changshan Xian', ',12,129,1370,', '浙江省-衢州市-常山县', '129', '02', '04', '01', '1370', '', null, null);
INSERT INTO t_sys_area VALUES ('1371', '330824', '330824', '330824', '开化县', 'Kaihua Xian', 'Kaihua Xian', ',12,129,1371,', '浙江省-衢州市-开化县', '129', '02', '04', '01', '1371', '', null, null);
INSERT INTO t_sys_area VALUES ('1372', '330825', '330825', '330825', '龙游县', 'Longyou Xian ', 'Longyou Xian ', ',12,129,1372,', '浙江省-衢州市-龙游县', '129', '02', '04', '01', '1372', '', null, null);
INSERT INTO t_sys_area VALUES ('1373', '330881', '330881', '330881', '江山市', 'Jiangshan Shi', 'Jiangshan Shi', ',12,129,1373,', '浙江省-衢州市-江山市', '129', '02', '04', '01', '1373', '', null, null);
INSERT INTO t_sys_area VALUES ('1374', '330901', '330901', '330901', '市辖区', 'Shixiaqu', 'Shixiaqu', ',12,130,1374,', '浙江省-舟山市-市辖区', '130', '02', '04', '01', '1374', '', null, null);
INSERT INTO t_sys_area VALUES ('1375', '330902', '330902', '330902', '定海区', 'Dinghai Qu', 'Dinghai Qu', ',12,130,1375,', '浙江省-舟山市-定海区', '130', '02', '04', '01', '1375', '', null, null);
INSERT INTO t_sys_area VALUES ('1376', '330903', '330903', '330903', '普陀区', 'Putuo Qu', 'Putuo Qu', ',12,130,1376,', '浙江省-舟山市-普陀区', '130', '02', '04', '01', '1376', '', null, null);
INSERT INTO t_sys_area VALUES ('1377', '330921', '330921', '330921', '岱山县', 'Daishan Xian', 'Daishan Xian', ',12,130,1377,', '浙江省-舟山市-岱山县', '130', '02', '04', '01', '1377', '', null, null);
INSERT INTO t_sys_area VALUES ('1378', '330922', '330922', '330922', '嵊泗县', 'Shengsi Xian', 'Shengsi Xian', ',12,130,1378,', '浙江省-舟山市-嵊泗县', '130', '02', '04', '01', '1378', '', null, null);
INSERT INTO t_sys_area VALUES ('1379', '331001', '331001', '331001', '市辖区', 'Shixiaqu', 'Shixiaqu', ',12,131,1379,', '浙江省-台州市-市辖区', '131', '02', '04', '01', '1379', '', null, null);
INSERT INTO t_sys_area VALUES ('1380', '331002', '331002', '331002', '椒江区', 'Jiaojiang Qu', 'Jiaojiang Qu', ',12,131,1380,', '浙江省-台州市-椒江区', '131', '02', '04', '01', '1380', '', null, null);
INSERT INTO t_sys_area VALUES ('1381', '331003', '331003', '331003', '黄岩区', 'Huangyan Qu', 'Huangyan Qu', ',12,131,1381,', '浙江省-台州市-黄岩区', '131', '02', '04', '01', '1381', '', null, null);
INSERT INTO t_sys_area VALUES ('1382', '331004', '331004', '331004', '路桥区', 'Luqiao Qu', 'Luqiao Qu', ',12,131,1382,', '浙江省-台州市-路桥区', '131', '02', '04', '01', '1382', '', null, null);
INSERT INTO t_sys_area VALUES ('1383', '331021', '331021', '331021', '玉环县', 'Yuhuan Xian', 'Yuhuan Xian', ',12,131,1383,', '浙江省-台州市-玉环县', '131', '02', '04', '01', '1383', '', null, null);
INSERT INTO t_sys_area VALUES ('1384', '331022', '331022', '331022', '三门县', 'Sanmen Xian', 'Sanmen Xian', ',12,131,1384,', '浙江省-台州市-三门县', '131', '02', '04', '01', '1384', '', null, null);
INSERT INTO t_sys_area VALUES ('1385', '331023', '331023', '331023', '天台县', 'Tiantai Xian', 'Tiantai Xian', ',12,131,1385,', '浙江省-台州市-天台县', '131', '02', '04', '01', '1385', '', null, null);
INSERT INTO t_sys_area VALUES ('1386', '331024', '331024', '331024', '仙居县', 'Xianju Xian', 'Xianju Xian', ',12,131,1386,', '浙江省-台州市-仙居县', '131', '02', '04', '01', '1386', '', null, null);
INSERT INTO t_sys_area VALUES ('1387', '331081', '331081', '331081', '温岭市', 'Wenling Shi', 'Wenling Shi', ',12,131,1387,', '浙江省-台州市-温岭市', '131', '02', '04', '01', '1387', '', null, null);
INSERT INTO t_sys_area VALUES ('1388', '331082', '331082', '331082', '临海市', 'Linhai Shi', 'Linhai Shi', ',12,131,1388,', '浙江省-台州市-临海市', '131', '02', '04', '01', '1388', '', null, null);
INSERT INTO t_sys_area VALUES ('1389', '331101', '331101', '331101', '市辖区', '1', '1', ',12,132,1389,', '浙江省-丽水市-市辖区', '132', '02', '04', '01', '1389', '', null, null);
INSERT INTO t_sys_area VALUES ('1390', '331102', '331102', '331102', '莲都区', 'Liandu Qu', 'Liandu Qu', ',12,132,1390,', '浙江省-丽水市-莲都区', '132', '02', '04', '01', '1390', '', null, null);
INSERT INTO t_sys_area VALUES ('1391', '331121', '331121', '331121', '青田县', 'Qingtian Xian', 'Qingtian Xian', ',12,132,1391,', '浙江省-丽水市-青田县', '132', '02', '04', '01', '1391', '', null, null);
INSERT INTO t_sys_area VALUES ('1392', '331122', '331122', '331122', '缙云县', 'Jinyun Xian', 'Jinyun Xian', ',12,132,1392,', '浙江省-丽水市-缙云县', '132', '02', '04', '01', '1392', '', null, null);
INSERT INTO t_sys_area VALUES ('1393', '331123', '331123', '331123', '遂昌县', 'Suichang Xian', 'Suichang Xian', ',12,132,1393,', '浙江省-丽水市-遂昌县', '132', '02', '04', '01', '1393', '', null, null);
INSERT INTO t_sys_area VALUES ('1394', '331124', '331124', '331124', '松阳县', 'Songyang Xian', 'Songyang Xian', ',12,132,1394,', '浙江省-丽水市-松阳县', '132', '02', '04', '01', '1394', '', null, null);
INSERT INTO t_sys_area VALUES ('1395', '331125', '331125', '331125', '云和县', 'Yunhe Xian', 'Yunhe Xian', ',12,132,1395,', '浙江省-丽水市-云和县', '132', '02', '04', '01', '1395', '', null, null);
INSERT INTO t_sys_area VALUES ('1396', '331126', '331126', '331126', '庆元县', 'Qingyuan Xian', 'Qingyuan Xian', ',12,132,1396,', '浙江省-丽水市-庆元县', '132', '02', '04', '01', '1396', '', null, null);
INSERT INTO t_sys_area VALUES ('1397', '331127', '331127', '331127', '景宁畲族自治县', 'Jingning Shezu Zizhixian', 'Jingning Shezu Zizhixian', ',12,132,1397,', '浙江省-丽水市-景宁畲族自治县', '132', '02', '04', '01', '1397', '', null, null);
INSERT INTO t_sys_area VALUES ('1398', '331181', '331181', '331181', '龙泉市', 'Longquan Shi', 'Longquan Shi', ',12,132,1398,', '浙江省-丽水市-龙泉市', '132', '02', '04', '01', '1398', '', null, null);
INSERT INTO t_sys_area VALUES ('1399', '340101', '340101', '340101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',13,133,1399,', '安徽省-合肥市-市辖区', '133', '02', '04', '01', '1399', '', null, null);
INSERT INTO t_sys_area VALUES ('1400', '340102', '340102', '340102', '瑶海区', 'Yaohai Qu', 'Yaohai Qu', ',13,133,1400,', '安徽省-合肥市-瑶海区', '133', '02', '04', '01', '1400', '', null, null);
INSERT INTO t_sys_area VALUES ('1401', '340103', '340103', '340103', '庐阳区', 'Luyang Qu', 'Luyang Qu', ',13,133,1401,', '安徽省-合肥市-庐阳区', '133', '02', '04', '01', '1401', '', null, null);
INSERT INTO t_sys_area VALUES ('1402', '340104', '340104', '340104', '蜀山区', 'Shushan Qu', 'Shushan Qu', ',13,133,1402,', '安徽省-合肥市-蜀山区', '133', '02', '04', '01', '1402', '', null, null);
INSERT INTO t_sys_area VALUES ('1403', '340111', '340111', '340111', '包河区', 'Baohe Qu', 'Baohe Qu', ',13,133,1403,', '安徽省-合肥市-包河区', '133', '02', '04', '01', '1403', '', null, null);
INSERT INTO t_sys_area VALUES ('1404', '340121', '340121', '340121', '长丰县', 'Changfeng Xian', 'Changfeng Xian', ',13,133,1404,', '安徽省-合肥市-长丰县', '133', '02', '04', '01', '1404', '', null, null);
INSERT INTO t_sys_area VALUES ('1405', '340122', '340122', '340122', '肥东县', 'Feidong Xian', 'Feidong Xian', ',13,133,1405,', '安徽省-合肥市-肥东县', '133', '02', '04', '01', '1405', '', null, null);
INSERT INTO t_sys_area VALUES ('1406', '340123', '340123', '340123', '肥西县', 'Feixi Xian', 'Feixi Xian', ',13,133,1406,', '安徽省-合肥市-肥西县', '133', '02', '04', '01', '1406', '', null, null);
INSERT INTO t_sys_area VALUES ('1407', '340201', '340201', '340201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',13,134,1407,', '安徽省-芜湖市-市辖区', '134', '02', '04', '01', '1407', '', null, null);
INSERT INTO t_sys_area VALUES ('1408', '340202', '340202', '340202', '镜湖区', 'Jinghu Qu', 'Jinghu Qu', ',13,134,1408,', '安徽省-芜湖市-镜湖区', '134', '02', '04', '01', '1408', '', null, null);
INSERT INTO t_sys_area VALUES ('1409', '340208', '340208', '340208', '三山区', 'Matang Qu', 'Matang Qu', ',13,134,1409,', '安徽省-芜湖市-三山区', '134', '02', '04', '01', '1409', '', null, null);
INSERT INTO t_sys_area VALUES ('1410', '340203', '340203', '340203', '弋江区', 'Xinwu Qu', 'Xinwu Qu', ',13,134,1410,', '安徽省-芜湖市-弋江区', '134', '02', '04', '01', '1410', '', null, null);
INSERT INTO t_sys_area VALUES ('1411', '340207', '340207', '340207', '鸠江区', 'Jiujiang Qu', 'Jiujiang Qu', ',13,134,1411,', '安徽省-芜湖市-鸠江区', '134', '02', '04', '01', '1411', '', null, null);
INSERT INTO t_sys_area VALUES ('1413', '340222', '340222', '340222', '繁昌县', 'Fanchang Xian', 'Fanchang Xian', ',13,134,1413,', '安徽省-芜湖市-繁昌县', '134', '02', '04', '01', '1413', '', null, null);
INSERT INTO t_sys_area VALUES ('1414', '340223', '340223', '340223', '南陵县', 'Nanling Xian', 'Nanling Xian', ',13,134,1414,', '安徽省-芜湖市-南陵县', '134', '02', '04', '01', '1414', '', null, null);
INSERT INTO t_sys_area VALUES ('1415', '340301', '340301', '340301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',13,135,1415,', '安徽省-蚌埠市-市辖区', '135', '02', '04', '01', '1415', '', null, null);
INSERT INTO t_sys_area VALUES ('1416', '340302', '340302', '340302', '龙子湖区', 'Longzihu Qu', 'Longzihu Qu', ',13,135,1416,', '安徽省-蚌埠市-龙子湖区', '135', '02', '04', '01', '1416', '', null, null);
INSERT INTO t_sys_area VALUES ('1417', '340303', '340303', '340303', '蚌山区', 'Bangshan Qu', 'Bangshan Qu', ',13,135,1417,', '安徽省-蚌埠市-蚌山区', '135', '02', '04', '01', '1417', '', null, null);
INSERT INTO t_sys_area VALUES ('1418', '340304', '340304', '340304', '禹会区', 'Yuhui Qu', 'Yuhui Qu', ',13,135,1418,', '安徽省-蚌埠市-禹会区', '135', '02', '04', '01', '1418', '', null, null);
INSERT INTO t_sys_area VALUES ('1419', '340311', '340311', '340311', '淮上区', 'Huaishang Qu', 'Huaishang Qu', ',13,135,1419,', '安徽省-蚌埠市-淮上区', '135', '02', '04', '01', '1419', '', null, null);
INSERT INTO t_sys_area VALUES ('1420', '340321', '340321', '340321', '怀远县', 'Huaiyuan Qu', 'Huaiyuan Qu', ',13,135,1420,', '安徽省-蚌埠市-怀远县', '135', '02', '04', '01', '1420', '', null, null);
INSERT INTO t_sys_area VALUES ('1421', '340322', '340322', '340322', '五河县', 'Wuhe Xian', 'Wuhe Xian', ',13,135,1421,', '安徽省-蚌埠市-五河县', '135', '02', '04', '01', '1421', '', null, null);
INSERT INTO t_sys_area VALUES ('1422', '340323', '340323', '340323', '固镇县', 'Guzhen Xian', 'Guzhen Xian', ',13,135,1422,', '安徽省-蚌埠市-固镇县', '135', '02', '04', '01', '1422', '', null, null);
INSERT INTO t_sys_area VALUES ('1423', '340401', '340401', '340401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',13,136,1423,', '安徽省-淮南市-市辖区', '136', '02', '04', '01', '1423', '', null, null);
INSERT INTO t_sys_area VALUES ('1424', '340402', '340402', '340402', '大通区', 'Datong Qu', 'Datong Qu', ',13,136,1424,', '安徽省-淮南市-大通区', '136', '02', '04', '01', '1424', '', null, null);
INSERT INTO t_sys_area VALUES ('1425', '340403', '340403', '340403', '田家庵区', 'Tianjia,an Qu', 'Tianjia,an Qu', ',13,136,1425,', '安徽省-淮南市-田家庵区', '136', '02', '04', '01', '1425', '', null, null);
INSERT INTO t_sys_area VALUES ('1426', '340404', '340404', '340404', '谢家集区', 'Xiejiaji Qu', 'Xiejiaji Qu', ',13,136,1426,', '安徽省-淮南市-谢家集区', '136', '02', '04', '01', '1426', '', null, null);
INSERT INTO t_sys_area VALUES ('1427', '340405', '340405', '340405', '八公山区', 'Bagongshan Qu', 'Bagongshan Qu', ',13,136,1427,', '安徽省-淮南市-八公山区', '136', '02', '04', '01', '1427', '', null, null);
INSERT INTO t_sys_area VALUES ('1428', '340406', '340406', '340406', '潘集区', 'Panji Qu', 'Panji Qu', ',13,136,1428,', '安徽省-淮南市-潘集区', '136', '02', '04', '01', '1428', '', null, null);
INSERT INTO t_sys_area VALUES ('1429', '340421', '340421', '340421', '凤台县', 'Fengtai Xian', 'Fengtai Xian', ',13,136,1429,', '安徽省-淮南市-凤台县', '136', '02', '04', '01', '1429', '', null, null);
INSERT INTO t_sys_area VALUES ('1430', '340501', '340501', '340501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',13,137,1430,', '安徽省-马鞍山市-市辖区', '137', '02', '04', '01', '1430', '', null, null);
INSERT INTO t_sys_area VALUES ('1431', '340502', '340502', '340502', '金家庄区', 'Jinjiazhuang Qu', 'Jinjiazhuang Qu', ',13,137,1431,', '安徽省-马鞍山市-金家庄区', '137', '02', '04', '01', '1431', '', null, null);
INSERT INTO t_sys_area VALUES ('1432', '340503', '340503', '340503', '花山区', 'Huashan Qu', 'Huashan Qu', ',13,137,1432,', '安徽省-马鞍山市-花山区', '137', '02', '04', '01', '1432', '', null, null);
INSERT INTO t_sys_area VALUES ('1433', '340504', '340504', '340504', '雨山区', 'Yushan Qu', 'Yushan Qu', ',13,137,1433,', '安徽省-马鞍山市-雨山区', '137', '02', '04', '01', '1433', '', null, null);
INSERT INTO t_sys_area VALUES ('1434', '340521', '340521', '340521', '当涂县', 'Dangtu Xian', 'Dangtu Xian', ',13,137,1434,', '安徽省-马鞍山市-当涂县', '137', '02', '04', '01', '1434', '', null, null);
INSERT INTO t_sys_area VALUES ('1435', '340601', '340601', '340601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',13,138,1435,', '安徽省-淮北市-市辖区', '138', '02', '04', '01', '1435', '', null, null);
INSERT INTO t_sys_area VALUES ('1436', '340602', '340602', '340602', '杜集区', 'Duji Qu', 'Duji Qu', ',13,138,1436,', '安徽省-淮北市-杜集区', '138', '02', '04', '01', '1436', '', null, null);
INSERT INTO t_sys_area VALUES ('1437', '340603', '340603', '340603', '相山区', 'Xiangshan Qu', 'Xiangshan Qu', ',13,138,1437,', '安徽省-淮北市-相山区', '138', '02', '04', '01', '1437', '', null, null);
INSERT INTO t_sys_area VALUES ('1438', '340604', '340604', '340604', '烈山区', 'Lieshan Qu', 'Lieshan Qu', ',13,138,1438,', '安徽省-淮北市-烈山区', '138', '02', '04', '01', '1438', '', null, null);
INSERT INTO t_sys_area VALUES ('1439', '340621', '340621', '340621', '濉溪县', 'Suixi Xian', 'Suixi Xian', ',13,138,1439,', '安徽省-淮北市-濉溪县', '138', '02', '04', '01', '1439', '', null, null);
INSERT INTO t_sys_area VALUES ('1440', '340701', '340701', '340701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',13,139,1440,', '安徽省-铜陵市-市辖区', '139', '02', '04', '01', '1440', '', null, null);
INSERT INTO t_sys_area VALUES ('1441', '340702', '340702', '340702', '铜官山区', 'Tongguanshan Qu', 'Tongguanshan Qu', ',13,139,1441,', '安徽省-铜陵市-铜官山区', '139', '02', '04', '01', '1441', '', null, null);
INSERT INTO t_sys_area VALUES ('1442', '340703', '340703', '340703', '狮子山区', 'Shizishan Qu', 'Shizishan Qu', ',13,139,1442,', '安徽省-铜陵市-狮子山区', '139', '02', '04', '01', '1442', '', null, null);
INSERT INTO t_sys_area VALUES ('1443', '340711', '340711', '340711', '郊区', 'Jiaoqu', 'Jiaoqu', ',13,139,1443,', '安徽省-铜陵市-郊区', '139', '02', '04', '01', '1443', '', null, null);
INSERT INTO t_sys_area VALUES ('1444', '340721', '340721', '340721', '铜陵县', 'Tongling Xian', 'Tongling Xian', ',13,139,1444,', '安徽省-铜陵市-铜陵县', '139', '02', '04', '01', '1444', '', null, null);
INSERT INTO t_sys_area VALUES ('1445', '340801', '340801', '340801', '市辖区', 'Shixiaqu', 'Shixiaqu', ',13,140,1445,', '安徽省-安庆市-市辖区', '140', '02', '04', '01', '1445', '', null, null);
INSERT INTO t_sys_area VALUES ('1446', '340802', '340802', '340802', '迎江区', 'Yingjiang Qu', 'Yingjiang Qu', ',13,140,1446,', '安徽省-安庆市-迎江区', '140', '02', '04', '01', '1446', '', null, null);
INSERT INTO t_sys_area VALUES ('1447', '340803', '340803', '340803', '大观区', 'Daguan Qu', 'Daguan Qu', ',13,140,1447,', '安徽省-安庆市-大观区', '140', '02', '04', '01', '1447', '', null, null);
INSERT INTO t_sys_area VALUES ('1448', '340811', '340811', '340811', '宜秀区', 'Yixiu Qu', 'Yixiu Qu', ',13,140,1448,', '安徽省-安庆市-宜秀区', '140', '02', '04', '01', '1448', '', null, null);
INSERT INTO t_sys_area VALUES ('1449', '340822', '340822', '340822', '怀宁县', 'Huaining Xian', 'Huaining Xian', ',13,140,1449,', '安徽省-安庆市-怀宁县', '140', '02', '04', '01', '1449', '', null, null);
INSERT INTO t_sys_area VALUES ('1450', '340823', '340823', '340823', '枞阳县', 'Zongyang Xian', 'Zongyang Xian', ',13,140,1450,', '安徽省-安庆市-枞阳县', '140', '02', '04', '01', '1450', '', null, null);
INSERT INTO t_sys_area VALUES ('1451', '340824', '340824', '340824', '潜山县', 'Qianshan Xian', 'Qianshan Xian', ',13,140,1451,', '安徽省-安庆市-潜山县', '140', '02', '04', '01', '1451', '', null, null);
INSERT INTO t_sys_area VALUES ('1452', '340825', '340825', '340825', '太湖县', 'Taihu Xian', 'Taihu Xian', ',13,140,1452,', '安徽省-安庆市-太湖县', '140', '02', '04', '01', '1452', '', null, null);
INSERT INTO t_sys_area VALUES ('1453', '340826', '340826', '340826', '宿松县', 'Susong Xian', 'Susong Xian', ',13,140,1453,', '安徽省-安庆市-宿松县', '140', '02', '04', '01', '1453', '', null, null);
INSERT INTO t_sys_area VALUES ('1454', '340827', '340827', '340827', '望江县', 'Wangjiang Xian', 'Wangjiang Xian', ',13,140,1454,', '安徽省-安庆市-望江县', '140', '02', '04', '01', '1454', '', null, null);
INSERT INTO t_sys_area VALUES ('1455', '340828', '340828', '340828', '岳西县', 'Yuexi Xian', 'Yuexi Xian', ',13,140,1455,', '安徽省-安庆市-岳西县', '140', '02', '04', '01', '1455', '', null, null);
INSERT INTO t_sys_area VALUES ('1456', '340881', '340881', '340881', '桐城市', 'Tongcheng Shi', 'Tongcheng Shi', ',13,140,1456,', '安徽省-安庆市-桐城市', '140', '02', '04', '01', '1456', '', null, null);
INSERT INTO t_sys_area VALUES ('1457', '341001', '341001', '341001', '市辖区', 'Shixiaqu', 'Shixiaqu', ',13,141,1457,', '安徽省-黄山市-市辖区', '141', '02', '04', '01', '1457', '', null, null);
INSERT INTO t_sys_area VALUES ('1458', '341002', '341002', '341002', '屯溪区', 'Tunxi Qu', 'Tunxi Qu', ',13,141,1458,', '安徽省-黄山市-屯溪区', '141', '02', '04', '01', '1458', '', null, null);
INSERT INTO t_sys_area VALUES ('1459', '341003', '341003', '341003', '黄山区', 'Huangshan Qu', 'Huangshan Qu', ',13,141,1459,', '安徽省-黄山市-黄山区', '141', '02', '04', '01', '1459', '', null, null);
INSERT INTO t_sys_area VALUES ('1460', '341004', '341004', '341004', '徽州区', 'Huizhou Qu', 'Huizhou Qu', ',13,141,1460,', '安徽省-黄山市-徽州区', '141', '02', '04', '01', '1460', '', null, null);
INSERT INTO t_sys_area VALUES ('1461', '341021', '341021', '341021', '歙县', 'She Xian', 'She Xian', ',13,141,1461,', '安徽省-黄山市-歙县', '141', '02', '04', '01', '1461', '', null, null);
INSERT INTO t_sys_area VALUES ('1462', '341022', '341022', '341022', '休宁县', 'Xiuning Xian', 'Xiuning Xian', ',13,141,1462,', '安徽省-黄山市-休宁县', '141', '02', '04', '01', '1462', '', null, null);
INSERT INTO t_sys_area VALUES ('1463', '341023', '341023', '341023', '黟县', 'Yi Xian', 'Yi Xian', ',13,141,1463,', '安徽省-黄山市-黟县', '141', '02', '04', '01', '1463', '', null, null);
INSERT INTO t_sys_area VALUES ('1464', '341024', '341024', '341024', '祁门县', 'Qimen Xian', 'Qimen Xian', ',13,141,1464,', '安徽省-黄山市-祁门县', '141', '02', '04', '01', '1464', '', null, null);
INSERT INTO t_sys_area VALUES ('1465', '341101', '341101', '341101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',13,142,1465,', '安徽省-滁州市-市辖区', '142', '02', '04', '01', '1465', '', null, null);
INSERT INTO t_sys_area VALUES ('1466', '341102', '341102', '341102', '琅琊区', 'Langya Qu', 'Langya Qu', ',13,142,1466,', '安徽省-滁州市-琅琊区', '142', '02', '04', '01', '1466', '', null, null);
INSERT INTO t_sys_area VALUES ('1467', '341103', '341103', '341103', '南谯区', 'Nanqiao Qu', 'Nanqiao Qu', ',13,142,1467,', '安徽省-滁州市-南谯区', '142', '02', '04', '01', '1467', '', null, null);
INSERT INTO t_sys_area VALUES ('1468', '341122', '341122', '341122', '来安县', 'Lai,an Xian', 'Lai,an Xian', ',13,142,1468,', '安徽省-滁州市-来安县', '142', '02', '04', '01', '1468', '', null, null);
INSERT INTO t_sys_area VALUES ('1469', '341124', '341124', '341124', '全椒县', 'Quanjiao Xian', 'Quanjiao Xian', ',13,142,1469,', '安徽省-滁州市-全椒县', '142', '02', '04', '01', '1469', '', null, null);
INSERT INTO t_sys_area VALUES ('1470', '341125', '341125', '341125', '定远县', 'Dingyuan Xian', 'Dingyuan Xian', ',13,142,1470,', '安徽省-滁州市-定远县', '142', '02', '04', '01', '1470', '', null, null);
INSERT INTO t_sys_area VALUES ('1471', '341126', '341126', '341126', '凤阳县', 'Fengyang Xian', 'Fengyang Xian', ',13,142,1471,', '安徽省-滁州市-凤阳县', '142', '02', '04', '01', '1471', '', null, null);
INSERT INTO t_sys_area VALUES ('1472', '341181', '341181', '341181', '天长市', 'Tianchang Shi', 'Tianchang Shi', ',13,142,1472,', '安徽省-滁州市-天长市', '142', '02', '04', '01', '1472', '', null, null);
INSERT INTO t_sys_area VALUES ('1473', '341182', '341182', '341182', '明光市', 'Mingguang Shi', 'Mingguang Shi', ',13,142,1473,', '安徽省-滁州市-明光市', '142', '02', '04', '01', '1473', '', null, null);
INSERT INTO t_sys_area VALUES ('1474', '341201', '341201', '341201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',13,143,1474,', '安徽省-阜阳市-市辖区', '143', '02', '04', '01', '1474', '', null, null);
INSERT INTO t_sys_area VALUES ('1475', '341202', '341202', '341202', '颍州区', 'Yingzhou Qu', 'Yingzhou Qu', ',13,143,1475,', '安徽省-阜阳市-颍州区', '143', '02', '04', '01', '1475', '', null, null);
INSERT INTO t_sys_area VALUES ('1476', '341203', '341203', '341203', '颍东区', 'Yingdong Qu', 'Yingdong Qu', ',13,143,1476,', '安徽省-阜阳市-颍东区', '143', '02', '04', '01', '1476', '', null, null);
INSERT INTO t_sys_area VALUES ('1477', '341204', '341204', '341204', '颍泉区', 'Yingquan Qu', 'Yingquan Qu', ',13,143,1477,', '安徽省-阜阳市-颍泉区', '143', '02', '04', '01', '1477', '', null, null);
INSERT INTO t_sys_area VALUES ('1478', '341221', '341221', '341221', '临泉县', 'Linquan Xian', 'Linquan Xian', ',13,143,1478,', '安徽省-阜阳市-临泉县', '143', '02', '04', '01', '1478', '', null, null);
INSERT INTO t_sys_area VALUES ('1479', '341222', '341222', '341222', '太和县', 'Taihe Xian', 'Taihe Xian', ',13,143,1479,', '安徽省-阜阳市-太和县', '143', '02', '04', '01', '1479', '', null, null);
INSERT INTO t_sys_area VALUES ('1480', '341225', '341225', '341225', '阜南县', 'Funan Xian', 'Funan Xian', ',13,143,1480,', '安徽省-阜阳市-阜南县', '143', '02', '04', '01', '1480', '', null, null);
INSERT INTO t_sys_area VALUES ('1481', '341226', '341226', '341226', '颍上县', 'Yingshang Xian', 'Yingshang Xian', ',13,143,1481,', '安徽省-阜阳市-颍上县', '143', '02', '04', '01', '1481', '', null, null);
INSERT INTO t_sys_area VALUES ('1482', '341282', '341282', '341282', '界首市', 'Jieshou Shi', 'Jieshou Shi', ',13,143,1482,', '安徽省-阜阳市-界首市', '143', '02', '04', '01', '1482', '', null, null);
INSERT INTO t_sys_area VALUES ('1483', '341301', '341301', '341301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',13,144,1483,', '安徽省-宿州市-市辖区', '144', '02', '04', '01', '1483', '', null, null);
INSERT INTO t_sys_area VALUES ('1484', '341302', '341302', '341302', '埇桥区', 'Yongqiao Qu', 'Yongqiao Qu', ',13,144,1484,', '安徽省-宿州市-埇桥区', '144', '02', '04', '01', '1484', '', null, null);
INSERT INTO t_sys_area VALUES ('1485', '341321', '341321', '341321', '砀山县', 'Dangshan Xian', 'Dangshan Xian', ',13,144,1485,', '安徽省-宿州市-砀山县', '144', '02', '04', '01', '1485', '', null, null);
INSERT INTO t_sys_area VALUES ('1486', '341322', '341322', '341322', '萧县', 'Xiao Xian', 'Xiao Xian', ',13,144,1486,', '安徽省-宿州市-萧县', '144', '02', '04', '01', '1486', '', null, null);
INSERT INTO t_sys_area VALUES ('1487', '341323', '341323', '341323', '灵璧县', 'Lingbi Xian', 'Lingbi Xian', ',13,144,1487,', '安徽省-宿州市-灵璧县', '144', '02', '04', '01', '1487', '', null, null);
INSERT INTO t_sys_area VALUES ('1488', '341324', '341324', '341324', '泗县', 'Si Xian ', 'Si Xian ', ',13,144,1488,', '安徽省-宿州市-泗县', '144', '02', '04', '01', '1488', '', null, null);
INSERT INTO t_sys_area VALUES ('1489', '341401', '341401', '341401', '市辖区', '1', '1', ',13,145,1489,', '安徽省-巢湖市-市辖区', '145', '02', '04', '01', '1489', '', null, null);
INSERT INTO t_sys_area VALUES ('1490', '341402', '341402', '341402', '居巢区', 'Juchao Qu', 'Juchao Qu', ',13,145,1490,', '安徽省-巢湖市-居巢区', '145', '02', '04', '01', '1490', '', null, null);
INSERT INTO t_sys_area VALUES ('1491', '341421', '341421', '341421', '庐江县', 'Lujiang Xian', 'Lujiang Xian', ',13,145,1491,', '安徽省-巢湖市-庐江县', '145', '02', '04', '01', '1491', '', null, null);
INSERT INTO t_sys_area VALUES ('1492', '341422', '341422', '341422', '无为县', 'Wuwei Xian', 'Wuwei Xian', ',13,145,1492,', '安徽省-巢湖市-无为县', '145', '02', '04', '01', '1492', '', null, null);
INSERT INTO t_sys_area VALUES ('1493', '341423', '341423', '341423', '含山县', 'Hanshan Xian', 'Hanshan Xian', ',13,145,1493,', '安徽省-巢湖市-含山县', '145', '02', '04', '01', '1493', '', null, null);
INSERT INTO t_sys_area VALUES ('1494', '341424', '341424', '341424', '和县', 'He Xian ', 'He Xian ', ',13,145,1494,', '安徽省-巢湖市-和县', '145', '02', '04', '01', '1494', '', null, null);
INSERT INTO t_sys_area VALUES ('1495', '341501', '341501', '341501', '市辖区', '1', '1', ',13,146,1495,', '安徽省-六安市-市辖区', '146', '02', '04', '01', '1495', '', null, null);
INSERT INTO t_sys_area VALUES ('1496', '341502', '341502', '341502', '金安区', 'Jinan Qu', 'Jinan Qu', ',13,146,1496,', '安徽省-六安市-金安区', '146', '02', '04', '01', '1496', '', null, null);
INSERT INTO t_sys_area VALUES ('1497', '341503', '341503', '341503', '裕安区', 'Yuan Qu', 'Yuan Qu', ',13,146,1497,', '安徽省-六安市-裕安区', '146', '02', '04', '01', '1497', '', null, null);
INSERT INTO t_sys_area VALUES ('1498', '341521', '341521', '341521', '寿县', 'Shou Xian', 'Shou Xian', ',13,146,1498,', '安徽省-六安市-寿县', '146', '02', '04', '01', '1498', '', null, null);
INSERT INTO t_sys_area VALUES ('1499', '341522', '341522', '341522', '霍邱县', 'Huoqiu Xian', 'Huoqiu Xian', ',13,146,1499,', '安徽省-六安市-霍邱县', '146', '02', '04', '01', '1499', '', null, null);
INSERT INTO t_sys_area VALUES ('1500', '341523', '341523', '341523', '舒城县', 'Shucheng Xian', 'Shucheng Xian', ',13,146,1500,', '安徽省-六安市-舒城县', '146', '02', '04', '01', '1500', '', null, null);
INSERT INTO t_sys_area VALUES ('1501', '341524', '341524', '341524', '金寨县', 'Jingzhai Xian', 'Jingzhai Xian', ',13,146,1501,', '安徽省-六安市-金寨县', '146', '02', '04', '01', '1501', '', null, null);
INSERT INTO t_sys_area VALUES ('1502', '341525', '341525', '341525', '霍山县', 'Huoshan Xian', 'Huoshan Xian', ',13,146,1502,', '安徽省-六安市-霍山县', '146', '02', '04', '01', '1502', '', null, null);
INSERT INTO t_sys_area VALUES ('1503', '341601', '341601', '341601', '市辖区', '1', '1', ',13,147,1503,', '安徽省-亳州市-市辖区', '147', '02', '04', '01', '1503', '', null, null);
INSERT INTO t_sys_area VALUES ('1504', '341602', '341602', '341602', '谯城区', 'Qiaocheng Qu', 'Qiaocheng Qu', ',13,147,1504,', '安徽省-亳州市-谯城区', '147', '02', '04', '01', '1504', '', null, null);
INSERT INTO t_sys_area VALUES ('1505', '341621', '341621', '341621', '涡阳县', 'Guoyang Xian', 'Guoyang Xian', ',13,147,1505,', '安徽省-亳州市-涡阳县', '147', '02', '04', '01', '1505', '', null, null);
INSERT INTO t_sys_area VALUES ('1506', '341622', '341622', '341622', '蒙城县', 'Mengcheng Xian', 'Mengcheng Xian', ',13,147,1506,', '安徽省-亳州市-蒙城县', '147', '02', '04', '01', '1506', '', null, null);
INSERT INTO t_sys_area VALUES ('1507', '341623', '341623', '341623', '利辛县', 'Lixin Xian', 'Lixin Xian', ',13,147,1507,', '安徽省-亳州市-利辛县', '147', '02', '04', '01', '1507', '', null, null);
INSERT INTO t_sys_area VALUES ('1508', '341701', '341701', '341701', '市辖区', '1', '1', ',13,148,1508,', '安徽省-池州市-市辖区', '148', '02', '04', '01', '1508', '', null, null);
INSERT INTO t_sys_area VALUES ('1509', '341702', '341702', '341702', '贵池区', 'Guichi Qu', 'Guichi Qu', ',13,148,1509,', '安徽省-池州市-贵池区', '148', '02', '04', '01', '1509', '', null, null);
INSERT INTO t_sys_area VALUES ('1510', '341721', '341721', '341721', '东至县', 'Dongzhi Xian', 'Dongzhi Xian', ',13,148,1510,', '安徽省-池州市-东至县', '148', '02', '04', '01', '1510', '', null, null);
INSERT INTO t_sys_area VALUES ('1511', '341722', '341722', '341722', '石台县', 'Shitai Xian', 'Shitai Xian', ',13,148,1511,', '安徽省-池州市-石台县', '148', '02', '04', '01', '1511', '', null, null);
INSERT INTO t_sys_area VALUES ('1512', '341723', '341723', '341723', '青阳县', 'Qingyang Xian', 'Qingyang Xian', ',13,148,1512,', '安徽省-池州市-青阳县', '148', '02', '04', '01', '1512', '', null, null);
INSERT INTO t_sys_area VALUES ('1513', '341801', '341801', '341801', '市辖区', '1', '1', ',13,149,1513,', '安徽省-宣城市-市辖区', '149', '02', '04', '01', '1513', '', null, null);
INSERT INTO t_sys_area VALUES ('1514', '341802', '341802', '341802', '宣州区', 'Xuanzhou Qu', 'Xuanzhou Qu', ',13,149,1514,', '安徽省-宣城市-宣州区', '149', '02', '04', '01', '1514', '', null, null);
INSERT INTO t_sys_area VALUES ('1515', '341821', '341821', '341821', '郎溪县', 'Langxi Xian', 'Langxi Xian', ',13,149,1515,', '安徽省-宣城市-郎溪县', '149', '02', '04', '01', '1515', '', null, null);
INSERT INTO t_sys_area VALUES ('1516', '341822', '341822', '341822', '广德县', 'Guangde Xian', 'Guangde Xian', ',13,149,1516,', '安徽省-宣城市-广德县', '149', '02', '04', '01', '1516', '', null, null);
INSERT INTO t_sys_area VALUES ('1517', '341823', '341823', '341823', '泾县', 'Jing Xian', 'Jing Xian', ',13,149,1517,', '安徽省-宣城市-泾县', '149', '02', '04', '01', '1517', '', null, null);
INSERT INTO t_sys_area VALUES ('1518', '341824', '341824', '341824', '绩溪县', 'Jixi Xian', 'Jixi Xian', ',13,149,1518,', '安徽省-宣城市-绩溪县', '149', '02', '04', '01', '1518', '', null, null);
INSERT INTO t_sys_area VALUES ('1519', '341825', '341825', '341825', '旌德县', 'Jingde Xian', 'Jingde Xian', ',13,149,1519,', '安徽省-宣城市-旌德县', '149', '02', '04', '01', '1519', '', null, null);
INSERT INTO t_sys_area VALUES ('1520', '341881', '341881', '341881', '宁国市', 'Ningguo Shi', 'Ningguo Shi', ',13,149,1520,', '安徽省-宣城市-宁国市', '149', '02', '04', '01', '1520', '', null, null);
INSERT INTO t_sys_area VALUES ('1521', '350101', '350101', '350101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',14,150,1521,', '福建省-福州市-市辖区', '150', '02', '04', '01', '1521', '', null, null);
INSERT INTO t_sys_area VALUES ('1522', '350102', '350102', '350102', '鼓楼区', 'Gulou Qu', 'Gulou Qu', ',14,150,1522,', '福建省-福州市-鼓楼区', '150', '02', '04', '01', '1522', '', null, null);
INSERT INTO t_sys_area VALUES ('1523', '350103', '350103', '350103', '台江区', 'Taijiang Qu', 'Taijiang Qu', ',14,150,1523,', '福建省-福州市-台江区', '150', '02', '04', '01', '1523', '', null, null);
INSERT INTO t_sys_area VALUES ('1524', '350104', '350104', '350104', '仓山区', 'Cangshan Qu', 'Cangshan Qu', ',14,150,1524,', '福建省-福州市-仓山区', '150', '02', '04', '01', '1524', '', null, null);
INSERT INTO t_sys_area VALUES ('1525', '350105', '350105', '350105', '马尾区', 'Mawei Qu', 'Mawei Qu', ',14,150,1525,', '福建省-福州市-马尾区', '150', '02', '04', '01', '1525', '', null, null);
INSERT INTO t_sys_area VALUES ('1526', '350111', '350111', '350111', '晋安区', 'Jin,an Qu', 'Jin,an Qu', ',14,150,1526,', '福建省-福州市-晋安区', '150', '02', '04', '01', '1526', '', null, null);
INSERT INTO t_sys_area VALUES ('1527', '350121', '350121', '350121', '闽侯县', 'Minhou Qu', 'Minhou Qu', ',14,150,1527,', '福建省-福州市-闽侯县', '150', '02', '04', '01', '1527', '', null, null);
INSERT INTO t_sys_area VALUES ('1528', '350122', '350122', '350122', '连江县', 'Lianjiang Xian', 'Lianjiang Xian', ',14,150,1528,', '福建省-福州市-连江县', '150', '02', '04', '01', '1528', '', null, null);
INSERT INTO t_sys_area VALUES ('1529', '350123', '350123', '350123', '罗源县', 'Luoyuan Xian', 'Luoyuan Xian', ',14,150,1529,', '福建省-福州市-罗源县', '150', '02', '04', '01', '1529', '', null, null);
INSERT INTO t_sys_area VALUES ('1530', '350124', '350124', '350124', '闽清县', 'Minqing Xian', 'Minqing Xian', ',14,150,1530,', '福建省-福州市-闽清县', '150', '02', '04', '01', '1530', '', null, null);
INSERT INTO t_sys_area VALUES ('1531', '350125', '350125', '350125', '永泰县', 'Yongtai Xian', 'Yongtai Xian', ',14,150,1531,', '福建省-福州市-永泰县', '150', '02', '04', '01', '1531', '', null, null);
INSERT INTO t_sys_area VALUES ('1532', '350128', '350128', '350128', '平潭县', 'Pingtan Xian', 'Pingtan Xian', ',14,150,1532,', '福建省-福州市-平潭县', '150', '02', '04', '01', '1532', '', null, null);
INSERT INTO t_sys_area VALUES ('1533', '350181', '350181', '350181', '福清市', 'Fuqing Shi', 'Fuqing Shi', ',14,150,1533,', '福建省-福州市-福清市', '150', '02', '04', '01', '1533', '', null, null);
INSERT INTO t_sys_area VALUES ('1534', '350182', '350182', '350182', '长乐市', 'Changle Shi', 'Changle Shi', ',14,150,1534,', '福建省-福州市-长乐市', '150', '02', '04', '01', '1534', '', null, null);
INSERT INTO t_sys_area VALUES ('1535', '350201', '350201', '350201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',14,151,1535,', '福建省-厦门市-市辖区', '151', '02', '04', '01', '1535', '', null, null);
INSERT INTO t_sys_area VALUES ('1536', '350203', '350203', '350203', '思明区', 'Siming Qu', 'Siming Qu', ',14,151,1536,', '福建省-厦门市-思明区', '151', '02', '04', '01', '1536', '', null, null);
INSERT INTO t_sys_area VALUES ('1537', '350205', '350205', '350205', '海沧区', 'Haicang Qu', 'Haicang Qu', ',14,151,1537,', '福建省-厦门市-海沧区', '151', '02', '04', '01', '1537', '', null, null);
INSERT INTO t_sys_area VALUES ('1538', '350206', '350206', '350206', '湖里区', 'Huli Qu', 'Huli Qu', ',14,151,1538,', '福建省-厦门市-湖里区', '151', '02', '04', '01', '1538', '', null, null);
INSERT INTO t_sys_area VALUES ('1539', '350211', '350211', '350211', '集美区', 'Jimei Qu', 'Jimei Qu', ',14,151,1539,', '福建省-厦门市-集美区', '151', '02', '04', '01', '1539', '', null, null);
INSERT INTO t_sys_area VALUES ('1540', '350212', '350212', '350212', '同安区', 'Tong,an Qu', 'Tong,an Qu', ',14,151,1540,', '福建省-厦门市-同安区', '151', '02', '04', '01', '1540', '', null, null);
INSERT INTO t_sys_area VALUES ('1541', '350213', '350213', '350213', '翔安区', 'Xiangan Qu', 'Xiangan Qu', ',14,151,1541,', '福建省-厦门市-翔安区', '151', '02', '04', '01', '1541', '', null, null);
INSERT INTO t_sys_area VALUES ('1542', '350301', '350301', '350301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',14,152,1542,', '福建省-莆田市-市辖区', '152', '02', '04', '01', '1542', '', null, null);
INSERT INTO t_sys_area VALUES ('1543', '350302', '350302', '350302', '城厢区', 'Chengxiang Qu', 'Chengxiang Qu', ',14,152,1543,', '福建省-莆田市-城厢区', '152', '02', '04', '01', '1543', '', null, null);
INSERT INTO t_sys_area VALUES ('1544', '350303', '350303', '350303', '涵江区', 'Hanjiang Qu', 'Hanjiang Qu', ',14,152,1544,', '福建省-莆田市-涵江区', '152', '02', '04', '01', '1544', '', null, null);
INSERT INTO t_sys_area VALUES ('1545', '350304', '350304', '350304', '荔城区', 'Licheng Qu', 'Licheng Qu', ',14,152,1545,', '福建省-莆田市-荔城区', '152', '02', '04', '01', '1545', '', null, null);
INSERT INTO t_sys_area VALUES ('1546', '350305', '350305', '350305', '秀屿区', 'Xiuyu Qu', 'Xiuyu Qu', ',14,152,1546,', '福建省-莆田市-秀屿区', '152', '02', '04', '01', '1546', '', null, null);
INSERT INTO t_sys_area VALUES ('1547', '350322', '350322', '350322', '仙游县', 'Xianyou Xian', 'Xianyou Xian', ',14,152,1547,', '福建省-莆田市-仙游县', '152', '02', '04', '01', '1547', '', null, null);
INSERT INTO t_sys_area VALUES ('1548', '350401', '350401', '350401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',14,153,1548,', '福建省-三明市-市辖区', '153', '02', '04', '01', '1548', '', null, null);
INSERT INTO t_sys_area VALUES ('1549', '350402', '350402', '350402', '梅列区', 'Meilie Qu', 'Meilie Qu', ',14,153,1549,', '福建省-三明市-梅列区', '153', '02', '04', '01', '1549', '', null, null);
INSERT INTO t_sys_area VALUES ('1550', '350403', '350403', '350403', '三元区', 'Sanyuan Qu', 'Sanyuan Qu', ',14,153,1550,', '福建省-三明市-三元区', '153', '02', '04', '01', '1550', '', null, null);
INSERT INTO t_sys_area VALUES ('1551', '350421', '350421', '350421', '明溪县', 'Mingxi Xian', 'Mingxi Xian', ',14,153,1551,', '福建省-三明市-明溪县', '153', '02', '04', '01', '1551', '', null, null);
INSERT INTO t_sys_area VALUES ('1552', '350423', '350423', '350423', '清流县', 'Qingliu Xian', 'Qingliu Xian', ',14,153,1552,', '福建省-三明市-清流县', '153', '02', '04', '01', '1552', '', null, null);
INSERT INTO t_sys_area VALUES ('1553', '350424', '350424', '350424', '宁化县', 'Ninghua Xian', 'Ninghua Xian', ',14,153,1553,', '福建省-三明市-宁化县', '153', '02', '04', '01', '1553', '', null, null);
INSERT INTO t_sys_area VALUES ('1554', '350425', '350425', '350425', '大田县', 'Datian Xian', 'Datian Xian', ',14,153,1554,', '福建省-三明市-大田县', '153', '02', '04', '01', '1554', '', null, null);
INSERT INTO t_sys_area VALUES ('1555', '350426', '350426', '350426', '尤溪县', 'Youxi Xian', 'Youxi Xian', ',14,153,1555,', '福建省-三明市-尤溪县', '153', '02', '04', '01', '1555', '', null, null);
INSERT INTO t_sys_area VALUES ('1556', '350427', '350427', '350427', '沙县', 'Sha Xian', 'Sha Xian', ',14,153,1556,', '福建省-三明市-沙县', '153', '02', '04', '01', '1556', '', null, null);
INSERT INTO t_sys_area VALUES ('1557', '350428', '350428', '350428', '将乐县', 'Jiangle Xian', 'Jiangle Xian', ',14,153,1557,', '福建省-三明市-将乐县', '153', '02', '04', '01', '1557', '', null, null);
INSERT INTO t_sys_area VALUES ('1558', '350429', '350429', '350429', '泰宁县', 'Taining Xian', 'Taining Xian', ',14,153,1558,', '福建省-三明市-泰宁县', '153', '02', '04', '01', '1558', '', null, null);
INSERT INTO t_sys_area VALUES ('1559', '350430', '350430', '350430', '建宁县', 'Jianning Xian', 'Jianning Xian', ',14,153,1559,', '福建省-三明市-建宁县', '153', '02', '04', '01', '1559', '', null, null);
INSERT INTO t_sys_area VALUES ('1560', '350481', '350481', '350481', '永安市', 'Yong,an Shi', 'Yong,an Shi', ',14,153,1560,', '福建省-三明市-永安市', '153', '02', '04', '01', '1560', '', null, null);
INSERT INTO t_sys_area VALUES ('1561', '350501', '350501', '350501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',14,154,1561,', '福建省-泉州市-市辖区', '154', '02', '04', '01', '1561', '', null, null);
INSERT INTO t_sys_area VALUES ('1562', '350502', '350502', '350502', '鲤城区', 'Licheng Qu', 'Licheng Qu', ',14,154,1562,', '福建省-泉州市-鲤城区', '154', '02', '04', '01', '1562', '', null, null);
INSERT INTO t_sys_area VALUES ('1563', '350503', '350503', '350503', '丰泽区', 'Fengze Qu', 'Fengze Qu', ',14,154,1563,', '福建省-泉州市-丰泽区', '154', '02', '04', '01', '1563', '', null, null);
INSERT INTO t_sys_area VALUES ('1564', '350504', '350504', '350504', '洛江区', 'Luojiang Qu', 'Luojiang Qu', ',14,154,1564,', '福建省-泉州市-洛江区', '154', '02', '04', '01', '1564', '', null, null);
INSERT INTO t_sys_area VALUES ('1565', '350505', '350505', '350505', '泉港区', 'Quangang Qu', 'Quangang Qu', ',14,154,1565,', '福建省-泉州市-泉港区', '154', '02', '04', '01', '1565', '', null, null);
INSERT INTO t_sys_area VALUES ('1566', '350521', '350521', '350521', '惠安县', 'Hui,an Xian', 'Hui,an Xian', ',14,154,1566,', '福建省-泉州市-惠安县', '154', '02', '04', '01', '1566', '', null, null);
INSERT INTO t_sys_area VALUES ('1567', '350524', '350524', '350524', '安溪县', 'Anxi Xian', 'Anxi Xian', ',14,154,1567,', '福建省-泉州市-安溪县', '154', '02', '04', '01', '1567', '', null, null);
INSERT INTO t_sys_area VALUES ('1568', '350525', '350525', '350525', '永春县', 'Yongchun Xian', 'Yongchun Xian', ',14,154,1568,', '福建省-泉州市-永春县', '154', '02', '04', '01', '1568', '', null, null);
INSERT INTO t_sys_area VALUES ('1569', '350526', '350526', '350526', '德化县', 'Dehua Xian', 'Dehua Xian', ',14,154,1569,', '福建省-泉州市-德化县', '154', '02', '04', '01', '1569', '', null, null);
INSERT INTO t_sys_area VALUES ('1570', '350527', '350527', '350527', '金门县', 'Jinmen Xian', 'Jinmen Xian', ',14,154,1570,', '福建省-泉州市-金门县', '154', '02', '04', '01', '1570', '', null, null);
INSERT INTO t_sys_area VALUES ('1571', '350581', '350581', '350581', '石狮市', 'Shishi Shi', 'Shishi Shi', ',14,154,1571,', '福建省-泉州市-石狮市', '154', '02', '04', '01', '1571', '', null, null);
INSERT INTO t_sys_area VALUES ('1572', '350582', '350582', '350582', '晋江市', 'Jinjiang Shi', 'Jinjiang Shi', ',14,154,1572,', '福建省-泉州市-晋江市', '154', '02', '04', '01', '1572', '', null, null);
INSERT INTO t_sys_area VALUES ('1573', '350583', '350583', '350583', '南安市', 'Nan,an Shi', 'Nan,an Shi', ',14,154,1573,', '福建省-泉州市-南安市', '154', '02', '04', '01', '1573', '', null, null);
INSERT INTO t_sys_area VALUES ('1574', '350601', '350601', '350601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',14,155,1574,', '福建省-漳州市-市辖区', '155', '02', '04', '01', '1574', '', null, null);
INSERT INTO t_sys_area VALUES ('1575', '350602', '350602', '350602', '芗城区', 'Xiangcheng Qu', 'Xiangcheng Qu', ',14,155,1575,', '福建省-漳州市-芗城区', '155', '02', '04', '01', '1575', '', null, null);
INSERT INTO t_sys_area VALUES ('1576', '350603', '350603', '350603', '龙文区', 'Longwen Qu', 'Longwen Qu', ',14,155,1576,', '福建省-漳州市-龙文区', '155', '02', '04', '01', '1576', '', null, null);
INSERT INTO t_sys_area VALUES ('1577', '350622', '350622', '350622', '云霄县', 'Yunxiao Xian', 'Yunxiao Xian', ',14,155,1577,', '福建省-漳州市-云霄县', '155', '02', '04', '01', '1577', '', null, null);
INSERT INTO t_sys_area VALUES ('1578', '350623', '350623', '350623', '漳浦县', 'Zhangpu Xian', 'Zhangpu Xian', ',14,155,1578,', '福建省-漳州市-漳浦县', '155', '02', '04', '01', '1578', '', null, null);
INSERT INTO t_sys_area VALUES ('1579', '350624', '350624', '350624', '诏安县', 'Zhao,an Xian', 'Zhao,an Xian', ',14,155,1579,', '福建省-漳州市-诏安县', '155', '02', '04', '01', '1579', '', null, null);
INSERT INTO t_sys_area VALUES ('1580', '350625', '350625', '350625', '长泰县', 'Changtai Xian', 'Changtai Xian', ',14,155,1580,', '福建省-漳州市-长泰县', '155', '02', '04', '01', '1580', '', null, null);
INSERT INTO t_sys_area VALUES ('1581', '350626', '350626', '350626', '东山县', 'Dongshan Xian', 'Dongshan Xian', ',14,155,1581,', '福建省-漳州市-东山县', '155', '02', '04', '01', '1581', '', null, null);
INSERT INTO t_sys_area VALUES ('1582', '350627', '350627', '350627', '南靖县', 'Nanjing Xian', 'Nanjing Xian', ',14,155,1582,', '福建省-漳州市-南靖县', '155', '02', '04', '01', '1582', '', null, null);
INSERT INTO t_sys_area VALUES ('1583', '350628', '350628', '350628', '平和县', 'Pinghe Xian', 'Pinghe Xian', ',14,155,1583,', '福建省-漳州市-平和县', '155', '02', '04', '01', '1583', '', null, null);
INSERT INTO t_sys_area VALUES ('1584', '350629', '350629', '350629', '华安县', 'Hua,an Xian', 'Hua,an Xian', ',14,155,1584,', '福建省-漳州市-华安县', '155', '02', '04', '01', '1584', '', null, null);
INSERT INTO t_sys_area VALUES ('1585', '350681', '350681', '350681', '龙海市', 'Longhai Shi', 'Longhai Shi', ',14,155,1585,', '福建省-漳州市-龙海市', '155', '02', '04', '01', '1585', '', null, null);
INSERT INTO t_sys_area VALUES ('1586', '350701', '350701', '350701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',14,156,1586,', '福建省-南平市-市辖区', '156', '02', '04', '01', '1586', '', null, null);
INSERT INTO t_sys_area VALUES ('1587', '350702', '350702', '350702', '延平区', 'Yanping Qu', 'Yanping Qu', ',14,156,1587,', '福建省-南平市-延平区', '156', '02', '04', '01', '1587', '', null, null);
INSERT INTO t_sys_area VALUES ('1588', '350721', '350721', '350721', '顺昌县', 'Shunchang Xian', 'Shunchang Xian', ',14,156,1588,', '福建省-南平市-顺昌县', '156', '02', '04', '01', '1588', '', null, null);
INSERT INTO t_sys_area VALUES ('1589', '350722', '350722', '350722', '浦城县', 'Pucheng Xian', 'Pucheng Xian', ',14,156,1589,', '福建省-南平市-浦城县', '156', '02', '04', '01', '1589', '', null, null);
INSERT INTO t_sys_area VALUES ('1590', '350723', '350723', '350723', '光泽县', 'Guangze Xian', 'Guangze Xian', ',14,156,1590,', '福建省-南平市-光泽县', '156', '02', '04', '01', '1590', '', null, null);
INSERT INTO t_sys_area VALUES ('1591', '350724', '350724', '350724', '松溪县', 'Songxi Xian', 'Songxi Xian', ',14,156,1591,', '福建省-南平市-松溪县', '156', '02', '04', '01', '1591', '', null, null);
INSERT INTO t_sys_area VALUES ('1592', '350725', '350725', '350725', '政和县', 'Zhenghe Xian', 'Zhenghe Xian', ',14,156,1592,', '福建省-南平市-政和县', '156', '02', '04', '01', '1592', '', null, null);
INSERT INTO t_sys_area VALUES ('1593', '350781', '350781', '350781', '邵武市', 'Shaowu Shi', 'Shaowu Shi', ',14,156,1593,', '福建省-南平市-邵武市', '156', '02', '04', '01', '1593', '', null, null);
INSERT INTO t_sys_area VALUES ('1594', '350782', '350782', '350782', '武夷山市', 'Wuyishan Shi', 'Wuyishan Shi', ',14,156,1594,', '福建省-南平市-武夷山市', '156', '02', '04', '01', '1594', '', null, null);
INSERT INTO t_sys_area VALUES ('1595', '350783', '350783', '350783', '建瓯市', 'Jian,ou Shi', 'Jian,ou Shi', ',14,156,1595,', '福建省-南平市-建瓯市', '156', '02', '04', '01', '1595', '', null, null);
INSERT INTO t_sys_area VALUES ('1596', '350784', '350784', '350784', '建阳市', 'Jianyang Shi', 'Jianyang Shi', ',14,156,1596,', '福建省-南平市-建阳市', '156', '02', '04', '01', '1596', '', null, null);
INSERT INTO t_sys_area VALUES ('1597', '350801', '350801', '350801', '市辖区', 'Shixiaqu', 'Shixiaqu', ',14,157,1597,', '福建省-龙岩市-市辖区', '157', '02', '04', '01', '1597', '', null, null);
INSERT INTO t_sys_area VALUES ('1598', '350802', '350802', '350802', '新罗区', 'Xinluo Qu', 'Xinluo Qu', ',14,157,1598,', '福建省-龙岩市-新罗区', '157', '02', '04', '01', '1598', '', null, null);
INSERT INTO t_sys_area VALUES ('1599', '350821', '350821', '350821', '长汀县', 'Changting Xian', 'Changting Xian', ',14,157,1599,', '福建省-龙岩市-长汀县', '157', '02', '04', '01', '1599', '', null, null);
INSERT INTO t_sys_area VALUES ('1600', '350822', '350822', '350822', '永定县', 'Yongding Xian', 'Yongding Xian', ',14,157,1600,', '福建省-龙岩市-永定县', '157', '02', '04', '01', '1600', '', null, null);
INSERT INTO t_sys_area VALUES ('1601', '350823', '350823', '350823', '上杭县', 'Shanghang Xian', 'Shanghang Xian', ',14,157,1601,', '福建省-龙岩市-上杭县', '157', '02', '04', '01', '1601', '', null, null);
INSERT INTO t_sys_area VALUES ('1602', '350824', '350824', '350824', '武平县', 'Wuping Xian', 'Wuping Xian', ',14,157,1602,', '福建省-龙岩市-武平县', '157', '02', '04', '01', '1602', '', null, null);
INSERT INTO t_sys_area VALUES ('1603', '350825', '350825', '350825', '连城县', 'Liancheng Xian', 'Liancheng Xian', ',14,157,1603,', '福建省-龙岩市-连城县', '157', '02', '04', '01', '1603', '', null, null);
INSERT INTO t_sys_area VALUES ('1604', '350881', '350881', '350881', '漳平市', 'Zhangping Xian', 'Zhangping Xian', ',14,157,1604,', '福建省-龙岩市-漳平市', '157', '02', '04', '01', '1604', '', null, null);
INSERT INTO t_sys_area VALUES ('1605', '350901', '350901', '350901', '市辖区', '1', '1', ',14,158,1605,', '福建省-宁德市-市辖区', '158', '02', '04', '01', '1605', '', null, null);
INSERT INTO t_sys_area VALUES ('1606', '350902', '350902', '350902', '蕉城区', 'Jiaocheng Qu', 'Jiaocheng Qu', ',14,158,1606,', '福建省-宁德市-蕉城区', '158', '02', '04', '01', '1606', '', null, null);
INSERT INTO t_sys_area VALUES ('1607', '350921', '350921', '350921', '霞浦县', 'Xiapu Xian', 'Xiapu Xian', ',14,158,1607,', '福建省-宁德市-霞浦县', '158', '02', '04', '01', '1607', '', null, null);
INSERT INTO t_sys_area VALUES ('1608', '350922', '350922', '350922', '古田县', 'Gutian Xian', 'Gutian Xian', ',14,158,1608,', '福建省-宁德市-古田县', '158', '02', '04', '01', '1608', '', null, null);
INSERT INTO t_sys_area VALUES ('1609', '350923', '350923', '350923', '屏南县', 'Pingnan Xian', 'Pingnan Xian', ',14,158,1609,', '福建省-宁德市-屏南县', '158', '02', '04', '01', '1609', '', null, null);
INSERT INTO t_sys_area VALUES ('1610', '350924', '350924', '350924', '寿宁县', 'Shouning Xian', 'Shouning Xian', ',14,158,1610,', '福建省-宁德市-寿宁县', '158', '02', '04', '01', '1610', '', null, null);
INSERT INTO t_sys_area VALUES ('1611', '350925', '350925', '350925', '周宁县', 'Zhouning Xian', 'Zhouning Xian', ',14,158,1611,', '福建省-宁德市-周宁县', '158', '02', '04', '01', '1611', '', null, null);
INSERT INTO t_sys_area VALUES ('1612', '350926', '350926', '350926', '柘荣县', 'Zherong Xian', 'Zherong Xian', ',14,158,1612,', '福建省-宁德市-柘荣县', '158', '02', '04', '01', '1612', '', null, null);
INSERT INTO t_sys_area VALUES ('1613', '350981', '350981', '350981', '福安市', 'Fu,an Shi', 'Fu,an Shi', ',14,158,1613,', '福建省-宁德市-福安市', '158', '02', '04', '01', '1613', '', null, null);
INSERT INTO t_sys_area VALUES ('1614', '350982', '350982', '350982', '福鼎市', 'Fuding Shi', 'Fuding Shi', ',14,158,1614,', '福建省-宁德市-福鼎市', '158', '02', '04', '01', '1614', '', null, null);
INSERT INTO t_sys_area VALUES ('1615', '360101', '360101', '360101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',15,159,1615,', '江西省-南昌市-市辖区', '159', '02', '04', '01', '1615', '', null, null);
INSERT INTO t_sys_area VALUES ('1616', '360102', '360102', '360102', '东湖区', 'Donghu Qu', 'Donghu Qu', ',15,159,1616,', '江西省-南昌市-东湖区', '159', '02', '04', '01', '1616', '', null, null);
INSERT INTO t_sys_area VALUES ('1617', '360103', '360103', '360103', '西湖区', 'Xihu Qu ', 'Xihu Qu ', ',15,159,1617,', '江西省-南昌市-西湖区', '159', '02', '04', '01', '1617', '', null, null);
INSERT INTO t_sys_area VALUES ('1618', '360104', '360104', '360104', '青云谱区', 'Qingyunpu Qu', 'Qingyunpu Qu', ',15,159,1618,', '江西省-南昌市-青云谱区', '159', '02', '04', '01', '1618', '', null, null);
INSERT INTO t_sys_area VALUES ('1619', '360105', '360105', '360105', '湾里区', 'Wanli Qu', 'Wanli Qu', ',15,159,1619,', '江西省-南昌市-湾里区', '159', '02', '04', '01', '1619', '', null, null);
INSERT INTO t_sys_area VALUES ('1620', '360111', '360111', '360111', '青山湖区', 'Qingshanhu Qu', 'Qingshanhu Qu', ',15,159,1620,', '江西省-南昌市-青山湖区', '159', '02', '04', '01', '1620', '', null, null);
INSERT INTO t_sys_area VALUES ('1621', '360121', '360121', '360121', '南昌县', 'Nanchang Xian', 'Nanchang Xian', ',15,159,1621,', '江西省-南昌市-南昌县', '159', '02', '04', '01', '1621', '', null, null);
INSERT INTO t_sys_area VALUES ('1622', '360122', '360122', '360122', '新建县', 'Xinjian Xian', 'Xinjian Xian', ',15,159,1622,', '江西省-南昌市-新建县', '159', '02', '04', '01', '1622', '', null, null);
INSERT INTO t_sys_area VALUES ('1623', '360123', '360123', '360123', '安义县', 'Anyi Xian', 'Anyi Xian', ',15,159,1623,', '江西省-南昌市-安义县', '159', '02', '04', '01', '1623', '', null, null);
INSERT INTO t_sys_area VALUES ('1624', '360124', '360124', '360124', '进贤县', 'Jinxian Xian', 'Jinxian Xian', ',15,159,1624,', '江西省-南昌市-进贤县', '159', '02', '04', '01', '1624', '', null, null);
INSERT INTO t_sys_area VALUES ('1625', '360201', '360201', '360201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',15,160,1625,', '江西省-景德镇市-市辖区', '160', '02', '04', '01', '1625', '', null, null);
INSERT INTO t_sys_area VALUES ('1626', '360202', '360202', '360202', '昌江区', 'Changjiang Qu', 'Changjiang Qu', ',15,160,1626,', '江西省-景德镇市-昌江区', '160', '02', '04', '01', '1626', '', null, null);
INSERT INTO t_sys_area VALUES ('1627', '360203', '360203', '360203', '珠山区', 'Zhushan Qu', 'Zhushan Qu', ',15,160,1627,', '江西省-景德镇市-珠山区', '160', '02', '04', '01', '1627', '', null, null);
INSERT INTO t_sys_area VALUES ('1628', '360222', '360222', '360222', '浮梁县', 'Fuliang Xian', 'Fuliang Xian', ',15,160,1628,', '江西省-景德镇市-浮梁县', '160', '02', '04', '01', '1628', '', null, null);
INSERT INTO t_sys_area VALUES ('1629', '360281', '360281', '360281', '乐平市', 'Leping Shi', 'Leping Shi', ',15,160,1629,', '江西省-景德镇市-乐平市', '160', '02', '04', '01', '1629', '', null, null);
INSERT INTO t_sys_area VALUES ('1630', '360301', '360301', '360301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',15,161,1630,', '江西省-萍乡市-市辖区', '161', '02', '04', '01', '1630', '', null, null);
INSERT INTO t_sys_area VALUES ('1631', '360302', '360302', '360302', '安源区', 'Anyuan Qu', 'Anyuan Qu', ',15,161,1631,', '江西省-萍乡市-安源区', '161', '02', '04', '01', '1631', '', null, null);
INSERT INTO t_sys_area VALUES ('1632', '360313', '360313', '360313', '湘东区', 'Xiangdong Qu', 'Xiangdong Qu', ',15,161,1632,', '江西省-萍乡市-湘东区', '161', '02', '04', '01', '1632', '', null, null);
INSERT INTO t_sys_area VALUES ('1633', '360321', '360321', '360321', '莲花县', 'Lianhua Xian', 'Lianhua Xian', ',15,161,1633,', '江西省-萍乡市-莲花县', '161', '02', '04', '01', '1633', '', null, null);
INSERT INTO t_sys_area VALUES ('1634', '360322', '360322', '360322', '上栗县', 'Shangli Xian', 'Shangli Xian', ',15,161,1634,', '江西省-萍乡市-上栗县', '161', '02', '04', '01', '1634', '', null, null);
INSERT INTO t_sys_area VALUES ('1635', '360323', '360323', '360323', '芦溪县', 'Lixi Xian', 'Lixi Xian', ',15,161,1635,', '江西省-萍乡市-芦溪县', '161', '02', '04', '01', '1635', '', null, null);
INSERT INTO t_sys_area VALUES ('1636', '360401', '360401', '360401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',15,162,1636,', '江西省-九江市-市辖区', '162', '02', '04', '01', '1636', '', null, null);
INSERT INTO t_sys_area VALUES ('1637', '360402', '360402', '360402', '庐山区', 'Lushan Qu', 'Lushan Qu', ',15,162,1637,', '江西省-九江市-庐山区', '162', '02', '04', '01', '1637', '', null, null);
INSERT INTO t_sys_area VALUES ('1638', '360403', '360403', '360403', '浔阳区', 'Xunyang Qu', 'Xunyang Qu', ',15,162,1638,', '江西省-九江市-浔阳区', '162', '02', '04', '01', '1638', '', null, null);
INSERT INTO t_sys_area VALUES ('1639', '360421', '360421', '360421', '九江县', 'Jiujiang Xian', 'Jiujiang Xian', ',15,162,1639,', '江西省-九江市-九江县', '162', '02', '04', '01', '1639', '', null, null);
INSERT INTO t_sys_area VALUES ('1640', '360423', '360423', '360423', '武宁县', 'Wuning Xian', 'Wuning Xian', ',15,162,1640,', '江西省-九江市-武宁县', '162', '02', '04', '01', '1640', '', null, null);
INSERT INTO t_sys_area VALUES ('1641', '360424', '360424', '360424', '修水县', 'Xiushui Xian', 'Xiushui Xian', ',15,162,1641,', '江西省-九江市-修水县', '162', '02', '04', '01', '1641', '', null, null);
INSERT INTO t_sys_area VALUES ('1642', '360425', '360425', '360425', '永修县', 'Yongxiu Xian', 'Yongxiu Xian', ',15,162,1642,', '江西省-九江市-永修县', '162', '02', '04', '01', '1642', '', null, null);
INSERT INTO t_sys_area VALUES ('1643', '360426', '360426', '360426', '德安县', 'De,an Xian', 'De,an Xian', ',15,162,1643,', '江西省-九江市-德安县', '162', '02', '04', '01', '1643', '', null, null);
INSERT INTO t_sys_area VALUES ('1644', '360427', '360427', '360427', '星子县', 'Xingzi Xian', 'Xingzi Xian', ',15,162,1644,', '江西省-九江市-星子县', '162', '02', '04', '01', '1644', '', null, null);
INSERT INTO t_sys_area VALUES ('1645', '360428', '360428', '360428', '都昌县', 'Duchang Xian', 'Duchang Xian', ',15,162,1645,', '江西省-九江市-都昌县', '162', '02', '04', '01', '1645', '', null, null);
INSERT INTO t_sys_area VALUES ('1646', '360429', '360429', '360429', '湖口县', 'Hukou Xian', 'Hukou Xian', ',15,162,1646,', '江西省-九江市-湖口县', '162', '02', '04', '01', '1646', '', null, null);
INSERT INTO t_sys_area VALUES ('1647', '360430', '360430', '360430', '彭泽县', 'Pengze Xian', 'Pengze Xian', ',15,162,1647,', '江西省-九江市-彭泽县', '162', '02', '04', '01', '1647', '', null, null);
INSERT INTO t_sys_area VALUES ('1648', '360481', '360481', '360481', '瑞昌市', 'Ruichang Shi', 'Ruichang Shi', ',15,162,1648,', '江西省-九江市-瑞昌市', '162', '02', '04', '01', '1648', '', null, null);
INSERT INTO t_sys_area VALUES ('1649', '360501', '360501', '360501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',15,163,1649,', '江西省-新余市-市辖区', '163', '02', '04', '01', '1649', '', null, null);
INSERT INTO t_sys_area VALUES ('1650', '360502', '360502', '360502', '渝水区', 'Yushui Qu', 'Yushui Qu', ',15,163,1650,', '江西省-新余市-渝水区', '163', '02', '04', '01', '1650', '', null, null);
INSERT INTO t_sys_area VALUES ('1651', '360521', '360521', '360521', '分宜县', 'Fenyi Xian', 'Fenyi Xian', ',15,163,1651,', '江西省-新余市-分宜县', '163', '02', '04', '01', '1651', '', null, null);
INSERT INTO t_sys_area VALUES ('1652', '360601', '360601', '360601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',15,164,1652,', '江西省-鹰潭市-市辖区', '164', '02', '04', '01', '1652', '', null, null);
INSERT INTO t_sys_area VALUES ('1653', '360602', '360602', '360602', '月湖区', 'Yuehu Qu', 'Yuehu Qu', ',15,164,1653,', '江西省-鹰潭市-月湖区', '164', '02', '04', '01', '1653', '', null, null);
INSERT INTO t_sys_area VALUES ('1654', '360622', '360622', '360622', '余江县', 'Yujiang Xian', 'Yujiang Xian', ',15,164,1654,', '江西省-鹰潭市-余江县', '164', '02', '04', '01', '1654', '', null, null);
INSERT INTO t_sys_area VALUES ('1655', '360681', '360681', '360681', '贵溪市', 'Guixi Shi', 'Guixi Shi', ',15,164,1655,', '江西省-鹰潭市-贵溪市', '164', '02', '04', '01', '1655', '', null, null);
INSERT INTO t_sys_area VALUES ('1656', '360701', '360701', '360701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',15,165,1656,', '江西省-赣州市-市辖区', '165', '02', '04', '01', '1656', '', null, null);
INSERT INTO t_sys_area VALUES ('1657', '360702', '360702', '360702', '章贡区', 'Zhanggong Qu', 'Zhanggong Qu', ',15,165,1657,', '江西省-赣州市-章贡区', '165', '02', '04', '01', '1657', '', null, null);
INSERT INTO t_sys_area VALUES ('1658', '360721', '360721', '360721', '赣县', 'Gan Xian', 'Gan Xian', ',15,165,1658,', '江西省-赣州市-赣县', '165', '02', '04', '01', '1658', '', null, null);
INSERT INTO t_sys_area VALUES ('1659', '360722', '360722', '360722', '信丰县', 'Xinfeng Xian ', 'Xinfeng Xian ', ',15,165,1659,', '江西省-赣州市-信丰县', '165', '02', '04', '01', '1659', '', null, null);
INSERT INTO t_sys_area VALUES ('1660', '360723', '360723', '360723', '大余县', 'Dayu Xian', 'Dayu Xian', ',15,165,1660,', '江西省-赣州市-大余县', '165', '02', '04', '01', '1660', '', null, null);
INSERT INTO t_sys_area VALUES ('1661', '360724', '360724', '360724', '上犹县', 'Shangyou Xian', 'Shangyou Xian', ',15,165,1661,', '江西省-赣州市-上犹县', '165', '02', '04', '01', '1661', '', null, null);
INSERT INTO t_sys_area VALUES ('1662', '360725', '360725', '360725', '崇义县', 'Chongyi Xian', 'Chongyi Xian', ',15,165,1662,', '江西省-赣州市-崇义县', '165', '02', '04', '01', '1662', '', null, null);
INSERT INTO t_sys_area VALUES ('1663', '360726', '360726', '360726', '安远县', 'Anyuan Xian', 'Anyuan Xian', ',15,165,1663,', '江西省-赣州市-安远县', '165', '02', '04', '01', '1663', '', null, null);
INSERT INTO t_sys_area VALUES ('1664', '360727', '360727', '360727', '龙南县', 'Longnan Xian', 'Longnan Xian', ',15,165,1664,', '江西省-赣州市-龙南县', '165', '02', '04', '01', '1664', '', null, null);
INSERT INTO t_sys_area VALUES ('1665', '360728', '360728', '360728', '定南县', 'Dingnan Xian', 'Dingnan Xian', ',15,165,1665,', '江西省-赣州市-定南县', '165', '02', '04', '01', '1665', '', null, null);
INSERT INTO t_sys_area VALUES ('1666', '360729', '360729', '360729', '全南县', 'Quannan Xian', 'Quannan Xian', ',15,165,1666,', '江西省-赣州市-全南县', '165', '02', '04', '01', '1666', '', null, null);
INSERT INTO t_sys_area VALUES ('1667', '360730', '360730', '360730', '宁都县', 'Ningdu Xian', 'Ningdu Xian', ',15,165,1667,', '江西省-赣州市-宁都县', '165', '02', '04', '01', '1667', '', null, null);
INSERT INTO t_sys_area VALUES ('1668', '360731', '360731', '360731', '于都县', 'Yudu Xian', 'Yudu Xian', ',15,165,1668,', '江西省-赣州市-于都县', '165', '02', '04', '01', '1668', '', null, null);
INSERT INTO t_sys_area VALUES ('1669', '360732', '360732', '360732', '兴国县', 'Xingguo Xian', 'Xingguo Xian', ',15,165,1669,', '江西省-赣州市-兴国县', '165', '02', '04', '01', '1669', '', null, null);
INSERT INTO t_sys_area VALUES ('1670', '360733', '360733', '360733', '会昌县', 'Huichang Xian', 'Huichang Xian', ',15,165,1670,', '江西省-赣州市-会昌县', '165', '02', '04', '01', '1670', '', null, null);
INSERT INTO t_sys_area VALUES ('1671', '360734', '360734', '360734', '寻乌县', 'Xunwu Xian', 'Xunwu Xian', ',15,165,1671,', '江西省-赣州市-寻乌县', '165', '02', '04', '01', '1671', '', null, null);
INSERT INTO t_sys_area VALUES ('1672', '360735', '360735', '360735', '石城县', 'Shicheng Xian', 'Shicheng Xian', ',15,165,1672,', '江西省-赣州市-石城县', '165', '02', '04', '01', '1672', '', null, null);
INSERT INTO t_sys_area VALUES ('1673', '360781', '360781', '360781', '瑞金市', 'Ruijin Shi', 'Ruijin Shi', ',15,165,1673,', '江西省-赣州市-瑞金市', '165', '02', '04', '01', '1673', '', null, null);
INSERT INTO t_sys_area VALUES ('1674', '360782', '360782', '360782', '南康市', 'Nankang Shi', 'Nankang Shi', ',15,165,1674,', '江西省-赣州市-南康市', '165', '02', '04', '01', '1674', '', null, null);
INSERT INTO t_sys_area VALUES ('1675', '360801', '360801', '360801', '市辖区', '1', '1', ',15,166,1675,', '江西省-吉安市-市辖区', '166', '02', '04', '01', '1675', '', null, null);
INSERT INTO t_sys_area VALUES ('1676', '360802', '360802', '360802', '吉州区', 'Jizhou Qu', 'Jizhou Qu', ',15,166,1676,', '江西省-吉安市-吉州区', '166', '02', '04', '01', '1676', '', null, null);
INSERT INTO t_sys_area VALUES ('1677', '360803', '360803', '360803', '青原区', 'Qingyuan Qu', 'Qingyuan Qu', ',15,166,1677,', '江西省-吉安市-青原区', '166', '02', '04', '01', '1677', '', null, null);
INSERT INTO t_sys_area VALUES ('1678', '360821', '360821', '360821', '吉安县', 'Ji,an Xian', 'Ji,an Xian', ',15,166,1678,', '江西省-吉安市-吉安县', '166', '02', '04', '01', '1678', '', null, null);
INSERT INTO t_sys_area VALUES ('1679', '360822', '360822', '360822', '吉水县', 'Jishui Xian', 'Jishui Xian', ',15,166,1679,', '江西省-吉安市-吉水县', '166', '02', '04', '01', '1679', '', null, null);
INSERT INTO t_sys_area VALUES ('1680', '360823', '360823', '360823', '峡江县', 'Xiajiang Xian', 'Xiajiang Xian', ',15,166,1680,', '江西省-吉安市-峡江县', '166', '02', '04', '01', '1680', '', null, null);
INSERT INTO t_sys_area VALUES ('1681', '360824', '360824', '360824', '新干县', 'Xingan Xian', 'Xingan Xian', ',15,166,1681,', '江西省-吉安市-新干县', '166', '02', '04', '01', '1681', '', null, null);
INSERT INTO t_sys_area VALUES ('1682', '360825', '360825', '360825', '永丰县', 'Yongfeng Xian', 'Yongfeng Xian', ',15,166,1682,', '江西省-吉安市-永丰县', '166', '02', '04', '01', '1682', '', null, null);
INSERT INTO t_sys_area VALUES ('1683', '360826', '360826', '360826', '泰和县', 'Taihe Xian', 'Taihe Xian', ',15,166,1683,', '江西省-吉安市-泰和县', '166', '02', '04', '01', '1683', '', null, null);
INSERT INTO t_sys_area VALUES ('1684', '360827', '360827', '360827', '遂川县', 'Suichuan Xian', 'Suichuan Xian', ',15,166,1684,', '江西省-吉安市-遂川县', '166', '02', '04', '01', '1684', '', null, null);
INSERT INTO t_sys_area VALUES ('1685', '360828', '360828', '360828', '万安县', 'Wan,an Xian', 'Wan,an Xian', ',15,166,1685,', '江西省-吉安市-万安县', '166', '02', '04', '01', '1685', '', null, null);
INSERT INTO t_sys_area VALUES ('1686', '360829', '360829', '360829', '安福县', 'Anfu Xian', 'Anfu Xian', ',15,166,1686,', '江西省-吉安市-安福县', '166', '02', '04', '01', '1686', '', null, null);
INSERT INTO t_sys_area VALUES ('1687', '360830', '360830', '360830', '永新县', 'Yongxin Xian ', 'Yongxin Xian ', ',15,166,1687,', '江西省-吉安市-永新县', '166', '02', '04', '01', '1687', '', null, null);
INSERT INTO t_sys_area VALUES ('1688', '360881', '360881', '360881', '井冈山市', 'Jinggangshan Shi', 'Jinggangshan Shi', ',15,166,1688,', '江西省-吉安市-井冈山市', '166', '02', '04', '01', '1688', '', null, null);
INSERT INTO t_sys_area VALUES ('1689', '360901', '360901', '360901', '市辖区', '1', '1', ',15,167,1689,', '江西省-宜春市-市辖区', '167', '02', '04', '01', '1689', '', null, null);
INSERT INTO t_sys_area VALUES ('1690', '360902', '360902', '360902', '袁州区', 'Yuanzhou Qu', 'Yuanzhou Qu', ',15,167,1690,', '江西省-宜春市-袁州区', '167', '02', '04', '01', '1690', '', null, null);
INSERT INTO t_sys_area VALUES ('1691', '360921', '360921', '360921', '奉新县', 'Fengxin Xian', 'Fengxin Xian', ',15,167,1691,', '江西省-宜春市-奉新县', '167', '02', '04', '01', '1691', '', null, null);
INSERT INTO t_sys_area VALUES ('1692', '360922', '360922', '360922', '万载县', 'Wanzai Xian', 'Wanzai Xian', ',15,167,1692,', '江西省-宜春市-万载县', '167', '02', '04', '01', '1692', '', null, null);
INSERT INTO t_sys_area VALUES ('1693', '360923', '360923', '360923', '上高县', 'Shanggao Xian', 'Shanggao Xian', ',15,167,1693,', '江西省-宜春市-上高县', '167', '02', '04', '01', '1693', '', null, null);
INSERT INTO t_sys_area VALUES ('1694', '360924', '360924', '360924', '宜丰县', 'Yifeng Xian', 'Yifeng Xian', ',15,167,1694,', '江西省-宜春市-宜丰县', '167', '02', '04', '01', '1694', '', null, null);
INSERT INTO t_sys_area VALUES ('1695', '360925', '360925', '360925', '靖安县', 'Jing,an Xian', 'Jing,an Xian', ',15,167,1695,', '江西省-宜春市-靖安县', '167', '02', '04', '01', '1695', '', null, null);
INSERT INTO t_sys_area VALUES ('1696', '360926', '360926', '360926', '铜鼓县', 'Tonggu Xian', 'Tonggu Xian', ',15,167,1696,', '江西省-宜春市-铜鼓县', '167', '02', '04', '01', '1696', '', null, null);
INSERT INTO t_sys_area VALUES ('1697', '360981', '360981', '360981', '丰城市', 'Fengcheng Shi', 'Fengcheng Shi', ',15,167,1697,', '江西省-宜春市-丰城市', '167', '02', '04', '01', '1697', '', null, null);
INSERT INTO t_sys_area VALUES ('1698', '360982', '360982', '360982', '樟树市', 'Zhangshu Shi', 'Zhangshu Shi', ',15,167,1698,', '江西省-宜春市-樟树市', '167', '02', '04', '01', '1698', '', null, null);
INSERT INTO t_sys_area VALUES ('1699', '360983', '360983', '360983', '高安市', 'Gao,an Shi', 'Gao,an Shi', ',15,167,1699,', '江西省-宜春市-高安市', '167', '02', '04', '01', '1699', '', null, null);
INSERT INTO t_sys_area VALUES ('1700', '361001', '361001', '361001', '市辖区', '1', '1', ',15,168,1700,', '江西省-抚州市-市辖区', '168', '02', '04', '01', '1700', '', null, null);
INSERT INTO t_sys_area VALUES ('1701', '361002', '361002', '361002', '临川区', 'Linchuan Qu', 'Linchuan Qu', ',15,168,1701,', '江西省-抚州市-临川区', '168', '02', '04', '01', '1701', '', null, null);
INSERT INTO t_sys_area VALUES ('1702', '361021', '361021', '361021', '南城县', 'Nancheng Xian', 'Nancheng Xian', ',15,168,1702,', '江西省-抚州市-南城县', '168', '02', '04', '01', '1702', '', null, null);
INSERT INTO t_sys_area VALUES ('1703', '361022', '361022', '361022', '黎川县', 'Lichuan Xian', 'Lichuan Xian', ',15,168,1703,', '江西省-抚州市-黎川县', '168', '02', '04', '01', '1703', '', null, null);
INSERT INTO t_sys_area VALUES ('1704', '361023', '361023', '361023', '南丰县', 'Nanfeng Xian', 'Nanfeng Xian', ',15,168,1704,', '江西省-抚州市-南丰县', '168', '02', '04', '01', '1704', '', null, null);
INSERT INTO t_sys_area VALUES ('1705', '361024', '361024', '361024', '崇仁县', 'Chongren Xian', 'Chongren Xian', ',15,168,1705,', '江西省-抚州市-崇仁县', '168', '02', '04', '01', '1705', '', null, null);
INSERT INTO t_sys_area VALUES ('1706', '361025', '361025', '361025', '乐安县', 'Le,an Xian', 'Le,an Xian', ',15,168,1706,', '江西省-抚州市-乐安县', '168', '02', '04', '01', '1706', '', null, null);
INSERT INTO t_sys_area VALUES ('1707', '361026', '361026', '361026', '宜黄县', 'Yihuang Xian', 'Yihuang Xian', ',15,168,1707,', '江西省-抚州市-宜黄县', '168', '02', '04', '01', '1707', '', null, null);
INSERT INTO t_sys_area VALUES ('1708', '361027', '361027', '361027', '金溪县', 'Jinxi Xian', 'Jinxi Xian', ',15,168,1708,', '江西省-抚州市-金溪县', '168', '02', '04', '01', '1708', '', null, null);
INSERT INTO t_sys_area VALUES ('1709', '361028', '361028', '361028', '资溪县', 'Zixi Xian', 'Zixi Xian', ',15,168,1709,', '江西省-抚州市-资溪县', '168', '02', '04', '01', '1709', '', null, null);
INSERT INTO t_sys_area VALUES ('1710', '361029', '361029', '361029', '东乡县', 'Dongxiang Xian', 'Dongxiang Xian', ',15,168,1710,', '江西省-抚州市-东乡县', '168', '02', '04', '01', '1710', '', null, null);
INSERT INTO t_sys_area VALUES ('1711', '361030', '361030', '361030', '广昌县', 'Guangchang Xian', 'Guangchang Xian', ',15,168,1711,', '江西省-抚州市-广昌县', '168', '02', '04', '01', '1711', '', null, null);
INSERT INTO t_sys_area VALUES ('1712', '361101', '361101', '361101', '市辖区', '1', '1', ',15,169,1712,', '江西省-上饶市-市辖区', '169', '02', '04', '01', '1712', '', null, null);
INSERT INTO t_sys_area VALUES ('1713', '361102', '361102', '361102', '信州区', 'Xinzhou Qu', 'Xinzhou Qu', ',15,169,1713,', '江西省-上饶市-信州区', '169', '02', '04', '01', '1713', '', null, null);
INSERT INTO t_sys_area VALUES ('1714', '361121', '361121', '361121', '上饶县', 'Shangrao Xian ', 'Shangrao Xian ', ',15,169,1714,', '江西省-上饶市-上饶县', '169', '02', '04', '01', '1714', '', null, null);
INSERT INTO t_sys_area VALUES ('1715', '361122', '361122', '361122', '广丰县', 'Guangfeng Xian', 'Guangfeng Xian', ',15,169,1715,', '江西省-上饶市-广丰县', '169', '02', '04', '01', '1715', '', null, null);
INSERT INTO t_sys_area VALUES ('1716', '361123', '361123', '361123', '玉山县', 'Yushan Xian', 'Yushan Xian', ',15,169,1716,', '江西省-上饶市-玉山县', '169', '02', '04', '01', '1716', '', null, null);
INSERT INTO t_sys_area VALUES ('1717', '361124', '361124', '361124', '铅山县', 'Qianshan Xian', 'Qianshan Xian', ',15,169,1717,', '江西省-上饶市-铅山县', '169', '02', '04', '01', '1717', '', null, null);
INSERT INTO t_sys_area VALUES ('1718', '361125', '361125', '361125', '横峰县', 'Hengfeng Xian', 'Hengfeng Xian', ',15,169,1718,', '江西省-上饶市-横峰县', '169', '02', '04', '01', '1718', '', null, null);
INSERT INTO t_sys_area VALUES ('1719', '361126', '361126', '361126', '弋阳县', 'Yiyang Xian', 'Yiyang Xian', ',15,169,1719,', '江西省-上饶市-弋阳县', '169', '02', '04', '01', '1719', '', null, null);
INSERT INTO t_sys_area VALUES ('1720', '361127', '361127', '361127', '余干县', 'Yugan Xian', 'Yugan Xian', ',15,169,1720,', '江西省-上饶市-余干县', '169', '02', '04', '01', '1720', '', null, null);
INSERT INTO t_sys_area VALUES ('1721', '361128', '361128', '361128', '鄱阳县', 'Poyang Xian', 'Poyang Xian', ',15,169,1721,', '江西省-上饶市-鄱阳县', '169', '02', '04', '01', '1721', '', null, null);
INSERT INTO t_sys_area VALUES ('1722', '361129', '361129', '361129', '万年县', 'Wannian Xian', 'Wannian Xian', ',15,169,1722,', '江西省-上饶市-万年县', '169', '02', '04', '01', '1722', '', null, null);
INSERT INTO t_sys_area VALUES ('1723', '361130', '361130', '361130', '婺源县', 'Wuyuan Xian', 'Wuyuan Xian', ',15,169,1723,', '江西省-上饶市-婺源县', '169', '02', '04', '01', '1723', '', null, null);
INSERT INTO t_sys_area VALUES ('1724', '361181', '361181', '361181', '德兴市', 'Dexing Shi', 'Dexing Shi', ',15,169,1724,', '江西省-上饶市-德兴市', '169', '02', '04', '01', '1724', '', null, null);
INSERT INTO t_sys_area VALUES ('1725', '370101', '370101', '370101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',16,170,1725,', '山东省-济南市-市辖区', '170', '02', '04', '01', '1725', '', null, null);
INSERT INTO t_sys_area VALUES ('1726', '370102', '370102', '370102', '历下区', 'Lixia Qu', 'Lixia Qu', ',16,170,1726,', '山东省-济南市-历下区', '170', '02', '04', '01', '1726', '', null, null);
INSERT INTO t_sys_area VALUES ('1727', '370101', '370101', '370101', '市中区', 'Shizhong Qu', 'Shizhong Qu', ',16,170,1727,', '山东省-济南市-市中区', '170', '02', '04', '01', '1727', '', null, null);
INSERT INTO t_sys_area VALUES ('1728', '370104', '370104', '370104', '槐荫区', 'Huaiyin Qu', 'Huaiyin Qu', ',16,170,1728,', '山东省-济南市-槐荫区', '170', '02', '04', '01', '1728', '', null, null);
INSERT INTO t_sys_area VALUES ('1729', '370105', '370105', '370105', '天桥区', 'Tianqiao Qu', 'Tianqiao Qu', ',16,170,1729,', '山东省-济南市-天桥区', '170', '02', '04', '01', '1729', '', null, null);
INSERT INTO t_sys_area VALUES ('1730', '370112', '370112', '370112', '历城区', 'Licheng Qu', 'Licheng Qu', ',16,170,1730,', '山东省-济南市-历城区', '170', '02', '04', '01', '1730', '', null, null);
INSERT INTO t_sys_area VALUES ('1731', '370113', '370113', '370113', '长清区', 'Changqing Qu', 'Changqing Qu', ',16,170,1731,', '山东省-济南市-长清区', '170', '02', '04', '01', '1731', '', null, null);
INSERT INTO t_sys_area VALUES ('1732', '370124', '370124', '370124', '平阴县', 'Pingyin Xian', 'Pingyin Xian', ',16,170,1732,', '山东省-济南市-平阴县', '170', '02', '04', '01', '1732', '', null, null);
INSERT INTO t_sys_area VALUES ('1733', '370125', '370125', '370125', '济阳县', 'Jiyang Xian', 'Jiyang Xian', ',16,170,1733,', '山东省-济南市-济阳县', '170', '02', '04', '01', '1733', '', null, null);
INSERT INTO t_sys_area VALUES ('1734', '370126', '370126', '370126', '商河县', 'Shanghe Xian', 'Shanghe Xian', ',16,170,1734,', '山东省-济南市-商河县', '170', '02', '04', '01', '1734', '', null, null);
INSERT INTO t_sys_area VALUES ('1735', '370181', '370181', '370181', '章丘市', 'Zhangqiu Shi', 'Zhangqiu Shi', ',16,170,1735,', '山东省-济南市-章丘市', '170', '02', '04', '01', '1735', '', null, null);
INSERT INTO t_sys_area VALUES ('1736', '370201', '370201', '370201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',16,171,1736,', '山东省-青岛市-市辖区', '171', '02', '04', '01', '1736', '', null, null);
INSERT INTO t_sys_area VALUES ('1737', '370202', '370202', '370202', '市南区', 'Shinan Qu', 'Shinan Qu', ',16,171,1737,', '山东省-青岛市-市南区', '171', '02', '04', '01', '1737', '', null, null);
INSERT INTO t_sys_area VALUES ('1738', '370203', '370203', '370203', '市北区', 'Shibei Qu', 'Shibei Qu', ',16,171,1738,', '山东省-青岛市-市北区', '171', '02', '04', '01', '1738', '', null, null);
INSERT INTO t_sys_area VALUES ('1739', '370205', '370205', '370205', '四方区', 'Sifang Qu', 'Sifang Qu', ',16,171,1739,', '山东省-青岛市-四方区', '171', '02', '04', '01', '1739', '', null, null);
INSERT INTO t_sys_area VALUES ('1740', '370211', '370211', '370211', '黄岛区', 'Huangdao Qu', 'Huangdao Qu', ',16,171,1740,', '山东省-青岛市-黄岛区', '171', '02', '04', '01', '1740', '', null, null);
INSERT INTO t_sys_area VALUES ('1741', '370212', '370212', '370212', '崂山区', 'Laoshan Qu', 'Laoshan Qu', ',16,171,1741,', '山东省-青岛市-崂山区', '171', '02', '04', '01', '1741', '', null, null);
INSERT INTO t_sys_area VALUES ('1742', '370213', '370213', '370213', '李沧区', 'Licang Qu', 'Licang Qu', ',16,171,1742,', '山东省-青岛市-李沧区', '171', '02', '04', '01', '1742', '', null, null);
INSERT INTO t_sys_area VALUES ('1743', '370214', '370214', '370214', '城阳区', 'Chengyang Qu', 'Chengyang Qu', ',16,171,1743,', '山东省-青岛市-城阳区', '171', '02', '04', '01', '1743', '', null, null);
INSERT INTO t_sys_area VALUES ('1744', '370281', '370281', '370281', '胶州市', 'Jiaozhou Shi', 'Jiaozhou Shi', ',16,171,1744,', '山东省-青岛市-胶州市', '171', '02', '04', '01', '1744', '', null, null);
INSERT INTO t_sys_area VALUES ('1745', '370282', '370282', '370282', '即墨市', 'Jimo Shi', 'Jimo Shi', ',16,171,1745,', '山东省-青岛市-即墨市', '171', '02', '04', '01', '1745', '', null, null);
INSERT INTO t_sys_area VALUES ('1746', '370283', '370283', '370283', '平度市', 'Pingdu Shi', 'Pingdu Shi', ',16,171,1746,', '山东省-青岛市-平度市', '171', '02', '04', '01', '1746', '', null, null);
INSERT INTO t_sys_area VALUES ('1747', '370284', '370284', '370284', '胶南市', 'Jiaonan Shi', 'Jiaonan Shi', ',16,171,1747,', '山东省-青岛市-胶南市', '171', '02', '04', '01', '1747', '', null, null);
INSERT INTO t_sys_area VALUES ('1748', '370285', '370285', '370285', '莱西市', 'Laixi Shi', 'Laixi Shi', ',16,171,1748,', '山东省-青岛市-莱西市', '171', '02', '04', '01', '1748', '', null, null);
INSERT INTO t_sys_area VALUES ('1749', '370301', '370301', '370301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',16,172,1749,', '山东省-淄博市-市辖区', '172', '02', '04', '01', '1749', '', null, null);
INSERT INTO t_sys_area VALUES ('1750', '370302', '370302', '370302', '淄川区', 'Zichuan Qu', 'Zichuan Qu', ',16,172,1750,', '山东省-淄博市-淄川区', '172', '02', '04', '01', '1750', '', null, null);
INSERT INTO t_sys_area VALUES ('1751', '370303', '370303', '370303', '张店区', 'Zhangdian Qu', 'Zhangdian Qu', ',16,172,1751,', '山东省-淄博市-张店区', '172', '02', '04', '01', '1751', '', null, null);
INSERT INTO t_sys_area VALUES ('1752', '370304', '370304', '370304', '博山区', 'Boshan Qu', 'Boshan Qu', ',16,172,1752,', '山东省-淄博市-博山区', '172', '02', '04', '01', '1752', '', null, null);
INSERT INTO t_sys_area VALUES ('1753', '370305', '370305', '370305', '临淄区', 'Linzi Qu', 'Linzi Qu', ',16,172,1753,', '山东省-淄博市-临淄区', '172', '02', '04', '01', '1753', '', null, null);
INSERT INTO t_sys_area VALUES ('1754', '370306', '370306', '370306', '周村区', 'Zhoucun Qu', 'Zhoucun Qu', ',16,172,1754,', '山东省-淄博市-周村区', '172', '02', '04', '01', '1754', '', null, null);
INSERT INTO t_sys_area VALUES ('1755', '370321', '370321', '370321', '桓台县', 'Huantai Xian', 'Huantai Xian', ',16,172,1755,', '山东省-淄博市-桓台县', '172', '02', '04', '01', '1755', '', null, null);
INSERT INTO t_sys_area VALUES ('1756', '370322', '370322', '370322', '高青县', 'Gaoqing Xian', 'Gaoqing Xian', ',16,172,1756,', '山东省-淄博市-高青县', '172', '02', '04', '01', '1756', '', null, null);
INSERT INTO t_sys_area VALUES ('1757', '370323', '370323', '370323', '沂源县', 'Yiyuan Xian', 'Yiyuan Xian', ',16,172,1757,', '山东省-淄博市-沂源县', '172', '02', '04', '01', '1757', '', null, null);
INSERT INTO t_sys_area VALUES ('1758', '370401', '370401', '370401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',16,173,1758,', '山东省-枣庄市-市辖区', '173', '02', '04', '01', '1758', '', null, null);
INSERT INTO t_sys_area VALUES ('1759', '370402', '370402', '370402', '市中区', 'Shizhong Qu', 'Shizhong Qu', ',16,173,1759,', '山东省-枣庄市-市中区', '173', '02', '04', '01', '1759', '', null, null);
INSERT INTO t_sys_area VALUES ('1760', '370403', '370403', '370403', '薛城区', 'Xuecheng Qu', 'Xuecheng Qu', ',16,173,1760,', '山东省-枣庄市-薛城区', '173', '02', '04', '01', '1760', '', null, null);
INSERT INTO t_sys_area VALUES ('1761', '370404', '370404', '370404', '峄城区', 'Yicheng Qu', 'Yicheng Qu', ',16,173,1761,', '山东省-枣庄市-峄城区', '173', '02', '04', '01', '1761', '', null, null);
INSERT INTO t_sys_area VALUES ('1762', '370405', '370405', '370405', '台儿庄区', 'Tai,erzhuang Qu', 'Tai,erzhuang Qu', ',16,173,1762,', '山东省-枣庄市-台儿庄区', '173', '02', '04', '01', '1762', '', null, null);
INSERT INTO t_sys_area VALUES ('1763', '370406', '370406', '370406', '山亭区', 'Shanting Qu', 'Shanting Qu', ',16,173,1763,', '山东省-枣庄市-山亭区', '173', '02', '04', '01', '1763', '', null, null);
INSERT INTO t_sys_area VALUES ('1764', '370481', '370481', '370481', '滕州市', 'Tengzhou Shi', 'Tengzhou Shi', ',16,173,1764,', '山东省-枣庄市-滕州市', '173', '02', '04', '01', '1764', '', null, null);
INSERT INTO t_sys_area VALUES ('1765', '370501', '370501', '370501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',16,174,1765,', '山东省-东营市-市辖区', '174', '02', '04', '01', '1765', '', null, null);
INSERT INTO t_sys_area VALUES ('1766', '370502', '370502', '370502', '东营区', 'Dongying Qu', 'Dongying Qu', ',16,174,1766,', '山东省-东营市-东营区', '174', '02', '04', '01', '1766', '', null, null);
INSERT INTO t_sys_area VALUES ('1767', '370503', '370503', '370503', '河口区', 'Hekou Qu', 'Hekou Qu', ',16,174,1767,', '山东省-东营市-河口区', '174', '02', '04', '01', '1767', '', null, null);
INSERT INTO t_sys_area VALUES ('1768', '370521', '370521', '370521', '垦利县', 'Kenli Xian', 'Kenli Xian', ',16,174,1768,', '山东省-东营市-垦利县', '174', '02', '04', '01', '1768', '', null, null);
INSERT INTO t_sys_area VALUES ('1769', '370522', '370522', '370522', '利津县', 'Lijin Xian', 'Lijin Xian', ',16,174,1769,', '山东省-东营市-利津县', '174', '02', '04', '01', '1769', '', null, null);
INSERT INTO t_sys_area VALUES ('1770', '370523', '370523', '370523', '广饶县', 'Guangrao Xian ', 'Guangrao Xian ', ',16,174,1770,', '山东省-东营市-广饶县', '174', '02', '04', '01', '1770', '', null, null);
INSERT INTO t_sys_area VALUES ('1771', '370601', '370601', '370601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',16,175,1771,', '山东省-烟台市-市辖区', '175', '02', '04', '01', '1771', '', null, null);
INSERT INTO t_sys_area VALUES ('1772', '370602', '370602', '370602', '芝罘区', 'Zhifu Qu', 'Zhifu Qu', ',16,175,1772,', '山东省-烟台市-芝罘区', '175', '02', '04', '01', '1772', '', null, null);
INSERT INTO t_sys_area VALUES ('1773', '370611', '370611', '370611', '福山区', 'Fushan Qu', 'Fushan Qu', ',16,175,1773,', '山东省-烟台市-福山区', '175', '02', '04', '01', '1773', '', null, null);
INSERT INTO t_sys_area VALUES ('1774', '370612', '370612', '370612', '牟平区', 'Muping Qu', 'Muping Qu', ',16,175,1774,', '山东省-烟台市-牟平区', '175', '02', '04', '01', '1774', '', null, null);
INSERT INTO t_sys_area VALUES ('1775', '370613', '370613', '370613', '莱山区', 'Laishan Qu', 'Laishan Qu', ',16,175,1775,', '山东省-烟台市-莱山区', '175', '02', '04', '01', '1775', '', null, null);
INSERT INTO t_sys_area VALUES ('1776', '370634', '370634', '370634', '长岛县', 'Changdao Xian', 'Changdao Xian', ',16,175,1776,', '山东省-烟台市-长岛县', '175', '02', '04', '01', '1776', '', null, null);
INSERT INTO t_sys_area VALUES ('1777', '370681', '370681', '370681', '龙口市', 'Longkou Shi', 'Longkou Shi', ',16,175,1777,', '山东省-烟台市-龙口市', '175', '02', '04', '01', '1777', '', null, null);
INSERT INTO t_sys_area VALUES ('1778', '370682', '370682', '370682', '莱阳市', 'Laiyang Shi', 'Laiyang Shi', ',16,175,1778,', '山东省-烟台市-莱阳市', '175', '02', '04', '01', '1778', '', null, null);
INSERT INTO t_sys_area VALUES ('1779', '370683', '370683', '370683', '莱州市', 'Laizhou Shi', 'Laizhou Shi', ',16,175,1779,', '山东省-烟台市-莱州市', '175', '02', '04', '01', '1779', '', null, null);
INSERT INTO t_sys_area VALUES ('1780', '370684', '370684', '370684', '蓬莱市', 'Penglai Shi', 'Penglai Shi', ',16,175,1780,', '山东省-烟台市-蓬莱市', '175', '02', '04', '01', '1780', '', null, null);
INSERT INTO t_sys_area VALUES ('1781', '370685', '370685', '370685', '招远市', 'Zhaoyuan Shi', 'Zhaoyuan Shi', ',16,175,1781,', '山东省-烟台市-招远市', '175', '02', '04', '01', '1781', '', null, null);
INSERT INTO t_sys_area VALUES ('1782', '370686', '370686', '370686', '栖霞市', 'Qixia Shi', 'Qixia Shi', ',16,175,1782,', '山东省-烟台市-栖霞市', '175', '02', '04', '01', '1782', '', null, null);
INSERT INTO t_sys_area VALUES ('1783', '370687', '370687', '370687', '海阳市', 'Haiyang Shi', 'Haiyang Shi', ',16,175,1783,', '山东省-烟台市-海阳市', '175', '02', '04', '01', '1783', '', null, null);
INSERT INTO t_sys_area VALUES ('1784', '370701', '370701', '370701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',16,176,1784,', '山东省-潍坊市-市辖区', '176', '02', '04', '01', '1784', '', null, null);
INSERT INTO t_sys_area VALUES ('1785', '370702', '370702', '370702', '潍城区', 'Weicheng Qu', 'Weicheng Qu', ',16,176,1785,', '山东省-潍坊市-潍城区', '176', '02', '04', '01', '1785', '', null, null);
INSERT INTO t_sys_area VALUES ('1786', '370703', '370703', '370703', '寒亭区', 'Hanting Qu', 'Hanting Qu', ',16,176,1786,', '山东省-潍坊市-寒亭区', '176', '02', '04', '01', '1786', '', null, null);
INSERT INTO t_sys_area VALUES ('1787', '370704', '370704', '370704', '坊子区', 'Fangzi Qu', 'Fangzi Qu', ',16,176,1787,', '山东省-潍坊市-坊子区', '176', '02', '04', '01', '1787', '', null, null);
INSERT INTO t_sys_area VALUES ('1788', '370705', '370705', '370705', '奎文区', 'Kuiwen Qu', 'Kuiwen Qu', ',16,176,1788,', '山东省-潍坊市-奎文区', '176', '02', '04', '01', '1788', '', null, null);
INSERT INTO t_sys_area VALUES ('1789', '370724', '370724', '370724', '临朐县', 'Linqu Xian', 'Linqu Xian', ',16,176,1789,', '山东省-潍坊市-临朐县', '176', '02', '04', '01', '1789', '', null, null);
INSERT INTO t_sys_area VALUES ('1790', '370725', '370725', '370725', '昌乐县', 'Changle Xian', 'Changle Xian', ',16,176,1790,', '山东省-潍坊市-昌乐县', '176', '02', '04', '01', '1790', '', null, null);
INSERT INTO t_sys_area VALUES ('1791', '370781', '370781', '370781', '青州市', 'Qingzhou Shi', 'Qingzhou Shi', ',16,176,1791,', '山东省-潍坊市-青州市', '176', '02', '04', '01', '1791', '', null, null);
INSERT INTO t_sys_area VALUES ('1792', '370782', '370782', '370782', '诸城市', 'Zhucheng Shi', 'Zhucheng Shi', ',16,176,1792,', '山东省-潍坊市-诸城市', '176', '02', '04', '01', '1792', '', null, null);
INSERT INTO t_sys_area VALUES ('1793', '370783', '370783', '370783', '寿光市', 'Shouguang Shi', 'Shouguang Shi', ',16,176,1793,', '山东省-潍坊市-寿光市', '176', '02', '04', '01', '1793', '', null, null);
INSERT INTO t_sys_area VALUES ('1794', '370784', '370784', '370784', '安丘市', 'Anqiu Shi', 'Anqiu Shi', ',16,176,1794,', '山东省-潍坊市-安丘市', '176', '02', '04', '01', '1794', '', null, null);
INSERT INTO t_sys_area VALUES ('1795', '370785', '370785', '370785', '高密市', 'Gaomi Shi', 'Gaomi Shi', ',16,176,1795,', '山东省-潍坊市-高密市', '176', '02', '04', '01', '1795', '', null, null);
INSERT INTO t_sys_area VALUES ('1796', '370786', '370786', '370786', '昌邑市', 'Changyi Shi', 'Changyi Shi', ',16,176,1796,', '山东省-潍坊市-昌邑市', '176', '02', '04', '01', '1796', '', null, null);
INSERT INTO t_sys_area VALUES ('1797', '370801', '370801', '370801', '市辖区', 'Shixiaqu', 'Shixiaqu', ',16,177,1797,', '山东省-济宁市-市辖区', '177', '02', '04', '01', '1797', '', null, null);
INSERT INTO t_sys_area VALUES ('1798', '370802', '370802', '370802', '市中区', 'Shizhong Qu', 'Shizhong Qu', ',16,177,1798,', '山东省-济宁市-市中区', '177', '02', '04', '01', '1798', '', null, null);
INSERT INTO t_sys_area VALUES ('1799', '370811', '370811', '370811', '任城区', 'Rencheng Qu', 'Rencheng Qu', ',16,177,1799,', '山东省-济宁市-任城区', '177', '02', '04', '01', '1799', '', null, null);
INSERT INTO t_sys_area VALUES ('1800', '370826', '370826', '370826', '微山县', 'Weishan Xian', 'Weishan Xian', ',16,177,1800,', '山东省-济宁市-微山县', '177', '02', '04', '01', '1800', '', null, null);
INSERT INTO t_sys_area VALUES ('1801', '370827', '370827', '370827', '鱼台县', 'Yutai Xian', 'Yutai Xian', ',16,177,1801,', '山东省-济宁市-鱼台县', '177', '02', '04', '01', '1801', '', null, null);
INSERT INTO t_sys_area VALUES ('1802', '370828', '370828', '370828', '金乡县', 'Jinxiang Xian', 'Jinxiang Xian', ',16,177,1802,', '山东省-济宁市-金乡县', '177', '02', '04', '01', '1802', '', null, null);
INSERT INTO t_sys_area VALUES ('1803', '370829', '370829', '370829', '嘉祥县', 'Jiaxiang Xian', 'Jiaxiang Xian', ',16,177,1803,', '山东省-济宁市-嘉祥县', '177', '02', '04', '01', '1803', '', null, null);
INSERT INTO t_sys_area VALUES ('1804', '370830', '370830', '370830', '汶上县', 'Wenshang Xian', 'Wenshang Xian', ',16,177,1804,', '山东省-济宁市-汶上县', '177', '02', '04', '01', '1804', '', null, null);
INSERT INTO t_sys_area VALUES ('1805', '370831', '370831', '370831', '泗水县', 'Sishui Xian', 'Sishui Xian', ',16,177,1805,', '山东省-济宁市-泗水县', '177', '02', '04', '01', '1805', '', null, null);
INSERT INTO t_sys_area VALUES ('1806', '370832', '370832', '370832', '梁山县', 'Liangshan Xian', 'Liangshan Xian', ',16,177,1806,', '山东省-济宁市-梁山县', '177', '02', '04', '01', '1806', '', null, null);
INSERT INTO t_sys_area VALUES ('1807', '370881', '370881', '370881', '曲阜市', 'Qufu Shi', 'Qufu Shi', ',16,177,1807,', '山东省-济宁市-曲阜市', '177', '02', '04', '01', '1807', '', null, null);
INSERT INTO t_sys_area VALUES ('1808', '370882', '370882', '370882', '兖州市', 'Yanzhou Shi', 'Yanzhou Shi', ',16,177,1808,', '山东省-济宁市-兖州市', '177', '02', '04', '01', '1808', '', null, null);
INSERT INTO t_sys_area VALUES ('1809', '370883', '370883', '370883', '邹城市', 'Zoucheng Shi', 'Zoucheng Shi', ',16,177,1809,', '山东省-济宁市-邹城市', '177', '02', '04', '01', '1809', '', null, null);
INSERT INTO t_sys_area VALUES ('1810', '370901', '370901', '370901', '市辖区', 'Shixiaqu', 'Shixiaqu', ',16,178,1810,', '山东省-泰安市-市辖区', '178', '02', '04', '01', '1810', '', null, null);
INSERT INTO t_sys_area VALUES ('1811', '370902', '370902', '370902', '泰山区', 'Taishan Qu', 'Taishan Qu', ',16,178,1811,', '山东省-泰安市-泰山区', '178', '02', '04', '01', '1811', '', null, null);
INSERT INTO t_sys_area VALUES ('1812', '370911', '370911', '370911', '岱岳区', 'Daiyue Qu', 'Daiyue Qu', ',16,178,1812,', '山东省-泰安市-岱岳区', '178', '02', '04', '01', '1812', '', null, null);
INSERT INTO t_sys_area VALUES ('1813', '370921', '370921', '370921', '宁阳县', 'Ningyang Xian', 'Ningyang Xian', ',16,178,1813,', '山东省-泰安市-宁阳县', '178', '02', '04', '01', '1813', '', null, null);
INSERT INTO t_sys_area VALUES ('1814', '370923', '370923', '370923', '东平县', 'Dongping Xian', 'Dongping Xian', ',16,178,1814,', '山东省-泰安市-东平县', '178', '02', '04', '01', '1814', '', null, null);
INSERT INTO t_sys_area VALUES ('1815', '370982', '370982', '370982', '新泰市', 'Xintai Shi', 'Xintai Shi', ',16,178,1815,', '山东省-泰安市-新泰市', '178', '02', '04', '01', '1815', '', null, null);
INSERT INTO t_sys_area VALUES ('1816', '370983', '370983', '370983', '肥城市', 'Feicheng Shi', 'Feicheng Shi', ',16,178,1816,', '山东省-泰安市-肥城市', '178', '02', '04', '01', '1816', '', null, null);
INSERT INTO t_sys_area VALUES ('1817', '371001', '371001', '371001', '市辖区', 'Shixiaqu', 'Shixiaqu', ',16,179,1817,', '山东省-威海市-市辖区', '179', '02', '04', '01', '1817', '', null, null);
INSERT INTO t_sys_area VALUES ('1818', '371002', '371002', '371002', '环翠区', 'Huancui Qu', 'Huancui Qu', ',16,179,1818,', '山东省-威海市-环翠区', '179', '02', '04', '01', '1818', '', null, null);
INSERT INTO t_sys_area VALUES ('1819', '371081', '371081', '371081', '文登市', 'Wendeng Shi', 'Wendeng Shi', ',16,179,1819,', '山东省-威海市-文登市', '179', '02', '04', '01', '1819', '', null, null);
INSERT INTO t_sys_area VALUES ('1820', '371082', '371082', '371082', '荣成市', 'Rongcheng Shi', 'Rongcheng Shi', ',16,179,1820,', '山东省-威海市-荣成市', '179', '02', '04', '01', '1820', '', null, null);
INSERT INTO t_sys_area VALUES ('1821', '371083', '371083', '371083', '乳山市', 'Rushan Shi', 'Rushan Shi', ',16,179,1821,', '山东省-威海市-乳山市', '179', '02', '04', '01', '1821', '', null, null);
INSERT INTO t_sys_area VALUES ('1822', '371101', '371101', '371101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',16,180,1822,', '山东省-日照市-市辖区', '180', '02', '04', '01', '1822', '', null, null);
INSERT INTO t_sys_area VALUES ('1823', '371102', '371102', '371102', '东港区', 'Donggang Qu', 'Donggang Qu', ',16,180,1823,', '山东省-日照市-东港区', '180', '02', '04', '01', '1823', '', null, null);
INSERT INTO t_sys_area VALUES ('1824', '371103', '371103', '371103', '岚山区', 'Lanshan Qu', 'Lanshan Qu', ',16,180,1824,', '山东省-日照市-岚山区', '180', '02', '04', '01', '1824', '', null, null);
INSERT INTO t_sys_area VALUES ('1825', '371121', '371121', '371121', '五莲县', 'Wulian Xian', 'Wulian Xian', ',16,180,1825,', '山东省-日照市-五莲县', '180', '02', '04', '01', '1825', '', null, null);
INSERT INTO t_sys_area VALUES ('1826', '371122', '371122', '371122', '莒县', 'Ju Xian', 'Ju Xian', ',16,180,1826,', '山东省-日照市-莒县', '180', '02', '04', '01', '1826', '', null, null);
INSERT INTO t_sys_area VALUES ('1827', '371201', '371201', '371201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',16,181,1827,', '山东省-莱芜市-市辖区', '181', '02', '04', '01', '1827', '', null, null);
INSERT INTO t_sys_area VALUES ('1828', '371202', '371202', '371202', '莱城区', 'Laicheng Qu', 'Laicheng Qu', ',16,181,1828,', '山东省-莱芜市-莱城区', '181', '02', '04', '01', '1828', '', null, null);
INSERT INTO t_sys_area VALUES ('1829', '371203', '371203', '371203', '钢城区', 'Gangcheng Qu', 'Gangcheng Qu', ',16,181,1829,', '山东省-莱芜市-钢城区', '181', '02', '04', '01', '1829', '', null, null);
INSERT INTO t_sys_area VALUES ('1830', '371301', '371301', '371301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',16,182,1830,', '山东省-临沂市-市辖区', '182', '02', '04', '01', '1830', '', null, null);
INSERT INTO t_sys_area VALUES ('1831', '371302', '371302', '371302', '兰山区', 'Lanshan Qu', 'Lanshan Qu', ',16,182,1831,', '山东省-临沂市-兰山区', '182', '02', '04', '01', '1831', '', null, null);
INSERT INTO t_sys_area VALUES ('1832', '371311', '371311', '371311', '罗庄区', 'Luozhuang Qu', 'Luozhuang Qu', ',16,182,1832,', '山东省-临沂市-罗庄区', '182', '02', '04', '01', '1832', '', null, null);
INSERT INTO t_sys_area VALUES ('1833', '371301', '371301', '371301', '河东区', 'Hedong Qu', 'Hedong Qu', ',16,182,1833,', '山东省-临沂市-河东区', '182', '02', '04', '01', '1833', '', null, null);
INSERT INTO t_sys_area VALUES ('1834', '371321', '371321', '371321', '沂南县', 'Yinan Xian', 'Yinan Xian', ',16,182,1834,', '山东省-临沂市-沂南县', '182', '02', '04', '01', '1834', '', null, null);
INSERT INTO t_sys_area VALUES ('1835', '371322', '371322', '371322', '郯城县', 'Tancheng Xian', 'Tancheng Xian', ',16,182,1835,', '山东省-临沂市-郯城县', '182', '02', '04', '01', '1835', '', null, null);
INSERT INTO t_sys_area VALUES ('1836', '371323', '371323', '371323', '沂水县', 'Yishui Xian', 'Yishui Xian', ',16,182,1836,', '山东省-临沂市-沂水县', '182', '02', '04', '01', '1836', '', null, null);
INSERT INTO t_sys_area VALUES ('1837', '371324', '371324', '371324', '苍山县', 'Cangshan Xian', 'Cangshan Xian', ',16,182,1837,', '山东省-临沂市-苍山县', '182', '02', '04', '01', '1837', '', null, null);
INSERT INTO t_sys_area VALUES ('1838', '371325', '371325', '371325', '费县', 'Fei Xian', 'Fei Xian', ',16,182,1838,', '山东省-临沂市-费县', '182', '02', '04', '01', '1838', '', null, null);
INSERT INTO t_sys_area VALUES ('1839', '371326', '371326', '371326', '平邑县', 'Pingyi Xian', 'Pingyi Xian', ',16,182,1839,', '山东省-临沂市-平邑县', '182', '02', '04', '01', '1839', '', null, null);
INSERT INTO t_sys_area VALUES ('1840', '371327', '371327', '371327', '莒南县', 'Junan Xian', 'Junan Xian', ',16,182,1840,', '山东省-临沂市-莒南县', '182', '02', '04', '01', '1840', '', null, null);
INSERT INTO t_sys_area VALUES ('1841', '371328', '371328', '371328', '蒙阴县', 'Mengyin Xian', 'Mengyin Xian', ',16,182,1841,', '山东省-临沂市-蒙阴县', '182', '02', '04', '01', '1841', '', null, null);
INSERT INTO t_sys_area VALUES ('1842', '371329', '371329', '371329', '临沭县', 'Linshu Xian', 'Linshu Xian', ',16,182,1842,', '山东省-临沂市-临沭县', '182', '02', '04', '01', '1842', '', null, null);
INSERT INTO t_sys_area VALUES ('1843', '371401', '371401', '371401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',16,183,1843,', '山东省-德州市-市辖区', '183', '02', '04', '01', '1843', '', null, null);
INSERT INTO t_sys_area VALUES ('1844', '371402', '371402', '371402', '德城区', 'Decheng Qu', 'Decheng Qu', ',16,183,1844,', '山东省-德州市-德城区', '183', '02', '04', '01', '1844', '', null, null);
INSERT INTO t_sys_area VALUES ('1845', '371421', '371421', '371421', '陵县', 'Ling Xian', 'Ling Xian', ',16,183,1845,', '山东省-德州市-陵县', '183', '02', '04', '01', '1845', '', null, null);
INSERT INTO t_sys_area VALUES ('1846', '371422', '371422', '371422', '宁津县', 'Ningjin Xian', 'Ningjin Xian', ',16,183,1846,', '山东省-德州市-宁津县', '183', '02', '04', '01', '1846', '', null, null);
INSERT INTO t_sys_area VALUES ('1847', '371423', '371423', '371423', '庆云县', 'Qingyun Xian', 'Qingyun Xian', ',16,183,1847,', '山东省-德州市-庆云县', '183', '02', '04', '01', '1847', '', null, null);
INSERT INTO t_sys_area VALUES ('1848', '371424', '371424', '371424', '临邑县', 'Linyi xian', 'Linyi xian', ',16,183,1848,', '山东省-德州市-临邑县', '183', '02', '04', '01', '1848', '', null, null);
INSERT INTO t_sys_area VALUES ('1849', '371425', '371425', '371425', '齐河县', 'Qihe Xian', 'Qihe Xian', ',16,183,1849,', '山东省-德州市-齐河县', '183', '02', '04', '01', '1849', '', null, null);
INSERT INTO t_sys_area VALUES ('1850', '371426', '371426', '371426', '平原县', 'Pingyuan Xian', 'Pingyuan Xian', ',16,183,1850,', '山东省-德州市-平原县', '183', '02', '04', '01', '1850', '', null, null);
INSERT INTO t_sys_area VALUES ('1851', '371427', '371427', '371427', '夏津县', 'Xiajin Xian', 'Xiajin Xian', ',16,183,1851,', '山东省-德州市-夏津县', '183', '02', '04', '01', '1851', '', null, null);
INSERT INTO t_sys_area VALUES ('1852', '371428', '371428', '371428', '武城县', 'Wucheng Xian', 'Wucheng Xian', ',16,183,1852,', '山东省-德州市-武城县', '183', '02', '04', '01', '1852', '', null, null);
INSERT INTO t_sys_area VALUES ('1853', '371481', '371481', '371481', '乐陵市', 'Leling Shi', 'Leling Shi', ',16,183,1853,', '山东省-德州市-乐陵市', '183', '02', '04', '01', '1853', '', null, null);
INSERT INTO t_sys_area VALUES ('1854', '371482', '371482', '371482', '禹城市', 'Yucheng Shi', 'Yucheng Shi', ',16,183,1854,', '山东省-德州市-禹城市', '183', '02', '04', '01', '1854', '', null, null);
INSERT INTO t_sys_area VALUES ('1855', '371501', '371501', '371501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',16,184,1855,', '山东省-聊城市-市辖区', '184', '02', '04', '01', '1855', '', null, null);
INSERT INTO t_sys_area VALUES ('1856', '371502', '371502', '371502', '东昌府区', 'Dongchangfu Qu', 'Dongchangfu Qu', ',16,184,1856,', '山东省-聊城市-东昌府区', '184', '02', '04', '01', '1856', '', null, null);
INSERT INTO t_sys_area VALUES ('1857', '371521', '371521', '371521', '阳谷县', 'Yanggu Xian ', 'Yanggu Xian ', ',16,184,1857,', '山东省-聊城市-阳谷县', '184', '02', '04', '01', '1857', '', null, null);
INSERT INTO t_sys_area VALUES ('1858', '371522', '371522', '371522', '莘县', 'Shen Xian', 'Shen Xian', ',16,184,1858,', '山东省-聊城市-莘县', '184', '02', '04', '01', '1858', '', null, null);
INSERT INTO t_sys_area VALUES ('1859', '371523', '371523', '371523', '茌平县', 'Chiping Xian ', 'Chiping Xian ', ',16,184,1859,', '山东省-聊城市-茌平县', '184', '02', '04', '01', '1859', '', null, null);
INSERT INTO t_sys_area VALUES ('1860', '371524', '371524', '371524', '东阿县', 'Dong,e Xian', 'Dong,e Xian', ',16,184,1860,', '山东省-聊城市-东阿县', '184', '02', '04', '01', '1860', '', null, null);
INSERT INTO t_sys_area VALUES ('1861', '371525', '371525', '371525', '冠县', 'Guan Xian', 'Guan Xian', ',16,184,1861,', '山东省-聊城市-冠县', '184', '02', '04', '01', '1861', '', null, null);
INSERT INTO t_sys_area VALUES ('1862', '371526', '371526', '371526', '高唐县', 'Gaotang Xian', 'Gaotang Xian', ',16,184,1862,', '山东省-聊城市-高唐县', '184', '02', '04', '01', '1862', '', null, null);
INSERT INTO t_sys_area VALUES ('1863', '371581', '371581', '371581', '临清市', 'Linqing Xian', 'Linqing Xian', ',16,184,1863,', '山东省-聊城市-临清市', '184', '02', '04', '01', '1863', '', null, null);
INSERT INTO t_sys_area VALUES ('1864', '371601', '371601', '371601', '市辖区', '1', '1', ',16,185,1864,', '山东省-滨州市-市辖区', '185', '02', '04', '01', '1864', '', null, null);
INSERT INTO t_sys_area VALUES ('1865', '371602', '371602', '371602', '滨城区', 'Bincheng Qu', 'Bincheng Qu', ',16,185,1865,', '山东省-滨州市-滨城区', '185', '02', '04', '01', '1865', '', null, null);
INSERT INTO t_sys_area VALUES ('1866', '371621', '371621', '371621', '惠民县', 'Huimin Xian', 'Huimin Xian', ',16,185,1866,', '山东省-滨州市-惠民县', '185', '02', '04', '01', '1866', '', null, null);
INSERT INTO t_sys_area VALUES ('1867', '371622', '371622', '371622', '阳信县', 'Yangxin Xian', 'Yangxin Xian', ',16,185,1867,', '山东省-滨州市-阳信县', '185', '02', '04', '01', '1867', '', null, null);
INSERT INTO t_sys_area VALUES ('1868', '371623', '371623', '371623', '无棣县', 'Wudi Xian', 'Wudi Xian', ',16,185,1868,', '山东省-滨州市-无棣县', '185', '02', '04', '01', '1868', '', null, null);
INSERT INTO t_sys_area VALUES ('1869', '371624', '371624', '371624', '沾化县', 'Zhanhua Xian', 'Zhanhua Xian', ',16,185,1869,', '山东省-滨州市-沾化县', '185', '02', '04', '01', '1869', '', null, null);
INSERT INTO t_sys_area VALUES ('1870', '371625', '371625', '371625', '博兴县', 'Boxing Xian', 'Boxing Xian', ',16,185,1870,', '山东省-滨州市-博兴县', '185', '02', '04', '01', '1870', '', null, null);
INSERT INTO t_sys_area VALUES ('1871', '371626', '371626', '371626', '邹平县', 'Zouping Xian', 'Zouping Xian', ',16,185,1871,', '山东省-滨州市-邹平县', '185', '02', '04', '01', '1871', '', null, null);
INSERT INTO t_sys_area VALUES ('1873', '371702', '371702', '371702', '牡丹区', 'Mudan Qu', 'Mudan Qu', ',16,186,1873,', '山东省-菏泽市-牡丹区', '186', '02', '04', '01', '1873', '', null, null);
INSERT INTO t_sys_area VALUES ('1874', '371721', '371721', '371721', '曹县', 'Cao Xian', 'Cao Xian', ',16,186,1874,', '山东省-菏泽市-曹县', '186', '02', '04', '01', '1874', '', null, null);
INSERT INTO t_sys_area VALUES ('1875', '371722', '371722', '371722', '单县', 'Shan Xian', 'Shan Xian', ',16,186,1875,', '山东省-菏泽市-单县', '186', '02', '04', '01', '1875', '', null, null);
INSERT INTO t_sys_area VALUES ('1876', '371723', '371723', '371723', '成武县', 'Chengwu Xian', 'Chengwu Xian', ',16,186,1876,', '山东省-菏泽市-成武县', '186', '02', '04', '01', '1876', '', null, null);
INSERT INTO t_sys_area VALUES ('1877', '371724', '371724', '371724', '巨野县', 'Juye Xian', 'Juye Xian', ',16,186,1877,', '山东省-菏泽市-巨野县', '186', '02', '04', '01', '1877', '', null, null);
INSERT INTO t_sys_area VALUES ('1878', '371725', '371725', '371725', '郓城县', 'Yuncheng Xian', 'Yuncheng Xian', ',16,186,1878,', '山东省-菏泽市-郓城县', '186', '02', '04', '01', '1878', '', null, null);
INSERT INTO t_sys_area VALUES ('1879', '371726', '371726', '371726', '鄄城县', 'Juancheng Xian', 'Juancheng Xian', ',16,186,1879,', '山东省-菏泽市-鄄城县', '186', '02', '04', '01', '1879', '', null, null);
INSERT INTO t_sys_area VALUES ('1880', '371727', '371727', '371727', '定陶县', 'Dingtao Xian', 'Dingtao Xian', ',16,186,1880,', '山东省-菏泽市-定陶县', '186', '02', '04', '01', '1880', '', null, null);
INSERT INTO t_sys_area VALUES ('1881', '371728', '371728', '371728', '东明县', 'Dongming Xian', 'Dongming Xian', ',16,186,1881,', '山东省-菏泽市-东明县', '186', '02', '04', '01', '1881', '', null, null);
INSERT INTO t_sys_area VALUES ('1882', '410101', '410101', '410101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',17,187,1882,', '河南省-郑州市-市辖区', '187', '02', '04', '01', '1882', '', null, null);
INSERT INTO t_sys_area VALUES ('1883', '410102', '410102', '410102', '中原区', 'Zhongyuan Qu', 'Zhongyuan Qu', ',17,187,1883,', '河南省-郑州市-中原区', '187', '02', '04', '01', '1883', '', null, null);
INSERT INTO t_sys_area VALUES ('1884', '410103', '410103', '410103', '二七区', 'Erqi Qu', 'Erqi Qu', ',17,187,1884,', '河南省-郑州市-二七区', '187', '02', '04', '01', '1884', '', null, null);
INSERT INTO t_sys_area VALUES ('1885', '410104', '410104', '410104', '管城回族区', 'Guancheng Huizu Qu', 'Guancheng Huizu Qu', ',17,187,1885,', '河南省-郑州市-管城回族区', '187', '02', '04', '01', '1885', '', null, null);
INSERT INTO t_sys_area VALUES ('1886', '410105', '410105', '410105', '金水区', 'Jinshui Qu', 'Jinshui Qu', ',17,187,1886,', '河南省-郑州市-金水区', '187', '02', '04', '01', '1886', '', null, null);
INSERT INTO t_sys_area VALUES ('1887', '410106', '410106', '410106', '上街区', 'Shangjie Qu', 'Shangjie Qu', ',17,187,1887,', '河南省-郑州市-上街区', '187', '02', '04', '01', '1887', '', null, null);
INSERT INTO t_sys_area VALUES ('1888', '410108', '410108', '410108', '惠济区', 'Mangshan Qu', 'Mangshan Qu', ',17,187,1888,', '河南省-郑州市-惠济区', '187', '02', '04', '01', '1888', '', null, null);
INSERT INTO t_sys_area VALUES ('1889', '410122', '410122', '410122', '中牟县', 'Zhongmou Xian', 'Zhongmou Xian', ',17,187,1889,', '河南省-郑州市-中牟县', '187', '02', '04', '01', '1889', '', null, null);
INSERT INTO t_sys_area VALUES ('1890', '410181', '410181', '410181', '巩义市', 'Gongyi Shi', 'Gongyi Shi', ',17,187,1890,', '河南省-郑州市-巩义市', '187', '02', '04', '01', '1890', '', null, null);
INSERT INTO t_sys_area VALUES ('1891', '410182', '410182', '410182', '荥阳市', 'Xingyang Shi', 'Xingyang Shi', ',17,187,1891,', '河南省-郑州市-荥阳市', '187', '02', '04', '01', '1891', '', null, null);
INSERT INTO t_sys_area VALUES ('1892', '410183', '410183', '410183', '新密市', 'Xinmi Shi', 'Xinmi Shi', ',17,187,1892,', '河南省-郑州市-新密市', '187', '02', '04', '01', '1892', '', null, null);
INSERT INTO t_sys_area VALUES ('1893', '410184', '410184', '410184', '新郑市', 'Xinzheng Shi', 'Xinzheng Shi', ',17,187,1893,', '河南省-郑州市-新郑市', '187', '02', '04', '01', '1893', '', null, null);
INSERT INTO t_sys_area VALUES ('1894', '410185', '410185', '410185', '登封市', 'Dengfeng Shi', 'Dengfeng Shi', ',17,187,1894,', '河南省-郑州市-登封市', '187', '02', '04', '01', '1894', '', null, null);
INSERT INTO t_sys_area VALUES ('1895', '410201', '410201', '410201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',17,188,1895,', '河南省-开封市-市辖区', '188', '02', '04', '01', '1895', '', null, null);
INSERT INTO t_sys_area VALUES ('1896', '410202', '410202', '410202', '龙亭区', 'Longting Qu', 'Longting Qu', ',17,188,1896,', '河南省-开封市-龙亭区', '188', '02', '04', '01', '1896', '', null, null);
INSERT INTO t_sys_area VALUES ('1897', '410203', '410203', '410203', '顺河回族区', 'Shunhe Huizu Qu', 'Shunhe Huizu Qu', ',17,188,1897,', '河南省-开封市-顺河回族区', '188', '02', '04', '01', '1897', '', null, null);
INSERT INTO t_sys_area VALUES ('1898', '410204', '410204', '410204', '鼓楼区', 'Gulou Qu', 'Gulou Qu', ',17,188,1898,', '河南省-开封市-鼓楼区', '188', '02', '04', '01', '1898', '', null, null);
INSERT INTO t_sys_area VALUES ('1899', '410205', '410205', '410205', '禹王台区', 'Yuwangtai Qu', 'Yuwangtai Qu', ',17,188,1899,', '河南省-开封市-禹王台区', '188', '02', '04', '01', '1899', '', null, null);
INSERT INTO t_sys_area VALUES ('1900', '410211', '410211', '410211', '金明区', 'Jinming Qu', 'Jinming Qu', ',17,188,1900,', '河南省-开封市-金明区', '188', '02', '04', '01', '1900', '', null, null);
INSERT INTO t_sys_area VALUES ('1901', '410221', '410221', '410221', '杞县', 'Qi Xian', 'Qi Xian', ',17,188,1901,', '河南省-开封市-杞县', '188', '02', '04', '01', '1901', '', null, null);
INSERT INTO t_sys_area VALUES ('1902', '410222', '410222', '410222', '通许县', 'Tongxu Xian', 'Tongxu Xian', ',17,188,1902,', '河南省-开封市-通许县', '188', '02', '04', '01', '1902', '', null, null);
INSERT INTO t_sys_area VALUES ('1903', '410223', '410223', '410223', '尉氏县', 'Weishi Xian', 'Weishi Xian', ',17,188,1903,', '河南省-开封市-尉氏县', '188', '02', '04', '01', '1903', '', null, null);
INSERT INTO t_sys_area VALUES ('1904', '410224', '410224', '410224', '开封县', 'Kaifeng Xian', 'Kaifeng Xian', ',17,188,1904,', '河南省-开封市-开封县', '188', '02', '04', '01', '1904', '', null, null);
INSERT INTO t_sys_area VALUES ('1905', '410225', '410225', '410225', '兰考县', 'Lankao Xian', 'Lankao Xian', ',17,188,1905,', '河南省-开封市-兰考县', '188', '02', '04', '01', '1905', '', null, null);
INSERT INTO t_sys_area VALUES ('1906', '410301', '410301', '410301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',17,189,1906,', '河南省-洛阳市-市辖区', '189', '02', '04', '01', '1906', '', null, null);
INSERT INTO t_sys_area VALUES ('1907', '410302', '410302', '410302', '老城区', 'Laocheng Qu', 'Laocheng Qu', ',17,189,1907,', '河南省-洛阳市-老城区', '189', '02', '04', '01', '1907', '', null, null);
INSERT INTO t_sys_area VALUES ('1908', '410303', '410303', '410303', '西工区', 'Xigong Qu', 'Xigong Qu', ',17,189,1908,', '河南省-洛阳市-西工区', '189', '02', '04', '01', '1908', '', null, null);
INSERT INTO t_sys_area VALUES ('1909', '410304', '410304', '410304', '瀍河回族区', 'Chanhehuizu Qu', 'Chanhehuizu Qu', ',17,189,1909,', '河南省-洛阳市-瀍河回族区', '189', '02', '04', '01', '1909', '', null, null);
INSERT INTO t_sys_area VALUES ('1910', '410305', '410305', '410305', '涧西区', 'Jianxi Qu', 'Jianxi Qu', ',17,189,1910,', '河南省-洛阳市-涧西区', '189', '02', '04', '01', '1910', '', null, null);
INSERT INTO t_sys_area VALUES ('1911', '410306', '410306', '410306', '吉利区', 'Jili Qu', 'Jili Qu', ',17,189,1911,', '河南省-洛阳市-吉利区', '189', '02', '04', '01', '1911', '', null, null);
INSERT INTO t_sys_area VALUES ('1912', '410311', '410311', '410311', '洛龙区', 'Luolong Qu', 'Luolong Qu', ',17,189,1912,', '河南省-洛阳市-洛龙区', '189', '02', '04', '01', '1912', '', null, null);
INSERT INTO t_sys_area VALUES ('1913', '410322', '410322', '410322', '孟津县', 'Mengjin Xian', 'Mengjin Xian', ',17,189,1913,', '河南省-洛阳市-孟津县', '189', '02', '04', '01', '1913', '', null, null);
INSERT INTO t_sys_area VALUES ('1914', '410323', '410323', '410323', '新安县', 'Xin,an Xian', 'Xin,an Xian', ',17,189,1914,', '河南省-洛阳市-新安县', '189', '02', '04', '01', '1914', '', null, null);
INSERT INTO t_sys_area VALUES ('1915', '410324', '410324', '410324', '栾川县', 'Luanchuan Xian', 'Luanchuan Xian', ',17,189,1915,', '河南省-洛阳市-栾川县', '189', '02', '04', '01', '1915', '', null, null);
INSERT INTO t_sys_area VALUES ('1916', '410325', '410325', '410325', '嵩县', 'Song Xian', 'Song Xian', ',17,189,1916,', '河南省-洛阳市-嵩县', '189', '02', '04', '01', '1916', '', null, null);
INSERT INTO t_sys_area VALUES ('1917', '410326', '410326', '410326', '汝阳县', 'Ruyang Xian', 'Ruyang Xian', ',17,189,1917,', '河南省-洛阳市-汝阳县', '189', '02', '04', '01', '1917', '', null, null);
INSERT INTO t_sys_area VALUES ('1918', '410327', '410327', '410327', '宜阳县', 'Yiyang Xian', 'Yiyang Xian', ',17,189,1918,', '河南省-洛阳市-宜阳县', '189', '02', '04', '01', '1918', '', null, null);
INSERT INTO t_sys_area VALUES ('1919', '410328', '410328', '410328', '洛宁县', 'Luoning Xian', 'Luoning Xian', ',17,189,1919,', '河南省-洛阳市-洛宁县', '189', '02', '04', '01', '1919', '', null, null);
INSERT INTO t_sys_area VALUES ('1920', '410329', '410329', '410329', '伊川县', 'Yichuan Xian', 'Yichuan Xian', ',17,189,1920,', '河南省-洛阳市-伊川县', '189', '02', '04', '01', '1920', '', null, null);
INSERT INTO t_sys_area VALUES ('1921', '410381', '410381', '410381', '偃师市', 'Yanshi Shi', 'Yanshi Shi', ',17,189,1921,', '河南省-洛阳市-偃师市', '189', '02', '04', '01', '1921', '', null, null);
INSERT INTO t_sys_area VALUES ('1922', '410401', '410401', '410401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',17,190,1922,', '河南省-平顶山市-市辖区', '190', '02', '04', '01', '1922', '', null, null);
INSERT INTO t_sys_area VALUES ('1923', '410402', '410402', '410402', '新华区', 'Xinhua Qu', 'Xinhua Qu', ',17,190,1923,', '河南省-平顶山市-新华区', '190', '02', '04', '01', '1923', '', null, null);
INSERT INTO t_sys_area VALUES ('1924', '410403', '410403', '410403', '卫东区', 'Weidong Qu', 'Weidong Qu', ',17,190,1924,', '河南省-平顶山市-卫东区', '190', '02', '04', '01', '1924', '', null, null);
INSERT INTO t_sys_area VALUES ('1925', '410404', '410404', '410404', '石龙区', 'Shilong Qu', 'Shilong Qu', ',17,190,1925,', '河南省-平顶山市-石龙区', '190', '02', '04', '01', '1925', '', null, null);
INSERT INTO t_sys_area VALUES ('1926', '410411', '410411', '410411', '湛河区', 'Zhanhe Qu', 'Zhanhe Qu', ',17,190,1926,', '河南省-平顶山市-湛河区', '190', '02', '04', '01', '1926', '', null, null);
INSERT INTO t_sys_area VALUES ('1927', '410421', '410421', '410421', '宝丰县', 'Baofeng Xian', 'Baofeng Xian', ',17,190,1927,', '河南省-平顶山市-宝丰县', '190', '02', '04', '01', '1927', '', null, null);
INSERT INTO t_sys_area VALUES ('1928', '410422', '410422', '410422', '叶县', 'Ye Xian', 'Ye Xian', ',17,190,1928,', '河南省-平顶山市-叶县', '190', '02', '04', '01', '1928', '', null, null);
INSERT INTO t_sys_area VALUES ('1929', '410423', '410423', '410423', '鲁山县', 'Lushan Xian', 'Lushan Xian', ',17,190,1929,', '河南省-平顶山市-鲁山县', '190', '02', '04', '01', '1929', '', null, null);
INSERT INTO t_sys_area VALUES ('1930', '410425', '410425', '410425', '郏县', 'Jia Xian', 'Jia Xian', ',17,190,1930,', '河南省-平顶山市-郏县', '190', '02', '04', '01', '1930', '', null, null);
INSERT INTO t_sys_area VALUES ('1931', '410481', '410481', '410481', '舞钢市', 'Wugang Shi', 'Wugang Shi', ',17,190,1931,', '河南省-平顶山市-舞钢市', '190', '02', '04', '01', '1931', '', null, null);
INSERT INTO t_sys_area VALUES ('1932', '410482', '410482', '410482', '汝州市', 'Ruzhou Shi', 'Ruzhou Shi', ',17,190,1932,', '河南省-平顶山市-汝州市', '190', '02', '04', '01', '1932', '', null, null);
INSERT INTO t_sys_area VALUES ('1933', '410501', '410501', '410501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',17,191,1933,', '河南省-安阳市-市辖区', '191', '02', '04', '01', '1933', '', null, null);
INSERT INTO t_sys_area VALUES ('1934', '410502', '410502', '410502', '文峰区', 'Wenfeng Qu', 'Wenfeng Qu', ',17,191,1934,', '河南省-安阳市-文峰区', '191', '02', '04', '01', '1934', '', null, null);
INSERT INTO t_sys_area VALUES ('1935', '410503', '410503', '410503', '北关区', 'Beiguan Qu', 'Beiguan Qu', ',17,191,1935,', '河南省-安阳市-北关区', '191', '02', '04', '01', '1935', '', null, null);
INSERT INTO t_sys_area VALUES ('1936', '410505', '410505', '410505', '殷都区', 'Yindu Qu', 'Yindu Qu', ',17,191,1936,', '河南省-安阳市-殷都区', '191', '02', '04', '01', '1936', '', null, null);
INSERT INTO t_sys_area VALUES ('1937', '410506', '410506', '410506', '龙安区', 'Longan Qu', 'Longan Qu', ',17,191,1937,', '河南省-安阳市-龙安区', '191', '02', '04', '01', '1937', '', null, null);
INSERT INTO t_sys_area VALUES ('1938', '410522', '410522', '410522', '安阳县', 'Anyang Xian', 'Anyang Xian', ',17,191,1938,', '河南省-安阳市-安阳县', '191', '02', '04', '01', '1938', '', null, null);
INSERT INTO t_sys_area VALUES ('1939', '410523', '410523', '410523', '汤阴县', 'Tangyin Xian', 'Tangyin Xian', ',17,191,1939,', '河南省-安阳市-汤阴县', '191', '02', '04', '01', '1939', '', null, null);
INSERT INTO t_sys_area VALUES ('1940', '410526', '410526', '410526', '滑县', 'Hua Xian', 'Hua Xian', ',17,191,1940,', '河南省-安阳市-滑县', '191', '02', '04', '01', '1940', '', null, null);
INSERT INTO t_sys_area VALUES ('1941', '410527', '410527', '410527', '内黄县', 'Neihuang Xian', 'Neihuang Xian', ',17,191,1941,', '河南省-安阳市-内黄县', '191', '02', '04', '01', '1941', '', null, null);
INSERT INTO t_sys_area VALUES ('1942', '410581', '410581', '410581', '林州市', 'Linzhou Shi', 'Linzhou Shi', ',17,191,1942,', '河南省-安阳市-林州市', '191', '02', '04', '01', '1942', '', null, null);
INSERT INTO t_sys_area VALUES ('1943', '410601', '410601', '410601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',17,192,1943,', '河南省-鹤壁市-市辖区', '192', '02', '04', '01', '1943', '', null, null);
INSERT INTO t_sys_area VALUES ('1944', '410602', '410602', '410602', '鹤山区', 'Heshan Qu', 'Heshan Qu', ',17,192,1944,', '河南省-鹤壁市-鹤山区', '192', '02', '04', '01', '1944', '', null, null);
INSERT INTO t_sys_area VALUES ('1945', '410603', '410603', '410603', '山城区', 'Shancheng Qu', 'Shancheng Qu', ',17,192,1945,', '河南省-鹤壁市-山城区', '192', '02', '04', '01', '1945', '', null, null);
INSERT INTO t_sys_area VALUES ('1946', '410611', '410611', '410611', '淇滨区', 'Qibin Qu', 'Qibin Qu', ',17,192,1946,', '河南省-鹤壁市-淇滨区', '192', '02', '04', '01', '1946', '', null, null);
INSERT INTO t_sys_area VALUES ('1947', '410621', '410621', '410621', '浚县', 'Xun Xian', 'Xun Xian', ',17,192,1947,', '河南省-鹤壁市-浚县', '192', '02', '04', '01', '1947', '', null, null);
INSERT INTO t_sys_area VALUES ('1948', '410622', '410622', '410622', '淇县', 'Qi Xian', 'Qi Xian', ',17,192,1948,', '河南省-鹤壁市-淇县', '192', '02', '04', '01', '1948', '', null, null);
INSERT INTO t_sys_area VALUES ('1949', '410701', '410701', '410701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',17,193,1949,', '河南省-新乡市-市辖区', '193', '02', '04', '01', '1949', '', null, null);
INSERT INTO t_sys_area VALUES ('1950', '410702', '410702', '410702', '红旗区', 'Hongqi Qu', 'Hongqi Qu', ',17,193,1950,', '河南省-新乡市-红旗区', '193', '02', '04', '01', '1950', '', null, null);
INSERT INTO t_sys_area VALUES ('1951', '410703', '410703', '410703', '卫滨区', 'Weibin Qu', 'Weibin Qu', ',17,193,1951,', '河南省-新乡市-卫滨区', '193', '02', '04', '01', '1951', '', null, null);
INSERT INTO t_sys_area VALUES ('1952', '410704', '410704', '410704', '凤泉区', 'Fengquan Qu', 'Fengquan Qu', ',17,193,1952,', '河南省-新乡市-凤泉区', '193', '02', '04', '01', '1952', '', null, null);
INSERT INTO t_sys_area VALUES ('1953', '410711', '410711', '410711', '牧野区', 'Muye Qu', 'Muye Qu', ',17,193,1953,', '河南省-新乡市-牧野区', '193', '02', '04', '01', '1953', '', null, null);
INSERT INTO t_sys_area VALUES ('1954', '410721', '410721', '410721', '新乡县', 'Xinxiang Xian', 'Xinxiang Xian', ',17,193,1954,', '河南省-新乡市-新乡县', '193', '02', '04', '01', '1954', '', null, null);
INSERT INTO t_sys_area VALUES ('1955', '410724', '410724', '410724', '获嘉县', 'Huojia Xian', 'Huojia Xian', ',17,193,1955,', '河南省-新乡市-获嘉县', '193', '02', '04', '01', '1955', '', null, null);
INSERT INTO t_sys_area VALUES ('1956', '410725', '410725', '410725', '原阳县', 'Yuanyang Xian', 'Yuanyang Xian', ',17,193,1956,', '河南省-新乡市-原阳县', '193', '02', '04', '01', '1956', '', null, null);
INSERT INTO t_sys_area VALUES ('1957', '410726', '410726', '410726', '延津县', 'Yanjin Xian', 'Yanjin Xian', ',17,193,1957,', '河南省-新乡市-延津县', '193', '02', '04', '01', '1957', '', null, null);
INSERT INTO t_sys_area VALUES ('1958', '410727', '410727', '410727', '封丘县', 'Fengqiu Xian', 'Fengqiu Xian', ',17,193,1958,', '河南省-新乡市-封丘县', '193', '02', '04', '01', '1958', '', null, null);
INSERT INTO t_sys_area VALUES ('1959', '410728', '410728', '410728', '长垣县', 'Changyuan Xian', 'Changyuan Xian', ',17,193,1959,', '河南省-新乡市-长垣县', '193', '02', '04', '01', '1959', '', null, null);
INSERT INTO t_sys_area VALUES ('1960', '410781', '410781', '410781', '卫辉市', 'Weihui Shi', 'Weihui Shi', ',17,193,1960,', '河南省-新乡市-卫辉市', '193', '02', '04', '01', '1960', '', null, null);
INSERT INTO t_sys_area VALUES ('1961', '410782', '410782', '410782', '辉县市', 'Huixian Shi', 'Huixian Shi', ',17,193,1961,', '河南省-新乡市-辉县市', '193', '02', '04', '01', '1961', '', null, null);
INSERT INTO t_sys_area VALUES ('1962', '410801', '410801', '410801', '市辖区', 'Shixiaqu', 'Shixiaqu', ',17,194,1962,', '河南省-焦作市-市辖区', '194', '02', '04', '01', '1962', '', null, null);
INSERT INTO t_sys_area VALUES ('1963', '410802', '410802', '410802', '解放区', 'Jiefang Qu', 'Jiefang Qu', ',17,194,1963,', '河南省-焦作市-解放区', '194', '02', '04', '01', '1963', '', null, null);
INSERT INTO t_sys_area VALUES ('1964', '410803', '410803', '410803', '中站区', 'Zhongzhan Qu', 'Zhongzhan Qu', ',17,194,1964,', '河南省-焦作市-中站区', '194', '02', '04', '01', '1964', '', null, null);
INSERT INTO t_sys_area VALUES ('1965', '410804', '410804', '410804', '马村区', 'Macun Qu', 'Macun Qu', ',17,194,1965,', '河南省-焦作市-马村区', '194', '02', '04', '01', '1965', '', null, null);
INSERT INTO t_sys_area VALUES ('1966', '410811', '410811', '410811', '山阳区', 'Shanyang Qu', 'Shanyang Qu', ',17,194,1966,', '河南省-焦作市-山阳区', '194', '02', '04', '01', '1966', '', null, null);
INSERT INTO t_sys_area VALUES ('1967', '410821', '410821', '410821', '修武县', 'Xiuwu Xian', 'Xiuwu Xian', ',17,194,1967,', '河南省-焦作市-修武县', '194', '02', '04', '01', '1967', '', null, null);
INSERT INTO t_sys_area VALUES ('1968', '410822', '410822', '410822', '博爱县', 'Bo,ai Xian', 'Bo,ai Xian', ',17,194,1968,', '河南省-焦作市-博爱县', '194', '02', '04', '01', '1968', '', null, null);
INSERT INTO t_sys_area VALUES ('1969', '410823', '410823', '410823', '武陟县', 'Wuzhi Xian', 'Wuzhi Xian', ',17,194,1969,', '河南省-焦作市-武陟县', '194', '02', '04', '01', '1969', '', null, null);
INSERT INTO t_sys_area VALUES ('1970', '410825', '410825', '410825', '温县', 'Wen Xian', 'Wen Xian', ',17,194,1970,', '河南省-焦作市-温县', '194', '02', '04', '01', '1970', '', null, null);
INSERT INTO t_sys_area VALUES ('1971', '419001', '419001', '419001', '济源市', 'Jiyuan Shi', 'Jiyuan Shi', ',17,194,1971,', '河南省-焦作市-济源市', '194', '02', '04', '01', '1971', '', null, null);
INSERT INTO t_sys_area VALUES ('1972', '410882', '410882', '410882', '沁阳市', 'Qinyang Shi', 'Qinyang Shi', ',17,194,1972,', '河南省-焦作市-沁阳市', '194', '02', '04', '01', '1972', '', null, null);
INSERT INTO t_sys_area VALUES ('1973', '410883', '410883', '410883', '孟州市', 'Mengzhou Shi', 'Mengzhou Shi', ',17,194,1973,', '河南省-焦作市-孟州市', '194', '02', '04', '01', '1973', '', null, null);
INSERT INTO t_sys_area VALUES ('1974', '410901', '410901', '410901', '市辖区', 'Shixiaqu', 'Shixiaqu', ',17,195,1974,', '河南省-濮阳市-市辖区', '195', '02', '04', '01', '1974', '', null, null);
INSERT INTO t_sys_area VALUES ('1975', '410902', '410902', '410902', '华龙区', 'Hualong Qu', 'Hualong Qu', ',17,195,1975,', '河南省-濮阳市-华龙区', '195', '02', '04', '01', '1975', '', null, null);
INSERT INTO t_sys_area VALUES ('1976', '410922', '410922', '410922', '清丰县', 'Qingfeng Xian', 'Qingfeng Xian', ',17,195,1976,', '河南省-濮阳市-清丰县', '195', '02', '04', '01', '1976', '', null, null);
INSERT INTO t_sys_area VALUES ('1977', '410923', '410923', '410923', '南乐县', 'Nanle Xian', 'Nanle Xian', ',17,195,1977,', '河南省-濮阳市-南乐县', '195', '02', '04', '01', '1977', '', null, null);
INSERT INTO t_sys_area VALUES ('1978', '410926', '410926', '410926', '范县', 'Fan Xian', 'Fan Xian', ',17,195,1978,', '河南省-濮阳市-范县', '195', '02', '04', '01', '1978', '', null, null);
INSERT INTO t_sys_area VALUES ('1979', '410927', '410927', '410927', '台前县', 'Taiqian Xian', 'Taiqian Xian', ',17,195,1979,', '河南省-濮阳市-台前县', '195', '02', '04', '01', '1979', '', null, null);
INSERT INTO t_sys_area VALUES ('1980', '410928', '410928', '410928', '濮阳县', 'Puyang Xian', 'Puyang Xian', ',17,195,1980,', '河南省-濮阳市-濮阳县', '195', '02', '04', '01', '1980', '', null, null);
INSERT INTO t_sys_area VALUES ('1981', '411001', '411001', '411001', '市辖区', 'Shixiaqu', 'Shixiaqu', ',17,196,1981,', '河南省-许昌市-市辖区', '196', '02', '04', '01', '1981', '', null, null);
INSERT INTO t_sys_area VALUES ('1982', '411002', '411002', '411002', '魏都区', 'Weidu Qu', 'Weidu Qu', ',17,196,1982,', '河南省-许昌市-魏都区', '196', '02', '04', '01', '1982', '', null, null);
INSERT INTO t_sys_area VALUES ('1983', '411023', '411023', '411023', '许昌县', 'Xuchang Xian', 'Xuchang Xian', ',17,196,1983,', '河南省-许昌市-许昌县', '196', '02', '04', '01', '1983', '', null, null);
INSERT INTO t_sys_area VALUES ('1984', '411024', '411024', '411024', '鄢陵县', 'Yanling Xian', 'Yanling Xian', ',17,196,1984,', '河南省-许昌市-鄢陵县', '196', '02', '04', '01', '1984', '', null, null);
INSERT INTO t_sys_area VALUES ('1985', '411025', '411025', '411025', '襄城县', 'Xiangcheng Xian', 'Xiangcheng Xian', ',17,196,1985,', '河南省-许昌市-襄城县', '196', '02', '04', '01', '1985', '', null, null);
INSERT INTO t_sys_area VALUES ('1986', '411081', '411081', '411081', '禹州市', 'Yuzhou Shi', 'Yuzhou Shi', ',17,196,1986,', '河南省-许昌市-禹州市', '196', '02', '04', '01', '1986', '', null, null);
INSERT INTO t_sys_area VALUES ('1987', '411082', '411082', '411082', '长葛市', 'Changge Shi', 'Changge Shi', ',17,196,1987,', '河南省-许昌市-长葛市', '196', '02', '04', '01', '1987', '', null, null);
INSERT INTO t_sys_area VALUES ('1988', '411101', '411101', '411101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',17,197,1988,', '河南省-漯河市-市辖区', '197', '02', '04', '01', '1988', '', null, null);
INSERT INTO t_sys_area VALUES ('1989', '411102', '411102', '411102', '源汇区', 'Yuanhui Qu', 'Yuanhui Qu', ',17,197,1989,', '河南省-漯河市-源汇区', '197', '02', '04', '01', '1989', '', null, null);
INSERT INTO t_sys_area VALUES ('1990', '411103', '411103', '411103', '郾城区', 'Yancheng Qu', 'Yancheng Qu', ',17,197,1990,', '河南省-漯河市-郾城区', '197', '02', '04', '01', '1990', '', null, null);
INSERT INTO t_sys_area VALUES ('1991', '411104', '411104', '411104', '召陵区', 'Zhaoling Qu', 'Zhaoling Qu', ',17,197,1991,', '河南省-漯河市-召陵区', '197', '02', '04', '01', '1991', '', null, null);
INSERT INTO t_sys_area VALUES ('1992', '411121', '411121', '411121', '舞阳县', 'Wuyang Xian', 'Wuyang Xian', ',17,197,1992,', '河南省-漯河市-舞阳县', '197', '02', '04', '01', '1992', '', null, null);
INSERT INTO t_sys_area VALUES ('1993', '411122', '411122', '411122', '临颍县', 'Linying Xian', 'Linying Xian', ',17,197,1993,', '河南省-漯河市-临颍县', '197', '02', '04', '01', '1993', '', null, null);
INSERT INTO t_sys_area VALUES ('1994', '411201', '411201', '411201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',17,198,1994,', '河南省-三门峡市-市辖区', '198', '02', '04', '01', '1994', '', null, null);
INSERT INTO t_sys_area VALUES ('1995', '411202', '411202', '411202', '湖滨区', 'Hubin Qu', 'Hubin Qu', ',17,198,1995,', '河南省-三门峡市-湖滨区', '198', '02', '04', '01', '1995', '', null, null);
INSERT INTO t_sys_area VALUES ('1996', '411221', '411221', '411221', '渑池县', 'Mianchi Xian', 'Mianchi Xian', ',17,198,1996,', '河南省-三门峡市-渑池县', '198', '02', '04', '01', '1996', '', null, null);
INSERT INTO t_sys_area VALUES ('1997', '411222', '411222', '411222', '陕县', 'Shan Xian', 'Shan Xian', ',17,198,1997,', '河南省-三门峡市-陕县', '198', '02', '04', '01', '1997', '', null, null);
INSERT INTO t_sys_area VALUES ('1998', '411224', '411224', '411224', '卢氏县', 'Lushi Xian', 'Lushi Xian', ',17,198,1998,', '河南省-三门峡市-卢氏县', '198', '02', '04', '01', '1998', '', null, null);
INSERT INTO t_sys_area VALUES ('1999', '411281', '411281', '411281', '义马市', 'Yima Shi', 'Yima Shi', ',17,198,1999,', '河南省-三门峡市-义马市', '198', '02', '04', '01', '1999', '', null, null);
INSERT INTO t_sys_area VALUES ('2000', '411282', '411282', '411282', '灵宝市', 'Lingbao Shi', 'Lingbao Shi', ',17,198,2000,', '河南省-三门峡市-灵宝市', '198', '02', '04', '01', '2000', '', null, null);
INSERT INTO t_sys_area VALUES ('2001', '411301', '411301', '411301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',17,199,2001,', '河南省-南阳市-市辖区', '199', '02', '04', '01', '2001', '', null, null);
INSERT INTO t_sys_area VALUES ('2002', '411302', '411302', '411302', '宛城区', 'Wancheng Qu', 'Wancheng Qu', ',17,199,2002,', '河南省-南阳市-宛城区', '199', '02', '04', '01', '2002', '', null, null);
INSERT INTO t_sys_area VALUES ('2003', '411303', '411303', '411303', '卧龙区', 'Wolong Qu', 'Wolong Qu', ',17,199,2003,', '河南省-南阳市-卧龙区', '199', '02', '04', '01', '2003', '', null, null);
INSERT INTO t_sys_area VALUES ('2004', '411321', '411321', '411321', '南召县', 'Nanzhao Xian', 'Nanzhao Xian', ',17,199,2004,', '河南省-南阳市-南召县', '199', '02', '04', '01', '2004', '', null, null);
INSERT INTO t_sys_area VALUES ('2005', '411322', '411322', '411322', '方城县', 'Fangcheng Xian', 'Fangcheng Xian', ',17,199,2005,', '河南省-南阳市-方城县', '199', '02', '04', '01', '2005', '', null, null);
INSERT INTO t_sys_area VALUES ('2006', '411323', '411323', '411323', '西峡县', 'Xixia Xian', 'Xixia Xian', ',17,199,2006,', '河南省-南阳市-西峡县', '199', '02', '04', '01', '2006', '', null, null);
INSERT INTO t_sys_area VALUES ('2007', '411324', '411324', '411324', '镇平县', 'Zhenping Xian', 'Zhenping Xian', ',17,199,2007,', '河南省-南阳市-镇平县', '199', '02', '04', '01', '2007', '', null, null);
INSERT INTO t_sys_area VALUES ('2008', '411325', '411325', '411325', '内乡县', 'Neixiang Xian', 'Neixiang Xian', ',17,199,2008,', '河南省-南阳市-内乡县', '199', '02', '04', '01', '2008', '', null, null);
INSERT INTO t_sys_area VALUES ('2009', '411326', '411326', '411326', '淅川县', 'Xichuan Xian', 'Xichuan Xian', ',17,199,2009,', '河南省-南阳市-淅川县', '199', '02', '04', '01', '2009', '', null, null);
INSERT INTO t_sys_area VALUES ('2010', '411327', '411327', '411327', '社旗县', 'Sheqi Xian', 'Sheqi Xian', ',17,199,2010,', '河南省-南阳市-社旗县', '199', '02', '04', '01', '2010', '', null, null);
INSERT INTO t_sys_area VALUES ('2011', '411328', '411328', '411328', '唐河县', 'Tanghe Xian', 'Tanghe Xian', ',17,199,2011,', '河南省-南阳市-唐河县', '199', '02', '04', '01', '2011', '', null, null);
INSERT INTO t_sys_area VALUES ('2012', '411329', '411329', '411329', '新野县', 'Xinye Xian', 'Xinye Xian', ',17,199,2012,', '河南省-南阳市-新野县', '199', '02', '04', '01', '2012', '', null, null);
INSERT INTO t_sys_area VALUES ('2013', '411330', '411330', '411330', '桐柏县', 'Tongbai Xian', 'Tongbai Xian', ',17,199,2013,', '河南省-南阳市-桐柏县', '199', '02', '04', '01', '2013', '', null, null);
INSERT INTO t_sys_area VALUES ('2014', '411381', '411381', '411381', '邓州市', 'Dengzhou Shi', 'Dengzhou Shi', ',17,199,2014,', '河南省-南阳市-邓州市', '199', '02', '04', '01', '2014', '', null, null);
INSERT INTO t_sys_area VALUES ('2015', '411401', '411401', '411401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',17,200,2015,', '河南省-商丘市-市辖区', '200', '02', '04', '01', '2015', '', null, null);
INSERT INTO t_sys_area VALUES ('2016', '411402', '411402', '411402', '梁园区', 'Liangyuan Qu', 'Liangyuan Qu', ',17,200,2016,', '河南省-商丘市-梁园区', '200', '02', '04', '01', '2016', '', null, null);
INSERT INTO t_sys_area VALUES ('2017', '411403', '411403', '411403', '睢阳区', 'Suiyang Qu', 'Suiyang Qu', ',17,200,2017,', '河南省-商丘市-睢阳区', '200', '02', '04', '01', '2017', '', null, null);
INSERT INTO t_sys_area VALUES ('2018', '411421', '411421', '411421', '民权县', 'Minquan Xian', 'Minquan Xian', ',17,200,2018,', '河南省-商丘市-民权县', '200', '02', '04', '01', '2018', '', null, null);
INSERT INTO t_sys_area VALUES ('2019', '411422', '411422', '411422', '睢县', 'Sui Xian', 'Sui Xian', ',17,200,2019,', '河南省-商丘市-睢县', '200', '02', '04', '01', '2019', '', null, null);
INSERT INTO t_sys_area VALUES ('2020', '411423', '411423', '411423', '宁陵县', 'Ningling Xian', 'Ningling Xian', ',17,200,2020,', '河南省-商丘市-宁陵县', '200', '02', '04', '01', '2020', '', null, null);
INSERT INTO t_sys_area VALUES ('2021', '411424', '411424', '411424', '柘城县', 'Zhecheng Xian', 'Zhecheng Xian', ',17,200,2021,', '河南省-商丘市-柘城县', '200', '02', '04', '01', '2021', '', null, null);
INSERT INTO t_sys_area VALUES ('2022', '411425', '411425', '411425', '虞城县', 'Yucheng Xian', 'Yucheng Xian', ',17,200,2022,', '河南省-商丘市-虞城县', '200', '02', '04', '01', '2022', '', null, null);
INSERT INTO t_sys_area VALUES ('2023', '411426', '411426', '411426', '夏邑县', 'Xiayi Xian', 'Xiayi Xian', ',17,200,2023,', '河南省-商丘市-夏邑县', '200', '02', '04', '01', '2023', '', null, null);
INSERT INTO t_sys_area VALUES ('2024', '411481', '411481', '411481', '永城市', 'Yongcheng Shi', 'Yongcheng Shi', ',17,200,2024,', '河南省-商丘市-永城市', '200', '02', '04', '01', '2024', '', null, null);
INSERT INTO t_sys_area VALUES ('2025', '411501', '411501', '411501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',17,201,2025,', '河南省-信阳市-市辖区', '201', '02', '04', '01', '2025', '', null, null);
INSERT INTO t_sys_area VALUES ('2026', '411502', '411502', '411502', '浉河区', 'Shihe Qu', 'Shihe Qu', ',17,201,2026,', '河南省-信阳市-浉河区', '201', '02', '04', '01', '2026', '', null, null);
INSERT INTO t_sys_area VALUES ('2027', '411503', '411503', '411503', '平桥区', 'Pingqiao Qu', 'Pingqiao Qu', ',17,201,2027,', '河南省-信阳市-平桥区', '201', '02', '04', '01', '2027', '', null, null);
INSERT INTO t_sys_area VALUES ('2028', '411521', '411521', '411521', '罗山县', 'Luoshan Xian', 'Luoshan Xian', ',17,201,2028,', '河南省-信阳市-罗山县', '201', '02', '04', '01', '2028', '', null, null);
INSERT INTO t_sys_area VALUES ('2029', '411522', '411522', '411522', '光山县', 'Guangshan Xian', 'Guangshan Xian', ',17,201,2029,', '河南省-信阳市-光山县', '201', '02', '04', '01', '2029', '', null, null);
INSERT INTO t_sys_area VALUES ('2030', '411523', '411523', '411523', '新县', 'Xin Xian', 'Xin Xian', ',17,201,2030,', '河南省-信阳市-新县', '201', '02', '04', '01', '2030', '', null, null);
INSERT INTO t_sys_area VALUES ('2031', '411524', '411524', '411524', '商城县', 'Shangcheng Xian', 'Shangcheng Xian', ',17,201,2031,', '河南省-信阳市-商城县', '201', '02', '04', '01', '2031', '', null, null);
INSERT INTO t_sys_area VALUES ('2032', '411525', '411525', '411525', '固始县', 'Gushi Xian', 'Gushi Xian', ',17,201,2032,', '河南省-信阳市-固始县', '201', '02', '04', '01', '2032', '', null, null);
INSERT INTO t_sys_area VALUES ('2033', '411526', '411526', '411526', '潢川县', 'Huangchuan Xian', 'Huangchuan Xian', ',17,201,2033,', '河南省-信阳市-潢川县', '201', '02', '04', '01', '2033', '', null, null);
INSERT INTO t_sys_area VALUES ('2034', '411527', '411527', '411527', '淮滨县', 'Huaibin Xian', 'Huaibin Xian', ',17,201,2034,', '河南省-信阳市-淮滨县', '201', '02', '04', '01', '2034', '', null, null);
INSERT INTO t_sys_area VALUES ('2035', '411528', '411528', '411528', '息县', 'Xi Xian', 'Xi Xian', ',17,201,2035,', '河南省-信阳市-息县', '201', '02', '04', '01', '2035', '', null, null);
INSERT INTO t_sys_area VALUES ('2036', '411601', '411601', '411601', '市辖区', '1', '1', ',17,202,2036,', '河南省-周口市-市辖区', '202', '02', '04', '01', '2036', '', null, null);
INSERT INTO t_sys_area VALUES ('2037', '411602', '411602', '411602', '川汇区', 'Chuanhui Qu', 'Chuanhui Qu', ',17,202,2037,', '河南省-周口市-川汇区', '202', '02', '04', '01', '2037', '', null, null);
INSERT INTO t_sys_area VALUES ('2038', '411621', '411621', '411621', '扶沟县', 'Fugou Xian', 'Fugou Xian', ',17,202,2038,', '河南省-周口市-扶沟县', '202', '02', '04', '01', '2038', '', null, null);
INSERT INTO t_sys_area VALUES ('2039', '411622', '411622', '411622', '西华县', 'Xihua Xian', 'Xihua Xian', ',17,202,2039,', '河南省-周口市-西华县', '202', '02', '04', '01', '2039', '', null, null);
INSERT INTO t_sys_area VALUES ('2040', '411623', '411623', '411623', '商水县', 'Shangshui Xian', 'Shangshui Xian', ',17,202,2040,', '河南省-周口市-商水县', '202', '02', '04', '01', '2040', '', null, null);
INSERT INTO t_sys_area VALUES ('2041', '411624', '411624', '411624', '沈丘县', 'Shenqiu Xian', 'Shenqiu Xian', ',17,202,2041,', '河南省-周口市-沈丘县', '202', '02', '04', '01', '2041', '', null, null);
INSERT INTO t_sys_area VALUES ('2042', '411625', '411625', '411625', '郸城县', 'Dancheng Xian', 'Dancheng Xian', ',17,202,2042,', '河南省-周口市-郸城县', '202', '02', '04', '01', '2042', '', null, null);
INSERT INTO t_sys_area VALUES ('2043', '411626', '411626', '411626', '淮阳县', 'Huaiyang Xian', 'Huaiyang Xian', ',17,202,2043,', '河南省-周口市-淮阳县', '202', '02', '04', '01', '2043', '', null, null);
INSERT INTO t_sys_area VALUES ('2044', '411627', '411627', '411627', '太康县', 'Taikang Xian', 'Taikang Xian', ',17,202,2044,', '河南省-周口市-太康县', '202', '02', '04', '01', '2044', '', null, null);
INSERT INTO t_sys_area VALUES ('2045', '411628', '411628', '411628', '鹿邑县', 'Luyi Xian', 'Luyi Xian', ',17,202,2045,', '河南省-周口市-鹿邑县', '202', '02', '04', '01', '2045', '', null, null);
INSERT INTO t_sys_area VALUES ('2046', '411681', '411681', '411681', '项城市', 'Xiangcheng Shi', 'Xiangcheng Shi', ',17,202,2046,', '河南省-周口市-项城市', '202', '02', '04', '01', '2046', '', null, null);
INSERT INTO t_sys_area VALUES ('2047', '411701', '411701', '411701', '市辖区', '1', '1', ',17,203,2047,', '河南省-驻马店市-市辖区', '203', '02', '04', '01', '2047', '', null, null);
INSERT INTO t_sys_area VALUES ('2048', '411702', '411702', '411702', '驿城区', 'Yicheng Qu', 'Yicheng Qu', ',17,203,2048,', '河南省-驻马店市-驿城区', '203', '02', '04', '01', '2048', '', null, null);
INSERT INTO t_sys_area VALUES ('2049', '411721', '411721', '411721', '西平县', 'Xiping Xian', 'Xiping Xian', ',17,203,2049,', '河南省-驻马店市-西平县', '203', '02', '04', '01', '2049', '', null, null);
INSERT INTO t_sys_area VALUES ('2050', '411722', '411722', '411722', '上蔡县', 'Shangcai Xian', 'Shangcai Xian', ',17,203,2050,', '河南省-驻马店市-上蔡县', '203', '02', '04', '01', '2050', '', null, null);
INSERT INTO t_sys_area VALUES ('2051', '411723', '411723', '411723', '平舆县', 'Pingyu Xian', 'Pingyu Xian', ',17,203,2051,', '河南省-驻马店市-平舆县', '203', '02', '04', '01', '2051', '', null, null);
INSERT INTO t_sys_area VALUES ('2052', '411724', '411724', '411724', '正阳县', 'Zhengyang Xian', 'Zhengyang Xian', ',17,203,2052,', '河南省-驻马店市-正阳县', '203', '02', '04', '01', '2052', '', null, null);
INSERT INTO t_sys_area VALUES ('2053', '411725', '411725', '411725', '确山县', 'Queshan Xian', 'Queshan Xian', ',17,203,2053,', '河南省-驻马店市-确山县', '203', '02', '04', '01', '2053', '', null, null);
INSERT INTO t_sys_area VALUES ('2054', '411726', '411726', '411726', '泌阳县', 'Biyang Xian', 'Biyang Xian', ',17,203,2054,', '河南省-驻马店市-泌阳县', '203', '02', '04', '01', '2054', '', null, null);
INSERT INTO t_sys_area VALUES ('2055', '411727', '411727', '411727', '汝南县', 'Runan Xian', 'Runan Xian', ',17,203,2055,', '河南省-驻马店市-汝南县', '203', '02', '04', '01', '2055', '', null, null);
INSERT INTO t_sys_area VALUES ('2056', '411728', '411728', '411728', '遂平县', 'Suiping Xian', 'Suiping Xian', ',17,203,2056,', '河南省-驻马店市-遂平县', '203', '02', '04', '01', '2056', '', null, null);
INSERT INTO t_sys_area VALUES ('2057', '411729', '411729', '411729', '新蔡县', 'Xincai Xian', 'Xincai Xian', ',17,203,2057,', '河南省-驻马店市-新蔡县', '203', '02', '04', '01', '2057', '', null, null);
INSERT INTO t_sys_area VALUES ('2058', '420101', '420101', '420101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',18,204,2058,', '湖北省-武汉市-市辖区', '204', '02', '04', '01', '2058', '', null, null);
INSERT INTO t_sys_area VALUES ('2059', '420102', '420102', '420102', '江岸区', 'Jiang,an Qu', 'Jiang,an Qu', ',18,204,2059,', '湖北省-武汉市-江岸区', '204', '02', '04', '01', '2059', '', null, null);
INSERT INTO t_sys_area VALUES ('2060', '420103', '420103', '420103', '江汉区', 'Jianghan Qu', 'Jianghan Qu', ',18,204,2060,', '湖北省-武汉市-江汉区', '204', '02', '04', '01', '2060', '', null, null);
INSERT INTO t_sys_area VALUES ('2061', '420104', '420104', '420104', '硚口区', 'Qiaokou Qu', 'Qiaokou Qu', ',18,204,2061,', '湖北省-武汉市-硚口区', '204', '02', '04', '01', '2061', '', null, null);
INSERT INTO t_sys_area VALUES ('2062', '420105', '420105', '420105', '汉阳区', 'Hanyang Qu', 'Hanyang Qu', ',18,204,2062,', '湖北省-武汉市-汉阳区', '204', '02', '04', '01', '2062', '', null, null);
INSERT INTO t_sys_area VALUES ('2063', '420106', '420106', '420106', '武昌区', 'Wuchang Qu', 'Wuchang Qu', ',18,204,2063,', '湖北省-武汉市-武昌区', '204', '02', '04', '01', '2063', '', null, null);
INSERT INTO t_sys_area VALUES ('2064', '420107', '420107', '420107', '青山区', 'Qingshan Qu', 'Qingshan Qu', ',18,204,2064,', '湖北省-武汉市-青山区', '204', '02', '04', '01', '2064', '', null, null);
INSERT INTO t_sys_area VALUES ('2065', '420111', '420111', '420111', '洪山区', 'Hongshan Qu', 'Hongshan Qu', ',18,204,2065,', '湖北省-武汉市-洪山区', '204', '02', '04', '01', '2065', '', null, null);
INSERT INTO t_sys_area VALUES ('2066', '420112', '420112', '420112', '东西湖区', 'Dongxihu Qu', 'Dongxihu Qu', ',18,204,2066,', '湖北省-武汉市-东西湖区', '204', '02', '04', '01', '2066', '', null, null);
INSERT INTO t_sys_area VALUES ('2067', '420113', '420113', '420113', '汉南区', 'Hannan Qu', 'Hannan Qu', ',18,204,2067,', '湖北省-武汉市-汉南区', '204', '02', '04', '01', '2067', '', null, null);
INSERT INTO t_sys_area VALUES ('2068', '420114', '420114', '420114', '蔡甸区', 'Caidian Qu', 'Caidian Qu', ',18,204,2068,', '湖北省-武汉市-蔡甸区', '204', '02', '04', '01', '2068', '', null, null);
INSERT INTO t_sys_area VALUES ('2069', '420115', '420115', '420115', '江夏区', 'Jiangxia Qu', 'Jiangxia Qu', ',18,204,2069,', '湖北省-武汉市-江夏区', '204', '02', '04', '01', '2069', '', null, null);
INSERT INTO t_sys_area VALUES ('2070', '420116', '420116', '420116', '黄陂区', 'Huangpi Qu', 'Huangpi Qu', ',18,204,2070,', '湖北省-武汉市-黄陂区', '204', '02', '04', '01', '2070', '', null, null);
INSERT INTO t_sys_area VALUES ('2071', '420117', '420117', '420117', '新洲区', 'Xinzhou Qu', 'Xinzhou Qu', ',18,204,2071,', '湖北省-武汉市-新洲区', '204', '02', '04', '01', '2071', '', null, null);
INSERT INTO t_sys_area VALUES ('2072', '420201', '420201', '420201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',18,205,2072,', '湖北省-黄石市-市辖区', '205', '02', '04', '01', '2072', '', null, null);
INSERT INTO t_sys_area VALUES ('2073', '420202', '420202', '420202', '黄石港区', 'Huangshigang Qu', 'Huangshigang Qu', ',18,205,2073,', '湖北省-黄石市-黄石港区', '205', '02', '04', '01', '2073', '', null, null);
INSERT INTO t_sys_area VALUES ('2074', '420203', '420203', '420203', '西塞山区', 'Xisaishan Qu', 'Xisaishan Qu', ',18,205,2074,', '湖北省-黄石市-西塞山区', '205', '02', '04', '01', '2074', '', null, null);
INSERT INTO t_sys_area VALUES ('2075', '420204', '420204', '420204', '下陆区', 'Xialu Qu', 'Xialu Qu', ',18,205,2075,', '湖北省-黄石市-下陆区', '205', '02', '04', '01', '2075', '', null, null);
INSERT INTO t_sys_area VALUES ('2076', '420205', '420205', '420205', '铁山区', 'Tieshan Qu', 'Tieshan Qu', ',18,205,2076,', '湖北省-黄石市-铁山区', '205', '02', '04', '01', '2076', '', null, null);
INSERT INTO t_sys_area VALUES ('2077', '420222', '420222', '420222', '阳新县', 'Yangxin Xian', 'Yangxin Xian', ',18,205,2077,', '湖北省-黄石市-阳新县', '205', '02', '04', '01', '2077', '', null, null);
INSERT INTO t_sys_area VALUES ('2078', '420281', '420281', '420281', '大冶市', 'Daye Shi', 'Daye Shi', ',18,205,2078,', '湖北省-黄石市-大冶市', '205', '02', '04', '01', '2078', '', null, null);
INSERT INTO t_sys_area VALUES ('2079', '420301', '420301', '420301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',18,206,2079,', '湖北省-十堰市-市辖区', '206', '02', '04', '01', '2079', '', null, null);
INSERT INTO t_sys_area VALUES ('2080', '420302', '420302', '420302', '茅箭区', 'Maojian Qu', 'Maojian Qu', ',18,206,2080,', '湖北省-十堰市-茅箭区', '206', '02', '04', '01', '2080', '', null, null);
INSERT INTO t_sys_area VALUES ('2081', '420303', '420303', '420303', '张湾区', 'Zhangwan Qu', 'Zhangwan Qu', ',18,206,2081,', '湖北省-十堰市-张湾区', '206', '02', '04', '01', '2081', '', null, null);
INSERT INTO t_sys_area VALUES ('2082', '420321', '420321', '420321', '郧县', 'Yun Xian', 'Yun Xian', ',18,206,2082,', '湖北省-十堰市-郧县', '206', '02', '04', '01', '2082', '', null, null);
INSERT INTO t_sys_area VALUES ('2083', '420322', '420322', '420322', '郧西县', 'Yunxi Xian', 'Yunxi Xian', ',18,206,2083,', '湖北省-十堰市-郧西县', '206', '02', '04', '01', '2083', '', null, null);
INSERT INTO t_sys_area VALUES ('2084', '420323', '420323', '420323', '竹山县', 'Zhushan Xian', 'Zhushan Xian', ',18,206,2084,', '湖北省-十堰市-竹山县', '206', '02', '04', '01', '2084', '', null, null);
INSERT INTO t_sys_area VALUES ('2085', '420324', '420324', '420324', '竹溪县', 'Zhuxi Xian', 'Zhuxi Xian', ',18,206,2085,', '湖北省-十堰市-竹溪县', '206', '02', '04', '01', '2085', '', null, null);
INSERT INTO t_sys_area VALUES ('2086', '420325', '420325', '420325', '房县', 'Fang Xian', 'Fang Xian', ',18,206,2086,', '湖北省-十堰市-房县', '206', '02', '04', '01', '2086', '', null, null);
INSERT INTO t_sys_area VALUES ('2087', '420381', '420381', '420381', '丹江口市', 'Danjiangkou Shi', 'Danjiangkou Shi', ',18,206,2087,', '湖北省-十堰市-丹江口市', '206', '02', '04', '01', '2087', '', null, null);
INSERT INTO t_sys_area VALUES ('2088', '420501', '420501', '420501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',18,207,2088,', '湖北省-宜昌市-市辖区', '207', '02', '04', '01', '2088', '', null, null);
INSERT INTO t_sys_area VALUES ('2089', '420502', '420502', '420502', '西陵区', 'Xiling Qu', 'Xiling Qu', ',18,207,2089,', '湖北省-宜昌市-西陵区', '207', '02', '04', '01', '2089', '', null, null);
INSERT INTO t_sys_area VALUES ('2090', '420503', '420503', '420503', '伍家岗区', 'Wujiagang Qu', 'Wujiagang Qu', ',18,207,2090,', '湖北省-宜昌市-伍家岗区', '207', '02', '04', '01', '2090', '', null, null);
INSERT INTO t_sys_area VALUES ('2091', '420504', '420504', '420504', '点军区', 'Dianjun Qu', 'Dianjun Qu', ',18,207,2091,', '湖北省-宜昌市-点军区', '207', '02', '04', '01', '2091', '', null, null);
INSERT INTO t_sys_area VALUES ('2092', '420505', '420505', '420505', '猇亭区', 'Xiaoting Qu', 'Xiaoting Qu', ',18,207,2092,', '湖北省-宜昌市-猇亭区', '207', '02', '04', '01', '2092', '', null, null);
INSERT INTO t_sys_area VALUES ('2093', '420506', '420506', '420506', '夷陵区', 'Yiling Qu', 'Yiling Qu', ',18,207,2093,', '湖北省-宜昌市-夷陵区', '207', '02', '04', '01', '2093', '', null, null);
INSERT INTO t_sys_area VALUES ('2094', '420525', '420525', '420525', '远安县', 'Yuan,an Xian', 'Yuan,an Xian', ',18,207,2094,', '湖北省-宜昌市-远安县', '207', '02', '04', '01', '2094', '', null, null);
INSERT INTO t_sys_area VALUES ('2095', '420526', '420526', '420526', '兴山县', 'Xingshan Xian', 'Xingshan Xian', ',18,207,2095,', '湖北省-宜昌市-兴山县', '207', '02', '04', '01', '2095', '', null, null);
INSERT INTO t_sys_area VALUES ('2096', '420527', '420527', '420527', '秭归县', 'Zigui Xian', 'Zigui Xian', ',18,207,2096,', '湖北省-宜昌市-秭归县', '207', '02', '04', '01', '2096', '', null, null);
INSERT INTO t_sys_area VALUES ('2097', '420528', '420528', '420528', '长阳土家族自治县', 'Changyang Tujiazu Zizhixian', 'Changyang Tujiazu Zizhixian', ',18,207,2097,', '湖北省-宜昌市-长阳土家族自治县', '207', '02', '04', '01', '2097', '', null, null);
INSERT INTO t_sys_area VALUES ('2098', '420529', '420529', '420529', '五峰土家族自治县', 'Wufeng Tujiazu Zizhixian', 'Wufeng Tujiazu Zizhixian', ',18,207,2098,', '湖北省-宜昌市-五峰土家族自治县', '207', '02', '04', '01', '2098', '', null, null);
INSERT INTO t_sys_area VALUES ('2099', '420581', '420581', '420581', '宜都市', 'Yidu Shi', 'Yidu Shi', ',18,207,2099,', '湖北省-宜昌市-宜都市', '207', '02', '04', '01', '2099', '', null, null);
INSERT INTO t_sys_area VALUES ('2100', '420582', '420582', '420582', '当阳市', 'Dangyang Shi', 'Dangyang Shi', ',18,207,2100,', '湖北省-宜昌市-当阳市', '207', '02', '04', '01', '2100', '', null, null);
INSERT INTO t_sys_area VALUES ('2101', '420583', '420583', '420583', '枝江市', 'Zhijiang Shi', 'Zhijiang Shi', ',18,207,2101,', '湖北省-宜昌市-枝江市', '207', '02', '04', '01', '2101', '', null, null);
INSERT INTO t_sys_area VALUES ('2102', '420601', '420601', '420601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',18,208,2102,', '湖北省-襄樊市-市辖区', '208', '02', '04', '01', '2102', '', null, null);
INSERT INTO t_sys_area VALUES ('2103', '420602', '420602', '420602', '襄城区', 'Xiangcheng Qu', 'Xiangcheng Qu', ',18,208,2103,', '湖北省-襄樊市-襄城区', '208', '02', '04', '01', '2103', '', null, null);
INSERT INTO t_sys_area VALUES ('2104', '420606', '420606', '420606', '樊城区', 'Fancheng Qu', 'Fancheng Qu', ',18,208,2104,', '湖北省-襄樊市-樊城区', '208', '02', '04', '01', '2104', '', null, null);
INSERT INTO t_sys_area VALUES ('2105', '420607', '420607', '420607', '襄阳区', 'Xiangyang Qu', 'Xiangyang Qu', ',18,208,2105,', '湖北省-襄樊市-襄阳区', '208', '02', '04', '01', '2105', '', null, null);
INSERT INTO t_sys_area VALUES ('2106', '420624', '420624', '420624', '南漳县', 'Nanzhang Xian', 'Nanzhang Xian', ',18,208,2106,', '湖北省-襄樊市-南漳县', '208', '02', '04', '01', '2106', '', null, null);
INSERT INTO t_sys_area VALUES ('2107', '420625', '420625', '420625', '谷城县', 'Gucheng Xian', 'Gucheng Xian', ',18,208,2107,', '湖北省-襄樊市-谷城县', '208', '02', '04', '01', '2107', '', null, null);
INSERT INTO t_sys_area VALUES ('2108', '420626', '420626', '420626', '保康县', 'Baokang Xian', 'Baokang Xian', ',18,208,2108,', '湖北省-襄樊市-保康县', '208', '02', '04', '01', '2108', '', null, null);
INSERT INTO t_sys_area VALUES ('2109', '420682', '420682', '420682', '老河口市', 'Laohekou Shi', 'Laohekou Shi', ',18,208,2109,', '湖北省-襄樊市-老河口市', '208', '02', '04', '01', '2109', '', null, null);
INSERT INTO t_sys_area VALUES ('2110', '420683', '420683', '420683', '枣阳市', 'Zaoyang Shi', 'Zaoyang Shi', ',18,208,2110,', '湖北省-襄樊市-枣阳市', '208', '02', '04', '01', '2110', '', null, null);
INSERT INTO t_sys_area VALUES ('2111', '420684', '420684', '420684', '宜城市', 'Yicheng Shi', 'Yicheng Shi', ',18,208,2111,', '湖北省-襄樊市-宜城市', '208', '02', '04', '01', '2111', '', null, null);
INSERT INTO t_sys_area VALUES ('2112', '420701', '420701', '420701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',18,209,2112,', '湖北省-鄂州市-市辖区', '209', '02', '04', '01', '2112', '', null, null);
INSERT INTO t_sys_area VALUES ('2113', '420702', '420702', '420702', '梁子湖区', 'Liangzihu Qu', 'Liangzihu Qu', ',18,209,2113,', '湖北省-鄂州市-梁子湖区', '209', '02', '04', '01', '2113', '', null, null);
INSERT INTO t_sys_area VALUES ('2114', '420703', '420703', '420703', '华容区', 'Huarong Qu', 'Huarong Qu', ',18,209,2114,', '湖北省-鄂州市-华容区', '209', '02', '04', '01', '2114', '', null, null);
INSERT INTO t_sys_area VALUES ('2115', '420704', '420704', '420704', '鄂城区', 'Echeng Qu', 'Echeng Qu', ',18,209,2115,', '湖北省-鄂州市-鄂城区', '209', '02', '04', '01', '2115', '', null, null);
INSERT INTO t_sys_area VALUES ('2116', '420801', '420801', '420801', '市辖区', 'Shixiaqu', 'Shixiaqu', ',18,210,2116,', '湖北省-荆门市-市辖区', '210', '02', '04', '01', '2116', '', null, null);
INSERT INTO t_sys_area VALUES ('2117', '420802', '420802', '420802', '东宝区', 'Dongbao Qu', 'Dongbao Qu', ',18,210,2117,', '湖北省-荆门市-东宝区', '210', '02', '04', '01', '2117', '', null, null);
INSERT INTO t_sys_area VALUES ('2118', '420804', '420804', '420804', '掇刀区', 'Duodao Qu', 'Duodao Qu', ',18,210,2118,', '湖北省-荆门市-掇刀区', '210', '02', '04', '01', '2118', '', null, null);
INSERT INTO t_sys_area VALUES ('2119', '420821', '420821', '420821', '京山县', 'Jingshan Xian', 'Jingshan Xian', ',18,210,2119,', '湖北省-荆门市-京山县', '210', '02', '04', '01', '2119', '', null, null);
INSERT INTO t_sys_area VALUES ('2120', '420822', '420822', '420822', '沙洋县', 'Shayang Xian', 'Shayang Xian', ',18,210,2120,', '湖北省-荆门市-沙洋县', '210', '02', '04', '01', '2120', '', null, null);
INSERT INTO t_sys_area VALUES ('2121', '420881', '420881', '420881', '钟祥市', 'Zhongxiang Shi', 'Zhongxiang Shi', ',18,210,2121,', '湖北省-荆门市-钟祥市', '210', '02', '04', '01', '2121', '', null, null);
INSERT INTO t_sys_area VALUES ('2122', '420901', '420901', '420901', '市辖区', 'Shixiaqu', 'Shixiaqu', ',18,211,2122,', '湖北省-孝感市-市辖区', '211', '02', '04', '01', '2122', '', null, null);
INSERT INTO t_sys_area VALUES ('2123', '420902', '420902', '420902', '孝南区', 'Xiaonan Qu', 'Xiaonan Qu', ',18,211,2123,', '湖北省-孝感市-孝南区', '211', '02', '04', '01', '2123', '', null, null);
INSERT INTO t_sys_area VALUES ('2124', '420921', '420921', '420921', '孝昌县', 'Xiaochang Xian', 'Xiaochang Xian', ',18,211,2124,', '湖北省-孝感市-孝昌县', '211', '02', '04', '01', '2124', '', null, null);
INSERT INTO t_sys_area VALUES ('2125', '420922', '420922', '420922', '大悟县', 'Dawu Xian', 'Dawu Xian', ',18,211,2125,', '湖北省-孝感市-大悟县', '211', '02', '04', '01', '2125', '', null, null);
INSERT INTO t_sys_area VALUES ('2126', '420923', '420923', '420923', '云梦县', 'Yunmeng Xian', 'Yunmeng Xian', ',18,211,2126,', '湖北省-孝感市-云梦县', '211', '02', '04', '01', '2126', '', null, null);
INSERT INTO t_sys_area VALUES ('2127', '420981', '420981', '420981', '应城市', 'Yingcheng Shi', 'Yingcheng Shi', ',18,211,2127,', '湖北省-孝感市-应城市', '211', '02', '04', '01', '2127', '', null, null);
INSERT INTO t_sys_area VALUES ('2128', '420982', '420982', '420982', '安陆市', 'Anlu Shi', 'Anlu Shi', ',18,211,2128,', '湖北省-孝感市-安陆市', '211', '02', '04', '01', '2128', '', null, null);
INSERT INTO t_sys_area VALUES ('2129', '420984', '420984', '420984', '汉川市', 'Hanchuan Shi', 'Hanchuan Shi', ',18,211,2129,', '湖北省-孝感市-汉川市', '211', '02', '04', '01', '2129', '', null, null);
INSERT INTO t_sys_area VALUES ('2130', '421001', '421001', '421001', '市辖区', 'Shixiaqu', 'Shixiaqu', ',18,212,2130,', '湖北省-荆州市-市辖区', '212', '02', '04', '01', '2130', '', null, null);
INSERT INTO t_sys_area VALUES ('2131', '421002', '421002', '421002', '沙市区', 'Shashi Qu', 'Shashi Qu', ',18,212,2131,', '湖北省-荆州市-沙市区', '212', '02', '04', '01', '2131', '', null, null);
INSERT INTO t_sys_area VALUES ('2132', '421003', '421003', '421003', '荆州区', 'Jingzhou Qu', 'Jingzhou Qu', ',18,212,2132,', '湖北省-荆州市-荆州区', '212', '02', '04', '01', '2132', '', null, null);
INSERT INTO t_sys_area VALUES ('2133', '421022', '421022', '421022', '公安县', 'Gong,an Xian', 'Gong,an Xian', ',18,212,2133,', '湖北省-荆州市-公安县', '212', '02', '04', '01', '2133', '', null, null);
INSERT INTO t_sys_area VALUES ('2134', '421023', '421023', '421023', '监利县', 'Jianli Xian', 'Jianli Xian', ',18,212,2134,', '湖北省-荆州市-监利县', '212', '02', '04', '01', '2134', '', null, null);
INSERT INTO t_sys_area VALUES ('2135', '421024', '421024', '421024', '江陵县', 'Jiangling Xian', 'Jiangling Xian', ',18,212,2135,', '湖北省-荆州市-江陵县', '212', '02', '04', '01', '2135', '', null, null);
INSERT INTO t_sys_area VALUES ('2136', '421081', '421081', '421081', '石首市', 'Shishou Shi', 'Shishou Shi', ',18,212,2136,', '湖北省-荆州市-石首市', '212', '02', '04', '01', '2136', '', null, null);
INSERT INTO t_sys_area VALUES ('2137', '421083', '421083', '421083', '洪湖市', 'Honghu Shi', 'Honghu Shi', ',18,212,2137,', '湖北省-荆州市-洪湖市', '212', '02', '04', '01', '2137', '', null, null);
INSERT INTO t_sys_area VALUES ('2138', '421087', '421087', '421087', '松滋市', 'Songzi Shi', 'Songzi Shi', ',18,212,2138,', '湖北省-荆州市-松滋市', '212', '02', '04', '01', '2138', '', null, null);
INSERT INTO t_sys_area VALUES ('2139', '421101', '421101', '421101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',18,213,2139,', '湖北省-黄冈市-市辖区', '213', '02', '04', '01', '2139', '', null, null);
INSERT INTO t_sys_area VALUES ('2140', '421102', '421102', '421102', '黄州区', 'Huangzhou Qu', 'Huangzhou Qu', ',18,213,2140,', '湖北省-黄冈市-黄州区', '213', '02', '04', '01', '2140', '', null, null);
INSERT INTO t_sys_area VALUES ('2141', '421121', '421121', '421121', '团风县', 'Tuanfeng Xian', 'Tuanfeng Xian', ',18,213,2141,', '湖北省-黄冈市-团风县', '213', '02', '04', '01', '2141', '', null, null);
INSERT INTO t_sys_area VALUES ('2142', '421122', '421122', '421122', '红安县', 'Hong,an Xian', 'Hong,an Xian', ',18,213,2142,', '湖北省-黄冈市-红安县', '213', '02', '04', '01', '2142', '', null, null);
INSERT INTO t_sys_area VALUES ('2143', '421123', '421123', '421123', '罗田县', 'Luotian Xian', 'Luotian Xian', ',18,213,2143,', '湖北省-黄冈市-罗田县', '213', '02', '04', '01', '2143', '', null, null);
INSERT INTO t_sys_area VALUES ('2144', '421124', '421124', '421124', '英山县', 'Yingshan Xian', 'Yingshan Xian', ',18,213,2144,', '湖北省-黄冈市-英山县', '213', '02', '04', '01', '2144', '', null, null);
INSERT INTO t_sys_area VALUES ('2145', '421125', '421125', '421125', '浠水县', 'Xishui Xian', 'Xishui Xian', ',18,213,2145,', '湖北省-黄冈市-浠水县', '213', '02', '04', '01', '2145', '', null, null);
INSERT INTO t_sys_area VALUES ('2146', '421126', '421126', '421126', '蕲春县', 'Qichun Xian', 'Qichun Xian', ',18,213,2146,', '湖北省-黄冈市-蕲春县', '213', '02', '04', '01', '2146', '', null, null);
INSERT INTO t_sys_area VALUES ('2147', '421127', '421127', '421127', '黄梅县', 'Huangmei Xian', 'Huangmei Xian', ',18,213,2147,', '湖北省-黄冈市-黄梅县', '213', '02', '04', '01', '2147', '', null, null);
INSERT INTO t_sys_area VALUES ('2148', '421181', '421181', '421181', '麻城市', 'Macheng Shi', 'Macheng Shi', ',18,213,2148,', '湖北省-黄冈市-麻城市', '213', '02', '04', '01', '2148', '', null, null);
INSERT INTO t_sys_area VALUES ('2149', '421182', '421182', '421182', '武穴市', 'Wuxue Shi', 'Wuxue Shi', ',18,213,2149,', '湖北省-黄冈市-武穴市', '213', '02', '04', '01', '2149', '', null, null);
INSERT INTO t_sys_area VALUES ('2150', '421201', '421201', '421201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',18,214,2150,', '湖北省-咸宁市-市辖区', '214', '02', '04', '01', '2150', '', null, null);
INSERT INTO t_sys_area VALUES ('2151', '421202', '421202', '421202', '咸安区', 'Xian,an Qu', 'Xian,an Qu', ',18,214,2151,', '湖北省-咸宁市-咸安区', '214', '02', '04', '01', '2151', '', null, null);
INSERT INTO t_sys_area VALUES ('2152', '421221', '421221', '421221', '嘉鱼县', 'Jiayu Xian', 'Jiayu Xian', ',18,214,2152,', '湖北省-咸宁市-嘉鱼县', '214', '02', '04', '01', '2152', '', null, null);
INSERT INTO t_sys_area VALUES ('2153', '421222', '421222', '421222', '通城县', 'Tongcheng Xian', 'Tongcheng Xian', ',18,214,2153,', '湖北省-咸宁市-通城县', '214', '02', '04', '01', '2153', '', null, null);
INSERT INTO t_sys_area VALUES ('2154', '421223', '421223', '421223', '崇阳县', 'Chongyang Xian', 'Chongyang Xian', ',18,214,2154,', '湖北省-咸宁市-崇阳县', '214', '02', '04', '01', '2154', '', null, null);
INSERT INTO t_sys_area VALUES ('2155', '421224', '421224', '421224', '通山县', 'Tongshan Xian', 'Tongshan Xian', ',18,214,2155,', '湖北省-咸宁市-通山县', '214', '02', '04', '01', '2155', '', null, null);
INSERT INTO t_sys_area VALUES ('2156', '421281', '421281', '421281', '赤壁市', 'Chibi Shi', 'Chibi Shi', ',18,214,2156,', '湖北省-咸宁市-赤壁市', '214', '02', '04', '01', '2156', '', null, null);
INSERT INTO t_sys_area VALUES ('2157', '421301', '421301', '421301', '市辖区', '1', '1', ',18,215,2157,', '湖北省-随州市-市辖区', '215', '02', '04', '01', '2157', '', null, null);
INSERT INTO t_sys_area VALUES ('2158', '421303', '421303', '421303', '曾都区', 'Zengdu Qu', 'Zengdu Qu', ',18,215,2158,', '湖北省-随州市-曾都区', '215', '02', '04', '01', '2158', '', null, null);
INSERT INTO t_sys_area VALUES ('2159', '421381', '421381', '421381', '广水市', 'Guangshui Shi', 'Guangshui Shi', ',18,215,2159,', '湖北省-随州市-广水市', '215', '02', '04', '01', '2159', '', null, null);
INSERT INTO t_sys_area VALUES ('2160', '422801', '422801', '422801', '恩施市', 'Enshi Shi', 'Enshi Shi', ',18,216,2160,', '湖北省-恩施土家族苗族自治州-恩施市', '216', '02', '04', '01', '2160', '', null, null);
INSERT INTO t_sys_area VALUES ('2161', '422802', '422802', '422802', '利川市', 'Lichuan Shi', 'Lichuan Shi', ',18,216,2161,', '湖北省-恩施土家族苗族自治州-利川市', '216', '02', '04', '01', '2161', '', null, null);
INSERT INTO t_sys_area VALUES ('2162', '422822', '422822', '422822', '建始县', 'Jianshi Xian', 'Jianshi Xian', ',18,216,2162,', '湖北省-恩施土家族苗族自治州-建始县', '216', '02', '04', '01', '2162', '', null, null);
INSERT INTO t_sys_area VALUES ('2163', '422823', '422823', '422823', '巴东县', 'Badong Xian', 'Badong Xian', ',18,216,2163,', '湖北省-恩施土家族苗族自治州-巴东县', '216', '02', '04', '01', '2163', '', null, null);
INSERT INTO t_sys_area VALUES ('2164', '422825', '422825', '422825', '宣恩县', 'Xuan,en Xian', 'Xuan,en Xian', ',18,216,2164,', '湖北省-恩施土家族苗族自治州-宣恩县', '216', '02', '04', '01', '2164', '', null, null);
INSERT INTO t_sys_area VALUES ('2165', '422826', '422826', '422826', '咸丰县', 'Xianfeng Xian', 'Xianfeng Xian', ',18,216,2165,', '湖北省-恩施土家族苗族自治州-咸丰县', '216', '02', '04', '01', '2165', '', null, null);
INSERT INTO t_sys_area VALUES ('2166', '422827', '422827', '422827', '来凤县', 'Laifeng Xian', 'Laifeng Xian', ',18,216,2166,', '湖北省-恩施土家族苗族自治州-来凤县', '216', '02', '04', '01', '2166', '', null, null);
INSERT INTO t_sys_area VALUES ('2167', '422828', '422828', '422828', '鹤峰县', 'Hefeng Xian', 'Hefeng Xian', ',18,216,2167,', '湖北省-恩施土家族苗族自治州-鹤峰县', '216', '02', '04', '01', '2167', '', null, null);
INSERT INTO t_sys_area VALUES ('2168', '429004', '429004', '429004', '仙桃市', 'Xiantao Shi', 'Xiantao Shi', ',18,217,2168,', '湖北省-省直辖县级行政区划-仙桃市', '217', '02', '04', '01', '2168', '', null, null);
INSERT INTO t_sys_area VALUES ('2169', '429005', '429005', '429005', '潜江市', 'Qianjiang Shi', 'Qianjiang Shi', ',18,217,2169,', '湖北省-省直辖县级行政区划-潜江市', '217', '02', '04', '01', '2169', '', null, null);
INSERT INTO t_sys_area VALUES ('2170', '429006', '429006', '429006', '天门市', 'Tianmen Shi', 'Tianmen Shi', ',18,217,2170,', '湖北省-省直辖县级行政区划-天门市', '217', '02', '04', '01', '2170', '', null, null);
INSERT INTO t_sys_area VALUES ('2171', '429021', '429021', '429021', '神农架林区', 'Shennongjia Linqu', 'Shennongjia Linqu', ',18,217,2171,', '湖北省-省直辖县级行政区划-神农架林区', '217', '02', '04', '01', '2171', '', null, null);
INSERT INTO t_sys_area VALUES ('2172', '430101', '430101', '430101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',19,218,2172,', '湖南省-长沙市-市辖区', '218', '02', '04', '01', '2172', '', null, null);
INSERT INTO t_sys_area VALUES ('2173', '430102', '430102', '430102', '芙蓉区', 'Furong Qu', 'Furong Qu', ',19,218,2173,', '湖南省-长沙市-芙蓉区', '218', '02', '04', '01', '2173', '', null, null);
INSERT INTO t_sys_area VALUES ('2174', '430103', '430103', '430103', '天心区', 'Tianxin Qu', 'Tianxin Qu', ',19,218,2174,', '湖南省-长沙市-天心区', '218', '02', '04', '01', '2174', '', null, null);
INSERT INTO t_sys_area VALUES ('2175', '430104', '430104', '430104', '岳麓区', 'Yuelu Qu', 'Yuelu Qu', ',19,218,2175,', '湖南省-长沙市-岳麓区', '218', '02', '04', '01', '2175', '', null, null);
INSERT INTO t_sys_area VALUES ('2176', '430105', '430105', '430105', '开福区', 'Kaifu Qu', 'Kaifu Qu', ',19,218,2176,', '湖南省-长沙市-开福区', '218', '02', '04', '01', '2176', '', null, null);
INSERT INTO t_sys_area VALUES ('2177', '430111', '430111', '430111', '雨花区', 'Yuhua Qu', 'Yuhua Qu', ',19,218,2177,', '湖南省-长沙市-雨花区', '218', '02', '04', '01', '2177', '', null, null);
INSERT INTO t_sys_area VALUES ('2178', '430121', '430121', '430121', '长沙县', 'Changsha Xian', 'Changsha Xian', ',19,218,2178,', '湖南省-长沙市-长沙县', '218', '02', '04', '01', '2178', '', null, null);
INSERT INTO t_sys_area VALUES ('2179', '430122', '430122', '430122', '望城县', 'Wangcheng Xian', 'Wangcheng Xian', ',19,218,2179,', '湖南省-长沙市-望城县', '218', '02', '04', '01', '2179', '', null, null);
INSERT INTO t_sys_area VALUES ('2180', '430124', '430124', '430124', '宁乡县', 'Ningxiang Xian', 'Ningxiang Xian', ',19,218,2180,', '湖南省-长沙市-宁乡县', '218', '02', '04', '01', '2180', '', null, null);
INSERT INTO t_sys_area VALUES ('2181', '430181', '430181', '430181', '浏阳市', 'Liuyang Shi', 'Liuyang Shi', ',19,218,2181,', '湖南省-长沙市-浏阳市', '218', '02', '04', '01', '2181', '', null, null);
INSERT INTO t_sys_area VALUES ('2182', '430201', '430201', '430201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',19,219,2182,', '湖南省-株洲市-市辖区', '219', '02', '04', '01', '2182', '', null, null);
INSERT INTO t_sys_area VALUES ('2183', '430202', '430202', '430202', '荷塘区', 'Hetang Qu', 'Hetang Qu', ',19,219,2183,', '湖南省-株洲市-荷塘区', '219', '02', '04', '01', '2183', '', null, null);
INSERT INTO t_sys_area VALUES ('2184', '430203', '430203', '430203', '芦淞区', 'Lusong Qu', 'Lusong Qu', ',19,219,2184,', '湖南省-株洲市-芦淞区', '219', '02', '04', '01', '2184', '', null, null);
INSERT INTO t_sys_area VALUES ('2185', '430204', '430204', '430204', '石峰区', 'Shifeng Qu', 'Shifeng Qu', ',19,219,2185,', '湖南省-株洲市-石峰区', '219', '02', '04', '01', '2185', '', null, null);
INSERT INTO t_sys_area VALUES ('2186', '430211', '430211', '430211', '天元区', 'Tianyuan Qu', 'Tianyuan Qu', ',19,219,2186,', '湖南省-株洲市-天元区', '219', '02', '04', '01', '2186', '', null, null);
INSERT INTO t_sys_area VALUES ('2187', '430221', '430221', '430221', '株洲县', 'Zhuzhou Xian', 'Zhuzhou Xian', ',19,219,2187,', '湖南省-株洲市-株洲县', '219', '02', '04', '01', '2187', '', null, null);
INSERT INTO t_sys_area VALUES ('2188', '430223', '430223', '430223', '攸县', 'You Xian', 'You Xian', ',19,219,2188,', '湖南省-株洲市-攸县', '219', '02', '04', '01', '2188', '', null, null);
INSERT INTO t_sys_area VALUES ('2189', '430224', '430224', '430224', '茶陵县', 'Chaling Xian', 'Chaling Xian', ',19,219,2189,', '湖南省-株洲市-茶陵县', '219', '02', '04', '01', '2189', '', null, null);
INSERT INTO t_sys_area VALUES ('2190', '430225', '430225', '430225', '炎陵县', 'Yanling Xian', 'Yanling Xian', ',19,219,2190,', '湖南省-株洲市-炎陵县', '219', '02', '04', '01', '2190', '', null, null);
INSERT INTO t_sys_area VALUES ('2191', '430281', '430281', '430281', '醴陵市', 'Liling Shi', 'Liling Shi', ',19,219,2191,', '湖南省-株洲市-醴陵市', '219', '02', '04', '01', '2191', '', null, null);
INSERT INTO t_sys_area VALUES ('2192', '430301', '430301', '430301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',19,220,2192,', '湖南省-湘潭市-市辖区', '220', '02', '04', '01', '2192', '', null, null);
INSERT INTO t_sys_area VALUES ('2193', '430302', '430302', '430302', '雨湖区', 'Yuhu Qu', 'Yuhu Qu', ',19,220,2193,', '湖南省-湘潭市-雨湖区', '220', '02', '04', '01', '2193', '', null, null);
INSERT INTO t_sys_area VALUES ('2194', '430304', '430304', '430304', '岳塘区', 'Yuetang Qu', 'Yuetang Qu', ',19,220,2194,', '湖南省-湘潭市-岳塘区', '220', '02', '04', '01', '2194', '', null, null);
INSERT INTO t_sys_area VALUES ('2195', '430321', '430321', '430321', '湘潭县', 'Xiangtan Qu', 'Xiangtan Qu', ',19,220,2195,', '湖南省-湘潭市-湘潭县', '220', '02', '04', '01', '2195', '', null, null);
INSERT INTO t_sys_area VALUES ('2196', '430381', '430381', '430381', '湘乡市', 'Xiangxiang Shi', 'Xiangxiang Shi', ',19,220,2196,', '湖南省-湘潭市-湘乡市', '220', '02', '04', '01', '2196', '', null, null);
INSERT INTO t_sys_area VALUES ('2197', '430382', '430382', '430382', '韶山市', 'Shaoshan Shi', 'Shaoshan Shi', ',19,220,2197,', '湖南省-湘潭市-韶山市', '220', '02', '04', '01', '2197', '', null, null);
INSERT INTO t_sys_area VALUES ('2198', '430401', '430401', '430401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',19,221,2198,', '湖南省-衡阳市-市辖区', '221', '02', '04', '01', '2198', '', null, null);
INSERT INTO t_sys_area VALUES ('2199', '430405', '430405', '430405', '珠晖区', 'Zhuhui Qu', 'Zhuhui Qu', ',19,221,2199,', '湖南省-衡阳市-珠晖区', '221', '02', '04', '01', '2199', '', null, null);
INSERT INTO t_sys_area VALUES ('2200', '430406', '430406', '430406', '雁峰区', 'Yanfeng Qu', 'Yanfeng Qu', ',19,221,2200,', '湖南省-衡阳市-雁峰区', '221', '02', '04', '01', '2200', '', null, null);
INSERT INTO t_sys_area VALUES ('2201', '430407', '430407', '430407', '石鼓区', 'Shigu Qu', 'Shigu Qu', ',19,221,2201,', '湖南省-衡阳市-石鼓区', '221', '02', '04', '01', '2201', '', null, null);
INSERT INTO t_sys_area VALUES ('2202', '430408', '430408', '430408', '蒸湘区', 'Zhengxiang Qu', 'Zhengxiang Qu', ',19,221,2202,', '湖南省-衡阳市-蒸湘区', '221', '02', '04', '01', '2202', '', null, null);
INSERT INTO t_sys_area VALUES ('2203', '430412', '430412', '430412', '南岳区', 'Nanyue Qu', 'Nanyue Qu', ',19,221,2203,', '湖南省-衡阳市-南岳区', '221', '02', '04', '01', '2203', '', null, null);
INSERT INTO t_sys_area VALUES ('2204', '430421', '430421', '430421', '衡阳县', 'Hengyang Xian', 'Hengyang Xian', ',19,221,2204,', '湖南省-衡阳市-衡阳县', '221', '02', '04', '01', '2204', '', null, null);
INSERT INTO t_sys_area VALUES ('2205', '430422', '430422', '430422', '衡南县', 'Hengnan Xian', 'Hengnan Xian', ',19,221,2205,', '湖南省-衡阳市-衡南县', '221', '02', '04', '01', '2205', '', null, null);
INSERT INTO t_sys_area VALUES ('2206', '430423', '430423', '430423', '衡山县', 'Hengshan Xian', 'Hengshan Xian', ',19,221,2206,', '湖南省-衡阳市-衡山县', '221', '02', '04', '01', '2206', '', null, null);
INSERT INTO t_sys_area VALUES ('2207', '430424', '430424', '430424', '衡东县', 'Hengdong Xian', 'Hengdong Xian', ',19,221,2207,', '湖南省-衡阳市-衡东县', '221', '02', '04', '01', '2207', '', null, null);
INSERT INTO t_sys_area VALUES ('2208', '430426', '430426', '430426', '祁东县', 'Qidong Xian', 'Qidong Xian', ',19,221,2208,', '湖南省-衡阳市-祁东县', '221', '02', '04', '01', '2208', '', null, null);
INSERT INTO t_sys_area VALUES ('2209', '430481', '430481', '430481', '耒阳市', 'Leiyang Shi', 'Leiyang Shi', ',19,221,2209,', '湖南省-衡阳市-耒阳市', '221', '02', '04', '01', '2209', '', null, null);
INSERT INTO t_sys_area VALUES ('2210', '430482', '430482', '430482', '常宁市', 'Changning Shi', 'Changning Shi', ',19,221,2210,', '湖南省-衡阳市-常宁市', '221', '02', '04', '01', '2210', '', null, null);
INSERT INTO t_sys_area VALUES ('2211', '430501', '430501', '430501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',19,222,2211,', '湖南省-邵阳市-市辖区', '222', '02', '04', '01', '2211', '', null, null);
INSERT INTO t_sys_area VALUES ('2212', '430502', '430502', '430502', '双清区', 'Shuangqing Qu', 'Shuangqing Qu', ',19,222,2212,', '湖南省-邵阳市-双清区', '222', '02', '04', '01', '2212', '', null, null);
INSERT INTO t_sys_area VALUES ('2213', '430503', '430503', '430503', '大祥区', 'Daxiang Qu', 'Daxiang Qu', ',19,222,2213,', '湖南省-邵阳市-大祥区', '222', '02', '04', '01', '2213', '', null, null);
INSERT INTO t_sys_area VALUES ('2214', '430511', '430511', '430511', '北塔区', 'Beita Qu', 'Beita Qu', ',19,222,2214,', '湖南省-邵阳市-北塔区', '222', '02', '04', '01', '2214', '', null, null);
INSERT INTO t_sys_area VALUES ('2215', '430521', '430521', '430521', '邵东县', 'Shaodong Xian', 'Shaodong Xian', ',19,222,2215,', '湖南省-邵阳市-邵东县', '222', '02', '04', '01', '2215', '', null, null);
INSERT INTO t_sys_area VALUES ('2216', '430522', '430522', '430522', '新邵县', 'Xinshao Xian', 'Xinshao Xian', ',19,222,2216,', '湖南省-邵阳市-新邵县', '222', '02', '04', '01', '2216', '', null, null);
INSERT INTO t_sys_area VALUES ('2217', '430523', '430523', '430523', '邵阳县', 'Shaoyang Xian', 'Shaoyang Xian', ',19,222,2217,', '湖南省-邵阳市-邵阳县', '222', '02', '04', '01', '2217', '', null, null);
INSERT INTO t_sys_area VALUES ('2218', '430524', '430524', '430524', '隆回县', 'Longhui Xian', 'Longhui Xian', ',19,222,2218,', '湖南省-邵阳市-隆回县', '222', '02', '04', '01', '2218', '', null, null);
INSERT INTO t_sys_area VALUES ('2219', '430525', '430525', '430525', '洞口县', 'Dongkou Xian', 'Dongkou Xian', ',19,222,2219,', '湖南省-邵阳市-洞口县', '222', '02', '04', '01', '2219', '', null, null);
INSERT INTO t_sys_area VALUES ('2220', '430527', '430527', '430527', '绥宁县', 'Suining Xian', 'Suining Xian', ',19,222,2220,', '湖南省-邵阳市-绥宁县', '222', '02', '04', '01', '2220', '', null, null);
INSERT INTO t_sys_area VALUES ('2221', '430528', '430528', '430528', '新宁县', 'Xinning Xian', 'Xinning Xian', ',19,222,2221,', '湖南省-邵阳市-新宁县', '222', '02', '04', '01', '2221', '', null, null);
INSERT INTO t_sys_area VALUES ('2222', '430529', '430529', '430529', '城步苗族自治县', 'Chengbu Miaozu Zizhixian', 'Chengbu Miaozu Zizhixian', ',19,222,2222,', '湖南省-邵阳市-城步苗族自治县', '222', '02', '04', '01', '2222', '', null, null);
INSERT INTO t_sys_area VALUES ('2223', '430581', '430581', '430581', '武冈市', 'Wugang Shi', 'Wugang Shi', ',19,222,2223,', '湖南省-邵阳市-武冈市', '222', '02', '04', '01', '2223', '', null, null);
INSERT INTO t_sys_area VALUES ('2224', '430601', '430601', '430601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',19,223,2224,', '湖南省-岳阳市-市辖区', '223', '02', '04', '01', '2224', '', null, null);
INSERT INTO t_sys_area VALUES ('2225', '430602', '430602', '430602', '岳阳楼区', 'Yueyanglou Qu', 'Yueyanglou Qu', ',19,223,2225,', '湖南省-岳阳市-岳阳楼区', '223', '02', '04', '01', '2225', '', null, null);
INSERT INTO t_sys_area VALUES ('2226', '430603', '430603', '430603', '云溪区', 'Yunxi Qu', 'Yunxi Qu', ',19,223,2226,', '湖南省-岳阳市-云溪区', '223', '02', '04', '01', '2226', '', null, null);
INSERT INTO t_sys_area VALUES ('2227', '430611', '430611', '430611', '君山区', 'Junshan Qu', 'Junshan Qu', ',19,223,2227,', '湖南省-岳阳市-君山区', '223', '02', '04', '01', '2227', '', null, null);
INSERT INTO t_sys_area VALUES ('2228', '430621', '430621', '430621', '岳阳县', 'Yueyang Xian', 'Yueyang Xian', ',19,223,2228,', '湖南省-岳阳市-岳阳县', '223', '02', '04', '01', '2228', '', null, null);
INSERT INTO t_sys_area VALUES ('2229', '430623', '430623', '430623', '华容县', 'Huarong Xian', 'Huarong Xian', ',19,223,2229,', '湖南省-岳阳市-华容县', '223', '02', '04', '01', '2229', '', null, null);
INSERT INTO t_sys_area VALUES ('2230', '430624', '430624', '430624', '湘阴县', 'Xiangyin Xian', 'Xiangyin Xian', ',19,223,2230,', '湖南省-岳阳市-湘阴县', '223', '02', '04', '01', '2230', '', null, null);
INSERT INTO t_sys_area VALUES ('2231', '430626', '430626', '430626', '平江县', 'Pingjiang Xian', 'Pingjiang Xian', ',19,223,2231,', '湖南省-岳阳市-平江县', '223', '02', '04', '01', '2231', '', null, null);
INSERT INTO t_sys_area VALUES ('2232', '430681', '430681', '430681', '汨罗市', 'Miluo Shi', 'Miluo Shi', ',19,223,2232,', '湖南省-岳阳市-汨罗市', '223', '02', '04', '01', '2232', '', null, null);
INSERT INTO t_sys_area VALUES ('2233', '430682', '430682', '430682', '临湘市', 'Linxiang Shi', 'Linxiang Shi', ',19,223,2233,', '湖南省-岳阳市-临湘市', '223', '02', '04', '01', '2233', '', null, null);
INSERT INTO t_sys_area VALUES ('2234', '430701', '430701', '430701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',19,224,2234,', '湖南省-常德市-市辖区', '224', '02', '04', '01', '2234', '', null, null);
INSERT INTO t_sys_area VALUES ('2235', '430702', '430702', '430702', '武陵区', 'Wuling Qu', 'Wuling Qu', ',19,224,2235,', '湖南省-常德市-武陵区', '224', '02', '04', '01', '2235', '', null, null);
INSERT INTO t_sys_area VALUES ('2236', '430703', '430703', '430703', '鼎城区', 'Dingcheng Qu', 'Dingcheng Qu', ',19,224,2236,', '湖南省-常德市-鼎城区', '224', '02', '04', '01', '2236', '', null, null);
INSERT INTO t_sys_area VALUES ('2237', '430721', '430721', '430721', '安乡县', 'Anxiang Xian', 'Anxiang Xian', ',19,224,2237,', '湖南省-常德市-安乡县', '224', '02', '04', '01', '2237', '', null, null);
INSERT INTO t_sys_area VALUES ('2238', '430722', '430722', '430722', '汉寿县', 'Hanshou Xian', 'Hanshou Xian', ',19,224,2238,', '湖南省-常德市-汉寿县', '224', '02', '04', '01', '2238', '', null, null);
INSERT INTO t_sys_area VALUES ('2239', '430723', '430723', '430723', '澧县', 'Li Xian', 'Li Xian', ',19,224,2239,', '湖南省-常德市-澧县', '224', '02', '04', '01', '2239', '', null, null);
INSERT INTO t_sys_area VALUES ('2240', '430724', '430724', '430724', '临澧县', 'Linli Xian', 'Linli Xian', ',19,224,2240,', '湖南省-常德市-临澧县', '224', '02', '04', '01', '2240', '', null, null);
INSERT INTO t_sys_area VALUES ('2241', '430725', '430725', '430725', '桃源县', 'Taoyuan Xian', 'Taoyuan Xian', ',19,224,2241,', '湖南省-常德市-桃源县', '224', '02', '04', '01', '2241', '', null, null);
INSERT INTO t_sys_area VALUES ('2242', '430726', '430726', '430726', '石门县', 'Shimen Xian', 'Shimen Xian', ',19,224,2242,', '湖南省-常德市-石门县', '224', '02', '04', '01', '2242', '', null, null);
INSERT INTO t_sys_area VALUES ('2243', '430781', '430781', '430781', '津市市', 'Jinshi Shi', 'Jinshi Shi', ',19,224,2243,', '湖南省-常德市-津市市', '224', '02', '04', '01', '2243', '', null, null);
INSERT INTO t_sys_area VALUES ('2244', '430801', '430801', '430801', '市辖区', 'Shixiaqu', 'Shixiaqu', ',19,225,2244,', '湖南省-张家界市-市辖区', '225', '02', '04', '01', '2244', '', null, null);
INSERT INTO t_sys_area VALUES ('2245', '430802', '430802', '430802', '永定区', 'Yongding Qu', 'Yongding Qu', ',19,225,2245,', '湖南省-张家界市-永定区', '225', '02', '04', '01', '2245', '', null, null);
INSERT INTO t_sys_area VALUES ('2246', '430811', '430811', '430811', '武陵源区', 'Wulingyuan Qu', 'Wulingyuan Qu', ',19,225,2246,', '湖南省-张家界市-武陵源区', '225', '02', '04', '01', '2246', '', null, null);
INSERT INTO t_sys_area VALUES ('2247', '430821', '430821', '430821', '慈利县', 'Cili Xian', 'Cili Xian', ',19,225,2247,', '湖南省-张家界市-慈利县', '225', '02', '04', '01', '2247', '', null, null);
INSERT INTO t_sys_area VALUES ('2248', '430822', '430822', '430822', '桑植县', 'Sangzhi Xian', 'Sangzhi Xian', ',19,225,2248,', '湖南省-张家界市-桑植县', '225', '02', '04', '01', '2248', '', null, null);
INSERT INTO t_sys_area VALUES ('2249', '430901', '430901', '430901', '市辖区', 'Shixiaqu', 'Shixiaqu', ',19,226,2249,', '湖南省-益阳市-市辖区', '226', '02', '04', '01', '2249', '', null, null);
INSERT INTO t_sys_area VALUES ('2250', '430902', '430902', '430902', '资阳区', 'Ziyang Qu', 'Ziyang Qu', ',19,226,2250,', '湖南省-益阳市-资阳区', '226', '02', '04', '01', '2250', '', null, null);
INSERT INTO t_sys_area VALUES ('2251', '430903', '430903', '430903', '赫山区', 'Heshan Qu', 'Heshan Qu', ',19,226,2251,', '湖南省-益阳市-赫山区', '226', '02', '04', '01', '2251', '', null, null);
INSERT INTO t_sys_area VALUES ('2252', '430921', '430921', '430921', '南县', 'Nan Xian', 'Nan Xian', ',19,226,2252,', '湖南省-益阳市-南县', '226', '02', '04', '01', '2252', '', null, null);
INSERT INTO t_sys_area VALUES ('2253', '430922', '430922', '430922', '桃江县', 'Taojiang Xian', 'Taojiang Xian', ',19,226,2253,', '湖南省-益阳市-桃江县', '226', '02', '04', '01', '2253', '', null, null);
INSERT INTO t_sys_area VALUES ('2254', '430923', '430923', '430923', '安化县', 'Anhua Xian', 'Anhua Xian', ',19,226,2254,', '湖南省-益阳市-安化县', '226', '02', '04', '01', '2254', '', null, null);
INSERT INTO t_sys_area VALUES ('2255', '430981', '430981', '430981', '沅江市', 'Yuanjiang Shi', 'Yuanjiang Shi', ',19,226,2255,', '湖南省-益阳市-沅江市', '226', '02', '04', '01', '2255', '', null, null);
INSERT INTO t_sys_area VALUES ('2256', '431001', '431001', '431001', '市辖区', 'Shixiaqu', 'Shixiaqu', ',19,227,2256,', '湖南省-郴州市-市辖区', '227', '02', '04', '01', '2256', '', null, null);
INSERT INTO t_sys_area VALUES ('2257', '431002', '431002', '431002', '北湖区', 'Beihu Qu', 'Beihu Qu', ',19,227,2257,', '湖南省-郴州市-北湖区', '227', '02', '04', '01', '2257', '', null, null);
INSERT INTO t_sys_area VALUES ('2258', '431003', '431003', '431003', '苏仙区', 'Suxian Qu', 'Suxian Qu', ',19,227,2258,', '湖南省-郴州市-苏仙区', '227', '02', '04', '01', '2258', '', null, null);
INSERT INTO t_sys_area VALUES ('2259', '431021', '431021', '431021', '桂阳县', 'Guiyang Xian', 'Guiyang Xian', ',19,227,2259,', '湖南省-郴州市-桂阳县', '227', '02', '04', '01', '2259', '', null, null);
INSERT INTO t_sys_area VALUES ('2260', '431022', '431022', '431022', '宜章县', 'yizhang Xian', 'yizhang Xian', ',19,227,2260,', '湖南省-郴州市-宜章县', '227', '02', '04', '01', '2260', '', null, null);
INSERT INTO t_sys_area VALUES ('2261', '431023', '431023', '431023', '永兴县', 'Yongxing Xian', 'Yongxing Xian', ',19,227,2261,', '湖南省-郴州市-永兴县', '227', '02', '04', '01', '2261', '', null, null);
INSERT INTO t_sys_area VALUES ('2262', '431024', '431024', '431024', '嘉禾县', 'Jiahe Xian', 'Jiahe Xian', ',19,227,2262,', '湖南省-郴州市-嘉禾县', '227', '02', '04', '01', '2262', '', null, null);
INSERT INTO t_sys_area VALUES ('2263', '431025', '431025', '431025', '临武县', 'Linwu Xian', 'Linwu Xian', ',19,227,2263,', '湖南省-郴州市-临武县', '227', '02', '04', '01', '2263', '', null, null);
INSERT INTO t_sys_area VALUES ('2264', '431026', '431026', '431026', '汝城县', 'Rucheng Xian', 'Rucheng Xian', ',19,227,2264,', '湖南省-郴州市-汝城县', '227', '02', '04', '01', '2264', '', null, null);
INSERT INTO t_sys_area VALUES ('2265', '431027', '431027', '431027', '桂东县', 'Guidong Xian', 'Guidong Xian', ',19,227,2265,', '湖南省-郴州市-桂东县', '227', '02', '04', '01', '2265', '', null, null);
INSERT INTO t_sys_area VALUES ('2266', '431028', '431028', '431028', '安仁县', 'Anren Xian', 'Anren Xian', ',19,227,2266,', '湖南省-郴州市-安仁县', '227', '02', '04', '01', '2266', '', null, null);
INSERT INTO t_sys_area VALUES ('2267', '431081', '431081', '431081', '资兴市', 'Zixing Shi', 'Zixing Shi', ',19,227,2267,', '湖南省-郴州市-资兴市', '227', '02', '04', '01', '2267', '', null, null);
INSERT INTO t_sys_area VALUES ('2268', '431101', '431101', '431101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',19,228,2268,', '湖南省-永州市-市辖区', '228', '02', '04', '01', '2268', '', null, null);
INSERT INTO t_sys_area VALUES ('2270', '431103', '431103', '431103', '冷水滩区', 'Lengshuitan Qu', 'Lengshuitan Qu', ',19,228,2270,', '湖南省-永州市-冷水滩区', '228', '02', '04', '01', '2270', '', null, null);
INSERT INTO t_sys_area VALUES ('2271', '431121', '431121', '431121', '祁阳县', 'Qiyang Xian', 'Qiyang Xian', ',19,228,2271,', '湖南省-永州市-祁阳县', '228', '02', '04', '01', '2271', '', null, null);
INSERT INTO t_sys_area VALUES ('2272', '431122', '431122', '431122', '东安县', 'Dong,an Xian', 'Dong,an Xian', ',19,228,2272,', '湖南省-永州市-东安县', '228', '02', '04', '01', '2272', '', null, null);
INSERT INTO t_sys_area VALUES ('2273', '431123', '431123', '431123', '双牌县', 'Shuangpai Xian', 'Shuangpai Xian', ',19,228,2273,', '湖南省-永州市-双牌县', '228', '02', '04', '01', '2273', '', null, null);
INSERT INTO t_sys_area VALUES ('2274', '431124', '431124', '431124', '道县', 'Dao Xian', 'Dao Xian', ',19,228,2274,', '湖南省-永州市-道县', '228', '02', '04', '01', '2274', '', null, null);
INSERT INTO t_sys_area VALUES ('2275', '431125', '431125', '431125', '江永县', 'Jiangyong Xian', 'Jiangyong Xian', ',19,228,2275,', '湖南省-永州市-江永县', '228', '02', '04', '01', '2275', '', null, null);
INSERT INTO t_sys_area VALUES ('2276', '431126', '431126', '431126', '宁远县', 'Ningyuan Xian', 'Ningyuan Xian', ',19,228,2276,', '湖南省-永州市-宁远县', '228', '02', '04', '01', '2276', '', null, null);
INSERT INTO t_sys_area VALUES ('2277', '431127', '431127', '431127', '蓝山县', 'Lanshan Xian', 'Lanshan Xian', ',19,228,2277,', '湖南省-永州市-蓝山县', '228', '02', '04', '01', '2277', '', null, null);
INSERT INTO t_sys_area VALUES ('2278', '431128', '431128', '431128', '新田县', 'Xintian Xian', 'Xintian Xian', ',19,228,2278,', '湖南省-永州市-新田县', '228', '02', '04', '01', '2278', '', null, null);
INSERT INTO t_sys_area VALUES ('2279', '431129', '431129', '431129', '江华瑶族自治县', 'Jianghua Yaozu Zizhixian', 'Jianghua Yaozu Zizhixian', ',19,228,2279,', '湖南省-永州市-江华瑶族自治县', '228', '02', '04', '01', '2279', '', null, null);
INSERT INTO t_sys_area VALUES ('2280', '431201', '431201', '431201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',19,229,2280,', '湖南省-怀化市-市辖区', '229', '02', '04', '01', '2280', '', null, null);
INSERT INTO t_sys_area VALUES ('2281', '431202', '431202', '431202', '鹤城区', 'Hecheng Qu', 'Hecheng Qu', ',19,229,2281,', '湖南省-怀化市-鹤城区', '229', '02', '04', '01', '2281', '', null, null);
INSERT INTO t_sys_area VALUES ('2282', '431221', '431221', '431221', '中方县', 'Zhongfang Xian', 'Zhongfang Xian', ',19,229,2282,', '湖南省-怀化市-中方县', '229', '02', '04', '01', '2282', '', null, null);
INSERT INTO t_sys_area VALUES ('2283', '431222', '431222', '431222', '沅陵县', 'Yuanling Xian', 'Yuanling Xian', ',19,229,2283,', '湖南省-怀化市-沅陵县', '229', '02', '04', '01', '2283', '', null, null);
INSERT INTO t_sys_area VALUES ('2284', '431223', '431223', '431223', '辰溪县', 'Chenxi Xian', 'Chenxi Xian', ',19,229,2284,', '湖南省-怀化市-辰溪县', '229', '02', '04', '01', '2284', '', null, null);
INSERT INTO t_sys_area VALUES ('2285', '431224', '431224', '431224', '溆浦县', 'Xupu Xian', 'Xupu Xian', ',19,229,2285,', '湖南省-怀化市-溆浦县', '229', '02', '04', '01', '2285', '', null, null);
INSERT INTO t_sys_area VALUES ('2286', '431225', '431225', '431225', '会同县', 'Huitong Xian', 'Huitong Xian', ',19,229,2286,', '湖南省-怀化市-会同县', '229', '02', '04', '01', '2286', '', null, null);
INSERT INTO t_sys_area VALUES ('2287', '431226', '431226', '431226', '麻阳苗族自治县', 'Mayang Miaozu Zizhixian', 'Mayang Miaozu Zizhixian', ',19,229,2287,', '湖南省-怀化市-麻阳苗族自治县', '229', '02', '04', '01', '2287', '', null, null);
INSERT INTO t_sys_area VALUES ('2288', '431227', '431227', '431227', '新晃侗族自治县', 'Xinhuang Dongzu Zizhixian', 'Xinhuang Dongzu Zizhixian', ',19,229,2288,', '湖南省-怀化市-新晃侗族自治县', '229', '02', '04', '01', '2288', '', null, null);
INSERT INTO t_sys_area VALUES ('2289', '431228', '431228', '431228', '芷江侗族自治县', 'Zhijiang Dongzu Zizhixian', 'Zhijiang Dongzu Zizhixian', ',19,229,2289,', '湖南省-怀化市-芷江侗族自治县', '229', '02', '04', '01', '2289', '', null, null);
INSERT INTO t_sys_area VALUES ('2290', '431229', '431229', '431229', '靖州苗族侗族自治县', 'Jingzhou Miaozu Dongzu Zizhixian', 'Jingzhou Miaozu Dongzu Zizhixian', ',19,229,2290,', '湖南省-怀化市-靖州苗族侗族自治县', '229', '02', '04', '01', '2290', '', null, null);
INSERT INTO t_sys_area VALUES ('2291', '431230', '431230', '431230', '通道侗族自治县', 'Tongdao Dongzu Zizhixian', 'Tongdao Dongzu Zizhixian', ',19,229,2291,', '湖南省-怀化市-通道侗族自治县', '229', '02', '04', '01', '2291', '', null, null);
INSERT INTO t_sys_area VALUES ('2292', '431281', '431281', '431281', '洪江市', 'Hongjiang Shi', 'Hongjiang Shi', ',19,229,2292,', '湖南省-怀化市-洪江市', '229', '02', '04', '01', '2292', '', null, null);
INSERT INTO t_sys_area VALUES ('2293', '431301', '431301', '431301', '市辖区', '1', '1', ',19,230,2293,', '湖南省-娄底市-市辖区', '230', '02', '04', '01', '2293', '', null, null);
INSERT INTO t_sys_area VALUES ('2294', '431302', '431302', '431302', '娄星区', 'Louxing Qu', 'Louxing Qu', ',19,230,2294,', '湖南省-娄底市-娄星区', '230', '02', '04', '01', '2294', '', null, null);
INSERT INTO t_sys_area VALUES ('2295', '431321', '431321', '431321', '双峰县', 'Shuangfeng Xian', 'Shuangfeng Xian', ',19,230,2295,', '湖南省-娄底市-双峰县', '230', '02', '04', '01', '2295', '', null, null);
INSERT INTO t_sys_area VALUES ('2296', '431322', '431322', '431322', '新化县', 'Xinhua Xian', 'Xinhua Xian', ',19,230,2296,', '湖南省-娄底市-新化县', '230', '02', '04', '01', '2296', '', null, null);
INSERT INTO t_sys_area VALUES ('2297', '431381', '431381', '431381', '冷水江市', 'Lengshuijiang Shi', 'Lengshuijiang Shi', ',19,230,2297,', '湖南省-娄底市-冷水江市', '230', '02', '04', '01', '2297', '', null, null);
INSERT INTO t_sys_area VALUES ('2298', '431382', '431382', '431382', '涟源市', 'Lianyuan Shi', 'Lianyuan Shi', ',19,230,2298,', '湖南省-娄底市-涟源市', '230', '02', '04', '01', '2298', '', null, null);
INSERT INTO t_sys_area VALUES ('2299', '433101', '433101', '433101', '吉首市', 'Jishou Shi', 'Jishou Shi', ',19,231,2299,', '湖南省-湘西土家族苗族自治州-吉首市', '231', '02', '04', '01', '2299', '', null, null);
INSERT INTO t_sys_area VALUES ('2300', '433122', '433122', '433122', '泸溪县', 'Luxi Xian', 'Luxi Xian', ',19,231,2300,', '湖南省-湘西土家族苗族自治州-泸溪县', '231', '02', '04', '01', '2300', '', null, null);
INSERT INTO t_sys_area VALUES ('2301', '433123', '433123', '433123', '凤凰县', 'Fenghuang Xian', 'Fenghuang Xian', ',19,231,2301,', '湖南省-湘西土家族苗族自治州-凤凰县', '231', '02', '04', '01', '2301', '', null, null);
INSERT INTO t_sys_area VALUES ('2302', '433124', '433124', '433124', '花垣县', 'Huayuan Xian', 'Huayuan Xian', ',19,231,2302,', '湖南省-湘西土家族苗族自治州-花垣县', '231', '02', '04', '01', '2302', '', null, null);
INSERT INTO t_sys_area VALUES ('2303', '433125', '433125', '433125', '保靖县', 'Baojing Xian', 'Baojing Xian', ',19,231,2303,', '湖南省-湘西土家族苗族自治州-保靖县', '231', '02', '04', '01', '2303', '', null, null);
INSERT INTO t_sys_area VALUES ('2304', '433126', '433126', '433126', '古丈县', 'Guzhang Xian', 'Guzhang Xian', ',19,231,2304,', '湖南省-湘西土家族苗族自治州-古丈县', '231', '02', '04', '01', '2304', '', null, null);
INSERT INTO t_sys_area VALUES ('2305', '433127', '433127', '433127', '永顺县', 'Yongshun Xian', 'Yongshun Xian', ',19,231,2305,', '湖南省-湘西土家族苗族自治州-永顺县', '231', '02', '04', '01', '2305', '', null, null);
INSERT INTO t_sys_area VALUES ('2306', '433130', '433130', '433130', '龙山县', 'Longshan Xian', 'Longshan Xian', ',19,231,2306,', '湖南省-湘西土家族苗族自治州-龙山县', '231', '02', '04', '01', '2306', '', null, null);
INSERT INTO t_sys_area VALUES ('2307', '440101', '440101', '440101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,232,2307,', '广东省-广州市-市辖区', '232', '02', '04', '01', '2307', '', null, null);
INSERT INTO t_sys_area VALUES ('2308', '440115', '440115', '440115', '南沙区', 'Nansha Qu', 'Nansha Qu', ',20,232,2308,', '广东省-广州市-南沙区', '232', '02', '04', '01', '2308', '', null, null);
INSERT INTO t_sys_area VALUES ('2309', '440103', '440103', '440103', '荔湾区', 'Liwan Qu', 'Liwan Qu', ',20,232,2309,', '广东省-广州市-荔湾区', '232', '02', '04', '01', '2309', '', null, null);
INSERT INTO t_sys_area VALUES ('2310', '440104', '440104', '440104', '越秀区', 'Yuexiu Qu', 'Yuexiu Qu', ',20,232,2310,', '广东省-广州市-越秀区', '232', '02', '04', '01', '2310', '', null, null);
INSERT INTO t_sys_area VALUES ('2311', '440105', '440105', '440105', '海珠区', 'Haizhu Qu', 'Haizhu Qu', ',20,232,2311,', '广东省-广州市-海珠区', '232', '02', '04', '01', '2311', '', null, null);
INSERT INTO t_sys_area VALUES ('2312', '440106', '440106', '440106', '天河区', 'Tianhe Qu', 'Tianhe Qu', ',20,232,2312,', '广东省-广州市-天河区', '232', '02', '04', '01', '2312', '', null, null);
INSERT INTO t_sys_area VALUES ('2313', '440116', '440116', '440116', '萝岗区', 'Luogang Qu', 'Luogang Qu', ',20,232,2313,', '广东省-广州市-萝岗区', '232', '02', '04', '01', '2313', '', null, null);
INSERT INTO t_sys_area VALUES ('2314', '440111', '440111', '440111', '白云区', 'Baiyun Qu', 'Baiyun Qu', ',20,232,2314,', '广东省-广州市-白云区', '232', '02', '04', '01', '2314', '', null, null);
INSERT INTO t_sys_area VALUES ('2315', '440112', '440112', '440112', '黄埔区', 'Huangpu Qu', 'Huangpu Qu', ',20,232,2315,', '广东省-广州市-黄埔区', '232', '02', '04', '01', '2315', '', null, null);
INSERT INTO t_sys_area VALUES ('2316', '440113', '440113', '440113', '番禺区', 'Panyu Qu', 'Panyu Qu', ',20,232,2316,', '广东省-广州市-番禺区', '232', '02', '04', '01', '2316', '', null, null);
INSERT INTO t_sys_area VALUES ('2317', '440114', '440114', '440114', '花都区', 'Huadu Qu', 'Huadu Qu', ',20,232,2317,', '广东省-广州市-花都区', '232', '02', '04', '01', '2317', '', null, null);
INSERT INTO t_sys_area VALUES ('2318', '440183', '440183', '440183', '增城市', 'Zengcheng Shi', 'Zengcheng Shi', ',20,232,2318,', '广东省-广州市-增城市', '232', '02', '04', '01', '2318', '', null, null);
INSERT INTO t_sys_area VALUES ('2319', '440184', '440184', '440184', '从化市', 'Conghua Shi', 'Conghua Shi', ',20,232,2319,', '广东省-广州市-从化市', '232', '02', '04', '01', '2319', '', null, null);
INSERT INTO t_sys_area VALUES ('2320', '440201', '440201', '440201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,233,2320,', '广东省-韶关市-市辖区', '233', '02', '04', '01', '2320', '', null, null);
INSERT INTO t_sys_area VALUES ('2321', '440203', '440203', '440203', '武江区', 'Wujiang Qu', 'Wujiang Qu', ',20,233,2321,', '广东省-韶关市-武江区', '233', '02', '04', '01', '2321', '', null, null);
INSERT INTO t_sys_area VALUES ('2322', '440204', '440204', '440204', '浈江区', 'Zhenjiang Qu', 'Zhenjiang Qu', ',20,233,2322,', '广东省-韶关市-浈江区', '233', '02', '04', '01', '2322', '', null, null);
INSERT INTO t_sys_area VALUES ('2323', '440205', '440205', '440205', '曲江区', 'Qujiang Qu', 'Qujiang Qu', ',20,233,2323,', '广东省-韶关市-曲江区', '233', '02', '04', '01', '2323', '', null, null);
INSERT INTO t_sys_area VALUES ('2324', '440222', '440222', '440222', '始兴县', 'Shixing Xian', 'Shixing Xian', ',20,233,2324,', '广东省-韶关市-始兴县', '233', '02', '04', '01', '2324', '', null, null);
INSERT INTO t_sys_area VALUES ('2325', '440224', '440224', '440224', '仁化县', 'Renhua Xian', 'Renhua Xian', ',20,233,2325,', '广东省-韶关市-仁化县', '233', '02', '04', '01', '2325', '', null, null);
INSERT INTO t_sys_area VALUES ('2326', '440229', '440229', '440229', '翁源县', 'Wengyuan Xian', 'Wengyuan Xian', ',20,233,2326,', '广东省-韶关市-翁源县', '233', '02', '04', '01', '2326', '', null, null);
INSERT INTO t_sys_area VALUES ('2327', '440232', '440232', '440232', '乳源瑶族自治县', 'Ruyuan Yaozu Zizhixian', 'Ruyuan Yaozu Zizhixian', ',20,233,2327,', '广东省-韶关市-乳源瑶族自治县', '233', '02', '04', '01', '2327', '', null, null);
INSERT INTO t_sys_area VALUES ('2328', '440233', '440233', '440233', '新丰县', 'Xinfeng Xian', 'Xinfeng Xian', ',20,233,2328,', '广东省-韶关市-新丰县', '233', '02', '04', '01', '2328', '', null, null);
INSERT INTO t_sys_area VALUES ('2329', '440281', '440281', '440281', '乐昌市', 'Lechang Shi', 'Lechang Shi', ',20,233,2329,', '广东省-韶关市-乐昌市', '233', '02', '04', '01', '2329', '', null, null);
INSERT INTO t_sys_area VALUES ('2330', '440282', '440282', '440282', '南雄市', 'Nanxiong Shi', 'Nanxiong Shi', ',20,233,2330,', '广东省-韶关市-南雄市', '233', '02', '04', '01', '2330', '', null, null);
INSERT INTO t_sys_area VALUES ('2331', '440301', '440301', '440301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,234,2331,', '广东省-深圳市-市辖区', '234', '02', '04', '01', '2331', '', null, null);
INSERT INTO t_sys_area VALUES ('2332', '440303', '440303', '440303', '罗湖区', 'Luohu Qu', 'Luohu Qu', ',20,234,2332,', '广东省-深圳市-罗湖区', '234', '02', '04', '01', '2332', '', null, null);
INSERT INTO t_sys_area VALUES ('2333', '440304', '440304', '440304', '福田区', 'Futian Qu', 'Futian Qu', ',20,234,2333,', '广东省-深圳市-福田区', '234', '02', '04', '01', '2333', '', null, null);
INSERT INTO t_sys_area VALUES ('2334', '440305', '440305', '440305', '南山区', 'Nanshan Qu', 'Nanshan Qu', ',20,234,2334,', '广东省-深圳市-南山区', '234', '02', '04', '01', '2334', '', null, null);
INSERT INTO t_sys_area VALUES ('2335', '440306', '440306', '440306', '宝安区', 'Bao,an Qu', 'Bao,an Qu', ',20,234,2335,', '广东省-深圳市-宝安区', '234', '02', '04', '01', '2335', '', null, null);
INSERT INTO t_sys_area VALUES ('2336', '440307', '440307', '440307', '龙岗区', 'Longgang Qu', 'Longgang Qu', ',20,234,2336,', '广东省-深圳市-龙岗区', '234', '02', '04', '01', '2336', '', null, null);
INSERT INTO t_sys_area VALUES ('2337', '440308', '440308', '440308', '盐田区', 'Yan Tian Qu', 'Yan Tian Qu', ',20,234,2337,', '广东省-深圳市-盐田区', '234', '02', '04', '01', '2337', '', null, null);
INSERT INTO t_sys_area VALUES ('2338', '440401', '440401', '440401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,235,2338,', '广东省-珠海市-市辖区', '235', '02', '04', '01', '2338', '', null, null);
INSERT INTO t_sys_area VALUES ('2339', '440402', '440402', '440402', '香洲区', 'Xiangzhou Qu', 'Xiangzhou Qu', ',20,235,2339,', '广东省-珠海市-香洲区', '235', '02', '04', '01', '2339', '', null, null);
INSERT INTO t_sys_area VALUES ('2340', '440403', '440403', '440403', '斗门区', 'Doumen Qu', 'Doumen Qu', ',20,235,2340,', '广东省-珠海市-斗门区', '235', '02', '04', '01', '2340', '', null, null);
INSERT INTO t_sys_area VALUES ('2341', '440404', '440404', '440404', '金湾区', 'Jinwan Qu', 'Jinwan Qu', ',20,235,2341,', '广东省-珠海市-金湾区', '235', '02', '04', '01', '2341', '', null, null);
INSERT INTO t_sys_area VALUES ('2342', '440501', '440501', '440501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,236,2342,', '广东省-汕头市-市辖区', '236', '02', '04', '01', '2342', '', null, null);
INSERT INTO t_sys_area VALUES ('2343', '440507', '440507', '440507', '龙湖区', 'Longhu Qu', 'Longhu Qu', ',20,236,2343,', '广东省-汕头市-龙湖区', '236', '02', '04', '01', '2343', '', null, null);
INSERT INTO t_sys_area VALUES ('2344', '440511', '440511', '440511', '金平区', 'Jinping Qu', 'Jinping Qu', ',20,236,2344,', '广东省-汕头市-金平区', '236', '02', '04', '01', '2344', '', null, null);
INSERT INTO t_sys_area VALUES ('2345', '440512', '440512', '440512', '濠江区', 'Haojiang Qu', 'Haojiang Qu', ',20,236,2345,', '广东省-汕头市-濠江区', '236', '02', '04', '01', '2345', '', null, null);
INSERT INTO t_sys_area VALUES ('2346', '440513', '440513', '440513', '潮阳区', 'Chaoyang  Qu', 'Chaoyang  Qu', ',20,236,2346,', '广东省-汕头市-潮阳区', '236', '02', '04', '01', '2346', '', null, null);
INSERT INTO t_sys_area VALUES ('2347', '440514', '440514', '440514', '潮南区', 'Chaonan Qu', 'Chaonan Qu', ',20,236,2347,', '广东省-汕头市-潮南区', '236', '02', '04', '01', '2347', '', null, null);
INSERT INTO t_sys_area VALUES ('2348', '440515', '440515', '440515', '澄海区', 'Chenghai QU', 'Chenghai QU', ',20,236,2348,', '广东省-汕头市-澄海区', '236', '02', '04', '01', '2348', '', null, null);
INSERT INTO t_sys_area VALUES ('2349', '440523', '440523', '440523', '南澳县', 'Nan,ao Xian', 'Nan,ao Xian', ',20,236,2349,', '广东省-汕头市-南澳县', '236', '02', '04', '01', '2349', '', null, null);
INSERT INTO t_sys_area VALUES ('2350', '440601', '440601', '440601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,237,2350,', '广东省-佛山市-市辖区', '237', '02', '04', '01', '2350', '', null, null);
INSERT INTO t_sys_area VALUES ('2351', '440604', '440604', '440604', '禅城区', 'Chancheng Qu', 'Chancheng Qu', ',20,237,2351,', '广东省-佛山市-禅城区', '237', '02', '04', '01', '2351', '', null, null);
INSERT INTO t_sys_area VALUES ('2352', '440605', '440605', '440605', '南海区', 'Nanhai Shi', 'Nanhai Shi', ',20,237,2352,', '广东省-佛山市-南海区', '237', '02', '04', '01', '2352', '', null, null);
INSERT INTO t_sys_area VALUES ('2353', '440606', '440606', '440606', '顺德区', 'Shunde Shi', 'Shunde Shi', ',20,237,2353,', '广东省-佛山市-顺德区', '237', '02', '04', '01', '2353', '', null, null);
INSERT INTO t_sys_area VALUES ('2354', '440607', '440607', '440607', '三水区', 'Sanshui Shi', 'Sanshui Shi', ',20,237,2354,', '广东省-佛山市-三水区', '237', '02', '04', '01', '2354', '', null, null);
INSERT INTO t_sys_area VALUES ('2355', '440608', '440608', '440608', '高明区', 'Gaoming Shi', 'Gaoming Shi', ',20,237,2355,', '广东省-佛山市-高明区', '237', '02', '04', '01', '2355', '', null, null);
INSERT INTO t_sys_area VALUES ('2356', '440701', '440701', '440701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,238,2356,', '广东省-江门市-市辖区', '238', '02', '04', '01', '2356', '', null, null);
INSERT INTO t_sys_area VALUES ('2357', '440703', '440703', '440703', '蓬江区', 'Pengjiang Qu', 'Pengjiang Qu', ',20,238,2357,', '广东省-江门市-蓬江区', '238', '02', '04', '01', '2357', '', null, null);
INSERT INTO t_sys_area VALUES ('2358', '440704', '440704', '440704', '江海区', 'Jianghai Qu', 'Jianghai Qu', ',20,238,2358,', '广东省-江门市-江海区', '238', '02', '04', '01', '2358', '', null, null);
INSERT INTO t_sys_area VALUES ('2359', '440705', '440705', '440705', '新会区', 'Xinhui Shi', 'Xinhui Shi', ',20,238,2359,', '广东省-江门市-新会区', '238', '02', '04', '01', '2359', '', null, null);
INSERT INTO t_sys_area VALUES ('2360', '440781', '440781', '440781', '台山市', 'Taishan Shi', 'Taishan Shi', ',20,238,2360,', '广东省-江门市-台山市', '238', '02', '04', '01', '2360', '', null, null);
INSERT INTO t_sys_area VALUES ('2361', '440783', '440783', '440783', '开平市', 'Kaiping Shi', 'Kaiping Shi', ',20,238,2361,', '广东省-江门市-开平市', '238', '02', '04', '01', '2361', '', null, null);
INSERT INTO t_sys_area VALUES ('2362', '440784', '440784', '440784', '鹤山市', 'Heshan Shi', 'Heshan Shi', ',20,238,2362,', '广东省-江门市-鹤山市', '238', '02', '04', '01', '2362', '', null, null);
INSERT INTO t_sys_area VALUES ('2363', '440785', '440785', '440785', '恩平市', 'Enping Shi', 'Enping Shi', ',20,238,2363,', '广东省-江门市-恩平市', '238', '02', '04', '01', '2363', '', null, null);
INSERT INTO t_sys_area VALUES ('2364', '440801', '440801', '440801', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,239,2364,', '广东省-湛江市-市辖区', '239', '02', '04', '01', '2364', '', null, null);
INSERT INTO t_sys_area VALUES ('2365', '440802', '440802', '440802', '赤坎区', 'Chikan Qu', 'Chikan Qu', ',20,239,2365,', '广东省-湛江市-赤坎区', '239', '02', '04', '01', '2365', '', null, null);
INSERT INTO t_sys_area VALUES ('2366', '440803', '440803', '440803', '霞山区', 'Xiashan Qu', 'Xiashan Qu', ',20,239,2366,', '广东省-湛江市-霞山区', '239', '02', '04', '01', '2366', '', null, null);
INSERT INTO t_sys_area VALUES ('2367', '440804', '440804', '440804', '坡头区', 'Potou Qu', 'Potou Qu', ',20,239,2367,', '广东省-湛江市-坡头区', '239', '02', '04', '01', '2367', '', null, null);
INSERT INTO t_sys_area VALUES ('2368', '440811', '440811', '440811', '麻章区', 'Mazhang Qu', 'Mazhang Qu', ',20,239,2368,', '广东省-湛江市-麻章区', '239', '02', '04', '01', '2368', '', null, null);
INSERT INTO t_sys_area VALUES ('2369', '440823', '440823', '440823', '遂溪县', 'Suixi Xian', 'Suixi Xian', ',20,239,2369,', '广东省-湛江市-遂溪县', '239', '02', '04', '01', '2369', '', null, null);
INSERT INTO t_sys_area VALUES ('2370', '440825', '440825', '440825', '徐闻县', 'Xuwen Xian', 'Xuwen Xian', ',20,239,2370,', '广东省-湛江市-徐闻县', '239', '02', '04', '01', '2370', '', null, null);
INSERT INTO t_sys_area VALUES ('2371', '440881', '440881', '440881', '廉江市', 'Lianjiang Shi', 'Lianjiang Shi', ',20,239,2371,', '广东省-湛江市-廉江市', '239', '02', '04', '01', '2371', '', null, null);
INSERT INTO t_sys_area VALUES ('2372', '440882', '440882', '440882', '雷州市', 'Leizhou Shi', 'Leizhou Shi', ',20,239,2372,', '广东省-湛江市-雷州市', '239', '02', '04', '01', '2372', '', null, null);
INSERT INTO t_sys_area VALUES ('2373', '440883', '440883', '440883', '吴川市', 'Wuchuan Shi', 'Wuchuan Shi', ',20,239,2373,', '广东省-湛江市-吴川市', '239', '02', '04', '01', '2373', '', null, null);
INSERT INTO t_sys_area VALUES ('2374', '440901', '440901', '440901', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,240,2374,', '广东省-茂名市-市辖区', '240', '02', '04', '01', '2374', '', null, null);
INSERT INTO t_sys_area VALUES ('2375', '440902', '440902', '440902', '茂南区', 'Maonan Qu', 'Maonan Qu', ',20,240,2375,', '广东省-茂名市-茂南区', '240', '02', '04', '01', '2375', '', null, null);
INSERT INTO t_sys_area VALUES ('2376', '440903', '440903', '440903', '茂港区', 'Maogang Qu', 'Maogang Qu', ',20,240,2376,', '广东省-茂名市-茂港区', '240', '02', '04', '01', '2376', '', null, null);
INSERT INTO t_sys_area VALUES ('2377', '440923', '440923', '440923', '电白县', 'Dianbai Xian', 'Dianbai Xian', ',20,240,2377,', '广东省-茂名市-电白县', '240', '02', '04', '01', '2377', '', null, null);
INSERT INTO t_sys_area VALUES ('2378', '440981', '440981', '440981', '高州市', 'Gaozhou Shi', 'Gaozhou Shi', ',20,240,2378,', '广东省-茂名市-高州市', '240', '02', '04', '01', '2378', '', null, null);
INSERT INTO t_sys_area VALUES ('2379', '440982', '440982', '440982', '化州市', 'Huazhou Shi', 'Huazhou Shi', ',20,240,2379,', '广东省-茂名市-化州市', '240', '02', '04', '01', '2379', '', null, null);
INSERT INTO t_sys_area VALUES ('2380', '440983', '440983', '440983', '信宜市', 'Xinyi Shi', 'Xinyi Shi', ',20,240,2380,', '广东省-茂名市-信宜市', '240', '02', '04', '01', '2380', '', null, null);
INSERT INTO t_sys_area VALUES ('2381', '441201', '441201', '441201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,241,2381,', '广东省-肇庆市-市辖区', '241', '02', '04', '01', '2381', '', null, null);
INSERT INTO t_sys_area VALUES ('2382', '441202', '441202', '441202', '端州区', 'Duanzhou Qu', 'Duanzhou Qu', ',20,241,2382,', '广东省-肇庆市-端州区', '241', '02', '04', '01', '2382', '', null, null);
INSERT INTO t_sys_area VALUES ('2383', '441203', '441203', '441203', '鼎湖区', 'Dinghu Qu', 'Dinghu Qu', ',20,241,2383,', '广东省-肇庆市-鼎湖区', '241', '02', '04', '01', '2383', '', null, null);
INSERT INTO t_sys_area VALUES ('2384', '441223', '441223', '441223', '广宁县', 'Guangning Xian', 'Guangning Xian', ',20,241,2384,', '广东省-肇庆市-广宁县', '241', '02', '04', '01', '2384', '', null, null);
INSERT INTO t_sys_area VALUES ('2385', '441224', '441224', '441224', '怀集县', 'Huaiji Xian', 'Huaiji Xian', ',20,241,2385,', '广东省-肇庆市-怀集县', '241', '02', '04', '01', '2385', '', null, null);
INSERT INTO t_sys_area VALUES ('2386', '441225', '441225', '441225', '封开县', 'Fengkai Xian', 'Fengkai Xian', ',20,241,2386,', '广东省-肇庆市-封开县', '241', '02', '04', '01', '2386', '', null, null);
INSERT INTO t_sys_area VALUES ('2387', '441226', '441226', '441226', '德庆县', 'Deqing Xian', 'Deqing Xian', ',20,241,2387,', '广东省-肇庆市-德庆县', '241', '02', '04', '01', '2387', '', null, null);
INSERT INTO t_sys_area VALUES ('2388', '441283', '441283', '441283', '高要市', 'Gaoyao Xian', 'Gaoyao Xian', ',20,241,2388,', '广东省-肇庆市-高要市', '241', '02', '04', '01', '2388', '', null, null);
INSERT INTO t_sys_area VALUES ('2389', '441284', '441284', '441284', '四会市', 'Sihui Shi', 'Sihui Shi', ',20,241,2389,', '广东省-肇庆市-四会市', '241', '02', '04', '01', '2389', '', null, null);
INSERT INTO t_sys_area VALUES ('2390', '441301', '441301', '441301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,242,2390,', '广东省-惠州市-市辖区', '242', '02', '04', '01', '2390', '', null, null);
INSERT INTO t_sys_area VALUES ('2391', '441302', '441302', '441302', '惠城区', 'Huicheng Qu', 'Huicheng Qu', ',20,242,2391,', '广东省-惠州市-惠城区', '242', '02', '04', '01', '2391', '', null, null);
INSERT INTO t_sys_area VALUES ('2392', '441303', '441303', '441303', '惠阳区', 'Huiyang Shi', 'Huiyang Shi', ',20,242,2392,', '广东省-惠州市-惠阳区', '242', '02', '04', '01', '2392', '', null, null);
INSERT INTO t_sys_area VALUES ('2393', '441322', '441322', '441322', '博罗县', 'Boluo Xian', 'Boluo Xian', ',20,242,2393,', '广东省-惠州市-博罗县', '242', '02', '04', '01', '2393', '', null, null);
INSERT INTO t_sys_area VALUES ('2394', '441323', '441323', '441323', '惠东县', 'Huidong Xian', 'Huidong Xian', ',20,242,2394,', '广东省-惠州市-惠东县', '242', '02', '04', '01', '2394', '', null, null);
INSERT INTO t_sys_area VALUES ('2395', '441324', '441324', '441324', '龙门县', 'Longmen Xian', 'Longmen Xian', ',20,242,2395,', '广东省-惠州市-龙门县', '242', '02', '04', '01', '2395', '', null, null);
INSERT INTO t_sys_area VALUES ('2396', '441401', '441401', '441401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,243,2396,', '广东省-梅州市-市辖区', '243', '02', '04', '01', '2396', '', null, null);
INSERT INTO t_sys_area VALUES ('2397', '441402', '441402', '441402', '梅江区', 'Meijiang Qu', 'Meijiang Qu', ',20,243,2397,', '广东省-梅州市-梅江区', '243', '02', '04', '01', '2397', '', null, null);
INSERT INTO t_sys_area VALUES ('2398', '441421', '441421', '441421', '梅县', 'Mei Xian', 'Mei Xian', ',20,243,2398,', '广东省-梅州市-梅县', '243', '02', '04', '01', '2398', '', null, null);
INSERT INTO t_sys_area VALUES ('2399', '441422', '441422', '441422', '大埔县', 'Dabu Xian', 'Dabu Xian', ',20,243,2399,', '广东省-梅州市-大埔县', '243', '02', '04', '01', '2399', '', null, null);
INSERT INTO t_sys_area VALUES ('2400', '441423', '441423', '441423', '丰顺县', 'Fengshun Xian', 'Fengshun Xian', ',20,243,2400,', '广东省-梅州市-丰顺县', '243', '02', '04', '01', '2400', '', null, null);
INSERT INTO t_sys_area VALUES ('2401', '441424', '441424', '441424', '五华县', 'Wuhua Xian', 'Wuhua Xian', ',20,243,2401,', '广东省-梅州市-五华县', '243', '02', '04', '01', '2401', '', null, null);
INSERT INTO t_sys_area VALUES ('2402', '441426', '441426', '441426', '平远县', 'Pingyuan Xian', 'Pingyuan Xian', ',20,243,2402,', '广东省-梅州市-平远县', '243', '02', '04', '01', '2402', '', null, null);
INSERT INTO t_sys_area VALUES ('2403', '441427', '441427', '441427', '蕉岭县', 'Jiaoling Xian', 'Jiaoling Xian', ',20,243,2403,', '广东省-梅州市-蕉岭县', '243', '02', '04', '01', '2403', '', null, null);
INSERT INTO t_sys_area VALUES ('2404', '441481', '441481', '441481', '兴宁市', 'Xingning Shi', 'Xingning Shi', ',20,243,2404,', '广东省-梅州市-兴宁市', '243', '02', '04', '01', '2404', '', null, null);
INSERT INTO t_sys_area VALUES ('2405', '441501', '441501', '441501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,244,2405,', '广东省-汕尾市-市辖区', '244', '02', '04', '01', '2405', '', null, null);
INSERT INTO t_sys_area VALUES ('2406', '441502', '441502', '441502', '城区', 'Chengqu', 'Chengqu', ',20,244,2406,', '广东省-汕尾市-城区', '244', '02', '04', '01', '2406', '', null, null);
INSERT INTO t_sys_area VALUES ('2407', '441521', '441521', '441521', '海丰县', 'Haifeng Xian', 'Haifeng Xian', ',20,244,2407,', '广东省-汕尾市-海丰县', '244', '02', '04', '01', '2407', '', null, null);
INSERT INTO t_sys_area VALUES ('2408', '441523', '441523', '441523', '陆河县', 'Luhe Xian', 'Luhe Xian', ',20,244,2408,', '广东省-汕尾市-陆河县', '244', '02', '04', '01', '2408', '', null, null);
INSERT INTO t_sys_area VALUES ('2409', '441581', '441581', '441581', '陆丰市', 'Lufeng Shi', 'Lufeng Shi', ',20,244,2409,', '广东省-汕尾市-陆丰市', '244', '02', '04', '01', '2409', '', null, null);
INSERT INTO t_sys_area VALUES ('2410', '441601', '441601', '441601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,245,2410,', '广东省-河源市-市辖区', '245', '02', '04', '01', '2410', '', null, null);
INSERT INTO t_sys_area VALUES ('2411', '441602', '441602', '441602', '源城区', 'Yuancheng Qu', 'Yuancheng Qu', ',20,245,2411,', '广东省-河源市-源城区', '245', '02', '04', '01', '2411', '', null, null);
INSERT INTO t_sys_area VALUES ('2412', '441621', '441621', '441621', '紫金县', 'Zijin Xian', 'Zijin Xian', ',20,245,2412,', '广东省-河源市-紫金县', '245', '02', '04', '01', '2412', '', null, null);
INSERT INTO t_sys_area VALUES ('2413', '441622', '441622', '441622', '龙川县', 'Longchuan Xian', 'Longchuan Xian', ',20,245,2413,', '广东省-河源市-龙川县', '245', '02', '04', '01', '2413', '', null, null);
INSERT INTO t_sys_area VALUES ('2414', '441623', '441623', '441623', '连平县', 'Lianping Xian', 'Lianping Xian', ',20,245,2414,', '广东省-河源市-连平县', '245', '02', '04', '01', '2414', '', null, null);
INSERT INTO t_sys_area VALUES ('2415', '441624', '441624', '441624', '和平县', 'Heping Xian', 'Heping Xian', ',20,245,2415,', '广东省-河源市-和平县', '245', '02', '04', '01', '2415', '', null, null);
INSERT INTO t_sys_area VALUES ('2416', '441625', '441625', '441625', '东源县', 'Dongyuan Xian', 'Dongyuan Xian', ',20,245,2416,', '广东省-河源市-东源县', '245', '02', '04', '01', '2416', '', null, null);
INSERT INTO t_sys_area VALUES ('2417', '441701', '441701', '441701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,246,2417,', '广东省-阳江市-市辖区', '246', '02', '04', '01', '2417', '', null, null);
INSERT INTO t_sys_area VALUES ('2418', '441702', '441702', '441702', '江城区', 'Jiangcheng Qu', 'Jiangcheng Qu', ',20,246,2418,', '广东省-阳江市-江城区', '246', '02', '04', '01', '2418', '', null, null);
INSERT INTO t_sys_area VALUES ('2419', '441721', '441721', '441721', '阳西县', 'Yangxi Xian', 'Yangxi Xian', ',20,246,2419,', '广东省-阳江市-阳西县', '246', '02', '04', '01', '2419', '', null, null);
INSERT INTO t_sys_area VALUES ('2420', '441723', '441723', '441723', '阳东县', 'Yangdong Xian', 'Yangdong Xian', ',20,246,2420,', '广东省-阳江市-阳东县', '246', '02', '04', '01', '2420', '', null, null);
INSERT INTO t_sys_area VALUES ('2421', '441781', '441781', '441781', '阳春市', 'Yangchun Shi', 'Yangchun Shi', ',20,246,2421,', '广东省-阳江市-阳春市', '246', '02', '04', '01', '2421', '', null, null);
INSERT INTO t_sys_area VALUES ('2422', '441801', '441801', '441801', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,247,2422,', '广东省-清远市-市辖区', '247', '02', '04', '01', '2422', '', null, null);
INSERT INTO t_sys_area VALUES ('2423', '441802', '441802', '441802', '清城区', 'Qingcheng Qu', 'Qingcheng Qu', ',20,247,2423,', '广东省-清远市-清城区', '247', '02', '04', '01', '2423', '', null, null);
INSERT INTO t_sys_area VALUES ('2424', '441821', '441821', '441821', '佛冈县', 'Fogang Xian', 'Fogang Xian', ',20,247,2424,', '广东省-清远市-佛冈县', '247', '02', '04', '01', '2424', '', null, null);
INSERT INTO t_sys_area VALUES ('2425', '441823', '441823', '441823', '阳山县', 'Yangshan Xian', 'Yangshan Xian', ',20,247,2425,', '广东省-清远市-阳山县', '247', '02', '04', '01', '2425', '', null, null);
INSERT INTO t_sys_area VALUES ('2426', '441825', '441825', '441825', '连山壮族瑶族自治县', 'Lianshan Zhuangzu Yaozu Zizhixian', 'Lianshan Zhuangzu Yaozu Zizhixian', ',20,247,2426,', '广东省-清远市-连山壮族瑶族自治县', '247', '02', '04', '01', '2426', '', null, null);
INSERT INTO t_sys_area VALUES ('2427', '441826', '441826', '441826', '连南瑶族自治县', 'Liannanyaozuzizhi Qu', 'Liannanyaozuzizhi Qu', ',20,247,2427,', '广东省-清远市-连南瑶族自治县', '247', '02', '04', '01', '2427', '', null, null);
INSERT INTO t_sys_area VALUES ('2428', '441827', '441827', '441827', '清新县', 'Qingxin Xian', 'Qingxin Xian', ',20,247,2428,', '广东省-清远市-清新县', '247', '02', '04', '01', '2428', '', null, null);
INSERT INTO t_sys_area VALUES ('2429', '441881', '441881', '441881', '英德市', 'Yingde Shi', 'Yingde Shi', ',20,247,2429,', '广东省-清远市-英德市', '247', '02', '04', '01', '2429', '', null, null);
INSERT INTO t_sys_area VALUES ('2430', '441882', '441882', '441882', '连州市', 'Lianzhou Shi', 'Lianzhou Shi', ',20,247,2430,', '广东省-清远市-连州市', '247', '02', '04', '01', '2430', '', null, null);
INSERT INTO t_sys_area VALUES ('2431', '445101', '445101', '445101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,250,2431,', '广东省-潮州市-市辖区', '250', '02', '04', '01', '2431', '', null, null);
INSERT INTO t_sys_area VALUES ('2432', '445102', '445102', '445102', '湘桥区', 'Xiangqiao Qu', 'Xiangqiao Qu', ',20,250,2432,', '广东省-潮州市-湘桥区', '250', '02', '04', '01', '2432', '', null, null);
INSERT INTO t_sys_area VALUES ('2433', '445121', '445121', '445121', '潮安县', 'Chao,an Xian', 'Chao,an Xian', ',20,250,2433,', '广东省-潮州市-潮安县', '250', '02', '04', '01', '2433', '', null, null);
INSERT INTO t_sys_area VALUES ('2434', '445122', '445122', '445122', '饶平县', 'Raoping Xian', 'Raoping Xian', ',20,250,2434,', '广东省-潮州市-饶平县', '250', '02', '04', '01', '2434', '', null, null);
INSERT INTO t_sys_area VALUES ('2435', '445201', '445201', '445201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,251,2435,', '广东省-揭阳市-市辖区', '251', '02', '04', '01', '2435', '', null, null);
INSERT INTO t_sys_area VALUES ('2436', '445202', '445202', '445202', '榕城区', 'Rongcheng Qu', 'Rongcheng Qu', ',20,251,2436,', '广东省-揭阳市-榕城区', '251', '02', '04', '01', '2436', '', null, null);
INSERT INTO t_sys_area VALUES ('2437', '445221', '445221', '445221', '揭东县', 'Jiedong Xian', 'Jiedong Xian', ',20,251,2437,', '广东省-揭阳市-揭东县', '251', '02', '04', '01', '2437', '', null, null);
INSERT INTO t_sys_area VALUES ('2438', '445222', '445222', '445222', '揭西县', 'Jiexi Xian', 'Jiexi Xian', ',20,251,2438,', '广东省-揭阳市-揭西县', '251', '02', '04', '01', '2438', '', null, null);
INSERT INTO t_sys_area VALUES ('2439', '445224', '445224', '445224', '惠来县', 'Huilai Xian', 'Huilai Xian', ',20,251,2439,', '广东省-揭阳市-惠来县', '251', '02', '04', '01', '2439', '', null, null);
INSERT INTO t_sys_area VALUES ('2440', '445281', '445281', '445281', '普宁市', 'Puning Shi', 'Puning Shi', ',20,251,2440,', '广东省-揭阳市-普宁市', '251', '02', '04', '01', '2440', '', null, null);
INSERT INTO t_sys_area VALUES ('2441', '445301', '445301', '445301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',20,252,2441,', '广东省-云浮市-市辖区', '252', '02', '04', '01', '2441', '', null, null);
INSERT INTO t_sys_area VALUES ('2442', '445302', '445302', '445302', '云城区', 'Yuncheng Qu', 'Yuncheng Qu', ',20,252,2442,', '广东省-云浮市-云城区', '252', '02', '04', '01', '2442', '', null, null);
INSERT INTO t_sys_area VALUES ('2443', '445321', '445321', '445321', '新兴县', 'Xinxing Xian', 'Xinxing Xian', ',20,252,2443,', '广东省-云浮市-新兴县', '252', '02', '04', '01', '2443', '', null, null);
INSERT INTO t_sys_area VALUES ('2444', '445322', '445322', '445322', '郁南县', 'Yunan Xian', 'Yunan Xian', ',20,252,2444,', '广东省-云浮市-郁南县', '252', '02', '04', '01', '2444', '', null, null);
INSERT INTO t_sys_area VALUES ('2445', '445323', '445323', '445323', '云安县', 'Yun,an Xian', 'Yun,an Xian', ',20,252,2445,', '广东省-云浮市-云安县', '252', '02', '04', '01', '2445', '', null, null);
INSERT INTO t_sys_area VALUES ('2446', '445381', '445381', '445381', '罗定市', 'Luoding Shi', 'Luoding Shi', ',20,252,2446,', '广东省-云浮市-罗定市', '252', '02', '04', '01', '2446', '', null, null);
INSERT INTO t_sys_area VALUES ('2447', '450101', '450101', '450101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',21,253,2447,', '广西壮族自治区-南宁市-市辖区', '253', '02', '04', '01', '2447', '', null, null);
INSERT INTO t_sys_area VALUES ('2448', '450102', '450102', '450102', '兴宁区', 'Xingning Qu', 'Xingning Qu', ',21,253,2448,', '广西壮族自治区-南宁市-兴宁区', '253', '02', '04', '01', '2448', '', null, null);
INSERT INTO t_sys_area VALUES ('2449', '450103', '450103', '450103', '青秀区', 'Qingxiu Qu', 'Qingxiu Qu', ',21,253,2449,', '广西壮族自治区-南宁市-青秀区', '253', '02', '04', '01', '2449', '', null, null);
INSERT INTO t_sys_area VALUES ('2450', '450105', '450105', '450105', '江南区', 'Jiangnan Qu', 'Jiangnan Qu', ',21,253,2450,', '广西壮族自治区-南宁市-江南区', '253', '02', '04', '01', '2450', '', null, null);
INSERT INTO t_sys_area VALUES ('2451', '450107', '450107', '450107', '西乡塘区', 'Xixiangtang Qu', 'Xixiangtang Qu', ',21,253,2451,', '广西壮族自治区-南宁市-西乡塘区', '253', '02', '04', '01', '2451', '', null, null);
INSERT INTO t_sys_area VALUES ('2452', '450108', '450108', '450108', '良庆区', 'Liangqing Qu', 'Liangqing Qu', ',21,253,2452,', '广西壮族自治区-南宁市-良庆区', '253', '02', '04', '01', '2452', '', null, null);
INSERT INTO t_sys_area VALUES ('2453', '450109', '450109', '450109', '邕宁区', 'Yongning Qu', 'Yongning Qu', ',21,253,2453,', '广西壮族自治区-南宁市-邕宁区', '253', '02', '04', '01', '2453', '', null, null);
INSERT INTO t_sys_area VALUES ('2454', '450122', '450122', '450122', '武鸣县', 'Wuming Xian', 'Wuming Xian', ',21,253,2454,', '广西壮族自治区-南宁市-武鸣县', '253', '02', '04', '01', '2454', '', null, null);
INSERT INTO t_sys_area VALUES ('2455', '450123', '450123', '450123', '隆安县', 'Long,an Xian', 'Long,an Xian', ',21,253,2455,', '广西壮族自治区-南宁市-隆安县', '253', '02', '04', '01', '2455', '', null, null);
INSERT INTO t_sys_area VALUES ('2456', '450124', '450124', '450124', '马山县', 'Mashan Xian', 'Mashan Xian', ',21,253,2456,', '广西壮族自治区-南宁市-马山县', '253', '02', '04', '01', '2456', '', null, null);
INSERT INTO t_sys_area VALUES ('2457', '450125', '450125', '450125', '上林县', 'Shanglin Xian', 'Shanglin Xian', ',21,253,2457,', '广西壮族自治区-南宁市-上林县', '253', '02', '04', '01', '2457', '', null, null);
INSERT INTO t_sys_area VALUES ('2458', '450126', '450126', '450126', '宾阳县', 'Binyang Xian', 'Binyang Xian', ',21,253,2458,', '广西壮族自治区-南宁市-宾阳县', '253', '02', '04', '01', '2458', '', null, null);
INSERT INTO t_sys_area VALUES ('2459', '450127', '450127', '450127', '横县', 'Heng Xian', 'Heng Xian', ',21,253,2459,', '广西壮族自治区-南宁市-横县', '253', '02', '04', '01', '2459', '', null, null);
INSERT INTO t_sys_area VALUES ('2460', '450201', '450201', '450201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',21,254,2460,', '广西壮族自治区-柳州市-市辖区', '254', '02', '04', '01', '2460', '', null, null);
INSERT INTO t_sys_area VALUES ('2461', '450202', '450202', '450202', '城中区', 'Chengzhong Qu', 'Chengzhong Qu', ',21,254,2461,', '广西壮族自治区-柳州市-城中区', '254', '02', '04', '01', '2461', '', null, null);
INSERT INTO t_sys_area VALUES ('2462', '450203', '450203', '450203', '鱼峰区', 'Yufeng Qu', 'Yufeng Qu', ',21,254,2462,', '广西壮族自治区-柳州市-鱼峰区', '254', '02', '04', '01', '2462', '', null, null);
INSERT INTO t_sys_area VALUES ('2463', '450204', '450204', '450204', '柳南区', 'Liunan Qu', 'Liunan Qu', ',21,254,2463,', '广西壮族自治区-柳州市-柳南区', '254', '02', '04', '01', '2463', '', null, null);
INSERT INTO t_sys_area VALUES ('2464', '450205', '450205', '450205', '柳北区', 'Liubei Qu', 'Liubei Qu', ',21,254,2464,', '广西壮族自治区-柳州市-柳北区', '254', '02', '04', '01', '2464', '', null, null);
INSERT INTO t_sys_area VALUES ('2465', '450221', '450221', '450221', '柳江县', 'Liujiang Xian', 'Liujiang Xian', ',21,254,2465,', '广西壮族自治区-柳州市-柳江县', '254', '02', '04', '01', '2465', '', null, null);
INSERT INTO t_sys_area VALUES ('2466', '450222', '450222', '450222', '柳城县', 'Liucheng Xian', 'Liucheng Xian', ',21,254,2466,', '广西壮族自治区-柳州市-柳城县', '254', '02', '04', '01', '2466', '', null, null);
INSERT INTO t_sys_area VALUES ('2467', '450223', '450223', '450223', '鹿寨县', 'Luzhai Xian', 'Luzhai Xian', ',21,254,2467,', '广西壮族自治区-柳州市-鹿寨县', '254', '02', '04', '01', '2467', '', null, null);
INSERT INTO t_sys_area VALUES ('2468', '450224', '450224', '450224', '融安县', 'Rong,an Xian', 'Rong,an Xian', ',21,254,2468,', '广西壮族自治区-柳州市-融安县', '254', '02', '04', '01', '2468', '', null, null);
INSERT INTO t_sys_area VALUES ('2469', '450225', '450225', '450225', '融水苗族自治县', 'Rongshui Miaozu Zizhixian', 'Rongshui Miaozu Zizhixian', ',21,254,2469,', '广西壮族自治区-柳州市-融水苗族自治县', '254', '02', '04', '01', '2469', '', null, null);
INSERT INTO t_sys_area VALUES ('2470', '450226', '450226', '450226', '三江侗族自治县', 'Sanjiang Dongzu Zizhixian', 'Sanjiang Dongzu Zizhixian', ',21,254,2470,', '广西壮族自治区-柳州市-三江侗族自治县', '254', '02', '04', '01', '2470', '', null, null);
INSERT INTO t_sys_area VALUES ('2471', '450301', '450301', '450301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',21,255,2471,', '广西壮族自治区-桂林市-市辖区', '255', '02', '04', '01', '2471', '', null, null);
INSERT INTO t_sys_area VALUES ('2472', '450302', '450302', '450302', '秀峰区', 'Xiufeng Qu', 'Xiufeng Qu', ',21,255,2472,', '广西壮族自治区-桂林市-秀峰区', '255', '02', '04', '01', '2472', '', null, null);
INSERT INTO t_sys_area VALUES ('2473', '450303', '450303', '450303', '叠彩区', 'Diecai Qu', 'Diecai Qu', ',21,255,2473,', '广西壮族自治区-桂林市-叠彩区', '255', '02', '04', '01', '2473', '', null, null);
INSERT INTO t_sys_area VALUES ('2474', '450304', '450304', '450304', '象山区', 'Xiangshan Qu', 'Xiangshan Qu', ',21,255,2474,', '广西壮族自治区-桂林市-象山区', '255', '02', '04', '01', '2474', '', null, null);
INSERT INTO t_sys_area VALUES ('2475', '450305', '450305', '450305', '七星区', 'Qixing Qu', 'Qixing Qu', ',21,255,2475,', '广西壮族自治区-桂林市-七星区', '255', '02', '04', '01', '2475', '', null, null);
INSERT INTO t_sys_area VALUES ('2476', '450311', '450311', '450311', '雁山区', 'Yanshan Qu', 'Yanshan Qu', ',21,255,2476,', '广西壮族自治区-桂林市-雁山区', '255', '02', '04', '01', '2476', '', null, null);
INSERT INTO t_sys_area VALUES ('2477', '450321', '450321', '450321', '阳朔县', 'Yangshuo Xian', 'Yangshuo Xian', ',21,255,2477,', '广西壮族自治区-桂林市-阳朔县', '255', '02', '04', '01', '2477', '', null, null);
INSERT INTO t_sys_area VALUES ('2478', '450322', '450322', '450322', '临桂县', 'Lingui Xian', 'Lingui Xian', ',21,255,2478,', '广西壮族自治区-桂林市-临桂县', '255', '02', '04', '01', '2478', '', null, null);
INSERT INTO t_sys_area VALUES ('2479', '450323', '450323', '450323', '灵川县', 'Lingchuan Xian', 'Lingchuan Xian', ',21,255,2479,', '广西壮族自治区-桂林市-灵川县', '255', '02', '04', '01', '2479', '', null, null);
INSERT INTO t_sys_area VALUES ('2480', '450324', '450324', '450324', '全州县', 'Quanzhou Xian', 'Quanzhou Xian', ',21,255,2480,', '广西壮族自治区-桂林市-全州县', '255', '02', '04', '01', '2480', '', null, null);
INSERT INTO t_sys_area VALUES ('2481', '450325', '450325', '450325', '兴安县', 'Xing,an Xian', 'Xing,an Xian', ',21,255,2481,', '广西壮族自治区-桂林市-兴安县', '255', '02', '04', '01', '2481', '', null, null);
INSERT INTO t_sys_area VALUES ('2482', '450326', '450326', '450326', '永福县', 'Yongfu Xian', 'Yongfu Xian', ',21,255,2482,', '广西壮族自治区-桂林市-永福县', '255', '02', '04', '01', '2482', '', null, null);
INSERT INTO t_sys_area VALUES ('2483', '450327', '450327', '450327', '灌阳县', 'Guanyang Xian', 'Guanyang Xian', ',21,255,2483,', '广西壮族自治区-桂林市-灌阳县', '255', '02', '04', '01', '2483', '', null, null);
INSERT INTO t_sys_area VALUES ('2484', '450328', '450328', '450328', '龙胜各族自治县', 'Longsheng Gezu Zizhixian', 'Longsheng Gezu Zizhixian', ',21,255,2484,', '广西壮族自治区-桂林市-龙胜各族自治县', '255', '02', '04', '01', '2484', '', null, null);
INSERT INTO t_sys_area VALUES ('2485', '450329', '450329', '450329', '资源县', 'Ziyuan Xian', 'Ziyuan Xian', ',21,255,2485,', '广西壮族自治区-桂林市-资源县', '255', '02', '04', '01', '2485', '', null, null);
INSERT INTO t_sys_area VALUES ('2486', '450330', '450330', '450330', '平乐县', 'Pingle Xian', 'Pingle Xian', ',21,255,2486,', '广西壮族自治区-桂林市-平乐县', '255', '02', '04', '01', '2486', '', null, null);
INSERT INTO t_sys_area VALUES ('2487', '450331', '450331', '450331', '荔蒲县', 'Lipu Xian', 'Lipu Xian', ',21,255,2487,', '广西壮族自治区-桂林市-荔蒲县', '255', '02', '04', '01', '2487', '', null, null);
INSERT INTO t_sys_area VALUES ('2488', '450332', '450332', '450332', '恭城瑶族自治县', 'Gongcheng Yaozu Zizhixian', 'Gongcheng Yaozu Zizhixian', ',21,255,2488,', '广西壮族自治区-桂林市-恭城瑶族自治县', '255', '02', '04', '01', '2488', '', null, null);
INSERT INTO t_sys_area VALUES ('2489', '450401', '450401', '450401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',21,256,2489,', '广西壮族自治区-梧州市-市辖区', '256', '02', '04', '01', '2489', '', null, null);
INSERT INTO t_sys_area VALUES ('2490', '450403', '450403', '450403', '万秀区', 'Wanxiu Qu', 'Wanxiu Qu', ',21,256,2490,', '广西壮族自治区-梧州市-万秀区', '256', '02', '04', '01', '2490', '', null, null);
INSERT INTO t_sys_area VALUES ('2491', '450404', '450404', '450404', '蝶山区', 'Dieshan Qu', 'Dieshan Qu', ',21,256,2491,', '广西壮族自治区-梧州市-蝶山区', '256', '02', '04', '01', '2491', '', null, null);
INSERT INTO t_sys_area VALUES ('2492', '450405', '450405', '450405', '长洲区', 'Changzhou Qu', 'Changzhou Qu', ',21,256,2492,', '广西壮族自治区-梧州市-长洲区', '256', '02', '04', '01', '2492', '', null, null);
INSERT INTO t_sys_area VALUES ('2493', '450421', '450421', '450421', '苍梧县', 'Cangwu Xian', 'Cangwu Xian', ',21,256,2493,', '广西壮族自治区-梧州市-苍梧县', '256', '02', '04', '01', '2493', '', null, null);
INSERT INTO t_sys_area VALUES ('2494', '450422', '450422', '450422', '藤县', 'Teng Xian', 'Teng Xian', ',21,256,2494,', '广西壮族自治区-梧州市-藤县', '256', '02', '04', '01', '2494', '', null, null);
INSERT INTO t_sys_area VALUES ('2495', '450423', '450423', '450423', '蒙山县', 'Mengshan Xian', 'Mengshan Xian', ',21,256,2495,', '广西壮族自治区-梧州市-蒙山县', '256', '02', '04', '01', '2495', '', null, null);
INSERT INTO t_sys_area VALUES ('2496', '450481', '450481', '450481', '岑溪市', 'Cenxi Shi', 'Cenxi Shi', ',21,256,2496,', '广西壮族自治区-梧州市-岑溪市', '256', '02', '04', '01', '2496', '', null, null);
INSERT INTO t_sys_area VALUES ('2497', '450501', '450501', '450501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',21,257,2497,', '广西壮族自治区-北海市-市辖区', '257', '02', '04', '01', '2497', '', null, null);
INSERT INTO t_sys_area VALUES ('2498', '450502', '450502', '450502', '海城区', 'Haicheng Qu', 'Haicheng Qu', ',21,257,2498,', '广西壮族自治区-北海市-海城区', '257', '02', '04', '01', '2498', '', null, null);
INSERT INTO t_sys_area VALUES ('2499', '450503', '450503', '450503', '银海区', 'Yinhai Qu', 'Yinhai Qu', ',21,257,2499,', '广西壮族自治区-北海市-银海区', '257', '02', '04', '01', '2499', '', null, null);
INSERT INTO t_sys_area VALUES ('2500', '450512', '450512', '450512', '铁山港区', 'Tieshangangqu ', 'Tieshangangqu ', ',21,257,2500,', '广西壮族自治区-北海市-铁山港区', '257', '02', '04', '01', '2500', '', null, null);
INSERT INTO t_sys_area VALUES ('2501', '450521', '450521', '450521', '合浦县', 'Hepu Xian', 'Hepu Xian', ',21,257,2501,', '广西壮族自治区-北海市-合浦县', '257', '02', '04', '01', '2501', '', null, null);
INSERT INTO t_sys_area VALUES ('2502', '450601', '450601', '450601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',21,258,2502,', '广西壮族自治区-防城港市-市辖区', '258', '02', '04', '01', '2502', '', null, null);
INSERT INTO t_sys_area VALUES ('2503', '450602', '450602', '450602', '港口区', 'Gangkou Qu', 'Gangkou Qu', ',21,258,2503,', '广西壮族自治区-防城港市-港口区', '258', '02', '04', '01', '2503', '', null, null);
INSERT INTO t_sys_area VALUES ('2504', '450603', '450603', '450603', '防城区', 'Fangcheng Qu', 'Fangcheng Qu', ',21,258,2504,', '广西壮族自治区-防城港市-防城区', '258', '02', '04', '01', '2504', '', null, null);
INSERT INTO t_sys_area VALUES ('2505', '450621', '450621', '450621', '上思县', 'Shangsi Xian', 'Shangsi Xian', ',21,258,2505,', '广西壮族自治区-防城港市-上思县', '258', '02', '04', '01', '2505', '', null, null);
INSERT INTO t_sys_area VALUES ('2506', '450681', '450681', '450681', '东兴市', 'Dongxing Shi', 'Dongxing Shi', ',21,258,2506,', '广西壮族自治区-防城港市-东兴市', '258', '02', '04', '01', '2506', '', null, null);
INSERT INTO t_sys_area VALUES ('2507', '450701', '450701', '450701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',21,259,2507,', '广西壮族自治区-钦州市-市辖区', '259', '02', '04', '01', '2507', '', null, null);
INSERT INTO t_sys_area VALUES ('2508', '450702', '450702', '450702', '钦南区', 'Qinnan Qu', 'Qinnan Qu', ',21,259,2508,', '广西壮族自治区-钦州市-钦南区', '259', '02', '04', '01', '2508', '', null, null);
INSERT INTO t_sys_area VALUES ('2509', '450703', '450703', '450703', '钦北区', 'Qinbei Qu', 'Qinbei Qu', ',21,259,2509,', '广西壮族自治区-钦州市-钦北区', '259', '02', '04', '01', '2509', '', null, null);
INSERT INTO t_sys_area VALUES ('2510', '450721', '450721', '450721', '灵山县', 'Lingshan Xian', 'Lingshan Xian', ',21,259,2510,', '广西壮族自治区-钦州市-灵山县', '259', '02', '04', '01', '2510', '', null, null);
INSERT INTO t_sys_area VALUES ('2511', '450722', '450722', '450722', '浦北县', 'Pubei Xian', 'Pubei Xian', ',21,259,2511,', '广西壮族自治区-钦州市-浦北县', '259', '02', '04', '01', '2511', '', null, null);
INSERT INTO t_sys_area VALUES ('2512', '450801', '450801', '450801', '市辖区', 'Shixiaqu', 'Shixiaqu', ',21,260,2512,', '广西壮族自治区-贵港市-市辖区', '260', '02', '04', '01', '2512', '', null, null);
INSERT INTO t_sys_area VALUES ('2513', '450802', '450802', '450802', '港北区', 'Gangbei Qu', 'Gangbei Qu', ',21,260,2513,', '广西壮族自治区-贵港市-港北区', '260', '02', '04', '01', '2513', '', null, null);
INSERT INTO t_sys_area VALUES ('2514', '450803', '450803', '450803', '港南区', 'Gangnan Qu', 'Gangnan Qu', ',21,260,2514,', '广西壮族自治区-贵港市-港南区', '260', '02', '04', '01', '2514', '', null, null);
INSERT INTO t_sys_area VALUES ('2515', '450804', '450804', '450804', '覃塘区', 'Tantang Qu', 'Tantang Qu', ',21,260,2515,', '广西壮族自治区-贵港市-覃塘区', '260', '02', '04', '01', '2515', '', null, null);
INSERT INTO t_sys_area VALUES ('2516', '450821', '450821', '450821', '平南县', 'Pingnan Xian', 'Pingnan Xian', ',21,260,2516,', '广西壮族自治区-贵港市-平南县', '260', '02', '04', '01', '2516', '', null, null);
INSERT INTO t_sys_area VALUES ('2517', '450881', '450881', '450881', '桂平市', 'Guiping Shi', 'Guiping Shi', ',21,260,2517,', '广西壮族自治区-贵港市-桂平市', '260', '02', '04', '01', '2517', '', null, null);
INSERT INTO t_sys_area VALUES ('2518', '450901', '450901', '450901', '市辖区', 'Shixiaqu', 'Shixiaqu', ',21,261,2518,', '广西壮族自治区-玉林市-市辖区', '261', '02', '04', '01', '2518', '', null, null);
INSERT INTO t_sys_area VALUES ('2519', '450902', '450902', '450902', '玉州区', 'Yuzhou Qu', 'Yuzhou Qu', ',21,261,2519,', '广西壮族自治区-玉林市-玉州区', '261', '02', '04', '01', '2519', '', null, null);
INSERT INTO t_sys_area VALUES ('2520', '450921', '450921', '450921', '容县', 'Rong Xian', 'Rong Xian', ',21,261,2520,', '广西壮族自治区-玉林市-容县', '261', '02', '04', '01', '2520', '', null, null);
INSERT INTO t_sys_area VALUES ('2521', '450922', '450922', '450922', '陆川县', 'Luchuan Xian', 'Luchuan Xian', ',21,261,2521,', '广西壮族自治区-玉林市-陆川县', '261', '02', '04', '01', '2521', '', null, null);
INSERT INTO t_sys_area VALUES ('2522', '450923', '450923', '450923', '博白县', 'Bobai Xian', 'Bobai Xian', ',21,261,2522,', '广西壮族自治区-玉林市-博白县', '261', '02', '04', '01', '2522', '', null, null);
INSERT INTO t_sys_area VALUES ('2523', '450924', '450924', '450924', '兴业县', 'Xingye Xian', 'Xingye Xian', ',21,261,2523,', '广西壮族自治区-玉林市-兴业县', '261', '02', '04', '01', '2523', '', null, null);
INSERT INTO t_sys_area VALUES ('2524', '450981', '450981', '450981', '北流市', 'Beiliu Shi', 'Beiliu Shi', ',21,261,2524,', '广西壮族自治区-玉林市-北流市', '261', '02', '04', '01', '2524', '', null, null);
INSERT INTO t_sys_area VALUES ('2525', '451001', '451001', '451001', '市辖区', '1', '1', ',21,262,2525,', '广西壮族自治区-百色市-市辖区', '262', '02', '04', '01', '2525', '', null, null);
INSERT INTO t_sys_area VALUES ('2526', '451002', '451002', '451002', '右江区', 'Youjiang Qu', 'Youjiang Qu', ',21,262,2526,', '广西壮族自治区-百色市-右江区', '262', '02', '04', '01', '2526', '', null, null);
INSERT INTO t_sys_area VALUES ('2527', '451021', '451021', '451021', '田阳县', 'Tianyang Xian', 'Tianyang Xian', ',21,262,2527,', '广西壮族自治区-百色市-田阳县', '262', '02', '04', '01', '2527', '', null, null);
INSERT INTO t_sys_area VALUES ('2528', '451022', '451022', '451022', '田东县', 'Tiandong Xian', 'Tiandong Xian', ',21,262,2528,', '广西壮族自治区-百色市-田东县', '262', '02', '04', '01', '2528', '', null, null);
INSERT INTO t_sys_area VALUES ('2529', '451023', '451023', '451023', '平果县', 'Pingguo Xian', 'Pingguo Xian', ',21,262,2529,', '广西壮族自治区-百色市-平果县', '262', '02', '04', '01', '2529', '', null, null);
INSERT INTO t_sys_area VALUES ('2530', '451024', '451024', '451024', '德保县', 'Debao Xian', 'Debao Xian', ',21,262,2530,', '广西壮族自治区-百色市-德保县', '262', '02', '04', '01', '2530', '', null, null);
INSERT INTO t_sys_area VALUES ('2531', '451025', '451025', '451025', '靖西县', 'Jingxi Xian', 'Jingxi Xian', ',21,262,2531,', '广西壮族自治区-百色市-靖西县', '262', '02', '04', '01', '2531', '', null, null);
INSERT INTO t_sys_area VALUES ('2532', '451026', '451026', '451026', '那坡县', 'Napo Xian', 'Napo Xian', ',21,262,2532,', '广西壮族自治区-百色市-那坡县', '262', '02', '04', '01', '2532', '', null, null);
INSERT INTO t_sys_area VALUES ('2533', '451027', '451027', '451027', '凌云县', 'Lingyun Xian', 'Lingyun Xian', ',21,262,2533,', '广西壮族自治区-百色市-凌云县', '262', '02', '04', '01', '2533', '', null, null);
INSERT INTO t_sys_area VALUES ('2534', '451028', '451028', '451028', '乐业县', 'Leye Xian', 'Leye Xian', ',21,262,2534,', '广西壮族自治区-百色市-乐业县', '262', '02', '04', '01', '2534', '', null, null);
INSERT INTO t_sys_area VALUES ('2535', '451029', '451029', '451029', '田林县', 'Tianlin Xian', 'Tianlin Xian', ',21,262,2535,', '广西壮族自治区-百色市-田林县', '262', '02', '04', '01', '2535', '', null, null);
INSERT INTO t_sys_area VALUES ('2536', '451030', '451030', '451030', '西林县', 'Xilin Xian', 'Xilin Xian', ',21,262,2536,', '广西壮族自治区-百色市-西林县', '262', '02', '04', '01', '2536', '', null, null);
INSERT INTO t_sys_area VALUES ('2537', '451031', '451031', '451031', '隆林各族自治县', 'Longlin Gezu Zizhixian', 'Longlin Gezu Zizhixian', ',21,262,2537,', '广西壮族自治区-百色市-隆林各族自治县', '262', '02', '04', '01', '2537', '', null, null);
INSERT INTO t_sys_area VALUES ('2538', '451101', '451101', '451101', '市辖区', '1', '1', ',21,263,2538,', '广西壮族自治区-贺州市-市辖区', '263', '02', '04', '01', '2538', '', null, null);
INSERT INTO t_sys_area VALUES ('2539', '451102', '451102', '451102', '八步区', 'Babu Qu', 'Babu Qu', ',21,263,2539,', '广西壮族自治区-贺州市-八步区', '263', '02', '04', '01', '2539', '', null, null);
INSERT INTO t_sys_area VALUES ('2540', '451121', '451121', '451121', '昭平县', 'Zhaoping Xian', 'Zhaoping Xian', ',21,263,2540,', '广西壮族自治区-贺州市-昭平县', '263', '02', '04', '01', '2540', '', null, null);
INSERT INTO t_sys_area VALUES ('2541', '451122', '451122', '451122', '钟山县', 'Zhongshan Xian', 'Zhongshan Xian', ',21,263,2541,', '广西壮族自治区-贺州市-钟山县', '263', '02', '04', '01', '2541', '', null, null);
INSERT INTO t_sys_area VALUES ('2542', '451123', '451123', '451123', '富川瑶族自治县', 'Fuchuan Yaozu Zizhixian', 'Fuchuan Yaozu Zizhixian', ',21,263,2542,', '广西壮族自治区-贺州市-富川瑶族自治县', '263', '02', '04', '01', '2542', '', null, null);
INSERT INTO t_sys_area VALUES ('2543', '451201', '451201', '451201', '市辖区', '1', '1', ',21,264,2543,', '广西壮族自治区-河池市-市辖区', '264', '02', '04', '01', '2543', '', null, null);
INSERT INTO t_sys_area VALUES ('2544', '451202', '451202', '451202', '金城江区', 'Jinchengjiang Qu', 'Jinchengjiang Qu', ',21,264,2544,', '广西壮族自治区-河池市-金城江区', '264', '02', '04', '01', '2544', '', null, null);
INSERT INTO t_sys_area VALUES ('2545', '451221', '451221', '451221', '南丹县', 'Nandan Xian', 'Nandan Xian', ',21,264,2545,', '广西壮族自治区-河池市-南丹县', '264', '02', '04', '01', '2545', '', null, null);
INSERT INTO t_sys_area VALUES ('2546', '451222', '451222', '451222', '天峨县', 'Tian,e Xian', 'Tian,e Xian', ',21,264,2546,', '广西壮族自治区-河池市-天峨县', '264', '02', '04', '01', '2546', '', null, null);
INSERT INTO t_sys_area VALUES ('2547', '451223', '451223', '451223', '凤山县', 'Fengshan Xian', 'Fengshan Xian', ',21,264,2547,', '广西壮族自治区-河池市-凤山县', '264', '02', '04', '01', '2547', '', null, null);
INSERT INTO t_sys_area VALUES ('2548', '451224', '451224', '451224', '东兰县', 'Donglan Xian', 'Donglan Xian', ',21,264,2548,', '广西壮族自治区-河池市-东兰县', '264', '02', '04', '01', '2548', '', null, null);
INSERT INTO t_sys_area VALUES ('2549', '451225', '451225', '451225', '罗城仫佬族自治县', 'Luocheng Mulaozu Zizhixian', 'Luocheng Mulaozu Zizhixian', ',21,264,2549,', '广西壮族自治区-河池市-罗城仫佬族自治县', '264', '02', '04', '01', '2549', '', null, null);
INSERT INTO t_sys_area VALUES ('2550', '451226', '451226', '451226', '环江毛南族自治县', 'Huanjiang Maonanzu Zizhixian', 'Huanjiang Maonanzu Zizhixian', ',21,264,2550,', '广西壮族自治区-河池市-环江毛南族自治县', '264', '02', '04', '01', '2550', '', null, null);
INSERT INTO t_sys_area VALUES ('2551', '451227', '451227', '451227', '巴马瑶族自治县', 'Bama Yaozu Zizhixian', 'Bama Yaozu Zizhixian', ',21,264,2551,', '广西壮族自治区-河池市-巴马瑶族自治县', '264', '02', '04', '01', '2551', '', null, null);
INSERT INTO t_sys_area VALUES ('2552', '451228', '451228', '451228', '都安瑶族自治县', 'Du,an Yaozu Zizhixian', 'Du,an Yaozu Zizhixian', ',21,264,2552,', '广西壮族自治区-河池市-都安瑶族自治县', '264', '02', '04', '01', '2552', '', null, null);
INSERT INTO t_sys_area VALUES ('2553', '451229', '451229', '451229', '大化瑶族自治县', 'Dahua Yaozu Zizhixian', 'Dahua Yaozu Zizhixian', ',21,264,2553,', '广西壮族自治区-河池市-大化瑶族自治县', '264', '02', '04', '01', '2553', '', null, null);
INSERT INTO t_sys_area VALUES ('2554', '451281', '451281', '451281', '宜州市', 'Yizhou Shi', 'Yizhou Shi', ',21,264,2554,', '广西壮族自治区-河池市-宜州市', '264', '02', '04', '01', '2554', '', null, null);
INSERT INTO t_sys_area VALUES ('2555', '451301', '451301', '451301', '市辖区', '1', '1', ',21,265,2555,', '广西壮族自治区-来宾市-市辖区', '265', '02', '04', '01', '2555', '', null, null);
INSERT INTO t_sys_area VALUES ('2556', '451302', '451302', '451302', '兴宾区', 'Xingbin Qu', 'Xingbin Qu', ',21,265,2556,', '广西壮族自治区-来宾市-兴宾区', '265', '02', '04', '01', '2556', '', null, null);
INSERT INTO t_sys_area VALUES ('2557', '451321', '451321', '451321', '忻城县', 'Xincheng Xian', 'Xincheng Xian', ',21,265,2557,', '广西壮族自治区-来宾市-忻城县', '265', '02', '04', '01', '2557', '', null, null);
INSERT INTO t_sys_area VALUES ('2558', '451322', '451322', '451322', '象州县', 'Xiangzhou Xian', 'Xiangzhou Xian', ',21,265,2558,', '广西壮族自治区-来宾市-象州县', '265', '02', '04', '01', '2558', '', null, null);
INSERT INTO t_sys_area VALUES ('2559', '451323', '451323', '451323', '武宣县', 'Wuxuan Xian', 'Wuxuan Xian', ',21,265,2559,', '广西壮族自治区-来宾市-武宣县', '265', '02', '04', '01', '2559', '', null, null);
INSERT INTO t_sys_area VALUES ('2560', '451324', '451324', '451324', '金秀瑶族自治县', 'Jinxiu Yaozu Zizhixian', 'Jinxiu Yaozu Zizhixian', ',21,265,2560,', '广西壮族自治区-来宾市-金秀瑶族自治县', '265', '02', '04', '01', '2560', '', null, null);
INSERT INTO t_sys_area VALUES ('2561', '451381', '451381', '451381', '合山市', 'Heshan Shi', 'Heshan Shi', ',21,265,2561,', '广西壮族自治区-来宾市-合山市', '265', '02', '04', '01', '2561', '', null, null);
INSERT INTO t_sys_area VALUES ('2562', '451401', '451401', '451401', '市辖区', '1', '1', ',21,266,2562,', '广西壮族自治区-崇左市-市辖区', '266', '02', '04', '01', '2562', '', null, null);
INSERT INTO t_sys_area VALUES ('2563', '451402', '451402', '451402', '江洲区', 'Jiangzhou Qu', 'Jiangzhou Qu', ',21,266,2563,', '广西壮族自治区-崇左市-江洲区', '266', '02', '04', '01', '2563', '', null, null);
INSERT INTO t_sys_area VALUES ('2564', '451421', '451421', '451421', '扶绥县', 'Fusui Xian', 'Fusui Xian', ',21,266,2564,', '广西壮族自治区-崇左市-扶绥县', '266', '02', '04', '01', '2564', '', null, null);
INSERT INTO t_sys_area VALUES ('2565', '451422', '451422', '451422', '宁明县', 'Ningming Xian', 'Ningming Xian', ',21,266,2565,', '广西壮族自治区-崇左市-宁明县', '266', '02', '04', '01', '2565', '', null, null);
INSERT INTO t_sys_area VALUES ('2566', '451423', '451423', '451423', '龙州县', 'Longzhou Xian', 'Longzhou Xian', ',21,266,2566,', '广西壮族自治区-崇左市-龙州县', '266', '02', '04', '01', '2566', '', null, null);
INSERT INTO t_sys_area VALUES ('2567', '451424', '451424', '451424', '大新县', 'Daxin Xian', 'Daxin Xian', ',21,266,2567,', '广西壮族自治区-崇左市-大新县', '266', '02', '04', '01', '2567', '', null, null);
INSERT INTO t_sys_area VALUES ('2568', '451425', '451425', '451425', '天等县', 'Tiandeng Xian', 'Tiandeng Xian', ',21,266,2568,', '广西壮族自治区-崇左市-天等县', '266', '02', '04', '01', '2568', '', null, null);
INSERT INTO t_sys_area VALUES ('2569', '451481', '451481', '451481', '凭祥市', 'Pingxiang Shi', 'Pingxiang Shi', ',21,266,2569,', '广西壮族自治区-崇左市-凭祥市', '266', '02', '04', '01', '2569', '', null, null);
INSERT INTO t_sys_area VALUES ('2570', '460101', '460101', '460101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',22,267,2570,', '海南省-海口市-市辖区', '267', '02', '04', '01', '2570', '', null, null);
INSERT INTO t_sys_area VALUES ('2571', '460105', '460105', '460105', '秀英区', 'Xiuying Qu', 'Xiuying Qu', ',22,267,2571,', '海南省-海口市-秀英区', '267', '02', '04', '01', '2571', '', null, null);
INSERT INTO t_sys_area VALUES ('2572', '460106', '460106', '460106', '龙华区', 'LongHua Qu', 'LongHua Qu', ',22,267,2572,', '海南省-海口市-龙华区', '267', '02', '04', '01', '2572', '', null, null);
INSERT INTO t_sys_area VALUES ('2573', '460107', '460107', '460107', '琼山区', 'QiongShan Qu', 'QiongShan Qu', ',22,267,2573,', '海南省-海口市-琼山区', '267', '02', '04', '01', '2573', '', null, null);
INSERT INTO t_sys_area VALUES ('2574', '460108', '460108', '460108', '美兰区', 'MeiLan Qu', 'MeiLan Qu', ',22,267,2574,', '海南省-海口市-美兰区', '267', '02', '04', '01', '2574', '', null, null);
INSERT INTO t_sys_area VALUES ('2575', '460201', '460201', '460201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',22,268,2575,', '海南省-三亚市-市辖区', '268', '02', '04', '01', '2575', '', null, null);
INSERT INTO t_sys_area VALUES ('2576', '469001', '469001', '469001', '五指山市', 'Wuzhishan Qu', 'Wuzhishan Qu', ',22,269,2576,', '海南省-省直辖县级行政区划-五指山市', '269', '02', '04', '01', '2576', '', null, null);
INSERT INTO t_sys_area VALUES ('2577', '469002', '469002', '469002', '琼海市', 'Qionghai Shi', 'Qionghai Shi', ',22,269,2577,', '海南省-省直辖县级行政区划-琼海市', '269', '02', '04', '01', '2577', '', null, null);
INSERT INTO t_sys_area VALUES ('2578', '469003', '469003', '469003', '儋州市', 'Danzhou Shi', 'Danzhou Shi', ',22,269,2578,', '海南省-省直辖县级行政区划-儋州市', '269', '02', '04', '01', '2578', '', null, null);
INSERT INTO t_sys_area VALUES ('2579', '469005', '469005', '469005', '文昌市', 'Wenchang Shi', 'Wenchang Shi', ',22,269,2579,', '海南省-省直辖县级行政区划-文昌市', '269', '02', '04', '01', '2579', '', null, null);
INSERT INTO t_sys_area VALUES ('2580', '469006', '469006', '469006', '万宁市', 'Wanning Shi', 'Wanning Shi', ',22,269,2580,', '海南省-省直辖县级行政区划-万宁市', '269', '02', '04', '01', '2580', '', null, null);
INSERT INTO t_sys_area VALUES ('2581', '469007', '469007', '469007', '东方市', 'Dongfang Shi', 'Dongfang Shi', ',22,269,2581,', '海南省-省直辖县级行政区划-东方市', '269', '02', '04', '01', '2581', '', null, null);
INSERT INTO t_sys_area VALUES ('2582', '469021', '469021', '469021', '定安县', 'Ding,an Xian', 'Ding,an Xian', ',22,269,2582,', '海南省-省直辖县级行政区划-定安县', '269', '02', '04', '01', '2582', '', null, null);
INSERT INTO t_sys_area VALUES ('2583', '469022', '469022', '469022', '屯昌县', 'Tunchang Xian', 'Tunchang Xian', ',22,269,2583,', '海南省-省直辖县级行政区划-屯昌县', '269', '02', '04', '01', '2583', '', null, null);
INSERT INTO t_sys_area VALUES ('2584', '469023', '469023', '469023', '澄迈县', 'Chengmai Xian', 'Chengmai Xian', ',22,269,2584,', '海南省-省直辖县级行政区划-澄迈县', '269', '02', '04', '01', '2584', '', null, null);
INSERT INTO t_sys_area VALUES ('2585', '469024', '469024', '469024', '临高县', 'Lingao Xian', 'Lingao Xian', ',22,269,2585,', '海南省-省直辖县级行政区划-临高县', '269', '02', '04', '01', '2585', '', null, null);
INSERT INTO t_sys_area VALUES ('2586', '469025', '469025', '469025', '白沙黎族自治县', 'Baisha Lizu Zizhixian', 'Baisha Lizu Zizhixian', ',22,269,2586,', '海南省-省直辖县级行政区划-白沙黎族自治县', '269', '02', '04', '01', '2586', '', null, null);
INSERT INTO t_sys_area VALUES ('2587', '469026', '469026', '469026', '昌江黎族自治县', 'Changjiang Lizu Zizhixian', 'Changjiang Lizu Zizhixian', ',22,269,2587,', '海南省-省直辖县级行政区划-昌江黎族自治县', '269', '02', '04', '01', '2587', '', null, null);
INSERT INTO t_sys_area VALUES ('2588', '469027', '469027', '469027', '乐东黎族自治县', 'Ledong Lizu Zizhixian', 'Ledong Lizu Zizhixian', ',22,269,2588,', '海南省-省直辖县级行政区划-乐东黎族自治县', '269', '02', '04', '01', '2588', '', null, null);
INSERT INTO t_sys_area VALUES ('2589', '469028', '469028', '469028', '陵水黎族自治县', 'Lingshui Lizu Zizhixian', 'Lingshui Lizu Zizhixian', ',22,269,2589,', '海南省-省直辖县级行政区划-陵水黎族自治县', '269', '02', '04', '01', '2589', '', null, null);
INSERT INTO t_sys_area VALUES ('2590', '469029', '469029', '469029', '保亭黎族苗族自治县', 'Baoting Lizu Miaozu Zizhixian', 'Baoting Lizu Miaozu Zizhixian', ',22,269,2590,', '海南省-省直辖县级行政区划-保亭黎族苗族自治县', '269', '02', '04', '01', '2590', '', null, null);
INSERT INTO t_sys_area VALUES ('2591', '469030', '469030', '469030', '琼中黎族苗族自治县', 'Qiongzhong Lizu Miaozu Zizhixian', 'Qiongzhong Lizu Miaozu Zizhixian', ',22,269,2591,', '海南省-省直辖县级行政区划-琼中黎族苗族自治县', '269', '02', '04', '01', '2591', '', null, null);
INSERT INTO t_sys_area VALUES ('2592', '469031', '469031', '469031', '西沙群岛', 'Xisha Qundao', 'Xisha Qundao', ',22,269,2592,', '海南省-省直辖县级行政区划-西沙群岛', '269', '02', '04', '01', '2592', '', null, null);
INSERT INTO t_sys_area VALUES ('2593', '469032', '469032', '469032', '南沙群岛', 'Nansha Qundao', 'Nansha Qundao', ',22,269,2593,', '海南省-省直辖县级行政区划-南沙群岛', '269', '02', '04', '01', '2593', '', null, null);
INSERT INTO t_sys_area VALUES ('2594', '469033', '469033', '469033', '中沙群岛的岛礁及其海域', 'Zhongsha Qundao de Daojiao Jiqi Haiyu', 'Zhongsha Qundao de Daojiao Jiqi Haiyu', ',22,269,2594,', '海南省-省直辖县级行政区划-中沙群岛的岛礁及其海域', '269', '02', '04', '01', '2594', '', null, null);
INSERT INTO t_sys_area VALUES ('2595', '500101', '500101', '500101', '万州区', 'Wanzhou Qu', 'Wanzhou Qu', ',23,270,2595,', '重庆-重庆市-万州区', '270', '02', '04', '01', '2595', '', null, null);
INSERT INTO t_sys_area VALUES ('2596', '500102', '500102', '500102', '涪陵区', 'Fuling Qu', 'Fuling Qu', ',23,270,2596,', '重庆-重庆市-涪陵区', '270', '02', '04', '01', '2596', '', null, null);
INSERT INTO t_sys_area VALUES ('2597', '500103', '500103', '500103', '渝中区', 'Yuzhong Qu', 'Yuzhong Qu', ',23,270,2597,', '重庆-重庆市-渝中区', '270', '02', '04', '01', '2597', '', null, null);
INSERT INTO t_sys_area VALUES ('2598', '500104', '500104', '500104', '大渡口区', 'Dadukou Qu', 'Dadukou Qu', ',23,270,2598,', '重庆-重庆市-大渡口区', '270', '02', '04', '01', '2598', '', null, null);
INSERT INTO t_sys_area VALUES ('2599', '500105', '500105', '500105', '江北区', 'Jiangbei Qu', 'Jiangbei Qu', ',23,270,2599,', '重庆-重庆市-江北区', '270', '02', '04', '01', '2599', '', null, null);
INSERT INTO t_sys_area VALUES ('2600', '500106', '500106', '500106', '沙坪坝区', 'Shapingba Qu', 'Shapingba Qu', ',23,270,2600,', '重庆-重庆市-沙坪坝区', '270', '02', '04', '01', '2600', '', null, null);
INSERT INTO t_sys_area VALUES ('2601', '500107', '500107', '500107', '九龙坡区', 'Jiulongpo Qu', 'Jiulongpo Qu', ',23,270,2601,', '重庆-重庆市-九龙坡区', '270', '02', '04', '01', '2601', '', null, null);
INSERT INTO t_sys_area VALUES ('2602', '500108', '500108', '500108', '南岸区', 'Nan,an Qu', 'Nan,an Qu', ',23,270,2602,', '重庆-重庆市-南岸区', '270', '02', '04', '01', '2602', '', null, null);
INSERT INTO t_sys_area VALUES ('2603', '500109', '500109', '500109', '北碚区', 'Beibei Qu', 'Beibei Qu', ',23,270,2603,', '重庆-重庆市-北碚区', '270', '02', '04', '01', '2603', '', null, null);
INSERT INTO t_sys_area VALUES ('2604', '500110', '500110', '500110', '万盛区', 'Wansheng Qu', 'Wansheng Qu', ',23,270,2604,', '重庆-重庆市-万盛区', '270', '02', '04', '01', '2604', '', null, null);
INSERT INTO t_sys_area VALUES ('2605', '500111', '500111', '500111', '双桥区', 'Shuangqiao Qu', 'Shuangqiao Qu', ',23,270,2605,', '重庆-重庆市-双桥区', '270', '02', '04', '01', '2605', '', null, null);
INSERT INTO t_sys_area VALUES ('2606', '500112', '500112', '500112', '渝北区', 'Yubei Qu', 'Yubei Qu', ',23,270,2606,', '重庆-重庆市-渝北区', '270', '02', '04', '01', '2606', '', null, null);
INSERT INTO t_sys_area VALUES ('2607', '500113', '500113', '500113', '巴南区', 'Banan Qu', 'Banan Qu', ',23,270,2607,', '重庆-重庆市-巴南区', '270', '02', '04', '01', '2607', '', null, null);
INSERT INTO t_sys_area VALUES ('2608', '500114', '500114', '500114', '黔江区', 'Qianjiang Qu', 'Qianjiang Qu', ',23,270,2608,', '重庆-重庆市-黔江区', '270', '02', '04', '01', '2608', '', null, null);
INSERT INTO t_sys_area VALUES ('2609', '500115', '500115', '500115', '长寿区', 'Changshou Qu', 'Changshou Qu', ',23,270,2609,', '重庆-重庆市-长寿区', '270', '02', '04', '01', '2609', '', null, null);
INSERT INTO t_sys_area VALUES ('2610', '500222', '500222', '500222', '綦江县', 'Qijiang Xian', 'Qijiang Xian', ',23,270,2610,', '重庆-重庆市-綦江县', '270', '02', '04', '01', '2610', '', null, null);
INSERT INTO t_sys_area VALUES ('2611', '500223', '500223', '500223', '潼南县', 'Tongnan Xian', 'Tongnan Xian', ',23,270,2611,', '重庆-重庆市-潼南县', '270', '02', '04', '01', '2611', '', null, null);
INSERT INTO t_sys_area VALUES ('2612', '500224', '500224', '500224', '铜梁县', 'Tongliang Xian', 'Tongliang Xian', ',23,270,2612,', '重庆-重庆市-铜梁县', '270', '02', '04', '01', '2612', '', null, null);
INSERT INTO t_sys_area VALUES ('2613', '500225', '500225', '500225', '大足县', 'Dazu Xian', 'Dazu Xian', ',23,270,2613,', '重庆-重庆市-大足县', '270', '02', '04', '01', '2613', '', null, null);
INSERT INTO t_sys_area VALUES ('2614', '500226', '500226', '500226', '荣昌县', 'Rongchang Xian', 'Rongchang Xian', ',23,270,2614,', '重庆-重庆市-荣昌县', '270', '02', '04', '01', '2614', '', null, null);
INSERT INTO t_sys_area VALUES ('2615', '500227', '500227', '500227', '璧山县', 'Bishan Xian', 'Bishan Xian', ',23,270,2615,', '重庆-重庆市-璧山县', '270', '02', '04', '01', '2615', '', null, null);
INSERT INTO t_sys_area VALUES ('2616', '500228', '500228', '500228', '梁平县', 'Liangping Xian', 'Liangping Xian', ',23,270,2616,', '重庆-重庆市-梁平县', '270', '02', '04', '01', '2616', '', null, null);
INSERT INTO t_sys_area VALUES ('2617', '500229', '500229', '500229', '城口县', 'Chengkou Xian', 'Chengkou Xian', ',23,270,2617,', '重庆-重庆市-城口县', '270', '02', '04', '01', '2617', '', null, null);
INSERT INTO t_sys_area VALUES ('2618', '500230', '500230', '500230', '丰都县', 'Fengdu Xian', 'Fengdu Xian', ',23,270,2618,', '重庆-重庆市-丰都县', '270', '02', '04', '01', '2618', '', null, null);
INSERT INTO t_sys_area VALUES ('2619', '500231', '500231', '500231', '垫江县', 'Dianjiang Xian', 'Dianjiang Xian', ',23,270,2619,', '重庆-重庆市-垫江县', '270', '02', '04', '01', '2619', '', null, null);
INSERT INTO t_sys_area VALUES ('2620', '500232', '500232', '500232', '武隆县', 'Wulong Xian', 'Wulong Xian', ',23,270,2620,', '重庆-重庆市-武隆县', '270', '02', '04', '01', '2620', '', null, null);
INSERT INTO t_sys_area VALUES ('2621', '500233', '500233', '500233', '忠县', 'Zhong Xian', 'Zhong Xian', ',23,270,2621,', '重庆-重庆市-忠县', '270', '02', '04', '01', '2621', '', null, null);
INSERT INTO t_sys_area VALUES ('2622', '500234', '500234', '500234', '开县', 'Kai Xian', 'Kai Xian', ',23,270,2622,', '重庆-重庆市-开县', '270', '02', '04', '01', '2622', '', null, null);
INSERT INTO t_sys_area VALUES ('2623', '500235', '500235', '500235', '云阳县', 'Yunyang Xian', 'Yunyang Xian', ',23,270,2623,', '重庆-重庆市-云阳县', '270', '02', '04', '01', '2623', '', null, null);
INSERT INTO t_sys_area VALUES ('2624', '500236', '500236', '500236', '奉节县', 'Fengjie Xian', 'Fengjie Xian', ',23,270,2624,', '重庆-重庆市-奉节县', '270', '02', '04', '01', '2624', '', null, null);
INSERT INTO t_sys_area VALUES ('2625', '500237', '500237', '500237', '巫山县', 'Wushan Xian', 'Wushan Xian', ',23,270,2625,', '重庆-重庆市-巫山县', '270', '02', '04', '01', '2625', '', null, null);
INSERT INTO t_sys_area VALUES ('2626', '500238', '500238', '500238', '巫溪县', 'Wuxi Xian', 'Wuxi Xian', ',23,270,2626,', '重庆-重庆市-巫溪县', '270', '02', '04', '01', '2626', '', null, null);
INSERT INTO t_sys_area VALUES ('2627', '500240', '500240', '500240', '石柱土家族自治县', 'Shizhu Tujiazu Zizhixian', 'Shizhu Tujiazu Zizhixian', ',23,270,2627,', '重庆-重庆市-石柱土家族自治县', '270', '02', '04', '01', '2627', '', null, null);
INSERT INTO t_sys_area VALUES ('2628', '500241', '500241', '500241', '秀山土家族苗族自治县', 'Xiushan Tujiazu Miaozu Zizhixian', 'Xiushan Tujiazu Miaozu Zizhixian', ',23,270,2628,', '重庆-重庆市-秀山土家族苗族自治县', '270', '02', '04', '01', '2628', '', null, null);
INSERT INTO t_sys_area VALUES ('2629', '500242', '500242', '500242', '酉阳土家族苗族自治县', 'Youyang Tujiazu Miaozu Zizhixian', 'Youyang Tujiazu Miaozu Zizhixian', ',23,270,2629,', '重庆-重庆市-酉阳土家族苗族自治县', '270', '02', '04', '01', '2629', '', null, null);
INSERT INTO t_sys_area VALUES ('2630', '500243', '500243', '500243', '彭水苗族土家族自治县', 'Pengshui Miaozu Tujiazu Zizhixian', 'Pengshui Miaozu Tujiazu Zizhixian', ',23,270,2630,', '重庆-重庆市-彭水苗族土家族自治县', '270', '02', '04', '01', '2630', '', null, null);
INSERT INTO t_sys_area VALUES ('2631', '500116', '500116', '500116', '江津区', 'Jiangjin Shi', 'Jiangjin Shi', ',23,270,2631,', '重庆-重庆市-江津区', '270', '02', '04', '01', '2631', '', null, null);
INSERT INTO t_sys_area VALUES ('2632', '500117', '500117', '500117', '合川区', 'Hechuan Shi', 'Hechuan Shi', ',23,270,2632,', '重庆-重庆市-合川区', '270', '02', '04', '01', '2632', '', null, null);
INSERT INTO t_sys_area VALUES ('2633', '500118', '500118', '500118', '永川区', 'Yongchuan Shi', 'Yongchuan Shi', ',23,270,2633,', '重庆-重庆市-永川区', '270', '02', '04', '01', '2633', '', null, null);
INSERT INTO t_sys_area VALUES ('2634', '500119', '500119', '500119', '南川区', 'Nanchuan Shi', 'Nanchuan Shi', ',23,270,2634,', '重庆-重庆市-南川区', '270', '02', '04', '01', '2634', '', null, null);
INSERT INTO t_sys_area VALUES ('2635', '510101', '510101', '510101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',24,273,2635,', '四川省-成都市-市辖区', '273', '02', '04', '01', '2635', '', null, null);
INSERT INTO t_sys_area VALUES ('2636', '510104', '510104', '510104', '锦江区', 'Jinjiang Qu', 'Jinjiang Qu', ',24,273,2636,', '四川省-成都市-锦江区', '273', '02', '04', '01', '2636', '', null, null);
INSERT INTO t_sys_area VALUES ('2637', '510105', '510105', '510105', '青羊区', 'Qingyang Qu', 'Qingyang Qu', ',24,273,2637,', '四川省-成都市-青羊区', '273', '02', '04', '01', '2637', '', null, null);
INSERT INTO t_sys_area VALUES ('2638', '510106', '510106', '510106', '金牛区', 'Jinniu Qu', 'Jinniu Qu', ',24,273,2638,', '四川省-成都市-金牛区', '273', '02', '04', '01', '2638', '', null, null);
INSERT INTO t_sys_area VALUES ('2639', '510107', '510107', '510107', '武侯区', 'Wuhou Qu', 'Wuhou Qu', ',24,273,2639,', '四川省-成都市-武侯区', '273', '02', '04', '01', '2639', '', null, null);
INSERT INTO t_sys_area VALUES ('2640', '510108', '510108', '510108', '成华区', 'Chenghua Qu', 'Chenghua Qu', ',24,273,2640,', '四川省-成都市-成华区', '273', '02', '04', '01', '2640', '', null, null);
INSERT INTO t_sys_area VALUES ('2641', '510112', '510112', '510112', '龙泉驿区', 'Longquanyi Qu', 'Longquanyi Qu', ',24,273,2641,', '四川省-成都市-龙泉驿区', '273', '02', '04', '01', '2641', '', null, null);
INSERT INTO t_sys_area VALUES ('2642', '510113', '510113', '510113', '青白江区', 'Qingbaijiang Qu', 'Qingbaijiang Qu', ',24,273,2642,', '四川省-成都市-青白江区', '273', '02', '04', '01', '2642', '', null, null);
INSERT INTO t_sys_area VALUES ('2643', '510114', '510114', '510114', '新都区', 'Xindu Qu', 'Xindu Qu', ',24,273,2643,', '四川省-成都市-新都区', '273', '02', '04', '01', '2643', '', null, null);
INSERT INTO t_sys_area VALUES ('2644', '510115', '510115', '510115', '温江区', 'Wenjiang Qu', 'Wenjiang Qu', ',24,273,2644,', '四川省-成都市-温江区', '273', '02', '04', '01', '2644', '', null, null);
INSERT INTO t_sys_area VALUES ('2645', '510121', '510121', '510121', '金堂县', 'Jintang Xian', 'Jintang Xian', ',24,273,2645,', '四川省-成都市-金堂县', '273', '02', '04', '01', '2645', '', null, null);
INSERT INTO t_sys_area VALUES ('2646', '510122', '510122', '510122', '双流县', 'Shuangliu Xian', 'Shuangliu Xian', ',24,273,2646,', '四川省-成都市-双流县', '273', '02', '04', '01', '2646', '', null, null);
INSERT INTO t_sys_area VALUES ('2647', '510124', '510124', '510124', '郫县', 'Pi Xian', 'Pi Xian', ',24,273,2647,', '四川省-成都市-郫县', '273', '02', '04', '01', '2647', '', null, null);
INSERT INTO t_sys_area VALUES ('2648', '510129', '510129', '510129', '大邑县', 'Dayi Xian', 'Dayi Xian', ',24,273,2648,', '四川省-成都市-大邑县', '273', '02', '04', '01', '2648', '', null, null);
INSERT INTO t_sys_area VALUES ('2649', '510131', '510131', '510131', '蒲江县', 'Pujiang Xian', 'Pujiang Xian', ',24,273,2649,', '四川省-成都市-蒲江县', '273', '02', '04', '01', '2649', '', null, null);
INSERT INTO t_sys_area VALUES ('2650', '510132', '510132', '510132', '新津县', 'Xinjin Xian', 'Xinjin Xian', ',24,273,2650,', '四川省-成都市-新津县', '273', '02', '04', '01', '2650', '', null, null);
INSERT INTO t_sys_area VALUES ('2651', '510181', '510181', '510181', '都江堰市', 'Dujiangyan Shi', 'Dujiangyan Shi', ',24,273,2651,', '四川省-成都市-都江堰市', '273', '02', '04', '01', '2651', '', null, null);
INSERT INTO t_sys_area VALUES ('2652', '510182', '510182', '510182', '彭州市', 'Pengzhou Shi', 'Pengzhou Shi', ',24,273,2652,', '四川省-成都市-彭州市', '273', '02', '04', '01', '2652', '', null, null);
INSERT INTO t_sys_area VALUES ('2653', '510183', '510183', '510183', '邛崃市', 'Qionglai Shi', 'Qionglai Shi', ',24,273,2653,', '四川省-成都市-邛崃市', '273', '02', '04', '01', '2653', '', null, null);
INSERT INTO t_sys_area VALUES ('2654', '510184', '510184', '510184', '崇州市', 'Chongzhou Shi', 'Chongzhou Shi', ',24,273,2654,', '四川省-成都市-崇州市', '273', '02', '04', '01', '2654', '', null, null);
INSERT INTO t_sys_area VALUES ('2655', '510301', '510301', '510301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',24,274,2655,', '四川省-自贡市-市辖区', '274', '02', '04', '01', '2655', '', null, null);
INSERT INTO t_sys_area VALUES ('2656', '510302', '510302', '510302', '自流井区', 'Ziliujing Qu', 'Ziliujing Qu', ',24,274,2656,', '四川省-自贡市-自流井区', '274', '02', '04', '01', '2656', '', null, null);
INSERT INTO t_sys_area VALUES ('2657', '510303', '510303', '510303', '贡井区', 'Gongjing Qu', 'Gongjing Qu', ',24,274,2657,', '四川省-自贡市-贡井区', '274', '02', '04', '01', '2657', '', null, null);
INSERT INTO t_sys_area VALUES ('2658', '510304', '510304', '510304', '大安区', 'Da,an Qu', 'Da,an Qu', ',24,274,2658,', '四川省-自贡市-大安区', '274', '02', '04', '01', '2658', '', null, null);
INSERT INTO t_sys_area VALUES ('2659', '510311', '510311', '510311', '沿滩区', 'Yantan Qu', 'Yantan Qu', ',24,274,2659,', '四川省-自贡市-沿滩区', '274', '02', '04', '01', '2659', '', null, null);
INSERT INTO t_sys_area VALUES ('2660', '510321', '510321', '510321', '荣县', 'Rong Xian', 'Rong Xian', ',24,274,2660,', '四川省-自贡市-荣县', '274', '02', '04', '01', '2660', '', null, null);
INSERT INTO t_sys_area VALUES ('2661', '510322', '510322', '510322', '富顺县', 'Fushun Xian', 'Fushun Xian', ',24,274,2661,', '四川省-自贡市-富顺县', '274', '02', '04', '01', '2661', '', null, null);
INSERT INTO t_sys_area VALUES ('2662', '510401', '510401', '510401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',24,275,2662,', '四川省-攀枝花市-市辖区', '275', '02', '04', '01', '2662', '', null, null);
INSERT INTO t_sys_area VALUES ('2663', '510402', '510402', '510402', '东区', 'Dong Qu', 'Dong Qu', ',24,275,2663,', '四川省-攀枝花市-东区', '275', '02', '04', '01', '2663', '', null, null);
INSERT INTO t_sys_area VALUES ('2664', '510403', '510403', '510403', '西区', 'Xi Qu', 'Xi Qu', ',24,275,2664,', '四川省-攀枝花市-西区', '275', '02', '04', '01', '2664', '', null, null);
INSERT INTO t_sys_area VALUES ('2665', '510411', '510411', '510411', '仁和区', 'Renhe Qu', 'Renhe Qu', ',24,275,2665,', '四川省-攀枝花市-仁和区', '275', '02', '04', '01', '2665', '', null, null);
INSERT INTO t_sys_area VALUES ('2666', '510421', '510421', '510421', '米易县', 'Miyi Xian', 'Miyi Xian', ',24,275,2666,', '四川省-攀枝花市-米易县', '275', '02', '04', '01', '2666', '', null, null);
INSERT INTO t_sys_area VALUES ('2667', '510422', '510422', '510422', '盐边县', 'Yanbian Xian', 'Yanbian Xian', ',24,275,2667,', '四川省-攀枝花市-盐边县', '275', '02', '04', '01', '2667', '', null, null);
INSERT INTO t_sys_area VALUES ('2668', '510501', '510501', '510501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',24,276,2668,', '四川省-泸州市-市辖区', '276', '02', '04', '01', '2668', '', null, null);
INSERT INTO t_sys_area VALUES ('2669', '510502', '510502', '510502', '江阳区', 'Jiangyang Qu', 'Jiangyang Qu', ',24,276,2669,', '四川省-泸州市-江阳区', '276', '02', '04', '01', '2669', '', null, null);
INSERT INTO t_sys_area VALUES ('2670', '510503', '510503', '510503', '纳溪区', 'Naxi Qu', 'Naxi Qu', ',24,276,2670,', '四川省-泸州市-纳溪区', '276', '02', '04', '01', '2670', '', null, null);
INSERT INTO t_sys_area VALUES ('2671', '510504', '510504', '510504', '龙马潭区', 'Longmatan Qu', 'Longmatan Qu', ',24,276,2671,', '四川省-泸州市-龙马潭区', '276', '02', '04', '01', '2671', '', null, null);
INSERT INTO t_sys_area VALUES ('2672', '510521', '510521', '510521', '泸县', 'Lu Xian', 'Lu Xian', ',24,276,2672,', '四川省-泸州市-泸县', '276', '02', '04', '01', '2672', '', null, null);
INSERT INTO t_sys_area VALUES ('2673', '510522', '510522', '510522', '合江县', 'Hejiang Xian', 'Hejiang Xian', ',24,276,2673,', '四川省-泸州市-合江县', '276', '02', '04', '01', '2673', '', null, null);
INSERT INTO t_sys_area VALUES ('2674', '510524', '510524', '510524', '叙永县', 'Xuyong Xian', 'Xuyong Xian', ',24,276,2674,', '四川省-泸州市-叙永县', '276', '02', '04', '01', '2674', '', null, null);
INSERT INTO t_sys_area VALUES ('2675', '510525', '510525', '510525', '古蔺县', 'Gulin Xian', 'Gulin Xian', ',24,276,2675,', '四川省-泸州市-古蔺县', '276', '02', '04', '01', '2675', '', null, null);
INSERT INTO t_sys_area VALUES ('2676', '510601', '510601', '510601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',24,277,2676,', '四川省-德阳市-市辖区', '277', '02', '04', '01', '2676', '', null, null);
INSERT INTO t_sys_area VALUES ('2677', '510603', '510603', '510603', '旌阳区', 'Jingyang Qu', 'Jingyang Qu', ',24,277,2677,', '四川省-德阳市-旌阳区', '277', '02', '04', '01', '2677', '', null, null);
INSERT INTO t_sys_area VALUES ('2678', '510623', '510623', '510623', '中江县', 'Zhongjiang Xian', 'Zhongjiang Xian', ',24,277,2678,', '四川省-德阳市-中江县', '277', '02', '04', '01', '2678', '', null, null);
INSERT INTO t_sys_area VALUES ('2679', '510626', '510626', '510626', '罗江县', 'Luojiang Xian', 'Luojiang Xian', ',24,277,2679,', '四川省-德阳市-罗江县', '277', '02', '04', '01', '2679', '', null, null);
INSERT INTO t_sys_area VALUES ('2680', '510681', '510681', '510681', '广汉市', 'Guanghan Shi', 'Guanghan Shi', ',24,277,2680,', '四川省-德阳市-广汉市', '277', '02', '04', '01', '2680', '', null, null);
INSERT INTO t_sys_area VALUES ('2681', '510682', '510682', '510682', '什邡市', 'Shifang Shi', 'Shifang Shi', ',24,277,2681,', '四川省-德阳市-什邡市', '277', '02', '04', '01', '2681', '', null, null);
INSERT INTO t_sys_area VALUES ('2682', '510683', '510683', '510683', '绵竹市', 'Jinzhou Shi', 'Jinzhou Shi', ',24,277,2682,', '四川省-德阳市-绵竹市', '277', '02', '04', '01', '2682', '', null, null);
INSERT INTO t_sys_area VALUES ('2683', '510701', '510701', '510701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',24,278,2683,', '四川省-绵阳市-市辖区', '278', '02', '04', '01', '2683', '', null, null);
INSERT INTO t_sys_area VALUES ('2684', '510703', '510703', '510703', '涪城区', 'Fucheng Qu', 'Fucheng Qu', ',24,278,2684,', '四川省-绵阳市-涪城区', '278', '02', '04', '01', '2684', '', null, null);
INSERT INTO t_sys_area VALUES ('2685', '510704', '510704', '510704', '游仙区', 'Youxian Qu', 'Youxian Qu', ',24,278,2685,', '四川省-绵阳市-游仙区', '278', '02', '04', '01', '2685', '', null, null);
INSERT INTO t_sys_area VALUES ('2686', '510722', '510722', '510722', '三台县', 'Santai Xian', 'Santai Xian', ',24,278,2686,', '四川省-绵阳市-三台县', '278', '02', '04', '01', '2686', '', null, null);
INSERT INTO t_sys_area VALUES ('2687', '510723', '510723', '510723', '盐亭县', 'Yanting Xian', 'Yanting Xian', ',24,278,2687,', '四川省-绵阳市-盐亭县', '278', '02', '04', '01', '2687', '', null, null);
INSERT INTO t_sys_area VALUES ('2688', '510724', '510724', '510724', '安县', 'An Xian', 'An Xian', ',24,278,2688,', '四川省-绵阳市-安县', '278', '02', '04', '01', '2688', '', null, null);
INSERT INTO t_sys_area VALUES ('2689', '510725', '510725', '510725', '梓潼县', 'Zitong Xian', 'Zitong Xian', ',24,278,2689,', '四川省-绵阳市-梓潼县', '278', '02', '04', '01', '2689', '', null, null);
INSERT INTO t_sys_area VALUES ('2690', '510726', '510726', '510726', '北川羌族自治县', 'Beichuanqiangzuzizhi Qu', 'Beichuanqiangzuzizhi Qu', ',24,278,2690,', '四川省-绵阳市-北川羌族自治县', '278', '02', '04', '01', '2690', '', null, null);
INSERT INTO t_sys_area VALUES ('2691', '510727', '510727', '510727', '平武县', 'Pingwu Xian', 'Pingwu Xian', ',24,278,2691,', '四川省-绵阳市-平武县', '278', '02', '04', '01', '2691', '', null, null);
INSERT INTO t_sys_area VALUES ('2692', '510781', '510781', '510781', '江油市', 'Jiangyou Shi', 'Jiangyou Shi', ',24,278,2692,', '四川省-绵阳市-江油市', '278', '02', '04', '01', '2692', '', null, null);
INSERT INTO t_sys_area VALUES ('2693', '510801', '510801', '510801', '市辖区', 'Shixiaqu', 'Shixiaqu', ',24,279,2693,', '四川省-广元市-市辖区', '279', '02', '04', '01', '2693', '', null, null);
INSERT INTO t_sys_area VALUES ('2694', '511002', '511002', '511002', '市中区', 'Shizhong Qu', 'Shizhong Qu', ',24,279,2694,', '四川省-广元市-市中区', '279', '02', '04', '01', '2694', '', null, null);
INSERT INTO t_sys_area VALUES ('2695', '510811', '510811', '510811', '元坝区', 'Yuanba Qu', 'Yuanba Qu', ',24,279,2695,', '四川省-广元市-元坝区', '279', '02', '04', '01', '2695', '', null, null);
INSERT INTO t_sys_area VALUES ('2696', '510812', '510812', '510812', '朝天区', 'Chaotian Qu', 'Chaotian Qu', ',24,279,2696,', '四川省-广元市-朝天区', '279', '02', '04', '01', '2696', '', null, null);
INSERT INTO t_sys_area VALUES ('2697', '510821', '510821', '510821', '旺苍县', 'Wangcang Xian', 'Wangcang Xian', ',24,279,2697,', '四川省-广元市-旺苍县', '279', '02', '04', '01', '2697', '', null, null);
INSERT INTO t_sys_area VALUES ('2698', '510822', '510822', '510822', '青川县', 'Qingchuan Xian', 'Qingchuan Xian', ',24,279,2698,', '四川省-广元市-青川县', '279', '02', '04', '01', '2698', '', null, null);
INSERT INTO t_sys_area VALUES ('2699', '510823', '510823', '510823', '剑阁县', 'Jiange Xian', 'Jiange Xian', ',24,279,2699,', '四川省-广元市-剑阁县', '279', '02', '04', '01', '2699', '', null, null);
INSERT INTO t_sys_area VALUES ('2700', '510824', '510824', '510824', '苍溪县', 'Cangxi Xian', 'Cangxi Xian', ',24,279,2700,', '四川省-广元市-苍溪县', '279', '02', '04', '01', '2700', '', null, null);
INSERT INTO t_sys_area VALUES ('2701', '510901', '510901', '510901', '市辖区', 'Shixiaqu', 'Shixiaqu', ',24,280,2701,', '四川省-遂宁市-市辖区', '280', '02', '04', '01', '2701', '', null, null);
INSERT INTO t_sys_area VALUES ('2702', '510903', '510903', '510903', '船山区', 'Chuanshan Qu', 'Chuanshan Qu', ',24,280,2702,', '四川省-遂宁市-船山区', '280', '02', '04', '01', '2702', '', null, null);
INSERT INTO t_sys_area VALUES ('2703', '510904', '510904', '510904', '安居区', 'Anju Qu', 'Anju Qu', ',24,280,2703,', '四川省-遂宁市-安居区', '280', '02', '04', '01', '2703', '', null, null);
INSERT INTO t_sys_area VALUES ('2704', '510921', '510921', '510921', '蓬溪县', 'Pengxi Xian', 'Pengxi Xian', ',24,280,2704,', '四川省-遂宁市-蓬溪县', '280', '02', '04', '01', '2704', '', null, null);
INSERT INTO t_sys_area VALUES ('2705', '510922', '510922', '510922', '射洪县', 'Shehong Xian', 'Shehong Xian', ',24,280,2705,', '四川省-遂宁市-射洪县', '280', '02', '04', '01', '2705', '', null, null);
INSERT INTO t_sys_area VALUES ('2706', '510923', '510923', '510923', '大英县', 'Daying Xian', 'Daying Xian', ',24,280,2706,', '四川省-遂宁市-大英县', '280', '02', '04', '01', '2706', '', null, null);
INSERT INTO t_sys_area VALUES ('2707', '511001', '511001', '511001', '市辖区', 'Shixiaqu', 'Shixiaqu', ',24,281,2707,', '四川省-内江市-市辖区', '281', '02', '04', '01', '2707', '', null, null);
INSERT INTO t_sys_area VALUES ('2708', '511002', '511002', '511002', '市中区', 'Shizhong Qu', 'Shizhong Qu', ',24,281,2708,', '四川省-内江市-市中区', '281', '02', '04', '01', '2708', '', null, null);
INSERT INTO t_sys_area VALUES ('2709', '511011', '511011', '511011', '东兴区', 'Dongxing Qu', 'Dongxing Qu', ',24,281,2709,', '四川省-内江市-东兴区', '281', '02', '04', '01', '2709', '', null, null);
INSERT INTO t_sys_area VALUES ('2710', '511024', '511024', '511024', '威远县', 'Weiyuan Xian', 'Weiyuan Xian', ',24,281,2710,', '四川省-内江市-威远县', '281', '02', '04', '01', '2710', '', null, null);
INSERT INTO t_sys_area VALUES ('2711', '511025', '511025', '511025', '资中县', 'Zizhong Xian', 'Zizhong Xian', ',24,281,2711,', '四川省-内江市-资中县', '281', '02', '04', '01', '2711', '', null, null);
INSERT INTO t_sys_area VALUES ('2712', '511028', '511028', '511028', '隆昌县', 'Longchang Xian', 'Longchang Xian', ',24,281,2712,', '四川省-内江市-隆昌县', '281', '02', '04', '01', '2712', '', null, null);
INSERT INTO t_sys_area VALUES ('2713', '511101', '511101', '511101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',24,282,2713,', '四川省-乐山市-市辖区', '282', '02', '04', '01', '2713', '', null, null);
INSERT INTO t_sys_area VALUES ('2714', '511102', '511102', '511102', '市中区', 'Shizhong Qu', 'Shizhong Qu', ',24,282,2714,', '四川省-乐山市-市中区', '282', '02', '04', '01', '2714', '', null, null);
INSERT INTO t_sys_area VALUES ('2715', '511111', '511111', '511111', '沙湾区', 'Shawan Qu', 'Shawan Qu', ',24,282,2715,', '四川省-乐山市-沙湾区', '282', '02', '04', '01', '2715', '', null, null);
INSERT INTO t_sys_area VALUES ('2716', '511112', '511112', '511112', '五通桥区', 'Wutongqiao Qu', 'Wutongqiao Qu', ',24,282,2716,', '四川省-乐山市-五通桥区', '282', '02', '04', '01', '2716', '', null, null);
INSERT INTO t_sys_area VALUES ('2717', '511113', '511113', '511113', '金口河区', 'Jinkouhe Qu', 'Jinkouhe Qu', ',24,282,2717,', '四川省-乐山市-金口河区', '282', '02', '04', '01', '2717', '', null, null);
INSERT INTO t_sys_area VALUES ('2718', '511123', '511123', '511123', '犍为县', 'Qianwei Xian', 'Qianwei Xian', ',24,282,2718,', '四川省-乐山市-犍为县', '282', '02', '04', '01', '2718', '', null, null);
INSERT INTO t_sys_area VALUES ('2719', '511124', '511124', '511124', '井研县', 'Jingyan Xian', 'Jingyan Xian', ',24,282,2719,', '四川省-乐山市-井研县', '282', '02', '04', '01', '2719', '', null, null);
INSERT INTO t_sys_area VALUES ('2720', '511126', '511126', '511126', '夹江县', 'Jiajiang Xian', 'Jiajiang Xian', ',24,282,2720,', '四川省-乐山市-夹江县', '282', '02', '04', '01', '2720', '', null, null);
INSERT INTO t_sys_area VALUES ('2721', '511129', '511129', '511129', '沐川县', 'Muchuan Xian', 'Muchuan Xian', ',24,282,2721,', '四川省-乐山市-沐川县', '282', '02', '04', '01', '2721', '', null, null);
INSERT INTO t_sys_area VALUES ('2722', '511132', '511132', '511132', '峨边彝族自治县', 'Ebian Yizu Zizhixian', 'Ebian Yizu Zizhixian', ',24,282,2722,', '四川省-乐山市-峨边彝族自治县', '282', '02', '04', '01', '2722', '', null, null);
INSERT INTO t_sys_area VALUES ('2723', '511133', '511133', '511133', '马边彝族自治县', 'Mabian Yizu Zizhixian', 'Mabian Yizu Zizhixian', ',24,282,2723,', '四川省-乐山市-马边彝族自治县', '282', '02', '04', '01', '2723', '', null, null);
INSERT INTO t_sys_area VALUES ('2724', '511181', '511181', '511181', '峨眉山市', 'Emeishan Shi', 'Emeishan Shi', ',24,282,2724,', '四川省-乐山市-峨眉山市', '282', '02', '04', '01', '2724', '', null, null);
INSERT INTO t_sys_area VALUES ('2725', '511301', '511301', '511301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',24,283,2725,', '四川省-南充市-市辖区', '283', '02', '04', '01', '2725', '', null, null);
INSERT INTO t_sys_area VALUES ('2726', '511302', '511302', '511302', '顺庆区', 'Shunqing Xian', 'Shunqing Xian', ',24,283,2726,', '四川省-南充市-顺庆区', '283', '02', '04', '01', '2726', '', null, null);
INSERT INTO t_sys_area VALUES ('2727', '511303', '511303', '511303', '高坪区', 'Gaoping Qu', 'Gaoping Qu', ',24,283,2727,', '四川省-南充市-高坪区', '283', '02', '04', '01', '2727', '', null, null);
INSERT INTO t_sys_area VALUES ('2728', '511304', '511304', '511304', '嘉陵区', 'Jialing Qu', 'Jialing Qu', ',24,283,2728,', '四川省-南充市-嘉陵区', '283', '02', '04', '01', '2728', '', null, null);
INSERT INTO t_sys_area VALUES ('2729', '511321', '511321', '511321', '南部县', 'Nanbu Xian', 'Nanbu Xian', ',24,283,2729,', '四川省-南充市-南部县', '283', '02', '04', '01', '2729', '', null, null);
INSERT INTO t_sys_area VALUES ('2730', '511322', '511322', '511322', '营山县', 'Yingshan Xian', 'Yingshan Xian', ',24,283,2730,', '四川省-南充市-营山县', '283', '02', '04', '01', '2730', '', null, null);
INSERT INTO t_sys_area VALUES ('2731', '511323', '511323', '511323', '蓬安县', 'Peng,an Xian', 'Peng,an Xian', ',24,283,2731,', '四川省-南充市-蓬安县', '283', '02', '04', '01', '2731', '', null, null);
INSERT INTO t_sys_area VALUES ('2732', '511324', '511324', '511324', '仪陇县', 'Yilong Xian', 'Yilong Xian', ',24,283,2732,', '四川省-南充市-仪陇县', '283', '02', '04', '01', '2732', '', null, null);
INSERT INTO t_sys_area VALUES ('2733', '511325', '511325', '511325', '西充县', 'Xichong Xian', 'Xichong Xian', ',24,283,2733,', '四川省-南充市-西充县', '283', '02', '04', '01', '2733', '', null, null);
INSERT INTO t_sys_area VALUES ('2734', '511381', '511381', '511381', '阆中市', 'Langzhong Shi', 'Langzhong Shi', ',24,283,2734,', '四川省-南充市-阆中市', '283', '02', '04', '01', '2734', '', null, null);
INSERT INTO t_sys_area VALUES ('2735', '511401', '511401', '511401', '市辖区', '1', '1', ',24,284,2735,', '四川省-眉山市-市辖区', '284', '02', '04', '01', '2735', '', null, null);
INSERT INTO t_sys_area VALUES ('2736', '511402', '511402', '511402', '东坡区', 'Dongpo Qu', 'Dongpo Qu', ',24,284,2736,', '四川省-眉山市-东坡区', '284', '02', '04', '01', '2736', '', null, null);
INSERT INTO t_sys_area VALUES ('2737', '511421', '511421', '511421', '仁寿县', 'Renshou Xian', 'Renshou Xian', ',24,284,2737,', '四川省-眉山市-仁寿县', '284', '02', '04', '01', '2737', '', null, null);
INSERT INTO t_sys_area VALUES ('2738', '511422', '511422', '511422', '彭山县', 'Pengshan Xian', 'Pengshan Xian', ',24,284,2738,', '四川省-眉山市-彭山县', '284', '02', '04', '01', '2738', '', null, null);
INSERT INTO t_sys_area VALUES ('2739', '511423', '511423', '511423', '洪雅县', 'Hongya Xian', 'Hongya Xian', ',24,284,2739,', '四川省-眉山市-洪雅县', '284', '02', '04', '01', '2739', '', null, null);
INSERT INTO t_sys_area VALUES ('2740', '511424', '511424', '511424', '丹棱县', 'Danling Xian', 'Danling Xian', ',24,284,2740,', '四川省-眉山市-丹棱县', '284', '02', '04', '01', '2740', '', null, null);
INSERT INTO t_sys_area VALUES ('2741', '511425', '511425', '511425', '青神县', 'Qingshen Xian', 'Qingshen Xian', ',24,284,2741,', '四川省-眉山市-青神县', '284', '02', '04', '01', '2741', '', null, null);
INSERT INTO t_sys_area VALUES ('2742', '511501', '511501', '511501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',24,285,2742,', '四川省-宜宾市-市辖区', '285', '02', '04', '01', '2742', '', null, null);
INSERT INTO t_sys_area VALUES ('2743', '511502', '511502', '511502', '翠屏区', 'Cuiping Qu', 'Cuiping Qu', ',24,285,2743,', '四川省-宜宾市-翠屏区', '285', '02', '04', '01', '2743', '', null, null);
INSERT INTO t_sys_area VALUES ('2744', '511521', '511521', '511521', '宜宾县', 'Yibin Xian', 'Yibin Xian', ',24,285,2744,', '四川省-宜宾市-宜宾县', '285', '02', '04', '01', '2744', '', null, null);
INSERT INTO t_sys_area VALUES ('2745', '511522', '511522', '511522', '南溪县', 'Nanxi Xian', 'Nanxi Xian', ',24,285,2745,', '四川省-宜宾市-南溪县', '285', '02', '04', '01', '2745', '', null, null);
INSERT INTO t_sys_area VALUES ('2746', '511523', '511523', '511523', '江安县', 'Jiang,an Xian', 'Jiang,an Xian', ',24,285,2746,', '四川省-宜宾市-江安县', '285', '02', '04', '01', '2746', '', null, null);
INSERT INTO t_sys_area VALUES ('2747', '511524', '511524', '511524', '长宁县', 'Changning Xian', 'Changning Xian', ',24,285,2747,', '四川省-宜宾市-长宁县', '285', '02', '04', '01', '2747', '', null, null);
INSERT INTO t_sys_area VALUES ('2748', '511525', '511525', '511525', '高县', 'Gao Xian', 'Gao Xian', ',24,285,2748,', '四川省-宜宾市-高县', '285', '02', '04', '01', '2748', '', null, null);
INSERT INTO t_sys_area VALUES ('2749', '511526', '511526', '511526', '珙县', 'Gong Xian', 'Gong Xian', ',24,285,2749,', '四川省-宜宾市-珙县', '285', '02', '04', '01', '2749', '', null, null);
INSERT INTO t_sys_area VALUES ('2750', '511527', '511527', '511527', '筠连县', 'Junlian Xian', 'Junlian Xian', ',24,285,2750,', '四川省-宜宾市-筠连县', '285', '02', '04', '01', '2750', '', null, null);
INSERT INTO t_sys_area VALUES ('2751', '511528', '511528', '511528', '兴文县', 'Xingwen Xian', 'Xingwen Xian', ',24,285,2751,', '四川省-宜宾市-兴文县', '285', '02', '04', '01', '2751', '', null, null);
INSERT INTO t_sys_area VALUES ('2752', '511529', '511529', '511529', '屏山县', 'Pingshan Xian', 'Pingshan Xian', ',24,285,2752,', '四川省-宜宾市-屏山县', '285', '02', '04', '01', '2752', '', null, null);
INSERT INTO t_sys_area VALUES ('2753', '511601', '511601', '511601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',24,286,2753,', '四川省-广安市-市辖区', '286', '02', '04', '01', '2753', '', null, null);
INSERT INTO t_sys_area VALUES ('2754', '511602', '511602', '511602', '广安区', 'Guang,an Qu', 'Guang,an Qu', ',24,286,2754,', '四川省-广安市-广安区', '286', '02', '04', '01', '2754', '', null, null);
INSERT INTO t_sys_area VALUES ('2755', '511621', '511621', '511621', '岳池县', 'Yuechi Xian', 'Yuechi Xian', ',24,286,2755,', '四川省-广安市-岳池县', '286', '02', '04', '01', '2755', '', null, null);
INSERT INTO t_sys_area VALUES ('2756', '511622', '511622', '511622', '武胜县', 'Wusheng Xian', 'Wusheng Xian', ',24,286,2756,', '四川省-广安市-武胜县', '286', '02', '04', '01', '2756', '', null, null);
INSERT INTO t_sys_area VALUES ('2757', '511623', '511623', '511623', '邻水县', 'Linshui Xian', 'Linshui Xian', ',24,286,2757,', '四川省-广安市-邻水县', '286', '02', '04', '01', '2757', '', null, null);
INSERT INTO t_sys_area VALUES ('2759', '511701', '511701', '511701', '市辖区', '1', '1', ',24,287,2759,', '四川省-达州市-市辖区', '287', '02', '04', '01', '2759', '', null, null);
INSERT INTO t_sys_area VALUES ('2760', '511702', '511702', '511702', '通川区', 'Tongchuan Qu', 'Tongchuan Qu', ',24,287,2760,', '四川省-达州市-通川区', '287', '02', '04', '01', '2760', '', null, null);
INSERT INTO t_sys_area VALUES ('2761', '511721', '511721', '511721', '达县', 'Da Xian', 'Da Xian', ',24,287,2761,', '四川省-达州市-达县', '287', '02', '04', '01', '2761', '', null, null);
INSERT INTO t_sys_area VALUES ('2762', '511722', '511722', '511722', '宣汉县', 'Xuanhan Xian', 'Xuanhan Xian', ',24,287,2762,', '四川省-达州市-宣汉县', '287', '02', '04', '01', '2762', '', null, null);
INSERT INTO t_sys_area VALUES ('2763', '511723', '511723', '511723', '开江县', 'Kaijiang Xian', 'Kaijiang Xian', ',24,287,2763,', '四川省-达州市-开江县', '287', '02', '04', '01', '2763', '', null, null);
INSERT INTO t_sys_area VALUES ('2764', '511724', '511724', '511724', '大竹县', 'Dazhu Xian', 'Dazhu Xian', ',24,287,2764,', '四川省-达州市-大竹县', '287', '02', '04', '01', '2764', '', null, null);
INSERT INTO t_sys_area VALUES ('2765', '511725', '511725', '511725', '渠县', 'Qu Xian', 'Qu Xian', ',24,287,2765,', '四川省-达州市-渠县', '287', '02', '04', '01', '2765', '', null, null);
INSERT INTO t_sys_area VALUES ('2766', '511781', '511781', '511781', '万源市', 'Wanyuan Shi', 'Wanyuan Shi', ',24,287,2766,', '四川省-达州市-万源市', '287', '02', '04', '01', '2766', '', null, null);
INSERT INTO t_sys_area VALUES ('2767', '511801', '511801', '511801', '市辖区', '1', '1', ',24,288,2767,', '四川省-雅安市-市辖区', '288', '02', '04', '01', '2767', '', null, null);
INSERT INTO t_sys_area VALUES ('2768', '511802', '511802', '511802', '雨城区', 'Yucheg Qu', 'Yucheg Qu', ',24,288,2768,', '四川省-雅安市-雨城区', '288', '02', '04', '01', '2768', '', null, null);
INSERT INTO t_sys_area VALUES ('2769', '511821', '511821', '511821', '名山县', 'Mingshan Xian', 'Mingshan Xian', ',24,288,2769,', '四川省-雅安市-名山县', '288', '02', '04', '01', '2769', '', null, null);
INSERT INTO t_sys_area VALUES ('2770', '511822', '511822', '511822', '荥经县', 'Yingjing Xian', 'Yingjing Xian', ',24,288,2770,', '四川省-雅安市-荥经县', '288', '02', '04', '01', '2770', '', null, null);
INSERT INTO t_sys_area VALUES ('2771', '511823', '511823', '511823', '汉源县', 'Hanyuan Xian', 'Hanyuan Xian', ',24,288,2771,', '四川省-雅安市-汉源县', '288', '02', '04', '01', '2771', '', null, null);
INSERT INTO t_sys_area VALUES ('2772', '511824', '511824', '511824', '石棉县', 'Shimian Xian', 'Shimian Xian', ',24,288,2772,', '四川省-雅安市-石棉县', '288', '02', '04', '01', '2772', '', null, null);
INSERT INTO t_sys_area VALUES ('2773', '511825', '511825', '511825', '天全县', 'Tianquan Xian', 'Tianquan Xian', ',24,288,2773,', '四川省-雅安市-天全县', '288', '02', '04', '01', '2773', '', null, null);
INSERT INTO t_sys_area VALUES ('2774', '511826', '511826', '511826', '芦山县', 'Lushan Xian', 'Lushan Xian', ',24,288,2774,', '四川省-雅安市-芦山县', '288', '02', '04', '01', '2774', '', null, null);
INSERT INTO t_sys_area VALUES ('2775', '511827', '511827', '511827', '宝兴县', 'Baoxing Xian', 'Baoxing Xian', ',24,288,2775,', '四川省-雅安市-宝兴县', '288', '02', '04', '01', '2775', '', null, null);
INSERT INTO t_sys_area VALUES ('2776', '511901', '511901', '511901', '市辖区', '1', '1', ',24,289,2776,', '四川省-巴中市-市辖区', '289', '02', '04', '01', '2776', '', null, null);
INSERT INTO t_sys_area VALUES ('2777', '511902', '511902', '511902', '巴州区', 'Bazhou Qu', 'Bazhou Qu', ',24,289,2777,', '四川省-巴中市-巴州区', '289', '02', '04', '01', '2777', '', null, null);
INSERT INTO t_sys_area VALUES ('2778', '511921', '511921', '511921', '通江县', 'Tongjiang Xian', 'Tongjiang Xian', ',24,289,2778,', '四川省-巴中市-通江县', '289', '02', '04', '01', '2778', '', null, null);
INSERT INTO t_sys_area VALUES ('2779', '511922', '511922', '511922', '南江县', 'Nanjiang Xian', 'Nanjiang Xian', ',24,289,2779,', '四川省-巴中市-南江县', '289', '02', '04', '01', '2779', '', null, null);
INSERT INTO t_sys_area VALUES ('2780', '511923', '511923', '511923', '平昌县', 'Pingchang Xian', 'Pingchang Xian', ',24,289,2780,', '四川省-巴中市-平昌县', '289', '02', '04', '01', '2780', '', null, null);
INSERT INTO t_sys_area VALUES ('2781', '512001', '512001', '512001', '市辖区', '1', '1', ',24,290,2781,', '四川省-资阳市-市辖区', '290', '02', '04', '01', '2781', '', null, null);
INSERT INTO t_sys_area VALUES ('2782', '512002', '512002', '512002', '雁江区', 'Yanjiang Qu', 'Yanjiang Qu', ',24,290,2782,', '四川省-资阳市-雁江区', '290', '02', '04', '01', '2782', '', null, null);
INSERT INTO t_sys_area VALUES ('2783', '512021', '512021', '512021', '安岳县', 'Anyue Xian', 'Anyue Xian', ',24,290,2783,', '四川省-资阳市-安岳县', '290', '02', '04', '01', '2783', '', null, null);
INSERT INTO t_sys_area VALUES ('2784', '512022', '512022', '512022', '乐至县', 'Lezhi Xian', 'Lezhi Xian', ',24,290,2784,', '四川省-资阳市-乐至县', '290', '02', '04', '01', '2784', '', null, null);
INSERT INTO t_sys_area VALUES ('2785', '512081', '512081', '512081', '简阳市', 'Jianyang Shi', 'Jianyang Shi', ',24,290,2785,', '四川省-资阳市-简阳市', '290', '02', '04', '01', '2785', '', null, null);
INSERT INTO t_sys_area VALUES ('2786', '513221', '513221', '513221', '汶川县', 'Wenchuan Xian', 'Wenchuan Xian', ',24,291,2786,', '四川省-阿坝藏族羌族自治州-汶川县', '291', '02', '04', '01', '2786', '', null, null);
INSERT INTO t_sys_area VALUES ('2787', '513222', '513222', '513222', '理县', 'Li Xian', 'Li Xian', ',24,291,2787,', '四川省-阿坝藏族羌族自治州-理县', '291', '02', '04', '01', '2787', '', null, null);
INSERT INTO t_sys_area VALUES ('2788', '513223', '513223', '513223', '茂县', 'Mao Xian', 'Mao Xian', ',24,291,2788,', '四川省-阿坝藏族羌族自治州-茂县', '291', '02', '04', '01', '2788', '', null, null);
INSERT INTO t_sys_area VALUES ('2789', '513224', '513224', '513224', '松潘县', 'Songpan Xian', 'Songpan Xian', ',24,291,2789,', '四川省-阿坝藏族羌族自治州-松潘县', '291', '02', '04', '01', '2789', '', null, null);
INSERT INTO t_sys_area VALUES ('2790', '513225', '513225', '513225', '九寨沟县', 'Jiuzhaigou Xian', 'Jiuzhaigou Xian', ',24,291,2790,', '四川省-阿坝藏族羌族自治州-九寨沟县', '291', '02', '04', '01', '2790', '', null, null);
INSERT INTO t_sys_area VALUES ('2791', '513226', '513226', '513226', '金川县', 'Jinchuan Xian', 'Jinchuan Xian', ',24,291,2791,', '四川省-阿坝藏族羌族自治州-金川县', '291', '02', '04', '01', '2791', '', null, null);
INSERT INTO t_sys_area VALUES ('2792', '513227', '513227', '513227', '小金县', 'Xiaojin Xian', 'Xiaojin Xian', ',24,291,2792,', '四川省-阿坝藏族羌族自治州-小金县', '291', '02', '04', '01', '2792', '', null, null);
INSERT INTO t_sys_area VALUES ('2793', '513228', '513228', '513228', '黑水县', 'Heishui Xian', 'Heishui Xian', ',24,291,2793,', '四川省-阿坝藏族羌族自治州-黑水县', '291', '02', '04', '01', '2793', '', null, null);
INSERT INTO t_sys_area VALUES ('2794', '513229', '513229', '513229', '马尔康县', 'Barkam Xian', 'Barkam Xian', ',24,291,2794,', '四川省-阿坝藏族羌族自治州-马尔康县', '291', '02', '04', '01', '2794', '', null, null);
INSERT INTO t_sys_area VALUES ('2795', '513230', '513230', '513230', '壤塘县', 'Zamtang Xian', 'Zamtang Xian', ',24,291,2795,', '四川省-阿坝藏族羌族自治州-壤塘县', '291', '02', '04', '01', '2795', '', null, null);
INSERT INTO t_sys_area VALUES ('2796', '513231', '513231', '513231', '阿坝县', 'Aba(Ngawa) Xian', 'Aba(Ngawa) Xian', ',24,291,2796,', '四川省-阿坝藏族羌族自治州-阿坝县', '291', '02', '04', '01', '2796', '', null, null);
INSERT INTO t_sys_area VALUES ('2797', '513232', '513232', '513232', '若尔盖县', 'ZoigeXian', 'ZoigeXian', ',24,291,2797,', '四川省-阿坝藏族羌族自治州-若尔盖县', '291', '02', '04', '01', '2797', '', null, null);
INSERT INTO t_sys_area VALUES ('2798', '513233', '513233', '513233', '红原县', 'Hongyuan Xian', 'Hongyuan Xian', ',24,291,2798,', '四川省-阿坝藏族羌族自治州-红原县', '291', '02', '04', '01', '2798', '', null, null);
INSERT INTO t_sys_area VALUES ('2799', '513321', '513321', '513321', '康定县', 'Kangding(Dardo) Xian', 'Kangding(Dardo) Xian', ',24,292,2799,', '四川省-甘孜藏族自治州-康定县', '292', '02', '04', '01', '2799', '', null, null);
INSERT INTO t_sys_area VALUES ('2800', '513322', '513322', '513322', '泸定县', 'Luding(Jagsamka) Xian', 'Luding(Jagsamka) Xian', ',24,292,2800,', '四川省-甘孜藏族自治州-泸定县', '292', '02', '04', '01', '2800', '', null, null);
INSERT INTO t_sys_area VALUES ('2801', '513323', '513323', '513323', '丹巴县', 'Danba(Rongzhag) Xian', 'Danba(Rongzhag) Xian', ',24,292,2801,', '四川省-甘孜藏族自治州-丹巴县', '292', '02', '04', '01', '2801', '', null, null);
INSERT INTO t_sys_area VALUES ('2802', '513324', '513324', '513324', '九龙县', 'Jiulong(Gyaisi) Xian', 'Jiulong(Gyaisi) Xian', ',24,292,2802,', '四川省-甘孜藏族自治州-九龙县', '292', '02', '04', '01', '2802', '', null, null);
INSERT INTO t_sys_area VALUES ('2803', '513325', '513325', '513325', '雅江县', 'Yajiang(Nyagquka) Xian', 'Yajiang(Nyagquka) Xian', ',24,292,2803,', '四川省-甘孜藏族自治州-雅江县', '292', '02', '04', '01', '2803', '', null, null);
INSERT INTO t_sys_area VALUES ('2804', '513326', '513326', '513326', '道孚县', 'Dawu Xian', 'Dawu Xian', ',24,292,2804,', '四川省-甘孜藏族自治州-道孚县', '292', '02', '04', '01', '2804', '', null, null);
INSERT INTO t_sys_area VALUES ('2805', '513327', '513327', '513327', '炉霍县', 'Luhuo(Zhaggo) Xian', 'Luhuo(Zhaggo) Xian', ',24,292,2805,', '四川省-甘孜藏族自治州-炉霍县', '292', '02', '04', '01', '2805', '', null, null);
INSERT INTO t_sys_area VALUES ('2806', '513328', '513328', '513328', '甘孜县', 'Garze Xian', 'Garze Xian', ',24,292,2806,', '四川省-甘孜藏族自治州-甘孜县', '292', '02', '04', '01', '2806', '', null, null);
INSERT INTO t_sys_area VALUES ('2807', '513329', '513329', '513329', '新龙县', 'Xinlong(Nyagrong) Xian', 'Xinlong(Nyagrong) Xian', ',24,292,2807,', '四川省-甘孜藏族自治州-新龙县', '292', '02', '04', '01', '2807', '', null, null);
INSERT INTO t_sys_area VALUES ('2808', '513330', '513330', '513330', '德格县', 'DegeXian', 'DegeXian', ',24,292,2808,', '四川省-甘孜藏族自治州-德格县', '292', '02', '04', '01', '2808', '', null, null);
INSERT INTO t_sys_area VALUES ('2809', '513331', '513331', '513331', '白玉县', 'Baiyu Xian', 'Baiyu Xian', ',24,292,2809,', '四川省-甘孜藏族自治州-白玉县', '292', '02', '04', '01', '2809', '', null, null);
INSERT INTO t_sys_area VALUES ('2810', '513332', '513332', '513332', '石渠县', 'Serxv Xian', 'Serxv Xian', ',24,292,2810,', '四川省-甘孜藏族自治州-石渠县', '292', '02', '04', '01', '2810', '', null, null);
INSERT INTO t_sys_area VALUES ('2811', '513333', '513333', '513333', '色达县', 'Sertar Xian', 'Sertar Xian', ',24,292,2811,', '四川省-甘孜藏族自治州-色达县', '292', '02', '04', '01', '2811', '', null, null);
INSERT INTO t_sys_area VALUES ('2812', '513334', '513334', '513334', '理塘县', 'Litang Xian', 'Litang Xian', ',24,292,2812,', '四川省-甘孜藏族自治州-理塘县', '292', '02', '04', '01', '2812', '', null, null);
INSERT INTO t_sys_area VALUES ('2813', '513335', '513335', '513335', '巴塘县', 'Batang Xian', 'Batang Xian', ',24,292,2813,', '四川省-甘孜藏族自治州-巴塘县', '292', '02', '04', '01', '2813', '', null, null);
INSERT INTO t_sys_area VALUES ('2814', '513336', '513336', '513336', '乡城县', 'Xiangcheng(Qagcheng) Xian', 'Xiangcheng(Qagcheng) Xian', ',24,292,2814,', '四川省-甘孜藏族自治州-乡城县', '292', '02', '04', '01', '2814', '', null, null);
INSERT INTO t_sys_area VALUES ('2815', '513337', '513337', '513337', '稻城县', 'Daocheng(Dabba) Xian', 'Daocheng(Dabba) Xian', ',24,292,2815,', '四川省-甘孜藏族自治州-稻城县', '292', '02', '04', '01', '2815', '', null, null);
INSERT INTO t_sys_area VALUES ('2816', '513338', '513338', '513338', '得荣县', 'Derong Xian', 'Derong Xian', ',24,292,2816,', '四川省-甘孜藏族自治州-得荣县', '292', '02', '04', '01', '2816', '', null, null);
INSERT INTO t_sys_area VALUES ('2817', '513401', '513401', '513401', '西昌市', 'Xichang Shi', 'Xichang Shi', ',24,293,2817,', '四川省-凉山彝族自治州-西昌市', '293', '02', '04', '01', '2817', '', null, null);
INSERT INTO t_sys_area VALUES ('2818', '513422', '513422', '513422', '木里藏族自治县', 'Muli Zangzu Zizhixian', 'Muli Zangzu Zizhixian', ',24,293,2818,', '四川省-凉山彝族自治州-木里藏族自治县', '293', '02', '04', '01', '2818', '', null, null);
INSERT INTO t_sys_area VALUES ('2819', '513423', '513423', '513423', '盐源县', 'Yanyuan Xian', 'Yanyuan Xian', ',24,293,2819,', '四川省-凉山彝族自治州-盐源县', '293', '02', '04', '01', '2819', '', null, null);
INSERT INTO t_sys_area VALUES ('2820', '513424', '513424', '513424', '德昌县', 'Dechang Xian', 'Dechang Xian', ',24,293,2820,', '四川省-凉山彝族自治州-德昌县', '293', '02', '04', '01', '2820', '', null, null);
INSERT INTO t_sys_area VALUES ('2821', '513425', '513425', '513425', '会理县', 'Huili Xian', 'Huili Xian', ',24,293,2821,', '四川省-凉山彝族自治州-会理县', '293', '02', '04', '01', '2821', '', null, null);
INSERT INTO t_sys_area VALUES ('2822', '513426', '513426', '513426', '会东县', 'Huidong Xian', 'Huidong Xian', ',24,293,2822,', '四川省-凉山彝族自治州-会东县', '293', '02', '04', '01', '2822', '', null, null);
INSERT INTO t_sys_area VALUES ('2823', '513427', '513427', '513427', '宁南县', 'Ningnan Xian', 'Ningnan Xian', ',24,293,2823,', '四川省-凉山彝族自治州-宁南县', '293', '02', '04', '01', '2823', '', null, null);
INSERT INTO t_sys_area VALUES ('2824', '513428', '513428', '513428', '普格县', 'Puge Xian', 'Puge Xian', ',24,293,2824,', '四川省-凉山彝族自治州-普格县', '293', '02', '04', '01', '2824', '', null, null);
INSERT INTO t_sys_area VALUES ('2825', '513429', '513429', '513429', '布拖县', 'Butuo Xian', 'Butuo Xian', ',24,293,2825,', '四川省-凉山彝族自治州-布拖县', '293', '02', '04', '01', '2825', '', null, null);
INSERT INTO t_sys_area VALUES ('2826', '513430', '513430', '513430', '金阳县', 'Jinyang Xian', 'Jinyang Xian', ',24,293,2826,', '四川省-凉山彝族自治州-金阳县', '293', '02', '04', '01', '2826', '', null, null);
INSERT INTO t_sys_area VALUES ('2827', '513431', '513431', '513431', '昭觉县', 'Zhaojue Xian', 'Zhaojue Xian', ',24,293,2827,', '四川省-凉山彝族自治州-昭觉县', '293', '02', '04', '01', '2827', '', null, null);
INSERT INTO t_sys_area VALUES ('2828', '513432', '513432', '513432', '喜德县', 'Xide Xian', 'Xide Xian', ',24,293,2828,', '四川省-凉山彝族自治州-喜德县', '293', '02', '04', '01', '2828', '', null, null);
INSERT INTO t_sys_area VALUES ('2829', '513433', '513433', '513433', '冕宁县', 'Mianning Xian', 'Mianning Xian', ',24,293,2829,', '四川省-凉山彝族自治州-冕宁县', '293', '02', '04', '01', '2829', '', null, null);
INSERT INTO t_sys_area VALUES ('2830', '513434', '513434', '513434', '越西县', 'Yuexi Xian', 'Yuexi Xian', ',24,293,2830,', '四川省-凉山彝族自治州-越西县', '293', '02', '04', '01', '2830', '', null, null);
INSERT INTO t_sys_area VALUES ('2831', '513435', '513435', '513435', '甘洛县', 'Ganluo Xian', 'Ganluo Xian', ',24,293,2831,', '四川省-凉山彝族自治州-甘洛县', '293', '02', '04', '01', '2831', '', null, null);
INSERT INTO t_sys_area VALUES ('2832', '513436', '513436', '513436', '美姑县', 'Meigu Xian', 'Meigu Xian', ',24,293,2832,', '四川省-凉山彝族自治州-美姑县', '293', '02', '04', '01', '2832', '', null, null);
INSERT INTO t_sys_area VALUES ('2833', '513437', '513437', '513437', '雷波县', 'Leibo Xian', 'Leibo Xian', ',24,293,2833,', '四川省-凉山彝族自治州-雷波县', '293', '02', '04', '01', '2833', '', null, null);
INSERT INTO t_sys_area VALUES ('2834', '520101', '520101', '520101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',25,294,2834,', '贵州省-贵阳市-市辖区', '294', '02', '04', '01', '2834', '', null, null);
INSERT INTO t_sys_area VALUES ('2835', '520102', '520102', '520102', '南明区', 'Nanming Qu', 'Nanming Qu', ',25,294,2835,', '贵州省-贵阳市-南明区', '294', '02', '04', '01', '2835', '', null, null);
INSERT INTO t_sys_area VALUES ('2836', '520103', '520103', '520103', '云岩区', 'Yunyan Qu', 'Yunyan Qu', ',25,294,2836,', '贵州省-贵阳市-云岩区', '294', '02', '04', '01', '2836', '', null, null);
INSERT INTO t_sys_area VALUES ('2837', '520111', '520111', '520111', '花溪区', 'Huaxi Qu', 'Huaxi Qu', ',25,294,2837,', '贵州省-贵阳市-花溪区', '294', '02', '04', '01', '2837', '', null, null);
INSERT INTO t_sys_area VALUES ('2838', '520112', '520112', '520112', '乌当区', 'Wudang Qu', 'Wudang Qu', ',25,294,2838,', '贵州省-贵阳市-乌当区', '294', '02', '04', '01', '2838', '', null, null);
INSERT INTO t_sys_area VALUES ('2839', '520113', '520113', '520113', '白云区', 'Baiyun Qu', 'Baiyun Qu', ',25,294,2839,', '贵州省-贵阳市-白云区', '294', '02', '04', '01', '2839', '', null, null);
INSERT INTO t_sys_area VALUES ('2840', '520114', '520114', '520114', '小河区', 'Xiaohe Qu', 'Xiaohe Qu', ',25,294,2840,', '贵州省-贵阳市-小河区', '294', '02', '04', '01', '2840', '', null, null);
INSERT INTO t_sys_area VALUES ('2841', '520121', '520121', '520121', '开阳县', 'Kaiyang Xian', 'Kaiyang Xian', ',25,294,2841,', '贵州省-贵阳市-开阳县', '294', '02', '04', '01', '2841', '', null, null);
INSERT INTO t_sys_area VALUES ('2842', '520122', '520122', '520122', '息烽县', 'Xifeng Xian', 'Xifeng Xian', ',25,294,2842,', '贵州省-贵阳市-息烽县', '294', '02', '04', '01', '2842', '', null, null);
INSERT INTO t_sys_area VALUES ('2843', '520123', '520123', '520123', '修文县', 'Xiuwen Xian', 'Xiuwen Xian', ',25,294,2843,', '贵州省-贵阳市-修文县', '294', '02', '04', '01', '2843', '', null, null);
INSERT INTO t_sys_area VALUES ('2844', '520181', '520181', '520181', '清镇市', 'Qingzhen Shi', 'Qingzhen Shi', ',25,294,2844,', '贵州省-贵阳市-清镇市', '294', '02', '04', '01', '2844', '', null, null);
INSERT INTO t_sys_area VALUES ('2845', '520201', '520201', '520201', '钟山区', 'Zhongshan Qu', 'Zhongshan Qu', ',25,295,2845,', '贵州省-六盘水市-钟山区', '295', '02', '04', '01', '2845', '', null, null);
INSERT INTO t_sys_area VALUES ('2846', '520203', '520203', '520203', '六枝特区', 'Liuzhi Tequ', 'Liuzhi Tequ', ',25,295,2846,', '贵州省-六盘水市-六枝特区', '295', '02', '04', '01', '2846', '', null, null);
INSERT INTO t_sys_area VALUES ('2847', '520221', '520221', '520221', '水城县', 'Shuicheng Xian', 'Shuicheng Xian', ',25,295,2847,', '贵州省-六盘水市-水城县', '295', '02', '04', '01', '2847', '', null, null);
INSERT INTO t_sys_area VALUES ('2848', '520222', '520222', '520222', '盘县', 'Pan Xian', 'Pan Xian', ',25,295,2848,', '贵州省-六盘水市-盘县', '295', '02', '04', '01', '2848', '', null, null);
INSERT INTO t_sys_area VALUES ('2849', '520301', '520301', '520301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',25,296,2849,', '贵州省-遵义市-市辖区', '296', '02', '04', '01', '2849', '', null, null);
INSERT INTO t_sys_area VALUES ('2850', '520302', '520302', '520302', '红花岗区', 'Honghuagang Qu', 'Honghuagang Qu', ',25,296,2850,', '贵州省-遵义市-红花岗区', '296', '02', '04', '01', '2850', '', null, null);
INSERT INTO t_sys_area VALUES ('2851', '520303', '520303', '520303', '汇川区', 'Huichuan Qu', 'Huichuan Qu', ',25,296,2851,', '贵州省-遵义市-汇川区', '296', '02', '04', '01', '2851', '', null, null);
INSERT INTO t_sys_area VALUES ('2852', '520321', '520321', '520321', '遵义县', 'Zunyi Xian', 'Zunyi Xian', ',25,296,2852,', '贵州省-遵义市-遵义县', '296', '02', '04', '01', '2852', '', null, null);
INSERT INTO t_sys_area VALUES ('2853', '520322', '520322', '520322', '桐梓县', 'Tongzi Xian', 'Tongzi Xian', ',25,296,2853,', '贵州省-遵义市-桐梓县', '296', '02', '04', '01', '2853', '', null, null);
INSERT INTO t_sys_area VALUES ('2854', '520323', '520323', '520323', '绥阳县', 'Suiyang Xian', 'Suiyang Xian', ',25,296,2854,', '贵州省-遵义市-绥阳县', '296', '02', '04', '01', '2854', '', null, null);
INSERT INTO t_sys_area VALUES ('2855', '520324', '520324', '520324', '正安县', 'Zhengan Xan', 'Zhengan Xan', ',25,296,2855,', '贵州省-遵义市-正安县', '296', '02', '04', '01', '2855', '', null, null);
INSERT INTO t_sys_area VALUES ('2856', '520325', '520325', '520325', '道真仡佬族苗族自治县', 'Daozhen Gelaozu Miaozu Zizhixian', 'Daozhen Gelaozu Miaozu Zizhixian', ',25,296,2856,', '贵州省-遵义市-道真仡佬族苗族自治县', '296', '02', '04', '01', '2856', '', null, null);
INSERT INTO t_sys_area VALUES ('2857', '520326', '520326', '520326', '务川仡佬族苗族自治县', 'Wuchuan Gelaozu Miaozu Zizhixian', 'Wuchuan Gelaozu Miaozu Zizhixian', ',25,296,2857,', '贵州省-遵义市-务川仡佬族苗族自治县', '296', '02', '04', '01', '2857', '', null, null);
INSERT INTO t_sys_area VALUES ('2858', '520327', '520327', '520327', '凤冈县', 'Fenggang Xian', 'Fenggang Xian', ',25,296,2858,', '贵州省-遵义市-凤冈县', '296', '02', '04', '01', '2858', '', null, null);
INSERT INTO t_sys_area VALUES ('2859', '520328', '520328', '520328', '湄潭县', 'Meitan Xian', 'Meitan Xian', ',25,296,2859,', '贵州省-遵义市-湄潭县', '296', '02', '04', '01', '2859', '', null, null);
INSERT INTO t_sys_area VALUES ('2860', '520329', '520329', '520329', '余庆县', 'Yuqing Xian', 'Yuqing Xian', ',25,296,2860,', '贵州省-遵义市-余庆县', '296', '02', '04', '01', '2860', '', null, null);
INSERT INTO t_sys_area VALUES ('2861', '520330', '520330', '520330', '习水县', 'Xishui Xian', 'Xishui Xian', ',25,296,2861,', '贵州省-遵义市-习水县', '296', '02', '04', '01', '2861', '', null, null);
INSERT INTO t_sys_area VALUES ('2862', '520381', '520381', '520381', '赤水市', 'Chishui Shi', 'Chishui Shi', ',25,296,2862,', '贵州省-遵义市-赤水市', '296', '02', '04', '01', '2862', '', null, null);
INSERT INTO t_sys_area VALUES ('2863', '520382', '520382', '520382', '仁怀市', 'Renhuai Shi', 'Renhuai Shi', ',25,296,2863,', '贵州省-遵义市-仁怀市', '296', '02', '04', '01', '2863', '', null, null);
INSERT INTO t_sys_area VALUES ('2864', '520401', '520401', '520401', '市辖区', '1', '1', ',25,297,2864,', '贵州省-安顺市-市辖区', '297', '02', '04', '01', '2864', '', null, null);
INSERT INTO t_sys_area VALUES ('2865', '520402', '520402', '520402', '西秀区', 'Xixiu Qu', 'Xixiu Qu', ',25,297,2865,', '贵州省-安顺市-西秀区', '297', '02', '04', '01', '2865', '', null, null);
INSERT INTO t_sys_area VALUES ('2866', '520421', '520421', '520421', '平坝县', 'Pingba Xian', 'Pingba Xian', ',25,297,2866,', '贵州省-安顺市-平坝县', '297', '02', '04', '01', '2866', '', null, null);
INSERT INTO t_sys_area VALUES ('2867', '520422', '520422', '520422', '普定县', 'Puding Xian', 'Puding Xian', ',25,297,2867,', '贵州省-安顺市-普定县', '297', '02', '04', '01', '2867', '', null, null);
INSERT INTO t_sys_area VALUES ('2868', '520423', '520423', '520423', '镇宁布依族苗族自治县', 'Zhenning Buyeizu Miaozu Zizhixian', 'Zhenning Buyeizu Miaozu Zizhixian', ',25,297,2868,', '贵州省-安顺市-镇宁布依族苗族自治县', '297', '02', '04', '01', '2868', '', null, null);
INSERT INTO t_sys_area VALUES ('2869', '520424', '520424', '520424', '关岭布依族苗族自治县', 'Guanling Buyeizu Miaozu Zizhixian', 'Guanling Buyeizu Miaozu Zizhixian', ',25,297,2869,', '贵州省-安顺市-关岭布依族苗族自治县', '297', '02', '04', '01', '2869', '', null, null);
INSERT INTO t_sys_area VALUES ('2870', '520425', '520425', '520425', '紫云苗族布依族自治县', 'Ziyun Miaozu Buyeizu Zizhixian', 'Ziyun Miaozu Buyeizu Zizhixian', ',25,297,2870,', '贵州省-安顺市-紫云苗族布依族自治县', '297', '02', '04', '01', '2870', '', null, null);
INSERT INTO t_sys_area VALUES ('2871', '522201', '522201', '522201', '铜仁市', 'Tongren Shi', 'Tongren Shi', ',25,298,2871,', '贵州省-铜仁地区-铜仁市', '298', '02', '04', '01', '2871', '', null, null);
INSERT INTO t_sys_area VALUES ('2872', '522222', '522222', '522222', '江口县', 'Jiangkou Xian', 'Jiangkou Xian', ',25,298,2872,', '贵州省-铜仁地区-江口县', '298', '02', '04', '01', '2872', '', null, null);
INSERT INTO t_sys_area VALUES ('2873', '522223', '522223', '522223', '玉屏侗族自治县', 'Yuping Dongzu Zizhixian', 'Yuping Dongzu Zizhixian', ',25,298,2873,', '贵州省-铜仁地区-玉屏侗族自治县', '298', '02', '04', '01', '2873', '', null, null);
INSERT INTO t_sys_area VALUES ('2874', '522224', '522224', '522224', '石阡县', 'Shiqian Xian', 'Shiqian Xian', ',25,298,2874,', '贵州省-铜仁地区-石阡县', '298', '02', '04', '01', '2874', '', null, null);
INSERT INTO t_sys_area VALUES ('2875', '522225', '522225', '522225', '思南县', 'Sinan Xian', 'Sinan Xian', ',25,298,2875,', '贵州省-铜仁地区-思南县', '298', '02', '04', '01', '2875', '', null, null);
INSERT INTO t_sys_area VALUES ('2876', '522226', '522226', '522226', '印江土家族苗族自治县', 'Yinjiang Tujiazu Miaozu Zizhixian', 'Yinjiang Tujiazu Miaozu Zizhixian', ',25,298,2876,', '贵州省-铜仁地区-印江土家族苗族自治县', '298', '02', '04', '01', '2876', '', null, null);
INSERT INTO t_sys_area VALUES ('2877', '522227', '522227', '522227', '德江县', 'Dejiang Xian', 'Dejiang Xian', ',25,298,2877,', '贵州省-铜仁地区-德江县', '298', '02', '04', '01', '2877', '', null, null);
INSERT INTO t_sys_area VALUES ('2878', '522228', '522228', '522228', '沿河土家族自治县', 'Yanhe Tujiazu Zizhixian', 'Yanhe Tujiazu Zizhixian', ',25,298,2878,', '贵州省-铜仁地区-沿河土家族自治县', '298', '02', '04', '01', '2878', '', null, null);
INSERT INTO t_sys_area VALUES ('2879', '522229', '522229', '522229', '松桃苗族自治县', 'Songtao Miaozu Zizhixian', 'Songtao Miaozu Zizhixian', ',25,298,2879,', '贵州省-铜仁地区-松桃苗族自治县', '298', '02', '04', '01', '2879', '', null, null);
INSERT INTO t_sys_area VALUES ('2880', '522230', '522230', '522230', '万山特区', 'Wanshan Tequ', 'Wanshan Tequ', ',25,298,2880,', '贵州省-铜仁地区-万山特区', '298', '02', '04', '01', '2880', '', null, null);
INSERT INTO t_sys_area VALUES ('2881', '522301', '522301', '522301', '兴义市', 'Xingyi Shi', 'Xingyi Shi', ',25,299,2881,', '贵州省-黔西南布依族苗族自治州-兴义市', '299', '02', '04', '01', '2881', '', null, null);
INSERT INTO t_sys_area VALUES ('2882', '522322', '522322', '522322', '兴仁县', 'Xingren Xian', 'Xingren Xian', ',25,299,2882,', '贵州省-黔西南布依族苗族自治州-兴仁县', '299', '02', '04', '01', '2882', '', null, null);
INSERT INTO t_sys_area VALUES ('2883', '522323', '522323', '522323', '普安县', 'Pu,an Xian', 'Pu,an Xian', ',25,299,2883,', '贵州省-黔西南布依族苗族自治州-普安县', '299', '02', '04', '01', '2883', '', null, null);
INSERT INTO t_sys_area VALUES ('2884', '522324', '522324', '522324', '晴隆县', 'Qinglong Xian', 'Qinglong Xian', ',25,299,2884,', '贵州省-黔西南布依族苗族自治州-晴隆县', '299', '02', '04', '01', '2884', '', null, null);
INSERT INTO t_sys_area VALUES ('2885', '522325', '522325', '522325', '贞丰县', 'Zhenfeng Xian', 'Zhenfeng Xian', ',25,299,2885,', '贵州省-黔西南布依族苗族自治州-贞丰县', '299', '02', '04', '01', '2885', '', null, null);
INSERT INTO t_sys_area VALUES ('2886', '522326', '522326', '522326', '望谟县', 'Wangmo Xian', 'Wangmo Xian', ',25,299,2886,', '贵州省-黔西南布依族苗族自治州-望谟县', '299', '02', '04', '01', '2886', '', null, null);
INSERT INTO t_sys_area VALUES ('2887', '522327', '522327', '522327', '册亨县', 'Ceheng Xian', 'Ceheng Xian', ',25,299,2887,', '贵州省-黔西南布依族苗族自治州-册亨县', '299', '02', '04', '01', '2887', '', null, null);
INSERT INTO t_sys_area VALUES ('2888', '522328', '522328', '522328', '安龙县', 'Anlong Xian', 'Anlong Xian', ',25,299,2888,', '贵州省-黔西南布依族苗族自治州-安龙县', '299', '02', '04', '01', '2888', '', null, null);
INSERT INTO t_sys_area VALUES ('2889', '522401', '522401', '522401', '毕节市', 'Bijie Shi', 'Bijie Shi', ',25,300,2889,', '贵州省-毕节地区-毕节市', '300', '02', '04', '01', '2889', '', null, null);
INSERT INTO t_sys_area VALUES ('2890', '522422', '522422', '522422', '大方县', 'Dafang Xian', 'Dafang Xian', ',25,300,2890,', '贵州省-毕节地区-大方县', '300', '02', '04', '01', '2890', '', null, null);
INSERT INTO t_sys_area VALUES ('2891', '522423', '522423', '522423', '黔西县', 'Qianxi Xian', 'Qianxi Xian', ',25,300,2891,', '贵州省-毕节地区-黔西县', '300', '02', '04', '01', '2891', '', null, null);
INSERT INTO t_sys_area VALUES ('2892', '522424', '522424', '522424', '金沙县', 'Jinsha Xian', 'Jinsha Xian', ',25,300,2892,', '贵州省-毕节地区-金沙县', '300', '02', '04', '01', '2892', '', null, null);
INSERT INTO t_sys_area VALUES ('2893', '522425', '522425', '522425', '织金县', 'Zhijin Xian', 'Zhijin Xian', ',25,300,2893,', '贵州省-毕节地区-织金县', '300', '02', '04', '01', '2893', '', null, null);
INSERT INTO t_sys_area VALUES ('2894', '522426', '522426', '522426', '纳雍县', 'Nayong Xian', 'Nayong Xian', ',25,300,2894,', '贵州省-毕节地区-纳雍县', '300', '02', '04', '01', '2894', '', null, null);
INSERT INTO t_sys_area VALUES ('2895', '522427', '522427', '522427', '威宁彝族回族苗族自治县', 'Weining Yizu Huizu Miaozu Zizhixian', 'Weining Yizu Huizu Miaozu Zizhixian', ',25,300,2895,', '贵州省-毕节地区-威宁彝族回族苗族自治县', '300', '02', '04', '01', '2895', '', null, null);
INSERT INTO t_sys_area VALUES ('2896', '522428', '522428', '522428', '赫章县', 'Hezhang Xian', 'Hezhang Xian', ',25,300,2896,', '贵州省-毕节地区-赫章县', '300', '02', '04', '01', '2896', '', null, null);
INSERT INTO t_sys_area VALUES ('2897', '522601', '522601', '522601', '凯里市', 'Kaili Shi', 'Kaili Shi', ',25,301,2897,', '贵州省-黔东南苗族侗族自治州-凯里市', '301', '02', '04', '01', '2897', '', null, null);
INSERT INTO t_sys_area VALUES ('2898', '522622', '522622', '522622', '黄平县', 'Huangping Xian', 'Huangping Xian', ',25,301,2898,', '贵州省-黔东南苗族侗族自治州-黄平县', '301', '02', '04', '01', '2898', '', null, null);
INSERT INTO t_sys_area VALUES ('2899', '522623', '522623', '522623', '施秉县', 'Shibing Xian', 'Shibing Xian', ',25,301,2899,', '贵州省-黔东南苗族侗族自治州-施秉县', '301', '02', '04', '01', '2899', '', null, null);
INSERT INTO t_sys_area VALUES ('2900', '522624', '522624', '522624', '三穗县', 'Sansui Xian', 'Sansui Xian', ',25,301,2900,', '贵州省-黔东南苗族侗族自治州-三穗县', '301', '02', '04', '01', '2900', '', null, null);
INSERT INTO t_sys_area VALUES ('2901', '522625', '522625', '522625', '镇远县', 'Zhenyuan Xian', 'Zhenyuan Xian', ',25,301,2901,', '贵州省-黔东南苗族侗族自治州-镇远县', '301', '02', '04', '01', '2901', '', null, null);
INSERT INTO t_sys_area VALUES ('2902', '522626', '522626', '522626', '岑巩县', 'Cengong Xian', 'Cengong Xian', ',25,301,2902,', '贵州省-黔东南苗族侗族自治州-岑巩县', '301', '02', '04', '01', '2902', '', null, null);
INSERT INTO t_sys_area VALUES ('2903', '522627', '522627', '522627', '天柱县', 'Tianzhu Xian', 'Tianzhu Xian', ',25,301,2903,', '贵州省-黔东南苗族侗族自治州-天柱县', '301', '02', '04', '01', '2903', '', null, null);
INSERT INTO t_sys_area VALUES ('2904', '522628', '522628', '522628', '锦屏县', 'Jinping Xian', 'Jinping Xian', ',25,301,2904,', '贵州省-黔东南苗族侗族自治州-锦屏县', '301', '02', '04', '01', '2904', '', null, null);
INSERT INTO t_sys_area VALUES ('2905', '522629', '522629', '522629', '剑河县', 'Jianhe Xian', 'Jianhe Xian', ',25,301,2905,', '贵州省-黔东南苗族侗族自治州-剑河县', '301', '02', '04', '01', '2905', '', null, null);
INSERT INTO t_sys_area VALUES ('2906', '522630', '522630', '522630', '台江县', 'Taijiang Xian', 'Taijiang Xian', ',25,301,2906,', '贵州省-黔东南苗族侗族自治州-台江县', '301', '02', '04', '01', '2906', '', null, null);
INSERT INTO t_sys_area VALUES ('2907', '522631', '522631', '522631', '黎平县', 'Liping Xian', 'Liping Xian', ',25,301,2907,', '贵州省-黔东南苗族侗族自治州-黎平县', '301', '02', '04', '01', '2907', '', null, null);
INSERT INTO t_sys_area VALUES ('2908', '522632', '522632', '522632', '榕江县', 'Rongjiang Xian', 'Rongjiang Xian', ',25,301,2908,', '贵州省-黔东南苗族侗族自治州-榕江县', '301', '02', '04', '01', '2908', '', null, null);
INSERT INTO t_sys_area VALUES ('2909', '522633', '522633', '522633', '从江县', 'Congjiang Xian', 'Congjiang Xian', ',25,301,2909,', '贵州省-黔东南苗族侗族自治州-从江县', '301', '02', '04', '01', '2909', '', null, null);
INSERT INTO t_sys_area VALUES ('2910', '522634', '522634', '522634', '雷山县', 'Leishan Xian', 'Leishan Xian', ',25,301,2910,', '贵州省-黔东南苗族侗族自治州-雷山县', '301', '02', '04', '01', '2910', '', null, null);
INSERT INTO t_sys_area VALUES ('2911', '522635', '522635', '522635', '麻江县', 'Majiang Xian', 'Majiang Xian', ',25,301,2911,', '贵州省-黔东南苗族侗族自治州-麻江县', '301', '02', '04', '01', '2911', '', null, null);
INSERT INTO t_sys_area VALUES ('2912', '522636', '522636', '522636', '丹寨县', 'Danzhai Xian', 'Danzhai Xian', ',25,301,2912,', '贵州省-黔东南苗族侗族自治州-丹寨县', '301', '02', '04', '01', '2912', '', null, null);
INSERT INTO t_sys_area VALUES ('2913', '522701', '522701', '522701', '都匀市', 'Duyun Shi', 'Duyun Shi', ',25,302,2913,', '贵州省-黔南布依族苗族自治州-都匀市', '302', '02', '04', '01', '2913', '', null, null);
INSERT INTO t_sys_area VALUES ('2914', '522702', '522702', '522702', '福泉市', 'Fuquan Shi', 'Fuquan Shi', ',25,302,2914,', '贵州省-黔南布依族苗族自治州-福泉市', '302', '02', '04', '01', '2914', '', null, null);
INSERT INTO t_sys_area VALUES ('2915', '522722', '522722', '522722', '荔波县', 'Libo Xian', 'Libo Xian', ',25,302,2915,', '贵州省-黔南布依族苗族自治州-荔波县', '302', '02', '04', '01', '2915', '', null, null);
INSERT INTO t_sys_area VALUES ('2916', '522723', '522723', '522723', '贵定县', 'Guiding Xian', 'Guiding Xian', ',25,302,2916,', '贵州省-黔南布依族苗族自治州-贵定县', '302', '02', '04', '01', '2916', '', null, null);
INSERT INTO t_sys_area VALUES ('2917', '522725', '522725', '522725', '瓮安县', 'Weng,an Xian', 'Weng,an Xian', ',25,302,2917,', '贵州省-黔南布依族苗族自治州-瓮安县', '302', '02', '04', '01', '2917', '', null, null);
INSERT INTO t_sys_area VALUES ('2918', '522726', '522726', '522726', '独山县', 'Dushan Xian', 'Dushan Xian', ',25,302,2918,', '贵州省-黔南布依族苗族自治州-独山县', '302', '02', '04', '01', '2918', '', null, null);
INSERT INTO t_sys_area VALUES ('2919', '522727', '522727', '522727', '平塘县', 'Pingtang Xian', 'Pingtang Xian', ',25,302,2919,', '贵州省-黔南布依族苗族自治州-平塘县', '302', '02', '04', '01', '2919', '', null, null);
INSERT INTO t_sys_area VALUES ('2920', '522728', '522728', '522728', '罗甸县', 'Luodian Xian', 'Luodian Xian', ',25,302,2920,', '贵州省-黔南布依族苗族自治州-罗甸县', '302', '02', '04', '01', '2920', '', null, null);
INSERT INTO t_sys_area VALUES ('2921', '522729', '522729', '522729', '长顺县', 'Changshun Xian', 'Changshun Xian', ',25,302,2921,', '贵州省-黔南布依族苗族自治州-长顺县', '302', '02', '04', '01', '2921', '', null, null);
INSERT INTO t_sys_area VALUES ('2922', '522730', '522730', '522730', '龙里县', 'Longli Xian', 'Longli Xian', ',25,302,2922,', '贵州省-黔南布依族苗族自治州-龙里县', '302', '02', '04', '01', '2922', '', null, null);
INSERT INTO t_sys_area VALUES ('2923', '522731', '522731', '522731', '惠水县', 'Huishui Xian', 'Huishui Xian', ',25,302,2923,', '贵州省-黔南布依族苗族自治州-惠水县', '302', '02', '04', '01', '2923', '', null, null);
INSERT INTO t_sys_area VALUES ('2924', '522732', '522732', '522732', '三都水族自治县', 'Sandu Suizu Zizhixian', 'Sandu Suizu Zizhixian', ',25,302,2924,', '贵州省-黔南布依族苗族自治州-三都水族自治县', '302', '02', '04', '01', '2924', '', null, null);
INSERT INTO t_sys_area VALUES ('2925', '530101', '530101', '530101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',26,303,2925,', '云南省-昆明市-市辖区', '303', '02', '04', '01', '2925', '', null, null);
INSERT INTO t_sys_area VALUES ('2926', '530102', '530102', '530102', '五华区', 'Wuhua Qu', 'Wuhua Qu', ',26,303,2926,', '云南省-昆明市-五华区', '303', '02', '04', '01', '2926', '', null, null);
INSERT INTO t_sys_area VALUES ('2927', '530103', '530103', '530103', '盘龙区', 'Panlong Qu', 'Panlong Qu', ',26,303,2927,', '云南省-昆明市-盘龙区', '303', '02', '04', '01', '2927', '', null, null);
INSERT INTO t_sys_area VALUES ('2928', '530111', '530111', '530111', '官渡区', 'Guandu Qu', 'Guandu Qu', ',26,303,2928,', '云南省-昆明市-官渡区', '303', '02', '04', '01', '2928', '', null, null);
INSERT INTO t_sys_area VALUES ('2929', '530112', '530112', '530112', '西山区', 'Xishan Qu', 'Xishan Qu', ',26,303,2929,', '云南省-昆明市-西山区', '303', '02', '04', '01', '2929', '', null, null);
INSERT INTO t_sys_area VALUES ('2930', '530113', '530113', '530113', '东川区', 'Dongchuan Qu', 'Dongchuan Qu', ',26,303,2930,', '云南省-昆明市-东川区', '303', '02', '04', '01', '2930', '', null, null);
INSERT INTO t_sys_area VALUES ('2931', '530121', '530121', '530121', '呈贡县', 'Chenggong Xian', 'Chenggong Xian', ',26,303,2931,', '云南省-昆明市-呈贡县', '303', '02', '04', '01', '2931', '', null, null);
INSERT INTO t_sys_area VALUES ('2932', '530122', '530122', '530122', '晋宁县', 'Jinning Xian', 'Jinning Xian', ',26,303,2932,', '云南省-昆明市-晋宁县', '303', '02', '04', '01', '2932', '', null, null);
INSERT INTO t_sys_area VALUES ('2933', '530124', '530124', '530124', '富民县', 'Fumin Xian', 'Fumin Xian', ',26,303,2933,', '云南省-昆明市-富民县', '303', '02', '04', '01', '2933', '', null, null);
INSERT INTO t_sys_area VALUES ('2934', '530125', '530125', '530125', '宜良县', 'Yiliang Xian', 'Yiliang Xian', ',26,303,2934,', '云南省-昆明市-宜良县', '303', '02', '04', '01', '2934', '', null, null);
INSERT INTO t_sys_area VALUES ('2935', '530126', '530126', '530126', '石林彝族自治县', 'Shilin Yizu Zizhixian', 'Shilin Yizu Zizhixian', ',26,303,2935,', '云南省-昆明市-石林彝族自治县', '303', '02', '04', '01', '2935', '', null, null);
INSERT INTO t_sys_area VALUES ('2936', '530127', '530127', '530127', '嵩明县', 'Songming Xian', 'Songming Xian', ',26,303,2936,', '云南省-昆明市-嵩明县', '303', '02', '04', '01', '2936', '', null, null);
INSERT INTO t_sys_area VALUES ('2937', '530128', '530128', '530128', '禄劝彝族苗族自治县', 'Luchuan Yizu Miaozu Zizhixian', 'Luchuan Yizu Miaozu Zizhixian', ',26,303,2937,', '云南省-昆明市-禄劝彝族苗族自治县', '303', '02', '04', '01', '2937', '', null, null);
INSERT INTO t_sys_area VALUES ('2938', '530129', '530129', '530129', '寻甸回族彝族自治县', 'Xundian Huizu Yizu Zizhixian', 'Xundian Huizu Yizu Zizhixian', ',26,303,2938,', '云南省-昆明市-寻甸回族彝族自治县', '303', '02', '04', '01', '2938', '', null, null);
INSERT INTO t_sys_area VALUES ('2939', '530181', '530181', '530181', '安宁市', 'Anning Shi', 'Anning Shi', ',26,303,2939,', '云南省-昆明市-安宁市', '303', '02', '04', '01', '2939', '', null, null);
INSERT INTO t_sys_area VALUES ('2940', '530301', '530301', '530301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',26,304,2940,', '云南省-曲靖市-市辖区', '304', '02', '04', '01', '2940', '', null, null);
INSERT INTO t_sys_area VALUES ('2941', '530302', '530302', '530302', '麒麟区', 'Qilin Xian', 'Qilin Xian', ',26,304,2941,', '云南省-曲靖市-麒麟区', '304', '02', '04', '01', '2941', '', null, null);
INSERT INTO t_sys_area VALUES ('2942', '530321', '530321', '530321', '马龙县', 'Malong Xian', 'Malong Xian', ',26,304,2942,', '云南省-曲靖市-马龙县', '304', '02', '04', '01', '2942', '', null, null);
INSERT INTO t_sys_area VALUES ('2943', '530322', '530322', '530322', '陆良县', 'Luliang Xian', 'Luliang Xian', ',26,304,2943,', '云南省-曲靖市-陆良县', '304', '02', '04', '01', '2943', '', null, null);
INSERT INTO t_sys_area VALUES ('2944', '530323', '530323', '530323', '师宗县', 'Shizong Xian', 'Shizong Xian', ',26,304,2944,', '云南省-曲靖市-师宗县', '304', '02', '04', '01', '2944', '', null, null);
INSERT INTO t_sys_area VALUES ('2945', '530324', '530324', '530324', '罗平县', 'Luoping Xian', 'Luoping Xian', ',26,304,2945,', '云南省-曲靖市-罗平县', '304', '02', '04', '01', '2945', '', null, null);
INSERT INTO t_sys_area VALUES ('2946', '530325', '530325', '530325', '富源县', 'Fuyuan Xian', 'Fuyuan Xian', ',26,304,2946,', '云南省-曲靖市-富源县', '304', '02', '04', '01', '2946', '', null, null);
INSERT INTO t_sys_area VALUES ('2947', '530326', '530326', '530326', '会泽县', 'Huize Xian', 'Huize Xian', ',26,304,2947,', '云南省-曲靖市-会泽县', '304', '02', '04', '01', '2947', '', null, null);
INSERT INTO t_sys_area VALUES ('2948', '530328', '530328', '530328', '沾益县', 'Zhanyi Xian', 'Zhanyi Xian', ',26,304,2948,', '云南省-曲靖市-沾益县', '304', '02', '04', '01', '2948', '', null, null);
INSERT INTO t_sys_area VALUES ('2949', '530381', '530381', '530381', '宣威市', 'Xuanwei Shi', 'Xuanwei Shi', ',26,304,2949,', '云南省-曲靖市-宣威市', '304', '02', '04', '01', '2949', '', null, null);
INSERT INTO t_sys_area VALUES ('2950', '530401', '530401', '530401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',26,305,2950,', '云南省-玉溪市-市辖区', '305', '02', '04', '01', '2950', '', null, null);
INSERT INTO t_sys_area VALUES ('2951', '530402', '530402', '530402', '红塔区', 'Hongta Qu', 'Hongta Qu', ',26,305,2951,', '云南省-玉溪市-红塔区', '305', '02', '04', '01', '2951', '', null, null);
INSERT INTO t_sys_area VALUES ('2952', '530421', '530421', '530421', '江川县', 'Jiangchuan Xian', 'Jiangchuan Xian', ',26,305,2952,', '云南省-玉溪市-江川县', '305', '02', '04', '01', '2952', '', null, null);
INSERT INTO t_sys_area VALUES ('2953', '530422', '530422', '530422', '澄江县', 'Chengjiang Xian', 'Chengjiang Xian', ',26,305,2953,', '云南省-玉溪市-澄江县', '305', '02', '04', '01', '2953', '', null, null);
INSERT INTO t_sys_area VALUES ('2954', '530423', '530423', '530423', '通海县', 'Tonghai Xian', 'Tonghai Xian', ',26,305,2954,', '云南省-玉溪市-通海县', '305', '02', '04', '01', '2954', '', null, null);
INSERT INTO t_sys_area VALUES ('2955', '530424', '530424', '530424', '华宁县', 'Huaning Xian', 'Huaning Xian', ',26,305,2955,', '云南省-玉溪市-华宁县', '305', '02', '04', '01', '2955', '', null, null);
INSERT INTO t_sys_area VALUES ('2956', '530425', '530425', '530425', '易门县', 'Yimen Xian', 'Yimen Xian', ',26,305,2956,', '云南省-玉溪市-易门县', '305', '02', '04', '01', '2956', '', null, null);
INSERT INTO t_sys_area VALUES ('2957', '530426', '530426', '530426', '峨山彝族自治县', 'Eshan Yizu Zizhixian', 'Eshan Yizu Zizhixian', ',26,305,2957,', '云南省-玉溪市-峨山彝族自治县', '305', '02', '04', '01', '2957', '', null, null);
INSERT INTO t_sys_area VALUES ('2958', '530427', '530427', '530427', '新平彝族傣族自治县', 'Xinping Yizu Daizu Zizhixian', 'Xinping Yizu Daizu Zizhixian', ',26,305,2958,', '云南省-玉溪市-新平彝族傣族自治县', '305', '02', '04', '01', '2958', '', null, null);
INSERT INTO t_sys_area VALUES ('2959', '530428', '530428', '530428', '元江哈尼族彝族傣族自治县', 'Yuanjiang Hanizu Yizu Daizu Zizhixian', 'Yuanjiang Hanizu Yizu Daizu Zizhixian', ',26,305,2959,', '云南省-玉溪市-元江哈尼族彝族傣族自治县', '305', '02', '04', '01', '2959', '', null, null);
INSERT INTO t_sys_area VALUES ('2960', '530501', '530501', '530501', '市辖区', '1', '1', ',26,306,2960,', '云南省-保山市-市辖区', '306', '02', '04', '01', '2960', '', null, null);
INSERT INTO t_sys_area VALUES ('2961', '530502', '530502', '530502', '隆阳区', 'Longyang Qu', 'Longyang Qu', ',26,306,2961,', '云南省-保山市-隆阳区', '306', '02', '04', '01', '2961', '', null, null);
INSERT INTO t_sys_area VALUES ('2962', '530521', '530521', '530521', '施甸县', 'Shidian Xian', 'Shidian Xian', ',26,306,2962,', '云南省-保山市-施甸县', '306', '02', '04', '01', '2962', '', null, null);
INSERT INTO t_sys_area VALUES ('2963', '530522', '530522', '530522', '腾冲县', 'Tengchong Xian', 'Tengchong Xian', ',26,306,2963,', '云南省-保山市-腾冲县', '306', '02', '04', '01', '2963', '', null, null);
INSERT INTO t_sys_area VALUES ('2964', '530523', '530523', '530523', '龙陵县', 'Longling Xian', 'Longling Xian', ',26,306,2964,', '云南省-保山市-龙陵县', '306', '02', '04', '01', '2964', '', null, null);
INSERT INTO t_sys_area VALUES ('2965', '530524', '530524', '530524', '昌宁县', 'Changning Xian', 'Changning Xian', ',26,306,2965,', '云南省-保山市-昌宁县', '306', '02', '04', '01', '2965', '', null, null);
INSERT INTO t_sys_area VALUES ('2966', '530601', '530601', '530601', '市辖区', '1', '1', ',26,307,2966,', '云南省-昭通市-市辖区', '307', '02', '04', '01', '2966', '', null, null);
INSERT INTO t_sys_area VALUES ('2967', '530602', '530602', '530602', '昭阳区', 'Zhaoyang Qu', 'Zhaoyang Qu', ',26,307,2967,', '云南省-昭通市-昭阳区', '307', '02', '04', '01', '2967', '', null, null);
INSERT INTO t_sys_area VALUES ('2968', '530621', '530621', '530621', '鲁甸县', 'Ludian Xian', 'Ludian Xian', ',26,307,2968,', '云南省-昭通市-鲁甸县', '307', '02', '04', '01', '2968', '', null, null);
INSERT INTO t_sys_area VALUES ('2969', '530622', '530622', '530622', '巧家县', 'Qiaojia Xian', 'Qiaojia Xian', ',26,307,2969,', '云南省-昭通市-巧家县', '307', '02', '04', '01', '2969', '', null, null);
INSERT INTO t_sys_area VALUES ('2970', '530623', '530623', '530623', '盐津县', 'Yanjin Xian', 'Yanjin Xian', ',26,307,2970,', '云南省-昭通市-盐津县', '307', '02', '04', '01', '2970', '', null, null);
INSERT INTO t_sys_area VALUES ('2971', '530624', '530624', '530624', '大关县', 'Daguan Xian', 'Daguan Xian', ',26,307,2971,', '云南省-昭通市-大关县', '307', '02', '04', '01', '2971', '', null, null);
INSERT INTO t_sys_area VALUES ('2972', '530625', '530625', '530625', '永善县', 'Yongshan Xian', 'Yongshan Xian', ',26,307,2972,', '云南省-昭通市-永善县', '307', '02', '04', '01', '2972', '', null, null);
INSERT INTO t_sys_area VALUES ('2973', '530626', '530626', '530626', '绥江县', 'Suijiang Xian', 'Suijiang Xian', ',26,307,2973,', '云南省-昭通市-绥江县', '307', '02', '04', '01', '2973', '', null, null);
INSERT INTO t_sys_area VALUES ('2974', '530627', '530627', '530627', '镇雄县', 'Zhenxiong Xian', 'Zhenxiong Xian', ',26,307,2974,', '云南省-昭通市-镇雄县', '307', '02', '04', '01', '2974', '', null, null);
INSERT INTO t_sys_area VALUES ('2975', '530628', '530628', '530628', '彝良县', 'Yiliang Xian', 'Yiliang Xian', ',26,307,2975,', '云南省-昭通市-彝良县', '307', '02', '04', '01', '2975', '', null, null);
INSERT INTO t_sys_area VALUES ('2976', '530629', '530629', '530629', '威信县', 'Weixin Xian', 'Weixin Xian', ',26,307,2976,', '云南省-昭通市-威信县', '307', '02', '04', '01', '2976', '', null, null);
INSERT INTO t_sys_area VALUES ('2977', '530630', '530630', '530630', '水富县', 'Shuifu Xian ', 'Shuifu Xian ', ',26,307,2977,', '云南省-昭通市-水富县', '307', '02', '04', '01', '2977', '', null, null);
INSERT INTO t_sys_area VALUES ('2978', '530701', '530701', '530701', '市辖区', '1', '1', ',26,308,2978,', '云南省-丽江市-市辖区', '308', '02', '04', '01', '2978', '', null, null);
INSERT INTO t_sys_area VALUES ('2979', '530702', '530702', '530702', '古城区', 'Gucheng Qu', 'Gucheng Qu', ',26,308,2979,', '云南省-丽江市-古城区', '308', '02', '04', '01', '2979', '', null, null);
INSERT INTO t_sys_area VALUES ('2980', '530721', '530721', '530721', '玉龙纳西族自治县', 'Yulongnaxizuzizhi Xian', 'Yulongnaxizuzizhi Xian', ',26,308,2980,', '云南省-丽江市-玉龙纳西族自治县', '308', '02', '04', '01', '2980', '', null, null);
INSERT INTO t_sys_area VALUES ('2981', '530722', '530722', '530722', '永胜县', 'Yongsheng Xian', 'Yongsheng Xian', ',26,308,2981,', '云南省-丽江市-永胜县', '308', '02', '04', '01', '2981', '', null, null);
INSERT INTO t_sys_area VALUES ('2982', '530723', '530723', '530723', '华坪县', 'Huaping Xian', 'Huaping Xian', ',26,308,2982,', '云南省-丽江市-华坪县', '308', '02', '04', '01', '2982', '', null, null);
INSERT INTO t_sys_area VALUES ('2983', '530724', '530724', '530724', '宁蒗彝族自治县', 'Ninglang Yizu Zizhixian', 'Ninglang Yizu Zizhixian', ',26,308,2983,', '云南省-丽江市-宁蒗彝族自治县', '308', '02', '04', '01', '2983', '', null, null);
INSERT INTO t_sys_area VALUES ('2984', '530801', '530801', '530801', '市辖区', '1', '1', ',26,309,2984,', '云南省-普洱市-市辖区', '309', '02', '04', '01', '2984', '', null, null);
INSERT INTO t_sys_area VALUES ('2985', '530802', '530802', '530802', '思茅区', 'Simao Qu', 'Simao Qu', ',26,309,2985,', '云南省-普洱市-思茅区', '309', '02', '04', '01', '2985', '', null, null);
INSERT INTO t_sys_area VALUES ('2986', '530821', '530821', '530821', '宁洱哈尼族彝族自治县', 'Pu,er Hanizu Yizu Zizhixian', 'Pu,er Hanizu Yizu Zizhixian', ',26,309,2986,', '云南省-普洱市-宁洱哈尼族彝族自治县', '309', '02', '04', '01', '2986', '', null, null);
INSERT INTO t_sys_area VALUES ('2987', '530822', '530822', '530822', '墨江哈尼族自治县', 'Mojiang Hanizu Zizhixian', 'Mojiang Hanizu Zizhixian', ',26,309,2987,', '云南省-普洱市-墨江哈尼族自治县', '309', '02', '04', '01', '2987', '', null, null);
INSERT INTO t_sys_area VALUES ('2988', '530823', '530823', '530823', '景东彝族自治县', 'Jingdong Yizu Zizhixian', 'Jingdong Yizu Zizhixian', ',26,309,2988,', '云南省-普洱市-景东彝族自治县', '309', '02', '04', '01', '2988', '', null, null);
INSERT INTO t_sys_area VALUES ('2989', '530824', '530824', '530824', '景谷傣族彝族自治县', 'Jinggu Daizu Yizu Zizhixian', 'Jinggu Daizu Yizu Zizhixian', ',26,309,2989,', '云南省-普洱市-景谷傣族彝族自治县', '309', '02', '04', '01', '2989', '', null, null);
INSERT INTO t_sys_area VALUES ('2990', '530825', '530825', '530825', '镇沅彝族哈尼族拉祜族自治县', 'Zhenyuan Yizu Hanizu Lahuzu Zizhixian', 'Zhenyuan Yizu Hanizu Lahuzu Zizhixian', ',26,309,2990,', '云南省-普洱市-镇沅彝族哈尼族拉祜族自治县', '309', '02', '04', '01', '2990', '', null, null);
INSERT INTO t_sys_area VALUES ('2991', '530826', '530826', '530826', '江城哈尼族彝族自治县', 'Jiangcheng Hanizu Yizu Zizhixian', 'Jiangcheng Hanizu Yizu Zizhixian', ',26,309,2991,', '云南省-普洱市-江城哈尼族彝族自治县', '309', '02', '04', '01', '2991', '', null, null);
INSERT INTO t_sys_area VALUES ('2992', '530827', '530827', '530827', '孟连傣族拉祜族佤族自治县', 'Menglian Daizu Lahuzu Vazu Zizixian', 'Menglian Daizu Lahuzu Vazu Zizixian', ',26,309,2992,', '云南省-普洱市-孟连傣族拉祜族佤族自治县', '309', '02', '04', '01', '2992', '', null, null);
INSERT INTO t_sys_area VALUES ('2993', '530828', '530828', '530828', '澜沧拉祜族自治县', 'Lancang Lahuzu Zizhixian', 'Lancang Lahuzu Zizhixian', ',26,309,2993,', '云南省-普洱市-澜沧拉祜族自治县', '309', '02', '04', '01', '2993', '', null, null);
INSERT INTO t_sys_area VALUES ('2994', '530829', '530829', '530829', '西盟佤族自治县', 'Ximeng Vazu Zizhixian', 'Ximeng Vazu Zizhixian', ',26,309,2994,', '云南省-普洱市-西盟佤族自治县', '309', '02', '04', '01', '2994', '', null, null);
INSERT INTO t_sys_area VALUES ('2995', '530901', '530901', '530901', '市辖区', '1', '1', ',26,310,2995,', '云南省-临沧市-市辖区', '310', '02', '04', '01', '2995', '', null, null);
INSERT INTO t_sys_area VALUES ('2996', '530902', '530902', '530902', '临翔区', 'Linxiang Qu', 'Linxiang Qu', ',26,310,2996,', '云南省-临沧市-临翔区', '310', '02', '04', '01', '2996', '', null, null);
INSERT INTO t_sys_area VALUES ('2997', '530921', '530921', '530921', '凤庆县', 'Fengqing Xian', 'Fengqing Xian', ',26,310,2997,', '云南省-临沧市-凤庆县', '310', '02', '04', '01', '2997', '', null, null);
INSERT INTO t_sys_area VALUES ('2998', '530922', '530922', '530922', '云县', 'Yun Xian', 'Yun Xian', ',26,310,2998,', '云南省-临沧市-云县', '310', '02', '04', '01', '2998', '', null, null);
INSERT INTO t_sys_area VALUES ('2999', '530923', '530923', '530923', '永德县', 'Yongde Xian', 'Yongde Xian', ',26,310,2999,', '云南省-临沧市-永德县', '310', '02', '04', '01', '2999', '', null, null);
INSERT INTO t_sys_area VALUES ('3000', '530924', '530924', '530924', '镇康县', 'Zhenkang Xian', 'Zhenkang Xian', ',26,310,3000,', '云南省-临沧市-镇康县', '310', '02', '04', '01', '3000', '', null, null);
INSERT INTO t_sys_area VALUES ('3001', '530925', '530925', '530925', '双江拉祜族佤族布朗族傣族自治县', 'Shuangjiang Lahuzu Vazu Bulangzu Daizu Zizhixian', 'Shuangjiang Lahuzu Vazu Bulangzu Daizu Zizhixian', ',26,310,3001,', '云南省-临沧市-双江拉祜族佤族布朗族傣族自治县', '310', '02', '04', '01', '3001', '', null, null);
INSERT INTO t_sys_area VALUES ('3002', '530926', '530926', '530926', '耿马傣族佤族自治县', 'Gengma Daizu Vazu Zizhixian', 'Gengma Daizu Vazu Zizhixian', ',26,310,3002,', '云南省-临沧市-耿马傣族佤族自治县', '310', '02', '04', '01', '3002', '', null, null);
INSERT INTO t_sys_area VALUES ('3003', '530927', '530927', '530927', '沧源佤族自治县', 'Cangyuan Vazu Zizhixian', 'Cangyuan Vazu Zizhixian', ',26,310,3003,', '云南省-临沧市-沧源佤族自治县', '310', '02', '04', '01', '3003', '', null, null);
INSERT INTO t_sys_area VALUES ('3004', '532301', '532301', '532301', '楚雄市', 'Chuxiong Shi', 'Chuxiong Shi', ',26,311,3004,', '云南省-楚雄彝族自治州-楚雄市', '311', '02', '04', '01', '3004', '', null, null);
INSERT INTO t_sys_area VALUES ('3005', '532322', '532322', '532322', '双柏县', 'Shuangbai Xian', 'Shuangbai Xian', ',26,311,3005,', '云南省-楚雄彝族自治州-双柏县', '311', '02', '04', '01', '3005', '', null, null);
INSERT INTO t_sys_area VALUES ('3006', '532323', '532323', '532323', '牟定县', 'Mouding Xian', 'Mouding Xian', ',26,311,3006,', '云南省-楚雄彝族自治州-牟定县', '311', '02', '04', '01', '3006', '', null, null);
INSERT INTO t_sys_area VALUES ('3007', '532324', '532324', '532324', '南华县', 'Nanhua Xian', 'Nanhua Xian', ',26,311,3007,', '云南省-楚雄彝族自治州-南华县', '311', '02', '04', '01', '3007', '', null, null);
INSERT INTO t_sys_area VALUES ('3008', '532325', '532325', '532325', '姚安县', 'Yao,an Xian', 'Yao,an Xian', ',26,311,3008,', '云南省-楚雄彝族自治州-姚安县', '311', '02', '04', '01', '3008', '', null, null);
INSERT INTO t_sys_area VALUES ('3009', '532326', '532326', '532326', '大姚县', 'Dayao Xian', 'Dayao Xian', ',26,311,3009,', '云南省-楚雄彝族自治州-大姚县', '311', '02', '04', '01', '3009', '', null, null);
INSERT INTO t_sys_area VALUES ('3010', '532327', '532327', '532327', '永仁县', 'Yongren Xian', 'Yongren Xian', ',26,311,3010,', '云南省-楚雄彝族自治州-永仁县', '311', '02', '04', '01', '3010', '', null, null);
INSERT INTO t_sys_area VALUES ('3011', '532328', '532328', '532328', '元谋县', 'Yuanmou Xian', 'Yuanmou Xian', ',26,311,3011,', '云南省-楚雄彝族自治州-元谋县', '311', '02', '04', '01', '3011', '', null, null);
INSERT INTO t_sys_area VALUES ('3012', '532329', '532329', '532329', '武定县', 'Wuding Xian', 'Wuding Xian', ',26,311,3012,', '云南省-楚雄彝族自治州-武定县', '311', '02', '04', '01', '3012', '', null, null);
INSERT INTO t_sys_area VALUES ('3013', '532331', '532331', '532331', '禄丰县', 'Lufeng Xian', 'Lufeng Xian', ',26,311,3013,', '云南省-楚雄彝族自治州-禄丰县', '311', '02', '04', '01', '3013', '', null, null);
INSERT INTO t_sys_area VALUES ('3014', '532501', '532501', '532501', '个旧市', 'Gejiu Shi', 'Gejiu Shi', ',26,312,3014,', '云南省-红河哈尼族彝族自治州-个旧市', '312', '02', '04', '01', '3014', '', null, null);
INSERT INTO t_sys_area VALUES ('3015', '532502', '532502', '532502', '开远市', 'Kaiyuan Shi', 'Kaiyuan Shi', ',26,312,3015,', '云南省-红河哈尼族彝族自治州-开远市', '312', '02', '04', '01', '3015', '', null, null);
INSERT INTO t_sys_area VALUES ('3016', '532503', '532503', '532503', '蒙自市', 'Mengzi Xian', 'Mengzi Xian', ',26,312,3016,', '云南省-红河哈尼族彝族自治州-蒙自市', '312', '02', '04', '01', '3016', '', null, null);
INSERT INTO t_sys_area VALUES ('3017', '532523', '532523', '532523', '屏边苗族自治县', 'Pingbian Miaozu Zizhixian', 'Pingbian Miaozu Zizhixian', ',26,312,3017,', '云南省-红河哈尼族彝族自治州-屏边苗族自治县', '312', '02', '04', '01', '3017', '', null, null);
INSERT INTO t_sys_area VALUES ('3018', '532524', '532524', '532524', '建水县', 'Jianshui Xian', 'Jianshui Xian', ',26,312,3018,', '云南省-红河哈尼族彝族自治州-建水县', '312', '02', '04', '01', '3018', '', null, null);
INSERT INTO t_sys_area VALUES ('3019', '532525', '532525', '532525', '石屏县', 'Shiping Xian', 'Shiping Xian', ',26,312,3019,', '云南省-红河哈尼族彝族自治州-石屏县', '312', '02', '04', '01', '3019', '', null, null);
INSERT INTO t_sys_area VALUES ('3020', '532526', '532526', '532526', '弥勒县', 'Mile Xian', 'Mile Xian', ',26,312,3020,', '云南省-红河哈尼族彝族自治州-弥勒县', '312', '02', '04', '01', '3020', '', null, null);
INSERT INTO t_sys_area VALUES ('3021', '532527', '532527', '532527', '泸西县', 'Luxi Xian', 'Luxi Xian', ',26,312,3021,', '云南省-红河哈尼族彝族自治州-泸西县', '312', '02', '04', '01', '3021', '', null, null);
INSERT INTO t_sys_area VALUES ('3022', '532528', '532528', '532528', '元阳县', 'Yuanyang Xian', 'Yuanyang Xian', ',26,312,3022,', '云南省-红河哈尼族彝族自治州-元阳县', '312', '02', '04', '01', '3022', '', null, null);
INSERT INTO t_sys_area VALUES ('3023', '532529', '532529', '532529', '红河县', 'Honghe Xian', 'Honghe Xian', ',26,312,3023,', '云南省-红河哈尼族彝族自治州-红河县', '312', '02', '04', '01', '3023', '', null, null);
INSERT INTO t_sys_area VALUES ('3024', '532530', '532530', '532530', '金平苗族瑶族傣族自治县', 'Jinping Miaozu Yaozu Daizu Zizhixian', 'Jinping Miaozu Yaozu Daizu Zizhixian', ',26,312,3024,', '云南省-红河哈尼族彝族自治州-金平苗族瑶族傣族自治县', '312', '02', '04', '01', '3024', '', null, null);
INSERT INTO t_sys_area VALUES ('3025', '532531', '532531', '532531', '绿春县', 'Lvchun Xian', 'Lvchun Xian', ',26,312,3025,', '云南省-红河哈尼族彝族自治州-绿春县', '312', '02', '04', '01', '3025', '', null, null);
INSERT INTO t_sys_area VALUES ('3026', '532532', '532532', '532532', '河口瑶族自治县', 'Hekou Yaozu Zizhixian', 'Hekou Yaozu Zizhixian', ',26,312,3026,', '云南省-红河哈尼族彝族自治州-河口瑶族自治县', '312', '02', '04', '01', '3026', '', null, null);
INSERT INTO t_sys_area VALUES ('3027', '532621', '532621', '532621', '文山县', 'Wenshan Xian', 'Wenshan Xian', ',26,313,3027,', '云南省-文山壮族苗族自治州-文山县', '313', '02', '04', '01', '3027', '', null, null);
INSERT INTO t_sys_area VALUES ('3028', '532622', '532622', '532622', '砚山县', 'Yanshan Xian', 'Yanshan Xian', ',26,313,3028,', '云南省-文山壮族苗族自治州-砚山县', '313', '02', '04', '01', '3028', '', null, null);
INSERT INTO t_sys_area VALUES ('3029', '532623', '532623', '532623', '西畴县', 'Xichou Xian', 'Xichou Xian', ',26,313,3029,', '云南省-文山壮族苗族自治州-西畴县', '313', '02', '04', '01', '3029', '', null, null);
INSERT INTO t_sys_area VALUES ('3030', '532624', '532624', '532624', '麻栗坡县', 'Malipo Xian', 'Malipo Xian', ',26,313,3030,', '云南省-文山壮族苗族自治州-麻栗坡县', '313', '02', '04', '01', '3030', '', null, null);
INSERT INTO t_sys_area VALUES ('3031', '532625', '532625', '532625', '马关县', 'Maguan Xian', 'Maguan Xian', ',26,313,3031,', '云南省-文山壮族苗族自治州-马关县', '313', '02', '04', '01', '3031', '', null, null);
INSERT INTO t_sys_area VALUES ('3032', '532626', '532626', '532626', '丘北县', 'Qiubei Xian', 'Qiubei Xian', ',26,313,3032,', '云南省-文山壮族苗族自治州-丘北县', '313', '02', '04', '01', '3032', '', null, null);
INSERT INTO t_sys_area VALUES ('3033', '532627', '532627', '532627', '广南县', 'Guangnan Xian', 'Guangnan Xian', ',26,313,3033,', '云南省-文山壮族苗族自治州-广南县', '313', '02', '04', '01', '3033', '', null, null);
INSERT INTO t_sys_area VALUES ('3034', '532628', '532628', '532628', '富宁县', 'Funing Xian', 'Funing Xian', ',26,313,3034,', '云南省-文山壮族苗族自治州-富宁县', '313', '02', '04', '01', '3034', '', null, null);
INSERT INTO t_sys_area VALUES ('3035', '532801', '532801', '532801', '景洪市', 'Jinghong Shi', 'Jinghong Shi', ',26,314,3035,', '云南省-西双版纳傣族自治州-景洪市', '314', '02', '04', '01', '3035', '', null, null);
INSERT INTO t_sys_area VALUES ('3036', '532822', '532822', '532822', '勐海县', 'Menghai Xian', 'Menghai Xian', ',26,314,3036,', '云南省-西双版纳傣族自治州-勐海县', '314', '02', '04', '01', '3036', '', null, null);
INSERT INTO t_sys_area VALUES ('3037', '532823', '532823', '532823', '勐腊县', 'Mengla Xian', 'Mengla Xian', ',26,314,3037,', '云南省-西双版纳傣族自治州-勐腊县', '314', '02', '04', '01', '3037', '', null, null);
INSERT INTO t_sys_area VALUES ('3038', '532901', '532901', '532901', '大理市', 'Dali Shi', 'Dali Shi', ',26,315,3038,', '云南省-大理白族自治州-大理市', '315', '02', '04', '01', '3038', '', null, null);
INSERT INTO t_sys_area VALUES ('3039', '532922', '532922', '532922', '漾濞彝族自治县', 'Yangbi Yizu Zizhixian', 'Yangbi Yizu Zizhixian', ',26,315,3039,', '云南省-大理白族自治州-漾濞彝族自治县', '315', '02', '04', '01', '3039', '', null, null);
INSERT INTO t_sys_area VALUES ('3040', '532923', '532923', '532923', '祥云县', 'Xiangyun Xian', 'Xiangyun Xian', ',26,315,3040,', '云南省-大理白族自治州-祥云县', '315', '02', '04', '01', '3040', '', null, null);
INSERT INTO t_sys_area VALUES ('3041', '532924', '532924', '532924', '宾川县', 'Binchuan Xian', 'Binchuan Xian', ',26,315,3041,', '云南省-大理白族自治州-宾川县', '315', '02', '04', '01', '3041', '', null, null);
INSERT INTO t_sys_area VALUES ('3042', '532925', '532925', '532925', '弥渡县', 'Midu Xian', 'Midu Xian', ',26,315,3042,', '云南省-大理白族自治州-弥渡县', '315', '02', '04', '01', '3042', '', null, null);
INSERT INTO t_sys_area VALUES ('3043', '532926', '532926', '532926', '南涧彝族自治县', 'Nanjian Yizu Zizhixian', 'Nanjian Yizu Zizhixian', ',26,315,3043,', '云南省-大理白族自治州-南涧彝族自治县', '315', '02', '04', '01', '3043', '', null, null);
INSERT INTO t_sys_area VALUES ('3044', '532927', '532927', '532927', '巍山彝族回族自治县', 'Weishan Yizu Huizu Zizhixian', 'Weishan Yizu Huizu Zizhixian', ',26,315,3044,', '云南省-大理白族自治州-巍山彝族回族自治县', '315', '02', '04', '01', '3044', '', null, null);
INSERT INTO t_sys_area VALUES ('3045', '532928', '532928', '532928', '永平县', 'Yongping Xian', 'Yongping Xian', ',26,315,3045,', '云南省-大理白族自治州-永平县', '315', '02', '04', '01', '3045', '', null, null);
INSERT INTO t_sys_area VALUES ('3046', '532929', '532929', '532929', '云龙县', 'Yunlong Xian', 'Yunlong Xian', ',26,315,3046,', '云南省-大理白族自治州-云龙县', '315', '02', '04', '01', '3046', '', null, null);
INSERT INTO t_sys_area VALUES ('3047', '532930', '532930', '532930', '洱源县', 'Eryuan Xian', 'Eryuan Xian', ',26,315,3047,', '云南省-大理白族自治州-洱源县', '315', '02', '04', '01', '3047', '', null, null);
INSERT INTO t_sys_area VALUES ('3048', '532931', '532931', '532931', '剑川县', 'Jianchuan Xian', 'Jianchuan Xian', ',26,315,3048,', '云南省-大理白族自治州-剑川县', '315', '02', '04', '01', '3048', '', null, null);
INSERT INTO t_sys_area VALUES ('3049', '532932', '532932', '532932', '鹤庆县', 'Heqing Xian', 'Heqing Xian', ',26,315,3049,', '云南省-大理白族自治州-鹤庆县', '315', '02', '04', '01', '3049', '', null, null);
INSERT INTO t_sys_area VALUES ('3050', '533102', '533102', '533102', '瑞丽市', 'Ruili Shi', 'Ruili Shi', ',26,316,3050,', '云南省-德宏傣族景颇族自治州-瑞丽市', '316', '02', '04', '01', '3050', '', null, null);
INSERT INTO t_sys_area VALUES ('3051', '533103', '533103', '533103', '芒市', 'Luxi Shi', 'Luxi Shi', ',26,316,3051,', '云南省-德宏傣族景颇族自治州-芒市', '316', '02', '04', '01', '3051', '', null, null);
INSERT INTO t_sys_area VALUES ('3052', '533122', '533122', '533122', '梁河县', 'Lianghe Xian', 'Lianghe Xian', ',26,316,3052,', '云南省-德宏傣族景颇族自治州-梁河县', '316', '02', '04', '01', '3052', '', null, null);
INSERT INTO t_sys_area VALUES ('3053', '533123', '533123', '533123', '盈江县', 'Yingjiang Xian', 'Yingjiang Xian', ',26,316,3053,', '云南省-德宏傣族景颇族自治州-盈江县', '316', '02', '04', '01', '3053', '', null, null);
INSERT INTO t_sys_area VALUES ('3054', '533124', '533124', '533124', '陇川县', 'Longchuan Xian', 'Longchuan Xian', ',26,316,3054,', '云南省-德宏傣族景颇族自治州-陇川县', '316', '02', '04', '01', '3054', '', null, null);
INSERT INTO t_sys_area VALUES ('3055', '533321', '533321', '533321', '泸水县', 'Lushui Xian', 'Lushui Xian', ',26,317,3055,', '云南省-怒江傈僳族自治州-泸水县', '317', '02', '04', '01', '3055', '', null, null);
INSERT INTO t_sys_area VALUES ('3056', '533323', '533323', '533323', '福贡县', 'Fugong Xian', 'Fugong Xian', ',26,317,3056,', '云南省-怒江傈僳族自治州-福贡县', '317', '02', '04', '01', '3056', '', null, null);
INSERT INTO t_sys_area VALUES ('3057', '533324', '533324', '533324', '贡山独龙族怒族自治县', 'Gongshan Dulongzu Nuzu Zizhixian', 'Gongshan Dulongzu Nuzu Zizhixian', ',26,317,3057,', '云南省-怒江傈僳族自治州-贡山独龙族怒族自治县', '317', '02', '04', '01', '3057', '', null, null);
INSERT INTO t_sys_area VALUES ('3058', '533325', '533325', '533325', '兰坪白族普米族自治县', 'Lanping Baizu Pumizu Zizhixian', 'Lanping Baizu Pumizu Zizhixian', ',26,317,3058,', '云南省-怒江傈僳族自治州-兰坪白族普米族自治县', '317', '02', '04', '01', '3058', '', null, null);
INSERT INTO t_sys_area VALUES ('3059', '533421', '533421', '533421', '香格里拉县', 'Xianggelila Xian', 'Xianggelila Xian', ',26,318,3059,', '云南省-迪庆藏族自治州-香格里拉县', '318', '02', '04', '01', '3059', '', null, null);
INSERT INTO t_sys_area VALUES ('3060', '533422', '533422', '533422', '德钦县', 'Deqen Xian', 'Deqen Xian', ',26,318,3060,', '云南省-迪庆藏族自治州-德钦县', '318', '02', '04', '01', '3060', '', null, null);
INSERT INTO t_sys_area VALUES ('3061', '533423', '533423', '533423', '维西傈僳族自治县', 'Weixi Lisuzu Zizhixian', 'Weixi Lisuzu Zizhixian', ',26,318,3061,', '云南省-迪庆藏族自治州-维西傈僳族自治县', '318', '02', '04', '01', '3061', '', null, null);
INSERT INTO t_sys_area VALUES ('3062', '540101', '540101', '540101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',27,319,3062,', '西藏自治区-拉萨市-市辖区', '319', '02', '04', '01', '3062', '', null, null);
INSERT INTO t_sys_area VALUES ('3063', '540102', '540102', '540102', '城关区', 'Chengguang Qu', 'Chengguang Qu', ',27,319,3063,', '西藏自治区-拉萨市-城关区', '319', '02', '04', '01', '3063', '', null, null);
INSERT INTO t_sys_area VALUES ('3064', '540121', '540121', '540121', '林周县', 'Lhvnzhub Xian', 'Lhvnzhub Xian', ',27,319,3064,', '西藏自治区-拉萨市-林周县', '319', '02', '04', '01', '3064', '', null, null);
INSERT INTO t_sys_area VALUES ('3065', '540122', '540122', '540122', '当雄县', 'Damxung Xian', 'Damxung Xian', ',27,319,3065,', '西藏自治区-拉萨市-当雄县', '319', '02', '04', '01', '3065', '', null, null);
INSERT INTO t_sys_area VALUES ('3066', '540123', '540123', '540123', '尼木县', 'Nyemo Xian', 'Nyemo Xian', ',27,319,3066,', '西藏自治区-拉萨市-尼木县', '319', '02', '04', '01', '3066', '', null, null);
INSERT INTO t_sys_area VALUES ('3067', '540124', '540124', '540124', '曲水县', 'Qvxv Xian', 'Qvxv Xian', ',27,319,3067,', '西藏自治区-拉萨市-曲水县', '319', '02', '04', '01', '3067', '', null, null);
INSERT INTO t_sys_area VALUES ('3068', '540125', '540125', '540125', '堆龙德庆县', 'Doilungdeqen Xian', 'Doilungdeqen Xian', ',27,319,3068,', '西藏自治区-拉萨市-堆龙德庆县', '319', '02', '04', '01', '3068', '', null, null);
INSERT INTO t_sys_area VALUES ('3069', '540126', '540126', '540126', '达孜县', 'Dagze Xian', 'Dagze Xian', ',27,319,3069,', '西藏自治区-拉萨市-达孜县', '319', '02', '04', '01', '3069', '', null, null);
INSERT INTO t_sys_area VALUES ('3070', '540127', '540127', '540127', '墨竹工卡县', 'Maizhokunggar Xian', 'Maizhokunggar Xian', ',27,319,3070,', '西藏自治区-拉萨市-墨竹工卡县', '319', '02', '04', '01', '3070', '', null, null);
INSERT INTO t_sys_area VALUES ('3071', '542121', '542121', '542121', '昌都县', 'Qamdo Xian', 'Qamdo Xian', ',27,320,3071,', '西藏自治区-昌都地区-昌都县', '320', '02', '04', '01', '3071', '', null, null);
INSERT INTO t_sys_area VALUES ('3072', '542122', '542122', '542122', '江达县', 'Jomda Xian', 'Jomda Xian', ',27,320,3072,', '西藏自治区-昌都地区-江达县', '320', '02', '04', '01', '3072', '', null, null);
INSERT INTO t_sys_area VALUES ('3073', '542123', '542123', '542123', '贡觉县', 'Konjo Xian', 'Konjo Xian', ',27,320,3073,', '西藏自治区-昌都地区-贡觉县', '320', '02', '04', '01', '3073', '', null, null);
INSERT INTO t_sys_area VALUES ('3074', '542124', '542124', '542124', '类乌齐县', 'Riwoqe Xian', 'Riwoqe Xian', ',27,320,3074,', '西藏自治区-昌都地区-类乌齐县', '320', '02', '04', '01', '3074', '', null, null);
INSERT INTO t_sys_area VALUES ('3075', '542125', '542125', '542125', '丁青县', 'Dengqen Xian', 'Dengqen Xian', ',27,320,3075,', '西藏自治区-昌都地区-丁青县', '320', '02', '04', '01', '3075', '', null, null);
INSERT INTO t_sys_area VALUES ('3076', '542126', '542126', '542126', '察雅县', 'Chagyab Xian', 'Chagyab Xian', ',27,320,3076,', '西藏自治区-昌都地区-察雅县', '320', '02', '04', '01', '3076', '', null, null);
INSERT INTO t_sys_area VALUES ('3077', '542127', '542127', '542127', '八宿县', 'Baxoi Xian', 'Baxoi Xian', ',27,320,3077,', '西藏自治区-昌都地区-八宿县', '320', '02', '04', '01', '3077', '', null, null);
INSERT INTO t_sys_area VALUES ('3078', '542128', '542128', '542128', '左贡县', 'Zogang Xian', 'Zogang Xian', ',27,320,3078,', '西藏自治区-昌都地区-左贡县', '320', '02', '04', '01', '3078', '', null, null);
INSERT INTO t_sys_area VALUES ('3079', '542129', '542129', '542129', '芒康县', 'Mangkam Xian', 'Mangkam Xian', ',27,320,3079,', '西藏自治区-昌都地区-芒康县', '320', '02', '04', '01', '3079', '', null, null);
INSERT INTO t_sys_area VALUES ('3080', '542132', '542132', '542132', '洛隆县', 'Lhorong Xian', 'Lhorong Xian', ',27,320,3080,', '西藏自治区-昌都地区-洛隆县', '320', '02', '04', '01', '3080', '', null, null);
INSERT INTO t_sys_area VALUES ('3081', '542133', '542133', '542133', '边坝县', 'Banbar Xian', 'Banbar Xian', ',27,320,3081,', '西藏自治区-昌都地区-边坝县', '320', '02', '04', '01', '3081', '', null, null);
INSERT INTO t_sys_area VALUES ('3082', '542221', '542221', '542221', '乃东县', 'Nedong Xian', 'Nedong Xian', ',27,321,3082,', '西藏自治区-山南地区-乃东县', '321', '02', '04', '01', '3082', '', null, null);
INSERT INTO t_sys_area VALUES ('3083', '542222', '542222', '542222', '扎囊县', 'Chanang(Chatang) Xian', 'Chanang(Chatang) Xian', ',27,321,3083,', '西藏自治区-山南地区-扎囊县', '321', '02', '04', '01', '3083', '', null, null);
INSERT INTO t_sys_area VALUES ('3084', '542223', '542223', '542223', '贡嘎县', 'Gonggar Xian', 'Gonggar Xian', ',27,321,3084,', '西藏自治区-山南地区-贡嘎县', '321', '02', '04', '01', '3084', '', null, null);
INSERT INTO t_sys_area VALUES ('3085', '542224', '542224', '542224', '桑日县', 'Sangri Xian', 'Sangri Xian', ',27,321,3085,', '西藏自治区-山南地区-桑日县', '321', '02', '04', '01', '3085', '', null, null);
INSERT INTO t_sys_area VALUES ('3086', '542225', '542225', '542225', '琼结县', 'Qonggyai Xian', 'Qonggyai Xian', ',27,321,3086,', '西藏自治区-山南地区-琼结县', '321', '02', '04', '01', '3086', '', null, null);
INSERT INTO t_sys_area VALUES ('3087', '542226', '542226', '542226', '曲松县', 'Qusum Xian', 'Qusum Xian', ',27,321,3087,', '西藏自治区-山南地区-曲松县', '321', '02', '04', '01', '3087', '', null, null);
INSERT INTO t_sys_area VALUES ('3088', '542227', '542227', '542227', '措美县', 'Comai Xian', 'Comai Xian', ',27,321,3088,', '西藏自治区-山南地区-措美县', '321', '02', '04', '01', '3088', '', null, null);
INSERT INTO t_sys_area VALUES ('3089', '542228', '542228', '542228', '洛扎县', 'Lhozhag Xian', 'Lhozhag Xian', ',27,321,3089,', '西藏自治区-山南地区-洛扎县', '321', '02', '04', '01', '3089', '', null, null);
INSERT INTO t_sys_area VALUES ('3090', '542229', '542229', '542229', '加查县', 'Gyaca Xian', 'Gyaca Xian', ',27,321,3090,', '西藏自治区-山南地区-加查县', '321', '02', '04', '01', '3090', '', null, null);
INSERT INTO t_sys_area VALUES ('3091', '542231', '542231', '542231', '隆子县', 'Lhvnze Xian', 'Lhvnze Xian', ',27,321,3091,', '西藏自治区-山南地区-隆子县', '321', '02', '04', '01', '3091', '', null, null);
INSERT INTO t_sys_area VALUES ('3092', '542232', '542232', '542232', '错那县', 'Cona Xian', 'Cona Xian', ',27,321,3092,', '西藏自治区-山南地区-错那县', '321', '02', '04', '01', '3092', '', null, null);
INSERT INTO t_sys_area VALUES ('3093', '542233', '542233', '542233', '浪卡子县', 'Nagarze Xian', 'Nagarze Xian', ',27,321,3093,', '西藏自治区-山南地区-浪卡子县', '321', '02', '04', '01', '3093', '', null, null);
INSERT INTO t_sys_area VALUES ('3094', '542301', '542301', '542301', '日喀则市', 'Xigaze Shi', 'Xigaze Shi', ',27,322,3094,', '西藏自治区-日喀则地区-日喀则市', '322', '02', '04', '01', '3094', '', null, null);
INSERT INTO t_sys_area VALUES ('3095', '542322', '542322', '542322', '南木林县', 'Namling Xian', 'Namling Xian', ',27,322,3095,', '西藏自治区-日喀则地区-南木林县', '322', '02', '04', '01', '3095', '', null, null);
INSERT INTO t_sys_area VALUES ('3096', '542323', '542323', '542323', '江孜县', 'Gyangze Xian', 'Gyangze Xian', ',27,322,3096,', '西藏自治区-日喀则地区-江孜县', '322', '02', '04', '01', '3096', '', null, null);
INSERT INTO t_sys_area VALUES ('3097', '542324', '542324', '542324', '定日县', 'Tingri Xian', 'Tingri Xian', ',27,322,3097,', '西藏自治区-日喀则地区-定日县', '322', '02', '04', '01', '3097', '', null, null);
INSERT INTO t_sys_area VALUES ('3098', '542325', '542325', '542325', '萨迦县', 'Sa,gya Xian', 'Sa,gya Xian', ',27,322,3098,', '西藏自治区-日喀则地区-萨迦县', '322', '02', '04', '01', '3098', '', null, null);
INSERT INTO t_sys_area VALUES ('3099', '542326', '542326', '542326', '拉孜县', 'Lhaze Xian', 'Lhaze Xian', ',27,322,3099,', '西藏自治区-日喀则地区-拉孜县', '322', '02', '04', '01', '3099', '', null, null);
INSERT INTO t_sys_area VALUES ('3100', '542327', '542327', '542327', '昂仁县', 'Ngamring Xian', 'Ngamring Xian', ',27,322,3100,', '西藏自治区-日喀则地区-昂仁县', '322', '02', '04', '01', '3100', '', null, null);
INSERT INTO t_sys_area VALUES ('3101', '542328', '542328', '542328', '谢通门县', 'Xaitongmoin Xian', 'Xaitongmoin Xian', ',27,322,3101,', '西藏自治区-日喀则地区-谢通门县', '322', '02', '04', '01', '3101', '', null, null);
INSERT INTO t_sys_area VALUES ('3102', '542329', '542329', '542329', '白朗县', 'Bainang Xian', 'Bainang Xian', ',27,322,3102,', '西藏自治区-日喀则地区-白朗县', '322', '02', '04', '01', '3102', '', null, null);
INSERT INTO t_sys_area VALUES ('3103', '542330', '542330', '542330', '仁布县', 'Rinbung Xian', 'Rinbung Xian', ',27,322,3103,', '西藏自治区-日喀则地区-仁布县', '322', '02', '04', '01', '3103', '', null, null);
INSERT INTO t_sys_area VALUES ('3104', '542331', '542331', '542331', '康马县', 'Kangmar Xian', 'Kangmar Xian', ',27,322,3104,', '西藏自治区-日喀则地区-康马县', '322', '02', '04', '01', '3104', '', null, null);
INSERT INTO t_sys_area VALUES ('3105', '542332', '542332', '542332', '定结县', 'Dinggye Xian', 'Dinggye Xian', ',27,322,3105,', '西藏自治区-日喀则地区-定结县', '322', '02', '04', '01', '3105', '', null, null);
INSERT INTO t_sys_area VALUES ('3106', '542333', '542333', '542333', '仲巴县', 'Zhongba Xian', 'Zhongba Xian', ',27,322,3106,', '西藏自治区-日喀则地区-仲巴县', '322', '02', '04', '01', '3106', '', null, null);
INSERT INTO t_sys_area VALUES ('3107', '542334', '542334', '542334', '亚东县', 'Yadong(Chomo) Xian', 'Yadong(Chomo) Xian', ',27,322,3107,', '西藏自治区-日喀则地区-亚东县', '322', '02', '04', '01', '3107', '', null, null);
INSERT INTO t_sys_area VALUES ('3108', '542335', '542335', '542335', '吉隆县', 'Gyirong Xian', 'Gyirong Xian', ',27,322,3108,', '西藏自治区-日喀则地区-吉隆县', '322', '02', '04', '01', '3108', '', null, null);
INSERT INTO t_sys_area VALUES ('3109', '542336', '542336', '542336', '聂拉木县', 'Nyalam Xian', 'Nyalam Xian', ',27,322,3109,', '西藏自治区-日喀则地区-聂拉木县', '322', '02', '04', '01', '3109', '', null, null);
INSERT INTO t_sys_area VALUES ('3110', '542337', '542337', '542337', '萨嘎县', 'Saga Xian', 'Saga Xian', ',27,322,3110,', '西藏自治区-日喀则地区-萨嘎县', '322', '02', '04', '01', '3110', '', null, null);
INSERT INTO t_sys_area VALUES ('3111', '542338', '542338', '542338', '岗巴县', 'Gamba Xian', 'Gamba Xian', ',27,322,3111,', '西藏自治区-日喀则地区-岗巴县', '322', '02', '04', '01', '3111', '', null, null);
INSERT INTO t_sys_area VALUES ('3112', '542421', '542421', '542421', '那曲县', 'Nagqu Xian', 'Nagqu Xian', ',27,323,3112,', '西藏自治区-那曲地区-那曲县', '323', '02', '04', '01', '3112', '', null, null);
INSERT INTO t_sys_area VALUES ('3113', '542422', '542422', '542422', '嘉黎县', 'Lhari Xian', 'Lhari Xian', ',27,323,3113,', '西藏自治区-那曲地区-嘉黎县', '323', '02', '04', '01', '3113', '', null, null);
INSERT INTO t_sys_area VALUES ('3114', '542423', '542423', '542423', '比如县', 'Biru Xian', 'Biru Xian', ',27,323,3114,', '西藏自治区-那曲地区-比如县', '323', '02', '04', '01', '3114', '', null, null);
INSERT INTO t_sys_area VALUES ('3115', '542424', '542424', '542424', '聂荣县', 'Nyainrong Xian', 'Nyainrong Xian', ',27,323,3115,', '西藏自治区-那曲地区-聂荣县', '323', '02', '04', '01', '3115', '', null, null);
INSERT INTO t_sys_area VALUES ('3116', '542425', '542425', '542425', '安多县', 'Amdo Xian', 'Amdo Xian', ',27,323,3116,', '西藏自治区-那曲地区-安多县', '323', '02', '04', '01', '3116', '', null, null);
INSERT INTO t_sys_area VALUES ('3117', '542426', '542426', '542426', '申扎县', 'Xainza Xian', 'Xainza Xian', ',27,323,3117,', '西藏自治区-那曲地区-申扎县', '323', '02', '04', '01', '3117', '', null, null);
INSERT INTO t_sys_area VALUES ('3118', '542427', '542427', '542427', '索县', 'Sog Xian', 'Sog Xian', ',27,323,3118,', '西藏自治区-那曲地区-索县', '323', '02', '04', '01', '3118', '', null, null);
INSERT INTO t_sys_area VALUES ('3119', '542428', '542428', '542428', '班戈县', 'Bangoin Xian', 'Bangoin Xian', ',27,323,3119,', '西藏自治区-那曲地区-班戈县', '323', '02', '04', '01', '3119', '', null, null);
INSERT INTO t_sys_area VALUES ('3120', '542429', '542429', '542429', '巴青县', 'Baqen Xian', 'Baqen Xian', ',27,323,3120,', '西藏自治区-那曲地区-巴青县', '323', '02', '04', '01', '3120', '', null, null);
INSERT INTO t_sys_area VALUES ('3121', '542430', '542430', '542430', '尼玛县', 'Nyima Xian', 'Nyima Xian', ',27,323,3121,', '西藏自治区-那曲地区-尼玛县', '323', '02', '04', '01', '3121', '', null, null);
INSERT INTO t_sys_area VALUES ('3122', '542521', '542521', '542521', '普兰县', 'Burang Xian', 'Burang Xian', ',27,324,3122,', '西藏自治区-阿里地区-普兰县', '324', '02', '04', '01', '3122', '', null, null);
INSERT INTO t_sys_area VALUES ('3123', '542522', '542522', '542522', '札达县', 'Zanda Xian', 'Zanda Xian', ',27,324,3123,', '西藏自治区-阿里地区-札达县', '324', '02', '04', '01', '3123', '', null, null);
INSERT INTO t_sys_area VALUES ('3124', '542523', '542523', '542523', '噶尔县', 'Gar Xian', 'Gar Xian', ',27,324,3124,', '西藏自治区-阿里地区-噶尔县', '324', '02', '04', '01', '3124', '', null, null);
INSERT INTO t_sys_area VALUES ('3125', '542524', '542524', '542524', '日土县', 'Rutog Xian', 'Rutog Xian', ',27,324,3125,', '西藏自治区-阿里地区-日土县', '324', '02', '04', '01', '3125', '', null, null);
INSERT INTO t_sys_area VALUES ('3126', '542525', '542525', '542525', '革吉县', 'Ge,gyai Xian', 'Ge,gyai Xian', ',27,324,3126,', '西藏自治区-阿里地区-革吉县', '324', '02', '04', '01', '3126', '', null, null);
INSERT INTO t_sys_area VALUES ('3127', '542526', '542526', '542526', '改则县', 'Gerze Xian', 'Gerze Xian', ',27,324,3127,', '西藏自治区-阿里地区-改则县', '324', '02', '04', '01', '3127', '', null, null);
INSERT INTO t_sys_area VALUES ('3128', '542527', '542527', '542527', '措勤县', 'Coqen Xian', 'Coqen Xian', ',27,324,3128,', '西藏自治区-阿里地区-措勤县', '324', '02', '04', '01', '3128', '', null, null);
INSERT INTO t_sys_area VALUES ('3129', '542621', '542621', '542621', '林芝县', 'Nyingchi Xian', 'Nyingchi Xian', ',27,325,3129,', '西藏自治区-林芝地区-林芝县', '325', '02', '04', '01', '3129', '', null, null);
INSERT INTO t_sys_area VALUES ('3130', '542622', '542622', '542622', '工布江达县', 'Gongbo,gyamda Xian', 'Gongbo,gyamda Xian', ',27,325,3130,', '西藏自治区-林芝地区-工布江达县', '325', '02', '04', '01', '3130', '', null, null);
INSERT INTO t_sys_area VALUES ('3131', '542623', '542623', '542623', '米林县', 'Mainling Xian', 'Mainling Xian', ',27,325,3131,', '西藏自治区-林芝地区-米林县', '325', '02', '04', '01', '3131', '', null, null);
INSERT INTO t_sys_area VALUES ('3132', '542624', '542624', '542624', '墨脱县', 'Metog Xian', 'Metog Xian', ',27,325,3132,', '西藏自治区-林芝地区-墨脱县', '325', '02', '04', '01', '3132', '', null, null);
INSERT INTO t_sys_area VALUES ('3133', '542625', '542625', '542625', '波密县', 'Bomi(Bowo) Xian', 'Bomi(Bowo) Xian', ',27,325,3133,', '西藏自治区-林芝地区-波密县', '325', '02', '04', '01', '3133', '', null, null);
INSERT INTO t_sys_area VALUES ('3134', '542626', '542626', '542626', '察隅县', 'Zayv Xian', 'Zayv Xian', ',27,325,3134,', '西藏自治区-林芝地区-察隅县', '325', '02', '04', '01', '3134', '', null, null);
INSERT INTO t_sys_area VALUES ('3135', '542627', '542627', '542627', '朗县', 'Nang Xian', 'Nang Xian', ',27,325,3135,', '西藏自治区-林芝地区-朗县', '325', '02', '04', '01', '3135', '', null, null);
INSERT INTO t_sys_area VALUES ('3136', '610101', '610101', '610101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',28,326,3136,', '陕西省-西安市-市辖区', '326', '02', '04', '01', '3136', '', null, null);
INSERT INTO t_sys_area VALUES ('3137', '610102', '610102', '610102', '新城区', 'Xincheng Qu', 'Xincheng Qu', ',28,326,3137,', '陕西省-西安市-新城区', '326', '02', '04', '01', '3137', '', null, null);
INSERT INTO t_sys_area VALUES ('3138', '610103', '610103', '610103', '碑林区', 'Beilin Qu', 'Beilin Qu', ',28,326,3138,', '陕西省-西安市-碑林区', '326', '02', '04', '01', '3138', '', null, null);
INSERT INTO t_sys_area VALUES ('3139', '610104', '610104', '610104', '莲湖区', 'Lianhu Qu', 'Lianhu Qu', ',28,326,3139,', '陕西省-西安市-莲湖区', '326', '02', '04', '01', '3139', '', null, null);
INSERT INTO t_sys_area VALUES ('3140', '610111', '610111', '610111', '灞桥区', 'Baqiao Qu', 'Baqiao Qu', ',28,326,3140,', '陕西省-西安市-灞桥区', '326', '02', '04', '01', '3140', '', null, null);
INSERT INTO t_sys_area VALUES ('3141', '610112', '610112', '610112', '未央区', 'Weiyang Qu', 'Weiyang Qu', ',28,326,3141,', '陕西省-西安市-未央区', '326', '02', '04', '01', '3141', '', null, null);
INSERT INTO t_sys_area VALUES ('3142', '610113', '610113', '610113', '雁塔区', 'Yanta Qu', 'Yanta Qu', ',28,326,3142,', '陕西省-西安市-雁塔区', '326', '02', '04', '01', '3142', '', null, null);
INSERT INTO t_sys_area VALUES ('3143', '610114', '610114', '610114', '阎良区', 'Yanliang Qu', 'Yanliang Qu', ',28,326,3143,', '陕西省-西安市-阎良区', '326', '02', '04', '01', '3143', '', null, null);
INSERT INTO t_sys_area VALUES ('3144', '610115', '610115', '610115', '临潼区', 'Lintong Qu', 'Lintong Qu', ',28,326,3144,', '陕西省-西安市-临潼区', '326', '02', '04', '01', '3144', '', null, null);
INSERT INTO t_sys_area VALUES ('3145', '610116', '610116', '610116', '长安区', 'Changan Qu', 'Changan Qu', ',28,326,3145,', '陕西省-西安市-长安区', '326', '02', '04', '01', '3145', '', null, null);
INSERT INTO t_sys_area VALUES ('3146', '610122', '610122', '610122', '蓝田县', 'Lantian Xian', 'Lantian Xian', ',28,326,3146,', '陕西省-西安市-蓝田县', '326', '02', '04', '01', '3146', '', null, null);
INSERT INTO t_sys_area VALUES ('3147', '610124', '610124', '610124', '周至县', 'Zhouzhi Xian', 'Zhouzhi Xian', ',28,326,3147,', '陕西省-西安市-周至县', '326', '02', '04', '01', '3147', '', null, null);
INSERT INTO t_sys_area VALUES ('3148', '610125', '610125', '610125', '户县', 'Hu Xian', 'Hu Xian', ',28,326,3148,', '陕西省-西安市-户县', '326', '02', '04', '01', '3148', '', null, null);
INSERT INTO t_sys_area VALUES ('3149', '610126', '610126', '610126', '高陵县', 'Gaoling Xian', 'Gaoling Xian', ',28,326,3149,', '陕西省-西安市-高陵县', '326', '02', '04', '01', '3149', '', null, null);
INSERT INTO t_sys_area VALUES ('3150', '610201', '610201', '610201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',28,327,3150,', '陕西省-铜川市-市辖区', '327', '02', '04', '01', '3150', '', null, null);
INSERT INTO t_sys_area VALUES ('3151', '610202', '610202', '610202', '王益区', 'Wangyi Qu', 'Wangyi Qu', ',28,327,3151,', '陕西省-铜川市-王益区', '327', '02', '04', '01', '3151', '', null, null);
INSERT INTO t_sys_area VALUES ('3152', '610203', '610203', '610203', '印台区', 'Yintai Qu', 'Yintai Qu', ',28,327,3152,', '陕西省-铜川市-印台区', '327', '02', '04', '01', '3152', '', null, null);
INSERT INTO t_sys_area VALUES ('3153', '610204', '610204', '610204', '耀州区', 'Yaozhou Qu', 'Yaozhou Qu', ',28,327,3153,', '陕西省-铜川市-耀州区', '327', '02', '04', '01', '3153', '', null, null);
INSERT INTO t_sys_area VALUES ('3154', '610222', '610222', '610222', '宜君县', 'Yijun Xian', 'Yijun Xian', ',28,327,3154,', '陕西省-铜川市-宜君县', '327', '02', '04', '01', '3154', '', null, null);
INSERT INTO t_sys_area VALUES ('3155', '610301', '610301', '610301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',28,328,3155,', '陕西省-宝鸡市-市辖区', '328', '02', '04', '01', '3155', '', null, null);
INSERT INTO t_sys_area VALUES ('3156', '610302', '610302', '610302', '渭滨区', 'Weibin Qu', 'Weibin Qu', ',28,328,3156,', '陕西省-宝鸡市-渭滨区', '328', '02', '04', '01', '3156', '', null, null);
INSERT INTO t_sys_area VALUES ('3157', '610303', '610303', '610303', '金台区', 'Jintai Qu', 'Jintai Qu', ',28,328,3157,', '陕西省-宝鸡市-金台区', '328', '02', '04', '01', '3157', '', null, null);
INSERT INTO t_sys_area VALUES ('3158', '610304', '610304', '610304', '陈仓区', 'Chencang Qu', 'Chencang Qu', ',28,328,3158,', '陕西省-宝鸡市-陈仓区', '328', '02', '04', '01', '3158', '', null, null);
INSERT INTO t_sys_area VALUES ('3159', '610322', '610322', '610322', '凤翔县', 'Fengxiang Xian', 'Fengxiang Xian', ',28,328,3159,', '陕西省-宝鸡市-凤翔县', '328', '02', '04', '01', '3159', '', null, null);
INSERT INTO t_sys_area VALUES ('3160', '610323', '610323', '610323', '岐山县', 'Qishan Xian', 'Qishan Xian', ',28,328,3160,', '陕西省-宝鸡市-岐山县', '328', '02', '04', '01', '3160', '', null, null);
INSERT INTO t_sys_area VALUES ('3161', '610324', '610324', '610324', '扶风县', 'Fufeng Xian', 'Fufeng Xian', ',28,328,3161,', '陕西省-宝鸡市-扶风县', '328', '02', '04', '01', '3161', '', null, null);
INSERT INTO t_sys_area VALUES ('3162', '610326', '610326', '610326', '眉县', 'Mei Xian', 'Mei Xian', ',28,328,3162,', '陕西省-宝鸡市-眉县', '328', '02', '04', '01', '3162', '', null, null);
INSERT INTO t_sys_area VALUES ('3163', '610327', '610327', '610327', '陇县', 'Long Xian', 'Long Xian', ',28,328,3163,', '陕西省-宝鸡市-陇县', '328', '02', '04', '01', '3163', '', null, null);
INSERT INTO t_sys_area VALUES ('3164', '610328', '610328', '610328', '千阳县', 'Qianyang Xian', 'Qianyang Xian', ',28,328,3164,', '陕西省-宝鸡市-千阳县', '328', '02', '04', '01', '3164', '', null, null);
INSERT INTO t_sys_area VALUES ('3165', '610329', '610329', '610329', '麟游县', 'Linyou Xian', 'Linyou Xian', ',28,328,3165,', '陕西省-宝鸡市-麟游县', '328', '02', '04', '01', '3165', '', null, null);
INSERT INTO t_sys_area VALUES ('3166', '610330', '610330', '610330', '凤县', 'Feng Xian', 'Feng Xian', ',28,328,3166,', '陕西省-宝鸡市-凤县', '328', '02', '04', '01', '3166', '', null, null);
INSERT INTO t_sys_area VALUES ('3167', '610331', '610331', '610331', '太白县', 'Taibai Xian', 'Taibai Xian', ',28,328,3167,', '陕西省-宝鸡市-太白县', '328', '02', '04', '01', '3167', '', null, null);
INSERT INTO t_sys_area VALUES ('3168', '610401', '610401', '610401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',28,329,3168,', '陕西省-咸阳市-市辖区', '329', '02', '04', '01', '3168', '', null, null);
INSERT INTO t_sys_area VALUES ('3169', '610402', '610402', '610402', '秦都区', 'Qindu Qu', 'Qindu Qu', ',28,329,3169,', '陕西省-咸阳市-秦都区', '329', '02', '04', '01', '3169', '', null, null);
INSERT INTO t_sys_area VALUES ('3170', '610403', '610403', '610403', '杨陵区', 'Yangling Qu', 'Yangling Qu', ',28,329,3170,', '陕西省-咸阳市-杨陵区', '329', '02', '04', '01', '3170', '', null, null);
INSERT INTO t_sys_area VALUES ('3171', '610404', '610404', '610404', '渭城区', 'Weicheng Qu', 'Weicheng Qu', ',28,329,3171,', '陕西省-咸阳市-渭城区', '329', '02', '04', '01', '3171', '', null, null);
INSERT INTO t_sys_area VALUES ('3172', '610422', '610422', '610422', '三原县', 'Sanyuan Xian', 'Sanyuan Xian', ',28,329,3172,', '陕西省-咸阳市-三原县', '329', '02', '04', '01', '3172', '', null, null);
INSERT INTO t_sys_area VALUES ('3173', '610423', '610423', '610423', '泾阳县', 'Jingyang Xian', 'Jingyang Xian', ',28,329,3173,', '陕西省-咸阳市-泾阳县', '329', '02', '04', '01', '3173', '', null, null);
INSERT INTO t_sys_area VALUES ('3174', '610424', '610424', '610424', '乾县', 'Qian Xian', 'Qian Xian', ',28,329,3174,', '陕西省-咸阳市-乾县', '329', '02', '04', '01', '3174', '', null, null);
INSERT INTO t_sys_area VALUES ('3175', '610425', '610425', '610425', '礼泉县', 'Liquan Xian', 'Liquan Xian', ',28,329,3175,', '陕西省-咸阳市-礼泉县', '329', '02', '04', '01', '3175', '', null, null);
INSERT INTO t_sys_area VALUES ('3176', '610426', '610426', '610426', '永寿县', 'Yongshou Xian', 'Yongshou Xian', ',28,329,3176,', '陕西省-咸阳市-永寿县', '329', '02', '04', '01', '3176', '', null, null);
INSERT INTO t_sys_area VALUES ('3177', '610427', '610427', '610427', '彬县', 'Bin Xian', 'Bin Xian', ',28,329,3177,', '陕西省-咸阳市-彬县', '329', '02', '04', '01', '3177', '', null, null);
INSERT INTO t_sys_area VALUES ('3178', '610428', '610428', '610428', '长武县', 'Changwu Xian', 'Changwu Xian', ',28,329,3178,', '陕西省-咸阳市-长武县', '329', '02', '04', '01', '3178', '', null, null);
INSERT INTO t_sys_area VALUES ('3179', '610429', '610429', '610429', '旬邑县', 'Xunyi Xian', 'Xunyi Xian', ',28,329,3179,', '陕西省-咸阳市-旬邑县', '329', '02', '04', '01', '3179', '', null, null);
INSERT INTO t_sys_area VALUES ('3180', '610430', '610430', '610430', '淳化县', 'Chunhua Xian', 'Chunhua Xian', ',28,329,3180,', '陕西省-咸阳市-淳化县', '329', '02', '04', '01', '3180', '', null, null);
INSERT INTO t_sys_area VALUES ('3181', '610431', '610431', '610431', '武功县', 'Wugong Xian', 'Wugong Xian', ',28,329,3181,', '陕西省-咸阳市-武功县', '329', '02', '04', '01', '3181', '', null, null);
INSERT INTO t_sys_area VALUES ('3182', '610481', '610481', '610481', '兴平市', 'Xingping Shi', 'Xingping Shi', ',28,329,3182,', '陕西省-咸阳市-兴平市', '329', '02', '04', '01', '3182', '', null, null);
INSERT INTO t_sys_area VALUES ('3183', '610501', '610501', '610501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',28,330,3183,', '陕西省-渭南市-市辖区', '330', '02', '04', '01', '3183', '', null, null);
INSERT INTO t_sys_area VALUES ('3184', '610502', '610502', '610502', '临渭区', 'Linwei Qu', 'Linwei Qu', ',28,330,3184,', '陕西省-渭南市-临渭区', '330', '02', '04', '01', '3184', '', null, null);
INSERT INTO t_sys_area VALUES ('3185', '610521', '610521', '610521', '华县', 'Hua Xian', 'Hua Xian', ',28,330,3185,', '陕西省-渭南市-华县', '330', '02', '04', '01', '3185', '', null, null);
INSERT INTO t_sys_area VALUES ('3186', '610522', '610522', '610522', '潼关县', 'Tongguan Xian', 'Tongguan Xian', ',28,330,3186,', '陕西省-渭南市-潼关县', '330', '02', '04', '01', '3186', '', null, null);
INSERT INTO t_sys_area VALUES ('3187', '610523', '610523', '610523', '大荔县', 'Dali Xian', 'Dali Xian', ',28,330,3187,', '陕西省-渭南市-大荔县', '330', '02', '04', '01', '3187', '', null, null);
INSERT INTO t_sys_area VALUES ('3188', '610524', '610524', '610524', '合阳县', 'Heyang Xian', 'Heyang Xian', ',28,330,3188,', '陕西省-渭南市-合阳县', '330', '02', '04', '01', '3188', '', null, null);
INSERT INTO t_sys_area VALUES ('3189', '610525', '610525', '610525', '澄城县', 'Chengcheng Xian', 'Chengcheng Xian', ',28,330,3189,', '陕西省-渭南市-澄城县', '330', '02', '04', '01', '3189', '', null, null);
INSERT INTO t_sys_area VALUES ('3190', '610526', '610526', '610526', '蒲城县', 'Pucheng Xian', 'Pucheng Xian', ',28,330,3190,', '陕西省-渭南市-蒲城县', '330', '02', '04', '01', '3190', '', null, null);
INSERT INTO t_sys_area VALUES ('3191', '610527', '610527', '610527', '白水县', 'Baishui Xian', 'Baishui Xian', ',28,330,3191,', '陕西省-渭南市-白水县', '330', '02', '04', '01', '3191', '', null, null);
INSERT INTO t_sys_area VALUES ('3192', '610528', '610528', '610528', '富平县', 'Fuping Xian', 'Fuping Xian', ',28,330,3192,', '陕西省-渭南市-富平县', '330', '02', '04', '01', '3192', '', null, null);
INSERT INTO t_sys_area VALUES ('3193', '610581', '610581', '610581', '韩城市', 'Hancheng Shi', 'Hancheng Shi', ',28,330,3193,', '陕西省-渭南市-韩城市', '330', '02', '04', '01', '3193', '', null, null);
INSERT INTO t_sys_area VALUES ('3194', '610582', '610582', '610582', '华阴市', 'Huayin Shi', 'Huayin Shi', ',28,330,3194,', '陕西省-渭南市-华阴市', '330', '02', '04', '01', '3194', '', null, null);
INSERT INTO t_sys_area VALUES ('3195', '610601', '610601', '610601', '市辖区', 'Shixiaqu', 'Shixiaqu', ',28,331,3195,', '陕西省-延安市-市辖区', '331', '02', '04', '01', '3195', '', null, null);
INSERT INTO t_sys_area VALUES ('3196', '610602', '610602', '610602', '宝塔区', 'Baota Qu', 'Baota Qu', ',28,331,3196,', '陕西省-延安市-宝塔区', '331', '02', '04', '01', '3196', '', null, null);
INSERT INTO t_sys_area VALUES ('3197', '610621', '610621', '610621', '延长县', 'Yanchang Xian', 'Yanchang Xian', ',28,331,3197,', '陕西省-延安市-延长县', '331', '02', '04', '01', '3197', '', null, null);
INSERT INTO t_sys_area VALUES ('3198', '610622', '610622', '610622', '延川县', 'Yanchuan Xian', 'Yanchuan Xian', ',28,331,3198,', '陕西省-延安市-延川县', '331', '02', '04', '01', '3198', '', null, null);
INSERT INTO t_sys_area VALUES ('3199', '610623', '610623', '610623', '子长县', 'Zichang Xian', 'Zichang Xian', ',28,331,3199,', '陕西省-延安市-子长县', '331', '02', '04', '01', '3199', '', null, null);
INSERT INTO t_sys_area VALUES ('3200', '610624', '610624', '610624', '安塞县', 'Ansai Xian', 'Ansai Xian', ',28,331,3200,', '陕西省-延安市-安塞县', '331', '02', '04', '01', '3200', '', null, null);
INSERT INTO t_sys_area VALUES ('3201', '610625', '610625', '610625', '志丹县', 'Zhidan Xian', 'Zhidan Xian', ',28,331,3201,', '陕西省-延安市-志丹县', '331', '02', '04', '01', '3201', '', null, null);
INSERT INTO t_sys_area VALUES ('3202', '610626', '610626', '610626', '吴起县', 'Wuqi Xian', 'Wuqi Xian', ',28,331,3202,', '陕西省-延安市-吴起县', '331', '02', '04', '01', '3202', '', null, null);
INSERT INTO t_sys_area VALUES ('3203', '610627', '610627', '610627', '甘泉县', 'Ganquan Xian', 'Ganquan Xian', ',28,331,3203,', '陕西省-延安市-甘泉县', '331', '02', '04', '01', '3203', '', null, null);
INSERT INTO t_sys_area VALUES ('3204', '610628', '610628', '610628', '富县', 'Fu Xian', 'Fu Xian', ',28,331,3204,', '陕西省-延安市-富县', '331', '02', '04', '01', '3204', '', null, null);
INSERT INTO t_sys_area VALUES ('3205', '610629', '610629', '610629', '洛川县', 'Luochuan Xian', 'Luochuan Xian', ',28,331,3205,', '陕西省-延安市-洛川县', '331', '02', '04', '01', '3205', '', null, null);
INSERT INTO t_sys_area VALUES ('3206', '610630', '610630', '610630', '宜川县', 'Yichuan Xian', 'Yichuan Xian', ',28,331,3206,', '陕西省-延安市-宜川县', '331', '02', '04', '01', '3206', '', null, null);
INSERT INTO t_sys_area VALUES ('3207', '610631', '610631', '610631', '黄龙县', 'Huanglong Xian', 'Huanglong Xian', ',28,331,3207,', '陕西省-延安市-黄龙县', '331', '02', '04', '01', '3207', '', null, null);
INSERT INTO t_sys_area VALUES ('3208', '610632', '610632', '610632', '黄陵县', 'Huangling Xian', 'Huangling Xian', ',28,331,3208,', '陕西省-延安市-黄陵县', '331', '02', '04', '01', '3208', '', null, null);
INSERT INTO t_sys_area VALUES ('3209', '610701', '610701', '610701', '市辖区', 'Shixiaqu', 'Shixiaqu', ',28,332,3209,', '陕西省-汉中市-市辖区', '332', '02', '04', '01', '3209', '', null, null);
INSERT INTO t_sys_area VALUES ('3210', '610702', '610702', '610702', '汉台区', 'Hantai Qu', 'Hantai Qu', ',28,332,3210,', '陕西省-汉中市-汉台区', '332', '02', '04', '01', '3210', '', null, null);
INSERT INTO t_sys_area VALUES ('3211', '610721', '610721', '610721', '南郑县', 'Nanzheng Xian', 'Nanzheng Xian', ',28,332,3211,', '陕西省-汉中市-南郑县', '332', '02', '04', '01', '3211', '', null, null);
INSERT INTO t_sys_area VALUES ('3212', '610722', '610722', '610722', '城固县', 'Chenggu Xian', 'Chenggu Xian', ',28,332,3212,', '陕西省-汉中市-城固县', '332', '02', '04', '01', '3212', '', null, null);
INSERT INTO t_sys_area VALUES ('3213', '610723', '610723', '610723', '洋县', 'Yang Xian', 'Yang Xian', ',28,332,3213,', '陕西省-汉中市-洋县', '332', '02', '04', '01', '3213', '', null, null);
INSERT INTO t_sys_area VALUES ('3214', '610724', '610724', '610724', '西乡县', 'Xixiang Xian', 'Xixiang Xian', ',28,332,3214,', '陕西省-汉中市-西乡县', '332', '02', '04', '01', '3214', '', null, null);
INSERT INTO t_sys_area VALUES ('3215', '610725', '610725', '610725', '勉县', 'Mian Xian', 'Mian Xian', ',28,332,3215,', '陕西省-汉中市-勉县', '332', '02', '04', '01', '3215', '', null, null);
INSERT INTO t_sys_area VALUES ('3216', '610726', '610726', '610726', '宁强县', 'Ningqiang Xian', 'Ningqiang Xian', ',28,332,3216,', '陕西省-汉中市-宁强县', '332', '02', '04', '01', '3216', '', null, null);
INSERT INTO t_sys_area VALUES ('3217', '610727', '610727', '610727', '略阳县', 'Lueyang Xian', 'Lueyang Xian', ',28,332,3217,', '陕西省-汉中市-略阳县', '332', '02', '04', '01', '3217', '', null, null);
INSERT INTO t_sys_area VALUES ('3218', '610728', '610728', '610728', '镇巴县', 'Zhenba Xian', 'Zhenba Xian', ',28,332,3218,', '陕西省-汉中市-镇巴县', '332', '02', '04', '01', '3218', '', null, null);
INSERT INTO t_sys_area VALUES ('3219', '610729', '610729', '610729', '留坝县', 'Liuba Xian', 'Liuba Xian', ',28,332,3219,', '陕西省-汉中市-留坝县', '332', '02', '04', '01', '3219', '', null, null);
INSERT INTO t_sys_area VALUES ('3220', '610730', '610730', '610730', '佛坪县', 'Foping Xian', 'Foping Xian', ',28,332,3220,', '陕西省-汉中市-佛坪县', '332', '02', '04', '01', '3220', '', null, null);
INSERT INTO t_sys_area VALUES ('3221', '610801', '610801', '610801', '市辖区', '1', '1', ',28,333,3221,', '陕西省-榆林市-市辖区', '333', '02', '04', '01', '3221', '', null, null);
INSERT INTO t_sys_area VALUES ('3222', '610802', '610802', '610802', '榆阳区', 'Yuyang Qu', 'Yuyang Qu', ',28,333,3222,', '陕西省-榆林市-榆阳区', '333', '02', '04', '01', '3222', '', null, null);
INSERT INTO t_sys_area VALUES ('3223', '610821', '610821', '610821', '神木县', 'Shenmu Xian', 'Shenmu Xian', ',28,333,3223,', '陕西省-榆林市-神木县', '333', '02', '04', '01', '3223', '', null, null);
INSERT INTO t_sys_area VALUES ('3224', '610822', '610822', '610822', '府谷县', 'Fugu Xian', 'Fugu Xian', ',28,333,3224,', '陕西省-榆林市-府谷县', '333', '02', '04', '01', '3224', '', null, null);
INSERT INTO t_sys_area VALUES ('3225', '610823', '610823', '610823', '横山县', 'Hengshan Xian', 'Hengshan Xian', ',28,333,3225,', '陕西省-榆林市-横山县', '333', '02', '04', '01', '3225', '', null, null);
INSERT INTO t_sys_area VALUES ('3226', '610824', '610824', '610824', '靖边县', 'Jingbian Xian', 'Jingbian Xian', ',28,333,3226,', '陕西省-榆林市-靖边县', '333', '02', '04', '01', '3226', '', null, null);
INSERT INTO t_sys_area VALUES ('3227', '610825', '610825', '610825', '定边县', 'Dingbian Xian', 'Dingbian Xian', ',28,333,3227,', '陕西省-榆林市-定边县', '333', '02', '04', '01', '3227', '', null, null);
INSERT INTO t_sys_area VALUES ('3228', '610826', '610826', '610826', '绥德县', 'Suide Xian', 'Suide Xian', ',28,333,3228,', '陕西省-榆林市-绥德县', '333', '02', '04', '01', '3228', '', null, null);
INSERT INTO t_sys_area VALUES ('3229', '610827', '610827', '610827', '米脂县', 'Mizhi Xian', 'Mizhi Xian', ',28,333,3229,', '陕西省-榆林市-米脂县', '333', '02', '04', '01', '3229', '', null, null);
INSERT INTO t_sys_area VALUES ('3230', '610828', '610828', '610828', '佳县', 'Jia Xian', 'Jia Xian', ',28,333,3230,', '陕西省-榆林市-佳县', '333', '02', '04', '01', '3230', '', null, null);
INSERT INTO t_sys_area VALUES ('3231', '610829', '610829', '610829', '吴堡县', 'Wubu Xian', 'Wubu Xian', ',28,333,3231,', '陕西省-榆林市-吴堡县', '333', '02', '04', '01', '3231', '', null, null);
INSERT INTO t_sys_area VALUES ('3232', '610830', '610830', '610830', '清涧县', 'Qingjian Xian', 'Qingjian Xian', ',28,333,3232,', '陕西省-榆林市-清涧县', '333', '02', '04', '01', '3232', '', null, null);
INSERT INTO t_sys_area VALUES ('3233', '610831', '610831', '610831', '子洲县', 'Zizhou Xian', 'Zizhou Xian', ',28,333,3233,', '陕西省-榆林市-子洲县', '333', '02', '04', '01', '3233', '', null, null);
INSERT INTO t_sys_area VALUES ('3234', '610901', '610901', '610901', '市辖区', '1', '1', ',28,334,3234,', '陕西省-安康市-市辖区', '334', '02', '04', '01', '3234', '', null, null);
INSERT INTO t_sys_area VALUES ('3235', '610902', '610902', '610902', '汉滨区', 'Hanbin Qu', 'Hanbin Qu', ',28,334,3235,', '陕西省-安康市-汉滨区', '334', '02', '04', '01', '3235', '', null, null);
INSERT INTO t_sys_area VALUES ('3236', '610921', '610921', '610921', '汉阴县', 'Hanyin Xian', 'Hanyin Xian', ',28,334,3236,', '陕西省-安康市-汉阴县', '334', '02', '04', '01', '3236', '', null, null);
INSERT INTO t_sys_area VALUES ('3237', '610922', '610922', '610922', '石泉县', 'Shiquan Xian', 'Shiquan Xian', ',28,334,3237,', '陕西省-安康市-石泉县', '334', '02', '04', '01', '3237', '', null, null);
INSERT INTO t_sys_area VALUES ('3238', '610923', '610923', '610923', '宁陕县', 'Ningshan Xian', 'Ningshan Xian', ',28,334,3238,', '陕西省-安康市-宁陕县', '334', '02', '04', '01', '3238', '', null, null);
INSERT INTO t_sys_area VALUES ('3239', '610924', '610924', '610924', '紫阳县', 'Ziyang Xian', 'Ziyang Xian', ',28,334,3239,', '陕西省-安康市-紫阳县', '334', '02', '04', '01', '3239', '', null, null);
INSERT INTO t_sys_area VALUES ('3240', '610925', '610925', '610925', '岚皋县', 'Langao Xian', 'Langao Xian', ',28,334,3240,', '陕西省-安康市-岚皋县', '334', '02', '04', '01', '3240', '', null, null);
INSERT INTO t_sys_area VALUES ('3241', '610926', '610926', '610926', '平利县', 'Pingli Xian', 'Pingli Xian', ',28,334,3241,', '陕西省-安康市-平利县', '334', '02', '04', '01', '3241', '', null, null);
INSERT INTO t_sys_area VALUES ('3242', '610927', '610927', '610927', '镇坪县', 'Zhenping Xian', 'Zhenping Xian', ',28,334,3242,', '陕西省-安康市-镇坪县', '334', '02', '04', '01', '3242', '', null, null);
INSERT INTO t_sys_area VALUES ('3243', '610928', '610928', '610928', '旬阳县', 'Xunyang Xian', 'Xunyang Xian', ',28,334,3243,', '陕西省-安康市-旬阳县', '334', '02', '04', '01', '3243', '', null, null);
INSERT INTO t_sys_area VALUES ('3244', '610929', '610929', '610929', '白河县', 'Baihe Xian', 'Baihe Xian', ',28,334,3244,', '陕西省-安康市-白河县', '334', '02', '04', '01', '3244', '', null, null);
INSERT INTO t_sys_area VALUES ('3245', '611001', '611001', '611001', '市辖区', '1', '1', ',28,335,3245,', '陕西省-商洛市-市辖区', '335', '02', '04', '01', '3245', '', null, null);
INSERT INTO t_sys_area VALUES ('3246', '611002', '611002', '611002', '商州区', 'Shangzhou Qu', 'Shangzhou Qu', ',28,335,3246,', '陕西省-商洛市-商州区', '335', '02', '04', '01', '3246', '', null, null);
INSERT INTO t_sys_area VALUES ('3247', '611021', '611021', '611021', '洛南县', 'Luonan Xian', 'Luonan Xian', ',28,335,3247,', '陕西省-商洛市-洛南县', '335', '02', '04', '01', '3247', '', null, null);
INSERT INTO t_sys_area VALUES ('3248', '611022', '611022', '611022', '丹凤县', 'Danfeng Xian', 'Danfeng Xian', ',28,335,3248,', '陕西省-商洛市-丹凤县', '335', '02', '04', '01', '3248', '', null, null);
INSERT INTO t_sys_area VALUES ('3249', '611023', '611023', '611023', '商南县', 'Shangnan Xian', 'Shangnan Xian', ',28,335,3249,', '陕西省-商洛市-商南县', '335', '02', '04', '01', '3249', '', null, null);
INSERT INTO t_sys_area VALUES ('3250', '611024', '611024', '611024', '山阳县', 'Shanyang Xian', 'Shanyang Xian', ',28,335,3250,', '陕西省-商洛市-山阳县', '335', '02', '04', '01', '3250', '', null, null);
INSERT INTO t_sys_area VALUES ('3251', '611025', '611025', '611025', '镇安县', 'Zhen,an Xian', 'Zhen,an Xian', ',28,335,3251,', '陕西省-商洛市-镇安县', '335', '02', '04', '01', '3251', '', null, null);
INSERT INTO t_sys_area VALUES ('3252', '611026', '611026', '611026', '柞水县', 'Zhashui Xian', 'Zhashui Xian', ',28,335,3252,', '陕西省-商洛市-柞水县', '335', '02', '04', '01', '3252', '', null, null);
INSERT INTO t_sys_area VALUES ('3253', '620101', '620101', '620101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',29,336,3253,', '甘肃省-兰州市-市辖区', '336', '02', '04', '01', '3253', '', null, null);
INSERT INTO t_sys_area VALUES ('3254', '620102', '620102', '620102', '城关区', 'Chengguan Qu', 'Chengguan Qu', ',29,336,3254,', '甘肃省-兰州市-城关区', '336', '02', '04', '01', '3254', '', null, null);
INSERT INTO t_sys_area VALUES ('3255', '620103', '620103', '620103', '七里河区', 'Qilihe Qu', 'Qilihe Qu', ',29,336,3255,', '甘肃省-兰州市-七里河区', '336', '02', '04', '01', '3255', '', null, null);
INSERT INTO t_sys_area VALUES ('3256', '620104', '620104', '620104', '西固区', 'Xigu Qu', 'Xigu Qu', ',29,336,3256,', '甘肃省-兰州市-西固区', '336', '02', '04', '01', '3256', '', null, null);
INSERT INTO t_sys_area VALUES ('3257', '620105', '620105', '620105', '安宁区', 'Anning Qu', 'Anning Qu', ',29,336,3257,', '甘肃省-兰州市-安宁区', '336', '02', '04', '01', '3257', '', null, null);
INSERT INTO t_sys_area VALUES ('3258', '620111', '620111', '620111', '红古区', 'Honggu Qu', 'Honggu Qu', ',29,336,3258,', '甘肃省-兰州市-红古区', '336', '02', '04', '01', '3258', '', null, null);
INSERT INTO t_sys_area VALUES ('3259', '620121', '620121', '620121', '永登县', 'Yongdeng Xian', 'Yongdeng Xian', ',29,336,3259,', '甘肃省-兰州市-永登县', '336', '02', '04', '01', '3259', '', null, null);
INSERT INTO t_sys_area VALUES ('3260', '620122', '620122', '620122', '皋兰县', 'Gaolan Xian', 'Gaolan Xian', ',29,336,3260,', '甘肃省-兰州市-皋兰县', '336', '02', '04', '01', '3260', '', null, null);
INSERT INTO t_sys_area VALUES ('3261', '620123', '620123', '620123', '榆中县', 'Yuzhong Xian', 'Yuzhong Xian', ',29,336,3261,', '甘肃省-兰州市-榆中县', '336', '02', '04', '01', '3261', '', null, null);
INSERT INTO t_sys_area VALUES ('3262', '620201', '620201', '620201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',29,337,3262,', '甘肃省-嘉峪关市-市辖区', '337', '02', '04', '01', '3262', '', null, null);
INSERT INTO t_sys_area VALUES ('3263', '620301', '620301', '620301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',29,338,3263,', '甘肃省-金昌市-市辖区', '338', '02', '04', '01', '3263', '', null, null);
INSERT INTO t_sys_area VALUES ('3264', '620302', '620302', '620302', '金川区', 'Jinchuan Qu', 'Jinchuan Qu', ',29,338,3264,', '甘肃省-金昌市-金川区', '338', '02', '04', '01', '3264', '', null, null);
INSERT INTO t_sys_area VALUES ('3265', '620321', '620321', '620321', '永昌县', 'Yongchang Xian', 'Yongchang Xian', ',29,338,3265,', '甘肃省-金昌市-永昌县', '338', '02', '04', '01', '3265', '', null, null);
INSERT INTO t_sys_area VALUES ('3266', '620401', '620401', '620401', '市辖区', 'Shixiaqu', 'Shixiaqu', ',29,339,3266,', '甘肃省-白银市-市辖区', '339', '02', '04', '01', '3266', '', null, null);
INSERT INTO t_sys_area VALUES ('3267', '620402', '620402', '620402', '白银区', 'Baiyin Qu', 'Baiyin Qu', ',29,339,3267,', '甘肃省-白银市-白银区', '339', '02', '04', '01', '3267', '', null, null);
INSERT INTO t_sys_area VALUES ('3268', '620403', '620403', '620403', '平川区', 'Pingchuan Qu', 'Pingchuan Qu', ',29,339,3268,', '甘肃省-白银市-平川区', '339', '02', '04', '01', '3268', '', null, null);
INSERT INTO t_sys_area VALUES ('3269', '620421', '620421', '620421', '靖远县', 'Jingyuan Xian', 'Jingyuan Xian', ',29,339,3269,', '甘肃省-白银市-靖远县', '339', '02', '04', '01', '3269', '', null, null);
INSERT INTO t_sys_area VALUES ('3270', '620422', '620422', '620422', '会宁县', 'Huining xian', 'Huining xian', ',29,339,3270,', '甘肃省-白银市-会宁县', '339', '02', '04', '01', '3270', '', null, null);
INSERT INTO t_sys_area VALUES ('3271', '620423', '620423', '620423', '景泰县', 'Jingtai Xian', 'Jingtai Xian', ',29,339,3271,', '甘肃省-白银市-景泰县', '339', '02', '04', '01', '3271', '', null, null);
INSERT INTO t_sys_area VALUES ('3272', '620501', '620501', '620501', '市辖区', 'Shixiaqu', 'Shixiaqu', ',29,340,3272,', '甘肃省-天水市-市辖区', '340', '02', '04', '01', '3272', '', null, null);
INSERT INTO t_sys_area VALUES ('3274', '620502', '620502', '620502', '秦州区', 'Beidao Qu', 'Beidao Qu', ',29,340,3274,', '甘肃省-天水市-秦州区', '340', '02', '04', '01', '3274', '', null, null);
INSERT INTO t_sys_area VALUES ('3275', '620521', '620521', '620521', '清水县', 'Qingshui Xian', 'Qingshui Xian', ',29,340,3275,', '甘肃省-天水市-清水县', '340', '02', '04', '01', '3275', '', null, null);
INSERT INTO t_sys_area VALUES ('3276', '620522', '620522', '620522', '秦安县', 'Qin,an Xian', 'Qin,an Xian', ',29,340,3276,', '甘肃省-天水市-秦安县', '340', '02', '04', '01', '3276', '', null, null);
INSERT INTO t_sys_area VALUES ('3277', '620523', '620523', '620523', '甘谷县', 'Gangu Xian', 'Gangu Xian', ',29,340,3277,', '甘肃省-天水市-甘谷县', '340', '02', '04', '01', '3277', '', null, null);
INSERT INTO t_sys_area VALUES ('3278', '620524', '620524', '620524', '武山县', 'Wushan Xian', 'Wushan Xian', ',29,340,3278,', '甘肃省-天水市-武山县', '340', '02', '04', '01', '3278', '', null, null);
INSERT INTO t_sys_area VALUES ('3279', '620525', '620525', '620525', '张家川回族自治县', 'Zhangjiachuan Huizu Zizhixian', 'Zhangjiachuan Huizu Zizhixian', ',29,340,3279,', '甘肃省-天水市-张家川回族自治县', '340', '02', '04', '01', '3279', '', null, null);
INSERT INTO t_sys_area VALUES ('3280', '620601', '620601', '620601', '市辖区', '1', '1', ',29,341,3280,', '甘肃省-武威市-市辖区', '341', '02', '04', '01', '3280', '', null, null);
INSERT INTO t_sys_area VALUES ('3281', '620602', '620602', '620602', '凉州区', 'Liangzhou Qu', 'Liangzhou Qu', ',29,341,3281,', '甘肃省-武威市-凉州区', '341', '02', '04', '01', '3281', '', null, null);
INSERT INTO t_sys_area VALUES ('3282', '620621', '620621', '620621', '民勤县', 'Minqin Xian', 'Minqin Xian', ',29,341,3282,', '甘肃省-武威市-民勤县', '341', '02', '04', '01', '3282', '', null, null);
INSERT INTO t_sys_area VALUES ('3283', '620622', '620622', '620622', '古浪县', 'Gulang Xian', 'Gulang Xian', ',29,341,3283,', '甘肃省-武威市-古浪县', '341', '02', '04', '01', '3283', '', null, null);
INSERT INTO t_sys_area VALUES ('3284', '620623', '620623', '620623', '天祝藏族自治县', 'Tianzhu Zangzu Zizhixian', 'Tianzhu Zangzu Zizhixian', ',29,341,3284,', '甘肃省-武威市-天祝藏族自治县', '341', '02', '04', '01', '3284', '', null, null);
INSERT INTO t_sys_area VALUES ('3285', '620701', '620701', '620701', '市辖区', '1', '1', ',29,342,3285,', '甘肃省-张掖市-市辖区', '342', '02', '04', '01', '3285', '', null, null);
INSERT INTO t_sys_area VALUES ('3286', '620702', '620702', '620702', '甘州区', 'Ganzhou Qu', 'Ganzhou Qu', ',29,342,3286,', '甘肃省-张掖市-甘州区', '342', '02', '04', '01', '3286', '', null, null);
INSERT INTO t_sys_area VALUES ('3287', '620721', '620721', '620721', '肃南裕固族自治县', 'Sunan Yugurzu Zizhixian', 'Sunan Yugurzu Zizhixian', ',29,342,3287,', '甘肃省-张掖市-肃南裕固族自治县', '342', '02', '04', '01', '3287', '', null, null);
INSERT INTO t_sys_area VALUES ('3288', '620722', '620722', '620722', '民乐县', 'Minle Xian', 'Minle Xian', ',29,342,3288,', '甘肃省-张掖市-民乐县', '342', '02', '04', '01', '3288', '', null, null);
INSERT INTO t_sys_area VALUES ('3289', '620723', '620723', '620723', '临泽县', 'Linze Xian', 'Linze Xian', ',29,342,3289,', '甘肃省-张掖市-临泽县', '342', '02', '04', '01', '3289', '', null, null);
INSERT INTO t_sys_area VALUES ('3290', '620724', '620724', '620724', '高台县', 'Gaotai Xian', 'Gaotai Xian', ',29,342,3290,', '甘肃省-张掖市-高台县', '342', '02', '04', '01', '3290', '', null, null);
INSERT INTO t_sys_area VALUES ('3291', '620725', '620725', '620725', '山丹县', 'Shandan Xian', 'Shandan Xian', ',29,342,3291,', '甘肃省-张掖市-山丹县', '342', '02', '04', '01', '3291', '', null, null);
INSERT INTO t_sys_area VALUES ('3292', '620801', '620801', '620801', '市辖区', '1', '1', ',29,343,3292,', '甘肃省-平凉市-市辖区', '343', '02', '04', '01', '3292', '', null, null);
INSERT INTO t_sys_area VALUES ('3293', '620802', '620802', '620802', '崆峒区', 'Kongdong Qu', 'Kongdong Qu', ',29,343,3293,', '甘肃省-平凉市-崆峒区', '343', '02', '04', '01', '3293', '', null, null);
INSERT INTO t_sys_area VALUES ('3294', '620821', '620821', '620821', '泾川县', 'Jingchuan Xian', 'Jingchuan Xian', ',29,343,3294,', '甘肃省-平凉市-泾川县', '343', '02', '04', '01', '3294', '', null, null);
INSERT INTO t_sys_area VALUES ('3295', '620822', '620822', '620822', '灵台县', 'Lingtai Xian', 'Lingtai Xian', ',29,343,3295,', '甘肃省-平凉市-灵台县', '343', '02', '04', '01', '3295', '', null, null);
INSERT INTO t_sys_area VALUES ('3296', '620823', '620823', '620823', '崇信县', 'Chongxin Xian', 'Chongxin Xian', ',29,343,3296,', '甘肃省-平凉市-崇信县', '343', '02', '04', '01', '3296', '', null, null);
INSERT INTO t_sys_area VALUES ('3297', '620824', '620824', '620824', '华亭县', 'Huating Xian', 'Huating Xian', ',29,343,3297,', '甘肃省-平凉市-华亭县', '343', '02', '04', '01', '3297', '', null, null);
INSERT INTO t_sys_area VALUES ('3298', '620825', '620825', '620825', '庄浪县', 'Zhuanglang Xian', 'Zhuanglang Xian', ',29,343,3298,', '甘肃省-平凉市-庄浪县', '343', '02', '04', '01', '3298', '', null, null);
INSERT INTO t_sys_area VALUES ('3299', '620826', '620826', '620826', '静宁县', 'Jingning Xian', 'Jingning Xian', ',29,343,3299,', '甘肃省-平凉市-静宁县', '343', '02', '04', '01', '3299', '', null, null);
INSERT INTO t_sys_area VALUES ('3300', '620901', '620901', '620901', '市辖区', '1', '1', ',29,344,3300,', '甘肃省-酒泉市-市辖区', '344', '02', '04', '01', '3300', '', null, null);
INSERT INTO t_sys_area VALUES ('3301', '620902', '620902', '620902', '肃州区', 'Suzhou Qu', 'Suzhou Qu', ',29,344,3301,', '甘肃省-酒泉市-肃州区', '344', '02', '04', '01', '3301', '', null, null);
INSERT INTO t_sys_area VALUES ('3302', '620921', '620921', '620921', '金塔县', 'Jinta Xian', 'Jinta Xian', ',29,344,3302,', '甘肃省-酒泉市-金塔县', '344', '02', '04', '01', '3302', '', null, null);
INSERT INTO t_sys_area VALUES ('3304', '620923', '620923', '620923', '肃北蒙古族自治县', 'Subei Monguzu Zizhixian', 'Subei Monguzu Zizhixian', ',29,344,3304,', '甘肃省-酒泉市-肃北蒙古族自治县', '344', '02', '04', '01', '3304', '', null, null);
INSERT INTO t_sys_area VALUES ('3305', '620924', '620924', '620924', '阿克塞哈萨克族自治县', 'Aksay Kazakzu Zizhixian', 'Aksay Kazakzu Zizhixian', ',29,344,3305,', '甘肃省-酒泉市-阿克塞哈萨克族自治县', '344', '02', '04', '01', '3305', '', null, null);
INSERT INTO t_sys_area VALUES ('3306', '620981', '620981', '620981', '玉门市', 'Yumen Shi', 'Yumen Shi', ',29,344,3306,', '甘肃省-酒泉市-玉门市', '344', '02', '04', '01', '3306', '', null, null);
INSERT INTO t_sys_area VALUES ('3307', '620982', '620982', '620982', '敦煌市', 'Dunhuang Shi', 'Dunhuang Shi', ',29,344,3307,', '甘肃省-酒泉市-敦煌市', '344', '02', '04', '01', '3307', '', null, null);
INSERT INTO t_sys_area VALUES ('3308', '621001', '621001', '621001', '市辖区', '1', '1', ',29,345,3308,', '甘肃省-庆阳市-市辖区', '345', '02', '04', '01', '3308', '', null, null);
INSERT INTO t_sys_area VALUES ('3309', '621002', '621002', '621002', '西峰区', 'Xifeng Qu', 'Xifeng Qu', ',29,345,3309,', '甘肃省-庆阳市-西峰区', '345', '02', '04', '01', '3309', '', null, null);
INSERT INTO t_sys_area VALUES ('3310', '621021', '621021', '621021', '庆城县', 'Qingcheng Xian', 'Qingcheng Xian', ',29,345,3310,', '甘肃省-庆阳市-庆城县', '345', '02', '04', '01', '3310', '', null, null);
INSERT INTO t_sys_area VALUES ('3311', '621022', '621022', '621022', '环县', 'Huan Xian', 'Huan Xian', ',29,345,3311,', '甘肃省-庆阳市-环县', '345', '02', '04', '01', '3311', '', null, null);
INSERT INTO t_sys_area VALUES ('3312', '621023', '621023', '621023', '华池县', 'Huachi Xian', 'Huachi Xian', ',29,345,3312,', '甘肃省-庆阳市-华池县', '345', '02', '04', '01', '3312', '', null, null);
INSERT INTO t_sys_area VALUES ('3313', '621024', '621024', '621024', '合水县', 'Heshui Xian', 'Heshui Xian', ',29,345,3313,', '甘肃省-庆阳市-合水县', '345', '02', '04', '01', '3313', '', null, null);
INSERT INTO t_sys_area VALUES ('3314', '621025', '621025', '621025', '正宁县', 'Zhengning Xian', 'Zhengning Xian', ',29,345,3314,', '甘肃省-庆阳市-正宁县', '345', '02', '04', '01', '3314', '', null, null);
INSERT INTO t_sys_area VALUES ('3315', '621026', '621026', '621026', '宁县', 'Ning Xian', 'Ning Xian', ',29,345,3315,', '甘肃省-庆阳市-宁县', '345', '02', '04', '01', '3315', '', null, null);
INSERT INTO t_sys_area VALUES ('3316', '621027', '621027', '621027', '镇原县', 'Zhenyuan Xian', 'Zhenyuan Xian', ',29,345,3316,', '甘肃省-庆阳市-镇原县', '345', '02', '04', '01', '3316', '', null, null);
INSERT INTO t_sys_area VALUES ('3317', '621101', '621101', '621101', '市辖区', '1', '1', ',29,346,3317,', '甘肃省-定西市-市辖区', '346', '02', '04', '01', '3317', '', null, null);
INSERT INTO t_sys_area VALUES ('3318', '621102', '621102', '621102', '安定区', 'Anding Qu', 'Anding Qu', ',29,346,3318,', '甘肃省-定西市-安定区', '346', '02', '04', '01', '3318', '', null, null);
INSERT INTO t_sys_area VALUES ('3319', '621121', '621121', '621121', '通渭县', 'Tongwei Xian', 'Tongwei Xian', ',29,346,3319,', '甘肃省-定西市-通渭县', '346', '02', '04', '01', '3319', '', null, null);
INSERT INTO t_sys_area VALUES ('3320', '621122', '621122', '621122', '陇西县', 'Longxi Xian', 'Longxi Xian', ',29,346,3320,', '甘肃省-定西市-陇西县', '346', '02', '04', '01', '3320', '', null, null);
INSERT INTO t_sys_area VALUES ('3321', '621123', '621123', '621123', '渭源县', 'Weiyuan Xian', 'Weiyuan Xian', ',29,346,3321,', '甘肃省-定西市-渭源县', '346', '02', '04', '01', '3321', '', null, null);
INSERT INTO t_sys_area VALUES ('3322', '621124', '621124', '621124', '临洮县', 'Lintao Xian', 'Lintao Xian', ',29,346,3322,', '甘肃省-定西市-临洮县', '346', '02', '04', '01', '3322', '', null, null);
INSERT INTO t_sys_area VALUES ('3323', '621125', '621125', '621125', '漳县', 'Zhang Xian', 'Zhang Xian', ',29,346,3323,', '甘肃省-定西市-漳县', '346', '02', '04', '01', '3323', '', null, null);
INSERT INTO t_sys_area VALUES ('3324', '621126', '621126', '621126', '岷县', 'Min Xian', 'Min Xian', ',29,346,3324,', '甘肃省-定西市-岷县', '346', '02', '04', '01', '3324', '', null, null);
INSERT INTO t_sys_area VALUES ('3325', '621201', '621201', '621201', '市辖区', '1', '1', ',29,347,3325,', '甘肃省-陇南市-市辖区', '347', '02', '04', '01', '3325', '', null, null);
INSERT INTO t_sys_area VALUES ('3326', '621202', '621202', '621202', '武都区', 'Wudu Qu', 'Wudu Qu', ',29,347,3326,', '甘肃省-陇南市-武都区', '347', '02', '04', '01', '3326', '', null, null);
INSERT INTO t_sys_area VALUES ('3327', '621221', '621221', '621221', '成县', 'Cheng Xian', 'Cheng Xian', ',29,347,3327,', '甘肃省-陇南市-成县', '347', '02', '04', '01', '3327', '', null, null);
INSERT INTO t_sys_area VALUES ('3328', '621222', '621222', '621222', '文县', 'Wen Xian', 'Wen Xian', ',29,347,3328,', '甘肃省-陇南市-文县', '347', '02', '04', '01', '3328', '', null, null);
INSERT INTO t_sys_area VALUES ('3329', '621223', '621223', '621223', '宕昌县', 'Dangchang Xian', 'Dangchang Xian', ',29,347,3329,', '甘肃省-陇南市-宕昌县', '347', '02', '04', '01', '3329', '', null, null);
INSERT INTO t_sys_area VALUES ('3330', '621224', '621224', '621224', '康县', 'Kang Xian', 'Kang Xian', ',29,347,3330,', '甘肃省-陇南市-康县', '347', '02', '04', '01', '3330', '', null, null);
INSERT INTO t_sys_area VALUES ('3331', '621225', '621225', '621225', '西和县', 'Xihe Xian', 'Xihe Xian', ',29,347,3331,', '甘肃省-陇南市-西和县', '347', '02', '04', '01', '3331', '', null, null);
INSERT INTO t_sys_area VALUES ('3332', '621226', '621226', '621226', '礼县', 'Li Xian', 'Li Xian', ',29,347,3332,', '甘肃省-陇南市-礼县', '347', '02', '04', '01', '3332', '', null, null);
INSERT INTO t_sys_area VALUES ('3333', '621227', '621227', '621227', '徽县', 'Hui Xian', 'Hui Xian', ',29,347,3333,', '甘肃省-陇南市-徽县', '347', '02', '04', '01', '3333', '', null, null);
INSERT INTO t_sys_area VALUES ('3334', '621228', '621228', '621228', '两当县', 'Liangdang Xian', 'Liangdang Xian', ',29,347,3334,', '甘肃省-陇南市-两当县', '347', '02', '04', '01', '3334', '', null, null);
INSERT INTO t_sys_area VALUES ('3335', '622901', '622901', '622901', '临夏市', 'Linxia Shi', 'Linxia Shi', ',29,348,3335,', '甘肃省-临夏回族自治州-临夏市', '348', '02', '04', '01', '3335', '', null, null);
INSERT INTO t_sys_area VALUES ('3336', '622921', '622921', '622921', '临夏县', 'Linxia Xian', 'Linxia Xian', ',29,348,3336,', '甘肃省-临夏回族自治州-临夏县', '348', '02', '04', '01', '3336', '', null, null);
INSERT INTO t_sys_area VALUES ('3337', '622922', '622922', '622922', '康乐县', 'Kangle Xian', 'Kangle Xian', ',29,348,3337,', '甘肃省-临夏回族自治州-康乐县', '348', '02', '04', '01', '3337', '', null, null);
INSERT INTO t_sys_area VALUES ('3338', '622923', '622923', '622923', '永靖县', 'Yongjing Xian', 'Yongjing Xian', ',29,348,3338,', '甘肃省-临夏回族自治州-永靖县', '348', '02', '04', '01', '3338', '', null, null);
INSERT INTO t_sys_area VALUES ('3339', '622924', '622924', '622924', '广河县', 'Guanghe Xian', 'Guanghe Xian', ',29,348,3339,', '甘肃省-临夏回族自治州-广河县', '348', '02', '04', '01', '3339', '', null, null);
INSERT INTO t_sys_area VALUES ('3340', '622925', '622925', '622925', '和政县', 'Hezheng Xian', 'Hezheng Xian', ',29,348,3340,', '甘肃省-临夏回族自治州-和政县', '348', '02', '04', '01', '3340', '', null, null);
INSERT INTO t_sys_area VALUES ('3341', '622926', '622926', '622926', '东乡族自治县', 'Dongxiangzu Zizhixian', 'Dongxiangzu Zizhixian', ',29,348,3341,', '甘肃省-临夏回族自治州-东乡族自治县', '348', '02', '04', '01', '3341', '', null, null);
INSERT INTO t_sys_area VALUES ('3342', '622927', '622927', '622927', '积石山保安族东乡族撒拉族自治县', 'Jishishan Bonanzu Dongxiangzu Salarzu Zizhixian', 'Jishishan Bonanzu Dongxiangzu Salarzu Zizhixian', ',29,348,3342,', '甘肃省-临夏回族自治州-积石山保安族东乡族撒拉族自治县', '348', '02', '04', '01', '3342', '', null, null);
INSERT INTO t_sys_area VALUES ('3343', '623001', '623001', '623001', '合作市', 'Hezuo Shi', 'Hezuo Shi', ',29,349,3343,', '甘肃省-甘南藏族自治州-合作市', '349', '02', '04', '01', '3343', '', null, null);
INSERT INTO t_sys_area VALUES ('3344', '623021', '623021', '623021', '临潭县', 'Lintan Xian', 'Lintan Xian', ',29,349,3344,', '甘肃省-甘南藏族自治州-临潭县', '349', '02', '04', '01', '3344', '', null, null);
INSERT INTO t_sys_area VALUES ('3345', '623022', '623022', '623022', '卓尼县', 'Jone', 'Jone', ',29,349,3345,', '甘肃省-甘南藏族自治州-卓尼县', '349', '02', '04', '01', '3345', '', null, null);
INSERT INTO t_sys_area VALUES ('3346', '623023', '623023', '623023', '舟曲县', 'Zhugqu Xian', 'Zhugqu Xian', ',29,349,3346,', '甘肃省-甘南藏族自治州-舟曲县', '349', '02', '04', '01', '3346', '', null, null);
INSERT INTO t_sys_area VALUES ('3347', '623024', '623024', '623024', '迭部县', 'Tewo Xian', 'Tewo Xian', ',29,349,3347,', '甘肃省-甘南藏族自治州-迭部县', '349', '02', '04', '01', '3347', '', null, null);
INSERT INTO t_sys_area VALUES ('3348', '623025', '623025', '623025', '玛曲县', 'Maqu Xian', 'Maqu Xian', ',29,349,3348,', '甘肃省-甘南藏族自治州-玛曲县', '349', '02', '04', '01', '3348', '', null, null);
INSERT INTO t_sys_area VALUES ('3349', '623026', '623026', '623026', '碌曲县', 'Luqu Xian', 'Luqu Xian', ',29,349,3349,', '甘肃省-甘南藏族自治州-碌曲县', '349', '02', '04', '01', '3349', '', null, null);
INSERT INTO t_sys_area VALUES ('3350', '623027', '623027', '623027', '夏河县', 'Xiahe Xian', 'Xiahe Xian', ',29,349,3350,', '甘肃省-甘南藏族自治州-夏河县', '349', '02', '04', '01', '3350', '', null, null);
INSERT INTO t_sys_area VALUES ('3351', '630101', '630101', '630101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',30,350,3351,', '青海省-西宁市-市辖区', '350', '02', '04', '01', '3351', '', null, null);
INSERT INTO t_sys_area VALUES ('3352', '630102', '630102', '630102', '城东区', 'Chengdong Qu', 'Chengdong Qu', ',30,350,3352,', '青海省-西宁市-城东区', '350', '02', '04', '01', '3352', '', null, null);
INSERT INTO t_sys_area VALUES ('3353', '630103', '630103', '630103', '城中区', 'Chengzhong Qu', 'Chengzhong Qu', ',30,350,3353,', '青海省-西宁市-城中区', '350', '02', '04', '01', '3353', '', null, null);
INSERT INTO t_sys_area VALUES ('3354', '630104', '630104', '630104', '城西区', 'Chengxi Qu', 'Chengxi Qu', ',30,350,3354,', '青海省-西宁市-城西区', '350', '02', '04', '01', '3354', '', null, null);
INSERT INTO t_sys_area VALUES ('3355', '630105', '630105', '630105', '城北区', 'Chengbei Qu', 'Chengbei Qu', ',30,350,3355,', '青海省-西宁市-城北区', '350', '02', '04', '01', '3355', '', null, null);
INSERT INTO t_sys_area VALUES ('3356', '630121', '630121', '630121', '大通回族土族自治县', 'Datong Huizu Tuzu Zizhixian', 'Datong Huizu Tuzu Zizhixian', ',30,350,3356,', '青海省-西宁市-大通回族土族自治县', '350', '02', '04', '01', '3356', '', null, null);
INSERT INTO t_sys_area VALUES ('3357', '630122', '630122', '630122', '湟中县', 'Huangzhong Xian', 'Huangzhong Xian', ',30,350,3357,', '青海省-西宁市-湟中县', '350', '02', '04', '01', '3357', '', null, null);
INSERT INTO t_sys_area VALUES ('3358', '630123', '630123', '630123', '湟源县', 'Huangyuan Xian', 'Huangyuan Xian', ',30,350,3358,', '青海省-西宁市-湟源县', '350', '02', '04', '01', '3358', '', null, null);
INSERT INTO t_sys_area VALUES ('3359', '632121', '632121', '632121', '平安县', 'Ping,an Xian', 'Ping,an Xian', ',30,351,3359,', '青海省-海东地区-平安县', '351', '02', '04', '01', '3359', '', null, null);
INSERT INTO t_sys_area VALUES ('3360', '632122', '632122', '632122', '民和回族土族自治县', 'Minhe Huizu Tuzu Zizhixian', 'Minhe Huizu Tuzu Zizhixian', ',30,351,3360,', '青海省-海东地区-民和回族土族自治县', '351', '02', '04', '01', '3360', '', null, null);
INSERT INTO t_sys_area VALUES ('3361', '632123', '632123', '632123', '乐都县', 'Ledu Xian', 'Ledu Xian', ',30,351,3361,', '青海省-海东地区-乐都县', '351', '02', '04', '01', '3361', '', null, null);
INSERT INTO t_sys_area VALUES ('3362', '632126', '632126', '632126', '互助土族自治县', 'Huzhu Tuzu Zizhixian', 'Huzhu Tuzu Zizhixian', ',30,351,3362,', '青海省-海东地区-互助土族自治县', '351', '02', '04', '01', '3362', '', null, null);
INSERT INTO t_sys_area VALUES ('3363', '632127', '632127', '632127', '化隆回族自治县', 'Hualong Huizu Zizhixian', 'Hualong Huizu Zizhixian', ',30,351,3363,', '青海省-海东地区-化隆回族自治县', '351', '02', '04', '01', '3363', '', null, null);
INSERT INTO t_sys_area VALUES ('3364', '632128', '632128', '632128', '循化撒拉族自治县', 'Xunhua Salazu Zizhixian', 'Xunhua Salazu Zizhixian', ',30,351,3364,', '青海省-海东地区-循化撒拉族自治县', '351', '02', '04', '01', '3364', '', null, null);
INSERT INTO t_sys_area VALUES ('3365', '632221', '632221', '632221', '门源回族自治县', 'Menyuan Huizu Zizhixian', 'Menyuan Huizu Zizhixian', ',30,352,3365,', '青海省-海北藏族自治州-门源回族自治县', '352', '02', '04', '01', '3365', '', null, null);
INSERT INTO t_sys_area VALUES ('3366', '632222', '632222', '632222', '祁连县', 'Qilian Xian', 'Qilian Xian', ',30,352,3366,', '青海省-海北藏族自治州-祁连县', '352', '02', '04', '01', '3366', '', null, null);
INSERT INTO t_sys_area VALUES ('3367', '632223', '632223', '632223', '海晏县', 'Haiyan Xian', 'Haiyan Xian', ',30,352,3367,', '青海省-海北藏族自治州-海晏县', '352', '02', '04', '01', '3367', '', null, null);
INSERT INTO t_sys_area VALUES ('3368', '632224', '632224', '632224', '刚察县', 'Gangca Xian', 'Gangca Xian', ',30,352,3368,', '青海省-海北藏族自治州-刚察县', '352', '02', '04', '01', '3368', '', null, null);
INSERT INTO t_sys_area VALUES ('3369', '632321', '632321', '632321', '同仁县', 'Tongren Xian', 'Tongren Xian', ',30,353,3369,', '青海省-黄南藏族自治州-同仁县', '353', '02', '04', '01', '3369', '', null, null);
INSERT INTO t_sys_area VALUES ('3370', '632322', '632322', '632322', '尖扎县', 'Jainca Xian', 'Jainca Xian', ',30,353,3370,', '青海省-黄南藏族自治州-尖扎县', '353', '02', '04', '01', '3370', '', null, null);
INSERT INTO t_sys_area VALUES ('3371', '632323', '632323', '632323', '泽库县', 'Zekog Xian', 'Zekog Xian', ',30,353,3371,', '青海省-黄南藏族自治州-泽库县', '353', '02', '04', '01', '3371', '', null, null);
INSERT INTO t_sys_area VALUES ('3372', '632324', '632324', '632324', '河南蒙古族自治县', 'Henan Mongolzu Zizhixian', 'Henan Mongolzu Zizhixian', ',30,353,3372,', '青海省-黄南藏族自治州-河南蒙古族自治县', '353', '02', '04', '01', '3372', '', null, null);
INSERT INTO t_sys_area VALUES ('3373', '632521', '632521', '632521', '共和县', 'Gonghe Xian', 'Gonghe Xian', ',30,354,3373,', '青海省-海南藏族自治州-共和县', '354', '02', '04', '01', '3373', '', null, null);
INSERT INTO t_sys_area VALUES ('3374', '632522', '632522', '632522', '同德县', 'Tongde Xian', 'Tongde Xian', ',30,354,3374,', '青海省-海南藏族自治州-同德县', '354', '02', '04', '01', '3374', '', null, null);
INSERT INTO t_sys_area VALUES ('3375', '632523', '632523', '632523', '贵德县', 'Guide Xian', 'Guide Xian', ',30,354,3375,', '青海省-海南藏族自治州-贵德县', '354', '02', '04', '01', '3375', '', null, null);
INSERT INTO t_sys_area VALUES ('3376', '632524', '632524', '632524', '兴海县', 'Xinghai Xian', 'Xinghai Xian', ',30,354,3376,', '青海省-海南藏族自治州-兴海县', '354', '02', '04', '01', '3376', '', null, null);
INSERT INTO t_sys_area VALUES ('3377', '632525', '632525', '632525', '贵南县', 'Guinan Xian', 'Guinan Xian', ',30,354,3377,', '青海省-海南藏族自治州-贵南县', '354', '02', '04', '01', '3377', '', null, null);
INSERT INTO t_sys_area VALUES ('3378', '632621', '632621', '632621', '玛沁县', 'Maqen Xian', 'Maqen Xian', ',30,355,3378,', '青海省-果洛藏族自治州-玛沁县', '355', '02', '04', '01', '3378', '', null, null);
INSERT INTO t_sys_area VALUES ('3379', '632622', '632622', '632622', '班玛县', 'Baima Xian', 'Baima Xian', ',30,355,3379,', '青海省-果洛藏族自治州-班玛县', '355', '02', '04', '01', '3379', '', null, null);
INSERT INTO t_sys_area VALUES ('3380', '632623', '632623', '632623', '甘德县', 'Gade Xian', 'Gade Xian', ',30,355,3380,', '青海省-果洛藏族自治州-甘德县', '355', '02', '04', '01', '3380', '', null, null);
INSERT INTO t_sys_area VALUES ('3381', '632624', '632624', '632624', '达日县', 'Tarlag Xian', 'Tarlag Xian', ',30,355,3381,', '青海省-果洛藏族自治州-达日县', '355', '02', '04', '01', '3381', '', null, null);
INSERT INTO t_sys_area VALUES ('3382', '632625', '632625', '632625', '久治县', 'Jigzhi Xian', 'Jigzhi Xian', ',30,355,3382,', '青海省-果洛藏族自治州-久治县', '355', '02', '04', '01', '3382', '', null, null);
INSERT INTO t_sys_area VALUES ('3383', '632626', '632626', '632626', '玛多县', 'Madoi Xian', 'Madoi Xian', ',30,355,3383,', '青海省-果洛藏族自治州-玛多县', '355', '02', '04', '01', '3383', '', null, null);
INSERT INTO t_sys_area VALUES ('3384', '632721', '632721', '632721', '玉树县', 'Yushu Xian', 'Yushu Xian', ',30,356,3384,', '青海省-玉树藏族自治州-玉树县', '356', '02', '04', '01', '3384', '', null, null);
INSERT INTO t_sys_area VALUES ('3385', '632722', '632722', '632722', '杂多县', 'Zadoi Xian', 'Zadoi Xian', ',30,356,3385,', '青海省-玉树藏族自治州-杂多县', '356', '02', '04', '01', '3385', '', null, null);
INSERT INTO t_sys_area VALUES ('3386', '632723', '632723', '632723', '称多县', 'Chindu Xian', 'Chindu Xian', ',30,356,3386,', '青海省-玉树藏族自治州-称多县', '356', '02', '04', '01', '3386', '', null, null);
INSERT INTO t_sys_area VALUES ('3387', '632724', '632724', '632724', '治多县', 'Zhidoi Xian', 'Zhidoi Xian', ',30,356,3387,', '青海省-玉树藏族自治州-治多县', '356', '02', '04', '01', '3387', '', null, null);
INSERT INTO t_sys_area VALUES ('3388', '632725', '632725', '632725', '囊谦县', 'Nangqen Xian', 'Nangqen Xian', ',30,356,3388,', '青海省-玉树藏族自治州-囊谦县', '356', '02', '04', '01', '3388', '', null, null);
INSERT INTO t_sys_area VALUES ('3389', '632726', '632726', '632726', '曲麻莱县', 'Qumarleb Xian', 'Qumarleb Xian', ',30,356,3389,', '青海省-玉树藏族自治州-曲麻莱县', '356', '02', '04', '01', '3389', '', null, null);
INSERT INTO t_sys_area VALUES ('3390', '632801', '632801', '632801', '格尔木市', 'Golmud Shi', 'Golmud Shi', ',30,357,3390,', '青海省-海西蒙古族藏族自治州-格尔木市', '357', '02', '04', '01', '3390', '', null, null);
INSERT INTO t_sys_area VALUES ('3391', '632802', '632802', '632802', '德令哈市', 'Delhi Shi', 'Delhi Shi', ',30,357,3391,', '青海省-海西蒙古族藏族自治州-德令哈市', '357', '02', '04', '01', '3391', '', null, null);
INSERT INTO t_sys_area VALUES ('3392', '632821', '632821', '632821', '乌兰县', 'Ulan Xian', 'Ulan Xian', ',30,357,3392,', '青海省-海西蒙古族藏族自治州-乌兰县', '357', '02', '04', '01', '3392', '', null, null);
INSERT INTO t_sys_area VALUES ('3393', '632822', '632822', '632822', '都兰县', 'Dulan Xian', 'Dulan Xian', ',30,357,3393,', '青海省-海西蒙古族藏族自治州-都兰县', '357', '02', '04', '01', '3393', '', null, null);
INSERT INTO t_sys_area VALUES ('3394', '632823', '632823', '632823', '天峻县', 'Tianjun Xian', 'Tianjun Xian', ',30,357,3394,', '青海省-海西蒙古族藏族自治州-天峻县', '357', '02', '04', '01', '3394', '', null, null);
INSERT INTO t_sys_area VALUES ('3395', '640101', '640101', '640101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',31,358,3395,', '宁夏回族自治区-银川市-市辖区', '358', '02', '04', '01', '3395', '', null, null);
INSERT INTO t_sys_area VALUES ('3396', '640104', '640104', '640104', '兴庆区', 'Xingqing Qu', 'Xingqing Qu', ',31,358,3396,', '宁夏回族自治区-银川市-兴庆区', '358', '02', '04', '01', '3396', '', null, null);
INSERT INTO t_sys_area VALUES ('3397', '640105', '640105', '640105', '西夏区', 'Xixia Qu', 'Xixia Qu', ',31,358,3397,', '宁夏回族自治区-银川市-西夏区', '358', '02', '04', '01', '3397', '', null, null);
INSERT INTO t_sys_area VALUES ('3398', '640106', '640106', '640106', '金凤区', 'Jinfeng Qu', 'Jinfeng Qu', ',31,358,3398,', '宁夏回族自治区-银川市-金凤区', '358', '02', '04', '01', '3398', '', null, null);
INSERT INTO t_sys_area VALUES ('3399', '640121', '640121', '640121', '永宁县', 'Yongning Xian', 'Yongning Xian', ',31,358,3399,', '宁夏回族自治区-银川市-永宁县', '358', '02', '04', '01', '3399', '', null, null);
INSERT INTO t_sys_area VALUES ('3400', '640122', '640122', '640122', '贺兰县', 'Helan Xian', 'Helan Xian', ',31,358,3400,', '宁夏回族自治区-银川市-贺兰县', '358', '02', '04', '01', '3400', '', null, null);
INSERT INTO t_sys_area VALUES ('3401', '640181', '640181', '640181', '灵武市', 'Lingwu Shi', 'Lingwu Shi', ',31,358,3401,', '宁夏回族自治区-银川市-灵武市', '358', '02', '04', '01', '3401', '', null, null);
INSERT INTO t_sys_area VALUES ('3402', '640201', '640201', '640201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',31,359,3402,', '宁夏回族自治区-石嘴山市-市辖区', '359', '02', '04', '01', '3402', '', null, null);
INSERT INTO t_sys_area VALUES ('3403', '640202', '640202', '640202', '大武口区', 'Dawukou Qu', 'Dawukou Qu', ',31,359,3403,', '宁夏回族自治区-石嘴山市-大武口区', '359', '02', '04', '01', '3403', '', null, null);
INSERT INTO t_sys_area VALUES ('3404', '640205', '640205', '640205', '惠农区', 'Huinong Qu', 'Huinong Qu', ',31,359,3404,', '宁夏回族自治区-石嘴山市-惠农区', '359', '02', '04', '01', '3404', '', null, null);
INSERT INTO t_sys_area VALUES ('3405', '640221', '640221', '640221', '平罗县', 'Pingluo Xian', 'Pingluo Xian', ',31,359,3405,', '宁夏回族自治区-石嘴山市-平罗县', '359', '02', '04', '01', '3405', '', null, null);
INSERT INTO t_sys_area VALUES ('3406', '640301', '640301', '640301', '市辖区', 'Shixiaqu', 'Shixiaqu', ',31,360,3406,', '宁夏回族自治区-吴忠市-市辖区', '360', '02', '04', '01', '3406', '', null, null);
INSERT INTO t_sys_area VALUES ('3407', '640302', '640302', '640302', '利通区', 'Litong Qu', 'Litong Qu', ',31,360,3407,', '宁夏回族自治区-吴忠市-利通区', '360', '02', '04', '01', '3407', '', null, null);
INSERT INTO t_sys_area VALUES ('3408', '640323', '640323', '640323', '盐池县', 'Yanchi Xian', 'Yanchi Xian', ',31,360,3408,', '宁夏回族自治区-吴忠市-盐池县', '360', '02', '04', '01', '3408', '', null, null);
INSERT INTO t_sys_area VALUES ('3409', '640324', '640324', '640324', '同心县', 'Tongxin Xian', 'Tongxin Xian', ',31,360,3409,', '宁夏回族自治区-吴忠市-同心县', '360', '02', '04', '01', '3409', '', null, null);
INSERT INTO t_sys_area VALUES ('3410', '640381', '640381', '640381', '青铜峡市', 'Qingtongxia Xian', 'Qingtongxia Xian', ',31,360,3410,', '宁夏回族自治区-吴忠市-青铜峡市', '360', '02', '04', '01', '3410', '', null, null);
INSERT INTO t_sys_area VALUES ('3411', '640401', '640401', '640401', '市辖区', '1', '1', ',31,361,3411,', '宁夏回族自治区-固原市-市辖区', '361', '02', '04', '01', '3411', '', null, null);
INSERT INTO t_sys_area VALUES ('3412', '640402', '640402', '640402', '原州区', 'Yuanzhou Qu', 'Yuanzhou Qu', ',31,361,3412,', '宁夏回族自治区-固原市-原州区', '361', '02', '04', '01', '3412', '', null, null);
INSERT INTO t_sys_area VALUES ('3413', '640422', '640422', '640422', '西吉县', 'Xiji Xian', 'Xiji Xian', ',31,361,3413,', '宁夏回族自治区-固原市-西吉县', '361', '02', '04', '01', '3413', '', null, null);
INSERT INTO t_sys_area VALUES ('3414', '640423', '640423', '640423', '隆德县', 'Longde Xian', 'Longde Xian', ',31,361,3414,', '宁夏回族自治区-固原市-隆德县', '361', '02', '04', '01', '3414', '', null, null);
INSERT INTO t_sys_area VALUES ('3415', '640424', '640424', '640424', '泾源县', 'Jingyuan Xian', 'Jingyuan Xian', ',31,361,3415,', '宁夏回族自治区-固原市-泾源县', '361', '02', '04', '01', '3415', '', null, null);
INSERT INTO t_sys_area VALUES ('3416', '640425', '640425', '640425', '彭阳县', 'Pengyang Xian', 'Pengyang Xian', ',31,361,3416,', '宁夏回族自治区-固原市-彭阳县', '361', '02', '04', '01', '3416', '', null, null);
INSERT INTO t_sys_area VALUES ('3417', '640501', '640501', '640501', '市辖区', '1', '1', ',31,362,3417,', '宁夏回族自治区-中卫市-市辖区', '362', '02', '04', '01', '3417', '', null, null);
INSERT INTO t_sys_area VALUES ('3418', '640502', '640502', '640502', '沙坡头区', 'Shapotou Qu', 'Shapotou Qu', ',31,362,3418,', '宁夏回族自治区-中卫市-沙坡头区', '362', '02', '04', '01', '3418', '', null, null);
INSERT INTO t_sys_area VALUES ('3419', '640521', '640521', '640521', '中宁县', 'Zhongning Xian', 'Zhongning Xian', ',31,362,3419,', '宁夏回族自治区-中卫市-中宁县', '362', '02', '04', '01', '3419', '', null, null);
INSERT INTO t_sys_area VALUES ('3420', '640522', '640522', '640522', '海原县', 'Haiyuan Xian', 'Haiyuan Xian', ',31,362,3420,', '宁夏回族自治区-中卫市-海原县', '362', '02', '04', '01', '3420', '', null, null);
INSERT INTO t_sys_area VALUES ('3421', '650101', '650101', '650101', '市辖区', 'Shixiaqu', 'Shixiaqu', ',32,363,3421,', '新疆维吾尔自治区-乌鲁木齐市-市辖区', '363', '02', '04', '01', '3421', '', null, null);
INSERT INTO t_sys_area VALUES ('3422', '650102', '650102', '650102', '天山区', 'Tianshan Qu', 'Tianshan Qu', ',32,363,3422,', '新疆维吾尔自治区-乌鲁木齐市-天山区', '363', '02', '04', '01', '3422', '', null, null);
INSERT INTO t_sys_area VALUES ('3423', '650103', '650103', '650103', '沙依巴克区', 'Saybag Qu', 'Saybag Qu', ',32,363,3423,', '新疆维吾尔自治区-乌鲁木齐市-沙依巴克区', '363', '02', '04', '01', '3423', '', null, null);
INSERT INTO t_sys_area VALUES ('3424', '650104', '650104', '650104', '新市区', 'Xinshi Qu', 'Xinshi Qu', ',32,363,3424,', '新疆维吾尔自治区-乌鲁木齐市-新市区', '363', '02', '04', '01', '3424', '', null, null);
INSERT INTO t_sys_area VALUES ('3425', '650105', '650105', '650105', '水磨沟区', 'Shuimogou Qu', 'Shuimogou Qu', ',32,363,3425,', '新疆维吾尔自治区-乌鲁木齐市-水磨沟区', '363', '02', '04', '01', '3425', '', null, null);
INSERT INTO t_sys_area VALUES ('3426', '650106', '650106', '650106', '头屯河区', 'Toutunhe Qu', 'Toutunhe Qu', ',32,363,3426,', '新疆维吾尔自治区-乌鲁木齐市-头屯河区', '363', '02', '04', '01', '3426', '', null, null);
INSERT INTO t_sys_area VALUES ('3427', '650107', '650107', '650107', '达坂城区', 'Dabancheng Qu', 'Dabancheng Qu', ',32,363,3427,', '新疆维吾尔自治区-乌鲁木齐市-达坂城区', '363', '02', '04', '01', '3427', '', null, null);
INSERT INTO t_sys_area VALUES ('3428', '650109', '650109', '650109', '米东区', 'Midong Qu', 'Midong Qu', ',32,363,3428,', '新疆维吾尔自治区-乌鲁木齐市-米东区', '363', '02', '04', '01', '3428', '', null, null);
INSERT INTO t_sys_area VALUES ('3429', '650121', '650121', '650121', '乌鲁木齐县', 'Urumqi Xian', 'Urumqi Xian', ',32,363,3429,', '新疆维吾尔自治区-乌鲁木齐市-乌鲁木齐县', '363', '02', '04', '01', '3429', '', null, null);
INSERT INTO t_sys_area VALUES ('3430', '650201', '650201', '650201', '市辖区', 'Shixiaqu', 'Shixiaqu', ',32,364,3430,', '新疆维吾尔自治区-克拉玛依市-市辖区', '364', '02', '04', '01', '3430', '', null, null);
INSERT INTO t_sys_area VALUES ('3431', '650202', '650202', '650202', '独山子区', 'Dushanzi Qu', 'Dushanzi Qu', ',32,364,3431,', '新疆维吾尔自治区-克拉玛依市-独山子区', '364', '02', '04', '01', '3431', '', null, null);
INSERT INTO t_sys_area VALUES ('3432', '650203', '650203', '650203', '克拉玛依区', 'Karamay Qu', 'Karamay Qu', ',32,364,3432,', '新疆维吾尔自治区-克拉玛依市-克拉玛依区', '364', '02', '04', '01', '3432', '', null, null);
INSERT INTO t_sys_area VALUES ('3433', '650204', '650204', '650204', '白碱滩区', 'Baijiantan Qu', 'Baijiantan Qu', ',32,364,3433,', '新疆维吾尔自治区-克拉玛依市-白碱滩区', '364', '02', '04', '01', '3433', '', null, null);
INSERT INTO t_sys_area VALUES ('3434', '650205', '650205', '650205', '乌尔禾区', 'Orku Qu', 'Orku Qu', ',32,364,3434,', '新疆维吾尔自治区-克拉玛依市-乌尔禾区', '364', '02', '04', '01', '3434', '', null, null);
INSERT INTO t_sys_area VALUES ('3435', '652101', '652101', '652101', '吐鲁番市', 'Turpan Shi', 'Turpan Shi', ',32,365,3435,', '新疆维吾尔自治区-吐鲁番地区-吐鲁番市', '365', '02', '04', '01', '3435', '', null, null);
INSERT INTO t_sys_area VALUES ('3436', '652122', '652122', '652122', '鄯善县', 'Shanshan(piqan) Xian', 'Shanshan(piqan) Xian', ',32,365,3436,', '新疆维吾尔自治区-吐鲁番地区-鄯善县', '365', '02', '04', '01', '3436', '', null, null);
INSERT INTO t_sys_area VALUES ('3437', '652123', '652123', '652123', '托克逊县', 'Toksun Xian', 'Toksun Xian', ',32,365,3437,', '新疆维吾尔自治区-吐鲁番地区-托克逊县', '365', '02', '04', '01', '3437', '', null, null);
INSERT INTO t_sys_area VALUES ('3438', '652201', '652201', '652201', '哈密市', 'Hami(kumul) Shi', 'Hami(kumul) Shi', ',32,366,3438,', '新疆维吾尔自治区-哈密地区-哈密市', '366', '02', '04', '01', '3438', '', null, null);
INSERT INTO t_sys_area VALUES ('3439', '652222', '652222', '652222', '巴里坤哈萨克自治县', 'Barkol Kazak Zizhixian', 'Barkol Kazak Zizhixian', ',32,366,3439,', '新疆维吾尔自治区-哈密地区-巴里坤哈萨克自治县', '366', '02', '04', '01', '3439', '', null, null);
INSERT INTO t_sys_area VALUES ('3440', '652223', '652223', '652223', '伊吾县', 'Yiwu(Araturuk) Xian', 'Yiwu(Araturuk) Xian', ',32,366,3440,', '新疆维吾尔自治区-哈密地区-伊吾县', '366', '02', '04', '01', '3440', '', null, null);
INSERT INTO t_sys_area VALUES ('3441', '652301', '652301', '652301', '昌吉市', 'Changji Shi', 'Changji Shi', ',32,367,3441,', '新疆维吾尔自治区-昌吉回族自治州-昌吉市', '367', '02', '04', '01', '3441', '', null, null);
INSERT INTO t_sys_area VALUES ('3442', '652302', '652302', '652302', '阜康市', 'Fukang Shi', 'Fukang Shi', ',32,367,3442,', '新疆维吾尔自治区-昌吉回族自治州-阜康市', '367', '02', '04', '01', '3442', '', null, null);
INSERT INTO t_sys_area VALUES ('3444', '652323', '652323', '652323', '呼图壁县', 'Hutubi Xian', 'Hutubi Xian', ',32,367,3444,', '新疆维吾尔自治区-昌吉回族自治州-呼图壁县', '367', '02', '04', '01', '3444', '', null, null);
INSERT INTO t_sys_area VALUES ('3445', '652324', '652324', '652324', '玛纳斯县', 'Manas Xian', 'Manas Xian', ',32,367,3445,', '新疆维吾尔自治区-昌吉回族自治州-玛纳斯县', '367', '02', '04', '01', '3445', '', null, null);
INSERT INTO t_sys_area VALUES ('3446', '652325', '652325', '652325', '奇台县', 'Qitai Xian', 'Qitai Xian', ',32,367,3446,', '新疆维吾尔自治区-昌吉回族自治州-奇台县', '367', '02', '04', '01', '3446', '', null, null);
INSERT INTO t_sys_area VALUES ('3447', '652327', '652327', '652327', '吉木萨尔县', 'Jimsar Xian', 'Jimsar Xian', ',32,367,3447,', '新疆维吾尔自治区-昌吉回族自治州-吉木萨尔县', '367', '02', '04', '01', '3447', '', null, null);
INSERT INTO t_sys_area VALUES ('3448', '652328', '652328', '652328', '木垒哈萨克自治县', 'Mori Kazak Zizhixian', 'Mori Kazak Zizhixian', ',32,367,3448,', '新疆维吾尔自治区-昌吉回族自治州-木垒哈萨克自治县', '367', '02', '04', '01', '3448', '', null, null);
INSERT INTO t_sys_area VALUES ('3449', '652701', '652701', '652701', '博乐市', 'Bole(Bortala) Shi', 'Bole(Bortala) Shi', ',32,368,3449,', '新疆维吾尔自治区-博尔塔拉蒙古自治州-博乐市', '368', '02', '04', '01', '3449', '', null, null);
INSERT INTO t_sys_area VALUES ('3450', '652722', '652722', '652722', '精河县', 'Jinghe(Jing) Xian', 'Jinghe(Jing) Xian', ',32,368,3450,', '新疆维吾尔自治区-博尔塔拉蒙古自治州-精河县', '368', '02', '04', '01', '3450', '', null, null);
INSERT INTO t_sys_area VALUES ('3451', '652723', '652723', '652723', '温泉县', 'Wenquan(Arixang) Xian', 'Wenquan(Arixang) Xian', ',32,368,3451,', '新疆维吾尔自治区-博尔塔拉蒙古自治州-温泉县', '368', '02', '04', '01', '3451', '', null, null);
INSERT INTO t_sys_area VALUES ('3452', '652801', '652801', '652801', '库尔勒市', 'Korla Shi', 'Korla Shi', ',32,369,3452,', '新疆维吾尔自治区-巴音郭楞蒙古自治州-库尔勒市', '369', '02', '04', '01', '3452', '', null, null);
INSERT INTO t_sys_area VALUES ('3453', '652822', '652822', '652822', '轮台县', 'Luntai(Bugur) Xian', 'Luntai(Bugur) Xian', ',32,369,3453,', '新疆维吾尔自治区-巴音郭楞蒙古自治州-轮台县', '369', '02', '04', '01', '3453', '', null, null);
INSERT INTO t_sys_area VALUES ('3454', '652823', '652823', '652823', '尉犁县', 'Yuli(Lopnur) Xian', 'Yuli(Lopnur) Xian', ',32,369,3454,', '新疆维吾尔自治区-巴音郭楞蒙古自治州-尉犁县', '369', '02', '04', '01', '3454', '', null, null);
INSERT INTO t_sys_area VALUES ('3455', '652824', '652824', '652824', '若羌县', 'Ruoqiang(Qakilik) Xian', 'Ruoqiang(Qakilik) Xian', ',32,369,3455,', '新疆维吾尔自治区-巴音郭楞蒙古自治州-若羌县', '369', '02', '04', '01', '3455', '', null, null);
INSERT INTO t_sys_area VALUES ('3456', '652825', '652825', '652825', '且末县', 'Qiemo(Qarqan) Xian', 'Qiemo(Qarqan) Xian', ',32,369,3456,', '新疆维吾尔自治区-巴音郭楞蒙古自治州-且末县', '369', '02', '04', '01', '3456', '', null, null);
INSERT INTO t_sys_area VALUES ('3457', '652826', '652826', '652826', '焉耆回族自治县', 'Yanqi Huizu Zizhixian', 'Yanqi Huizu Zizhixian', ',32,369,3457,', '新疆维吾尔自治区-巴音郭楞蒙古自治州-焉耆回族自治县', '369', '02', '04', '01', '3457', '', null, null);
INSERT INTO t_sys_area VALUES ('3458', '652827', '652827', '652827', '和静县', 'Hejing Xian', 'Hejing Xian', ',32,369,3458,', '新疆维吾尔自治区-巴音郭楞蒙古自治州-和静县', '369', '02', '04', '01', '3458', '', null, null);
INSERT INTO t_sys_area VALUES ('3459', '652828', '652828', '652828', '和硕县', 'Hoxud Xian', 'Hoxud Xian', ',32,369,3459,', '新疆维吾尔自治区-巴音郭楞蒙古自治州-和硕县', '369', '02', '04', '01', '3459', '', null, null);
INSERT INTO t_sys_area VALUES ('3460', '652829', '652829', '652829', '博湖县', 'Bohu(Bagrax) Xian', 'Bohu(Bagrax) Xian', ',32,369,3460,', '新疆维吾尔自治区-巴音郭楞蒙古自治州-博湖县', '369', '02', '04', '01', '3460', '', null, null);
INSERT INTO t_sys_area VALUES ('3461', '652901', '652901', '652901', '阿克苏市', 'Aksu Shi', 'Aksu Shi', ',32,370,3461,', '新疆维吾尔自治区-阿克苏地区-阿克苏市', '370', '02', '04', '01', '3461', '', null, null);
INSERT INTO t_sys_area VALUES ('3462', '652922', '652922', '652922', '温宿县', 'Wensu Xian', 'Wensu Xian', ',32,370,3462,', '新疆维吾尔自治区-阿克苏地区-温宿县', '370', '02', '04', '01', '3462', '', null, null);
INSERT INTO t_sys_area VALUES ('3463', '652923', '652923', '652923', '库车县', 'Kuqa Xian', 'Kuqa Xian', ',32,370,3463,', '新疆维吾尔自治区-阿克苏地区-库车县', '370', '02', '04', '01', '3463', '', null, null);
INSERT INTO t_sys_area VALUES ('3464', '652924', '652924', '652924', '沙雅县', 'Xayar Xian', 'Xayar Xian', ',32,370,3464,', '新疆维吾尔自治区-阿克苏地区-沙雅县', '370', '02', '04', '01', '3464', '', null, null);
INSERT INTO t_sys_area VALUES ('3465', '652925', '652925', '652925', '新和县', 'Xinhe(Toksu) Xian', 'Xinhe(Toksu) Xian', ',32,370,3465,', '新疆维吾尔自治区-阿克苏地区-新和县', '370', '02', '04', '01', '3465', '', null, null);
INSERT INTO t_sys_area VALUES ('3466', '652926', '652926', '652926', '拜城县', 'Baicheng(Bay) Xian', 'Baicheng(Bay) Xian', ',32,370,3466,', '新疆维吾尔自治区-阿克苏地区-拜城县', '370', '02', '04', '01', '3466', '', null, null);
INSERT INTO t_sys_area VALUES ('3467', '652927', '652927', '652927', '乌什县', 'Wushi(Uqturpan) Xian', 'Wushi(Uqturpan) Xian', ',32,370,3467,', '新疆维吾尔自治区-阿克苏地区-乌什县', '370', '02', '04', '01', '3467', '', null, null);
INSERT INTO t_sys_area VALUES ('3468', '652928', '652928', '652928', '阿瓦提县', 'Awat Xian', 'Awat Xian', ',32,370,3468,', '新疆维吾尔自治区-阿克苏地区-阿瓦提县', '370', '02', '04', '01', '3468', '', null, null);
INSERT INTO t_sys_area VALUES ('3469', '652929', '652929', '652929', '柯坪县', 'Kalpin Xian', 'Kalpin Xian', ',32,370,3469,', '新疆维吾尔自治区-阿克苏地区-柯坪县', '370', '02', '04', '01', '3469', '', null, null);
INSERT INTO t_sys_area VALUES ('3470', '653001', '653001', '653001', '阿图什市', 'Artux Shi', 'Artux Shi', ',32,371,3470,', '新疆维吾尔自治区-克孜勒苏柯尔克孜自治州-阿图什市', '371', '02', '04', '01', '3470', '', null, null);
INSERT INTO t_sys_area VALUES ('3471', '653022', '653022', '653022', '阿克陶县', 'Akto Xian', 'Akto Xian', ',32,371,3471,', '新疆维吾尔自治区-克孜勒苏柯尔克孜自治州-阿克陶县', '371', '02', '04', '01', '3471', '', null, null);
INSERT INTO t_sys_area VALUES ('3472', '653023', '653023', '653023', '阿合奇县', 'Akqi Xian', 'Akqi Xian', ',32,371,3472,', '新疆维吾尔自治区-克孜勒苏柯尔克孜自治州-阿合奇县', '371', '02', '04', '01', '3472', '', null, null);
INSERT INTO t_sys_area VALUES ('3473', '653024', '653024', '653024', '乌恰县', 'Wuqia(Ulugqat) Xian', 'Wuqia(Ulugqat) Xian', ',32,371,3473,', '新疆维吾尔自治区-克孜勒苏柯尔克孜自治州-乌恰县', '371', '02', '04', '01', '3473', '', null, null);
INSERT INTO t_sys_area VALUES ('3474', '653101', '653101', '653101', '喀什市', 'Kashi (Kaxgar) Shi', 'Kashi (Kaxgar) Shi', ',32,372,3474,', '新疆维吾尔自治区-喀什地区-喀什市', '372', '02', '04', '01', '3474', '', null, null);
INSERT INTO t_sys_area VALUES ('3475', '653121', '653121', '653121', '疏附县', 'Shufu Xian', 'Shufu Xian', ',32,372,3475,', '新疆维吾尔自治区-喀什地区-疏附县', '372', '02', '04', '01', '3475', '', null, null);
INSERT INTO t_sys_area VALUES ('3476', '653122', '653122', '653122', '疏勒县', 'Shule Xian', 'Shule Xian', ',32,372,3476,', '新疆维吾尔自治区-喀什地区-疏勒县', '372', '02', '04', '01', '3476', '', null, null);
INSERT INTO t_sys_area VALUES ('3477', '653123', '653123', '653123', '英吉沙县', 'Yengisar Xian', 'Yengisar Xian', ',32,372,3477,', '新疆维吾尔自治区-喀什地区-英吉沙县', '372', '02', '04', '01', '3477', '', null, null);
INSERT INTO t_sys_area VALUES ('3478', '653124', '653124', '653124', '泽普县', 'Zepu(Poskam) Xian', 'Zepu(Poskam) Xian', ',32,372,3478,', '新疆维吾尔自治区-喀什地区-泽普县', '372', '02', '04', '01', '3478', '', null, null);
INSERT INTO t_sys_area VALUES ('3479', '653125', '653125', '653125', '莎车县', 'Shache(Yarkant) Xian', 'Shache(Yarkant) Xian', ',32,372,3479,', '新疆维吾尔自治区-喀什地区-莎车县', '372', '02', '04', '01', '3479', '', null, null);
INSERT INTO t_sys_area VALUES ('3480', '653126', '653126', '653126', '叶城县', 'Yecheng(Kargilik) Xian', 'Yecheng(Kargilik) Xian', ',32,372,3480,', '新疆维吾尔自治区-喀什地区-叶城县', '372', '02', '04', '01', '3480', '', null, null);
INSERT INTO t_sys_area VALUES ('3481', '653127', '653127', '653127', '麦盖提县', 'Markit Xian', 'Markit Xian', ',32,372,3481,', '新疆维吾尔自治区-喀什地区-麦盖提县', '372', '02', '04', '01', '3481', '', null, null);
INSERT INTO t_sys_area VALUES ('3482', '653128', '653128', '653128', '岳普湖县', 'Yopurga Xian', 'Yopurga Xian', ',32,372,3482,', '新疆维吾尔自治区-喀什地区-岳普湖县', '372', '02', '04', '01', '3482', '', null, null);
INSERT INTO t_sys_area VALUES ('3483', '653129', '653129', '653129', '伽师县', 'Jiashi(Payzawat) Xian', 'Jiashi(Payzawat) Xian', ',32,372,3483,', '新疆维吾尔自治区-喀什地区-伽师县', '372', '02', '04', '01', '3483', '', null, null);
INSERT INTO t_sys_area VALUES ('3484', '653130', '653130', '653130', '巴楚县', 'Bachu(Maralbexi) Xian', 'Bachu(Maralbexi) Xian', ',32,372,3484,', '新疆维吾尔自治区-喀什地区-巴楚县', '372', '02', '04', '01', '3484', '', null, null);
INSERT INTO t_sys_area VALUES ('3485', '653131', '653131', '653131', '塔什库尔干塔吉克自治县', 'Taxkorgan Tajik Zizhixian', 'Taxkorgan Tajik Zizhixian', ',32,372,3485,', '新疆维吾尔自治区-喀什地区-塔什库尔干塔吉克自治县', '372', '02', '04', '01', '3485', '', null, null);
INSERT INTO t_sys_area VALUES ('3486', '653201', '653201', '653201', '和田市', 'Hotan Shi', 'Hotan Shi', ',32,373,3486,', '新疆维吾尔自治区-和田地区-和田市', '373', '02', '04', '01', '3486', '', null, null);
INSERT INTO t_sys_area VALUES ('3487', '653221', '653221', '653221', '和田县', 'Hotan Xian', 'Hotan Xian', ',32,373,3487,', '新疆维吾尔自治区-和田地区-和田县', '373', '02', '04', '01', '3487', '', null, null);
INSERT INTO t_sys_area VALUES ('3488', '653222', '653222', '653222', '墨玉县', 'Moyu(Karakax) Xian', 'Moyu(Karakax) Xian', ',32,373,3488,', '新疆维吾尔自治区-和田地区-墨玉县', '373', '02', '04', '01', '3488', '', null, null);
INSERT INTO t_sys_area VALUES ('3489', '653223', '653223', '653223', '皮山县', 'Pishan(Guma) Xian', 'Pishan(Guma) Xian', ',32,373,3489,', '新疆维吾尔自治区-和田地区-皮山县', '373', '02', '04', '01', '3489', '', null, null);
INSERT INTO t_sys_area VALUES ('3490', '653224', '653224', '653224', '洛浦县', 'Lop Xian', 'Lop Xian', ',32,373,3490,', '新疆维吾尔自治区-和田地区-洛浦县', '373', '02', '04', '01', '3490', '', null, null);
INSERT INTO t_sys_area VALUES ('3491', '653225', '653225', '653225', '策勒县', 'Qira Xian', 'Qira Xian', ',32,373,3491,', '新疆维吾尔自治区-和田地区-策勒县', '373', '02', '04', '01', '3491', '', null, null);
INSERT INTO t_sys_area VALUES ('3492', '653226', '653226', '653226', '于田县', 'Yutian(Keriya) Xian', 'Yutian(Keriya) Xian', ',32,373,3492,', '新疆维吾尔自治区-和田地区-于田县', '373', '02', '04', '01', '3492', '', null, null);
INSERT INTO t_sys_area VALUES ('3493', '653227', '653227', '653227', '民丰县', 'Minfeng(Niya) Xian', 'Minfeng(Niya) Xian', ',32,373,3493,', '新疆维吾尔自治区-和田地区-民丰县', '373', '02', '04', '01', '3493', '', null, null);
INSERT INTO t_sys_area VALUES ('3494', '654002', '654002', '654002', '伊宁市', 'Yining(Gulja) Shi', 'Yining(Gulja) Shi', ',32,374,3494,', '新疆维吾尔自治区-伊犁哈萨克自治州-伊宁市', '374', '02', '04', '01', '3494', '', null, null);
INSERT INTO t_sys_area VALUES ('3495', '654003', '654003', '654003', '奎屯市', 'Kuytun Shi', 'Kuytun Shi', ',32,374,3495,', '新疆维吾尔自治区-伊犁哈萨克自治州-奎屯市', '374', '02', '04', '01', '3495', '', null, null);
INSERT INTO t_sys_area VALUES ('3496', '654021', '654021', '654021', '伊宁县', 'Yining(Gulja) Xian', 'Yining(Gulja) Xian', ',32,374,3496,', '新疆维吾尔自治区-伊犁哈萨克自治州-伊宁县', '374', '02', '04', '01', '3496', '', null, null);
INSERT INTO t_sys_area VALUES ('3497', '654022', '654022', '654022', '察布查尔锡伯自治县', 'Qapqal Xibe Zizhixian', 'Qapqal Xibe Zizhixian', ',32,374,3497,', '新疆维吾尔自治区-伊犁哈萨克自治州-察布查尔锡伯自治县', '374', '02', '04', '01', '3497', '', null, null);
INSERT INTO t_sys_area VALUES ('3498', '654023', '654023', '654023', '霍城县', 'Huocheng Xin', 'Huocheng Xin', ',32,374,3498,', '新疆维吾尔自治区-伊犁哈萨克自治州-霍城县', '374', '02', '04', '01', '3498', '', null, null);
INSERT INTO t_sys_area VALUES ('3499', '654024', '654024', '654024', '巩留县', 'Gongliu(Tokkuztara) Xian', 'Gongliu(Tokkuztara) Xian', ',32,374,3499,', '新疆维吾尔自治区-伊犁哈萨克自治州-巩留县', '374', '02', '04', '01', '3499', '', null, null);
INSERT INTO t_sys_area VALUES ('3500', '654025', '654025', '654025', '新源县', 'Xinyuan(Kunes) Xian', 'Xinyuan(Kunes) Xian', ',32,374,3500,', '新疆维吾尔自治区-伊犁哈萨克自治州-新源县', '374', '02', '04', '01', '3500', '', null, null);
INSERT INTO t_sys_area VALUES ('3501', '654026', '654026', '654026', '昭苏县', 'Zhaosu(Mongolkure) Xian', 'Zhaosu(Mongolkure) Xian', ',32,374,3501,', '新疆维吾尔自治区-伊犁哈萨克自治州-昭苏县', '374', '02', '04', '01', '3501', '', null, null);
INSERT INTO t_sys_area VALUES ('3502', '654027', '654027', '654027', '特克斯县', 'Tekes Xian', 'Tekes Xian', ',32,374,3502,', '新疆维吾尔自治区-伊犁哈萨克自治州-特克斯县', '374', '02', '04', '01', '3502', '', null, null);
INSERT INTO t_sys_area VALUES ('3503', '654028', '654028', '654028', '尼勒克县', 'Nilka Xian', 'Nilka Xian', ',32,374,3503,', '新疆维吾尔自治区-伊犁哈萨克自治州-尼勒克县', '374', '02', '04', '01', '3503', '', null, null);
INSERT INTO t_sys_area VALUES ('3504', '654201', '654201', '654201', '塔城市', 'Tacheng(Qoqek) Shi', 'Tacheng(Qoqek) Shi', ',32,375,3504,', '新疆维吾尔自治区-塔城地区-塔城市', '375', '02', '04', '01', '3504', '', null, null);
INSERT INTO t_sys_area VALUES ('3505', '654202', '654202', '654202', '乌苏市', 'Usu Shi', 'Usu Shi', ',32,375,3505,', '新疆维吾尔自治区-塔城地区-乌苏市', '375', '02', '04', '01', '3505', '', null, null);
INSERT INTO t_sys_area VALUES ('3506', '654221', '654221', '654221', '额敏县', 'Emin(Dorbiljin) Xian', 'Emin(Dorbiljin) Xian', ',32,375,3506,', '新疆维吾尔自治区-塔城地区-额敏县', '375', '02', '04', '01', '3506', '', null, null);
INSERT INTO t_sys_area VALUES ('3507', '654223', '654223', '654223', '沙湾县', 'Shawan Xian', 'Shawan Xian', ',32,375,3507,', '新疆维吾尔自治区-塔城地区-沙湾县', '375', '02', '04', '01', '3507', '', null, null);
INSERT INTO t_sys_area VALUES ('3508', '654224', '654224', '654224', '托里县', 'Toli Xian', 'Toli Xian', ',32,375,3508,', '新疆维吾尔自治区-塔城地区-托里县', '375', '02', '04', '01', '3508', '', null, null);
INSERT INTO t_sys_area VALUES ('3509', '654225', '654225', '654225', '裕民县', 'Yumin(Qagantokay) Xian', 'Yumin(Qagantokay) Xian', ',32,375,3509,', '新疆维吾尔自治区-塔城地区-裕民县', '375', '02', '04', '01', '3509', '', null, null);
INSERT INTO t_sys_area VALUES ('3510', '654226', '654226', '654226', '和布克赛尔蒙古自治县', 'Hebukesaiermengguzizhi Xian', 'Hebukesaiermengguzizhi Xian', ',32,375,3510,', '新疆维吾尔自治区-塔城地区-和布克赛尔蒙古自治县', '375', '02', '04', '01', '3510', '', null, null);
INSERT INTO t_sys_area VALUES ('3511', '654301', '654301', '654301', '阿勒泰市', 'Altay Shi', 'Altay Shi', ',32,376,3511,', '新疆维吾尔自治区-阿勒泰地区-阿勒泰市', '376', '02', '04', '01', '3511', '', null, null);
INSERT INTO t_sys_area VALUES ('3512', '654321', '654321', '654321', '布尔津县', 'Burqin Xian', 'Burqin Xian', ',32,376,3512,', '新疆维吾尔自治区-阿勒泰地区-布尔津县', '376', '02', '04', '01', '3512', '', null, null);
INSERT INTO t_sys_area VALUES ('3513', '654322', '654322', '654322', '富蕴县', 'Fuyun(Koktokay) Xian', 'Fuyun(Koktokay) Xian', ',32,376,3513,', '新疆维吾尔自治区-阿勒泰地区-富蕴县', '376', '02', '04', '01', '3513', '', null, null);
INSERT INTO t_sys_area VALUES ('3514', '654323', '654323', '654323', '福海县', 'Fuhai(Burultokay) Xian', 'Fuhai(Burultokay) Xian', ',32,376,3514,', '新疆维吾尔自治区-阿勒泰地区-福海县', '376', '02', '04', '01', '3514', '', null, null);
INSERT INTO t_sys_area VALUES ('3515', '654324', '654324', '654324', '哈巴河县', 'Habahe(Kaba) Xian', 'Habahe(Kaba) Xian', ',32,376,3515,', '新疆维吾尔自治区-阿勒泰地区-哈巴河县', '376', '02', '04', '01', '3515', '', null, null);
INSERT INTO t_sys_area VALUES ('3516', '654325', '654325', '654325', '青河县', 'Qinghe(Qinggil) Xian', 'Qinghe(Qinggil) Xian', ',32,376,3516,', '新疆维吾尔自治区-阿勒泰地区-青河县', '376', '02', '04', '01', '3516', '', null, null);
INSERT INTO t_sys_area VALUES ('3517', '654326', '654326', '654326', '吉木乃县', 'Jeminay Xian', 'Jeminay Xian', ',32,376,3517,', '新疆维吾尔自治区-阿勒泰地区-吉木乃县', '376', '02', '04', '01', '3517', '', null, null);
INSERT INTO t_sys_area VALUES ('3518', '659001', '659001', '659001', '石河子市', 'Shihezi Shi', 'Shihezi Shi', ',32,377,3518,', '新疆维吾尔自治区-自治区直辖县级行政区划-石河子市', '377', '02', '04', '01', '3518', '', null, null);
INSERT INTO t_sys_area VALUES ('3519', '659002', '659002', '659002', '阿拉尔市', 'Alaer Shi', 'Alaer Shi', ',32,377,3519,', '新疆维吾尔自治区-自治区直辖县级行政区划-阿拉尔市', '377', '02', '04', '01', '3519', '', null, null);
INSERT INTO t_sys_area VALUES ('3520', '659003', '659003', '659003', '图木舒克市', 'Tumushuke Shi', 'Tumushuke Shi', ',32,377,3520,', '新疆维吾尔自治区-自治区直辖县级行政区划-图木舒克市', '377', '02', '04', '01', '3520', '', null, null);
INSERT INTO t_sys_area VALUES ('3521', '659004', '659004', '659004', '五家渠市', 'Wujiaqu Shi', 'Wujiaqu Shi', ',32,377,3521,', '新疆维吾尔自治区-自治区直辖县级行政区划-五家渠市', '377', '02', '04', '01', '3521', '', null, null);
INSERT INTO t_sys_area VALUES ('4000', '620503', '620503', '620503', '麦积区', 'Maiji Qu', 'Maiji Qu', ',29,340,4000,', '甘肃省-天水市-麦积区', '340', '02', '04', '01', '4000', '', null, null);
INSERT INTO t_sys_area VALUES ('4001', '500116', '500116', '500116', '江津区', 'Jiangjin Qu', 'Jiangjin Qu', ',23,270,4001,', '重庆-重庆市-江津区', '270', '02', '04', '01', '4001', '', null, null);
INSERT INTO t_sys_area VALUES ('4002', '500117', '500117', '500117', '合川区', 'Hechuan Qu', 'Hechuan Qu', ',23,270,4002,', '重庆-重庆市-合川区', '270', '02', '04', '01', '4002', '', null, null);
INSERT INTO t_sys_area VALUES ('4003', '500118', '500118', '500118', '永川区', 'Yongchuan Qu', 'Yongchuan Qu', ',23,270,4003,', '重庆-重庆市-永川区', '270', '02', '04', '01', '4003', '', null, null);
INSERT INTO t_sys_area VALUES ('4004', '500119', '500119', '500119', '南川区', 'Nanchuan Qu', 'Nanchuan Qu', ',23,270,4004,', '重庆-重庆市-南川区', '270', '02', '04', '01', '4004', '', null, null);
INSERT INTO t_sys_area VALUES ('4006', '340221', '340221', '340221', '芜湖县', 'Wuhu Xian', 'Wuhu Xian', ',13,134,4006,', '安徽省-芜湖市-芜湖县', '134', '02', '04', '01', '4006', '', null, null);
INSERT INTO t_sys_area VALUES ('4100', '232701', '232701', '232701', '加格达奇区', 'Jiagedaqi Qu', 'Jiagedaqi Qu', ',9,106,4100,', '黑龙江省-大兴安岭地区-加格达奇区', '106', '02', '04', '01', '4100', '', null, null);
INSERT INTO t_sys_area VALUES ('4101', '232702', '232702', '232702', '松岭区', 'Songling Qu', 'Songling Qu', ',9,106,4101,', '黑龙江省-大兴安岭地区-松岭区', '106', '02', '04', '01', '4101', '', null, null);
INSERT INTO t_sys_area VALUES ('4102', '232703', '232703', '232703', '新林区', 'Xinlin Qu', 'Xinlin Qu', ',9,106,4102,', '黑龙江省-大兴安岭地区-新林区', '106', '02', '04', '01', '4102', '', null, null);
INSERT INTO t_sys_area VALUES ('4103', '232704', '232704', '232704', '呼中区', 'Huzhong Qu', 'Huzhong Qu', ',9,106,4103,', '黑龙江省-大兴安岭地区-呼中区', '106', '02', '04', '01', '4103', '', null, null);
INSERT INTO t_sys_area VALUES ('4200', '330402', '330402', '330402', '南湖区', 'Nanhu Qu', 'Nanhu Qu', ',12,125,4200,', '浙江省-嘉兴市-南湖区', '125', '02', '04', '01', '4200', '', null, null);
INSERT INTO t_sys_area VALUES ('4300', '360482', '360482', '360482', '共青城市', 'Gongqingcheng Shi', 'Gongqingcheng Shi', ',15,162,4300,', '江西省-九江市-共青城市', '162', '02', '04', '01', '4300', '', null, null);
INSERT INTO t_sys_area VALUES ('4400', '640303', '640303', '640303', '红寺堡区', 'Hongsibao Qu', 'Hongsibao Qu', ',31,360,4400,', '宁夏回族自治区-吴忠市-红寺堡区', '360', '02', '04', '01', '4400', '', null, null);
INSERT INTO t_sys_area VALUES ('4500', '620922', '620922', '620922', '瓜州县', 'Guazhou Xian', 'Guazhou Xian', ',29,344,4500,', '甘肃省-酒泉市-瓜州县', '344', '02', '04', '01', '4500', '', null, null);
INSERT INTO t_sys_area VALUES ('4600', '421321', '421321', '421321', '随县', 'Sui Xian', 'Sui Xian', ',18,215,4600,', '湖北省-随州市-随县', '215', '02', '04', '01', '4600', '', null, null);
INSERT INTO t_sys_area VALUES ('4700', '431102', '431102', '431102', '零陵区', 'Lingling Qu', 'Lingling Qu', ',19,228,4700,', '湖南省-永州市-零陵区', '228', '02', '04', '01', '4700', '', null, null);
INSERT INTO t_sys_area VALUES ('4800', '451119', '451119', '451119', '平桂管理区', 'Pingguiguanli Qu', 'Pingguiguanli Qu', ',21,263,4800,', '广西壮族自治区-贺州市-平桂管理区', '263', '02', '04', '01', '4800', '', null, null);
INSERT INTO t_sys_area VALUES ('4900', '510802', '510802', '510802', '利州区', 'Lizhou Qu', 'Lizhou Qu', ',24,279,4900,', '四川省-广元市-利州区', '279', '02', '04', '01', '4900', '', null, null);
INSERT INTO t_sys_area VALUES ('5000', '511681', '511681', '511681', '华蓥市', 'Huaying Shi', 'Huaying Shi', ',24,286,5000,', '四川省-广安市-华蓥市', '286', '02', '04', '01', '5000', '', null, null);

-- ----------------------------
-- Table structure for `t_sys_atta`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_atta`;
CREATE TABLE `t_sys_atta` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '文件ID',
  `NAME` varchar(256) NOT NULL COMMENT '文件名',
  `EXT` varchar(32) DEFAULT NULL COMMENT '扩展名',
  `SIZE` bigint(11) NOT NULL COMMENT '文件大小',
  `PATH` varchar(256) NOT NULL COMMENT '文件路径',
  `MIME_TYPE` varchar(256) DEFAULT NULL COMMENT '文件类型',
  `MD5` varchar(256) DEFAULT NULL COMMENT '文件MD5',
  `CTIME` datetime NOT NULL COMMENT '创建时间',
  `USER_ID` int(11) NOT NULL COMMENT '创建人',
  `DATA_TYPE` varchar(32) DEFAULT NULL COMMENT '文件来源',
  `DATA_ID` int(11) DEFAULT NULL COMMENT '文件来源主键',
  PRIMARY KEY (`ID`),
  KEY `IDX_FILE_SRC` (`DATA_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8 COMMENT='系统附件表';

-- ----------------------------
-- Records of t_sys_atta
-- ----------------------------
INSERT INTO t_sys_atta VALUES ('204', '5.jpg', 'jpg', '122366', 'user_photo\\201711\\204.jpg', 'image/jpeg', null, '2017-11-02 18:34:40', '1', 'user_photo', '1');
INSERT INTO t_sys_atta VALUES ('205', '检测报告.jpg', 'jpg', '47646', 'notice_atta\\201711\\205.jpg', 'image/jpeg', null, '2017-11-02 18:35:06', '1', 'notice_atta', '2');
INSERT INTO t_sys_atta VALUES ('206', 'logo.png', 'png', '7462', 'notice_atta\\201711\\206.png', 'image/png', null, '2017-11-03 12:14:38', '1', 'notice_atta', '4');
INSERT INTO t_sys_atta VALUES ('208', '-ypnvquu9js5zps3e.jpg', 'jpg', '72753', 'notice_atta\\201711\\208.jpg', 'image/jpeg', null, '2017-11-03 12:19:54', '1', 'notice_atta', '6');
INSERT INTO t_sys_atta VALUES ('209', '11.jpg', 'jpg', '86472', 'notice_atta\\201711\\209.jpg', 'image/jpeg', null, '2017-11-03 17:55:07', '1', 'notice_atta', '1');
INSERT INTO t_sys_atta VALUES ('210', '5.jpg', 'jpg', '122366', 'notice_atta\\201711\\210.jpg', 'image/jpeg', null, '2017-11-03 18:50:51', '1', 'notice_atta', '6');
INSERT INTO t_sys_atta VALUES ('212', 'logo.png', 'png', '7391', 'user_photo\\201711\\212.png', 'image/png', null, '2017-11-03 22:42:37', '1', 'user_photo', '6');
INSERT INTO t_sys_atta VALUES ('213', '2.jpg', 'jpg', '79464', 'notice_atta\\201711\\213.jpg', 'image/jpeg', null, '2017-11-03 23:23:33', '1', 'notice_atta', '6');
INSERT INTO t_sys_atta VALUES ('214', '3.jpg', 'jpg', '42810', 'notice_atta\\201711\\214.jpg', 'image/jpeg', null, '2017-11-03 23:23:33', '1', 'notice_atta', '6');
INSERT INTO t_sys_atta VALUES ('215', '4.jpg', 'jpg', '121452', 'notice_atta\\201711\\215.jpg', 'image/jpeg', null, '2017-11-03 23:23:33', '1', 'notice_atta', '6');
INSERT INTO t_sys_atta VALUES ('216', '5.jpg', 'jpg', '122366', 'notice_atta\\201711\\216.jpg', 'image/jpeg', null, '2017-11-03 23:23:33', '1', 'notice_atta', '6');
INSERT INTO t_sys_atta VALUES ('217', 'logo.png', 'png', '7462', 'notice_atta\\201711\\217.png', 'image/png', null, '2017-11-03 23:23:34', '1', 'notice_atta', '6');
INSERT INTO t_sys_atta VALUES ('218', '11.jpg', 'jpg', '86472', 'notice_atta\\201711\\218.jpg', 'image/jpeg', null, '2017-11-03 23:23:34', '1', 'notice_atta', '6');
INSERT INTO t_sys_atta VALUES ('219', 'top01.jpg', 'jpg', '11314', 'notice_atta\\201711\\219.jpg', 'image/jpeg', null, '2017-11-03 23:23:34', '1', 'notice_atta', '6');
INSERT INTO t_sys_atta VALUES ('220', 'w3qf28cohpvy604z.jpg', 'jpg', '73240', 'notice_atta\\201711\\220.jpg', 'image/jpeg', null, '2017-11-03 23:23:34', '1', 'notice_atta', '6');
INSERT INTO t_sys_atta VALUES ('221', '-ypnvquu9js5zps3e.jpg', 'jpg', '72753', 'notice_atta\\201711\\221.jpg', 'image/jpeg', null, '2017-11-03 23:23:34', '1', 'notice_atta', '6');
INSERT INTO t_sys_atta VALUES ('222', '检测报告.jpg', 'jpg', '47646', 'notice_atta\\201711\\222.jpg', 'image/jpeg', null, '2017-11-03 23:23:34', '1', 'notice_atta', '6');
INSERT INTO t_sys_atta VALUES ('225', 'add.png', 'png', '4245', 'notice_atta\\201711\\225.png', 'image/png', null, '2017-11-03 23:52:24', '1', 'notice_atta', '3');
INSERT INTO t_sys_atta VALUES ('226', 'main_top.png', 'png', '193713', 'notice_atta\\201711\\226.png', 'image/png', null, '2017-11-03 23:52:59', '1', 'notice_atta', '3');
INSERT INTO t_sys_atta VALUES ('229', '道德经.txt', 'txt', '62546', 'notice_atta\\201711\\229.txt', 'text/plain', null, '2017-11-08 15:54:26', '1', 'notice_atta', '7');
INSERT INTO t_sys_atta VALUES ('230', '测试Office2003.doc', 'doc', '851456', 'notice_atta\\201711\\230.doc', 'application/msword', null, '2017-11-08 15:54:26', '1', 'notice_atta', '7');
INSERT INTO t_sys_atta VALUES ('231', '测试Office2010.docx', 'docx', '1327263', 'notice_atta\\201711\\231.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', null, '2017-11-08 15:54:26', '1', 'notice_atta', '7');
INSERT INTO t_sys_atta VALUES ('232', 'jfinal-3.2-manual.pdf', 'pdf', '2853418', 'notice_atta\\201711\\232.pdf', 'application/pdf', null, '2017-11-08 15:54:26', '1', 'notice_atta', '7');
INSERT INTO t_sys_atta VALUES ('233', '钢琴.txt', 'txt', '10930', 'notice_atta\\201711\\233.txt', 'text/plain', null, '2017-11-08 19:18:25', '1', 'notice_atta', '1');
INSERT INTO t_sys_atta VALUES ('235', '上海海鸥数码照相机有限公司.txt', 'txt', '1775', 'notice_atta\\201711\\235.txt', 'text/plain', null, '2017-11-08 23:30:25', '1', 'notice_atta', '3');
INSERT INTO t_sys_atta VALUES ('240', '测试删除.txt', 'txt', '1240', 'notice_atta\\201711\\240.txt', 'text/plain', null, '2017-11-09 12:35:23', '1', 'notice_atta', '7');
INSERT INTO t_sys_atta VALUES ('241', '移动架构图.pptx', 'pptx', '404418', 'notice_atta\\201711\\241.pptx', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', null, '2017-11-09 12:35:35', '1', 'notice_atta', '3');
INSERT INTO t_sys_atta VALUES ('242', '淘宝美工.txt', 'txt', '815', 'notice_atta\\201711\\242.txt', 'text/plain', null, '2017-11-09 13:00:35', '1', 'notice_atta', '2');
INSERT INTO t_sys_atta VALUES ('244', '3.jpg', 'jpg', '42810', 'doc_atta\\201711\\244.jpg', 'image/jpeg', null, '2017-11-10 10:12:52', '1', 'doc_atta', '2');
INSERT INTO t_sys_atta VALUES ('245', '检测报告.jpg', 'jpg', '47646', 'msg_atta\\201711\\245.jpg', 'image/jpeg', null, '2017-11-12 22:39:00', '2', 'msg_atta', '12');
INSERT INTO t_sys_atta VALUES ('246', 'maven3.0.4学习教程.docx', 'docx', '1902194', 'msg_atta\\201711\\246.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', null, '2017-11-13 12:35:56', '1', 'msg_atta', '16');

-- ----------------------------
-- Table structure for `t_sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_config`;
CREATE TABLE `t_sys_config` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '配置主键',
  `CODE` varchar(64) NOT NULL COMMENT '配置代码',
  `NAME` varchar(64) NOT NULL COMMENT '配置名称',
  `VAL` varchar(512) NOT NULL COMMENT '配置值',
  `TYPE` varchar(32) NOT NULL DEFAULT '01' COMMENT '配置类型',
  `STATE` varchar(32) NOT NULL COMMENT '配置状态',
  `SORT` int(11) NOT NULL COMMENT '排序',
  `REMARK` varchar(1024) DEFAULT NULL COMMENT '描述',
  `PARAM1` varchar(512) DEFAULT NULL COMMENT '参数1',
  `PARAM2` varchar(512) DEFAULT NULL COMMENT '参数2',
  `PARAM3` varchar(512) DEFAULT NULL COMMENT '参数3',
  `LUSER` int(11) DEFAULT NULL COMMENT '最后修改人',
  `LTIME` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UN_CONFIG_CODE` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='系统配置';

-- ----------------------------
-- Records of t_sys_config
-- ----------------------------
INSERT INTO t_sys_config VALUES ('1', 'LUCENE_INDEX_LAST_ATTA_ID', '全文检索索引附件ID', '242', '01', '01', '10', null, null, null, null, '1', '2017-11-10 12:46:32');

-- ----------------------------
-- Table structure for `t_sys_dict`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_dict`;
CREATE TABLE `t_sys_dict` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `PARENT_ID` int(11) NOT NULL COMMENT '上级节点',
  `VAL` varchar(128) NOT NULL COMMENT '代码值',
  `NAME` varchar(128) NOT NULL COMMENT '代码中文名称',
  `ENAME` varchar(128) DEFAULT NULL COMMENT '代码英文名称',
  `TYPE` varchar(64) NOT NULL COMMENT '代码类型',
  `STATE` varchar(32) NOT NULL COMMENT '状态',
  `SORT` int(11) NOT NULL COMMENT '排序号',
  `REMARK` varchar(1024) DEFAULT NULL COMMENT '描述',
  `PARAM1` varchar(512) DEFAULT NULL COMMENT '参数1',
  `PARAM2` varchar(512) DEFAULT NULL COMMENT '参数2',
  `PARAM3` varchar(512) DEFAULT NULL COMMENT '参数3',
  `LUSER` int(11) DEFAULT NULL COMMENT '最后修改人',
  `LTIME` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UN_DICT_VAL` (`PARENT_ID`,`VAL`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8 COMMENT='系统代码表';

-- ----------------------------
-- Records of t_sys_dict
-- ----------------------------
INSERT INTO t_sys_dict VALUES ('1', '0', 'DICT_TYPE', '字典类型', null, '01', '01', '4', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('2', '1', '01', '系统配置', '', '01', '01', '1', '系统核心配置', null, null, null, '0', '2017-09-25 19:19:03');
INSERT INTO t_sys_dict VALUES ('3', '1', '02', '系统扩展', '', '01', '01', '2', '系统扩展配置', null, null, null, '0', '2017-09-25 19:22:51');
INSERT INTO t_sys_dict VALUES ('4', '1', '03', '应用配置', '', '01', '01', '3', '业务系统扩展配置', null, null, null, '0', '2017-09-25 19:24:57');
INSERT INTO t_sys_dict VALUES ('5', '1', '04', '其它扩展', null, '01', '01', '4', null, null, null, null, '0', null);
INSERT INTO t_sys_dict VALUES ('10', '0', 'DATA_STATE', '数据有效性', '', '01', '01', '1', '', null, null, null, '0', '2017-09-25 23:51:47');
INSERT INTO t_sys_dict VALUES ('11', '10', '01', '有效', 'VALID', '01', '01', '0', null, null, null, null, '0', '2017-10-01 13:35:51');
INSERT INTO t_sys_dict VALUES ('12', '10', '02', '无效', 'INVALID', '01', '01', '1', null, null, null, null, '1', '2017-10-26 21:48:50');
INSERT INTO t_sys_dict VALUES ('15', '0', 'YES_OR_NO', '是否标志', null, '01', '01', '2', null, null, null, null, '1', '2017-11-10 12:27:52');
INSERT INTO t_sys_dict VALUES ('16', '15', '01', '是', null, '01', '01', '1', null, null, null, null, null, '2017-09-28 17:56:01');
INSERT INTO t_sys_dict VALUES ('17', '15', '02', '否', null, '01', '01', '2', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('20', '0', 'HAS_OR_NO', '是否拥有', null, '01', '01', '3', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('21', '20', '01', '有', null, '01', '01', '1', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('22', '20', '02', '无', null, '01', '01', '2', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('25', '0', 'AREA_LEVEL', '区域级别', null, '01', '01', '4', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('26', '25', '01', '国家', null, '01', '01', '1', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('27', '25', '02', '省', null, '01', '01', '2', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('28', '25', '03', '市', null, '01', '01', '3', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('29', '25', '04', '区县', null, '01', '01', '4', null, null, null, null, '1', '2017-10-18 18:41:24');
INSERT INTO t_sys_dict VALUES ('30', '0', 'USER_STATE', '用户状态', null, '02', '01', '4', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('31', '30', '01', '正常', null, '02', '01', '1', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('32', '30', '02', '禁用', null, '02', '01', '2', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('33', '30', '03', '删除', null, '02', '01', '3', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('35', '0', 'ORG_TYPE', '机构类型', null, '02', '01', '2', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('36', '35', '01', '单位', null, '02', '01', '1', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('37', '35', '02', '部门', null, '02', '01', '2', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('40', '0', 'ROLE_TYPE', '角色类型', null, '02', '01', '1', null, null, null, null, '1', '2017-10-27 17:27:37');
INSERT INTO t_sys_dict VALUES ('41', '40', '01', '超级管理员', null, '02', '01', '1', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('42', '40', '02', '系统管理员', null, '02', '01', '2', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('43', '40', '03', '单位领导', null, '02', '01', '3', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('44', '40', '04', '部门领导', null, '02', '01', '4', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('45', '40', '05', '普通员工', null, '02', '01', '5', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('55', '0', 'SUCCESS_OR_FAILED', '是否成功', null, '01', '01', '5', null, null, null, null, '1', '2017-11-06 19:04:01');
INSERT INTO t_sys_dict VALUES ('56', '55', '01', '成功', null, '01', '01', '0', null, null, null, null, '1', '2017-10-27 17:23:47');
INSERT INTO t_sys_dict VALUES ('57', '55', '02', '失败', null, '01', '01', '1', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('60', '0', 'SCHEDULER_STATE', '任务状态', null, '01', '01', '6', null, null, null, null, '1', '2017-11-06 18:40:30');
INSERT INTO t_sys_dict VALUES ('61', '60', 'NONE', '休眠', null, '01', '01', '1', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('62', '60', 'NORMAL', '正常', null, '01', '01', '2', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('63', '60', 'PAUSED', '暂停', null, '01', '01', '3', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('64', '60', 'COMPLETE', '完整', null, '01', '01', '4', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('65', '60', 'BLOCKED', '阻塞', null, '01', '01', '6', null, null, null, null, '1', '2017-11-06 18:42:18');
INSERT INTO t_sys_dict VALUES ('66', '60', 'ERROR', '错误', null, '01', '01', '5', null, null, null, null, '1', '2017-11-06 18:42:06');
INSERT INTO t_sys_dict VALUES ('67', '60', 'DISABLE', '禁用', null, '01', '01', '7', null, null, null, null, null, null);
INSERT INTO t_sys_dict VALUES ('68', '60', 'UNKNOWN', '未知', null, '01', '01', '8', null, null, null, null, '1', '2017-11-06 19:04:05');
INSERT INTO t_sys_dict VALUES ('77', '0', 'NOTICE_TYPE', '公告类型', null, '02', '01', '3', null, null, null, null, '1', '2017-11-02 13:07:10');
INSERT INTO t_sys_dict VALUES ('78', '77', '01', '通知', null, '02', '01', '0', null, null, null, null, '1', '2017-11-02 13:07:27');
INSERT INTO t_sys_dict VALUES ('79', '77', '02', '公告', null, '02', '01', '1', null, null, null, null, '1', '2017-11-06 19:02:37');
INSERT INTO t_sys_dict VALUES ('85', '0', 'FEEDBACK_STATE', '反馈状态', null, '02', '01', '6', null, null, null, null, '1', '2017-11-10 12:28:12');
INSERT INTO t_sys_dict VALUES ('86', '85', '01', '新增', null, '02', '01', '1', null, null, null, null, '1', '2017-11-10 12:28:15');
INSERT INTO t_sys_dict VALUES ('87', '85', '02', '已回复', null, '02', '01', '2', null, null, null, null, '1', '2017-11-10 12:28:17');
INSERT INTO t_sys_dict VALUES ('95', '0', 'FEEDBACK_TYPE', '反馈类型', null, '02', '01', '5', null, null, null, null, '0', '2017-10-01 13:35:08');
INSERT INTO t_sys_dict VALUES ('96', '95', '01', '系统错误', null, '02', '01', '3', null, null, null, null, '1', '2017-11-10 12:32:34');
INSERT INTO t_sys_dict VALUES ('97', '95', '02', '优化建议', null, '02', '01', '4', null, null, null, null, '1', '2017-11-10 12:32:42');

-- ----------------------------
-- Table structure for `t_sys_org`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_org`;
CREATE TABLE `t_sys_org` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '机构主键',
  `CODE` varchar(64) NOT NULL COMMENT '机构编码',
  `NAME` varchar(64) NOT NULL COMMENT '机构名称',
  `PARENT_ID` int(11) NOT NULL COMMENT '上级机构',
  `TYPE` varchar(32) NOT NULL COMMENT '机构类型',
  `STATE` varchar(32) NOT NULL COMMENT '机构状态',
  `SORT` int(11) NOT NULL COMMENT '排序号',
  `PATH` varchar(256) DEFAULT NULL COMMENT '机构路径',
  `REMARK` varchar(1024) DEFAULT NULL COMMENT '机构描述',
  `PARAM1` varchar(512) DEFAULT NULL COMMENT '参数1',
  `PARAM2` varchar(512) DEFAULT NULL COMMENT '参数2',
  `PARAM3` varchar(512) DEFAULT NULL COMMENT '参数3',
  `LUSER` int(11) DEFAULT NULL COMMENT '最后修改人',
  `LTIME` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UN_ORG_CODE` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='系统机构';

-- ----------------------------
-- Records of t_sys_org
-- ----------------------------
INSERT INTO t_sys_org VALUES ('1', 'YJKJ', '云间科技', '0', '01', '01', '2', ',1,', null, null, null, null, '0', '2017-10-02 21:57:33');
INSERT INTO t_sys_org VALUES ('2', 'DEV', '开发部', '1', '02', '01', '2', ',1,2,', null, null, null, null, '0', '2017-10-02 21:57:36');
INSERT INTO t_sys_org VALUES ('3', 'OA', 'OA项目组', '2', '02', '01', '0', ',1,2,3,', null, null, null, null, '0', '2017-10-02 21:57:39');
INSERT INTO t_sys_org VALUES ('5', 'NWXX', '纳纬信息', '0', '01', '02', '1', ',5,', '9月30日晚22：30分左右，李干杰一行到达北京市新发地农产品批发市场。该市场每天批发蔬菜、水果等农产品4万多吨，日均车流量达到3万多辆，其中重型柴油货车3000多辆，曾经是超标排放车辆集中区域。时值初秋深夜，市场内柴油货车进进出出，一派繁荣景象。看到环境监察人员正在拦截查看柴油车尾气排放情况，李干杰详细询问市场车辆进出频次、基层环保部门日常监管手段以及超标车辆检出率等相关情况，并不时与被检测车辆驾驶员进行交谈，了解车辆拉载果蔬种类、行驶途经地、车用柴油购买使用情况等。他还俯下身子、蹲在地上，和执法人员一道借着手电光照，接连对多辆柴油车进行检查，查看尾气管排放是否冒黑烟、车用尿素是否正常加注使用。\r\n之后，李干杰一行绕行南六环高速公路，沿途查看检查北京市9月21日起对柴油车采取新管控措施后的落实情况。\r\n10月1日凌晨0：30分左右，李干杰一行到达琉璃河综合检查站。该检查站位于107国道北京市房山区琉璃河镇，与河北省涿州市交界，每天有三五百辆重型柴油货车通行，也是查处超标排放车辆比较多的区域。看到环保、公安和交通部门的人员正在联合执法检查，他与大家一一握手交谈并表示亲切慰问。他仔细询问环境监察人员，了解现场执法检', null, null, null, '0', '2017-10-11 17:19:25');
INSERT INTO t_sys_org VALUES ('9', 'NWXX_BGS', '办公室', '5', '01', '01', '0', ',5,9,', null, null, null, null, '0', '2017-10-03 10:04:57');
INSERT INTO t_sys_org VALUES ('10', 'YXB', '营销部', '1', '02', '01', '1', ',1,10,', null, null, null, null, '1', '2017-11-09 22:44:29');

-- ----------------------------
-- Table structure for `t_sys_resource`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_resource`;
CREATE TABLE `t_sys_resource` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '资源主键',
  `NAME` varchar(64) NOT NULL COMMENT '名称',
  `PARENT_ID` int(11) NOT NULL COMMENT '上级节点',
  `URI` varchar(128) DEFAULT NULL COMMENT '功能URL',
  `ICON` varchar(64) DEFAULT NULL COMMENT '资源图标',
  `PATH` varchar(256) DEFAULT NULL COMMENT '资源路径',
  `AUTH` varchar(512) DEFAULT NULL COMMENT '功能键',
  `TARGET` varchar(32) DEFAULT NULL COMMENT '加载容器',
  `STATE` varchar(32) NOT NULL COMMENT '状态',
  `SORT` int(11) NOT NULL COMMENT '排序',
  `REMARK` varchar(1024) DEFAULT NULL COMMENT '描述',
  `PARAM1` varchar(512) DEFAULT NULL COMMENT '参数1',
  `PARAM2` varchar(512) DEFAULT NULL COMMENT '参数2',
  `PARAM3` varchar(512) DEFAULT NULL COMMENT '参数3',
  `LUSER` int(11) DEFAULT NULL COMMENT '最后修改人',
  `LTIME` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UN_RES_URI` (`URI`)
) ENGINE=InnoDB AUTO_INCREMENT=1231 DEFAULT CHARSET=utf8 COMMENT='系统资源';

-- ----------------------------
-- Records of t_sys_resource
-- ----------------------------
INSERT INTO t_sys_resource VALUES ('1000', '系统管理', '0', null, 'fa-cogs', ',1000,', '[]', null, '01', '1', null, null, null, null, '0', '2017-10-19 09:31:41');
INSERT INTO t_sys_resource VALUES ('1002', '机构管理', '1000', 'org/index', 'fa-sitemap', ',1000,1002,', '[{\"code\":\"all\",\"name\":\"所有权限\"}]', null, '01', '5', null, null, null, null, '1', '2017-11-09 19:08:38');
INSERT INTO t_sys_resource VALUES ('1003', '角色管理', '1000', 'role/index', 'fa-group', ',1000,1003,', '[{\"code\":\"all\",\"name\":\"所有权限\"}]', null, '01', '4', null, null, null, null, '1', '2017-11-09 19:08:33');
INSERT INTO t_sys_resource VALUES ('1004', '用户管理', '1000', 'user/index', 'fa-user', ',1000,1004,', '[{\"code\":\"all\",\"name\":\"所有权限\"}]', null, '01', '6', null, null, null, null, '1', '2017-11-09 19:08:42');
INSERT INTO t_sys_resource VALUES ('1100', '系统配置', '1000', null, 'fa-gear', ',1000,1100,', '[]', null, '01', '7', null, null, null, null, '1', '2017-11-05 10:20:53');
INSERT INTO t_sys_resource VALUES ('1101', '字典管理', '1100', 'dict/index', 'fa-book', ',1000,1100,1101,', '[{\"code\":\"all\",\"name\":\"所有权限\"}]', null, '01', '2', null, null, null, null, '1', '2017-11-01 22:14:54');
INSERT INTO t_sys_resource VALUES ('1102', '区域管理', '1100', 'area/index', 'fa-globe', ',1000,1100,1102,', '[{\"code\":\"all\",\"name\":\"所有权限\"}]', null, '01', '3', null, null, null, null, '1', '2017-11-01 22:36:01');
INSERT INTO t_sys_resource VALUES ('1103', '资源管理', '1100', 'resource/index', 'fa-list', ',1000,1100,1103,', '[{\"code\":\"all\",\"name\":\"所有权限\"}]', null, '01', '4', null, null, null, null, '0', '2017-10-19 09:32:45');
INSERT INTO t_sys_resource VALUES ('1104', '配置参数', '1100', 'config/index', 'fa-gavel', ',1000,1100,1104,', '[{\"code\":\"all\",\"name\":\"所有权限\"}]', null, '01', '1', null, null, null, null, '1', '2017-11-05 10:21:20');
INSERT INTO t_sys_resource VALUES ('1105', '定时任务', '1100', 'schedule/index', 'fa-tasks', ',1000,1100,1105,', '[{\"code\":\"all\",\"name\":\"所有权限\"}]', null, '01', '5', null, null, null, null, '1', '2017-11-05 10:15:34');
INSERT INTO t_sys_resource VALUES ('1106', '通知公告', '1000', 'notice/index', 'fa-bullhorn', ',1000,1106,', '[{\"code\":\"all\",\"name\":\"所有权限\"},{\"code\":\"add\",\"name\":\"新增\"},{\"code\":\"edit\",\"name\":\"修改\"},{\"code\":\"del\",\"name\":\"删除\"}]', null, '01', '2', null, null, null, null, '1', '2017-11-09 19:08:29');
INSERT INTO t_sys_resource VALUES ('1107', '知识库', '1000', 'doc/index', 'fa-file-word-o', ',1000,1107,', '[{\"code\":\"all\",\"name\":\"所有权限\"},{\"code\":\"del\",\"name\":\"删除\"},{\"code\":\"add\",\"name\":\"新增\"},{\"code\":\"edit\",\"name\":\"修改\"}]', null, '01', '3', null, null, null, null, '1', '2017-11-09 23:05:32');
INSERT INTO t_sys_resource VALUES ('1108', '系统反馈', '1000', 'feedback/index', 'fa-handshake-o', ',1000,1108,', '[{\"code\":\"all\",\"name\":\"所有权限\"},{\"code\":\"edit\",\"name\":\"修改\"},{\"code\":\"del\",\"name\":\"删除\"},{\"code\":\"add\",\"name\":\"新增\"}]', null, '01', '9', null, null, null, null, '1', '2017-11-09 19:08:30');
INSERT INTO t_sys_resource VALUES ('1109', '系统消息', '1000', null, 'fa-folder', ',1000,1109,', '[]', null, '01', '1', null, null, null, null, '1', '2017-11-10 15:36:18');
INSERT INTO t_sys_resource VALUES ('1110', '收件箱', '1109', 'msg/box/01', 'fa-envelope', ',1000,1109,1110,', '[]', null, '01', '1', null, null, null, null, '1', '2017-11-10 15:36:20');
INSERT INTO t_sys_resource VALUES ('1111', '发件箱', '1109', 'msg/box/02', 'fa-envelope-open', ',1000,1109,1111,', '[]', null, '01', '2', null, null, null, null, '1', '2017-11-10 15:36:30');
INSERT INTO t_sys_resource VALUES ('1112', '草稿箱', '1109', 'msg/box/03', 'fa-envelope-o', ',1000,1109,1112,', '[]', null, '01', '3', null, null, null, null, '1', '2017-11-10 15:36:55');
INSERT INTO t_sys_resource VALUES ('1113', '废件箱', '1109', 'msg/box/04', 'fa-trash-o', ',1000,1109,1113,', '[]', null, '02', '4', null, null, null, null, '1', '2017-11-09 19:08:53');
INSERT INTO t_sys_resource VALUES ('1199', '代码生成', '1100', 'generator/index', 'fa-codepen', ',1000,1100,1109,', '[{\"code\":\"all\",\"name\":\"所有权限\"}]', null, '01', '6', null, null, null, null, '1', '2017-11-05 10:17:27');
INSERT INTO t_sys_resource VALUES ('1200', '日志管理', '1000', null, 'fa-reorder', ',1000,1200,', '[]', null, '01', '8', null, null, null, null, '0', '2017-10-19 09:32:55');
INSERT INTO t_sys_resource VALUES ('1211', '登录日志', '1200', 'log/login/index', 'fa-universal-access', ',1000,1200,1211,', '[{\"code\":\"all\",\"name\":\"所有权限\"}]', null, '01', '1', null, null, null, null, '1', '2017-10-27 15:00:47');
INSERT INTO t_sys_resource VALUES ('1212', '访问日志', '1200', 'log/access/index', 'fa-keyboard-o', ',1000,1200,1212,', '[{\"code\":\"all\",\"name\":\"所有权限\"}]', null, '01', '2', null, null, null, null, '1', '2017-10-27 15:01:37');
INSERT INTO t_sys_resource VALUES ('1213', '阅读记录', '1200', 'log/reading/index', 'fa-eye', ',1000,1200,1213,', '[{\"code\":\"all\",\"name\":\"所有权限\"}]', null, '01', '4', null, null, null, null, '1', '2017-10-27 17:01:00');
INSERT INTO t_sys_resource VALUES ('1214', '上传文件', '1200', 'atta/index', 'fa-paperclip', ',1000,1200,1214,', '[{\"code\":\"all\",\"name\":\"所有权限\"}]', null, '01', '5', null, null, null, null, '1', '2017-11-05 10:28:04');
INSERT INTO t_sys_resource VALUES ('1215', '任务日志', '1200', 'log/schedule/index', 'fa-tasks', ',1000,1200,1215,', '[{\"code\":\"all\",\"name\":\"所有权限\"}]', null, '01', '3', null, null, null, null, '1', '2017-11-05 22:58:50');
INSERT INTO t_sys_resource VALUES ('1220', '系统日志', '1200', 'log/log4j', 'fa-file-archive-o', ',1000,1200,1220,', '[]', '', '01', '6', null, '', null, null, '1', '2017-10-27 09:40:21');
INSERT INTO t_sys_resource VALUES ('1230', '连接池监控', '1200', 'log/druid', 'fa-database', ',1000,1200,1230,', '[]', '', '01', '7', null, '', null, null, '1', '2017-10-27 12:11:31');

-- ----------------------------
-- Table structure for `t_sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role`;
CREATE TABLE `t_sys_role` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色主键',
  `CODE` varchar(64) NOT NULL COMMENT '角色代码',
  `NAME` varchar(64) NOT NULL COMMENT '角色名称',
  `TYPE` varchar(64) NOT NULL COMMENT '角色类型',
  `STATE` varchar(32) NOT NULL COMMENT '角色状态',
  `SORT` int(11) NOT NULL COMMENT '排序',
  `REMARK` varchar(1024) DEFAULT NULL COMMENT '描述',
  `PARAM1` varchar(512) DEFAULT NULL COMMENT '参数1',
  `PARAM2` varchar(512) DEFAULT NULL COMMENT '参数2',
  `PARAM3` varchar(512) DEFAULT NULL COMMENT '参数3',
  `LUSER` int(11) DEFAULT NULL COMMENT '最后修改人',
  `LTIME` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UN_ROLE_CODE` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='系统角色';

-- ----------------------------
-- Records of t_sys_role
-- ----------------------------
INSERT INTO t_sys_role VALUES ('1', 'SuperUser', '超级管理员', '01', '01', '1', null, null, null, null, '1', '2017-11-09 19:10:16');
INSERT INTO t_sys_role VALUES ('2', 'Administrator', '系统管理员', '02', '01', '10', null, null, null, null, '1', '2017-11-09 19:10:25');

-- ----------------------------
-- Table structure for `t_sys_role_resource`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role_resource`;
CREATE TABLE `t_sys_role_resource` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ROLE_ID` int(11) NOT NULL COMMENT '系统角_角色主键',
  `RES_ID` int(11) NOT NULL COMMENT '资源主键',
  `RES_CODE` varchar(64) DEFAULT NULL COMMENT '资源编号',
  PRIMARY KEY (`ID`),
  KEY `FK_REFERENCE_11` (`ROLE_ID`),
  KEY `FK_REFERENCE_6` (`RES_ID`),
  CONSTRAINT `FK_REFERENCE_11` FOREIGN KEY (`ROLE_ID`) REFERENCES `t_sys_role` (`ID`),
  CONSTRAINT `FK_REFERENCE_6` FOREIGN KEY (`RES_ID`) REFERENCES `t_sys_resource` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=992 DEFAULT CHARSET=utf8 COMMENT='系统角色资源权限';

-- ----------------------------
-- Records of t_sys_role_resource
-- ----------------------------
INSERT INTO t_sys_role_resource VALUES ('893', '1', '1000', null);
INSERT INTO t_sys_role_resource VALUES ('894', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('895', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('896', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('897', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('898', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('899', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('900', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('901', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('902', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('903', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('904', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('905', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('906', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('907', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('908', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('909', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('910', '1', '1000', 'all');
INSERT INTO t_sys_role_resource VALUES ('911', '1', '1002', null);
INSERT INTO t_sys_role_resource VALUES ('912', '1', '1002', 'all');
INSERT INTO t_sys_role_resource VALUES ('913', '1', '1003', null);
INSERT INTO t_sys_role_resource VALUES ('914', '1', '1003', 'all');
INSERT INTO t_sys_role_resource VALUES ('915', '1', '1109', null);
INSERT INTO t_sys_role_resource VALUES ('916', '1', '1112', null);
INSERT INTO t_sys_role_resource VALUES ('917', '1', '1113', null);
INSERT INTO t_sys_role_resource VALUES ('918', '1', '1110', null);
INSERT INTO t_sys_role_resource VALUES ('919', '1', '1111', null);
INSERT INTO t_sys_role_resource VALUES ('920', '1', '1200', null);
INSERT INTO t_sys_role_resource VALUES ('921', '1', '1200', 'all');
INSERT INTO t_sys_role_resource VALUES ('922', '1', '1200', 'all');
INSERT INTO t_sys_role_resource VALUES ('923', '1', '1200', 'all');
INSERT INTO t_sys_role_resource VALUES ('924', '1', '1200', 'all');
INSERT INTO t_sys_role_resource VALUES ('925', '1', '1200', 'all');
INSERT INTO t_sys_role_resource VALUES ('926', '1', '1211', null);
INSERT INTO t_sys_role_resource VALUES ('927', '1', '1211', 'all');
INSERT INTO t_sys_role_resource VALUES ('928', '1', '1214', null);
INSERT INTO t_sys_role_resource VALUES ('929', '1', '1214', 'all');
INSERT INTO t_sys_role_resource VALUES ('930', '1', '1215', null);
INSERT INTO t_sys_role_resource VALUES ('931', '1', '1215', 'all');
INSERT INTO t_sys_role_resource VALUES ('932', '1', '1212', null);
INSERT INTO t_sys_role_resource VALUES ('933', '1', '1212', 'all');
INSERT INTO t_sys_role_resource VALUES ('934', '1', '1230', null);
INSERT INTO t_sys_role_resource VALUES ('935', '1', '1220', null);
INSERT INTO t_sys_role_resource VALUES ('936', '1', '1213', null);
INSERT INTO t_sys_role_resource VALUES ('937', '1', '1213', 'all');
INSERT INTO t_sys_role_resource VALUES ('938', '1', '1100', null);
INSERT INTO t_sys_role_resource VALUES ('939', '1', '1100', 'all');
INSERT INTO t_sys_role_resource VALUES ('940', '1', '1100', 'all');
INSERT INTO t_sys_role_resource VALUES ('941', '1', '1100', 'all');
INSERT INTO t_sys_role_resource VALUES ('942', '1', '1100', 'all');
INSERT INTO t_sys_role_resource VALUES ('943', '1', '1100', 'all');
INSERT INTO t_sys_role_resource VALUES ('944', '1', '1100', 'all');
INSERT INTO t_sys_role_resource VALUES ('945', '1', '1103', null);
INSERT INTO t_sys_role_resource VALUES ('946', '1', '1103', 'all');
INSERT INTO t_sys_role_resource VALUES ('947', '1', '1102', null);
INSERT INTO t_sys_role_resource VALUES ('948', '1', '1102', 'all');
INSERT INTO t_sys_role_resource VALUES ('949', '1', '1101', null);
INSERT INTO t_sys_role_resource VALUES ('950', '1', '1101', 'all');
INSERT INTO t_sys_role_resource VALUES ('951', '1', '1105', null);
INSERT INTO t_sys_role_resource VALUES ('952', '1', '1105', 'all');
INSERT INTO t_sys_role_resource VALUES ('953', '1', '1104', null);
INSERT INTO t_sys_role_resource VALUES ('954', '1', '1104', 'all');
INSERT INTO t_sys_role_resource VALUES ('955', '1', '1199', null);
INSERT INTO t_sys_role_resource VALUES ('956', '1', '1199', 'all');
INSERT INTO t_sys_role_resource VALUES ('957', '1', '1108', null);
INSERT INTO t_sys_role_resource VALUES ('958', '1', '1108', 'all');
INSERT INTO t_sys_role_resource VALUES ('959', '1', '1004', null);
INSERT INTO t_sys_role_resource VALUES ('960', '1', '1004', 'all');
INSERT INTO t_sys_role_resource VALUES ('961', '1', '1107', null);
INSERT INTO t_sys_role_resource VALUES ('962', '1', '1107', 'all');
INSERT INTO t_sys_role_resource VALUES ('963', '1', '1106', null);
INSERT INTO t_sys_role_resource VALUES ('964', '1', '1106', 'all');
INSERT INTO t_sys_role_resource VALUES ('965', '2', '1000', null);
INSERT INTO t_sys_role_resource VALUES ('966', '2', '1002', null);
INSERT INTO t_sys_role_resource VALUES ('967', '2', '1003', null);
INSERT INTO t_sys_role_resource VALUES ('968', '2', '1109', null);
INSERT INTO t_sys_role_resource VALUES ('969', '2', '1112', null);
INSERT INTO t_sys_role_resource VALUES ('970', '2', '1113', null);
INSERT INTO t_sys_role_resource VALUES ('971', '2', '1110', null);
INSERT INTO t_sys_role_resource VALUES ('972', '2', '1111', null);
INSERT INTO t_sys_role_resource VALUES ('973', '2', '1200', null);
INSERT INTO t_sys_role_resource VALUES ('974', '2', '1211', null);
INSERT INTO t_sys_role_resource VALUES ('975', '2', '1214', null);
INSERT INTO t_sys_role_resource VALUES ('976', '2', '1215', null);
INSERT INTO t_sys_role_resource VALUES ('977', '2', '1212', null);
INSERT INTO t_sys_role_resource VALUES ('978', '2', '1230', null);
INSERT INTO t_sys_role_resource VALUES ('979', '2', '1220', null);
INSERT INTO t_sys_role_resource VALUES ('980', '2', '1213', null);
INSERT INTO t_sys_role_resource VALUES ('981', '2', '1100', null);
INSERT INTO t_sys_role_resource VALUES ('982', '2', '1103', null);
INSERT INTO t_sys_role_resource VALUES ('983', '2', '1102', null);
INSERT INTO t_sys_role_resource VALUES ('984', '2', '1101', null);
INSERT INTO t_sys_role_resource VALUES ('985', '2', '1105', null);
INSERT INTO t_sys_role_resource VALUES ('986', '2', '1104', null);
INSERT INTO t_sys_role_resource VALUES ('987', '2', '1199', null);
INSERT INTO t_sys_role_resource VALUES ('988', '2', '1108', null);
INSERT INTO t_sys_role_resource VALUES ('989', '2', '1004', null);
INSERT INTO t_sys_role_resource VALUES ('990', '2', '1107', null);
INSERT INTO t_sys_role_resource VALUES ('991', '2', '1106', null);

-- ----------------------------
-- Table structure for `t_sys_schedule`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_schedule`;
CREATE TABLE `t_sys_schedule` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '任务主键',
  `CODE` varchar(64) NOT NULL COMMENT '任务代码',
  `NAME` varchar(64) NOT NULL COMMENT '任务名称',
  `TYPE` varchar(32) NOT NULL DEFAULT '01' COMMENT '任务类型',
  `STATE` varchar(32) NOT NULL COMMENT '任务状态',
  `IMPL` varchar(256) NOT NULL COMMENT '实现类',
  `CRON` varchar(128) NOT NULL COMMENT 'CRON表达式',
  `SERVER_IP` varchar(32) DEFAULT NULL COMMENT '执行服务器主机名或者IP',
  `SORT` int(11) NOT NULL COMMENT '排序',
  `REMARK` varchar(1024) DEFAULT NULL COMMENT '描述',
  `PARAM1` varchar(512) DEFAULT NULL COMMENT '参数1',
  `PARAM2` varchar(512) DEFAULT NULL COMMENT '参数2',
  `PARAM3` varchar(512) DEFAULT NULL COMMENT '参数3',
  `LUSER` int(11) DEFAULT NULL COMMENT '最后修改人',
  `LTIME` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UN_CONFIG_CODE` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='系统定时任务';

-- ----------------------------
-- Records of t_sys_schedule
-- ----------------------------
INSERT INTO t_sys_schedule VALUES ('1', 'ClearInvalidAttasTask', '清除无效附件', 'DEFAULT_JOB_GROUP', 'NONE', 'com.yj.auto.job.impl.ClearInvalidAttasJob', '0 0 0/1 * * ? ', '192.168.1.14', '10', '每小时运行', null, null, null, '1', '2017-11-07 09:53:35');
INSERT INTO t_sys_schedule VALUES ('2', 'ClearTempFileJob', '清除临时文件', 'DEFAULT_JOB_GROUP', 'NONE', 'com.yj.auto.job.impl.ClearTempFileJob', '0 0 0/1 * * ? ', '192.168.1.14', '10', '每小时运行', null, '0 0 0 1/1 * ? ', null, '1', '2017-11-07 09:23:39');
INSERT INTO t_sys_schedule VALUES ('3', 'ClearLogHistoryTask', '清除日志', 'DEFAULT_JOB_GROUP', 'NONE', 'com.yj.auto.job.impl.ClearLogHistoryJob', '0 0 0/1 * * ? ', '192.168.1.14', '10', '每小时运行', '', '', '', '1', '2017-11-07 10:56:23');
INSERT INTO t_sys_schedule VALUES ('4', 'LuceneIndexBuildJob', '生成全文检索索引', 'DEFAULT_JOB_GROUP', 'NONE', 'com.yj.auto.plugin.lucene.LuceneIndexBuildJob', '0 0 0/1 * * ?', '192.168.1.105', '10', null, null, null, null, '1', '2017-11-08 12:52:21');

-- ----------------------------
-- Table structure for `t_sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user`;
CREATE TABLE `t_sys_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户主键',
  `ORG_ID` int(11) DEFAULT NULL COMMENT '机构主键',
  `CODE` varchar(64) NOT NULL COMMENT '登录账号',
  `NAME` varchar(64) NOT NULL COMMENT '用户名称',
  `PWD` varchar(64) NOT NULL COMMENT '登录密码',
  `TEL` varchar(32) DEFAULT NULL COMMENT '电话号码',
  `MOBILE` varchar(32) DEFAULT NULL COMMENT '手机号码',
  `EMAIL` varchar(32) DEFAULT NULL COMMENT '电子邮件',
  `CARD` varchar(32) DEFAULT NULL COMMENT '身份证',
  `STATE` varchar(32) NOT NULL COMMENT '用户状态',
  `LOGIN_IP` varchar(32) DEFAULT NULL COMMENT '最后登录IP',
  `LOGIN_TIME` datetime DEFAULT NULL COMMENT '最后登录时间',
  `SORT` int(11) NOT NULL COMMENT '排序号',
  `REMARK` varchar(1024) DEFAULT NULL COMMENT '描述',
  `PARAM1` varchar(512) DEFAULT NULL COMMENT '参数1',
  `PARAM2` varchar(512) DEFAULT NULL COMMENT '参数2',
  `PARAM3` varchar(512) DEFAULT NULL COMMENT '参数3',
  `LTIME` datetime DEFAULT NULL COMMENT '最后修改时间',
  `LUSER` int(11) DEFAULT NULL COMMENT '最后修改人',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UN_USER_CODE` (`CODE`),
  KEY `FK_REF_USER_ORG` (`ORG_ID`),
  CONSTRAINT `FK_REF_USER_ORG` FOREIGN KEY (`ORG_ID`) REFERENCES `t_sys_org` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='系统用户';

-- ----------------------------
-- Records of t_sys_user
-- ----------------------------
INSERT INTO t_sys_user VALUES ('1', '1', 'sa', 'Super', 'eed8cdc400dfd4ec85dff70a170066b7', '159', null, 'test@qq.com', null, '01', null, null, '0', null, null, null, null, '2017-11-02 18:34:42', '1');
INSERT INTO t_sys_user VALUES ('2', '1', 'admin', '管理员', 'eed8cdc400dfd4ec85dff70a170066b7', null, null, 'test@qq.com', null, '01', null, null, '1', null, null, null, null, '2017-10-12 17:40:06', '1');
INSERT INTO t_sys_user VALUES ('3', '1', 'test', '测试用户', 'eed8cdc400dfd4ec85dff70a170066b7', null, null, 'test@qq.com', null, '02', null, null, '10', null, null, null, null, '2017-11-02 13:11:40', '1');

-- ----------------------------
-- Table structure for `t_sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user_role`;
CREATE TABLE `t_sys_user_role` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `USER_ID` int(11) NOT NULL COMMENT '用户主键',
  `ROLE_ID` int(11) NOT NULL COMMENT '角色主键',
  PRIMARY KEY (`ID`),
  KEY `FK_REFERENCE_10` (`ROLE_ID`),
  KEY `FK_REFERENCE_8` (`USER_ID`),
  CONSTRAINT `FK_REFERENCE_10` FOREIGN KEY (`ROLE_ID`) REFERENCES `t_sys_role` (`ID`),
  CONSTRAINT `FK_REFERENCE_8` FOREIGN KEY (`USER_ID`) REFERENCES `t_sys_user` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='系统用户角色组';

-- ----------------------------
-- Records of t_sys_user_role
-- ----------------------------
INSERT INTO t_sys_user_role VALUES ('1', '1', '1');
INSERT INTO t_sys_user_role VALUES ('25', '2', '2');
INSERT INTO t_sys_user_role VALUES ('27', '3', '2');

-- ----------------------------
-- Procedure structure for `DEL_LOG_ACCESS`
-- ----------------------------
DROP PROCEDURE IF EXISTS `DEL_LOG_ACCESS`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `DEL_LOG_ACCESS`()
BEGIN
	DECLARE today VARCHAR(32);
	SET today=DATE_ADD(NOW(),INTERVAL -15 DAY);
	SET @delSql=CONCAT('DELETE FROM T_LOG_ACCESS WHERE ACCESS_TIME<''',today,'''');
	
	PREPARE DEL FROM @delSql;
	EXECUTE DEL;

	DEALLOCATE PREPARE DEL;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `DEL_LOG_LOGIN`
-- ----------------------------
DROP PROCEDURE IF EXISTS `DEL_LOG_LOGIN`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `DEL_LOG_LOGIN`()
BEGIN
	DECLARE today VARCHAR(32);
	SET today=DATE_ADD(NOW(),INTERVAL -3 MONTH);
	SET @delSql=CONCAT('DELETE FROM T_LOG_LOGIN WHERE LOGIN_TIME<''',today,'''');
	PREPARE DEL FROM @delSql;
	EXECUTE DEL;

	DEALLOCATE PREPARE DEL;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `DEL_LOG_SCHEDULE`
-- ----------------------------
DROP PROCEDURE IF EXISTS `DEL_LOG_SCHEDULE`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `DEL_LOG_SCHEDULE`()
BEGIN
	DECLARE today VARCHAR(32);
	SET today=DATE_ADD(NOW(),INTERVAL -15 DAY);
	SET @delSql=CONCAT('DELETE FROM T_LOG_SCHEDULE WHERE STIME<''',today,'''');
	
	PREPARE DEL FROM @delSql;
	EXECUTE DEL;

	DEALLOCATE PREPARE DEL;

END
;;
DELIMITER ;

-- ----------------------------
-- Event structure for `DEL_LOG_ACCESS`
-- ----------------------------
DROP EVENT IF EXISTS `DEL_LOG_ACCESS`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` EVENT `DEL_LOG_ACCESS` ON SCHEDULE EVERY 1 DAY STARTS '2017-07-28 01:30:00' ON COMPLETION NOT PRESERVE ENABLE DO CALL DEL_LOG_ACCESS()
;;
DELIMITER ;

-- ----------------------------
-- Event structure for `DEL_LOG_LOGIN`
-- ----------------------------
DROP EVENT IF EXISTS `DEL_LOG_LOGIN`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` EVENT `DEL_LOG_LOGIN` ON SCHEDULE EVERY 1 DAY STARTS '2017-07-28 01:30:00' ON COMPLETION NOT PRESERVE ENABLE DO CALL DEL_LOG_LOGIN()
;;
DELIMITER ;

-- ----------------------------
-- Event structure for `DEL_LOG_SCHEDULE`
-- ----------------------------
DROP EVENT IF EXISTS `DEL_LOG_SCHEDULE`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` EVENT `DEL_LOG_SCHEDULE` ON SCHEDULE EVERY 1 DAY STARTS '2017-07-28 01:30:00' ON COMPLETION NOT PRESERVE ENABLE DO CALL DEL_LOG_SCHEDULE()
;;
DELIMITER ;
