package #(basepackage).service;

import com.jfinal.kit.*;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.*;
import com.yj.auto.Constants;
import com.yj.auto.core.base.model.*;
import com.yj.auto.core.jfinal.base.*;
import com.yj.auto.core.base.annotation.*;
import #(basepackage).model.*;

/**
 * #(modelName) 管理	
 * 描述：
 */
@Service(name = "#(firstCharToLowerCase(modelName))Srv")
public class #(modelName)Service  extends BaseService<#(modelName)> {

	private static final Log logger = Log.getLog(#(modelName)Service.class);
	
	public static final String SQL_LIST = "#(sqlnamespace).#(simpleName).list";
	
	public static final #(modelName) dao = new #(modelName)().dao();
	
	@Override
	public #(modelName) getDao() {
		return dao;
	}

	@Override
	public SqlPara getListSqlPara(QueryModel query) {
		if (StrKit.isBlank(query.getOrderby())) {
			query.setOrderby("sort");
		}
		return getSqlPara(SQL_LIST, query);
	}	
   	#for(x : table.columnMetas)#if(!x.isUnique)#continue#end
   	
    //根据#(x.remarks)查询
	public #(modelName) getBy#(firstCharToUpperCase(x.attrName))(#(x.javaType) #(x.attrName)){
		String sql="select * from #(table.name.toLowerCase()) where #(x.name.toLowerCase())=?";
   		return dao.findFirst(sql, #(x.attrName));
   	}
	#end
	
}
