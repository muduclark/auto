package #(basepackage).model;

import com.yj.auto.core.base.annotation.*;
import #(basepackage).model.bean.*;
/**
 * #(table.remarks)
 */
@SuppressWarnings("serial")
@Table(name = #(modelName).TABLE_NAME, key = #(modelName).TABLE_PK, remark = #(modelName).TABLE_REMARK)
public class #(modelName) extends #(modelName)Entity<#(modelName)> {

}
