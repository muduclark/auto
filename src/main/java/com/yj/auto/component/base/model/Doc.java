package com.yj.auto.component.base.model;

import com.yj.auto.core.base.annotation.*;
import com.yj.auto.component.base.model.bean.*;

/**
 * 知识库
 */
@SuppressWarnings("serial")
@Table(name = Doc.TABLE_NAME, key = Doc.TABLE_PK, remark = Doc.TABLE_REMARK)
public class Doc extends DocEntity<Doc> {
	private DocType type = null;

	public DocType getType() {
		return type;
	}

	public void setType(DocType type) {
		this.type = type;
	}

}