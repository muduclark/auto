package com.yj.auto.component.base.model.bean;

import java.util.Date;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;
import com.yj.auto.core.jfinal.base.*;

/**
 * 知识库
 */
@SuppressWarnings("serial")
public abstract class DocEntity<M extends DocEntity<M>> extends BaseEntity<M> {

	public static final String TABLE_NAME = "t_com_doc"; // 数据表名称

	public static final String TABLE_PK = "id"; // 数据表主键

	public static final String TABLE_REMARK = "知识库"; // 数据表备注

	public String getTableName() {
		return TABLE_NAME;
	}

	public String getTableRemark() {
		return TABLE_REMARK;
	}

	public String getTablePK() {
		return TABLE_PK;
	}

	/**
	 * Column ：id
	 * 
	 * @return 主键
	 */

	public Integer getId() {
		return get("id");
	}

	public void setId(Integer id) {
		set("id", id);
	}

	/**
	 * Column ：type_id
	 * 
	 * @return 知识分类
	 */
	@NotNull
	public Integer getTypeId() {
		return get("type_id");
	}

	public void setTypeId(Integer typeId) {
		set("type_id", typeId);
	}

	/**
	 * Column ：title
	 * 
	 * @return 标题
	 */
	@NotBlank
	@Length(max = 256)
	public String getTitle() {
		return get("title");
	}

	public void setTitle(String title) {
		set("title", title);
	}

	/**
	 * Column ：keyword
	 * 
	 * @return 关键字
	 */
	@Length(max = 512)
	public String getKeyword() {
		return get("keyword");
	}

	public void setKeyword(String keyword) {
		set("keyword", keyword);
	}

	/**
	 * Column ：ctime
	 * 
	 * @return 上传日期
	 */

	public Date getCtime() {
		return get("ctime");
	}

	public void setCtime(Date ctime) {
		set("ctime", ctime);
	}

	/**
	 * Column ：user_id
	 * 
	 * @return 上传人员
	 */

	public Integer getUserId() {
		return get("user_id");
	}

	public void setUserId(Integer userId) {
		set("user_id", userId);
	}

	/**
	 * Column ：evaluate
	 * 
	 * @return 知识评星
	 */
	@Length(max = 32)
	public String getEvaluate() {
		return get("evaluate");
	}

	public void setEvaluate(String evaluate) {
		set("evaluate", evaluate);
	}

	/**
	 * Column ：remark
	 * 
	 * @return 知识简介
	 */
	@Length(max = 1024)
	public String getRemark() {
		return get("remark");
	}

	public void setRemark(String remark) {
		set("remark", remark);
	}

	/**
	 * Column ：sort
	 * 
	 * @return 排序号
	 */
	@NotNull
	public Integer getSort() {
		return get("sort");
	}

	public void setSort(Integer sort) {
		set("sort", sort);
	}

	/**
	 * Column ：state
	 * 
	 * @return 状态
	 */
	@Length(max = 32)
	public String getState() {
		return get("state");
	}

	public void setState(String state) {
		set("state", state);
	}

	/**
	 * Column ：param1
	 * 
	 * @return 参数一
	 */
	@Length(max = 512)
	public String getParam1() {
		return get("param1");
	}

	public void setParam1(String param1) {
		set("param1", param1);
	}

	/**
	 * Column ：param2
	 * 
	 * @return 参数二
	 */
	@Length(max = 512)
	public String getParam2() {
		return get("param2");
	}

	public void setParam2(String param2) {
		set("param2", param2);
	}

	/**
	 * Column ：param3
	 * 
	 * @return 参数三
	 */
	@Length(max = 512)
	public String getParam3() {
		return get("param3");
	}

	public void setParam3(String param3) {
		set("param3", param3);
	}

	/**
	 * Column ：luser
	 * 
	 * @return 最后编辑人
	 */

	public Integer getLuser() {
		return get("luser");
	}

	public void setLuser(Integer luser) {
		set("luser", luser);
	}

	/**
	 * Column ：ltime
	 * 
	 * @return 最后编辑时间
	 */

	public Date getLtime() {
		return get("ltime");
	}

	public void setLtime(Date ltime) {
		set("ltime", ltime);
	}
}