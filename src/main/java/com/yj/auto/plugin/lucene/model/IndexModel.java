package com.yj.auto.plugin.lucene.model;

import com.jfinal.json.FastJson;

public class IndexModel {
	public final static String IM_ID = "id";
	public final static String IM_NAME = "name";
	//public final static String IM_DATA_TABLE = "dataTable";
	public final static String IM_DATA_ID = "dataId";
	public final static String IM_DATA_TYPE = "dataType";
	public final static String IM_CONTENT = "content";
	private Integer id;
	private String name;
	private Integer dataId;
	private String dataType;
	private String fragment;

	public IndexModel() {
		super();
	}

	public IndexModel(Integer id, String name, Integer dataId, String dataType, String fragment) {
		super();
		this.id = id;
		this.name = name;
		this.dataId = dataId;
		this.dataType = dataType;
		this.fragment = fragment;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDataId() {
		return dataId;
	}

	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getFragment() {
		return fragment;
	}

	public void setFragment(String fragment) {
		this.fragment = fragment;
	}

	public String toJson() {
		return FastJson.getJson().toJson(this);
	}
}
