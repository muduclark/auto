package com.yj.auto.core.web.system.model;

import com.yj.auto.core.base.annotation.*;
import com.yj.auto.core.web.system.model.bean.*;
/**
 * 系统角色资源权限
 */
@SuppressWarnings("serial")
@Table(name = RoleResource.TABLE_NAME, key = RoleResource.TABLE_PK, remark = RoleResource.TABLE_REMARK)
public class RoleResource extends RoleResourceEntity<RoleResource> {
	

}