package com.yj.auto.core.web.system.controller;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.log.Log;
import com.yj.auto.Constants;
import com.yj.auto.core.base.annotation.Controller;
import com.yj.auto.core.base.annotation.Valid;
import com.yj.auto.core.base.model.ResponseModel;
import com.yj.auto.core.jfinal.base.BaseController;
import com.yj.auto.core.web.system.model.Dict;
import com.yj.auto.core.web.system.service.DictService;
import com.yj.auto.plugin.ztree.ZTreeNode;

/**
 * 系统代码表 管理 描述：
 * 
 */
@Controller(viewPath = "system")
public class DictController extends BaseController {
	private static final Log logger = Log.getLog(DictController.class);

	private static final String RESOURCE_URI = "dict/index";
	private static final String INDEX_PAGE = "dict_index.html";
	private static final String FORM_PAGE = "dict_form.html";
	private static final String SHOW_PAGE = "dict_show.html";
	public static final String DICT_TYPE = "DICT_TYPE";

	DictService dictSrv = null;

	public void index() {
		render(INDEX_PAGE);
	}

	private void tree() {
		List<ZTreeNode> result = new ArrayList<ZTreeNode>();
		List<Dict> list = dictSrv.children(DICT_TYPE, null);
		for (Dict c : list) {
			ZTreeNode t = getZTreeNode(c, false);
			t.addAttribute("type", c.getVal());
			t.addAttribute("url", "dict/get/" + c.getId());
			t.setOpen(true);
			List<Dict> child = dictSrv.children(0, null, c.getVal());
			for (Dict d : child) {
				if (d.getParentId() == 0 && DICT_TYPE.equals(d.getVal()))
					continue;
				t.addChildren(getZTreeNode(d, dictSrv.children(d.getId(), null).size() > 0));
			}
			result.add(t);
		}
		renderJson(result);
	}

	public void children() {
		Integer id = getParaToInt("id");
		if (null == id) {
			tree();
			return;
		}
		List<Dict> list = dictSrv.children(id, null);
		List<ZTreeNode> result = new ArrayList<ZTreeNode>();
		for (Dict c : list) {
			ZTreeNode t = getZTreeNode(c, dictSrv.children(c.getId(), null).size() > 0);
			result.add(t);
		}
		renderJson(result);
	}

	private ZTreeNode getZTreeNode(Dict c, boolean parent) {
		ZTreeNode t = new ZTreeNode(c.getId().toString(), c.getName());
		t.setIsParent(parent);
		t.addAttribute("type", c.getType());
		t.addAttribute("val", c.getVal());
		return t;
	}

	public void get() {
		Integer id = getParaToInt(0);
		Dict model = dictSrv.get(id);
		setAttr("model", model);
		render(SHOW_PAGE);
	}

	public boolean updateSort() {
		Integer[] id = getParaValuesToInt();
		boolean success = dictSrv.updateSort(id);
		ResponseModel<String> res = new ResponseModel<String>(success);
		renderJson(res);
		return success;
	}

	public void form() throws Exception {
		String id = getPara(0);
		Dict model = null;
		if (id.startsWith("new_")) {
			model = new Dict();
			Integer parentId = getParaToInt(1);
			if (null == parentId || parentId < 11) {
				parentId = 0;
			}
			model.setParentId(parentId);
			model.setSort(getParaToInt(2));
			model.setName(getParaDecode(3));
		} else {
			model = dictSrv.get(Integer.parseInt(id));
		}
		if (null != model.getParentId()) {
			Dict parent = dictSrv.get(model.getParentId());
			if (null != parent) {
				setAttr("parent", parent);
			}
		}
		setAttr("model", model);
		render(FORM_PAGE);
	}

	@Valid(type = Dict.class)
	public boolean saveOrUpdate(Dict model) {
		boolean success = false;
		model.setLuser(getSuId());
		model.setLtime(Constants.NOW());
		if (null == model.getId()) {
			success = dictSrv.save(model);
		} else {
			success = dictSrv.update(model);
		}
		ResponseModel<String> res = new ResponseModel<String>(success);
		res.setData(model.getId().toString());
		res.setMsg("保存" + dictSrv.getDao().getTableRemark() + (res.isSuccess() ? "成功" : "失败"));
		renderJson(res);
		return success;
	}

	public boolean delete() {
		ResponseModel<String> res = _delete(dictSrv);
		return res.isSuccess();
	}

	public void cache() {
		List<Dict> list = dictSrv.getCache(this.getPara());
		ResponseModel<List<Dict>> res = new ResponseModel<List<Dict>>(true);
		res.setData(list);
		renderJson(res);
	}
}