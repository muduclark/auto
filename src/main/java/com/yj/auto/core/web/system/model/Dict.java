package com.yj.auto.core.web.system.model;

import com.yj.auto.core.base.annotation.*;
import com.yj.auto.core.web.system.model.bean.*;
/**
 * 系统代码表
 */
@SuppressWarnings("serial")
@Table(name = Dict.TABLE_NAME, key = Dict.TABLE_PK, remark = Dict.TABLE_REMARK)
public class Dict extends DictEntity<Dict> {
	
}