package com.yj.auto.core.base.exception;

public class AutoRuntimeException extends RuntimeException {

	public AutoRuntimeException() {
		super();
	}

	public AutoRuntimeException(String msg) {
		super(msg);
	}

	public AutoRuntimeException(String msg, Throwable e) {
		super(msg, e);
	}
}
