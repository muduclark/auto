package com.yj.auto.core.base.online;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

public class OnlineContext {
	private final static Map<String, HttpSession> onlineMap = new HashMap<String, HttpSession>();

	public static HttpSession get(String sessionId) {
		return onlineMap.get(sessionId);
	}

	public static synchronized void add(HttpSession session) {
		if (null != session && null != SessionUserUtil.getSessionUser(session)) {
			onlineMap.put(session.getId(), session);
		}
	}

	public static synchronized void remove(String sessionId) {
		onlineMap.remove(sessionId);
	}

	public static synchronized void invalidate(String sessionId) {
		HttpSession session = get(sessionId);
		if (null != session) {
			session.invalidate();
		}
	}

	public static synchronized void invalidateAndRemove(String sessionId) {
		invalidate(sessionId);
		remove(sessionId);
	}
}
