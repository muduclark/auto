package com.yj.auto.core.base.model;

//消息验证结果Bean
public class ValidResult {
	private String field;
	private String type;
	private String msg;

	public ValidResult(String field, String type) {
		this.field = field;
		this.type = type;
	}

	public ValidResult(String id, String type, String msg) {
		this(id, type);
		this.msg = msg;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}