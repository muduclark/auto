eclipse wtp 工程，用户名、密码：sa/ffffff

# 基础功能 #
- 系统管理
- 系统消息
	- 收件箱
	- 发件箱
	- 草稿箱
- 通知公告
- 知识库
- 角色管理
- 机构管理
- 用户管理
- 系统配置
- 配置参数
	- 字典管理
	- 区域管理
	- 资源管理
	- 定时任务
	- 代码生成
- 日志管理
	- 登录日志
	- 访问日志
	- 任务日志
	- 阅读记录
	- 上传文件
	- 系统日志
	- 连接池监控
- 系统反馈

# 架构 #

- JAVA
	- jfinal3.3
	- apache-commons
	- hibernate.validator
	- quartz
	- fastjson
	- druid
	- lucene
	- poi
	- log4j
	- hutool-all
	
- JS Plugins
	- jquery
	- bootstrap
	- awesome
	- AdminLTE-master
	- webuploader
	- echarts
	- zTree
	
- DB
	- mysql-5.6.38
	
- Server
	- tomcat8	

# 界面截图 #

![系统登录](https://gitee.com/uploads/images/2017/1127/210011_7524a6db_1239052.png "系统登录.png")

![系统首页](https://gitee.com/uploads/images/2017/1127/210034_c505fb3c_1239052.png "系统首页.png")

![字典管理](https://gitee.com/uploads/images/2017/1127/210057_0a5f8d31_1239052.png "字典管理.png")

![角色权限管理](https://gitee.com/uploads/images/2017/1127/210122_386d5723_1239052.png "角色管理.png")

![定时任务管理](https://gitee.com/uploads/images/2017/1127/210208_04688c98_1239052.png "定时任务.png")

![代码生成](https://gitee.com/uploads/images/2017/1127/210226_8d4f6489_1239052.png "代码生成.png")

![内部邮件](https://gitee.com/uploads/images/2017/1127/210247_08e24f74_1239052.png "新建邮件.png")

![知识库](https://gitee.com/uploads/images/2017/1127/210303_74ad7c3f_1239052.png "知识库.png")

![全文检索](https://gitee.com/uploads/images/2017/1127/210320_8c15457d_1239052.png "全文检索.png")
